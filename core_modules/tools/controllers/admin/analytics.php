<?php

class Analytics extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->bep_assets->load_asset_group('GA');
	}
	
	public function index($view=NULL)
	{
		$this->load->module_library('tools','ganalytics');
		
		if($post=$this->input->post()){
			$post = array_filter($post);

			if(isset($post['from_date'])) $this->session->set_userdata('from_date',date('Y-m-d',strtotime($post['from_date'])));
			if(isset($post['to_date'])) $this->session->set_userdata('to_date',date('Y-m-d',strtotime($post['to_date'])));
			if(isset($post['period'])) $this->session->set_userdata('period',$post['period']);
		}

		
		$from = $this->session->userdata('from_date')? $this->session->userdata('from_date'):date('Y-m-d',strtotime('-1 weeks'));
		$to = $this->session->userdata('to_date')? $this->session->userdata('to_date'):date('Y-m-d');
		
		$period = $this->session->userdata('period')? $this->session->userdata('period'):'ga:date';
		
		$this->ganalytics->setDateRange($from, $to);
		$this->ganalytics->setPeriod($period);
		$data['module']='tools';
		$data['header']='Google Analytics';
		if($view){
			$data['page'] = $this->config->item('template_admin')."analytics/$view";
			$this->load->view($this->_container, $data);
		}else{
			$data['page'] = $this->config->item('template_admin').'analytics/index';
			$this->load->view($this->_container, $data);
		}		
	}
	
}