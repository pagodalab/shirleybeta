
<div region="center" border="false">
<div style="padding:20px">

<div id="ga_container">
	<h2>Google Analytics Report</h2>
	<form action="" method="post">
	<div id="ga_search" >
		<table>
			<tr>
<?php
$from = $this->session->userdata('from_date')? date('m/d/Y',strtotime($this->session->userdata('from_date'))):date('m/d/Y',strtotime('-1 weeks'));
$to = $this->session->userdata('to_date')? date('m/d/Y',strtotime($this->session->userdata('to_date'))):date('m/d/Y');

?>
				<td width="230">
                From date <input class="easyui-datebox" id="from_date" name="from_date" value="<?php echo $from; ?>" size="30" type="text" style="float:left;padding:5px;width:230px;border:1px solid #ddd" />
				</td>
				<td width="230">
				 To date <input class="easyui-datebox" id="to_date"  name="to_date" value="<?php echo $to; ?>" size="30" type="text" style="float:left;padding:5px;width:230px;border:1px solid #ddd" />
				</td>
				<td width="100" valign="bottom"><input name="submit" value="Refresh Report" style="margin-top:10px;margin-left:0px" type="submit" /></td>
				<td width="110"> </td>
				<td><ul class="options_container">
					<?php
						$period = $this->session->userdata('period')? $this->session->userdata('period'):'ga:date';
					
					?>
					<li option_value="ga:date" class="option <?php if($period=='ga:date') echo 'active_option'; ?> LR">Day</li>
					<li option_value="ga:week" class="option <?php if($period=='ga:week') echo 'active_option'; ?> LR">Week</li>
					<li option_value="ga:month" class="option <?php if($period=='ga:month') echo 'active_option'; ?> LR">Month</li>
				</ul></td>
			</tr>
		</table>
	</div>
	</form>
    <br/>
	<div id="ga_chart"> </div>
	
	<div id="other_details">
	
		<div  class="box box_1">
			<h2><a href="<?php echo site_url('tools/admin/analytics/index/referral')?>">More &raquo;</a>Top Referral </h2>
			<div>
				<?php
				$this->ganalytics->set_max_results(10);
				$referrers	= $this->ganalytics->getReferrers();
				if(!empty($referrers)){?>
					<ul class="ga_list">
					<?php foreach($referrers as $key => $val){
						if(strpos($key,'.')){?>
							<li><span><?php echo $val;?></span><a target="_blank" href="http://<?php echo $key;?>"><?php echo $key;?></a> </li>
						<?php }else{?>
								<li><span><?php echo $val;?></span><?php echo $key;?> </li>
						
						<?php }
					}?>
					</ul>
				<?php }
				?>
			</div>
		</div>
		<div  class="box box_1">
			<h2><a href="<?php echo site_url('tools/admin/analytics/index/keywords')?>">More &raquo;</a>Top Keywords </h2>
				<div>
				<?php
				$keywords	= $this->ganalytics->getSearchWords();
				if(!empty($keywords)){?>
					<ul class="ga_list">
					<?php foreach($keywords as $key => $val){
						if(strpos($key,'.')){?>
							<li><span><?php echo $val;?></span><a target="_blank" href="http://<?php echo $key;?>"><?php echo $key;?></a> </li>
						<?php }else{?>
								<li><span><?php echo $val;?></span><?php echo $key;?> </li>
						
						<?php }
					}?>
					</ul>
				<?php }
				?>
			</div>
		</div>
		
			<div  class="box box_1">
			<h2><a href="<?php echo site_url('tools/admin/analytics/index/visited')?>">More &raquo;</a>Visited pages </h2>
			<div>
				<?php
				$VisitedPages	= $this->ganalytics->getVisitedPages();
				if(!empty($VisitedPages)){?>
					<ul class="ga_list">
					<?php foreach($VisitedPages as $key => $val){
						?>
							<li><span><?php echo $val;?></span><a target="_blank" href="<?php echo substr_replace(base_url(),"",-1).$key;?>"><?php echo $key;?></a> </li>
						<?php ?>
								
						
						<?php 
					}?>
					</ul>
				<?php }
				?>
			</div>
		</div>
		
		<div  class="box box_1">
			<h2><a href="<?php echo site_url('tools/admin/analytics/index/locations')?>">More &raquo;</a>Visitors Locations</h2>
			<div>
				<?php
				$cities	= $this->ganalytics->getVisitorsLocations();


				
				if(!empty($cities)){?>
					<ul class="ga_list">
					<?php foreach($cities as $key => $val){
						?>
							<li><span><?php echo $val;?></span><?php echo $key;?> </li>
						<?php ?>
								
						
						<?php 
					}?>
					</ul>
				<?php }
				?>
			</div>
		</div>
		
		<div  class="box box_1" >
			<h2>Visitors type</h2>
			<?php 
				$visitors_types	= $this->ganalytics->getVisitorsType();

				if(!empty($visitors_types)){
					$visits_sum = array_sum($visitors_types);
					$return_types=array(); 
					foreach($visitors_types as $key=>$count){
						$percentage = round(($count/$visits_sum)*100,2);
						$return_types[] = "['$key',$percentage]"; 
					}
					$return_types = implode(',',$return_types);?>
			
			
			<div id="pie_chart" style="height:200px;margin-left:0px"> </div>
			<script>
			$(document).ready(function(){						
			 chart = new Highcharts.Chart({
					chart: {
						renderTo: 'pie_chart',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Visitors types',
						margin:30
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return this.point.name ;
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Visitors types',
						data: [
							<?php echo $return_types;?>
						]
					}]
				});
			});
			
			</script>
				<?php }
			?>
		</div>
		
	
		
		
		<div  class="box box_1" >
			<h2>Browsers</h2>
			<?php 
				$browsers	= $this->ganalytics->getBrowsers();

				if(!empty($browsers)){
					$visits_sum = array_sum($browsers);
					$return_browsers=array(); 
					foreach($browsers as $key=>$count){
						$percentage = round(($count/$visits_sum)*100,2);
						if($percentage>1)$return_browsers[] = "['$key',$percentage]"; 
					}
					$return_browsers = implode(',',$return_browsers);?>
			
			
			<div id="pie_chart_2" style="height:200px;margin-left:0px;"> </div>
			<script>
			$(document).ready(function(){		
			 chart = new Highcharts.Chart({
					chart: {
						renderTo: 'pie_chart_2',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Browsers',
						margin:30
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return  this.point.name ;
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Browsers',
						data: [
							<?php echo $return_browsers;?>
						]
					}]
				});
			});
			
			</script>
				<?php }
			?>
		</div>
		
		

		<div style="clear:both"><!--sdf--></div>
		<div style="height:12px"><!--sdf--></div>
	</div>

</div>
</div>    
</div>

	<script>
		<?php 
		$visitors = $this->ganalytics->getVisitors();
		$page_views = $this->ganalytics->getPageviews();
		
		$keys = array_keys($visitors);
		//print_r($keys);
		
		$period = $this->session->userdata('period')? $this->session->userdata('period'):'ga:date';
		$from = $this->session->userdata('from_date')? $this->session->userdata('from_date'):date('Y-m-d',strtotime('-1 weeks'));
		//echo $from;
		$categories = array();
		
		if($period =='ga:date'){
			foreach($keys as $val){
				$categories[]= '"' .substr($val,4,2).' - '.substr($val,-2).'"';
			}
		}
		
		if($period =='ga:week'){
			foreach($keys as $val){
				$categories[]= '"' .strval(date('Y-m-d',strtotime("+$val weeks $from"))).'"';
			}
		}
		
		if($period =='ga:month'){
			foreach($keys as $val){
				$categories[]='"' .$val.'"';
			}
		}
		
		if(!empty($categories)){ $categories = "[".implode(',',$categories)."]";?>
		var categories_array = <?php echo $categories; ?>;
		
		<?php } ?>
		
		<?php if(is_array($visitors) && !empty($visitors)){ ?>
			var  visitors = [<?php echo implode(',',$visitors) ?>];
		<?php }else{?>
			var  visitors = false;
		<?php }?>
		<?php if(is_array($page_views) && !empty($page_views)){ ?>
			var  page_views = [<?php echo implode(',',$page_views) ?>];
		<?php }else{?>
			var  page_views =false;
		<?php }?>
 
	
	</script>

