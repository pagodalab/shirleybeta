<form id="margin-form" method="post" action="<?php  echo site_url('settings/margin')?>">
    <table style="float:left; margin:20px">
        <tr>
            <td>Exchange Rate : </td>
            <td>
            	<input type="text" name="exchange_rate" id="exchange_rate" value="<?php echo ($exchangerate)?$exchangerate['rate']:''?>">
            	<input type="hidden" name="rate_id" id="rate_id" value="<?php echo ($exchangerate)?$exchangerate['id']:''?>">
            </td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.50)</strong>(%) : </td>
            <td><input type="text" name="margin_50" id="margin_50" value="<?php echo set_value('margin_50',$costing['margin_50'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.50-100)</strong>(%) : </td>
            <td><input type="text" name="margin_50_100" id="margin_50_100" value="<?php echo set_value('margin_50_100',$costing['margin_50_100'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.100-500)</strong>(%) : </td>
            <td><input type="text" name="margin_100_500" id="margin_100_500" value="<?php echo set_value('margin_100_500',$costing['margin_100_500'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.500-1000)</strong>(%) : </td>
            <td><input type="text" name="margin_500_1000" id="margin_500_1000" value="<?php echo set_value('margin_500_1000',$costing['margin_500_1000'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.1000-5000)</strong>(%) : </td>
            <td><input type="text" name="margin_1000_5000" id="margin_1000_5000" value="<?php echo set_value('margin_1000_5000',$costing['margin_1000_5000'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Rs.5000-25000)</strong>(%) : </td>
            <td><input type="text" name="margin_5000_25000" id="margin_5000_25000" value="<?php echo set_value('margin_5000_25000',$costing['margin_5000_25000'])?>"></td>
        </tr>
        <tr>
            <td>Profit Margin <strong>(Above Rs.25000)</strong>(%) : </td>
            <td><input type="text" name="margin_above_25000" id="margin_above_25000" value="<?php echo set_value('margin_above_25000',$costing['margin_above_25000'])?>"></td>
        </tr>
    </table>
    <table style="float:right; margin:20px">  
        <tr>
            <td>Admin Charge (%) : </td>
            <td>
            	<input type="text" name="admin_charge" id="admin_charge" value="<?php echo set_value('admin_charge',$costing['admin_charge'])?>" required="required">
            </td>
        </tr>
        <tr>
            <td>Merchant Charge (%) : </td>
            <td>
            	<input type="text" name="merchant_charge" id="merchant_charge" value="<?php echo set_value('merchant_charge',$costing['merchant_charge'])?>" required="required">
            </td>
        </tr>
        <tr>
            <td>Processing Fee ($) : </td>
            <td>
            	<input type="text" name="processing_fee" id="processing_fee" value="<?php echo set_value('processing_fee',$costing['processing_fee'])?>" required="required">
                <span class="error"><?php echo form_error('processing_fee')?></span>
            </td>
        </tr>
        <tr>
            <td>Export Path : </td>
            <td><input type="text" name="export_path" id="export_path" value="<?php echo set_value('export_path',$costing['export_path'])?>"></td>
        </tr>
        <tr>
            <td>HK Path : </td>
            <td><input type="text" name="hk_path" id="hk_path" value="<?php echo set_value('hk_path',$costing['hk_path'])?>"></td>
        </tr>
        <tr>
            <td>UK Path : </td>
            <td><input type="text" name="uk_path" id="uk_path" value="<?php echo set_value('uk_path',$costing['uk_path'])?>"></td>
        </tr>
    </table>
    <div style="clear:both"></div>
    <div align="center">
    	<button class="tooltip-top btn btn-small" type="submit"><span><i class="icon-ok-sign icon-inverse"></i></span> Update</button>
    </div>
</form>
