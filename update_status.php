<?php

// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['order_status']) && isset($_POST['o_id'])) {
    
	$o_id = $_POST['o_id'];
    $order_status = $_POST['order_status'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    $result = mysql_query("UPDATE orders SET order_status = '$order_status' WHERE o_id = ".$o_id);

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
