<?php

session_start();

//set the session userdata if non-existant
if(!isset($_SESSION['userdata']))
{
	$_SESSION['userdata'] = array();
}
//set newFlashdata if non-existent
if(!isset($_SESSION['newFlashdata']))
{
	$_SESSION['newFlashdata'] = array();
}

//empty out the "oldFlashdata" field
$_SESSION['oldFlashdata'] = array();

//shift newFlashdata over to oldFlashdata
$_SESSION['oldFlashdata'] = $_SESSION['newFlashdata'];
$_SESSION['newFlashdata'] = array();

require_once('CI.php');

//set default datetime zone
date_default_timezone_set('Asia/Kathmandu');

exit;


// <script>
//   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

//   ga('create', 'UA-60889276-1', 'auto');
//   ga('send', 'pageview');

// </script>