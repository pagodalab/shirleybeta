<?php

$response = array();

//$fp=fopen('log.'.date('h_i_s').'.txt','w+');
//fwrite($fp,implode(',', $_REQUEST));
//fclose($fp);

require_once __DIR__ . '/db_connect.php';

$db = new DB_CONNECT();

if (isset($_GET["o_id"])) {
    
	$o_id = $_GET['o_id'];

    $result = mysql_query("SELECT *FROM orders WHERE o_id = '".$o_id."'");

    if (!empty($result)) {
       
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $cust = array();
			$cust["o_id"] = $result["o_id"];
			$cust["c_id"] = $result["c_id"];
            $cust["c_name"] = $result["c_name"];
			$cust["c_address"] = $result["c_address"];
			$cust["c_contact"] = $result["c_contact"];
			$cust["order_status"] = $result["order_status"];
			$cust["lat"] = $result["lat"];
			$cust["lon"] = $result["lon"];
            // success
            $response["success"] = 1;

            // user node
            $response["cust"] = array();

            array_push($response["cust"], $cust);

            // echoing JSON response
            echo json_encode($response);
        } else {
            
            $response["success"] = 0;
            $response["message"] = "No customer found";

            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No customer found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
	exit;
}
?>