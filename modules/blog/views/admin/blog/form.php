
<script src="<?php echo base_url()?>assets/js/jquery.upload.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>"/>

<?php echo form_open('blog/admin/form/'.$id); ?>

<div class="tabbable">
	
	<ul class="nav nav-tabs">
		<li class="active"><a href="#content_tab" data-toggle="tab"><?php echo lang('content');?></a></li>
		<!-- <li><a href="#attributes_tab" data-toggle="tab"><?php echo lang('attributes');?></a></li>
		<li><a href="#seo_tab" data-toggle="tab"><?php echo lang('seo');?></a></li> -->
	</ul>
	
	<div class="tab-content">
		<div class="tab-pane active" id="content_tab">
			<fieldset>
				<label for="title"><?php echo lang('title');?></label>
				<?php
				$data	= array('name'=>'title_en', 'value'=>set_value('title_en', $title_en), 'class'=>'span12');
				echo form_input($data);
				?>
				<label for="title"><?php echo lang('title');?>(in danish)</label>
				<?php
				$data	= array('name'=>'title_da', 'value'=>set_value('title_da', $title_da), 'class'=>'span12');
				echo form_input($data);
				?>
				<td width="34%" ><label>Image:</label></td>
			  	<td width="66%"><label id="upload_image_name"><?php echo ($image_name!='')?$image_name:'';?></label>
              	<input name="image_name" id="image_name" type="text" style="display:none" value="<?php echo ($image_name!='')?$image_name:'';?>">
              	<input type="file" id="upload_image" name="userfile" class="<?php echo ($image_name!='')?'hide':''?>"/>
              	<a href="#" id="change-image" title="Delete" style="<?php echo($image_name!='')?'display:block':'display:none'?>"><img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a></td>
				
				<label for="content"><?php echo lang('content');?></label>
				<?php
				$data	= array('name'=>'content_en', 'class'=>'redactor', 'value'=>set_value('content_en', $content_en));
				echo form_textarea($data);
				?>
				<label for="content"><?php echo lang('content');?>(in danish)</label>
				<?php
				$data	= array('name'=>'content_da', 'class'=>'redactor', 'value'=>set_value('content_da', $content_da));
				echo form_textarea($data);
				?>

				<label>Post Date</label>
				
				<div id="post_date" class="input-append">
                    <input data-format="yyyy-MM-dd" type="text" name="post_date" placeholder="Post Date" value="<?php echo isset($post_date)?$post_date:null;?>"></input>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                  </div>

				<label>Status</label>
				
				<?php
					$options = array(	 '1'	=> 'Enable'
										,'0'	=> 'Disable'
										);
					echo form_dropdown('status', $options, set_value('status',$status), 'class="span4"');
				?>
			</fieldset>
		</div>

		<!-- <div class="tab-pane" id="attributes_tab">
			<fieldset>
				<label for="menu_title"><?php echo lang('menu_title');?></label>
				<?php
				$data	= array('name'=>'menu_title', 'value'=>set_value('menu_title', $menu_title), 'class'=>'span3');
				echo form_input($data);
				?>
			
				<label for="slug"><?php echo lang('slug');?></label>
				<?php
				$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'span3');
				echo form_input($data);
				?>
			
				<label for="sequence"><?php echo lang('parent_id');?></label>
				<?php
				$options	= array();
				$options[0]	= lang('top_level');
				function page_loop($blog, $dash = '', $id=0)
				{
					$options	= array();
					foreach($blog as $page)
					{
						//this is to stop the whole tree of a particular link from showing up while editing it
						if($id != $page->id)
						{
							$options[$page->id]	= $dash.' '.$page->title;
							$options			= $options + page_loop($page->children, $dash.'-', $id);
						}
					}
					return $options;
				}
				$options	= $options + page_loop($blog, '', $id);
				echo form_dropdown('parent_id', $options,  set_value('parent_id', $parent_id));
				?>
			
				<label for="sequence"><?php echo lang('sequence');?></label>
				<?php
				$data	= array('name'=>'sequence', 'value'=>set_value('sequence', $sequence), 'class'=>'span3');
				echo form_input($data);
				?>
			</fieldset>
		</div>
	
		<div class="tab-pane" id="seo_tab">
			<fieldset>
				<label for="code"><?php echo lang('seo_title');?></label>
				<?php
				$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'span12');
				echo form_input($data);
				?>
			
				<label><?php echo lang('meta');?></label>
				<?php
				$data	= array('rows'=>'3', 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'span12');
				echo form_textarea($data);
				?>
				
				<p class="help-block"><?php echo lang('meta_data_description');?></p>
			</fieldset>
		</div> -->
	</div>
</div>

<div class="form-actions">
	<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
    <a href="<?php echo site_url('blog/admin')?>" class="btn btn-danger">Cancel</a>    
</div>	
</form>

<script>
$(function() {
    $('#post_date').datetimepicker({
      dateFormat: 'yy-mm-dd',
      pickTime: false
    });
    uploadReady();


		$('#change-image').click(function(){
			var filename = $('#image_name').val();
			if(confirm('Are you sure to delete the files?'))
			{
				
					$.post('<?php echo site_url('blog/admin/upload_delete')?>',{ filename: filename },function(){
			
					 $('#image_name').val('');
					 $('#upload_image_name').html('');
					 $('#upload_image_name').hide();
					 $('#change-image').hide();
					 $('#upload_image').show();
					});
				
			}


		});	

  });

   function uploadReady()
    {
      uploader=$('#upload_image');
      new AjaxUpload(uploader, {
        action: '<?php  echo site_url('blog/admin/upload_image')?>',
        name: 'userfile',
        responseType: "json",
        onSubmit: function(file, ext){
           if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
            // extension is not allowed 
            $.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
            return false;
          }
          //status.text('Uploading...');
        },
        onComplete: function(file, response){
          if(response.error==null){
            var filename = response.file_name;
            $('#upload_image').hide();
            //$('#no_image').css('display','none');
            $('#image_name').val(filename);
            $('#upload_image_name').html(filename);
            $('#upload_image_name').show();
            $('#change-image').show();
          }
          else
          {
            $.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
          }
        }   
      });   
    }
</script>

<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>