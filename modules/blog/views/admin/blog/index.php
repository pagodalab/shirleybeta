<table id="blog-table" title="Pages" rownumbers="true" toolbar="#toolbar" collapsible="true" fitColumns="true">
    <thead>
    <th field="title" sortable="true" width="50" formatter="formatLink">Title</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<?php echo site_url('blog/admin/form')?>" class="tooltip-top btn btn-small" title="<?=lang('create_page')?>"><span><i class="icon-plus"></i></span> Add BLOG</a>	
    <!-- <a class="tooltip-top btn btn-small" href="<?php echo site_url('page/admin/link_form'); ?>"><i class="icon-plus-sign"></i> <?php echo lang('add_new_link');?></a> -->
    </p>

</div> 
<script language="javascript" type="text/javascript">
	$(function(){
		/*$('#clear').click(function(){
			$('#page-search-form').form('clear');
			$('#blog-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#blog-table').datagrid({
				queryParams:{data:$('#page-search-form').serialize()}
				});
		});		*/

		$('#blog-table').treegrid({
			url:'<?=site_url('blog/admin/treeview_json')?>',
			height:'auto',
			width:'auto',
			idField:'id',treeField:'title',
		});					
	});

	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('blog/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit Page"><span><i class="icon-pencil"></i></span> </a>';
				
		var d = '<a href="#" onclick="removePage()" class="tooltip-top btn btn-small" title="Remove Page"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatLink(value,row)
	{
		return '<a href="<?php echo base_url()?>blog/index/'+row.id+'" target="_blank">'+value+'</a>';
	}

	function removePage()
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#blog-table').treegrid('getSelected');
				//console.log('row',row); return false;
				$.post('<?php  echo site_url('blog/admin/delete_json')?>', {id:row.id}, function(){
					<?php $this->session->set_flashdata('message', lang('message_delete_page'));?>
					$('#blog-table').treegrid('reload');
					
				});

			}
		});	
	}
	
	
</script>