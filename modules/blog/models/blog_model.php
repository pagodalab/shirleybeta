<?php
Class Blog_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('BLOG'=>$this->prefix.'blog');
    }
	/********************************************************************
	BLOG functions
	********************************************************************/
	
	public function getBlogs($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='blog.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['BLOG']. ' blog');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('blog.title_en', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countblog($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['BLOG'].' blog');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
	function get_blogs()
	{
		//$this->db->order_by('sequence', 'ASC');
		//$this->db->where('parent_id', $parent);
		$result = $this->db->get($this->_TABLES['BLOG'])->result();
		
		$return	= array();
		foreach($result as $BLOG)
		{

			// Set a class to active, so we can highlight our current BLOG
			if($this->uri->segment(1) == $BLOG->slug) {
				$BLOG->active = true;
			} else {
				$BLOG->active = false;
			}

			$return[$BLOG->id]				= $BLOG;
			$return[$BLOG->id]->children	= $this->get_blog($BLOG->id);
		}
		
		return $return;
	}

	function get_blog_tiered()
    {
		$this->db->order_by('sequence', 'ASC');
		$this->db->order_by('title', 'ASC');
		$blog = $this->db->get($this->_TABLES['BLOG'])->result();
		
		$results	= array();
		foreach($blog as $BLOG)
		{
			$results[$BLOG->parent_id][$BLOG->id] = $BLOG;
		}
		
		return $results;
	}

	function get_blog($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get($this->_TABLES['BLOG'])->row();
		
		return $result;
	}
	
	function get_slug($id)
	{
		$BLOG = $this->get_blog($id);
		if($BLOG) 
		{
			return $BLOG->slug;
		}
	}
	
	function save($data)
	{
		if($data['id'])
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_TABLES['BLOG'], $data);
			return $data['id'];
		}
		else
		{
			$this->db->insert($this->_TABLES['BLOG'], $data);
			return $this->db->insert_id();
		}
	}
	
	function delete_BLOG($id)
	{
		//delete the BLOG
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['BLOG']);
	
	}
	
	function get_blog_by_slug($slug)
	{
		$this->db->where('slug', $slug);
		$result = $this->db->get($this->_TABLES['BLOG'])->row();
		
		return $result;
	}
}