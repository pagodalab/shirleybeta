<?php 
class blog extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog/blog_model');
		$this->menu_active = 'Blog';
	}
	
	public function index()
	{
		//if there is no blog id provided redirect to the homeblog.
		$this->db->where('post_date <= curdate()');
		$this->db->where('status','1');
		$data['blogs']	= $this->blog_model->getBlogs()->result_array();
		
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;

		$data['page_title']			= 'BLOG';
		
		//$data['meta']				= $data['blog']->meta;
		//$data['seo_title']			= (!empty($data['blog']->seo_title))?$data['blog']->seo_title:$data['blog']->title;
		
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('blog/index', $data);
	}

	public function detail($id)
	{
		$this->load->model('review/review_model');
		$data['comments'] = $this->review_model->getReviews(array('blog_id'=>$id))->result_array();
		
		//if there is no blog id provided redirect to the homeblog.
		
		$data['blog']	= $this->blog_model->getBlogs(array('id'=>$id))->row();
		
		if(!$data['blog'])
		{
			show_404();
		}
		$this->load->model('blog_model');
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;
		$title = 'title_'.$this->culture_code;
		$data['page_title']			= $data['blog']->$title;
		
		//$data['meta']				= $data['blog']->meta;
		//$data['seo_title']			= (!empty($data['blog']->seo_title))?$data['blog']->seo_title:$data['blog']->title;
		
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('blog/detail', $data);
	}

	public function sortDate()
	{
		$date = $this->input->post('date');

		$this->db->where('post_date <= curdate()');
		$this->db->where('status','1');
		$this->db->where('post_date like "%'.$date.'%"');
		$data['blogs'] = $this->blog_model->getBlogs()->result_array();

		$this->load->view('blog/content',$data);
	}
}