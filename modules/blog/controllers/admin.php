<?php
class Admin extends Admin_Controller
{   
    protected $uploadPath = 'uploads/blog';
	protected $uploadthumbpath= 'uploads/blog/medium/';
	
	function __construct()
	{
		parent::__construct();
        
		$this->authenticate->check_access('Admin', true);
		$this->load->model('blog/blog_model');
		$this->lang->load('blog');
	}
		
	function index()
	{
		$data['page_title']	= lang('blog');
		$data['blog']		= $this->blog_model->getBlogs();
		
		$this->view($this->config->item('admin_folder').'/blog/index', $data);
	}
	
	public function info()
	{
	    phpinfo();
	}
	public function json()
	{
		$total = $this->blog_model->countblog();
		paging('id desc');
		$rows = $this->blog_model->getBlog()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	function getTree()
	{
	   $arr = array();
	
	    //$sql="select * from table_menu where parent_id =". $id;
		//$result=$this->db->query($sql);
		$this->db->select('id,title_en');
		$this->db->from('tbl_blog');
		//$this->db->where('parent_id',$id);
		$result=$this->db->get();
		 foreach($result->result_array() as $row)
		 {		 
			 $arr[] = array(
			   "id"=>$row['id'],
			   "title" => $row['title_en'],
			  // "children" => $this->getTree($row['id'])
		 );
	   }
	   return $arr;
	}
	
	public function treeview_json()
	{		
		echo json_encode($this->getTree());
	}
	
	public function delete_json()
	{
    	$id=$this->input->post('id');
		$this->blog_model->delete('BLOG',array('id'=>$id));
		//$this->blog_model->delete('blog',array('parent_id'=>$id));
	}  
	
	
	/********************************************************************
	edit blog
	********************************************************************/
	function form($id = false)
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']				= '';
		$data['title_en']		= '';
		$data['title_da']		= '';
		$data['content_en']		= '';
		$data['content_da']		= '';
		$data['slug']			= '';
		$data['added_date']		= '';
		$data['post_date']	= '';
		$data['modified_date']	= '';
		$data['status']			= '';
		$data['image_name']		= '';
		$data['page_title']	= lang('blog_form');
		$data['blog']		= $this->blog_model->getBlogs();
		
		if($id)
		{
			
			$blog			= $this->blog_model->get_blog($id);

			if(!$blog)
			{
				//blog does not exist
				$this->session->set_flashdata('error', lang('error_blog_not_found'));
				redirect(site_url('blog/admin'));
			}
			
			//set values to db values
			$data['id']					= $blog->id;
			$data['title_en']			= $blog->title_en;
			$data['title_da']			= $blog->title_da;
			$data['content_en']			= $blog->content_en;
			$data['content_da']			= $blog->content_da;
			$data['added_date']			= $blog->added_date;
			$data['post_date']			= $blog->post_date;
			$data['modified_date']		= $blog->modified_date;
			$data['status']				= $blog->status;
			$data['slug']				= $blog->slug;
			$data['image_name']			= $blog->image_name;
		}
		
		$this->form_validation->set_rules('title_en', 'lang:title', 'trim|required');
		//$this->form_validation->set_rules('menu_title', 'lang:menu_title', 'trim');
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim');
		//$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		//$this->form_validation->set_rules('meta', 'lang:meta', 'trim');
		//$this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
		//$this->form_validation->set_rules('parent_id', 'lang:parent_id', 'trim|integer');
		$this->form_validation->set_rules('content', 'lang:content', 'trim');
		
		// Validate the form
		if($this->form_validation->run() == false)
		{
			$this->view($this->config->item('admin_folder').'/blog/form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('title_en');
			}
			
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');
			if($id)
			{
				$slug		= $this->Routes_model->validate_slug($slug, $blog->route_id);
				$route_id	= $blog->route_id;
			}
			else
			{
				$slug			= $this->Routes_model->validate_slug($slug);
				$route['slug']	= $slug;	
				$route_id		= $this->Routes_model->save($route);
			}
			
			
			$save = array();
			$save['id']			= $id;

			if($id)
			{
				$save['modified_date']	= date('Y-m-d');
			}
			else
			{
				$save['added_date']		= date('Y-m-d');
			}	
			$save['title_en']	= $this->input->post('title_en');
			$save['title_da']	= $this->input->post('title_da');
			$save['status']		= $this->input->post('status');
			$save['post_date']	= $this->input->post('post_date');	
			//$save['menu_title']	= $this->input->post('menu_title'); 
			//$save['sequence']	= $this->input->post('sequence');
			$save['content_en']	= $this->input->post('content_en');
			$save['content_da']	= $this->input->post('content_da');
			//$save['seo_title']	= $this->input->post('seo_title');
			//$save['meta']		= $this->input->post('meta');
			$save['route_id']	= $route_id;
			$save['slug']		= $slug;
			$save['image_name']	= $this->input->post('image_name');
			
			//set the menu title to the blog title if if is empty
			// if ($save['menu_title'] == '')
			// {
			// 	$save['menu_title']	= $this->input->post('title');
			// }
			
			//save the blog
			$blog_id	= $this->blog_model->save($save);
			
			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'blog/detail/'.$blog_id;
			
			$this->Routes_model->save($route);
			
			$this->session->set_flashdata('message', lang('message_saved_blog'));
			
			//go back to the blog list
			redirect(site_url('blog/admin'));
		}
	}
	
	function link_form($id = false)
	{
	
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']			= '';
		$data['title_en']	= '';
		$data['url']		= '';
		$data['new_window']	= false;
		$data['sequence']	= 0;
		$data['parent_id']	= 0;

		
		$data['page_title']	= lang('link_form');
		$data['blog']		= $this->blog_model->get_blog();
		if($id)
		{
			$blog			= $this->blog_model->get_blog($id);

			if(!$blog)
			{
				//blog does not exist
				$this->session->set_flashdata('error', lang('error_link_not_found'));
				redirect(site_url('blog/admin'));
			}
			
			
			//set values to db values
			$data['id']			= $blog->id;
			$data['parent_id']	= $blog->parent_id;
			$data['title_en']	= $blog->title_en;
			$data['url']		= $blog->url;
			$data['new_window']	= (bool)$blog->new_window;
			$data['sequence']	= $blog->sequence;
		}
		
		$this->form_validation->set_rules('title_en', 'lang:title', 'trim|required');
		$this->form_validation->set_rules('url', 'lang:url', 'trim|required');
		$this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
		$this->form_validation->set_rules('new_window', 'lang:new_window', 'trim|integer');
		$this->form_validation->set_rules('parent_id', 'lang:parent_id', 'trim|integer');
		
		// Validate the form
		if($this->form_validation->run() == false)
		{
			$this->view($this->config->item('admin_folder').'/blog/link_form', $data);
		}
		else
		{	
			$save = array();
			$save['id']			= $id;
			$save['parent_id']	= $this->input->post('parent_id');
			$save['title_en']	= $this->input->post('title_en');
			$save['menu_title']	= $this->input->post('title_en'); 
			$save['url']		= $this->input->post('url');
			$save['sequence']	= $this->input->post('sequence');
			$save['new_window']	= $this->input->post('new_window');
			
			//save the blog
			$this->blog_model->save($save);
			
			$this->session->set_flashdata('message', lang('message_saved_link'));
			
			//go back to the blog list
			redirect(site_url('blog/admin'));
		}
	}
	
	/********************************************************************
	delete blog
	********************************************************************/
	function delete($id)
	{
		
		$blog	= $this->blog_model->get_blog($id);
		
		if($blog)
		{
			$this->load->model('Routes_model');
			$blog	= $this->blog_model->get_blog($id);
			@unlink($this->uploadPath . '/' . $blog->image_name);
			$this->Routes_model->delete($blog->route_id);
			$this->blog_model->delete_blog($id);
			$this->session->set_flashdata('message', lang('message_deleted_blog'));
		}
		else
		{
			$this->session->set_flashdata('error', lang('error_blog_not_found'));
		}
		
		redirect(site_url('blog/admin'));
	}
	
	 function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '1024000';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
 		 //  $config['image_library'] = 'gd2';
		  // $config['source_image'] = $data['full_path'];
    //       $config['new_image']    = $this->uploadthumbpath;
		  // //$config['create_thumb'] = TRUE;
		  // $config['maintain_ratio'] = TRUE;
		  // $config['height'] =150;
		  // $config['width'] = 150;

		  // $this->load->library('image_lib', $config);
		  // $this->image_lib->resize();
		  echo json_encode($data);
	    }
	}

	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
	} 	
}	