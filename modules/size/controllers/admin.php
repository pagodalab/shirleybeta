<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('size/size_model');
        $this->lang->load('size/size');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'sizes';
		$data['page_title'] = 'Sizes';
		$this->view($this->config->item('admin_folder').'/sizes/index', $data);	
	}

	public function json()
	{
		//$this->_get_search_param();	
		$total=$this->size_model->count();
		paging('sequence asc');
		//$this->_get_search_param();	
		$rows=$this->size_model->getSizes()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	// public function _get_search_param()
	// {
	// 	// Search Param Goes Here
	// 	parse_str($this->input->post('data'),$params);
	// 	if(!empty($params['search']))
	// 	{
	// 		($params['search']['sizes_name']!='')?$this->db->like('sizes_name',$params['search']['sizes_name']):'';
	// 		($params['search']['address']!='')?$this->db->like('address',$params['search']['address']):'';
	// 		($params['search']['city']!='')?$this->db->like('city',$params['search']['city']):'';
	// 		($params['search']['contact_person']!='')?$this->db->like('contact_person',$params['search']['contact_person']):'';
	// 		($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
	// 		($params['search']['mobile']!='')?$this->db->like('mobile',$params['search']['mobile']):'';
	// 		($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
	// 		(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

	// 	}  
	// }
    
	public function combo_json()
    {
		$rows=$this->size_model->getSizes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->size_model->delete('SIZES',array('size_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('size_id'))
        {
			//$data['added_date'] =date('Y-m-d H:i:s');		
            $success=$this->size_model->insert('SIZES',$data);
        }
        else
        {
			//$data['modified_date'] =date('Y-m-d H:i:s');			
            $success=$this->size_model->update('SIZES',$data,array('size_id'=>$data['size_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
       		$data['size_id'] 		= $this->input->post('size_id');
		$data['size_title_en'] 	= $this->input->post('size_title_en');
		$data['size_title_da']	= $this->input->post('size_title_da');
		$data['size_code'] 		= $this->input->post('size_code');
		$data['type']			= $this->input->post('type');
		//$data['sequence']		= $this->input->post('sequence');

        return $data;
   }
   
    function sequence()
    {
    	$row = $this->input->post('row');

   		if(!$this->input->post('action')) //move up sequence
   		{
   			if($row['sequence'] > 0)
   			{
   				$row['sequence']--;
   				$second_data = $this->db->get_where('tbl_sizes',array('sequence'=>$row['sequence']))->row_array();
   				$success = $this->db->update('tbl_sizes',array('sequence'=>$row['sequence']),array('size_id'=>$row['size_id']));
   				
   				$second_data['sequence']++;
   				$success = $this->db->update('tbl_sizes',array('sequence'=>$second_data['sequence']),array('size_id'=>$second_data['size_id']));
   			}
   		}
   		else //move down sequence
   		{
   			$count = $this->db->get('tbl_sizes')->num_rows();
   			if($row['sequence'] < $count - 1)
   			{

   				$row['sequence']++;
   				$second_data = $this->db->get_where('tbl_sizes',array('sequence'=>$row['sequence']))->row_array();
   				$success = $this->db->update('tbl_sizes',array('sequence'=>$row['sequence']),array('size_id'=>$row['size_id']));
   				$second_data['sequence']--;
   				$success = $this->db->update('tbl_sizes',array('sequence'=>$second_data['sequence']),array('size_id'=>$second_data['size_id']));
   			}
   			
   		}
   	}
   	
   	function reset_sequence()
   	{
   		//exit();
   		$sn = 0;
   		$rows = $this->db->get('tbl_sizes')->result_array();

   		foreach ($rows as $value) {
   			$this->db->update('tbl_sizes',array('sequence'=>$sn++),array('size_id'=>$value['size_id']));
   		}
   		
   		redirect('size/admin');

   	}
   
   	
	    
}