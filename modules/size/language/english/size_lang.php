<?php


$lang['size_id']				= 'Size Id';
$lang['size_name']				= 'Size Title';
$lang['size_name_da']			= 'Size Title (in danish)';
$lang['size_code']				= 'Size Code';
$lang['size_sequence']				= 'Size Sequence';
$lang['address'] = 'Address';
$lang['city'] = 'City';
$lang['contact_person'] = 'Contact Person';
$lang['phone'] = 'Phone';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['added_date'] = 'Added Date';
$lang['status'] = 'Status';

$lang['create_size']='Create size';
$lang['edit_size']='Edit size';
$lang['delete_size']='Delete size';
$lang['size_search']='size Search';

$lang['sizes']='Sizes';
$lang['delete_confirm']='Are you sure you want to delete?';
$lang['type']='type';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

