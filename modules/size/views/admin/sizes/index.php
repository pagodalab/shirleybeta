<table id="size-table" data-options="pagination:true,title:'<?php  echo lang('sizes')?>',pageSize:'50', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="size_id" sortable="true" width="30"><?=lang('size_id')?></th><?php */?>
	<th field="size_title_en" sortable="true" width="50"><?=lang('size_name')?></th>
    <th field="size_code" sortable="true" width="20"><?=lang('size_code')?></th>
    	<!-- <th field="sequence" sortable="true" width="50"><?=lang('size_sequence')?></th> -->
    	<th field="sequence_action" formatter="sequence_action" width="20"><?php echo "Actions"?></th>
	<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Add Brand"><span><i class="icon-plus icon-inverse"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Brand"><span><i class="icon-trash icon-inverse"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit size form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-size" method="post" >
    <table>
		<tr>
              <td width="34%" ><label><?=lang('size_name')?>:</label></td>
			  <td width="66%"><input type="text" name="size_title_en" id="size_title_en" class="easyui-validatebox" required="true"></td>
       </tr>
       <tr>
              <td width="34%" ><label><?=lang('size_name_da')?>:</label></td>
			  <td width="66%"><input type="text" name="size_title_da" id="size_title_da" class="easyui-validatebox" required="true"></td>
       </tr>
       <tr>
              <td width="34%" ><label><?=lang('size_code')?>:</label></td>
			  <td width="66%"><input type="text" name="size_code" id="size_code" class="easyui-validatebox" required="true"></td>
       </tr>
        	<tr>
		<td width="34%" ><label><?=lang('size_sequence')?>:</label></td>
			<td width="66%"><input type="text" name="sequence" id="sequence" class="easyui-validatebox" required="true"></td>
		</tr>
       <tr>
       		 <td width="34%" ><label><?=lang('type')?>:</label></td>
       		  <td width="66%">
       		  	<select name="type" id="type" class="easyui-validatebox" required="true">
       		  		<option value="26">Baby</option>
       		  		<option value="23">Child</option>
       		  		<option value="24">Others</option>
       		  	</select>
       		  </td>
       </tr>
       <!-- <tr>
              <td width="34%" ><label>Sequence:</label></td>
			  <td width="66%"><input type="text" name="sequence" id="sequence" class="easyui-validatebox" required="true"></td>
       </tr> -->
		       <input type="hidden" name="size_id" id="size_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->

<script language="javascript" type="text/javascript">
	$(function(){
		// $('#clear').click(function(){
		// 	$('#size-search-form').form('clear');
		// 	$('#size-table').datagrid({
		// 		queryParams:null
		// 		});

		// });

		// $('#search').click(function(){
		// 	$('#size-table').datagrid({
		// 		queryParams:{data:$('#size-search-form').serialize()}
		// 		});
		// });		
		$('#size-table').datagrid({
			url:'<?php  echo site_url('size/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_size')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removesize('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_size')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;		
	}
	
	function sequence_action(value, row, index)
	{
		var up 		= '<button onclick="sequence('+index+',0)" class="btn btn-default btn-xs btn-flat">˄</button>';
		var down 	= '<button onclick="sequence('+index+',1)" class="btn btn-default btn-xs btn-flat">˅</button>';
		return up+down;
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-size').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_size')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#size-table').datagrid('getRows')[index];
		if (row){
			$('#form-size').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_size')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removesize(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#size-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('size/admin/delete_json')?>', {id:[row.size_id]}, function(){
					$('#size-table').datagrid('deleteRow', index);
					$('#size-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#size-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].size_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('size/admin/delete_json')?>',{id:selected},function(data){
						$('#size-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-size').form('submit',{
			url: '<?php  echo site_url('size/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-size').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#size-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	function sequence(index,action)
	{
		var row = $('#size-table').datagrid('getRows')[index];
		
		$.post('<?php echo site_url('size/admin/sequence'); ?>',{action:action,row:row},function(data){
			$('#size-table').datagrid('reload');  
		});
		

	}
	
	
</script>