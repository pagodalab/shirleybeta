<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('occasion_category_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="occasion_category-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('occasion_id')?></label>:</td>
<td><input type="text" name="search[occasion_id]" id="search_occasion_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('category_id')?></label>:</td>
<td><input type="text" name="search[category_id]" id="search_category_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="occasion_category-table" data-options="pagination:true,title:'<?php  echo lang('occasion_category')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'occasion_id',sortable:true" width="50"><?php echo lang('occasion_id')?></th>
<th data-options="field:'category_id',sortable:true" width="50"><?php echo lang('category_id')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_occasion_category')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_occasion_category')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit occasion_category form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-occasion_category" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('occasion_id')?>:</label></td>
					  <td width="66%"><input name="occasion_id" id="occasion_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('category_id')?>:</label></td>
					  <td width="66%"><input name="category_id" id="category_id" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="" id=""/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#occasion_category-search-form').form('clear');
			$('#occasion_category-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#occasion_category-table').datagrid({
				queryParams:{data:$('#occasion_category-search-form').serialize()}
				});
		});		
		$('#occasion_category-table').datagrid({
			url:'<?php  echo site_url('occasion_category/admin/occasion_category/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_occasion_category')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeoccasion_category('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_occasion_category')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-occasion_category').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_occasion_category')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#occasion_category-table').datagrid('getRows')[index];
		if (row){
			$('#form-occasion_category').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_occasion_category')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeoccasion_category(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#occasion_category-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('occasion_category/admin/occasion_category/delete_json')?>', {id:[row.]}, function(){
					$('#occasion_category-table').datagrid('deleteRow', index);
					$('#occasion_category-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#occasion_category-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('occasion_category/admin/occasion_category/delete_json')?>',{id:selected},function(data){
						$('#occasion_category-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-occasion_category').form('submit',{
			url: '<?php  echo site_url('occasion_category/admin/occasion_category/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-occasion_category').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#occasion_category-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>