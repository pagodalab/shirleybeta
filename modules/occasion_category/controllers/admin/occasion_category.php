<?php

class Occasion_category extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('occasion_category','occasion_category_model');
        $this->lang->module_load('occasion_category','occasion_category');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'occasion_category';
		$data['page'] = $this->config->item('template_admin') . "occasion_category/index";
		$data['module'] = 'occasion_category';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->occasion_category_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->occasion_category_model->getOccasionCategories()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['occasion_id']!='')?$this->db->where('occasion_id',$params['search']['occasion_id']):'';
($params['search']['category_id']!='')?$this->db->where('category_id',$params['search']['category_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->occasion_category_model->getOccasionCategories()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->occasion_category_model->delete('OCCASION_CATEGORY',array(''=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post(''))
        {
            $success=$this->occasion_category_model->insert('OCCASION_CATEGORY',$data);
        }
        else
        {
            $success=$this->occasion_category_model->update('OCCASION_CATEGORY',$data,array(''=>$data['']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['occasion_id'] = $this->input->post('occasion_id');
$data['category_id'] = $this->input->post('category_id');

        return $data;
   }
   
   	
	    
}