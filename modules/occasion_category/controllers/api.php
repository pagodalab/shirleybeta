<?php

class API extends API_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('occasion_category','occasion_category_model');
    }
    
    function index_get()
    {
        $data = array('returned: '. $this->get('id'));
        $this->response($data);
    }
     
    function index_post()
    {       
        $data = array('returned: '. $this->post('id'));
        $this->response($data);
    }
 
    function index_put()
    {       
        $data = array('returned: '. $this->put('id'));
        $this->response($data);
    }
 
    function index_delete()
    {
        $data = array('returned: '. $this->delete('id'));
        $this->response($data);
    }
    
}	

