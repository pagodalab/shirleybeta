<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->authenticate->check_access('Admin', true);
		
		$this->load->model('occasion/occasion_model');
		$this->load->model('occasion_category/occasion_category_model');
		$this->load->model('occasion/occasion_product_model');
		$this->load->model('category/category_model');
		$this->load->helper('form');
		$this->lang->load('occasion');
	}

	public function index()
	{
		// Display Page

		$data['page_title'] = 'Occasions';
		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'occasion';
		$this->view($this->config->item('template_admin') . "admin/occasion/index",$data);		
	}

	public function json()
	{
		
		$this->_get_search_param();	
		//$this->occasion_model->joins = array('BRANDS','CATEGORY_PRODUCT');
		$total=$this->occasion_model->countOccasions();
		paging('occasion_id desc');
		$this->_get_search_param();
		$rows=$this->occasion_model->getOccasions()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
			($params['search']['enabled']!='')?$this->db->like('enabled',$params['search']['enabled']):'';
			(!empty($params['search']['is_perishable']))?$this->db->where('is_perishable = 1'):'';
			(!empty($params['search']['is_flexible']))?$this->db->where('is_flexible = 1'):'';
			($params['search']['category']!='')?$this->db->where('category_id',$params['search']['category']):'';
			($params['search']['brand']!='')?$this->db->where('brand',$params['search']['brand']):'';
		
		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
	}
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}
	
	public function save()
	{
		$data = $this->_get_posted_data();
		//echo "<pre>"; print_r($_POST); exit;
		$category_id = $this->input->post('category_id');
		
		if($data['occasion_id'])
		{
			
			$success = $this->occasion_model->update('OCCASION',$data,array('occasion_id'=>$data['occasion_id']));
		}
		else
		{
			$success = $this->occasion_model->insert('OCCASION',$data);
			$new_occasion_id = $this->db->insert_id();
			
		}
		
		$occasion_id = (!($data['occasion_id']))?$new_occasion_id:$data['occasion_id'];
		if($category_id)
		{
			if(is_array($category_id))
			{
				if($data['occasion_id'])
				{
					$this->occasion_category_model->delete('OCCASION_CATEGORY',array('occasion_id'=>$occasion_id));
				}
				foreach($category_id as $category)
				{
					$occ_cat['occasion_id'] = $occasion_id;
					$occ_cat['category_id'] = $category;
					
					$this->occasion_category_model->insert('OCCASION_CATEGORY',$occ_cat);
				}
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	
	private function _get_posted_data()
	{
		$data = array();
		$data['occasion_id'] = $this->input->post('occasion_id');
		$data['occasion_name'] = $this->input->post('occasion_name');
		$data['start_date'] = $this->input->post('start_date');
		$data['end_date'] = $this->input->post('end_date');
		$data['sort_order'] = $this->input->post('sort_order');
		$data['status'] = $this->input->post('status');
		
		return $data;
	}
	
	public function delete_json()
	{
		//echo "<pre>"; print_r($_POST); exit;
    		$id=$this->input->post('id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->occasion_category_model->delete('OCCASION_CATEGORY',array('occasion_id'=>$row));
				$success = $this->occasion_model->delete('OCCASION',array('occasion_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
		
	}  
	
	public function combo_json()
	{
		$rows=$this->occasion_model->getOccasion()->result_array();
		echo json_encode($rows);    	
	} 
	
	public function getCategories()
	{
		$data['occasion_id'] = ($this->input->post('id'))?$this->input->post('id'):'';
		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'occasion';
		$this->load->view($this->config->item('template_admin') . "admin/occasion/categories",$data);	
	}

	public function products($id)
	{
		$data['page_title'] = 'Occasion Products';
		$this->db->order_by('sort_order asc');
		$this->occasion_product_model->joins = array('PRODUCT');
		$data['products'] = $this->occasion_product_model->getOccasionProducts(array('occasion_id'=>$id,'status'=>1))->result_array();
		$data['occasion_id'] = $id;
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'occasion';
		$this->view($this->config->item('template_admin') . "admin/occasion/products",$data);		
	}

	public function organize_products()
	{
		//echo "<pre>"; print_r($_POST);
		$products = $this->input->post('product');
		$sequence = 1;
		foreach($products as $product)
		{
			//$this->db->where('product_id',$product);
			$this->occasion_product_model->update('OCCASION_PRODUCTS', array('sort_order'=>$sequence),array('product_id'=>$product));
			$sequence++;
		} 
		
	}
	
	public function remove_product()
	{
		//echo "<pre>"; print_r($_POST['products']); exit;
    		$id=$this->input->post('id');
		$occasion_id = $this->input->post('occ_id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$success = $this->occasion_product_model->delete('OCCASION_PRODUCTS',array('occasion_id'=>$occasion_id,'product_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
		
	}  
	
}
