<?php
class Occasion_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('OCCASION'=>$this->prefix.'occasions','OCCASION_PRODUCT'=>$this->prefix.'occasion_products','PRODUCT'=>$this->prefix.'products','CATEGORY_PRODUCT'=>$this->prefix.'category_products','CATEGORY'=>$this->prefix.'categories');
		$this->_JOINS=array('OCCASION_PRODUCT'=>array('join_type'=>'INNER','join_field'=>'occasion_products.occasion_id=occasions.occasion_id',
                                           'select'=>"occasion_products.product_id",'alias'=>'occasion_products'),
								   
						'CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=share_list.customer_id',
                                           'select'=>"concat(customers.firstname,' ',customers.lastname)as customer",'alias'=>'customers'),			
						'PRODUCT'=>array('join_type'=>'LEFT','join_field'=>'products.id=occasion_products.product_id',
                                           'select'=>'products.*','alias'=>'products'),
						'CATEGORY_PRODUCT'=>array('join_type'=>'INNER','join_field'=>'category_products.product_id=products.id',
                                           'select'=>"category_products.sequence,category_products.category_id",'alias'=>'category_products'),
								   'CATEGORY'=>array('join_type'=>'INNER','join_field'=>'category_products.category_id=categories.id','select'=>"categories.slug as category",'alias'=>'categories'),
                            );        
    }
	
    public function getOccasions($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='occasions.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['OCCASION']. ' occasions');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):'';//$this->db->order_by('products.name', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countOccasions($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['OCCASION'].' occasions');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['OCCASION'].' occasions');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	function save($data)
	{
		/*if($data['customer_id'])
		{
			$result = $this->getShareLists(array('customer_id'=>$data['customer_id']))->row_array();
			if(count($result) > 0)
			{
				$this->update('OCCASION',array('customer_id'=>$data['customer_id']),$data);
				return $result['share_list_id'];
			}
			else
			{*/
				$this->insert('OCCASION',$data);
				return $this->db->insert_id();
			/*}
		}*/
		
	}
	
	function deleteOccasion($id)
	{
		// delete product 
		$this->db->where('occasion_id', $id);
		$this->db->delete($this->_TABLES['OCCASION']);

	}
	
	public function getOccasionById($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='occasions.occasion_id';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['OCCASION']. ' occasions');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		//(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.name', 'ASC');

		/*if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}*/
		return $this->db->get();
    }

}