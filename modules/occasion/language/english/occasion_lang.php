<?php


$lang['occasion_id'] = 'Brand Id';
$lang['occasion_name'] = 'Title';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';


$lang['create_occasion']='Add Occasion';
$lang['edit_occasion']='Edit Occasion';
$lang['delete_occasion']='Delete Occasion';
$lang['occasion_search']='Occasion Search';

$lang['occasion']='Occasion';
$lang['success_message'] = "Success";
$lang['failure_message'] = "Error";
$lang['success'] = "Success";
//$lang['error'] = "Error";
$lang['edit_selection_error'] = "Please select at least one to remove.";

//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

