<?php /*?><div id="search-panel" class="easyui-panel" data-options="title:'Product Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="product-search-form">
<table width="100%">
<tr><td><label><?=lang('product_name')?></label>:</td>
<td><input name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<td><label>Category</label>:</td>
<td><input name="search[category]" id="search_category"/></td>
</tr>
<tr>
<td><label>Brand</label>:</td>
<td><input name="search[occasion]" id="search_occasion"  class="easyui-combobox"/></td>
<td><label>Added Date</label>:</td>
<td><input type="text" name="date[added_date][from]" id="search_added_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[added_date][to]" id="search_added_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><input type="checkbox" name="search[is_flexible]" id="search_is_flexible"> Is Flexible</label>
</td>
<td><label><input type="checkbox" name="search[is_perishable]" id="search_is_perishable"> Is Perishable</label>
<td><label>Status</label>:</td>
<td>
	<select type="text" name="search[enabled]" id="search_enabled"  class="easyui-combobox"/>
		<option value=""></option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
</td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div><?php */?>
<br/>
<table id="occasion-table" data-options="pagination:true,title:'Occasions',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="occasion_name" sortable="true" width="80">Title</th>
    <th field="start_date" sortable="true" width="100">Start Date</th>
    <th field="end_date" sortable="true" width="100">End Date</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="javascript:void(0)" onclick="create()" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Occasion</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <?php /*?><select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a><?php */?>
    </p>

</div> 

<!--for create and edit occasion form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
         <form id="form-occasion" method="post" >
          <div id="tt" class="easyui-tabs" style="width:545px;height:auto;">
               <div title="Occasion Info." style="padding:20px;">
               	<table>
                         <tr>
                              <td width="34%" ><label><?=lang('occasion_name')?>:</label></td>
                              <td width="66%"><input type="text" name="occasion_name" id="occasion_name" class="easyui-validatebox" required="true"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label><?=lang('start_date')?>:</label></td>
                              <td width="66%"><input type="text" name="start_date" id="start_date" class="easyui-datebox" required="true"></td>
                         </tr><tr>
                              <td width="34%" ><label><?=lang('end_date')?>:</label></td>
                              <td width="66%"><input type="text" name="end_date" id="end_date" class="easyui-datebox" required="true"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label>Sort Order:</label></td>
                              <td width="66%"><input type="text" name="sort_order" id="sort_order" class="easyui-numberbox" required="true"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label><?=lang('status')?>:</label></td>
                              <td width="66%">
                              	<label><input type="radio" name="status" id="status1" required="true" value="1"> Active </label>
                              	<label><input type="radio" name="status" id="status0" required="true" value="0"> InActive </label>
                              </td>
                         </tr>
                              <input type="hidden" name="occasion_id" id="occasion_id"/>
                    </table>
               </div>
               <div title="Categories" style="overflow:auto;padding:20px;">
               	<div id="categories"></div>
               </div>
          </div>
   		</form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->     

<script language="javascript" type="text/javascript">
$(function(){
		$('#search_occasion').combobox({url:'<?php echo site_url('occasion/admin/combo_json')?>',valueField:'occasion_id',textField:'occasion_name'});
		
			
		$('#clear').click(function(){
			$('#product-search-form').form('clear');
			$('#occasion-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#occasion-table').datagrid({
				queryParams:{data:$('#product-search-form').serialize()}
				});
		});		
		$('#occasion-table').datagrid({
			url:'<?=site_url('occasion/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
			$('#form-product').form('load',row);
			$('#dlg').window('open').window('setTitle','<?=lang('edit_product')?>');
			}
		});
	
	});
	
	function create(){
		//Create code here
		$('#categories').html('');
		$('#form-occasion').form('clear');
		$.post('<?php echo site_url('occasion/admin/getCategories')?>',{},function(data){
					$('#categories').html(data);
				});
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_occasion')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		$('#categories').html('');
		var row = $('#occasion-table').datagrid('getRows')[index];
		if (row){
			$('#form-occasion').form('load',row);
			$.post('<?php echo site_url('occasion/admin/getCategories')?>',{id:row.occasion_id},function(data){
					$('#categories').html(data);
				});
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_occasion')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_occasion')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var f = '<a href="<?php echo site_url('occasion/admin/products/')?>/'+row.occasion_id+'" class="tooltip-top btn btn-small" title="Sort Products" target="_blank"><span><i class="icon-th-large icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removeOccasion('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_occasion')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+f+d;
	}
	
	function save()
	{
		$('#form-occasion').form('submit',{
			url: '<?php  echo site_url('occasion/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-occasion').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#occasion-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	function upload(index)
	{
		var row = $('#occasion-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('product_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {product_id: row.product_id};
		}
		$('#form-upload').form('clear');
	}
		
	function removeOccasion(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#occasion-table').datagrid('getRows')[index];
				//console.log(row); return false;
				$.post('<?=site_url('occasion/admin/delete_json')?>', {id:[row.occasion_id]}, function(data){
					$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
					//$('#occasion-table').datagrid('deleteRow', index);
					$('#occasion-table').datagrid('reload');
				},'json');

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#occasion-table').datagrid('getSelections');
		//console.log('selected ',rows.length); return false;
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].occasion_id);
			}
			//console.log('selected ',selected); return false;
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?=site_url('occasion/admin/delete_json')?>',{id:selected},function(data){
						$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
						$('#occasion-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function checkSubs(id)
	{
		var status = false;
		var disabled = true;
		
		if($('#category'+id).prop('checked'))
		{
			status = true;
			disabled = false;
			//console.log(status); 
			//return false;
			//$('.subcategory'+id).prop('checked',status);
		}
		$('.subcategory'+id).prop('checked',status);
		$('.suborder'+id).prop('disabled',disabled);
		$('.order'+id).prop('disabled',disabled);
	}
</script>
