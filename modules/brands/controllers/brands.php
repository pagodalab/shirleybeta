<?php 

class Brands extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('brands/brand_model');
	}
	
	public function index()
	{	
		$data['vendors'] = $this->brand_model->getBrands()->result();
		$this->view('vendor/index', $data);
	}
}
?>