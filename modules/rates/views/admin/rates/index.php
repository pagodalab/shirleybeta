<div style="float:right">
	<button class="btn btn-default" onclick="add()"><span><i class="icon-plus-sign"></i></span> Add Rate</button>
</div>
<div class="clearfix"></div>
<?php if(count($rates) > 0){?>
<table class="table table-striped">
	<tr>
     	<th>Location</th>
          <th>Range</th>
          <th>Rate(DKK)</th>
          <th>Actions</th>
     </tr>
     <?php foreach($rates as $rate){?>
     	<tr>
          	<td><?php echo isset($rate['name'])?$rate['name']:$rate['location_id'];?></td>
               <td><?php echo $rate['range_max']?></td>
               <td><?php echo $rate['rate']?></td>
               <td>
               	<button class="btn btn-default" onclick="edit(<?php echo $rate['rate_id']?>)"><span><i class="icon-pencil"></i></span> Edit</button>
                    <button class="btn btn-danger" onclick="deleteRate(<?php echo $rate['rate_id']?>)"><span><i class="icon-trash icon-white"></i></span> Delete</button>
               </td>
          </tr>
     <?php }?>
</table>
<?php }else{?>
	<p align="center"><h3>No Rates Available!</h3></p>
<?php } ?>

<div id="rate_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-header">
     	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
     	<h3 id="myModalLabel"></h3>
     </div>
     <div class="modal-body">
     	<form action="" method="post" id="rates_form">
          	<table>
               	<tr>
                    	<td>Location : </td>
                         <td>
                         <select name="location_id" id="location_id">
                        
                              <?php foreach($countries as $country):?>
                                   <?php //if($country->id == 57 || $country->id == 203 || $country->id == 160):?>
                                   <option value="<?php echo $country->id?>"><?php echo $country->name?></option>
                                   <?php //endif;?> 
                              <?php endforeach;?>     
                                   <option value="others">Rest of the World</option>
                         </select> 
                         
                         
                         </td>
                    </tr>
                    <tr>
                         <td>Rate Rages: </td>
                         <td><input type="text" name="range_max" id="range_max" placeholder="Empty for defaults"> <span></span> </td>
                    </tr>
                    <tr>
                         <td>Rate (DKK): </td>
                         <td><input type="text" name="rate" id="rate"></td>
                    </tr>
                     <input type="hidden" name="rate_id" id="rate_id">
               </table>
          </form>
     </div>
     <div class="modal-footer">
          <button class="btn btn-primary" onclick="save()">Save</button>
          <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
     </div>
</div>

<script>
function add()
{
	$('#myModalLabel').text('Add Rate');
	$('#location_name').val('');
	$('#rate_id').val('');
     $('#rate').val('');
	$('#range_max').val('');
	$('#rate_modal').modal();
}

function edit(id)
{
	$.post('<?php echo site_url('rates/admin/getRateById')?>',{rate_id:id},function(data)
	{
		$('#myModalLabel').text('Edit Rate');
		$('#location_name').val(data.row.location_name);
		$('#rate').val(data.row.rate);
          $('#rate_id').val(data.row.rate_id);
		$('#location_id').val(data.row.location_id);

          $('#range_max').val(data.row.range_max);

		$('#rate_modal').modal();
	},'json');
}

function save()
{
	$.ajax({
		type:'post',
		url:'<?php echo site_url('rates/admin/save')?>',
		data: $('#rates_form').serialize(),
		dataType:'json',
		success:function(data)
		{
			if(data.success)
			{
				window.location.href = '<?php echo site_url('rates/admin')?>';
			}
		}
	});
	return false;
}

function deleteRate(rate_id)
{
    $.post('<?php echo site_url('rates/admin/delete_json')?>',{rate_id:rate_id},function(data)
     {

               if(data.success)
               {
                    window.location.href = '<?php echo site_url('rates/admin')?>';
               }
     },'json');
     return false;
}

</script>