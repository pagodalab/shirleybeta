<?php

class Admin extends Admin_Controller {	
	
	function __construct()
	{		
		parent::__construct();
		
		$this->authenticate->check_access('Admin', true);
		$this->load->model('rates/location_rate_model');
		$this->lang->load('rates');
		$this->load->model('location/location_model');
	}
	
	function index()
	{
		$data['countries'] = $this->location_model->get_countries();

		$data['page_title']	= lang('rates');

		$this->location_rate_model->joins = array('COUNTRY');
		$this->db->order_by('location_id, range_max desc');
		$data['rates']	= $this->location_rate_model->getLocationRates()->result_array();

		$page = $this->config->item('template_admin') .'admin/rates/index';
		$this->view($page, $data);
	}
	
	public function getRateById()
	{
		$rate_id = $this->input->post('rate_id');
		$row = $this->location_rate_model->get_location_rate_by_id($rate_id);
		echo json_encode(array('row'=>$row));
	}
	
	public function save()
	{
		$data['rate_id'] = $this->input->post('rate_id');
		$data['location_id'] = $this->input->post('location_id');
		$data['rate'] = $this->input->post('rate');
		$data['range_max'] = $this->input->post('range_max');

		$data = array_filter($data, function($value) {
			return ($value !== null && $value !== false && $value !== ''); 
		});

		
		$success = $this->location_rate_model->save_location_rate($data);
				
		if($success)
		{
			$success = TRUE;
			$this->session->set_flashdata('message','Location Rate Add/Edit Successfull.');
			echo json_encode(array('success'=>$success));
		}
		else
		{
			$success = FALSE;
			$this->session->set_flashdata('message','Unable to add New Location Rate!');
			echo json_encode(array('success'=>$success));
		}
		
	}

	public function delete_json()
	{
    	$id=$this->input->post('rate_id');
		$success = $this->location_rate_model->delete('LOCATION_RATE',array('rate_id'=>$id));
		echo json_encode(array('success'=>$success));
		// $this->page_model->delete('PAGE',array('parent_id'=>$id));
	}
	
	
}