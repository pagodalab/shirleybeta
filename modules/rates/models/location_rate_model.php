<?php
class Location_rate_model extends MY_Model
{	
	var $joins=array();
	function __construct()
	{
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('LOCATION_RATE'=>$this->prefix.'location_rates',
        					'COUNTRY'=>$this->prefix.'countries'
        					);
		$this->_JOINS=array('COUNTRY'=>array('join_type'=>'LEFT','join_field'=>'countries.id=location_rates.location_id',
                                           'select'=>"countries.*",'alias'=>'countries'),
                            );        
    }

     public function getLocationRates($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='location_rates.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['LOCATION_RATE']. ' location_rates');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('location_rates.rate_id', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }

	//zone areas
	function save_location_rate($data)
	{
		if(!$data['rate_id']) 
		{
			$this->db->insert($this->_TABLES['LOCATION_RATE'], $data);
			return $this->db->insert_id();
		} 
		else 
		{
			$this->db->where('rate_id', $data['rate_id']);
			$this->db->update($this->_TABLES['LOCATION_RATE'], $data);
			return $data['rate_id'];
		}
	}
	
	function delete_zone_areas($country_id)
	{
		$this->db->where('zone_id', $country_id)->delete($this->_TABLES['LOCATION_RATE']);
	}
	
	function delete_zone_area($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['LOCATION_RATE']);
	}
	
	function get_location_rates() 
	{
		return $this->db->get($this->_TABLES['LOCATION_RATE'])->result_array();
	}
	
	function get_location_rate_by_id($id)
	{
		$this->db->where('rate_id', $id);
		return $this->db->get($this->_TABLES['LOCATION_RATE'])->row_array();
	}
	
}	