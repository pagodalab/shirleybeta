<?php
class Banner_model extends MY_Model
{
	var $joins=array();	
	public function __construct()
	{
		parent::__construct();
		$this->prefix='tbl_';
		$this->_TABLES=array('COLLECTION'=>$this->prefix.'banner_collections',
							 'BANNER'=>$this->prefix.'banners');
		$this->_JOINS=array('DOCTORS'=>array('join_type'=>'INNER','join_field'=>'doctors.doctor_id=appointments.doctor',
					   'select'=>"doctor_id,concat(doctors.first_name,' ',doctors.last_name) as doctor_name",'alias'=>'doctors'),
								  
		);  
	}
	
	function getCollections($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
	{
       $fields='collections.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['COLLECTION']. ' collections');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('name','ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();			
	}
	
	function collectionById($banner_collection_id)
	{
		return $this->getCollections(array('banner_collection_id'=>$banner_collection_id))->row();
	}
	
	function banner_collection_banners($banner_collection_id, $only_active=false, $limit=5)
	{
		$this->db->where('banner_collection_id', $banner_collection_id);
		$banners	= $this->db->order_by('sequence', 'ASC')->get($this->_TABLES['BANNER'])->result();
		
		if($only_active)
		{
			$return	= array();
			foreach ($banners as $banner)
			{
				if ($banner->enable_date == '0000-00-00')
				{
					$enable_test	= false;
					$enable			= '';
				}
				else
				{
					$eo			 	= explode('-', $banner->enable_date);
					$enable_test	= $eo[0].$eo[1].$eo[2];
					$enable			= $eo[1].'-'.$eo[2].'-'.$eo[0];
				}

				if ($banner->disable_date == '0000-00-00')
				{
					$disable_test	= false;
					$disable		= '';
				}
				else
				{
					$do			 	= explode('-', $banner->disable_date);
					$disable_test	= $do[0].$do[1].$do[2];
					$disable		= $do[1].'-'.$do[2].'-'.$do[0];
				}

				$curDate		= date('Ymd');

				if ( (!$enable_test || $curDate >= $enable_test) && (!$disable_test || $curDate < $disable_test))
				{
					$return[]	= $banner;
				}

				if(count($return) == $limit)
				{
					break;
				}
			}
			
			return $return;
		}
		else
		{
			return $banners;
		}
	}
	
	function banner($banner_id)
	{
		$this->db->where('banner_id', $banner_id);
		$result = $this->db->get($this->_TABLES['BANNER']);
		$result = $result->row();
		
		if ($result)
		{
			if ($result->enable_date == '0000-00-00')
			{
				$result->enable_date = '';
			}
			
			if ($result->disable_date == '0000-00-00')
			{
				$result->disable_date = '';
			}
		
			return $result;
		}
		else
		{ 
			return array();
		}
	}
	
	function save_banner($data)
	{
		if(isset($data['banner_id']))
		{
			$this->db->where('banner_id', $data['banner_id']);
			$this->db->update($this->_TABLES['BANNER'], $data);
		}
		else
		{
			$data['sequence'] = $this->get_next_sequence($data['banner_collection_id']);
			$this->db->insert($this->_TABLES['BANNER'], $data);
		}
	}
	
	function save_banner_collection($data)
	{
		if(isset($data['banner_collection_id']) && (bool)$data['banner_collection_id'])
		{
			$this->db->where('banner_collection_id', $data['banner_collection_id']);
			$this->db->update($this->_TABLES['COLLECTION'], $data);
		}
		else
		{
			$this->db->insert($this->_TABLES['COLLECTION'], $data);
		}
	}
	
	function get_homepage_banners($limit = false)
	{
		$banners	= $this->db->order_by('sequence ASC')->get($this->_TABLES['BANNER'])->result();
		$count	= 1;
		foreach ($banners as &$banner)
		{
			if ($banner->enable_date == '0000-00-00')
			{
				$enable_test	= false;
				$enable			= '';
			}
			else
			{
				$eo			 	= explode('-', $banner->enable_date);
				$enable_test	= $eo[0].$eo[1].$eo[2];
				$enable			= $eo[1].'-'.$eo[2].'-'.$eo[0];
			}

			if ($banner->disable_date == '0000-00-00')
			{
				$disable_test	= false;
				$disable		= '';
			}
			else
			{
				$do			 	= explode('-', $banner->disable_date);
				$disable_test	= $do[0].$do[1].$do[2];
				$disable		= $do[1].'-'.$do[2].'-'.$do[0];
			}

			$curDate		= date('Ymd');

			if (($enable_test && $enable_test > $curDate) || ($disable_test && $disable_test <= $curDate))
			{
				unset($banner);
			}
			else
			{
				$count++;
			}
			
			if($limit)
			{
				if($count > $limit)
				{
					continue;
				}				
			}
		}
		return $banners;
	}
	

	
	function delete_collection($banner_collection_id)
	{
		$where=array('banner_collection_id'=>$banner_collection_id);
		$this->delete('BANNER',$where);
		$this->delete('COLLECTION',$where);
	}
	
	function get_next_sequence($banner_collection_id)
	{
		$this->db->where('banner_collection_id', $banner_collection_id);
		$this->db->select('sequence');
		$this->db->order_by('sequence DESC');
		$this->db->limit(1);
		$result = $this->db->get($this->_TABLES['BANNER']);
		$result = $result->row();
		if ($result)
		{
			return $result->sequence + 1;
		}
		else
		{
			return 0;
		}
	}

	function organize($banners)
	{
		foreach ($banners as $sequence => $id)
		{
			$data = array('sequence' => $sequence);
			$this->db->where('banner_id', $id);
			$this->db->update($this->_TABLES['BANNER'], $data);
		}
	}
}