<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('feature_slider/feature_slider_model');
		$this->lang->load('feature_slider/feature_slider');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'feature_slider';
		$data['page_title'] = 'Featured Slider';
		$this->view($this->config->item('admin_folder').'/index', $data);	
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->feature_slider_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->feature_slider_model->getFeature_sliders()->result_array();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['product_id']!='')?$this->db->like('product_id',$params['search']['product_id']):'';
			($params['search']['sequence']!='')?$this->db->like('sequence',$params['search']['sequence']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  
	}

	public function combo_json()
	{
		$rows=$this->feature_slider_model->getFeature_sliders()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');

		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->feature_slider_model->delete('FEATURE_SLIDER',array('feature_slider_id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(! $data['product_id']) {
        	echo json_encode(array('msg'=>"Fail",'success'=>false));		
        	exit;
        }

        if(!$this->input->post('id'))
        {
        	$data['added_date'] =date('Y-m-d H:i:s');		
        	$success=$this->feature_slider_model->insert('FEATURE_SLIDER',$data);
        }
        else
        {
        	$data['modified_date'] =date('Y-m-d H:i:s');			
        	$success=$this->feature_slider_model->update('FEATURE_SLIDER',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	$data['product_id'] = $this->input->post('product_id');
    	$data['sequence'] = $this->input->post('sequence');
    	$data['status'] = $this->input->post('status');

    	return $data;
    }



}