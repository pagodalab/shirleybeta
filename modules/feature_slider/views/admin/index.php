	<table id="feature_slider-table" data-options="pagination:true,title:'<?php  echo lang('feature_slider')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
		<thead>
			<th field="checkbox" checkbox="true"></th>
			<?php /*?><th field="id" sortable="true" width="30"><?php echo lang('id')?></th><?php */?>
			<th field="name_en" sortable="true" width="100"><?php echo lang('product_name')?></th>
			<th field="sequence" sortable="true" width="30"><?php echo lang('sequence')?></th>
			<th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?php echo lang('status')?></th>

			<th field="action" width="10" formatter="getActions"><?php  echo lang('action')?></th>
		</thead>
	</table>

	<div id="toolbar" style="padding:5px;height:auto">
		<p>
			<a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Add Feature Slider"><span><i class="icon-plus icon-inverse"></i></span> Create</a>
			<a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Feature Slider"><span><i class="icon-trash icon-inverse"></i></span> Remove Selected</a>
		</p>

	</div> 

	<!--for create and edit feature_slider form-->
	<div id="dlg" class="easyui-dialog" style="width:900px;height:auto;padding:10px 20px" data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true" >

		<form id="form-feature_slider" method="post" >
			<div class="col-md-4">
				<div class="col-md-3"><label><?php echo lang('product_name') ?></label></div>
				<div class="col-md-9"> <input type="text" name="product" class="span8" id="product_search"> </div>
				<select class="span8" id="product_list" name="product_id" size="5" style="margin:0px;"></select>

				<div class="col-md-3"><label><?php echo lang('sequence') ?></label></div>
				<div class="col-md-9"> <input type="text" name="sequence" class="span1"> </div>

				<div class="col-md-3"><label><?php echo lang('status') ?> <input type="checkbox" name="status" class="span1" checked> </label></div>

				<input type="hidden" name="id" id="id">

			</div>	
		</form>
		<div id="dlg-buttons">
			<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
			<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
		</div>    
	</div>
	<!--div ends-->

	<script language="javascript" type="text/javascript">
		$(function(){
			$('#clear').click(function(){
				$('#feature_slider-search-form').form('clear');
				$('#feature_slider-table').datagrid({
					queryParams:null
				});

			});

			$('#search').click(function(){
				$('#feature_slider-table').datagrid({
					queryParams:{data:$('#feature_slider-search-form').serialize()}
				});
			});		
			$('#feature_slider-table').datagrid({
				url:'<?php  echo site_url('feature_slider/admin/json')?>',
				height:'auto',
				width:'auto',
				onDblClickRow:function(index,row)
				{
					edit(index);
				}
			});

			$('#product_search').keyup(function(){
				$('#product_list').html('');
				run_product_query();
			});

		});

		function run_product_query()
		{
			$.post("<?php echo site_url('product/admin/product_autocomplete/');?>", { name: $('#product_search').val(), limit:10},
				function(data) {

					$('#product_list').html('');

					$.each(data, function(index, value){

						if($('#related_product_'+index).length == 0)
						{
							$('#product_list').append('<option id="product_item_'+index+'" value="'+index+'">'+value+'</option>');
						}
					});

				}, 'json');
		}

		function getActions(value,row,index)
		{

			var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_feature_slider')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
			var d = '<a href="#" onclick="removefeature_slider('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_feature_slider')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
			return e+d;		
		}

		function formatStatus(value)
		{
			if(value==1)
			{
				return 'Yes';
			}
			return 'No';
		}

		function create(){
		//Create code here
		$('#form-feature_slider').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_feature_slider')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#feature_slider-table').datagrid('getRows')[index];
		if (row){
			$('#form-feature_slider').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_feature_slider')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	
	function removefeature_slider(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#feature_slider-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('feature_slider/admin/delete_json')?>', {id:[row.id]}, function(){
					$('#feature_slider-table').datagrid('deleteRow', index);
					$('#feature_slider-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#feature_slider-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].feature_slider_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('feature_slider/admin/delete_json')?>',{id:selected},function(data){
						$('#feature_slider-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-feature_slider').form('submit',{
			url: '<?php  echo site_url('feature_slider/admin/save')?>',
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-feature_slider').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#feature_slider-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
			
		});		
		
	}
	
	
</script>