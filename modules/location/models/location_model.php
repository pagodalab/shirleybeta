<?php
class Location_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('COUNTRY'=>$this->prefix.'countries','ZONE_AREA'=>$this->prefix.'country_zone_areas',
							 'ZONE'=>$this->prefix.'country_zones');
		$this->_JOINS=array('ORDER_ITEM'=>array('join_type'=>'INNER','join_field'=>'orders.id=order_items.id',
                                           'select'=>"*",'alias'=>'order_items'),
                            );        
    }
	//zone areas
	function save_zone_area($data)
	{
		if(!$data['id']) 
		{
			$this->db->insert($this->_TABLES['ZONE_AREA'], $data);
			return $this->db->insert_id();
		} 
		else 
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_TABLES['ZONE_AREA'], $data);
			return $data['id'];
		}
	}
	
	function delete_zone_areas($country_id)
	{
		$this->db->where('zone_id', $country_id)->delete($this->_TABLES['ZONE_AREA']);
	}
	
	function delete_zone_area($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['ZONE_AREA']);
	}
	
	function get_zone_areas($country_id) 
	{
		$this->db->where('zone_id', $country_id);
		return $this->db->get($this->_TABLES['ZONE_AREA'])->result();
	}
	
	function get_zone_area($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_TABLES['ZONE_AREA'])->row();
	}
	
	//zones
	function save_zone($data)
	{
		if(!$data['id']) 
		{
			$this->db->insert($this->_TABLES['ZONE'], $data);
			return $this->db->insert_id();
		} 
		else 
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_TABLES['ZONE'], $data);
			return $data['id'];
		}
	}
	
	function delete_zones($country_id)
	{
		$this->db->where('country_id', $country_id)->delete($this->_TABLES['ZONE']);
	}
	
	function delete_zone($id)
	{
		$this->delete_zone_areas($id);
		
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['ZONE']);
	}
	
	function get_zones($country_id) 
	{
		$this->db->where('country_id', $country_id);
		return $this->db->get($this->_TABLES['ZONE'])->result();
	}
	
	
	function get_zone($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_TABLES['ZONE'])->row();
	}
	
	
	
	//countries
	function save_country($data)
	{
		if(!$data['id']) 
		{
			$this->db->insert($this->_TABLES['COUNTRY'], $data);
			return $this->db->insert_id();
		} 
		else 
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_TABLES['COUNTRY'], $data);
			return $data['id'];
		}
	}
	
	function organize_countries($countries)
	{
		//now loop through the products we have and add them in
		$sequence = 0;
		foreach ($countries as $country)
		{
			$this->db->where('id',$country)->update($this->_TABLES['COUNTRY'], array('sequence'=>$sequence));
			$sequence++;
		}
	}
	
	function get_countries()
	{
		return $this->db->order_by('sequence', 'ASC')->get($this->_TABLES['COUNTRY'])->result();
	}
	
	function get_country_by_zone_id($id)
	{
		$zone	= $this->get_zone($id);
		return $this->get_country($zone->country_id);
	}
	
	function get_country($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_TABLES['COUNTRY'])->row();
	}
	
	
	function delete_country($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['COUNTRY']);
	}
	
	
	function get_countries_menu()
	{	
		$countries	= $this->db->order_by('sequence', 'ASC')->where('status', 1)->get($this->_TABLES['COUNTRY'])->result();
		$return		= array();
		foreach($countries as $c)
		{
			$return[$c->id] = $c->name;
		}
		return $return;
	}
	
	function get_zones_menu($country_id)
	{
		$zones	= $this->db->where(array('status'=>1, 'country_id'=>$country_id))->get($this->_TABLES['ZONE'])->result();
		$return	= array();
		foreach($zones as $z)
		{
			$return[$z->id] = $z->name;
		}
		return $return;
	}
	
	function has_zones($country_id)
	{
		if(!$country_id)
		{
			return false;
		}
		$count = $this->db->where('country_id', $country_id)->count_all_results($this->_TABLES['ZONE']);
		if($count > 0)
		{
			return true;
		} else {
			return false;
		}
	}

	/*
	// returns array of strings formatted for select boxes
	function get_countries_zones()
	{
		$countries = $this->db->get($this->_TABLES['COUNTRY'])->result_array();
		
		$list = array();
		foreach($countries as $c)
		{
			if(!empty($c['name']))
			{		
				$zones =  $this->db->where('country_id', $c['id'])->get($this->_TABLES['ZONE'])->result_array();
				$states = array();
				foreach($zones as $z)
				{
					// todo - what to put if there are no zones in a country?
					
					if(!empty($z['code']))
					{
						$states[$z['id']] = $z['name'];
					}
				}
				
				$list[$c['name']] = $states;
			}
		}
		
		return $list;
	}
	*/
	
	function get_country_by_name($name)
	{
		$this->db->where("name like '%".$name."%'");
		return $this->db->get($this->_TABLES['COUNTRY'])->row();
	}
}	