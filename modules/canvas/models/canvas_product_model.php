<?php

class Canvas_product_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('CANVAS_PRODUCT'=>$this->prefix.'canvas_products',
							 'CATEGORY_PRODUCT'=>$this->prefix.'category_products',
							 'CATEGORY'=>$this->prefix.'categories',
							 'PRODUCT'=>$this->prefix.'products'
							 );
		$this->_JOINS=array('CATEGORY_PRODUCT'=>array('join_type'=>'LEFT','join_field'=>'canvas_products.product_id=category_products.product_id',
                                           'select'=>'category_products.*','alias'=>'category_products'),
							'CATEGORY'=>array('join_type'=>'LEFT','join_field'=>'categories.id=category_products.category_id',
                                           'select'=>'categories.name as category_name','alias'=>'categories'),
							'PRODUCT'=>array('join_type'=>'LEFT','join_field'=>'canvas_products.product_id=products.id',
                                           'select'=>'products.name as product_name,products.*,LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price ','alias'=>'products')			   

                            );
    }

    public function getCanvasProducts($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='canvas_products.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['CANVAS_PRODUCT']. ' canvas_products');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {

        $this->db->from($this->_TABLES['CANVAS_PRODUCT'].' canvas_products');

        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;

       (! is_null($where))?$this->db->where($where):NULL;

        return $this->db->count_all_results();
    }

}
?>