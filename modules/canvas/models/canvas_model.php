<?php

class Canvas_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('CANVAS_TEMPLATE'=>$this->prefix.'canvas_templates','CATEGORY'=>$this->prefix.'categories');
		$this->_JOINS=array('CATEGORY'=>array('join_type'=>'LEFT','join_field'=>' FIND_IN_SET(categories.id, canvas_templates.categories) > 0',
                                           'select'=>'group_concat(categories.name) as category_names','alias'=>'categories'),

                            );
    }

    public function getCanvasTemplates($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='canvas_templates.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CANVAS_TEMPLATE']. ' canvas_templates');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {

        $this->db->from($this->_TABLES['CANVAS_TEMPLATE'].' canvas_templates');

        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;

       (! is_null($where))?$this->db->where($where):NULL;

        return $this->db->count_all_results();
    }

}
?>