  <style>
	.category
	{
		float: left;
		margin: 5px;
		border:1px solid red; 
		padding:25px;
		background-color:#D7D7D7;
	}
	
	.temp-categories
	{
		border:1px solid #ccc;
		width:350px;
		height:500px;
	}
	
	.temp-categories > div
	{
		border:1px dotted #999999; 
		background-color:yellowgreen; 
		padding:25px;
		position:absolute !important;
		/*right:10px !important;*/
	}
	
  </style>
<form method="post" action="<?php echo site_url('canvas/admin/save')?>" enctype="multipart/form-data" id="canvas-form">
	<div class="form-contents">
		<fieldset>
			<label for="template_name">Template Title :</label>
			<input type="text" name="template_name" id="template_name" value="<?php echo $canvas['template_name']?>" required="required">
			
			<label for="categories">Categories : </label>
			<select name="categories[]" multiple style="height:200px" id="categories">
                <?php
                    $category_ids = explode(',', $canvas['categories']);
                    foreach($categories as $category){
                ?>
                       <option value="<?php echo $category['id']?>" <?=(in_array($category['id'], $category_ids)?'selected="selected"':'')?>><?php echo $category['name']?></option>
                <?php }?>
            </select>

            <label for="image">Image :</label>
            <label id="upload_image_name" style="display:none"></label>
            <input name="template_image" id="template_image" type="text" style="display:none"/>
            <input type="file" id="upload_image" name="userfile" style="display:block"/>
            <a href="#" id="change-image" title="Delete" style="display:none"><img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a>
			<label id="temp_image"><?php echo $canvas['template_image']?></label>
            
            <label for="status">Status :</label>
			<label class="radio inline"><input type="radio" name="status" id="status1" value="1" <?php echo ($canvas['status'] == 1)?'checked="checked"':''?>> Active  </label>
			<label class="radio inline"><input type="radio" name="status" id="status0" value="0" <?php echo ($canvas['status'] == 0)?'checked="checked"':''?>> Inactive  </label>
			<input type="hidden" name="template_id" id="template_id" value="<?php echo $canvas['template_id']?>">
			<label>Contents : </label>
			<textarea id="template_content" name="template_content" style="width: 1048px; height: 100px;"><?php echo $canvas['template_content']?></textarea>
			<br/>
			<br/>
			
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success">
							Generate Code
						</a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="content">
                            	
                                <div id="target" class="ui-widget-header" style="border:1px solid #ccc;width:350px;height:500px;float:left;<?php if($canvas['template_image']){?>background-image:url(<?php echo base_url()?>uploads/canvas/background/<?php echo $canvas['template_image']?>);<?php }?>"></div>
                                <div id="source" style="border:1px solid #ccc;width:67%;height:400px;float:left;margin:5px;">
                                	
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn">Submit</button>
	    <a href="<?php echo site_url('canvas/admin')?>" class="btn btn-danger">Cancel</a>
	</div>	
</form>

<script type="text/javascript">

$(function() {	
	
	uploadReady();
	var $source = $( "#source" );
	var $template = $( "#target" );
	
	$("#categories").on('blur',function(){
		var ids = $("#categories").val();
		$.post('<?php echo site_url('canvas/admin/getCategories')?>',{id:ids},function(data){
			//console.log("This is data ",data); //return false;
			var content = '<h4 align="center">Categories</h4>';
			$.each(data,function(i,v){
				content+='<div class="category" id="'+v.name+'" data-cat="'+v.name+'" data-canvas="new" data-index="'+i+'">'+v.name+'</div>';
			});
			$('#source').html(content);

			/**drag and drop starts**/
			
			$( ".category" ).draggable({ revert: "invalid" }).resizable();
			
 			var items = [];
			var item_desc = [];
			$( "#target" ).droppable({
			  activeClass: "ui-state-default",
			  hoverClass: "ui-state-hover",
			  drop: function( event, ui ) {
				  
				$this=$(this);
				$this.addClass( "ui-state-highlight" );
				$item = ui.draggable;
				//console.log('this ',$item);
				
				var id = $item.attr('id');
				var style = $item.attr('style');
				var currentPos = $item.position();
				//var temp_offset = $template.offset();
				var childPos = $item.offset();
				var parentPos = $template.parent().offset();
				var top = childPos.top - parentPos.top;
				var	left = childPos.left - parentPos.left;
				
				var template_contents = $('#template_content').val();
				var content = '';
				
				//console.log('contents ',template_contents);
				var result = isInArray(id, items);
				console.log('result ',result);
				
				if(result == true)
				{
					var new_content =  '<div id="'+id+'" style="margin-left:'+left + ';margin-top:' + top+';height:' + top+';width:' + left+'">';
					//console.log('new_content ',new_content);
					
					var search_term = id; // search term
					var search = new RegExp(search_term , "gi");
					var arr = jQuery.map(item_desc, function (value,i) {
						//console.log('i ',i);
						//console.log('Values ',value);
                      if(value.match(search))
					  { 
					  	item_desc[i] = new_content;
					  	//return value;
					  }
					  else
					  {
                      	//return null;
					  }//return value;
                    });
					
					$.each(item_desc,function(i,value)
					{
						content+=value;
					});
					
					$('#template_content').text(content);

				}
				else
				{
					//var new_content =  '<div id="'+id+'" style="margin-left:'+ currentPos.left + ';margin-top:' + currentPos.top+'">';
					var new_content =  '<div id="'+id+'" style="margin-left:'+ left + ';margin-top:' + top+';height:' + top+';width:' + left+'">';
					items.push(id);
					item_desc.push(new_content);
					
					$.each(item_desc,function(i,value)
					{
						content+=value;
					});
					
					$('#template_content').text(content);
				}
				
				//console.log('items description ',item_desc);
			  }
			  
			});
			
			/**drag and drop ends**/
			
			
		},'json');
		
	});

	$('#change-image').on('click',function(){
		$.messager.confirm('Confirm','Are you sure to delete ?',function(r){
			if (r){
				$.post('<?php echo site_url('canvas/admin/upload_delete')?>',{filename:$('#template_image').val()},function(data){
				$('#upload_image_name').html('').hide();
				$('#change-image').hide();
				$('#temp_image').hide();
				$('#upload_image').show();	
				$('#target').css('background','');
				});
			}
		});
	});
	//$(".product_box" ).draggable({ containment: "template" }).resizable({ containment: "template" });
	function isInArray(value, new_item) {
	  return new_item.indexOf(value) > -1;
	}
	
	function uploadReady()
	{
		var template_id = $('#template_id').val();
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('canvas/admin/upload_image?template_id=')?>'+template_id,
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#template_image').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
					$('#temp_image').html(filename);
					$('#target').css('background-image','url(<?php echo base_url()?>uploads/canvas/background/'+filename+')');
				}
                else
                {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
                }
			}		
		});		
	}

});

</script>