  <style>
	.category
	{
		float: left;
		margin: 5px;
		border:1px solid red; 
		padding:25px;
	}
	
	.temp-categories
	{
		border:1px solid #ccc;
		width:350px;
		height:500px;
	}
	
	.temp-categories > div
	{
		border:1px dotted #999999; 
		background-color:yellowgreen; 
		padding:25px;
		position:absolute !important;
		/*right:10px !important;*/
	}
	
  </style>
<form method="post" action="<?php echo site_url('canvas/admin/save')?>" enctype="multipart/form-data" id="canvas-form">
	<div class="form-contents">
		<fieldset>
			<label for="template_name">Template Title :</label>
			<input type="text" name="template_name" id="template_name" value="<?php echo $canvas['template_name']?>" required="required">
			
			<label for="categories">Categories : </label>
			<select name="categories[]" multiple style="height:200px" id="categories">
                <?php
                    $category_ids = explode(',', $canvas['categories']);
                    foreach($categories as $category){
                ?>
                       <option value="<?php echo $category['id']?>" <?=(in_array($category['id'], $category_ids)?'selected="selected"':'')?>><?php echo $category['name']?></option>
                <?php }?>
            </select>

            <label for="image">Image :</label>
            <label id="upload_image_name" style="display:none"></label>
            <input name="template_image" id="template_image" type="text" style="display:none"/>
            <input type="file" id="upload_image" name="userfile" style="display:block"/>
            <a href="#" id="change-image" title="Delete" style="display:none"><img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a>
			<label id="temp_image"><?php echo $canvas['template_image']?></label>
            
            <label for="status">Status :</label>
			<label class="radio inline"><input type="radio" name="status" id="status1" value="1" <?php echo ($canvas['status'] == 1)?'checked="checked"':''?>> Active  </label>
			<label class="radio inline"><input type="radio" name="status" id="status0" value="0" <?php echo ($canvas['status'] == 0)?'checked="checked"':''?>> Inactive  </label>
			<input type="hidden" name="template_id" id="template_id" value="<?php echo $canvas['template_id']?>">
			<label>Contents : </label>
			<textarea id="template_content" name="template_content" style="width: 1048px; height: 279px;"><?php echo $canvas['template_content']?></textarea>
			<br/>
			<br/>
			
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success">
							Generate Code
						</a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="content">
                            	<div id="source" style="border:1px solid #ccc;width:67%;height:400px;float:left;margin:5px;">
                                	
                                </div>
                                <div id="target" class="ui-widget-header" style="border:1px solid #ccc;width:350px;height:500px;float:left;<?php if($canvas['template_image']){?>background-image:url(<?php echo base_url()?>uploads/canvas/background/<?php echo $canvas['template_image']?>);<?php }?>"></div>
                                
                                <div class="clearfix"></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn">Submit</button>
	    <a href="<?php echo site_url('canvas/admin')?>" class="btn btn-danger">Cancel</a>
	</div>	
</form>

<script type="text/javascript">

$(function() {	
	
	uploadReady();
	var $source = $( "#source" );
	var $template = $( "#target" );
	
	$("#categories").on('blur',function(){
		var ids = $("#categories").val();
		$.post('<?php echo site_url('canvas/admin/getCategories')?>',{id:ids},function(data){
			//console.log("This is data ",data); //return false;
			var content = '<h4 align="center">Categories</h4>';
			$.each(data,function(i,v){
				content+='<div class="category" id="'+v.name+'" data-cat="'+v.name+'" data-canvas="new" data-index="'+i+'">'+v.name+'</div>';
			});
			$('#source').html(content);

			/**drag and drop starts**/
			
			$( ".category" ).draggable({ 
				revert: "invalid",
				//helper: "clone",
				//cursor: "move",
				containment: $template,
				stop:function(event,ui)
						{
							var currentPos = $(this).position();
							//var content = 
       						//$("#template_content").text("<div " + currentPos.left + "\nTop: " + currentPos.top);
						}
			}).resizable(
			{
				stop:function(event,ui)
					{
						var offset = $(this).offset();
						var temp_offset = $template.offset();
						var posY = offset.top - temp_offset.top;
						var posX = offset.left - temp_offset.left;
						 //$("#template_content").text("CURRENT1: \nLeft: " + posY + "\nTop: " + posX);
					}
			});
 
			$( "#target" ).droppable({
				accept:".category",
				activeClass: "ui-state-default",
				drop: function( event, ui ) {
				//$( this ).addClass( "ui-state-highlight" );
				addCategory( ui.draggable );
			  }
			});
			
			//var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
			function addCategory( $item ) {
				var new_item = new Array();
				
				var $list = $( ".temp-categories", $template ).length ?
				$( ".temp-categories", $template) :
				$( "<div class='temp-categories ui-helper-reset'/>" ).appendTo( $template );
				$item.appendTo( $list );//.append( recycle_icon );
				
				$item.draggable({ 
					revert: "invalid",
					//helper: "clone",
					//cursor: "move",
					containment: $template,
					stop:function(event,ui)
						{
							var id = $item.attr('id');
							var result = isInArray(id, new_item);
							console.log('result ',result);
							var currentPos = $(this).position();
							var content = '<div id="'+id+'" style="margin-left:'+ currentPos.left + ';margin-top:' + currentPos.top+'">';
							if(result == true)
							{
								$("#template_content").text(content);
							}
							else
							{
								$("#template_content").append(content);
							}
							
       						
						}
				}).resizable(
				{
					stop:function(event,ui)
						{
							var offset = $(this).offset();
							var temp_offset = $template.offset();
							var posY = offset.top - temp_offset.top;
							var posX = offset.left - temp_offset.left;
							 //$("#template_content").text("CURRENT1: \nLeft: " + posY + "\nTop: " + posX);
						}
				});
				new_item.push($item);
				console.log('items ',new_item);
			}
			
			
			/**drag and drop ends**/
			
			
		},'json');
		
	});

	$('#change-image').on('click',function(){
		$.messager.confirm('Confirm','Are you sure to delete ?',function(r){
			if (r){
				$.post('<?php echo site_url('canvas/admin/upload_delete')?>',{filename:$('#template_image').val()},function(data){
				$('#upload_image_name').html('').hide();
				$('#change-image').hide();
				$('#temp_image').hide();
				$('#upload_image').show();	
				$('#target').css('background','');
				});
			}
		});
	});
	//$(".product_box" ).draggable({ containment: "template" }).resizable({ containment: "template" });
	function isInArray(value, new_item) {
	  return new_item.indexOf(value) > -1;
	}
	
	function uploadReady()
	{
		var template_id = $('#template_id').val();
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('canvas/admin/upload_image?template_id=')?>'+template_id,
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#template_image').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
					$('#temp_image').html(filename);
					$('#target').css('background-image','url(<?php echo base_url()?>uploads/canvas/background/'+filename+')');
				}
                else
                {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
                }
			}		
		});		
	}

});

</script>