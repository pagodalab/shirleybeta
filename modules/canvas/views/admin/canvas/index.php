<?php /*?><div id="search-panel" class="easyui-panel" data-options="title:'template Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="template-search-form">
<table width="100%">
<tr><td><label><?=lang('template_name')?></label>:</td>
<td><input name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<td><label>Category</label>:</td>
<td><input name="search[category]" id="search_category"/></td>
</tr>
<tr>
<td><label>Brand</label>:</td>
<td><input name="search[brand]" id="search_brand"  class="easyui-combobox"/></td>
<td><label>Added Date</label>:</td>
<td><input type="text" name="date[added_date][from]" id="search_added_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[added_date][to]" id="search_added_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><input type="checkbox" name="search[is_flexible]" id="search_is_flexible"> Is Flexible</label>
</td>
<td><label><input type="checkbox" name="search[is_perishable]" id="search_is_perishable"> Is Perishable</label>
<td><label>Status</label>:</td>
<td>
	<select type="text" name="search[enabled]" id="search_enabled"  class="easyui-combobox"/>
		<option value=""></option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
</td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div><?php */?>
<br/>
<table id="template-table" data-options="pagination:true,title:'Canvas Templates',pagesize:'10', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
        <th field="checkbox" checkbox="true"></th>
        <th field="image" sortable="true" width="80" formatter="formatImage"><?=lang('template_image')?></th>
        <th field="template_name" sortable="true" width="100"><?=lang('template_title')?></th>
        <th field="template_code" sortable="true" width="100"><?=lang('template_code')?></th>
        <th field="categories" width="50"><?=lang('template_category')?></th>
        <th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?=lang('template_status')?></th>
        <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<?php echo site_url('canvas/admin/form')?>" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Template</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
</div> 

<!--for survey detail -->
<div id="detail-dialog" class="easyui-window" style="width:600px;height:500px;padding:10px 20px"
        closed="true" collapsible="true"  buttons="#dlg-buttons">
<div id="template-detail-view"></div>        
</div>        


<script language="javascript" type="text/javascript">

	$(function(){
		//$('#search_brand').combobox({url:'<?php echo site_url('brand/admin/combo_json')?>',valueField:'brand_id',textField:'brand_name'});
		//$('#search_category').combotree({url:'<?php echo site_url('category/admin/treeview_json')?>'});
			
		/*$('#clear').click(function(){
			$('#template-search-form').form('clear');
			$('#template-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#template-table').datagrid({
				queryParams:{data:$('#template-search-form').serialize()}
				});
		});	*/
			
		$('#template-table').datagrid({
			url:'<?=site_url('canvas/admin/json')?>',
			height:'auto',
			width:'auto',
			/*onDblClickRow:function(index,row)
			{
				$('#form-template').form('load',row);
				$('#dlg').window('open').window('setTitle','<?=lang('edit_template')?>');
			}*/
		});
		$('#change-image').on('click',function(){
			$.messager.confirm('Confirm','Are you sure to delete ?',function(r){
				if (r){
					$.post('<?php echo site_url('canvas/admin/upload_delete')?>',{filename:$('#template_image').val()},function(data){
					$('#upload_image_name').html('').hide();
					$('#change-image').hide();
					$('#upload_image').show();	
					});
				}
			});
		});
	});
	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return "Yes";	
		}
		return "No";
	}
	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('canvas/admin/form/')?>/'+row.template_id+'" class="tooltip-top btn btn-small" title="<?=lang('edit_template')?>"><span><i class="icon-pencil"></i></span> </a>';
		
		var d = '<a href="#" onclick="removeTemplate('+index+')" class="tooltip-top btn btn-small" title="<?=lang('delete_template')?>"><span><i class="icon-trash"></i></span> </a>';

		return e+d;		
	}

	function upload(index)
	{
		var row = $('#template-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('template_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {template_id: row.template_id};
		}
		$('#form-upload').form('clear');
	}
		
	function removeTemplate(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#template-table').datagrid('getRows')[index];
				//console.log(row); return false;
				$.post('<?=site_url('canvas/admin/delete_json')?>', {id:[row.template_id]}, function(){
					<?php $this->session->set_flashdata('message', lang('message_saved_template'));?>
					$('#template-table').datagrid('deleteRow', index);
					$('#template-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#template-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].template_id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?=site_url('canvas/admin/delete_json')?>',{id:selected},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_template'));?>
						$('#template-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function formatImage(value)
	{
		//console.log(value);
		var img = '<img src="<?php echo base_url()?>uploads/canvas/templates/'+value+'" height="150px" width="150px">';

		return img;
	}


	function changeSelected()
	{
		var status = $('#status').combobox('getValue');
		var rows=$('#template-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to Change?',function(r){
				if(r){				
					$.post('<?=site_url('canvas/admin/change_status')?>',{id:selected,enabled:status},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_template'));?>
						$('#template-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No template Selected!');	
		}	
	}
	
	function save()
	{
		$('#form-template').form('submit',{
			url: '<?php  echo site_url('canvas/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-template').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#table-template').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	function uploadReady()
	{
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('canvas/admin/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#template_image').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
				}
                else
                {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
                }
			}		
		});		
	}
</script>
