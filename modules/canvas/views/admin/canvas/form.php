  <style>
	.category
	{
		float: left;
		margin: 5px;
		border:1px solid red; 
		padding:25px;
		background-color:#D7D7D7;
		/*background: rgba(20,20,20,0.3);*/
	}
	
	.temp-categories
	{
		border:1px solid #ccc;
		width:350px;
		height:500px;
	}
	
	.temp-categories > div
	{
		border:1px dotted #999999; 
		background-color:yellowgreen; 
		padding:25px;
		position:absolute !important;
		/*right:10px !important;*/
	}
	
  </style>
<form method="" action="" enctype="multipart/form-data" id="canvas-form">
	<div class="form-contents">
		<fieldset>
			<label for="template_name">Template Title :</label>
			<input type="text" name="template_name" id="template_name" value="<?php echo $canvas['template_name']?>" required="required">
			
			<label for="categories">Categories : </label>
			<select name="categories[]" multiple style="height:200px" id="categories">
                <?php
                    $category_ids = explode(',', $canvas['categories']);
                    foreach($categories as $category){
                ?>
                       <option value="<?php echo $category['id']?>" <?=(in_array($category['id'], $category_ids)?'selected="selected"':'')?>><?php echo $category['name']?></option>
                <?php }?>
            </select>

            <label for="image">Image :</label>
            <label id="upload_image_name" style="display:none"></label>
            <input name="template_image" id="template_image" type="text" style="display:none"/>
            <input type="file" id="upload_image" name="userfile" style="display:block"/>
            <a href="#" id="change-image" title="Delete" style="display:none"><img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a>
			<label id="temp_image"><?php echo $canvas['template_image']?></label>
            
            <label for="status">Status :</label>
			<label class="radio inline"><input type="radio" name="status" id="status1" value="1" <?php echo ($canvas['status'] == 1)?'checked="checked"':''?>> Active  </label>
			<label class="radio inline"><input type="radio" name="status" id="status0" value="0" <?php echo ($canvas['status'] == 0)?'checked="checked"':''?>> Inactive  </label>
			
			<label>Contents : </label>
			<textarea id="template_content" name="template_content" style="width: 1048px; height: 100px;"><?php echo $canvas['template_content']?></textarea>
            <input type="hidden" name="template_id" id="template_id" value="<?php echo $canvas['template_id']?>">
            <input type="hidden" name="template_code" id="template_code" value="<?php echo $canvas['template_code']?>">
			<br/>
			<br/>
			
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success" onclick="generateCanvas()">
							Generate Code
						</a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="content">
                            	
                                <div id="target" class="ui-widget-header" style="border:1px solid #ccc;width:350px;height:500px;float:left;<?php if($canvas['template_image']){?>background-image:url(<?php echo base_url()?>uploads/canvas/background/<?php echo $canvas['template_image']?>);<?php }?>">
                                <?php //if($canvas['template_content']){?>
                                	<?php //echo $canvas['template_content']?>
                                <?php //}?>
                                </div>
                                <div id="source" style="border:1px solid #ccc;width:67%;height:400px;float:left;margin:5px;"></div>
                                <div class="clearfix"></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>

	<div class="form-actions">
		<button type="button" onclick="submitCanvas()" class="btn btn-primary" id="canvas-submit">Submit</button>
	    <a href="<?php echo site_url('canvas/admin')?>" class="btn btn-danger">Cancel</a>
	</div>	
</form>

<script type="text/javascript">
var categories = [];
var bg_image = '';
$(function() {	
	
	uploadReady();

	$('#change-image').on('click',function(){
		///$.messager.confirm('Confirm','Are you sure to delete ?',function(r){
			//if (r){
				$.post('<?php echo site_url('canvas/admin/upload_delete')?>',{filename:$('#template_image').val()},function(data){
				$('#upload_image_name').html('').hide();
				$('#change-image').hide();
				$('#temp_image').hide();
				$('#upload_image').show();	
				$('#target').css('background','');
				bg_image = '';
				});
			//}
		//});
	});
	//$(".product_box" ).draggable({ containment: "template" }).resizable({ containment: "template" });
	function isInArray(value, items) {
	  return items.indexOf(value) > -1;
	}
	
	function uploadReady()
	{
		var template_id = $('#template_id').val();
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('canvas/admin/upload_image?template_id=')?>'+template_id,
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#template_image').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
					$('#temp_image').html(filename);
					$('#target').css('background-image','url(<?php echo base_url()?>uploads/canvas/background/'+filename+')');
					bg_image = 'uploads/canvas/background/'+filename;
				}
                else
                {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
                }
			}		
		});		
	}

});

function generateCanvas()
{
	var ids = $("#categories").val();
		$.post('<?php echo site_url('canvas/admin/getCategories')?>',{id:ids},function(data){
			//console.log("This is data ",data); //return false;
			var content = '<h4 align="center">Categories</h4>';
			$.each(data,function(i,v){
				content+='<div class="category" id="'+v.name+'" data-cat="'+v.name+'" data-id="'+v.id+'" data-canvas="new" data-index="'+i+'">'+v.name+'</div>';
			});
			$('#source').html(content);

			/**drag and drop starts**/
			
			$( ".category" ).draggable({ revert: "invalid",snap:"#target",helper:'clone',cursor:'move'}).resizable();

			/*$("#target").droppable(
			{
				//accept: "#source > .category",
				drop:function(event,ui)
				{
					//$item = ui.draggable;
				    //console.log($item.resizable());
					/*var new_content =  '<div id="'+id+'" class="product_box" data-cat="'+id+'" data-index="'+i+'" style="left:'+left + 'px;top:' + top+'px;height:' + height+'px;width:' + width+'px;position:absolute;background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.3);">'+id+'</div>';
					$('#template_content').text(content);
					imageData={top:$item.css('top'),left:$item.css('left'),height:$category.css('height'),width:$category.css('width'),id:id,cat_id:cat_id};
					//getObjects();
				}
			});*/

  			var items = [];
			$("#target").droppable({

			    drop: function (e, ui) {
			    	$id = $(ui.draggable)[0].id;
			    	var result = jQuery.inArray($id, items);
			    	if(result == -1)
			    	{
			    		items.push($id);
			    		getObjects();
					    if ($(ui.draggable)[0].id != "") {
						    	//console.log($(ui.draggable)[0].id);
							    x = ui.helper.clone();
							    x.draggable({
							    helper: 'ui-resizable-helper',
							    containment: '#target',
							    tolerance: 'fit',
							    stop:function(event,ui)
							    {
							    	getObjects();

							    }
						    });
						    x.find('.ui-resizable-handle').remove();
						    x.resizable({
						    	containment: '#target',
						    	stop:function(event,ui){
						    		getObjects();

						    	}
						    });
						    
						    x.appendTo('#target');
						    ui.helper.remove();
						    //getObjects();

					    }
			    	}
			    	
			    }
			});			
			
			/**drag and drop ends**/
			
		},'json');
}

function getObjects()
{
	var canvas_contents = $('#target').find("div.category");
	//alert("Hello");
	var new_content="";
	$.each(canvas_contents,function(i,value){
		$item=$(value);
		var id = $item.attr('data-cat');
		var cat_id = $item.attr('data-id');
		new_content +=  '<div id="'+$item.attr('data-cat')+'" class="product_box" data-cat="'+$item.attr('data-cat')+'" data-index="'+i+'" style="left:'+$item.css('left') + ';top:' + $item.css('top')+';height:' + $item.css('height')+';width:' + $item.css('width')+';position:absolute;background: none repeat scroll 0% 0% rgba(0, 0, 0, 0.3);">'+$item.attr('data-cat')+'</div>';
		imageData={top:$item.css('top'),left:$item.css('left'),height:$item.css('height'),width:$item.css('width'),id:id,cat_id:cat_id};		
		categories[i]=imageData;
	});
	console.log('categories ',categories);
	$('#template_content').text(new_content);
}

function submitCanvas()
{
	var data = $('#canvas-form').serialize();
	$.post('<?php echo site_url('canvas/admin/save')?>',{canvas:data,categories:categories,bg_image:bg_image},function(data){
			if(data.success)
			{
				$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
				<?php $this->session->set_flashdata('message', lang('message_saved_template'));?>
				window.location.href = '<?php echo site_url('canvas/admin')?>';
			}
			else
			{
				$.messager.show({title: '<?php  echo lang('error')?>',msg: data.msg});
			}
		},'json');
	
}
</script>