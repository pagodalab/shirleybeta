  
  <textarea id="code">
  </textarea>
  <style>
  #draggable, .draggable2 { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 500px; height: 550px; padding: 0.5em; float: left; margin: 10px; }
  </style>
  <script>
  $(function() {

    $( ".draggable2" ).draggable({ revert: "invalid" }).resizable();
 
    $( "#droppable" ).droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      drop: function( event, ui ) {
		  $this=$(this);
        $this
          .addClass( "ui-state-highlight" )
          .find( "p" )
            .html( "Dropped!" );
			$('#code').val($this.height());
      }
    });
  });
  </script>
</head>
<body>

 
<div class="ui-widget-content draggable2" id="cat-1">
  <p>I revert when I'm not dropped</p>
</div>

<div class="ui-widget-content draggable2" id="cat-2">
  <p>I revert when I'm not dropped</p>
</div>


 
<div id="droppable" class="ui-widget-header">
  <p>Drop me here</p>
</div>

