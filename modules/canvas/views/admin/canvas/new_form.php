  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <style>
  	#template{
		float:left;
		/*position:relative;*/
		height:500px;
		width:340px;
		
	}
	#options{
		margin-left:30px;
		padding:0 0 0 20px;
		float:left;
		width:400px;
	}

	.category
	{
		background-color: yellowgreen;
		padding: 40px;
		/*float:left;*/
	}
	 .drag{
		width:100px;
		height:50px;
		padding:10px;
		margin:5px;
		border:1px solid #ccc;
		background:#AACCFF;
	}
	.dp{
		opacity:0.5;
		filter:alpha(opacity=50);
	}
	.over{
		background:#FBEC88;
	}
	
	.cnv-content
	{
		position:relative;
	}
  </style>
<form method="post" action="<?php echo site_url('canvas/admin/save')?>" enctype="multipart/form-data" id="canvas-form">
	<div class="form-contents">
		<fieldset>
			<label for="template_name">Template Title :</label>
			<input type="text" name="template_name" id="template_name" value="<?php echo $canvas['template_name']?>" required="required">
			
			<label for="categories">Categories : </label>
			<select name="categories[]" multiple style="height:200px" id="categories">
                <?php
                    $category_ids = explode(',', $canvas['categories']);
                    foreach($categories as $category){
                ?>
                       <option value="<?php echo $category['id']?>" <?=(in_array($category['id'], $category_ids)?'selected="selected"':'')?>><?php echo $category['name']?></option>
                <?php }?>
            </select>

            <label for="image">Image :</label>
            <label id="upload_image_name" style="display:none"></label>
            <input name="template_image" id="template_image" type="text" style="display:none"/>
            <input type="file" id="upload_image" name="userfile" style="display:block"/>
            <a href="#" id="change-image" title="Delete" style="display:none"><img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a>
			<label id="temp_image"><?php echo $canvas['template_image']?></label>
            
            <label for="status">Status :</label>
			<label class="radio inline"><input type="radio" name="status" id="status1" value="1" <?php echo ($canvas['status'] == 1)?'checked="checked"':''?>> Active  </label>
			<label class="radio inline"><input type="radio" name="status" id="status0" value="0" <?php echo ($canvas['status'] == 0)?'checked="checked"':''?>> Inactive  </label>
			<input type="hidden" name="template_id" id="template_id" value="<?php echo $canvas['template_id']?>">
			<label>Contents : </label>
			<textarea id="template_content" name="template_content" style="width: 1048px; height: 279px;"><?php echo $canvas['template_content']?></textarea>
			<br/>
			<br/>
			
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-success">
							Generate Code
						</a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="content">
                            
                                <div id="target" style="border:1px solid #ccc;width:300px;height:400px;float:left;margin:5px;<?php if($canvas['template_image']){?>background-image:url(<?php echo base_url()?>uploads/canvas/background/<?php echo $canvas['template_image']?>);<?php }?>">
                                	
                                </div>
                                <div id="source" style="border:1px solid #ccc;width:300px;height:400px;float:left;margin:5px;">
                                	
                                </div>
                                <div class="clearfix"></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn">Submit</button>
	    <a href="<?php echo site_url('canvas/admin')?>" class="btn btn-danger">Cancel</a>
	</div>	
</form>

<script type="text/javascript">

$(function() {	
	
	uploadReady();
	
	$("#categories").on('blur',function(){
		var ids = $("#categories").val();
		$.post('<?php echo site_url('canvas/admin/getCategories')?>',{id:ids},function(data){
			//console.log("This is data ",data); //return false;
			var content = '';
			$.each(data,function(i,v){
				content+='<div class="drag" id="'+v.name+'" data-cat="'+v.name+'" data-canvas="new" data-index="'+i+'" style="border:1px dotted #999999; background-color:yellowgreen; padding:25px;">'+v.name+'</div>';
			});
			$('#source').html(content);
			$('.drag').draggable({
					proxy:'clone',
					revert:true,
					cursor:'auto',
					onStartDrag:function(){
					$(this).draggable('options').cursor='not-allowed';
					$(this).draggable('proxy').addClass('dp');
				},
					onStopDrag:function(){
					$(this).draggable('options').cursor='auto';
				}
			}).resizable();
			$('#target').droppable({
					accept:'.drag',
					onDragEnter:function(e,source){
					$(source).draggable('options').cursor='auto';
					$(source).draggable('proxy').css('border','1px solid red');
					$(this).addClass('over');
				},
					onDragLeave:function(e,source){
					$(source).draggable('options').cursor='not-allowed';
					$(source).draggable('proxy').css('border','1px solid #ccc');
					$(this).removeClass('over');
				},
					onDrop:function(e,source){
					$(source).addClass('cnv-content');
					$(this).append(source)
					$(this).removeClass('over');
				}
			});
		},'json');
		
	});

	$('#change-image').on('click',function(){
		$.messager.confirm('Confirm','Are you sure to delete ?',function(r){
			if (r){
				$.post('<?php echo site_url('canvas/admin/upload_delete')?>',{filename:$('#template_image').val()},function(data){
				$('#upload_image_name').html('').hide();
				$('#change-image').hide();
				$('#temp_image').hide();
				$('#upload_image').show();	
				$('#target').css('background','');
				});
			}
		});
	});
	//$(".product_box" ).draggable({ containment: "template" }).resizable({ containment: "template" });

	function uploadReady()
	{
		var template_id = $('#template_id').val();
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('canvas/admin/upload_image?template_id=')?>'+template_id,
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					$.messager.show({title: '<?php  echo lang('error')?>',msg: 'Only JPG, PNG or GIF files are allowed'});
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#template_image').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
					$('#temp_image').html(filename);
					$('#target').css('background-image','url(<?php echo base_url()?>uploads/canvas/background/'+filename+')');
				}
                else
                {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: response.error});                
                }
			}		
		});		
	}

});

</script>