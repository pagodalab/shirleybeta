<?php 
class Canvas extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('canvas/canvas_model');
		$this->load->model('canvas/user_canvas_model');
		$this->load->model('canvas/canvas_product_model');
		$this->load->model('category/category_model');
		$this->load->model('product/product_model');
	}
	
	public function index()
	{
		//get the canvas				
		$data['templates'] = $this->canvas_model->getCanvasTemplates(array('status'=>1))->result_array();
		$this->view('canvas/index', $data);
	}
	
	public function getCanvasContents()
	{
		$template_id = $this->input->post('id');
		$data['template'] = $this->canvas_model->getCanvasTemplates(array('template_id'=>$template_id))->row_array();
		
		$this->load->view('canvas/content', $data);
	}
	
	public function getCanvasCategories()
	{
		$template_id = $this->input->post('id');
		$template = $this->canvas_model->getCanvasTemplates(array('template_id'=>$template_id))->row_array();
		
		$categories = explode(',',$template['categories']);
		foreach($categories as $category_id)
		{
			$category = $this->category_model->getCategories(array('id'=>$category_id))->row_array();
			$canvas_categories[] = $category;
		}
		$data['categories'] = $canvas_categories;
		$this->load->view('canvas/categories', $data);
	}
	
	public function getProducts()
	{
		$category_id = $this->input->post('id');
		
		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
		
		$data['products'] = $this->product_model->getProducts(array('category_id'=>$category_id,'products.enabled'=>1))->result();
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		//echo "<pre>"; print_r($data['products']); exit;
		$this->load->view('canvas/products', $data);
	}
	
	public function save()
	{
		//echo "<pre>"; print_r($_POST);  exit;
		$products=$this->input->post('products');
		$bg_image = $this->input->post('bg_image');
		
		// Load the stamp and the photo to apply the watermark to
		$im = imagecreatefromjpeg($bg_image);
		
		foreach($products as $product)
		{
			//echo "<pre>"; print_r($_POST); 
			$dst = 'uploads/canvas/user_canvas/tests/';
			@mkdir($dst);
			// Set the margins for the stamp and get the height/width of the stamp image
			$top = (int)$product['top'];
			$left = (int)$product['left'];
			$newwidth = (int)$product['width'];
			$newheight = (int)$product['height'];
			
			$file = $product['url'];
			list($width, $height) = getimagesize($file);
			// Load
			$thumb = imagecreatetruecolor($newwidth, $newheight);
			// *** Get extension
			$extension = strtolower(strrchr($file, '.'));
		 	//echo "<pre>"; print_r($extension);
			
			switch($extension)
			{
				case '.jpg':
				case '.jpeg':
					$stamp = @imagecreatefromjpeg($file);
					break;
				case '.png':
					$stamp = @imagecreatefrompng($file);
					break;
				default:
					$stamp = false;
					break;
			}
			// Resize
			imagecopyresized($thumb, $stamp, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			
			// Output
			$thumb_name = uniqid().$extension;
			$thumb_file= $dst.$thumb_name;
			
			// *** Get extension
			$extension = strrchr($thumb_file, '.');
			$extension = strtolower($extension);
		 
			switch($extension)
			{
				case '.jpg':
				case '.jpeg':
						imagejpeg($thumb, $thumb_file, 100);
					break;		 
				case '.png':
					// *** Scale quality from 0-100 to 0-9
					
					$transparent = imagecolorallocatealpha($thumb, 0, 0, 0, 127);

					// Make the background transparent
					//imagecolortransparent($thumb, $transparent);
					imagefill($thumb, 0, 0, $transparent);
					imagepng($thumb, $thumb_file);
					break;
		 
				// ... etc
		 
				default:
					// *** No extension - No save.
					break;
			}
		 
			imagecopy($im, $thumb,$left,$top, 0, 0, imagesx($thumb), imagesy($thumb));
			imagedestroy($thumb);
			$canvas_products[] = $product['id'];

		}
		
		//exit;

		// Output and free memory
		$temp_dir = "uploads/canvas/user_canvas/templates/";
		@mkdir($temp_dir);
		$file_name = uniqid().".jpg";
		$file= $temp_dir.$file_name;
		imagejpeg($im,$file);
		imagedestroy($im);
		
		$serializedData = $this->input->post('canvas');
		$unserializedData = array();

		parse_str($serializedData,$unserializedData);

		//echo "<pre> cats "; print_r($cats);
        $data['template_id'] 		= $unserializedData['template_id'];
		$data['canvas_title'] 		= $unserializedData['canvas_name'];
		$data['created_date'] 		= date('Y-m-d h:i:s');
		$data['user_id']			= $this->go_cart->customer('id');
		$data['canvas_image'] 		= $file_name;
		$data['status'] 			= 1;

		$success=$this->user_canvas_model->insert('USER_CANVAS',$data);
		$user_canvas_id = $this->db->insert_id();
		foreach($canvas_products as $product_id)
		{
			$canvas_product['canvas_id'] = $user_canvas_id;
			$canvas_product['product_id'] = $product_id;
			
			$this->canvas_product_model->insert('CANVAS_PRODUCT',$canvas_product);
		}
		
		if($success)
		{
			$success = TRUE;
			$this->session->set_flashdata('message', lang('message_saved_template'));
		} 
		else
		{
			$success = FALSE;
			$this->session->set_flashdata('message', lang('message_saved_template'));
		}
		 
		 echo json_encode(array('success'=>$success));		
	}
	
	public function detail($id)
	{
		$data['template'] = $this->user_canvas_model->getUserCanvas(array('user_canvas_id'=>$id))->row_array();
		$this->db->group_by('canvas_products.product_id');
		$this->canvas_product_model->joins = array('CATEGORY_PRODUCT','CATEGORY','PRODUCT');
		$data['products'] = $this->canvas_product_model->getCanvasProducts(array('canvas_id'=>$id))->result();
		foreach($data['products'] as $product)
		{
			if($product->images == 'false')
			{
				$product->images = array();
			}
			else
			{
				$product->images	= array_values((array)json_decode($product->images));
			}
		}
		
		$this->view('canvas/detail',$data);
	}
	
	function add_to_wishlist()
	{
		$json = array();
		$wishlists = array();

		$this->load->model('customer/customer_model');
		
		$this->db->where('id',$this->go_cart->customer('id'));
		$customer=$this->customer_model->getCustomers()->row_array();
		
		if($customer['canvas_wishlist'])
		{
			$wishlists = unserialize($customer['canvas_wishlist']);
		
			if(!in_array($this->input->post('canvas_id'),$wishlists))
			{
				array_push($wishlists,$this->input->post('canvas_id'));
			}
		}
		else
		{
			$wishlists[] = $this->input->post('canvas_id');
		}	
		
		
		

		$wishlist=serialize($wishlists);
		$success = $this->customer_model->update('CUSTOMER',array('canvas_wishlist'=>$wishlist),array('id'=>$this->go_cart->customer('id')));
 		
		$json['success']=TRUE;
		//$json['total'] = count($this->go_cart->get_wishlist()) ;
		echo json_encode($json);
		
	}	
}