<?php 

class Admin extends Admin_Controller
{
	protected $uploadPath = 'uploads/canvas/background';
	protected $uploadthumbPath = 'uploads/canvas/background/thumbs';
	
	public function __construct()
	{
		parent::__construct();
		$this->authenticate->check_access('Admin', true);
		$this->load->model('canvas/canvas_model');
		$this->load->model('category/category_model');
		$this->lang->load('canvas');
		$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}
	
	public function index()
	{	
		$data['page_title'] = 'Canvas Templates';
		$data['templates'] = $this->canvas_model->getCanvasTemplates()->result_array();
		$data['module'] = 'canvas';
		
		$this->view($this->config->item('template_admin') . "admin/canvas/index",$data);		
	}
	
	public function json()
	{
		
		//$this->_get_search_param();	
		$this->db->group_by('canvas_templates.template_id desc');	
		$total=$this->canvas_model->count();
		paging('canvas_templates.template_id desc');
		//$this->_get_search_param();
		//$this->canvas_model->joins = array('CATEGORY');
		$this->db->group_by('canvas_templates.template_id');	
		$rows=$this->canvas_model->getCanvasTemplates()->result_array();
		//echo $this->db->last_query(); 
		//echo "<pre>"; print_r($rows); exit; 
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function form($id=NULL)
	{
		$this->bep_assets->load_asset_group('JQUERY_UI');
		$data['categories'] = $this->category_model->getCategories()->result_array();
		$data['canvas'] = NULL;
		if($id)
		{
			$data['canvas'] = $this->canvas_model->getCanvasTemplates(array('template_id'=>$id))->row_array();
		}
		$data['page_title'] = 'Canvas Form';
		$data['module'] = 'canvas';
		
		$this->view($this->config->item('template_admin') . "admin/canvas/form",$data);	
		
	}
	
	public function save()
	{
		$categories=$this->input->post('categories');
		$bg_image = $this->input->post('bg_image');
		
		// Load the stamp and the photo to apply the watermark to
		$im = imagecreatefromjpeg($bg_image);
		
		foreach($categories as $category)
		{
			
			$dst = 'uploads/canvas/tests/';
			@mkdir($dst);
			// Set the margins for the stamp and get the height/width of the stamp image
			$top = (int)$category['top'];
			$left = (int)$category['left'];
			$sx = (int)$category['width'] + 50;
			$sy = (int)$category['height'] + 50;
			
			// Copy the stamp image onto our photo using the margin offsets and the photo 
			// width to calculate positioning of the stamp. 
			
			$img = imagecreatetruecolor($sx,$sy);
			
			// Set the border and fill colors
			$border = imagecolorallocate($img, 200, 20, 150);
			$trans_colour = imagecolorallocatealpha($img, 200, 200, 200,60);
			//imagefill($img, 0, 0, $trans_colour);
			
			imagefilltoborder($img, 0, 0, $border,$trans_colour);
			
			$textcolor = imagecolorallocate($img, 50, 50, 50);
			// Write the string at the top left
			imagestring($img, 5, 15, 10, $category['id'], $textcolor);
			imagesavealpha($img, true);
			$image = $dst.uniqid().".png";
			imagepng($img,$image);
			imagedestroy($img);
			$stamp = imagecreatefrompng($image);
			imagecopy($im, $stamp,$left,$top, 0, 0,imagesx($stamp), imagesy($stamp));

			$canvas_categories[] = $category['cat_id'];

		}

		// Output and free memory
		$temp_dir = "uploads/canvas/templates/";
		@mkdir($temp_dir);
		$file_name = uniqid().".jpg";
		$file= $temp_dir.$file_name;
		imagejpeg($im,$file);
		imagedestroy($im);
		
		$serializedData = $this->input->post('canvas');
		$unserializedData = array();

		parse_str($serializedData,$unserializedData);
		
		//*template image upload config*//
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types']	= 'gif|jpg|png';
		$config['max_size']			= 10024;
		$this->load->library('upload', $config);

        $code = 'CTMP-'.rand(1000,9999);
        $categories = $canvas_categories;
		//echo "<pre> categories "; print_r($categories);
        if(is_array($categories))
        {
        	$cats = implode(",", $categories);
        }
		//echo "<pre> cats "; print_r($cats);
        $data['template_id'] 		= $unserializedData['template_id'];
		$data['template_name'] 		= $unserializedData['template_name'];
		$data['template_image'] 	= $unserializedData['template_image'];
		$data['image'] 				= $file_name;
		$data['categories'] 		= $cats;
		$data['template_content'] 	= $unserializedData['template_content'];
		$data['template_code'] 		= ($unserializedData['template_code'])?$unserializedData['template_code']:$code ;
		$data['status'] 			= $unserializedData['status'];

		//echo "<pre>"; print_r($data); exit;

        if(!$unserializedData['template_id'])
        {
			$data['added_date'] =date('Y-m-d H:i:s');		
            $success=$this->canvas_model->insert('CANVAS_TEMPLATE',$data);
        }
        else
        {
			$data['modified_date'] =date('Y-m-d H:i:s');			
            $success=$this->canvas_model->update('CANVAS_TEMPLATE',$data,array('template_id'=>$data['template_id']));
        }
			
		
        //echo $success;
		/*if($success)
		{
			$this->session->set_flashdata('message', lang('message_saved_template'));
		} 
		else
		{
			$this->session->set_flashdata('message', lang('message_failure'));
		}*/
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	

   	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$canvas = $this->canvas_model->getCanvasTemplates(array('template_id'=>$row))->row_array();
				//$this->user_canvas_model->delete('USER_CANVAS',array('template_id'=>$row));
				$this->canvas_model->delete('CANVAS_TEMPLATE',array('template_id'=>$row));
				@unlink('uploads/canvas/'.$canvas['template_image']);
            endforeach;
		}
	}  

	public function getCategories()
	{
		$categories = $this->input->post('id');
		if(is_array($categories))
		{
			foreach ($categories as $category)
			{
				$category_name[] = $this->category_model->getCategories(array('id'=>$category))->row_array();
			}
		}
		//$data['categories'] = $category_name;
		//$data['module'] = 'canvas';
		
		//$this->load->view("admin/canvas/template",$data);
		echo json_encode($category_name);	
	}

	function upload_image()
	{
		$temp_id = $this->input->get('template_id');

		if($temp_id)
		{
			$template = $this->canvas_model->getCanvasTemplates(array('template_id'=>$temp_id))->row_array();
			@unlink($this->uploadPath . '/' . $template['template_image']);
			@unlink($this->uploadthumbPath . '/' . $template['template_image']);
		}

		//Image Upload Config
		@mkdir($this->uploadPath);
		@mkdir($this->uploadthumbPath);

		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
		  $this->load->library('image_lib');
		  
		 /********for thumb********/
			$config['image_library'] = 'gd2';
			$config['source_image'] = $data['full_path'];
			$config['maintain_ratio'] = FALSE;
			list($width, $height, $type, $attr) = getimagesize($data['full_path']);
			$config['new_image'] = $this->uploadthumbPath;
			$config['x_axis'] = '20';
			$config['y_axis'] = '20';
			$config['height'] = 150;
			$config['width'] = 150;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();
		  
		  echo json_encode($data);
	    }
	}
    
    function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
		@unlink($this->uploadthumbPath . '/' . $filename);
	} 
   	
	function testdata()
	{
		$this->bep_assets->load_asset_group('JQUERY_UI');
		$this->view($this->config->item('template_admin') . "admin/canvas/test");
	}
	
}
?>