<?php

class Occasion_product extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('occasion_product','occasion_product_model');
        $this->lang->module_load('occasion_product','occasion_product');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'occasion_product';
		$data['page'] = $this->config->item('template_admin') . "occasion_product/index";
		$data['module'] = 'occasion_product';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->occasion_product_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->occasion_product_model->getOccasionProducts()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['occasion_id']!='')?$this->db->where('occasion_id',$params['search']['occasion_id']):'';
($params['search']['product_id']!='')?$this->db->where('product_id',$params['search']['product_id']):'';
($params['search']['sort_order']!='')?$this->db->where('sort_order',$params['search']['sort_order']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->occasion_product_model->getOccasionProducts()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->occasion_product_model->delete('OCCASION_PRODUCTS',array(''=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post(''))
        {
            $success=$this->occasion_product_model->insert('OCCASION_PRODUCTS',$data);
        }
        else
        {
            $success=$this->occasion_product_model->update('OCCASION_PRODUCTS',$data,array(''=>$data['']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['occasion_id'] = $this->input->post('occasion_id');
$data['product_id'] = $this->input->post('product_id');
$data['sort_order'] = $this->input->post('sort_order');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}