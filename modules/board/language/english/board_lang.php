<?php


$lang['board_id'] = 'Board Id';
$lang['description'] = 'Description';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['status'] = 'Status';

$lang['create_board']='Create Board';
$lang['edit_board']='Edit Board';
$lang['delete_board']='Delete Board';
$lang['board_search']='Board Search';

$lang['board']='Board';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

