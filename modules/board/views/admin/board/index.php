<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('board_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="board-search-form">
<table width="100%">
<tr><td><label><?=lang('start_date')?>:</label></td>
<td><input type="text" name="date[start_date][from]" id="search_start_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[start_date][to]" id="search_start_date_to"  class="easyui-datebox"/></td>
<td><label><?=lang('status')?>:</label></td>
<td><label class="radio inline"><input type="radio" name="search[status]" id="search_status1" value="1"/><?=lang('general_yes')?></label>
	<label class="radio inline"><input type="radio" name="search[status]" id="search_status0" value="0"/><?=lang('general_no')?></label></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?php  echo lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="board-table" data-options="pagination:true,title:'<?php  echo lang('board')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="board_id" sortable="true" width="30"><?=lang('board_id')?></th>
<th field="start_date" sortable="true" width="50"><?=lang('start_date')?></th>
<th field="end_date" sortable="true" width="50"><?=lang('end_date')?></th>
<th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?=lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Create Board"><span><i class="icon-plus"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Delete Board"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit board form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-board" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?=lang('description')?>:</label></td>
					  <td width="66%"><textarea name="description" id="description" class="easyui-validatebox class redactor" required="true" style="width:300px;height:100px"></textarea></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('start_date')?>:</label></td>
					  <td width="66%"><input name="start_date" id="start_date" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('end_date')?>:</label></td>
					  <td width="66%"><input name="end_date" id="end_date" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('status')?>:</label></td>
					  <td width="66%"><label class="radio inline"><input type="radio" value="1" name="status" id="status1" /><?=lang("general_yes")?></label> <label class="radio inline"><input type="radio" value="0" name="status" id="status0" /><?=lang("general_no")?></label></td>
		       </tr><input type="hidden" name="board_id" id="board_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->
   

<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#board-search-form').form('clear');
			$('#board-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#board-table').datagrid({
				queryParams:{data:$('#board-search-form').serialize()}
				});
		});		
		$('#board-table').datagrid({
			url:'<?php  echo site_url('board/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="Edit Board"><span><i class="icon-pencil"></i></span></a>';
		var d = '<a href="#" onclick="removeboard('+index+')" class="tooltip-top btn btn-small" title="Delete Board"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-board').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_board')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#board-table').datagrid('getRows')[index];
		if (row){
			$('#form-board').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_board')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeboard(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#board-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('board/admin/delete_json')?>', {id:[row.board_id]}, function(){
					$('#board-table').datagrid('deleteRow', index);
					$('#board-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#board-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].board_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('board/admin/delete_json')?>',{id:selected},function(data){
						$('#board-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-board').form('submit',{
			url: '<?php  echo site_url('board/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-board').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#board-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>