<?php


$lang['currency_id'] = 'Currency Id';
$lang['currency_name'] = 'Currency Name';
$lang['currency_code'] = 'Currency Code';
$lang['symbol'] = 'Symbol';
$lang['default'] = 'Default';
$lang['added_date'] = 'Added Date';
$lang['modified_date'] = 'Modified Date';
$lang['status'] = 'Status';

$lang['create_currency']='Create Currency';
$lang['edit_currency']='Edit Currency';
$lang['delete_currency']='Delete Currency';
$lang['currency_search']='Currency Search';

$lang['currency']='Currency';
$lang['value'] = 'Value';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

