<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('currency_search')?>',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px;width:1088px">
<form action="" method="post" id="currency-search-form">
<table width="100%">
<tr><td><label><?=lang('currency_name')?>:</label></td>
<td><input type="text" name="search[currency_name]" id="search_currency_name"  class="easyui-validatebox"/></td>
<td><label><?=lang('currency_code')?>:</label></td>
<td><input type="text" name="search[currency_code]" id="search_currency_code"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?php  echo lang('search')?></a>
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="currency-table" data-options="pagination:true,title:'<?php  echo lang('currency')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="currency_id" sortable="true" width="30"><?=lang('currency_id')?></th>
<th field="currency_name" sortable="true" width="50"><?=lang('currency_name')?></th>
<th field="currency_code" sortable="true" width="50"><?=lang('currency_code')?></th>
<th field="symbol" sortable="true" width="50"><?=lang('symbol')?></th>
<th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?=lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Create Currency"><span><i class="icon-plus"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Currency"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div>

<!--for create and edit currency form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-currency" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?=lang('currency_name')?>:</label></td>
					  <td width="66%"><input type="text" name="currency_name" id="currency_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('currency_code')?>:</label></td>
					  <td width="66%"><input type="text" name="currency_code" id="currency_code" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('symbol')?>:</label></td>
					  <td width="66%"><input type="text" name="symbol" id="symbol" class="easyui-validatebox" required="true"></td>
		       </tr>

               <tr>
		              <td width="34%" ><label><?=lang('value')?>:</label></td>
					  <td width="66%"><input type="text" name="value" id="value" class="easyui-validatebox" required="true"></td>
		       </tr>

               <tr>
		              <td width="34%" ><label><?=lang('default')?>:</label></td>
                      <td width="66%"><input type="checkbox" name="default" id="default" value="1"></td>
		       </tr>

               <tr>
		              <td width="34%" ><label><?=lang('status')?>:</label></td>
					  <td width="66%"><label class="radio inline"><input type="radio" value="1" name="status" id="status1" /><?=lang("general_yes")?></label> <label class="radio inline"><input type="radio" value="0" name="status" id="status0" /><?=lang("general_no")?></label></td>
		       </tr><input type="hidden" name="currency_id" id="currency_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
	</div>
</div>
<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#currency-search-form').form('clear');
			$('#currency-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#currency-table').datagrid({
				queryParams:{data:$('#currency-search-form').serialize()}
				});
		});
		$('#currency-table').datagrid({
			url:'<?php  echo site_url('currency/admin/json')?>',
			height:'auto',
			width:'1088',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="Edit Currency"><span><i class="icon-pencil"></i></span></a>';
		var d = '';
	if(row.delete_not_allowed=='0'){
		'<a href="#" onclick="removecurrency('+index+')" class="tooltip-top btn btn-small" title="Remove Currency"><span><i class="icon-trash"></i></span> </a>';
	}
		return e+d;
	}

	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-currency').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_currency')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}

	function edit(index)
	{
		var row = $('#currency-table').datagrid('getRows')[index];
		if (row){
			$('#form-currency').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_currency')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');
		}
	}


	function removecurrency(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#currency-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('currency/admin/delete_json')?>', {id:[row.currency_id]}, function(){
					$('#currency-table').datagrid('deleteRow', index);
					$('#currency-table').datagrid('reload');
				});

			}
		});
	}

	function removeSelected()
	{
		var rows=$('#currency-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].currency_id);
			}

			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){
					$.post('<?php  echo site_url('currency/admin/delete_json')?>',{id:selected},function(data){
						$('#currency-table').datagrid('reload');
					});
				}

			});

		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');
		}

	}

	function save()
	{
		$('#form-currency').form('submit',{
			url: '<?php  echo site_url('currency/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-currency').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: 'Success',msg: result.msg});
					$('#currency-table').datagrid('reload');	// reload the user data
				}
				else
				{
					$.messager.show({title: 'Error',msg: result.msg});
				} //if close
			}//success close

		});

	}


</script>