<?php

class Admin extends Admin_Controller
{

	public function __construct(){
    	parent::__construct();
        $this->load->model('currency/currency_model');
        $this->lang->load('currency/currency');
    }

	public function index()
	{
		// Display Page
		$data['header'] = 'Currency';
		$data['page_title'] = 'Currency';
		$this->view($this->config->item('admin_folder').'/currency/index', $data);
	}

	public function json()
	{
		$this->_get_search_param();
		$total=$this->currency_model->count();
		paging('currency_id');
		$this->_get_search_param();
		$rows=$this->currency_model->getCurrencies()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['currency_name']!='')?$this->db->like('currency_name',$params['search']['currency_name']):'';
			($params['search']['currency_code']!='')?$this->db->like('currency_code',$params['search']['currency_code']):'';
		}
	}

	public function combo_json()
    {
		$rows=$this->currency_model->getCurrencies()->result_array();
		echo json_encode($rows);
    }

	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->db->where('delete_not_allowed',0);
				$this->currency_model->delete('CURRENCIES',array('currency_id'=>$row));
            endforeach;
		}
	}

	public function save()
	{

        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('currency_id'))
        {
			$data['added_date']=date('Y-m-d H:i:s');
            $success=$this->currency_model->insert('CURRENCIES',$data);
            $currency_id = $this->db->insert_id();
        }
        else
        {
			$data['added_date']=date('Y-m-d H:i:s');
            $success=$this->currency_model->update('CURRENCIES',$data,array('currency_id'=>$data['currency_id']));
            $currency_id = $data['currency_id'];
        }

		if($success)
		{
			if($data['default'] == 1) {
                $this->currency_model->update('CURRENCIES',array('default'=>0),'currency_id <> '.$currency_id);
            }
            $success = TRUE;
			$msg=lang('success_message');
		}
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));

	}

   private function _get_posted_data()
   {
   		$data=array();
        $data['currency_id'] = $this->input->post('currency_id');
		$data['currency_name'] = $this->input->post('currency_name');
		$data['currency_code'] = $this->input->post('currency_code');
		$data['symbol'] = $this->input->post('symbol');
		$data['status'] = $this->input->post('status');
        $data['value'] = $this->input->post('value');
        $data['default'] = $this->input->post('default');

        return $data;
   }



}