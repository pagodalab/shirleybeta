<?php

class Material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('material_model');
        $this->lang->load('material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'material';
		//$data['page'] = $this->config->item('template_admin') . "material/index";
		$data['module'] = 'material';
		
		$this->view($this->config->item('template_admin') . "admin/material/index",$data);			
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->material_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->material_model->getMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name_en']!='')?$this->db->like('name_en',$params['search']['name_en']):'';
($params['search']['name_da']!='')?$this->db->like('name_da',$params['search']['name_da']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_model->getMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_model->delete('MATERIALS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->material_model->insert('MATERIALS',$data);
        }
        else
        {
            $success=$this->material_model->update('MATERIALS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
		$data['name_en'] = $this->input->post('name_en');
		$data['name_da'] = $this->input->post('name_da');

        return $data;
   }
   
   	
	    
}