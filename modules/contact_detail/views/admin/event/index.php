<table id="event-table" data-options="pagination:true,title:'Events',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="address" sortable="true" width="80">Address</th>
    <th field="city" sortable="true" width="100">City</th>
    <th field="country" sortable="true" width="80">Country</th>
    <th field="phone" sortable="true" width="100">Phone</th>
    <th field="mobile" sortable="true" width="80">Mobile</th>
    <th field="email" sortable="true" width="100">Email</th>
    <th field="status" sortable="true" width="100" formatter="formatStatus">Status</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="javascript:void(0)" onclick="create()" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Contacts</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <?php /*?><select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a><?php */?>
    </p>

</div> 

<!--for create and edit event form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
         <form id="form-event" method="post" >
          <div id="tt" class="easyui-tabs" style="width:545px;height:auto;">
               <div title="Contact Details" style="padding:20px;">
               	<table>
                         <tr>
                              <td width="34%" ><label>Address:</td>
                              <td width="66%"><input type="text" name="address" id="address" class="easyui-validatebox" required="true"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label>City:</label></td>
                              <td width="66%"><input type="text" name="city" id="city" class="easyui-validatebox" required="true"></td>
                         </tr>
                          <tr>
                              <td width="34%" ><label>Country:</label></td>
                              <td width="66%"><input type="text" name="country" id="country" class="easyui-validatebox" required="true"></td>
                         </tr>
                          <tr>
                              <td width="34%" ><label>Phone:</label></td>
                              <td width="66%"><input type="text" name="phone" id="phone" class="easyui-validatebox"></td>
                         </tr>
                          <tr>
                              <td width="34%" ><label>Mobile:</label></td>
                              <td width="66%"><input type="text" name="mobile" id="mobile" class="easyui-validatebox"></td>
                         </tr>
                          <tr>
                              <td width="34%" ><label>Email:</label></td>
                              <td width="66%"><input type="text" name="email" id="email" class="easyui-validatebox"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label>Status:</label></td>
                              <td width="66%">
                              	<label><input type="radio" name="status" id="status1" required="true" value="1"> Active </label>
                              	<label><input type="radio" name="status" id="status0" required="true" value="0"> InActive </label>
                              </td>
                         </tr>
                              <input type="hidden" name="id" id="id"/>
                    </table>
               </div>
            
          </div>
   		</form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->     

<script language="javascript" type="text/javascript">
$(function(){
	
		$('#event-table').datagrid({
			url:'<?=site_url('contact_detail/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	
	});
	function create(){
		//Create code here
		$('#categories').html('');
		$('#form-event').form('clear');
	
		$('#dlg').window('open').window('setTitle','Add Contacts');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		$('#categories').html('');
		var row = $('#event-table').datagrid('getRows')[index];
		if (row){
			$('#form-event').form('load',row);
		
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_event')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_event')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';

		var d = '<a href="#" onclick="removeOccasion('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_event')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;
	}
	
	function save()
	{
		$('#form-event').form('submit',{
			url: '<?php  echo site_url('contact_detail/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-event').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#event-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	

</script>
