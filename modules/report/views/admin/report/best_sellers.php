<table class="table table-striped" id="table-best_seller" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th><?php echo lang('sku');?></th>
			<th><?php echo lang('name');?></th>
			<th><?php echo lang('quantity');?></th>
			<th><?php echo lang('product_frequency');?></th>
		</tr>
	</thead>
	<tbody>

		<?php foreach($best_sellers as $b):?>
			<tr>
				<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
				<td><?php echo  $b->sku; ?></td>

				<td><?php echo  @$b->name_en; ?></td>
				<td><?php echo $b->best_selling; ?></a></td>
				<td><?php echo $b->best_frequency; ?></a></td>
			</tr>
		<?php endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th><?php echo lang('sku');?></th>
			<th><?php echo lang('name');?></th>
			<th><?php echo lang('quantity');?></th>
			<th><?php echo lang('product_frequency');?></th>
		</tr>
	</tfoot>
</table>



<script type="text/javascript">
	$('#table-best_seller').DataTable({
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};

			total = api
			.column( 3 )
			.data()
			.reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );

			/*$.each(result.array_total, function(a,b){
				total = api
				.column( b )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );*/
                                /*

                                pageTotal = api
                                .column( b, { page: 'current'} )
                                .data()
                                .reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0 );*/

                                $( api.column( 3 ).footer() ).html(
                                	total
                                	);
                            });

		},
		"dom": '<"top"f>t<"clear">',
		"bPaginate": false,
	});
</script>