<?php

class Admin extends Admin_Controller {

	//this is used when editing or adding a customer
	var $customer_id	= false;	

	function __construct()
	{		
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		
		$this->load->model('order/order_model');
		$this->load->model('Search_model');
		$this->load->helper(array('formatting'));
		
		$this->lang->load('report');
	}
	
	function index()
	{
		$data['page_title']	= lang('reports');
		$data['years']		= $this->order_model->get_sales_years();
		$this->view($this->config->item('admin_folder').'/report/index', $data);
	}
	
	function best_sellers()
	{
		$start	= $this->input->post('start');
		$end	= $this->input->post('end');

		$data['best_sellers']	= $this->order_model->get_best_sellers($start, $end);
		
		$this->load->view($this->config->item('admin_folder').'/report/best_sellers', $data);	
	}
	
	function sales()
	{
		$year			= $this->input->post('year');
		$data['orders']	= $this->order_model->get_gross_monthly_sales($year);
		$this->load->view($this->config->item('admin_folder').'/report/sales', $data);	
	}

}