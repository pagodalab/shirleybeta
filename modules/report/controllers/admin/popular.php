<?php

class Popular extends Admin_Controller {

	//this is used when editing or adding a customer
	var $customer_id	= false;	

	function __construct()
	{		
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		
		$this->load->model('product/product_model');
		$this->load->helper(array('formatting'));
		
		$this->lang->load('report');
	}
	
	public function index()
	{
		echo "Popular";
	}
}