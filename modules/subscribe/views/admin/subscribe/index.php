<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('subscribe_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="subscribe-search-form">
				<table width="100%">
					<tr>
						<td><label><?php echo lang('email')?>:</label></td>
						<td><input type="text" name="search[email]" id="search_email"  class="easyui-validatebox"/></td>
						<td><label><?php echo lang('status')?>:</label></td>
						<td><input type="radio" name="search[status]" id="search_status1" value="1"/><?php echo lang('general_yes')?>
							<input type="radio" name="search[status]" id="search_status0" value="0"/><?php echo lang('general_no')?></td>
						</tr>
						<tr>
						</tr>
						<tr>
							<td colspan="4">
								<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
								<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
							</td>
						</tr>
					</table>

				</form>
			</div>
			<br/>
			<table id="subscribe-table" data-options="pagination:true,title:'<?php  echo lang('subscribe')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
				<thead>
					<th data-options="field:'checkbox',checkbox:true"></th>
					<th data-options="field:'sub_id',sortable:true" width="30"><?php echo lang('sub_id')?></th>
					<th data-options="field:'email',sortable:true" width="50"><?php echo lang('email')?></th>
					<th data-options="field:'status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('status')?></th>

					<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
				</thead>
			</table>

			<div id="toolbar" style="padding:5px;height:auto">
				<p>
					<a href="#" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_subscribe')?>"><span><i class="icon-plus"></i></span><?php  echo "Create"//lang('create')?></a>
					<a href="#" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_subscribe')?>"><span><i class="icon-trash"></i></span><?php  echo "Delete"//lang('remove_selected')?></a>
				</p>

			</div> 

			<!--for create and edit subscribe form-->
			<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
			data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
			<form id="form-subscribe" method="post" >
				<table>
					<tr>
						<td width="34%" ><label><?php echo lang('email')?>:</label></td>
						<td width="66%"><input name="email" id="email" class="easyui-validatebox" required="true"></td>
					</tr><tr>
					<td width="34%" ><label><?php echo lang('status')?>:</label></td>
					<td width="66%"><input type="radio" value="1" name="status" id="status1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="status" id="status0" /><?php echo lang("general_no")?></td>
				</tr><input type="hidden" name="sub_id" id="sub_id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
			<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#subscribe-search-form').form('clear');
			$('#subscribe-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#subscribe-table').datagrid({
				queryParams:{data:$('#subscribe-search-form').serialize()}
			});
		});		
		$('#subscribe-table').datagrid({
			url:'<?php  echo site_url('subscribe/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_subscribe')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removesubscribe('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_subscribe')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-subscribe').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_subscribe')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#subscribe-table').datagrid('getRows')[index];
		if (row){
			$('#form-subscribe').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_subscribe')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	

	function removesubscribe(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#subscribe-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('subscribe/admin/delete_json')?>', {id:[row.sub_id]}, function(){
					$('#subscribe-table').datagrid('deleteRow', index);
					$('#subscribe-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#subscribe-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].sub_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('subscribe/admin/delete_json')?>',{id:selected},function(data){
						$('#subscribe-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-subscribe').form('submit',{
			url: '<?php  echo site_url('subscribe/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-subscribe').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#subscribe-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close

		});		
		
	}
	
	
</script>