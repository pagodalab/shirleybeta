<?php

class Subscribe extends Admin_Controller
{
	function __construct(){
    	parent::__construct();
        $this->load->model('subscribe/subscribe_model');
        $this->lang->load('subscribe');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'subscribe';
		$data['page'] = $this->config->item('template_admin') . "subscribe/index";
		$data['module'] = 'subscribe';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->subscribe_model->count();
		paging('sub_id');
		$this->_get_search_param();	
		$rows=$this->subscribe_model->getSubscribes()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}
		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->subscribe_model->getSubscribes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->subscribe_model->delete('SUBSCRIBES',array('sub_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('sub_id'))
        {
            $success=$this->subscribe_model->insert('SUBSCRIBES',$data);
        }
        else
        {
            $success=$this->subscribe_model->update('SUBSCRIBES',$data,array('sub_id'=>$data['sub_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['sub_id'] = $this->input->post('sub_id');
$data['email'] = $this->input->post('email');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}