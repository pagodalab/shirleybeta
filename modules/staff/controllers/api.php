<?php 

class Api extends API_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('staff/staff_order_lib');
		$this->load->model('staff/staff_order_model');
		$this->load->library('Authenticate');
	}
	
	public function order_get()
	{
		$order_id = $this->get('o_id');
		$staff_id = $this->get('id');
		$where = array('staff_id'=>$staff_id);
		if(!empty($order_id))
		{
			$where = array('staff_id'=>$staff_id,'order_id'=>$order_id);
		}
		
		$orders = $this->staff_order_lib->getStaffOrders($where);
		//echo "<pre>"; print_r($orders);
		$this->response(array('staffs'=>$orders),200);
	}
	
	
	public function update_order_post()
	{

		$data['order_id'] = $this->post('order_id');
		$data['is_delivered'] = $this->post('is_delivered');
		$access = $this->post('access_token');

         $staff_access = $this->_has_access($access);
	   // echo "<pre> staff_access "; print_r($staff_access); exit;
	  
		if(empty($data['order_id']) || $data['order_id'] == NULL)
		{
			//return false;
			$this->response(array('success'=>FALSE),200);
		}
		else
		{
			$this->staff_order_lib->changeOrderStatus($data);
		
			$this->response(array('success'=>TRUE,'logout'=>false),200);
		}
	  
	}

	private function _has_access($access)
	{
		 $ac = $this->authenticate->has_api_access($access);
		  //echo "<pre> staff_access "; print_r($ac);// exit;
		 if(!$ac)
		 {
			$this->response(array('success'=>false,'logout'=>true),200); 
			exit;
		}
		
		
	}
	
}