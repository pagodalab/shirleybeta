<?php

class Admin extends Admin_Controller {	

	protected $uploadPath = 'uploads/order';
	function __construct()
	{		
		parent::__construct();

		$this->load->model('Admin_model');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->helper(array('formatting'));
		$this->lang->load('order');
		$this->load->model('staff/staff_order_model');
	}
	
	public function index($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		
		$data['page_title'] = 'Orders';
		$data['module'] = 'staff';
		$this->view($this->config->item('template_admin') . "admin/staff/index",$data);
	}
	
	public function json()
	{
		$user = $this->session->userdata('admin');
		$where = array('staff_id'=>$user['id']);
		$this->staff_order_model->joins = array('ORDER','ADMIN');
		$rows = $this->staff_order_model->getStaffOrders($where)->result_array();
		//paging('order_id desc');
		$total = count($rows);
		//echo "<pre>"; print_r($rows); exit;
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function changeStatus()
	{
		//$this->authenticate->is_logged_in();
		$data['order_id']		= $this->input->post('order_id');
		$data['is_delivered']	= $this->input->post('is_delivered');
		$data['delivered_date']	= $this->input->post('delivered_date');
		
		$this->staff_order_model->save($data);
		$success = TRUE;
		echo json_encode(array('success'=>$success));

	}
	
	public function change_status()
	{
		//$this->authenticate->is_logged_in();
		$order_id		= $this->input->post('id');
		
		if(is_array($order_id))
		{
			foreach($order_id as $order)
			{
				$data['order_id'] = $order;
				$data['is_delivered']	= $this->input->post('enabled');
				$data['delivered_date']	= date('Y-m-d');
				
								
				$this->staff_order_model->save($data);
			}
		}
		
		$success = TRUE;
		echo json_encode(array('success'=>$success));

	}
    
	public function bulk_delete()
	{
		$orders	= $this->input->post('order');
	
		if($orders)
		{
			foreach($orders as $order)
			{
				$this->order_model->delete($order);
			}
			$this->session->set_flashdata('message', lang('message_orders_deleted'));
		}
		else
		{
			$this->session->set_flashdata('error', lang('error_no_orders_selected'));
		}
		//redirect as to change the url
		redirect(site_url('order/admin'));	
	}
	
	

}