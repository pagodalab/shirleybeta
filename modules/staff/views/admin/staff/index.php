<?php /*?><div id="search-panel" class="easyui-panel" data-options="title:'order Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
     <form action="" method="post" id="order-search-form">
          <table width="100%">
               <tr><td><label>Order no.</label>:</td>
                    <td><input name="search[order_id]" id="search_name"  class="easyui-validatebox"/></td>
                    <td><label>Status</label>:</td>
                    <td>
                         <select type="text" name="search[is_delivered]" id="search_is_delivered"  class="easyui-combobox"/>
                              <option value=""></option>
                            <option value="1">Delivered</option>
                            <option value="0">Not Delivered</option>
                        </select>
                    </td>
               </tr>
               <tr>
               </tr>
                 <tr>
                   <td colspan="4">
                   <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
                   <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
                   </td>
               </tr>
          </table>
     </form>
</div>

<br/>

<?php */?>


<table id="order-table" data-options="pagination:true,title:'Orders',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="id" sortable="true" width="30">order Id</th><?php */?>
    <th field="order_number" sortable="true" width="100">Order Number</th>
    <th field="bill_firstname" sortable="true" width="100" formatter="formatBilling">Billing Detail</th>
    <th field="ship_firstname" sortable="true" width="100" formatter="formatShipping">Shipping Detail</th>
    <th field="total" sortable="true" width="30" align="center">Total</th>
    <th field="is_delivered" sortable="true" width="50" align="center" formatter="formatStatus">Status</th>
    <th field="shipping_notes" sortable="true" width="50" align="center">Notes</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>
<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Delivered</option>
        <option value="0">Not Delivered</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a>
    </p>
    
<!--for create and edit country form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-order" method="post" >
         <table>
              	  <tr>
                   <td width="34%" ><label>Order Number : </label></td>
                      <td width="66%"><input name="order_number" id="order_number" readonly="true"></td>
                 </tr><tr>
                        <td width="34%" ><label>Delivery Status : </label></td>
                        <td width="66%">
                           <select name="is_delivered" id="is_delivered" class="easyui-combobox" required="true">
                              <option value="1">Delivered</option>
                              <option value="0">Not Delivered</option>
                           </select>
                        </td>
                 </tr>
                 <input type="hidden" name="order_id" id="order_id"/>
         </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->

</div> 

<script language="javascript" type="text/javascript">

	$(function(){
					
		/*$('#clear').click(function(){
			$('#order-search-form').form('clear');
			$('#order-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#order-table').datagrid({
				queryParams:{data:$('#order-search-form').serialize()}
				});
		});		*/
		$('#order-table').datagrid({
			url:'<?=site_url('staff/admin/json')?>',
			height:'auto',
			width:'auto',
			
		});
	
	});
	

	
	function formatBilling(value,row,index)
	{
		if(row !== '')
		{
			var detail = '<p>'+row.bill_firstname+' '+row.bill_firstname+'<br/>'+row.bill_email+'<br/>'+row.bill_address1+', '+row.bill_phone+'</p>';
		}
		else
		{
			var detail = 'Not Available!';
		}
		return detail;
	}
	
	function formatShipping(value,row,index)
	{
		if(row !== null)
		{
			var detail = '<p>'+row.ship_firstname+' '+row.ship_firstname+'<br/>'+row.ship_email+'<br/>'+row.ship_address1+', '+row.ship_phone+'</p>';
		}
		else
		{
			var detail = 'Not Available!';
		}
		return detail;
	}
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?=lang('edit_order')?>"><span><i class="icon-pencil"></i></span> </a>';
		
		return e;		
	}
	
	function edit(index)
	{
		var row = $('#order-table').datagrid('getRows')[index];
		if (row){
			$('#form-order').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','Change Order Status');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	function save()
	{
		$('#form-order').form('submit',{
			url: '<?php  echo site_url('staff/admin/changeStatus')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-order').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: 'Success',msg: "Success"});
					$('#order-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: 'Error',msg: "Error"});
				} //if close
			}//success close
		
		});		
		
	}
	
	function changeSelected()
	{
		var status = $('#enabled').combobox('getValue');
		var rows=$('#order-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to Change?',function(r){
				if(r){				
					$.post('<?=site_url('staff/admin/change_status')?>',{id:selected,enabled:status},function(data){
						$.messager.show({title: 'Success',msg: "Success"});
						$('#order-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No order Selected!');	
		}	
	}
	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return "Yes";
		}
		else
		{
			return "No";	
		}
	}
	
</script>