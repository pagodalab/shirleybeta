<?php
Class staff_order_model extends MY_Model
{	
	var $joins=array();
	function __construct()
	{
		
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('STAFF_ORDER'=>$this->prefix.'staff_orders','ORDER'=>$this->prefix.'orders',
							 'ADMIN'=>$this->prefix.'admin','CUSTOMER'=>$this->prefix.'customers');
		$this->_JOINS=array('ORDER'=>array('join_type'=>'INNER','join_field'=>'orders.id=staff_order.order_id',
                                           'select'=>"orders.customer_id,orders.id,concat(orders.ship_firstname,' ',orders.ship_lastname) as customer,orders.ship_address1,orders.ship_phone,orders.latitude,orders.longitude",'alias'=>'orders'),
						'ADMIN'=>array('join_type'=>'INNER','join_field'=>'admins.id=staff_order.staff_id',
                                           'select'=>"concat(admins.firstname,' ',admins.lastname) as staff_name",'alias'=>'admins'),
						'CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=orders.customer_id',
                                           'select'=>"concat(customers.firstname,' ',customers.lastname) as cust_name",'alias'=>'customers')
                            );        
    }

	
	public function getStaffOrders($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='staff_order.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['STAFF_ORDER']. ' staff_order');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
		
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['ORDER']);
		
		//now delete the order items
		$this->db->where('order_id', $id);
		$this->db->delete($this->_TABLES['ORDER_ITEM']);
	}
	
	function save($data)
	{

		$where = array('staff_order.order_id'=>$data['order_id']);
		
		$result = $this->getStaffOrders($where)->row_array();
		$num = count($result);
		
		if ($num > 0)
		{
			$this->db->where('order_id', $data['order_id']);
			$this->db->update($this->_TABLES['STAFF_ORDER'], $data);
		}
		else
		{
			$this->db->insert($this->_TABLES['STAFF_ORDER'], $data);
			
		}
		
		//echo $this->db->last_query();
		
		return TRUE;

	}
	
	
	function changeStaffOrderStatus($data)
	{
		$data['delivered_date'] = date('Y-m-d h:i:s');
		$this->db->where('order_id', $data['order_id']);
		$this->db->update($this->_TABLES['STAFF_ORDER'], $data);
	}
	
	
}