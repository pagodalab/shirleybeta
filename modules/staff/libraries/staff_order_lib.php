<?php 

class Staff_order_lib
{
	var $CI;
	
	public function __construct()
	{
		$this->CI=&get_instance();
		$this->CI->load->model('staff/staff_order_model');
		$this->CI->load->model('order/order_model');
	}
	
	public function getStaffOrders($where=NULL)
	{
		$this->CI->staff_order_model->joins = array('ADMIN','ORDER','CUSTOMER');
		$orders = $this->CI->staff_order_model->getStaffOrders($where);
		return $orders->result_array();
	}
	
	public function changeOrderStatus($data)
	{
		$this->CI->staff_order_model->changeStaffOrderStatus($data);
		$this->CI->order_model->updateOrderStatus($data);
	}
	
}