<table id="page-table" title="Pages" rownumbers="true" toolbar="#toolbar" collapsible="true" fitColumns="true">
    <thead>
    <th field="title" sortable="true" width="50" formatter="formatLink">Title</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<?php echo site_url('page/admin/form')?>" class="tooltip-top btn btn-small" title="<?=lang('create_page')?>"><span><i class="icon-plus"></i></span> Add Page</a>	
    <a class="tooltip-top btn btn-small" href="<?php echo site_url('page/admin/link_form'); ?>"><i class="icon-plus-sign"></i> <?php echo lang('add_new_link');?></a>
    </p>

</div> 
<script language="javascript" type="text/javascript">
	$(function(){
		/*$('#clear').click(function(){
			$('#page-search-form').form('clear');
			$('#page-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#page-table').datagrid({
				queryParams:{data:$('#page-search-form').serialize()}
				});
		});		*/

		$('#page-table').treegrid({
			url:'<?=site_url('page/admin/treeview_json')?>',
			height:'auto',
			width:'auto',
			idField:'id',treeField:'title',
		});					
	});

	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('page/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit Page"><span><i class="icon-pencil"></i></span> </a>';
				
		var d = '<a href="#" onclick="removePage()" class="tooltip-top btn btn-small" title="Remove Page"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatLink(value,row)
	{
		return '<a href="<?php echo base_url()?>page/index/'+row.id+'" target="_blank">'+value+'</a>';
	}

	function removePage()
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#page-table').treegrid('getSelected');
				//console.log('row',row); return false;
				$.post('<?php  echo site_url('page/admin/delete_json')?>', {id:row.id}, function(){
					<?php $this->session->set_flashdata('message', lang('message_delete_page'));?>
					$('#page-table').treegrid('reload');
					
				});

			}
		});	
	}
	
	
</script>