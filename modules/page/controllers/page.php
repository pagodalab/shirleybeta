<?php 
class Page extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('custom_order/custom_order_model');
		$this->load->model('custom_order/personal_detail_model');
		$this->load->model('size/size_model');
		$this->load->model('material/material_model');
		$this->load->library('session');
	}
	
	public function index($id)
	{
		//if there is no page id provided redirect to the homepage.
		$data['page']	= $this->page_model->get_page($id);
		
		if($data['page']->parent_id == 0) {
			$this->db->order_by('sequence');
			$data['subpages'] = $this->page_model->getPages(array('parent_id' => $data['page']->id))->result_array();
		}
		// echo "<pre>";
		// print_r($data);exit;

		if(!$data['page'])
		{
			show_404();
		}
		
		$title = "title_".$this->culture_code;

		$this->load->model('page_model');
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;

		$data['page_title']			= $data['page']->$title;
		
		$data['meta']				= $data['page']->meta;
		$data['seo_title']			= (!empty($data['page']->seo_title))?$data['page']->seo_title:$data['page']->$title;
		
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['sizes'] =$this->size_model->getSizes()->result_array();
		$data['materials']=$this->material_model->getMaterials()->result_array();
		$this->view('page/index', $data);
	}

	public function size_guide()
	{	
		$data['page_title']	= 'size-guide';
		$this->view('page/size', $data);
	}
	public function custom_order()
	{
	   //echo '<pre>';  print_r($this->input->post()); exit;
		$data['first_name'] = $this->input->post('first_name');
		$data['last_name'] = $this->input->post('last_name');
		$data['address'] = $this->input->post('address');
		$data['contact'] = $this->input->post('phone');
		$data['email'] = $this->input->post('email');
		$success = $this->personal_detail_model->insert('PERSONAL_DETAIL',$data);
		$personal_id = $this->db->insert_id();
		
		$personal_id = $personal_id;
		$style_name = $this->input->post('style_name');
		$color = $this->input->post('color');
		$material = $this->input->post('material');
		$size = $this->input->post('size');
		
		$i = 0;
		foreach($style_name as $style)
		{
			$data_ = array(
				'personal_id' =>$personal_id,
				'style_name' =>$style,
				'color' => $color[$i],
				'size' =>$size[$i]
			);
			$success = $this->custom_order_model->insert('CUSTOM_ORDER',$data_);
			$i++;
		}
		
		if($success)
		{
			$this->session->set_userdata('msg_customorder','Ordered Place Sucessfully');
		}
		else
		{
			$this->session->set_userdata('msg_customorder','Ordered Place Sucessfully');
		}
		
		redirect($_SERVER['HTTP_REFERER']);
		
	}
}