<?php
class Admin extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		$this->load->model('page/page_model');
		$this->lang->load('page');
	}
		
	function index()
	{
		$data['page_title']	= lang('pages');
		$data['pages']		= $this->page_model->get_pages();
		
		$this->view($this->config->item('admin_folder').'/page/index', $data);
	}
	
	public function json()
	{
		$total = $this->page_model->countPages();
		paging('sequence');
		$rows = $this->page_model->getPages()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	function getTree($id='0')
	{
	   $arr = array();
	
	    //$sql="select * from table_menu where parent_id =". $id;
		//$result=$this->db->query($sql);
		$this->db->select('id,title_en,parent_id');
		$this->db->from('tbl_pages');
		$this->db->where('parent_id',$id);
		$this->db->order_by('sequence');
		$result=$this->db->get();
		 foreach($result->result_array() as $row)
		 {		 
			 $arr[] = array(
			   "id"=>$row['id'],
			   "title" => $row['title_en'],
			   "children" => $this->getTree($row['id'])
		 );
	   }
	   return $arr;
	}
	
	public function treeview_json()
	{		
		echo json_encode($this->getTree());
	}
	
	public function delete_json()
	{
    	$id=$this->input->post('id');
		$this->page_model->delete('PAGE',array('id'=>$id));
		$this->page_model->delete('PAGE',array('parent_id'=>$id));
	}  
	
	
	/********************************************************************
	edit page
	********************************************************************/
	function form($id = false)
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']				= '';
		$data['title_en']		= '';
		$data['title_da']		= '';
		$data['menu_title_en']	= '';
		$data['menu_title_da']	= '';
		$data['slug']			= '';
		$data['sequence']		= 0;
		$data['parent_id']		= 0;
		$data['content_en']		= '';
		$data['content_da']		= '';
		$data['seo_title']		= '';
		$data['meta']			= '';
		
		$data['page_title']		= lang('page_form');
		$data['pages']			= $this->page_model->get_pages();
		
		if($id)
		{
			
			$page			= $this->page_model->get_page($id);

			if(!$page)
			{
				//page does not exist
				$this->session->set_flashdata('error', lang('error_page_not_found'));
				redirect(site_url('page/admin'));
			}
			
			
			//set values to db values
			$data['id']					= $page->id;
			$data['parent_id']			= $page->parent_id;
			$data['title_en']			= $page->title_en;
			$data['title_da']			= $page->title_da;
			$data['menu_title_en']		= $page->menu_title_en;
			$data['menu_title_da']		= $page->menu_title_da;
			$data['sequence']			= $page->sequence;
			$data['content_en']			= $page->content_en;
			$data['content_da']			= $page->content_da;
			$data['seo_title']			= $page->seo_title;
			$data['meta']				= $page->meta;
			$data['slug']				= $page->slug;
			$data['menu_footer'] = $page->menu_footer;
		}
		
		$this->form_validation->set_rules('title_en', 'lang:title', 'trim|required');
		$this->form_validation->set_rules('menu_title_en', 'lang:menu_title', 'trim');
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta', 'trim');
		$this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
		$this->form_validation->set_rules('parent_id', 'lang:parent_id', 'trim|integer');
		$this->form_validation->set_rules('content', 'lang:content', 'trim');
		
		// Validate the form
		if($this->form_validation->run() == false)
		{
			$this->view($this->config->item('admin_folder').'/page/form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('title_en');
			}
			
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');
			if($id)
			{
				$slug		= $this->Routes_model->validate_slug($slug, $page->route_id);
				$route_id	= $page->route_id;
			}
			else
			{
				$slug			= $this->Routes_model->validate_slug($slug);
				$route['slug']	= $slug;	
				$route_id		= $this->Routes_model->save($route);
			}
			
			
			$save = array();
			$save['id']				= $id;
			$save['parent_id']		= $this->input->post('parent_id');
			$save['title_en']		= $this->input->post('title_en');
			$save['title_da']		= $this->input->post('title_da');
			$save['menu_title_en']	= $this->input->post('menu_title_en');
			$save['menu_title_da']	= $this->input->post('menu_title_da'); 
			$save['sequence']		= $this->input->post('sequence');
			$save['content_en']		= $this->input->post('content_en');
			$save['content_da']		= $this->input->post('content_da');
			$save['seo_title']		= $this->input->post('seo_title');
			$save['meta']			= $this->input->post('meta');
// 			$save['menu_header']
            $save['menu_footer']   = $this->input->post('menu_footer');
			$save['route_id']		= $route_id;
			$save['slug']			= $slug;
			
			//set the menu title to the page title if if is empty
			if ($save['menu_title_en'] == '')
			{
				$save['menu_title_en']	= $this->input->post('title_en');
			}
			
			//save the page
			$page_id	= $this->page_model->save($save);
			
			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'page/index/'.$page_id;
			
			$this->Routes_model->save($route);
			
			$this->session->set_flashdata('message', lang('message_saved_page'));
			
			//go back to the page list
			redirect(site_url('page/admin'));
		}
	}
	
	function link_form($id = false)
	{
	
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']			= '';
		$data['title_en']		= '';
		$data['url']		= '';
		$data['new_window']	= false;
		$data['sequence']	= 0;
		$data['parent_id']	= 0;

		
		$data['page_title']	= lang('link_form');
		$data['pages']		= $this->page_model->get_pages();
		if($id)
		{
			$page			= $this->page_model->get_page($id);

			if(!$page)
			{
				//page does not exist
				$this->session->set_flashdata('error', lang('error_link_not_found'));
				redirect(site_url('page/admin'));
			}
			
			
			//set values to db values
			$data['id']			= $page->id;
			$data['parent_id']	= $page->parent_id;
			$data['title_en']	= $page->title_en;
			$data['title_da']	= $page->title_da;
			$data['url']		= $page->url;
			$data['new_window']	= (bool)$page->new_window;
			$data['sequence']	= $page->sequence;
		}
		
		$this->form_validation->set_rules('title_en', 'lang:title', 'trim|required');
		$this->form_validation->set_rules('url', 'lang:url', 'trim|required');
		$this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
		$this->form_validation->set_rules('new_window', 'lang:new_window', 'trim|integer');
		$this->form_validation->set_rules('parent_id', 'lang:parent_id', 'trim|integer');
		
		// Validate the form
		if($this->form_validation->run() == false)
		{
			$this->view($this->config->item('admin_folder').'/page/link_form', $data);
		}
		else
		{	
			$save = array();
			$save['id']				= $id;
			$save['parent_id']		= $this->input->post('parent_id');
			$save['title_en']		= $this->input->post('title_en');
			$save['title_da']		= $this->input->post('title_da');
			$save['menu_title_en']	= $this->input->post('menu_title_en');
			$save['menu_title_da']	= $this->input->post('menu_title_da'); 
			$save['url']			= $this->input->post('url');
			$save['sequence']		= $this->input->post('sequence');
			$save['new_window']		= $this->input->post('new_window');
			
			//save the page
			$this->page_model->save($save);
			
			$this->session->set_flashdata('message', lang('message_saved_link'));
			
			//go back to the page list
			redirect(site_url('page/admin'));
		}
	}
	
	/********************************************************************
	delete page
	********************************************************************/
	function delete($id)
	{
		
		$page	= $this->page_model->get_page($id);
		
		if($page)
		{
			$this->load->model('Routes_model');
			
			$this->Routes_model->delete($page->route_id);
			$this->page_model->delete_page($id);
			$this->session->set_flashdata('message', lang('message_deleted_page'));
		}
		else
		{
			$this->session->set_flashdata('error', lang('error_page_not_found'));
		}
		
		redirect(site_url('page/admin'));
	}
}	