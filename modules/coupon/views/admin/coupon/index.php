<?php /*?><div id="search-panel" class="easyui-panel" data-options="title:'Coupon Search',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="coupon-search-form">
<table width="100%">
<tr><td><label>Coupon Code</label>:</td>
<td><input name="search[code]" id="search_code"  class="easyui-validatebox"/></td>
<td><label>Date</label>:</td>
<td><input type="text" name="date[start_date][from]" id="search_start_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[end_date][to]" id="search_end_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search" iconCls="icon-search"><?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear" iconCls="icon-clear">Clear</a>
    </td>
    </tr>
</table>

</form>
</div><?php */?>
<table id="coupon-table" data-options="pagination:true,title:'coupons',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="code" sortable="true" width="150">Name</th>
    <th field="num_uses" width="50" formatter="formatUsage">Usage</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<?php echo site_url("coupon/admin/form")?>" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add coupon</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<script language="javascript" type="text/javascript">

	$(function(){
			
		/*$('#clear').click(function(){
			$('#coupon-search-form').form('clear');
			$('#coupon-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#coupon-table').datagrid({
				queryParams:{data:$('#coupon-search-form').serialize()}
				});
		});		*/
		$('#coupon-table').datagrid({
			url:'<?=site_url('coupon/admin/json')?>',
			height:'auto',
			width:'auto'

		});
	
	});
	

	
	function formatItem(value,row,index)
	{
		if(row.active == 1)
		{
			return "Yes";	
		}
		else
		{
			return "No";
		}
	}
	
	function formatUsage(value,row,index)
	{
		var uses = '<p>' +row.num_uses + ' / ' + row.max_uses +'<p>';
		return uses;
	}
	
	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('coupon/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit"><span><i class="icon-pencil"></i></span> </a>';
		
		var d = '<a href="#" onclick="removeCoupon('+index+')" class="tooltip-top btn btn-small" title="Delete"><span><i class="icon-trash"></i></span> </a>';
		
		return e+d;		
	}
	
	function removeCoupon(index)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#coupon-table').datagrid('getRows')[index];
				
				$.post('<?php  echo site_url('coupon/admin/delete_json')?>', {id:[row.id]}, function(){
					$('#coupon-table').datagrid('deleteRow', index);
					$('#coupon-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#coupon-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
				if(r){				
					$.post('<?php  echo site_url('coupon/admin/delete_json')?>',{id:selected},function(data){
						$('#coupon-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
</script>
