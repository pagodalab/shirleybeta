<?php 
class Share extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('share_list/share_list_model');
	}
	
	public function wishlist($uniqid)
	{
		//echo "<pre>"; print_r($uniqid); //exit;
		$data['page_title']	= 'Share List';
		
		$this->share_list_model->joins = array('CUSTOMER');
		$where = array('share_unique_id'=>$uniqid);
		$data['share_list'] = $this->share_list_model->getShareLists($where)->row_array();
		
		$this->share_list_model->joins = array('SHARE_LIST_PRODUCT','CUSTOMER','PRODUCT');
		$data['products'] = $this->share_list_model->getShareLists($where)->result_array();
		//echo "<pre>"; print_r($data['products']); exit;

		$this->view('share/index', $data);
	}

}