<?php 

class Vendor extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/brand_model');
	}
	
	public function index()
	{
		
		$data['vendors'] = $this->brand_model->getVendors(null,'brand_id desc')->result();
		$this->view('vendor/index', $data);
	}
}
?>