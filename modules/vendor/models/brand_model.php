<?php

class Brand_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('BRANDS'=>$this->prefix.'BRANDS');
		$this->_JOINS=array('KEY'=>array('join_type'=>'LEFT','join_field'=>'join1.id=join2.id',
                                           'select'=>'field_names','alias'=>'alias_name'),

                            );
    }

    public function getVendors($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='brands.*';

		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;

        $this->db->select($fields);
        $this->db->from($this->_TABLES['BRANDS']. ' BRANDS');

		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;

		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('currency_name','asc');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();
    }

	public function getById($id)
	{
		return $this->getBRANDS(array('currency_id'=>$id))->row_array();
	}
    public function count($where=NULL)
    {

        $this->db->from($this->_TABLES['BRANDS'].' BRANDS');

        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;

       (! is_null($where))?$this->db->where($where):NULL;

        return $this->db->count_all_results();
    }

	public function settings()
	{
		$this->db->select('currency_id,currency_code');
        $result=$this->db->from($this->_TABLES['BRANDS']. ' BRANDS')->order_by('currency_id')->get()->result_array();
		$data=array();
		foreach($result as $record){
			$data[$record['currency_id']]=$record['currency_code'];
		}
		return $data;
	}
}
?>