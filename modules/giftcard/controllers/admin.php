<?php

class Admin extends Admin_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('Settings_model');
		$this->load->model('giftcard/gift_card_model');
		$this->load->model('product/product_model');
		$this->load->model('category/category_products_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->lang->load('giftcard');
	}
	
	function index()
	{
		
		$data['page_title'] = lang('giftcards');
		$data['cards'] = $this->gift_card_model->get_all_new();
		
		$gc_settings = $this->Settings_model->get('gift_cards');
		if(isset($gc_settings['enabled']))
		{
			$data['gift_cards']['enabled'] = $gc_settings['enabled'];
		}
		else
		{
			$data['gift_cards']['enabled'] = false;
		}
		
		$this->view($this->config->item('admin_folder').'/giftcard/index', $data);
		
	}
	
	public function json()
	{
		//$this->_get_search_param();	
		$total=$this->gift_card_model->countGiftCards();
		paging('id desc');
		//$this->_get_search_param();	
		$rows=$this->gift_card_model->getGiftCards()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	/*public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['code']!='')?$this->db->like('code',$params['search']['code']):'';
						
		}
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);
			}
		}  
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}*/
	
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->gift_card_model->delete('GIFT_CARD',array('id'=>$row));
            endforeach;
		}
	} 
	
	function form()
	{
		
		$this->form_validation->set_rules('to_email', 'lang:recipient_email', 'trim|required');
		$this->form_validation->set_rules('to_name', 'lang:recipient_name', 'trim|required');
		$this->form_validation->set_rules('from', 'lang:sender_name', 'trim|required');
		$this->form_validation->set_rules('personal_message', 'lang:personal_message', 'trim');
		$this->form_validation->set_rules('beginning_amount', 'lang:amount', 'trim|required|numeric');
		
		$data['page_title'] = lang('add_giftcard');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->view($this->config->item('admin_folder').'/giftcard/form', $data);
		}
		else
		{
			$save['code'] = generate_code(); // from the string helper
			$save['to_email'] = $this->input->post('to_email');
			$save['to_name'] = $this->input->post('to_name');
			$save['from'] = $this->input->post('from');
			$save['personal_message'] = $this->input->post('personal_message');
			$save['beginning_amount'] = $this->input->post('beginning_amount');
			$save['activated'] = 1;
			
			$this->gift_card_model->save_card($save);
			
			if($this->input->post('send_notification'))
			{
				//get the canned message for gift cards
				$row = $this->db->where('id', '1')->get('tbl_canned_messages')->row_array();

				// set replacement values for subject & body
				$row['subject']	= str_replace('{from}', $save['from'], $row['subject']);
				$row['subject']	= str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
				
				$row['content']	= str_replace('{code}', $save['code'], $row['content']);
				$row['content']	= str_replace('{amount}', $save['beginning_amount'], $row['content']);
				$row['content']	= str_replace('{from}', $save['from'], $row['content']);
				$row['content']	= str_replace('{personal_message}', nl2br($save['personal_message']), $row['content']);
				$row['content']	= str_replace('{url}', $this->config->item('base_url'), $row['content']);
				$row['content']	= str_replace('{site_name}', $this->config->item('company_name'), $row['content']);

				$this->load->library('email');

				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from($this->config->item('email'));
				$this->email->to($save['to_email']);

				$this->email->subject($row['subject']);
				$this->email->message($row['content']);

				$this->email->send();
			}
			
			$this->session->set_flashdata('message', lang('message_saved_giftcard'));
			
			redirect(site_url('giftcard/admin'));
		}
		
	}
	
	function activate($code)
	{
		$this->gift_card_model->activate($code);
		$this->gift_card_model->send_notification($code);
		$this->session->set_flashdata('message', lang('message_activated_giftcard'));
		redirect(site_url('giftcard/admin'));
	}
	
	function delete($id)
	{
		$this->gift_card_model->delete($id);
		
		$this->session->set_flashdata('message', lang('message_deleted_giftcard'));
		redirect(site_url('giftcard/admin'));
	}
	
	// Gift card functionality 
	function enable()
	{
		
		$config['predefined_card_amounts'] = "10,20,25,50,100";
		$config['allow_custom_amount'] = "1";
		$config['enabled'] = '1';
		$this->Settings_model->save('gift_cards', $config);
		redirect(site_url('giftcard/admin'));
	}
	
	function disable() 
	{
		$config['enabled'] = '0';
		$this->Settings_model->save('gift_cards', $config);
		redirect(site_url('giftcard/admin'));
	}
	
	function settings()
	{		
		$gc_settings = $this->Settings_model->get('gift_cards');
				
		$data['predefined_card_amounts']	= $gc_settings['predefined_card_amounts'];
		$data['allow_custom_amount'] 		= $gc_settings['allow_custom_amount'];
		
		$this->form_validation->set_rules('predefined_card_amounts', 'lang:predefined_card_amounts', 'trim');
		$this->form_validation->set_rules('allow_custom_amount', 'lang:allow_custom_amounts', 'trim');
		
		$data['page_title']	= lang('giftcard_settings');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->view($this->config->item('admin_folder').'/giftcard/settings', $data);
		}
		else
		{
		
			$save['predefined_card_amounts']	= $this->input->post('predefined_card_amounts');
			$save['allow_custom_amount']		= $this->input->post('allow_custom_amount');
			
			$this->Settings_model->save('gift_cards', $save);
			
			$this->session->set_flashdata('message', lang('message_saved_settings'));
			
			redirect(site_url('giftcard/admin'));
			
		}
	}
        function cardproductform(){
//            $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
            $this->form_validation->set_rules('amount', 'lang:amount', 'trim|required');
            $data['page_title'] = 'New Gift Card';
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->view($this->config->item('admin_folder').'/giftcard/gift_card_form', $data);
		}
		else
		{
			echo '<pre>';
                        /*
                         * image upload
                         */
                        $image = $this->product_image_upload();
                        $save['images'] = json_encode(
                                array($image['file_name'] =>
                                    array(
                                        'filename' => $image['file_name'],
                                        'alt' => "",
                                        'caption' => "",
                                        'primary' => TRUE
                                    )
                                )
                        );
                        
                        echo '<br>';
                        $save['name_en'] = $this->input->post('currency').'_gift_card_'.$this->input->post('amount');
                        $save['name_da'] = 'gift_card_'.$this->input->post('currency');
                        $save['display_name_en'] = 'gift card ';
                        $save['display_name_da'] = 'gift card ';
                                    if($this->input->post('currency') == 'euro'):?>
                                            <?php $price = round($this->input->post('amount')/$this->exrate['rate_euro']) ?>
                                    <?php elseif($this->input->post('currency') == 'usd'):?>
                                              <?php $price = round($this->input->post('amount')/$this->exrate['rate_dollar']) ?>
                                    <?php else:?>
                                    
                                      <?php $price = round($this->input->post('amount')) ?>
                                   <?php endif;
                        $save['price'] = $price;
//                        $save['currency'] = $this->input->post('currency');
                        $save['added_date'] = date('Y-m-d h:i:s');
                        $save['enabled'] = $this->input->post('enable');
                        
//                        print_r($save);exit;
                        $success = $this->product_model->insert('PRODUCT',$save);
                        if($success){
                            $category['product_id'] = $this->db->insert_id();
                            $category['category_id'] = '71';
                            $this->category_products_model->insert('CATEGORY_PRODUCT',$category);
                        }
			
			redirect(site_url('giftcard/admin'));
		}
        }
        
        function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 450;
			$config['height'] = 350;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 261;
			$config['height'] = 245;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['file_name']	= $upload_data['file_name'];
                        return $data;
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
//		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}
}
