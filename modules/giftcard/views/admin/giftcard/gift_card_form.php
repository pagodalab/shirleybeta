<?php echo form_open_multipart(site_url('giftcard/admin/cardproductform')); ?>
        
        
        <?php /*<label for="name"><?php echo lang('name');?> </label>
	<?php
	$data	= array('name'=>'name', 'value'=>set_value('name'), 'class'=>'span3');
	echo form_input($data);
	*/?>
        
        <label for="amount"><?php echo lang('amount');?> </label>
	<?php
	$data	= array('name'=>'amount', 'value'=>set_value('amount'), 'type'=>'number', 'class'=>'span3');
	echo form_input($data);
	?>
        
        <label for="currency"><?php echo lang('currency');?> </label>
	<?php
	$option = array(
            'DKK' => 'dkk',
            'euro' => '&#8364',
            'usd' => '$'
        );
	echo form_dropdown('currency',$option);
	?>
        
        <label for="image"><?php echo lang('card_image');?> </label>
	<?php
	$data	= array('name'=>'userfile', 'value'=>set_value('image'), 'class'=>'span3');
	echo form_upload($data);
	?>
        
        <label for="image"><?php echo lang('status');?> </label>
	<?php
        $option = array(
            '0' => 'disable',
            '1' => 'enable'
        );
	echo form_dropdown('enable',$option);
	?>
        
	<div class="form-actions">
		<input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>"/>
        <a href="<?php echo site_url('giftcard/admin')?>" class="btn btn-danger">Cancel</a>
	</div>