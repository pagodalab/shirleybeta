<table id="giftcard-table" data-options="pagination:true,title:'Gift Cards',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="code" sortable="true" width="150">Name</th>
    <th field="from" sortable="true" width="150">From</th>
    <th field="to_name" sortable="true" width="150">To</th>
    <th field="beginning_amount" width="50">Total</th>
    <th field="amount_used" width="50">Used</th>
    <th field="amount" width="50" formatter="formatItem">Remaining</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <?php if ($gift_cards['enabled']):?>
	<a href="<? echo site_url("giftcard/admin/form")?>" class="tooltip-top btn btn-small"><span><i class="icon-plus"></i></span> Add giftcard</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
	<a class="tooltip-top btn btn-small" href="<?php echo site_url('giftcard/admin/settings'); ?>"><i class="icon-cog"></i> <?php echo lang('settings');?></a>
	
	<a class="tooltip-top btn btn-small" href="<?php echo site_url('giftcard/admin/disable'); ?>"><i class="icon-ban-circle"></i> <?php echo lang('disable_giftcards');?></a>
	
<?php else: ?>
	<a class="tooltip-top btn btn-small" href="<?php echo site_url('giftcard/admin/enable'); ?>"><i class="icon-ok icon-white"></i> <?php echo lang('enable_giftcards');?></a>
<?php endif; ?>
    </p>

</div> 

<script language="javascript" type="text/javascript">

	$(function(){
			
		/*$('#clear').click(function(){
			$('#giftcard-search-form').form('clear');
			$('#giftcard-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#giftcard-table').datagrid({
				queryParams:{data:$('#giftcard-search-form').serialize()}
				});
		});		*/
		$('#giftcard-table').datagrid({
			url:'<?=site_url('giftcard/admin/json')?>',
			height:'auto',
			width:'auto'

		});
	
	});
	

	function formatItem(value,row,index)
	{
		var beginning = row.beginning_amount;
		var used = row.amount_used;
		var remaining = '<p>' +(beginning-used)+'<p>';
		return remaining;
	}
	
	
	function getActions(value,row,index)
	{
		
		var d = '<a href="#" onclick="removegiftcard('+index+')" class="tooltip-top btn btn-small" title="Delete"><span><i class="icon-trash"></i></span> </a>';
		
		return d;		
	}
	
	function removegiftcard(index)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#giftcard-table').datagrid('getRows')[index];
				
				$.post('<?php  echo site_url('giftcard/admin/delete_json')?>', {id:[row.id]}, function(){
					$('#giftcard-table').datagrid('deleteRow', index);
					$('#giftcard-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#giftcard-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
				if(r){				
					$.post('<?php  echo site_url('giftcard/admin/delete_json')?>',{id:selected},function(data){
						$('#giftcard-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
</script>
