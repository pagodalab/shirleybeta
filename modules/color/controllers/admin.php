<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('color/color_model');
        $this->lang->load('color/color');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'colors';
		$data['page_title'] = 'Colors';
		$this->view($this->config->item('admin_folder').'/colors/index', $data);	
	}

	public function json()
	{
		//$this->_get_search_param();	
		$this->db->where('del_flag',0);
		$total=$this->color_model->count();
		paging('color_id desc');
		//$this->_get_search_param();	
		$this->db->where('del_flag',0);
		$rows=$this->color_model->getColors()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	// public function _get_search_param()
	// {
	// 	// Search Param Goes Here
	// 	parse_str($this->input->post('data'),$params);
	// 	if(!empty($params['search']))
	// 	{
	// 		($params['search']['colors_name']!='')?$this->db->like('colors_name',$params['search']['colors_name']):'';
	// 		($params['search']['address']!='')?$this->db->like('address',$params['search']['address']):'';
	// 		($params['search']['city']!='')?$this->db->like('city',$params['search']['city']):'';
	// 		($params['search']['contact_person']!='')?$this->db->like('contact_person',$params['search']['contact_person']):'';
	// 		($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
	// 		($params['search']['mobile']!='')?$this->db->like('mobile',$params['search']['mobile']):'';
	// 		($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
	// 		(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

	// 	}  
	// }
    
	public function combo_json()
    {
		$rows=$this->color_model->getColors()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->color_model->update('COLORS',array('del_flag'=>1),array('color_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('color_id'))
        {
			//$data['added_date'] =date('Y-m-d H:i:s');		
            $success=$this->color_model->insert('COLORS',$data);
        }
        else
        {
			//$data['modified_date'] =date('Y-m-d H:i:s');			
            $success=$this->color_model->update('COLORS',$data,array('color_id'=>$data['color_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['color_id'] = $this->input->post('color_id');
		$data['color_name_en'] = $this->input->post('color_name_en');
		$data['color_name_da'] = $this->input->post('color_name_da');
		$data['color_code'] = $this->input->post('color_code');

        return $data;
   }
   
   	
	    
}