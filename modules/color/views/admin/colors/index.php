<script type="text/javascript" src="<?php echo base_url()?>assets/js/colorpicker/farbtastic.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/colorpicker/farbtastic.css" type="text/css" />

<table id="color-table" data-options="pagination:true,title:'<?php  echo lang('color')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="color_id" sortable="true" width="30"><?=lang('color_id')?></th><?php */?>
    <th field="color_code" sortable="true" width="30" formatter="formatColor"><?=lang('color')?></th>
	<th field="color_name_<?php echo $this->culture_code; ?>" sortable="true" width="50"><?=lang('color_name')?></th>
	<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Add Brand"><span><i class="icon-plus icon-inverse"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Brand"><span><i class="icon-trash icon-inverse"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit color form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-color" method="post" >
    <table>
		<tr>
              <td width="34%" ><label><?=lang('color_name')?>:</label></td>
			  <td width="66%"><input type="text" name="color_name_en" id="color_name" class="easyui-validatebox" required="true"></td>
       </tr>
       <tr>
              <td width="34%" ><label><?=lang('color_name_da')?>:</label></td>
			  <td width="66%"><input type="text" name="color_name_da" id="color_name_da" class="easyui-validatebox" required="true"></td>
       </tr>
       <tr>
              <td width="34%" ><label><?=lang('color_code')?>:</label></td>
			  <td width="66%">
              	<input type="text" id="color_code" name="color_code" />
                </td>
                <td>
             	<div id="colorpicker"></div>
              </td>
       </tr>
       <input type="hidden" name="color_id" id="color_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->

<script language="javascript" type="text/javascript">
	$(function(){
		// $('#clear').click(function(){
		// 	$('#color-search-form').form('clear');
		// 	$('#color-table').datagrid({
		// 		queryParams:null
		// 		});

		// });

		// $('#search').click(function(){
		// 	$('#color-table').datagrid({
		// 		queryParams:{data:$('#color-search-form').serialize()}
		// 		});
		// });	
		$('#colorpicker').farbtastic(function() {
		  $("#color_code").css("background-color",$.farbtastic('#colorpicker').color);
		  $("#color_code").val($.farbtastic('#colorpicker').color);
		});
		$('#color-table').datagrid({
			url:'<?php  echo site_url('color/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_color')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removecolor('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_color')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-color').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_color')?>');
		$("#color_code").css("background-color",'');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#color-table').datagrid('getRows')[index];
		if (row){
			$('#form-color').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_color')?>');
			$("#color_code").css("background-color",row.color_code);
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removecolor(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#color-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('color/admin/delete_json')?>', {id:[row.color_id]}, function(){
					$('#color-table').datagrid('deleteRow', index);
					$('#color-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#color-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].color_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('color/admin/delete_json')?>',{id:selected},function(data){
						$('#color-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-color').form('submit',{
			url: '<?php  echo site_url('color/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-color').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#color-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	function formatColor(value)
	{
		console.log(value);
		return '<span style="padding:20px;background-color:'+value+'"></span>';
	}
</script>