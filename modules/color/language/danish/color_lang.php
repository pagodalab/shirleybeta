<?php


$lang['color_id']				= 'Color Id';
$lang['color_name']				= 'Color Name';
$lang['color_name_da']			= 'Color Name (in danish)';
$lang['color_code']				= 'Color Code';
$lang['create_color']='Create Color';
$lang['edit_color']='Edit Color';
$lang['delete_color']='Delete Color';
$lang['color_search']='Color Search';

$lang['colors']='Colors';
$lang['color']='Color';
$lang['delete_confirm']='Are you sure you want to delete?';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

