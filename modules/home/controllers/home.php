<?php 
class Home extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product/product_model');
		$this->load->model('occasion/occasion_model');
		$this->load->model('canvas/user_canvas_model');
		$this->load->model('canvas/canvas_product_model');
		$this->load->model('category/category_model');
		$this->load->model('banner/banner_model');
		$this->load->model('product/product_model');
		$this->load->model('page/page_model');
		$this->load->model('blog/blog_model');
		$this->load->model('exchangerate/exchangerate_model');
		$this->load->model('feature_slider/feature_slider_model');

		
		$this->filter=TRUE;
	}
	
	public function index($id=null)
	{	
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){

			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['canvas_wishlist']);
		}
		//blog
		$this->db->limit('2');
		$this->db->where('post_date <= curdate()');
		$this->db->where('status','1');
		$data['blogs'] = $this->blog_model->getBlogs(null,'post_date desc')->result_array();

		//about us
		$aboutus = $this->page_model->getPages(array('id'=>1))->row_array();
		
		$content = "content_".$this->culture_code;
		$data['aboutus'] = substr($aboutus[$content],0,550);
		
		
		//featured products
		$this->db->limit('9');
		$this->db->order_by('modified_date','desc');
		$data['products'] = $this->product_model->getProducts(array('is_featured'=>1,'enabled'=>1))->result_array();
// 			echo '<pre>'; print_r($data['products']); exit;
		$this->load->model('size/size_model');
		$this->db->order_by('sequence','asc');
		$data['sizes'] =$this->size_model->getSizes()->result_array();
		
		$this->load->model('color/color_model');
		$data['colors'] = $this->color_model->getColors()->result_array();
		//banner
		$data['banners'] = $this->banner_model->banner_collection_banners(1);//default banner limit is 5
		// echo '<pre>';print_r($data['banners']);exit;

		// for feature category
		$where['parent_id'] = 75;
		$where['enabled'] = 1;
		$data['featured_category'] = $this->category_model->getCategories($where)->row_array();

		//$this->db->limit('1');
		$this->db->where("(curdate() between enable_date AND disable_date) OR disable_date='0000-00-00'");
		$this->db->where('banner_collection_id',1);
		$data['banner'] = $this->banner_model->get_homepage_banners();
		
		$data['user_canvas'] = $this->user_canvas_model->getUserCanvas(array('user_canvas.status'=>1),'user_canvas_id desc',NULL)->result_array();

		//Featured Slider
		$data['feature_slider'] = $this->feature_slider_model->getFeature_sliders()->result_array();

		//Popup Notice data
		$data['popup_notice'] = $this->db->get_where('tbl_popup_notice', array('start_date <=' => date('Y-m-d'), 'end_date >=' => date('Y-m-d'), 'status' => 1))->result_array();

		$data['page'] = 'home';
		$this->view('home/index',$data);
	}
	
	public function getUserCanvasTemplates()
	{
		
		
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){

			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['canvas_wishlist']);
		}

		$latest_id = $this->input->post('id');
		if($latest_id)
		{
			$where['user_canvas.user_canvas_id > '.$latest_id]=NULL;
		}
		$data['user_canvas'] = $this->user_canvas_model->getUserCanvas(array('user_canvas.status'=>1),'user_canvas_id desc',NULL)->result_array();
		
		$this->load->view('home/content',$data);
	}
	
	public function getCanvasUpdates()
	{
		
		$latest_id = $this->input->post('id');
		$where['user_canvas.user_canvas_id > '.$latest_id]=NULL;
		$this->db->where(array('user_canvas.status'=>1));
		$data['user_canvas'] = $this->user_canvas_model->getUserCanvas($where,'user_canvas_id desc',NULL)->result_array();
		//echo $this->db->last_query(); exit;
		
		$new_templates = count($data['user_canvas']);
		
		echo json_encode(array('new_templates'=>$new_templates));
	}
	
	public function addSharePoints()
	{
		$this->load->model('customer/customer_points_model');
		$canvas_id = $this->input->post('id');
		if($canvas_id)
		{
			$cust_point['user_id'] 		= $this->go_cart->customer('id');
			$cust_point['point_type'] 	= 'Share';
			$cust_point['type_id']		= $canvas_id;
			$cust_point['points']		= 1;
			$cust_point['added_date']	= date('Y-m-d h:i:s');
			$cust_point['status']		= 'Pending';
			
			$success = $this->customer_points_model->insert('CUSTOMER_POINT',$cust_point);
		}
		
		if($success)
		{
			$success = TRUE;
		}
		else
		{
			$success = FALSE;
		}
		
		echo json_encode(array('success'=>$success));
	}

	public function todays_rate_eur()
	{
		$query = $this->exchangerate_model->fetch('EXCHANGERATE',NULL,NULL,array('rate_date'=>date('Y-m-d')))->row_array();

		if ($query) {
			exit;
		}

		$url = 'http://api.fixer.io/latest?base=DKK';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);

		$rates = json_decode($result,true);


		$rateData = array(
			'rate_date'	=>	date('Y-m-d'),
			'rate_euro'	=>	$rates['rates']['EUR'],
			'rate_dollar'	=>	$rates['rates']['USD'],
			'rate_yen'	=>	$rates['rates']['JPY'],
			'rate_hdollar'	=>	$rates['rates']['HKD'],
			'rate_korea'	=>	$rates['rates']['KRW'],
			'rate_adollar'	=>	$rates['rates']['AUD'],
			'rate_pound'	=>	$rates['rates']['GBP'],
		);

		//Skipp if found today's exchange rate
		// $rateData['id'] = $query['id'];
		// $success=$this->exchangerate_model->update('EXCHANGERATE',$rateData,array('id'=>$rateData['id']));
		$success=$this->exchangerate_model->insert('EXCHANGERATE',$rateData);
		exit;
	}


	function changeCurrency($currency)
	{
		$this->session->set_userdata('currency',$currency);

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function setFlagSession($currency)
	{
		if($currency=='euro')
		{
			$this->session->set_userdata('currency',$currency);
			redirect(base_url('en'));
		}
		else
		{
			$this->session->set_userdata('currency',$currency);
			redirect(base_url('da'));
		}	
	}
	function insert_email_notificatoin(){
		$data['email'] = $this->input->post('email_notify');
		$data['size'] = $this->input->post('product_size');
		$data['product_id'] = $this->input->post('product_id');
		$data['created_date'] = date("Y-m-d H:i:s");

		if($data['email']) {
			echo json_encode(array('success' => false, 'msg'=>'Fail adding email.'));
			exit;
		}

		$success = $this->db->insert('tbl_restock_notify',$data);
		if($success){
			echo json_encode(array('success' => true, 'msg'=>'We will notify you when we add the product.'));
		}

	}


	function language()
	{
		$this->set_language($this->input->post('currlang'));
	}


	//Function if you are selecting from the Link


	function set_language($language_code)
	{
		$this->set_current_culture($language_code);
		redirect(site_url('home'));

	}
	
	//Image compression codes //niroj
	/*public function compress_image()
	{
		$path = FCPATH.'uploads/images/full';
		$this->load->library('image_lib');


		$files = array_slice(scandir($path), 2);  

		$this->thumb1($files,$path);
		// $this->thumb2($files,$path);
	}
	private function thumb1($file_list,$path){
		ini_set('MAX_EXECUTION_TIME', -1);

		foreach ($file_list as $key => $file) {

			if($key <= 1100)
			{
				continue;
			}
			// if($key > 1100) {
			// 	break;
			// }
			$imgConfig['image_library']    = 'gd2';
			$imgConfig['source_image']     = $path.'/'.$file;
			$imgConfig['maintain_ratio']   = TRUE;
			$imgConfig['width']            = 450;
			$imgConfig['height']            = 450;

			$newpath = FCPATH.'uploads/images/';

			$imgConfig['new_image']           = $newpath.'450/'.$file;   

			$this->load->library('image_lib');
			$this->image_lib->initialize($imgConfig);


			if ( ! $this->image_lib->resize())
			{
				echo $this->image_lib->display_errors();
			}

			$this->image_lib->clear();
		}

	}*/


	public function get_hover_image_json()
	{
		$product_id = $this->input->post('id');
	    // print_r($product_id); exit;
		$this->db->select('images');
		$this->db->where('id',$product_id);
		$data = $this->db->get('tbl_products')->row_array();
		$images = json_decode($data['images']);
	    // echo $this->db->last_query();
		$filtered =array();
		foreach($images as $key => $value){
			$filtered[] = $value->filename;
		}
		echo json_encode(array('images'=>$filtered)); 
	}

}