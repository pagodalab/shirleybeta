
<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
      
    }
    
	public function index()
	{
		// Display Page
		$data['page_title'] = 'Footer Menu';
		$page = $this->config->item('admin_folder') . "/footer_menu/index";
		$this->view($page,$data);		
	}
	
}	