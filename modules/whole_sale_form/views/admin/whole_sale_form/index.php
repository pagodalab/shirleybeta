<div region="center" border="false">
	<div style="padding:20px">
		<!-- <div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('whole_sale_form_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
		<form action="" method="post" id="whole_sale_form-search-form">
		<table width="100%" border="1" cellspacing="1" cellpadding="1">
		<tr><td><label><?php echo lang('name_en')?></label>:</td>
		<td><input type="text" name="search[name_en]" id="search_name_en"  class="easyui-validatebox"/></td>
		<td><label><?php echo lang('name_da')?></label>:</td>
		<td><input type="text" name="search[name_da]" id="search_name_da"  class="easyui-validatebox"/></td>
		</tr>
		<tr>
		</tr>
		<tr>
		<td colspan="4">
		<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
		<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
		</td>
		</tr>
		</table>

		</form>
		</div>
		<br/> -->


		<table id="whole_sale_form-table" data-options="pagination:true,title:'<?php  echo lang('whole_sale_form')?>', pagesize:'20', rownumbers:true, toolbar:'#toolbar', collapsible:true, fitColumns:true">
			<thead>
				<th data-options="field:'checkbox',checkbox:true"></th>
				<th data-options="field:'name',sortable:true" width=""><?php echo lang('name')?></th>
				<th data-options="field:'email',sortable:true" width=""><?php echo lang('email')?></th>
				<th data-options="field:'phone',sortable:true" width=""><?php echo lang('phone')?></th>
				<th data-options="field:'address',sortable:true" width=""><?php echo lang('address')?></th>
				<th data-options="field:'shop_location',sortable:true" width=""><?php echo lang('shop_location')?></th>
				<th data-options="field:'online_address',sortable:true" width=""><?php echo lang('online_address')?></th>
				<th data-options="field:'establish_year',sortable:true" width=""><?php echo lang('establish_year')?></th>
				<th data-options="field:'brands_retailed',sortable:true" width=""><?php echo lang('brands_retailed')?></th>
				<th data-options="field:'reasons',sortable:true" width=""><?php echo lang('reasons')?></th>
				<th data-options="field:'status',sortable:true" width=""><?php echo lang('status')?></th>
				<th field="action" width="" formatter="getActions"><?php  echo lang('actions')?></th>

			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<!-- <a href="#" class="easyui-linkbutton" plain="false" onclick="header_description()" title="<?php echo lang('header_description')?>"><?php echo lang('header_description') ?></a> -->
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_whole_sale_form')?>">Selected Remove</a>
				<a href="<?php echo site_url('whole_sale_form/admin/export_whole_sale_csv');?>" class="tooltip-top btn btn-small" title="Subscriber List"><span><i class="icon-download"></i></span> <?php echo lang('subscriber_download');?></a>
			</p>
		</div> 


		<div id="header_description" class="easyui-dialog" style="width: 1000px; height: auto; padding: 10px 20px" data-options="closed:true, collapsible: true, modal:true">
			<form id="form-header_description" method="post">
				<div class="col-md-12">
					<div class="col-md-12">
						<label>Form Header</label>
						<textarea name="description" class="redactor"></textarea>
					</div>
					<div class="pull-right">
						<div class="btn-group btn-group-sm">
							<button type="button" class="btn btn-primary" onclick="saveFormHeader()">Save</button>
							<button type="button" class="btn btn-link">Cancel</button>
						</div>
					</div>

				</div>			
			</form>
		</div>

	</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#whole_sale_form-search-form').form('clear');
			$('#whole_sale_form-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#whole_sale_form-table').datagrid({
				queryParams:{data:$('#whole_sale_form-search-form').serialize()}
			});
		});		
		$('#whole_sale_form-table').datagrid({
			url:'<?php  echo site_url('whole_sale_form/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		$('.datepicker').datepicker({format: 'yyyy-mm-dd',});

	});

	function getActions(value,row,index)
	{
		

		var d = '<a href="#" onclick="removewhole_sale_form('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="delete"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return d;		
	}

	function getImage(v,r,i) {
		var image = "<img src='<?php echo base_url('uploads/notices'); ?>/"+v+"' style='height: 150px;' />";
		return image;
	}

	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function header_description() {
		$('#header_description').window('open').window('setTitle', '<?php echo lang('header_description'); ?>');
	}

	function create(){
		$('#form-whole_sale_form').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_whole_sale_form')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#whole_sale_form-table').datagrid('getRows')[index];
		if (row){
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#form-whole_sale_form').form('load', row);
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_whole_sale_form')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}


	function removewhole_sale_form(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#whole_sale_form-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('whole_sale_form/admin/delete_json')?>', {id:[row.id]}, function(){
					$('#whole_sale_form-table').datagrid('deleteRow', index);
					$('#whole_sale_form-table').datagrid('reload');
				});

			}
		});
	}

	function removeSelected()
	{
		var rows=$('#whole_sale_form-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}

			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('whole_sale_form/admin/delete_json')?>',{id:selected},function(data){
						$('#whole_sale_form-table').datagrid('reload');
					});
				}

			});

		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}

	}

	function save()
	{
		$('#form-whole_sale_form').submit();
		return;


		$('#form-whole_sale_form').form('submit',{
			url: '<?php  echo site_url('whole_sale_form/admin/whole_sale_form/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-whole_sale_form').form('clear');
					$('#dlg').window('close');
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#whole_sale_form-table').datagrid('reload');
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				}
			}

		});		

	}

	function saveFormHeader() {
		$('#form-header_description').form('submit', {
			url: '<?php echo site_url('whole_sale_form/admin/saveFormHeader'); ?>',
			onSubmit: function() {
				return $(this).form('validate');
			},
			success: function(result) {
				var result = eval('('+result+')');
				if(result.success) {
					$('#header_description').window('close');
				}
				else {
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				}
			}
		});
	}


</script>