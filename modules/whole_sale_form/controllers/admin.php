<?php
class Admin extends Admin_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('whole_sale_form_model');
		$this->lang->load('whole_sale_form');
		$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
		$this->load->model('Settings_model');

	}

	public function index()
	{
		// Display Page
		$data['header'] = 'whole_sale_form';
		//$data['page'] = $this->config->item('template_admin') . "whole_sale_form/index";
		$data['module'] = 'whole_sale_form';
		$this->view($this->config->item('template_admin') . "admin/whole_sale_form/index",$data);			
	}


	public function json()
	{
		$this->_get_search_param();	
		$this->db->where('del_flag', 0);
		$total = $this->whole_sale_form_model->count();

		paging('id desc');

		$this->_get_search_param();	
		$this->db->where('del_flag', 0);
		$rows=$this->whole_sale_form_model->getWholeSaleForm()->result_array();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name_en']!='')?$this->db->like('name_en',$params['search']['name_en']):'';
			($params['search']['name_da']!='')?$this->db->like('name_da',$params['search']['name_da']):'';

		}

		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}

	}

	public function combo_json()
	{
		$rows=$this->whole_sale_form_model->getWholeSaleForm()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				// $this->whole_sale_form_model->delete('whole_sale_form', array('id'=>$row));
				$this->whole_sale_form_model->update('WHOLE_SALE_FORM', array('status' => 0,'del_flag'=>1), array('id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		$data = $this->_get_posted_data(); //Retrive Posted Data

		if($data == false || empty($data) || @$data['image'] == '') {
			redirect('whole_sale_form/admin/whole_sale_form');
		}

		if(!$this->input->post('id'))
		{
			$data['created_date'] = date('Y-m-d');
			$success = $this->whole_sale_form_model->insert('whole_sale_form',$data);
		}
		else
		{
			$data['modified_date'] = date('Y-m-d');
			$success = $this->whole_sale_form_model->update('whole_sale_form',$data,array('id'=>$data['id']));
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}

		redirect('whole_sale_form/admin/whole_sale_form');
		// echo json_encode(array('msg'=>$msg,'success'=>$success));		

	}

	private function _get_posted_data()
	{
		$data=array();

		$config['upload_path']		= 'uploads/notices/';
		$config['allowed_types']	= 'gif|jpg|png';
		$config['max_size']			= $this->config->item('size_limit');
		$this->load->library('upload');
		$this->upload->initialize($config);

		if ( ! $upload_data = $this->upload->do_upload('image'))
		{
			$error_data['error'] = array('error' => $this->upload->display_errors());
			$error_data['header'] = 'whole_sale_form';
			$error_data['module'] = 'whole_sale_form';
			$this->view($this->config->item('template_admin') . "admin/whole_sale_form/index",$error_data['error']);
			return false;
		}

		$upload_data = $this->upload->data();
		// redirect('whole_sale_form/admin/whole_sale_form');

		$data['id'] = $this->input->post('id');
		$data['name'] = $this->input->post('name');
		$data['image'] = $upload_data['file_name'];
		$data['start_date'] = $this->input->post('start_date');
		$data['end_date'] = $this->input->post('end_date');

		$data['status'] = $this->input->post('status');

		return $data;
	}

	public function saveFormHeader() {
		$post = $this->input->post();

		$success = $this->Settings_model->save('whole_sale_form', array('form-header' => $post['description']));

		//always return success
		echo json_encode(array('success' => true, 'msg' => 'Saved' ));
	}

	public function export_whole_sale_csv()
	{
		//echo "<pre>"; print_r($_POST); exit;
		$this->load->library('PHPExcel');

		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

		$whole_sale_form_keys = array('name','retail_type','email','phone','address','shop_location','online_address','establish_year','brands_retailed','reasons');
		$primary_headings = array('Name','Retail Type','Email','Phone','Address','Shop Location','Online Address','Established Year','Brands Retailed','Reasons');

		// $filter = $this->input->post('filter_option');
		// $annual = $this->input->post('year');

		$title = 'Whole Sale Forms ';

		$style_title = array( 

			'font' => array( 'bold' => true, ),
			'color' => array('rgb'=>'#a82d32')

		);
		$default_border = array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,

		); 
		$style_header = array( 
			'borders' => array(
				'bottom' => $default_border,
				'left' => $default_border,
				'top' => $default_border,
				'right' => $default_border,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,

			),
			'font' => array( 'bold' => true, 'font-size'=>'18px'),
		);
		$style_body = array( 

			'font' => array( 'bold' => false, 'font-size'=>'18px'),
		);


		// 			$whole_sale_forms = $this->whole_sale_form_model->getWholeSaleForm(array('status'=>1,'del_flag'=>0))->result_array();

		// foreach($whole_sale_forms as $whole_sale_form)
		// {
		// 	array_push($primary_headings, $whole_sale_form['name']);
		// }
		// echo print_r($primary_headings);
		// exit;

		$objPHPExcel = new PHPExcel();
		//$objPHPExcel->getActiveSheet()->setTitle('CBAPO Activities During Partolling');  //set excel sheet title
		$objPHPExcel->getProperties()->setTitle("Bank-report");



		$objPHPExcel->getActiveSheet()->setCellValue('A1',$title)->getStyle()->applyFromArray($style_title)->getFont()->setSize(18);

		$row_number = 5;
		$col = 'A';
		foreach($primary_headings as $p_heading)         //add column headings
		{
			$objPHPExcel->getActiveSheet()->setCellValue($col.$row_number, $p_heading)
			->getStyle($col.$row_number)
			->applyFromArray( $style_header )
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)
			->setWidth(22);

			// ->getColumnDimension($col1)
			// ->setWidth(12);   
			$col++;
			$objPHPExcel->getActiveSheet()->getRowDimension($row_number)->setRowHeight(30);
		}

		// if($filter=='SA')
		// {	
		// 	$sa = $this->input->post('SA');
		// 	if($sa==1)
		// 	{
		// 		$start = date($annual.'-'.'01-01');
		// 		$end = date($annual.'-'.'06-30');
		// 	}
		// 	else
		// 	{
		// 		$start = date($annual.'-'.'07-01');
		// 		$end = date($annual.'-'.'12-30');
		// 	}	

		// 	$this->db->where('send_date between "'.$start.'" and "'.$end.'"');
		// }
		// else if($filter=='Q')
		// {
		// 	$quarter = $this->input->post('Q');

		// 	if($quarter==1)
		// 	{
		// 		$start = date($annual.'-'.'01-01');
		// 		$end = date($annual.'-'.'03-30');
		// 	}
		// 	elseif ($quarter==2)
		// 	{
		// 		$start = date($annual.'-'.'04-01');
		// 		$end = date($annual.'-'.'06-30');
		// 	}
		// 	elseif ($quarter==3)
		// 	{
		// 		$start = date($annual.'-'.'07-01');
		// 		$end = date($annual.'-'.'09-30');
		// 	}
		// 	else
		// 	{
		// 		$start = date($annual.'-'.'10-01');
		// 		$end = date($annual.'-'.'12-30');
		// 	}	

		// 	$this->db->where('send_date between "'.$start.'" and "'.$end.'"');
		// }
		// else if($filter=='M')
		// {
		// 	$month = $this->input->post('M');



		// 	$this->db->where('send_date like "'.$annual.'-'.$month.'%"');
		// }

		$whole_sales = $this->whole_sale_form_model->getWholeSaleForm(array("del_flag"=>0,"status"=>1))->result_array();

		// echo $this->db->last_query();
		// echo '<pre>'; print_r($feedbacks);
		// exit;
		$row_number1 = 6;
		foreach ($whole_sales as $whole_sale) 
		{
			$col1 = 'A';
			foreach($whole_sale_form_keys as $header)
			{
				foreach($whole_sale as $key=>$val)
				{
					if($header == $key)
					{
						// $objPHPExcel->getActiveSheet()->setCellValue($col1.$row_number1, $val);	
						// $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col1)->setAutoSize(true);
						// $col++;	
						$objPHPExcel->getActiveSheet()->setCellValue($col1.$row_number1, $val) 
						->getStyle($col1.$row_number1)
						->applyFromArray( $style_body )
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
						->setWrapText(true);

						$objPHPExcel->getActiveSheet()->getColumnDimension($col1,$row_number1)
						->setWidth(22);

						$col1++;
						$objPHPExcel->getActiveSheet()->getRowDimension($row_number1)->setRowHeight(30);
						break;	
					}
				}

			}		

			// $answers = $this->feedback_question_model->getFeedbackQuestions(array('feedback_id'=>$feedback['id']))->result_array();
			// foreach($answers as $answer)
			// {

			// 	$objPHPExcel->getActiveSheet()->setCellValue($col1.$row_number1, $answer['answer']) 
			// 										->getStyle($col1.$row_number1)
			//  										  		->applyFromArray( $style_body )
			//  										  		->getAlignment()
			//  										  		->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			//  										  		->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			//  										  		->setWrapText(true);

			//  				$objPHPExcel->getActiveSheet()->getColumnDimension($col1,$row_number1)
			//  										  ->setWidth(22);

			// 		$col1++;
			// 		$objPHPExcel->getActiveSheet()->getRowDimension($row_number1)->setRowHeight(30);
			// }	

			$row_number1++;
		}



		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$file_name = 'whole_sale_form'.date('Y_m_d_h_i_s').'.xlsx';
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'"');
		header('Cache-Control: max-age=0'); 
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('php://output');

	}	


}