<?php 
class Whole_sale_form extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('whole_sale_form/whole_sale_form_model','whole_sale_form_model');
		$this->menu_active = 'whole_sale_form';
		$this->load->model('Settings_model');
		$this->lang->load('whole_sale_form');
	}
	
	public function index()
	{
		
		// Display
		// $data['base_url']			= $this->uri->segment_array();
		$data['page_title']			= 'Wholesale Form';
		$data['header'] = 'Wholesale Form';

		$this->load->view('whole_sale_form/index', $data);
	}

	public function save_whole_sale_form(){
		$data=$this->_map_posted_data();
		if(!$this->input->post('id')){
			$data["created_date"] = date('Y-m-d h:i:s');
			$data["status"]=1;
			$data["del_flag"]=0;
			$this->whole_sale_form_model->insert('WHOLE_SALE_FORM',$data);
		}
		else{
			$data["modified_date"]=data('Y-m-d h:i:s');
		}

		/*for email*/
		$this->reply_email($data['email']);

		redirect(site_url('whole_sale_form'));
	}

	private function _map_posted_data(){
		$data = array();
		$data["retail_type"]=$this->input->post("retail_type");
		$data["name"]=$this->input->post("name");
		$data["email"]=$this->input->post("email");
		$data["phone"]=$this->input->post("phone");
		$data["address"]=$this->input->post("address");
		$data["shop_location"]=$this->input->post("shop_location");
		$data["online_address"]=$this->input->post("online_address");
		$data["establish_year"]=$this->input->post("establish_year");
		$data["brands_retailed"]=$this->input->post("brands_retailed");
		$data["reasons"]=$this->input->post("reasons");	 
		return $data;    
	}

	function reply_email($email) {
		$this->load->library('email');

		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['protocol'] = 'sendmail';
		
		/*$config['protocol'] = 'smtp';
		$config['charset'] = 'iso-8859-1';
		$config['smtp_host'] = 'smtp.vianet.com.np';*/

		$this->email->initialize($config);

		$subject	=	"Wholesale Application Form ";
		$body = "<p> Your wholesale application form to retail Shirley Bredal clothing has been received by Shirley Bredal's Administrative Team. </p> <p> Just a reminder, wholesale applications are reviewed periodically as production capacity increases and it may take us a bit of time to get back to you. If you would like to provide any additional information at any time, in support of your application - such as the starting volume and styles your are particularly interested in selling - please feel free to contact our Administrative Team via <a href='mailto:admin@shirleybredal.com'>admin@shirleybredal.com</a> </p> <p>Many thanks for your application. </p>";


		//this also in admincontroller sendemailnotification
		$this->email->from('admin@shirleybredal.com', 'Shirley Bredal');
		$this->email->to($email); 
		$this->email->bcc('admin@shirleybredal.com');
		$this->email->subject($subject);
		$this->email->message(html_entity_decode($body));
		$this->email->send();
	}
}