<?php


$lang['id'] = 'Id';
$lang['name'] = 'Event Name';
$lang['image'] = 'Image';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['status'] = 'Status';
$lang['actions'] = 'Actions';

$lang['create_whole_sale_form']='Create WholeSale Form';
$lang['edit_whole_sale_form']='Edit WholeSale Form';
$lang['delete_whole_sale_form']='Delete WholeSale Form';
$lang['whole_sale_form_search']='WholeSale Form Search';

$lang['whole_sale_form']='WholeSale Form';
$lang['header_description']='Header Description';

// $lang['saved_successful']='Saved Successful';
// $lang['delete_successful']='Delete Successful';


$lang['name']		=	"Company Name";
$lang['email']		=	"Email";
$lang['phone']		=	"Phone";
$lang['retail_type']		=	"Retail Type";
$lang['address']			=	"Address";
$lang['shop_location']		=	"Shop Location";
$lang['online_address']		=	"Online Address";
$lang['establish_year']		=	"Established Year";
$lang['brands_retailed']	=	"Brands Retailed";
$lang['reasons']			=	"Reasons";
$lang['subscriber_download']       = "Export Whole Sale Form(CSV)";
$lang['delete_confirm'] = "Are sure to delete ?";

$lang['wholesale_title'] = "Wholesale Customer: Application Form";
