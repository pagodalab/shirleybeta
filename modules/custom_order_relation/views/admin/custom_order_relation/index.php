
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('search')?>',collapsible:false,iconCls:'icon-search'" style="padding:5px">
	<form action="" method="post" id="custom_order-search-form">
		<table width="100%">
			<tr>
				<td><label>Style:</label></td>
				<td><input type="text" name="search[style]" id="search_style"  class="easyui-validatebox"/></td>
				<td><label>Color:</label></td>
				<td><input type="text" name="search[color]" id="search_color"  class="easyui-validatebox"/></td>
			</tr>
			<tr>
				<td><label>Size</label></td>
				<td><input type="text" name="search[size]" id="search_size" class="easyui-validatebox"/></td>
				<td><label>Material</label></td>
				<td><input type="text" name="search[material]" id="search_material" class="easyui-validatebox" value="merino"></td>
			</tr>
			<tr>
				<td colspan="4">
					<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
					<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
				</td>
			</tr>
		</table>

	</form>
</div>


<table id="event-table" data-options="pagination:true,title:'Styles',pagesize:'20', pageList:[20,50,100,200], rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
	<thead>
		<th field="checkbox" checkbox="true"></th>
		<th field="style_name_en" sortable="true" width="80">Style</th>
		<th field="color_name_en" sortable="true" width="100">Color</th>
		<th field="size_title_en" sortable="true" width="100">Size</th>
		<th field="name_en" sortable="true" width="100">Material</th>

		<th field="action" width="100" formatter="getActions">Action</th>
	</thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
	<p>
		<a href="javascript:void(0)" onclick="create()" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Relation</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <?php /*?><select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a><?php */?>
</p>

</div> 

<!--for create and edit event form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
<form id="form-event" method="post" >

	<table>
		<tr>
			<td width="34%" ><label>Style : </label></td>
			<td width="66%">
				<select name="style_id" class="easyui-validatebox">
					<?php foreach ($styles as $style) { ?>
					<option value="<?php echo $style['id'] ?>"><?php echo $style['style_name_en'] ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td width="34%" ><label>Color : </label></td>
			<td width="66%">
				<select name="color_id" class="easyui-validatebox">
					<?php foreach ($colors as $color) { ?>
					<option value="<?php echo $color['color_id'] ?>"><?php echo $color['color_name_en'] ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td width="34%" ><label>Size : </label></td>
			<td width="66%">
				<select name="size_id" class="easyui-validatebox">
					<?php foreach ($sizes as $size) { ?>
					<option value="<?php echo $size['size_id'] ?>"><?php echo $size['size_title_en'] ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td width="34%" ><label>Material : </label></td>
			<td width="66%">
				<select name="material_id" class="easyui-validatebox">
					<?php foreach ($materials as $material) { ?>
					<option value="<?php echo $material['id'] ?>"><?php echo $material['name_en'] ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>

		<input type="hidden" name="id" id="id"/>
	</table>

</form>
<div id="dlg-buttons">
	<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
	<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
</div>    
</div>
<!--div ends-->     

<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#custom_order-search-form').form('clear');
			$('#event-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#event-table').datagrid({
				queryParams:{data:$('#custom_order-search-form').serialize()}
			});
		});

		$('#event-table').datagrid({
			url:'<?=site_url('custom_order_relation/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

	});
	function create(){
		//Create code here
		$('#categories').html('');
		$('#form-event').form('clear');
		
		$('#dlg').window('open').window('setTitle','Insert Style');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		$('#categories').html('');
		var row = $('#event-table').datagrid('getRows')[index];

	}

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_event')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removeOccasion('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_event')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;
	}
	
	function save()
	{
		$('#form-event').form('submit',{
			url: '<?php  echo site_url('custom_order_relation/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-event').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#event-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close

		});		
		
	}

	function upload(index)
	{
		var row = $('#event-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('product_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {product_id: row.product_id};
		}
		$('#form-upload').form('clear');
	}

	function removeOccasion(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#event-table').datagrid('getRows')[index];
				$.post('<?php echo site_url('custom_order_relation/admin/delete_json')?>', {id:[row.id]}, function(data){
					$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
					//$('#event-table').datagrid('deleteRow', index);
					$('#event-table').datagrid('reload');
				},'json');

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#event-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?php echo site_url('custom_order_relation/admin/delete_json')?>',{id:selected},function(data){
						$('#event-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function checkSubs(id)
	{
		var status = false;
		var disabled = true;
		
		if($('#category'+id).prop('checked'))
		{
			status = true;
			disabled = false;
			//console.log(status); 
			//return false;
			//$('.subcategory'+id).prop('checked',status);
		}
		$('.subcategory'+id).prop('checked',status);
		$('.suborder'+id).prop('disabled',disabled);
		$('.order'+id).prop('disabled',disabled);
	}
	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return 'Yes';
		}
		else
		{
			return 'No';
		}
	}
</script>
