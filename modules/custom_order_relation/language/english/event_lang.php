<?php


$lang['event_id'] = 'Brand Id';
$lang['event_name'] = 'Title';
$lang['sort_order'] = 'Sequence';
$lang['status'] = 'Status';


$lang['create_event']='Add Events';
$lang['edit_event']='Edit Events';
$lang['delete_event']='Delete Events';
$lang['event_search']='Events Search';

$lang['event']='Events';
$lang['success_message'] = "Success";
$lang['failure_message'] = "Error";
$lang['success'] = "Success";
//$lang['error'] = "Error";
$lang['edit_selection_error'] = "Please select at least one to remove.";

//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

