<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		$this->load->helper('form');
		// $this->lang->load('STYLE');
		$this->load->model('custom_order_relation/custom_order_relation');
		$this->load->model('style/styles_model');
		$this->load->model('size/size_model');
		$this->load->model('color/color_model');
		$this->load->model('material/material_model');




	}

	public function index()
	{
		// Display Page
		$data['page_title'] = 'Custom Order Relation';
		$data['styles'] = $this->styles_model->getStyles()->result_array();
		$data['materials'] = $this->material_model->getMaterials()->result_array();
		$data['sizes'] = $this->size_model->getSizes()->result_array();
		$data['colors'] = $this->color_model->getColors()->result_array();
		// echo '<pre>'; print_r($data); exit;
		// $data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'custom_order_relation';
		$this->view($this->config->item('template_admin') . "admin/custom_order_relation/index",$data);

	}

	public function json()
	{
		$this->_get_search_param();	
		$total = $this->db->get('view_custom_order_relation_all')->num_rows();
		paging('id desc');
		$this->_get_search_param();
		$rows = $this->db->get('view_custom_order_relation_all')->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['style']!='')?$this->db->like("style_name_{$this->culture_code}",$params['search']['style']):'';
			($params['search']['color']!='')?$this->db->like("color_name_{$this->culture_code}",$params['search']['color']):'';
			($params['search']['size']!='')?$this->db->like("size_title_{$this->culture_code}",$params['search']['size']):'';
			($params['search']['material']!='')?$this->db->like("name_{$this->culture_code}",$params['search']['material']):'';
		} 
	}
	
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}
	
	public function delete_json()
	{
		$id = $this->input->post('id');
		foreach ($id as $row) {
			$success = $this->custom_order_relation->delete('ORDER_RELATION',array('id'=>$row));
		}
		echo json_encode(array('success' => $success));
	}
	public function save()
	{
		$data = $this->_get_posted_data();
		if($data['id'])
		{
			
			$success = $this->custom_order_relation->update('ORDER_RELATION',$data,array('id'=>$data['id']));
		}
		else
		{
			$success = $this->custom_order_relation->insert('ORDER_RELATION',$data);
			// $new_id = $this->db->insert_id();
			
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	
	private function _get_posted_data()
	{
		$data = array();
		$data['id'] = $this->input->post('id');
		$data['style_id'] = $this->input->post('style_id');
		$data['color_id'] = $this->input->post('color_id');
		$data['size_id'] = $this->input->post('size_id');
		$data['material_id'] = $this->input->post('material_id');

		// echo '<pre>'; print_r($data); exit;
		return $data;
	}
	
	
	

}
