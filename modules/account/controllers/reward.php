<?php 

class Reward extends Secure_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->customer_name	=$this->customer['lastname'] .' '.$this->customer['firstname'];
	}
	
	public function index()
	{
		$this->Customer_model->is_logged_in('secure/reward/');
		
		$data['rewards']=array();
		$data['page_title']	= 'Your Reward Points';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['customer_name']		=$this->customer_name;

		$data['rewards']=$this->Customer_model->get_customer_reward($this->customer['id']);
		$data['total_rewards']=$this->Customer_model->get_total_reward($this->customer['id']);
		
		//foreach ($this->go_cart->get_wishlist() as $key => $product_id) {				
//			$data['rewards'][] = $this->Product_model->get_product($product_id);
//			
//		}
		
		$this->load->view('secure/reward/index', $data);
	}
	
	
}