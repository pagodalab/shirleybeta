<?php 

class Order extends Member_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->customer_name	=$this->customer['lastname'] .' '.$this->customer['firstname'];

	}
	
	public function index()
	{
		$this->history();
	}
	
	public function history($offset=0)
	{
		$data['products']=array();
		$data['page_title']	= 'Order History';
		$data['customer_name']		=$this->customer_name;
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$this->load->helper('date');		

		$this->load->library('pagination');

		$limit=2;
		$config=$this->pagination_config();
		$config['uri_segment'] = '4';
		$config['base_url'] = site_url('account/order/history');
		$config['total_rows'] = $this->order_model->count_customer_orders($this->customer['id']);
		$config['per_page'] = $limit; 
	
		
		
		$this->pagination->initialize($config); 
		
		$data['orders_pagination'] = $this->pagination->create_links();

		$data['orders']		= $this->order_model->get_customer_orders($this->customer['id'], $offset,$limit);

		$this->view('account/order/index', $data);
	}
	
	public function detail($id=NULL)
	{
		if(is_null($id))
		{
			redirect(site_url('account'));
		}
		$data['page_title']	= 'Order Detail';
		$data['customer_name']		=$this->customer_name;
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$this->load->model('order_model');
		$this->load->model('order/order_images_model');
		$this->load->helper('date');		

		$this->db->where(array('customer_id'=>$this->customer['id']));
		$data['order']	= $this->order_model->get_order($id);
		$data['images'] = $this->order_images_model->getOrderImages(array('order_id'=>$id))->result_array();	
			
		$this->view('account/order/detail', $data);				
	}
}