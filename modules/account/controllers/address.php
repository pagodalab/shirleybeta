<?php 

class Address extends Member_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->customer_name	=$this->customer['lastname'] .' '.$this->customer['firstname'];
	}
	
	public function index()
	{
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		$data['customer']			= (array)$this->customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->customer_model->get_address_list($this->customer['id']);
		$data['customer_name']		=$this->customer_name;
		
		$data['page_title']			= 'Address Book';
		$data['customer_addresses']	= $this->customer_model->get_address_list($data['customer']['id']);
		
		$this->load->helper('date');
		
		$this->view('account/address/index', $data);
	}

	function form($id = 0)
	{
		
		$customer = $this->go_cart->customer();
		
		//grab the address if it's available
		$data['id']			= false;
		$data['company']	= $customer['company'];
		$data['firstname']	= $customer['firstname'];
		$data['lastname']	= $customer['lastname'];
		$data['email']		= $customer['email'];
		$data['phone']		= $customer['phone'];
		$data['address1']	= '';
		$data['address2']	= '';
		$data['city']		= '';
		$data['country_id'] = '';
		$data['zone_id']	= '';
		$data['zip']		= '';
		
		

		if($id != 0)
		{
			$a	= $this->customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$data		= $a['field_data'];
				$data['id']	= $id;
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
			
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}
		
		//get the countries list for the dropdown
		$data['countries_menu']	= $this->location_model->get_countries_menu();
		
		if($id==0)
		{
			//if there is no set ID, the get the zones of the first country in the countries menu
			$data['zones_menu']	= $this->location_model->get_zones_menu(array_shift(array_keys($data['countries_menu'])));
		} else {
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}

		$this->load->library('form_validation');	
		$this->form_validation->set_rules('company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'lang:address_firstname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:address_email', 'trim|required|valid_email|max_length[128]');
		$this->form_validation->set_rules('phone', 'lang:address_phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('address1', 'lang:address:address', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('address2', 'lang:address:address', 'trim|max_length[128]');
		$this->form_validation->set_rules('city', 'lang:address:city', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('country_id', 'lang:address:country', 'trim|required|numeric');
		$this->form_validation->set_rules('zone_id', 'lang:address:state', 'trim|required|numeric');
		$this->form_validation->set_rules('zip', 'lang:address:zip', 'trim|required|max_length[32]');
		
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if(validation_errors() != '')
			{
				echo validation_errors();
			}
			else
			{
				$this->partial('address_form', $data);
			}
		}
		else
		{
			$a = array();
			$a['id']						= ($id==0) ? '' : $id;
			$a['customer_id']				= $this->customer['id'];
			$a['field_data']['company']		= $this->input->post('company');
			$a['field_data']['firstname']		= $this->input->post('firstname');
			$a['field_data']['lastname']		= $this->input->post('lastname');
			$a['field_data']['email']		= $this->input->post('email');
			$a['field_data']['phone']		= $this->input->post('phone');
			$a['field_data']['address1']		= $this->input->post('address1');
			$a['field_data']['address2']		= $this->input->post('address2');
			$a['field_data']['city']			= $this->input->post('city');
			$a['field_data']['zip']			= $this->input->post('zip');
			
			// get zone / country data using the zone id submitted as state
			$country = $this->location_model->get_country(set_value('country_id'));	
			$zone    = $this->location_model->get_zone(set_value('zone_id'));		
			if(!empty($country))
			{
				$a['field_data']['zone']		= $zone->code;  // save the state for output formatted addresses
				$a['field_data']['country']		= $country->name; // some shipping libraries require country name
				$a['field_data']['country_code']   = $country->iso_code_2; // some shipping libraries require the code 
				$a['field_data']['country_id']  = $this->input->post('country_id');
				$a['field_data']['zone_id']		= $this->input->post('zone_id');  
			}
			
			$this->customer_model->save_address($a);
			//$this->session->set_flashdata('message', lang('message_address_saved'));
			//redirect(site_url('account/address'));
			echo 1;
		}

	}
	
	function delete()
	{
		$id = $this->input->post('id');
		// use the customer id with the addr id to prevent a random number from being sent in and deleting an address
		$customer = $this->go_cart->customer();
		$this->customer_model->delete_address($id, $customer['id']);
		echo $id;
	}	
	
	public function set_default()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');
	
		$customer = $this->go_cart->customer();
		$save['id'] = $customer['id'];
		
		if($type=='bill')
		{
			$save['default_billing_address'] = $id;

			$customer['bill_address'] = $this->customer_model->get_address($id);
			$customer['default_billing_address'] = $id;
		} else {

			$save['default_shipping_address'] = $id;

			$customer['ship_address'] = $this->customer_model->get_address($id);
			$customer['default_shipping_address'] = $id;
		} 
		
		//update customer db record
		$this->customer_model->save($save);
		
		//update customer session info
		$this->go_cart->save_customer($customer);
		
		echo "1";		
	}
}