<?php 

class Download extends Secure_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->customer_name	=$this->customer['lastname'] .' '.$this->customer['firstname'];
	}
	
	public function index($code=false)
	{
		$this->Customer_model->is_logged_in('secure/download/');

			$this->Customer_model->is_logged_in();
			$this->load->model('Order_model');
			
			$customer					= $this->go_cart->customer();
			$data['customer_name']		= $this->customer_name;
			$data['gift_cards_enabled']	= $this->gift_cards_enabled;
			$data['page_title'] 		= lang('my_downloads');
			
			$offset		=0;
			$total_rows	=0;
			$display_no	=$this->config->item('download_per_page');
			$result		=$this->Order_model->user_downloads($customer['id']);
			if(!empty($result)) $total_rows	=count($result);
			
			if(isset($_GET['display_no'])) $display_no	= $_GET['display_no'];
			if(isset($_GET['per_page']))   $offset		= $_GET['per_page'];
			
			//set up pagination
			$this->load->library('pagination');
			$segments						= $this->uri->total_segments();
			$config['uri_segment']			= $segments;
			$config['per_page']				= $display_no;
			$config['page_query_string']	= TRUE;
			$config['total_rows']			= $total_rows;
			
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;
	
			$config['full_tag_open'] = '<div class="pagination"><span class="page"> page:</span>';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = '<span class="active">';
			$config['cur_tag_close'] = '</span>';
			
			$config['num_tag_open'] = '<span>';
			$config['num_tag_close'] = '</span>';
			
			$config['prev_link'] = FALSE;
			$config['next_link'] =FALSE;
			
			$config['anchor_class']	='class="dwld_paging"';
			
			$is_ajax=1;
			if($_SERVER['QUERY_STRING']=='') $is_ajax=0;	
			$query_string=preg_replace('/&per_page=\d*/is','',$_SERVER['QUERY_STRING']);
			
			$config['base_url']		=site_url('secure/download'). '?'.$query_string;
			
			$this->pagination->initialize($config);

			$data['downloads']		= $this->Order_model->user_downloads($customer['id'],$config['per_page'], $offset);
//			$i=0;
//			foreach($data['downloads'] as $data['downloads']){
//                            echo '<pre>';
//                            //print_r($data['downloads']);
//                            print_r(unserialize($data['downloads']['contents']));
//                            $i++;
//                        }exit;
//                        
			
			$data['page_count']		= ceil($this->pagination->total_rows/$this->pagination->per_page); 
		
		//echo'<pre>';print_r($data);exit;
		($is_ajax==0)?$this->load->view('secure/download/index', $data):$this->load->view('secure/download/pagination', $data);		
	}
	
	function link($orderItemId)
	{
		$this->Customer_model->is_logged_in('secure/download/');
		$customer=$this->go_cart->customer();
		
		if($customer)
		{
			$this->load->model('Order_model');
			$temp=$this->Order_model->get_item_contents($orderItemId,$customer['id']);
                        //echo'<pre>';print_r($temp['bloomstock_type']);exit;
			if(!empty($temp))
			{
			
				$path=$this->get_download_path(isset($temp['radiolist']['value'])?$temp['radiolist']['value']:false, $temp['bloomstock_type']).$temp['dealer_code'].'/'.$temp['download_file'];
				$exists = $this->__remoteFileExists($path);
				if (!$exists) {
						$this->session->set_flashdata('error',lang('file_doesnot_exist'));
						redirect(site_url('secure/download/index'));
				}

				if(!$path)  show_404();
				
				$extension=substr(strrchr($temp['download_file'], '.'), 1);
				$filename=$temp['sku'].'.'.$extension;

				$this->__force_download($path,$filename,$extension);
				
			}
			else show_404();
		}
		else show_404();
	}	
	
	
	function __force_download($path, $filename,$extension)
	{
		// Load the mime types
		if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/mimes.php'))
		{
			include(APPPATH.'config/'.ENVIRONMENT.'/mimes.php');
		}
		elseif (is_file(APPPATH.'config/mimes.php'))
		{
			include(APPPATH.'config/mimes.php');
		}

		// Set a default mime if we can't find it
		if ( ! isset($mimes[$extension]))
		{
			$mime = 'application/octet-stream';
		}
		else
		{
			$mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
		}
		
		//header('Content-Type:image/jpg');	
		header('Content-Type: "'.$mime.'"');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		//header("Content-Transfer-Encoding: binary");
		//header('Expires: 0');
		//header('Pragma: no-cache');
		readfile($path);
		//header("Content-Length: ".strlen($data));

	}
	
	
	function __remoteFileExists($url) 
	{
		$curl = curl_init($url);
	
		//don't fetch the actual page, you only want to check the connection is ok
		curl_setopt($curl, CURLOPT_NOBODY, true);
	
		//do request
		$result = curl_exec($curl);
	
		$ret = false;
	
		//if request did not fail
		if ($result !== false) {
			//if request was ok, check response code
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
	
			if ($statusCode == 200) {
				$ret = true;   
			}
		}
	
		curl_close($curl);
	
		return $ret;
	}

	
	function get_download_path($size=false, $bloomstock_type)
	{
		$path='';
		$this->load->model('Settings_model');
		$config=(array)$this->Settings_model->get_config(1);
		
		if($bloomstock_type==0 && $size!=false)
		{
			if($size==$this->config->item('high_value'))
			{
				$path=$config['high_image_path'];
			}
			else if($size==$this->config->item('medium_value'))
			{
				$path=$config['medium_image_path'];
			}
			else if($size==$this->config->item('low_value'))
			{
				$path=$config['low_image_path'];
			}
		}
		else if($bloomstock_type==1 && $size!=false)
		{
			if($size==$this->config->item('high_value'))
			{
				$path=$config['high_video_path'];
			}
			else if($size==$this->config->item('low_value'))
			{
				$path=$config['low_video_path'];
			}
		}
		else if($bloomstock_type==2)
		{
			$path=$config['music_path'];
		}
		
		return $path; 
	}
		
	function request($order_id,$order_item_id)
	{
		$this->load->model(array('Request_model','Order_model'));
		
		$this->Customer_model->is_logged_in('secure/download/');
		
		$customer=$this->go_cart->customer();
		if($customer)
		{
			$order=$this->Order_model->get_order($order_id);
			
			if($order->customer_id!=$customer['id']) // Whether requesting customer and the order's customer is same
			{
				$this->session->set_flashdata('error',lang('invalid_order'));
				redirect(site_url('secure/download/index'));
			}
			
			if(strtotime($order->expiry_on) > strtotime(date('Y-m-d H:i:s')))// Rechecking the expiry date 
			{
				$this->session->set_flashdata('error',lang('invalid_request'));
				redirect(site_url('secure/download/index'));
			}
			
			$temp=$this->Request_model->get_request_by_order($order_id,array('status'=>0));//Checking if request for this order is already sent
			if(!empty($temp))
			{
				$this->session->set_flashdata('error',lang('request_already_sent'));
				redirect(site_url('secure/download/index'));
			}
			

			
			$request['order_id']		=$order_id;
			$request['order_item_id']	=$order_item_id;
			$request['customer_id']		=$customer['id'];
			
			$id=$this->Request_model->save($request);
			if(isset($id)) 
			{
				$this->session->set_flashdata('message', lang('request_sent_success'));
				redirect(site_url('secure/download/index'));
			}
			else 
			{
				$this->session->set_flashdata('error',lang('invalid_order'));
				redirect(site_url('secure/download/index'));
			}
		}
		else 
		{
			$this->session->set_flashdata('error',lang('invalid_user'));
			redirect(site_url('secure/download/index'));
		}
	}
}