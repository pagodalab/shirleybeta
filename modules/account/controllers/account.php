<?php 

class Account extends Member_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
		$this->load->model('canvas/user_canvas_model');
	  	$this->db->where('user_id',$this->customer['id']);
	  	$data['user_canvas'] = $this->user_canvas_model->getUserCanvas(array('user_canvas.status'=>1),'user_canvas_id desc',NULL)->result_array();
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		$data['customer']			= (array)$this->customer_model->get_customer($this->customer['id']);
		$data['addresses'] 			= $this->customer_model->get_address_list($this->customer['id']);
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
                
		$data['customer_addresses']	= $this->customer_model->get_address_list($data['customer']['id']);
		$this->view('account/account/index', $data);
	}
	
	public function edit()
	{
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		$data['customer']			= (array)$this->customer_model->get_customer($this->customer['id']);
		$data['customer_type']		=$data['customer']['group_id'];	
		$data['page_title']			= lang('edit_account_info');

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('firstname', 'lang:account_firstname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'lang:account_lastname', 'trim|required|max_length[32]');
		
		$this->form_validation->set_rules('company', 'lang:account_company', 'trim|max_length[128]');
		
		$this->form_validation->set_rules('phone', 'lang:phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		//$this->form_validation->set_rules('address1', 'Address', 'trim|required');
		//$this->form_validation->set_rules('address2', 'Address', 'trim');
		//$this->form_validation->set_rules('city', 'City', 'trim');
		//$this->form_validation->set_rules('zip', 'Zip', 'trim');
		//$this->form_validation->set_rules('country_id', 'Country', 'trim|required');
		//$this->form_validation->set_rules('zone_id', 'Zone', 'trim');


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('location/location_model');
			$data['error'] = validation_errors();
			$data['countries'] = $this->location_model->get_countries_menu();
			$this->view('account/account/edit', $data);
		}
		else
		{
			$customer = array();
			$customer['id']					= $this->customer['id'];
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['company']				= $this->input->post('company');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			//$customer['address1']				= $this->input->post('address1');
			//$customer['address2']				= $this->input->post('address2');
			//$customer['zip']					= $this->input->post('zip');
			//$customer['city']					= $this->input->post('city');
			//$customer['zone_id']				= $this->input->post('zone_id');
			//$customer['country_id']				= $this->input->post('country_id');
			//$customer['is_profile_complete']		= 1;
			
			
			//$customer['question']				= $this->input->post('question');
			//$customer['answer']					= $this->input->post('answer');
			//echo "<pre> Cont "; print_r($customer); //exit;
			$this->go_cart->save_customer($this->customer);
			$this->customer_model->save($customer);
			
			$this->session->set_flashdata('message', lang('message_account_updated'));
			if($this->session->userdata('redirect_url'))
			{
				$redirect=$this->session->userdata('redirect_url');
				$this->session->unset_userdata('redirect_url');
				redirect($redirect);
			}
			else
			{
				redirect('account');
			}
		}
	}
	
	public function password()
	{
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['page_title']			= 'Change Password';

		$this->load->library('form_validation');	
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
		$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE)
		{
			$data['error']=validation_errors();//	echo 'acvvvv';exit;
			$this->view('account/account/password', $data);
		}
		else
		{
			$this->load->helper('string');
               $this->load->library('email');

			$customer = array();
			$customer['id']				= $this->customer['id'];
			$customer['password']			= $this->input->post('password');
			$changed_pass		= $this->input->post('confirm');
			
			$this->go_cart->save_customer($this->customer);
			$this->customer_model->save($customer);
                        /* send an email */
                        $config['mailtype'] = 'html';
			$this->email->initialize($config);	
			// get the email template
			$res = $this->db->where('id', '12')->get('tbl_canned_messages');
			$row = $res->row_array();
                        $this->load->library('session');
//                        echo'<pre>';
//                        print_r($this->config->item('email'));
//                        exit;
                        //{customer_name}
                        $row['content'] = str_replace('{customer_name}', $this->customer['lastname'].' '. $this->customer['firstname'], $row['content']);
			// password
			$row['content'] = str_replace('{password}',$changed_pass, $row['content']);
                        //current year
			$row['content'] = str_replace('{current_yr}', date('Y'), $row['content']);
                        
                        $this->email->from($this->config->item('email'), $this->config->item('company_name'));
			$this->email->to($this->customer['email']);
                        $this->email->subject(mb_convert_encoding($row['subject'],'UTF-8'));
			$this->email->message(html_entity_decode($row['content']));
			
			$this->email->send();

			
			$this->session->set_flashdata('message', lang('message_account_updated'));
			
			redirect('account');
		}	
	}
	
	
	public function newsletter()
	{
		$this->customer_model->is_logged_in('account/');
		
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['page_title']			= 'Newsletter Subscription';
		$data['customer']			= (array)$this->customer_model->get_customer($this->customer['id']);	
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules('email_subscribe', 'Subscribe', 'trim|numeric|max_length[1]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('account/account/newsletter', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['email_subscribe']			= $this->input->post('email_subscribe');
			
			$this->go_cart->save_customer($this->customer);
			$this->customer_model->save($customer);
			
			$this->session->set_flashdata('message', lang('message_account_updated'));
			
			redirect('account');
		}			
	}
	

}