<?php 

class Wishlist extends Member_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->customer_name	=$this->customer['lastname'] .' '.$this->customer['firstname'];
		$this->load->model('share_list/share_list_model');
		$this->load->model('share_list/sharelist_product_model');
		$this->load->model('canvas/user_canvas_model');
	}
	
	public function index()
	{
		$data['products']=array();
		$data['page_title']	= lang('wish_list');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['customer_name']		=$this->customer_name;
		
		$customer_id = $this->go_cart->customer('id');
		$data['share_list'] = $this->share_list_model->getShareLists(array('customer_id'=>$customer_id))->row_array();


		$this->product_model->joins = array('BRANDS');
		foreach ($this->go_cart->get_wishlist() as $key => $product_id) {				
				$data['products'][] = $this->product_model->get_product($product_id);
				
		}
		/*Extracting canvas wishlist*/
		$this->db->where('id',$this->go_cart->customer('id'));
		$customer=$this->customer_model->getCustomers()->row_array();

		$canvas = unserialize($customer['canvas_wishlist']);
		
		if($canvas)
		{
			foreach($canvas as $key => $id)
			{
				$this->db->where('user_canvas_id',$id);
				$data['canvases'][] = $this->user_canvas_model->getUserCanvas()->row_array();

			}
		}	
		
		
		$this->view('account/wishlist/index', $data);
	}
	
	public function delete()
	{
			$key = array_search($this->input->get('id'), $this->go_cart->get_wishlist());
			
			if ($key !== false) {
				$wishlist=$this->go_cart->get_wishlist();
				unset($wishlist[$key]);
				$this->go_cart->set_wishlist($wishlist);
			}
		
			redirect(site_url('account/wishlist'));		
	}

	public function delete_canvas()
	{
		$this->db->where('id',$this->go_cart->customer('id'));
		$customer=$this->customer_model->getCustomers()->row_array();
		
		$canvas = unserialize($customer['canvas_wishlist']);

		$key = array_search($this->input->get('id'),$canvas);

		if ($key !== false) {
				
				unset($canvas[$key]);
				
				$wishlist=serialize($canvas);
				$this->customer_model->update('CUSTOMER',array('canvas_wishlist'=>$wishlist),array('id'=>$this->go_cart->customer('id')));
			}
		
			redirect(site_url('account/wishlist'));		
	}
	
	public function addToShareList()
	{
		//echo "<pre>"; print_r($_POST); exit;	
		$list_name = $this->input->post('list_name');
		$product_id = $this->input->post('product_id');
		if(!empty($product_id))
		{
			$share_list['list_name'] = $list_name;
			$share_list['customer_id'] = $this->go_cart->customer('id');
			$share_list['share_date'] = date('Y-m-d h:i:s');
			$share_list['share_unique_id'] = uniqid();
			$share_list_id = $this->share_list_model->save($share_list);
			
			if(is_array($product_id))
			{	
				//$this->sharelist_product_model->delete_share_list($share_list_id);
				
				foreach($product_id as $product)
				{
					$share_list_product['share_list_id'] = $share_list_id;
					$share_list_product['product_id'] = $product;
					
					$success = $this->sharelist_product_model->save($share_list_product);
				}
			}
		}
		
		if($success)
		{
			$success = TRUE;
		}
		else
		{
			$success = FALSE;
		}
		echo json_encode(array('success'=>$success));
	}
	
	public function share_lists()
	{
		$data['page_title']	= 'Share List';
		$data['customer_name']		=$this->customer_name;
		
		$customer_id = $this->go_cart->customer('id');
		$data['share_lists'] = $this->share_list_model->getShareLists(array('customer_id'=>$customer_id))->result_array();
		
		$this->view('account/wishlist/share_list', $data);
	}

}