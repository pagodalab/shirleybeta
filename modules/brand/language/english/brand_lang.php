<?php


$lang['brand_id'] = 'Brand Id';
$lang['brand_name'] = 'Brand Name';
$lang['address'] = 'Address';
$lang['city'] = 'City';
$lang['contact_person'] = 'Contact Person';
$lang['phone'] = 'Phone';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['added_date'] = 'Added Date';
$lang['status'] = 'Status';

$lang['create_brand']='Create Brand';
$lang['edit_brand']='Edit Brand';
$lang['delete_brand']='Delete Brand';
$lang['brand_search']='Brand Search';

$lang['brand']='Brand';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

