<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('brand/brand_model');
        $this->lang->load('brand/brand');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'brand';
		$data['page_title'] = 'Brands';
		$this->view($this->config->item('admin_folder').'/brand/index', $data);	
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->brand_model->count();
		paging('brand_id');
		$this->_get_search_param();	
		$rows=$this->brand_model->getBrands()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['brand_name']!='')?$this->db->like('brand_name',$params['search']['brand_name']):'';
			($params['search']['address']!='')?$this->db->like('address',$params['search']['address']):'';
			($params['search']['city']!='')?$this->db->like('city',$params['search']['city']):'';
			($params['search']['contact_person']!='')?$this->db->like('contact_person',$params['search']['contact_person']):'';
			($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
			($params['search']['mobile']!='')?$this->db->like('mobile',$params['search']['mobile']):'';
			($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  
	}
    
	public function combo_json()
    {
		$rows=$this->brand_model->getBrands()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->brand_model->delete('BRANDS',array('brand_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('brand_id'))
        {
			$data['added_date'] =date('Y-m-d H:i:s');		
            $success=$this->brand_model->insert('BRANDS',$data);
        }
        else
        {
			$data['modified_date'] =date('Y-m-d H:i:s');			
            $success=$this->brand_model->update('BRANDS',$data,array('brand_id'=>$data['brand_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        	$data['brand_id'] = $this->input->post('brand_id');
		$data['brand_name'] = $this->input->post('brand_name');
		$data['address'] = $this->input->post('address');
		$data['city'] = $this->input->post('city');
		$data['contact_person'] = $this->input->post('contact_person');
		$data['phone'] = $this->input->post('phone');
		$data['mobile'] = $this->input->post('mobile');
		$data['email'] = $this->input->post('email');
		$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}