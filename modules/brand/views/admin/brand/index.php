<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('brand_search')?>',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="brand-search-form">
<table width="100%">
<tr><td><label><?=lang('brand_name')?>:</label></td>
<td><input type="text" name="search[brand_name]" id="search_brand_name"  class="easyui-validatebox"/></td>
<td><label><?=lang('address')?>:</label></td>
<td><input type="text" name="search[address]" id="search_address"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?=lang('city')?>:</label></td>
<td><input type="text" name="search[city]" id="search_city"  class="easyui-validatebox"/></td>
<td><label><?=lang('contact_person')?>:</label></td>
<td><input type="text" name="search[contact_person]" id="search_contact_person"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?=lang('phone')?>:</label></td>
<td><input type="text" name="search[phone]" id="search_phone"  class="easyui-validatebox"/></td>
<td><label><?=lang('mobile')?>:</label></td>
<td><input type="text" name="search[mobile]" id="search_mobile"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?=lang('email')?>:</label></td>
<td><input type="text" name="search[email]" id="search_email"  class="easyui-validatebox"/></td>
<td><label><?=lang('status')?>:</label></td>
<td><label class="radio inline"><input type="radio" name="search[status]" id="search_status1" value="1"/> <?=lang('general_yes')?> </label>
<label class="radio inline"><input type="radio" name="search[status]" id="search_status0" value="0"/> <?=lang('general_no')?> </label></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search icon-inverse"></i></span> <?php  echo lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="brand-table" data-options="pagination:true,title:'<?php  echo lang('brand')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="brand_id" sortable="true" width="30"><?=lang('brand_id')?></th><?php */?>
<th field="brand_name" sortable="true" width="50"><?=lang('brand_name')?></th>
<th field="address" sortable="true" width="50"><?=lang('address')?></th>
<?php /*?><th field="city" sortable="true" width="50"><?=lang('city')?></th><?php */?>
<th field="contact_person" sortable="true" width="50"><?=lang('contact_person')?></th>
<th field="phone" sortable="true" width="50"><?=lang('phone')?></th>
<th field="mobile" sortable="true" width="50"><?=lang('mobile')?></th>
<th field="email" sortable="true" width="50"><?=lang('email')?></th>
<th field="added_date" sortable="true" width="50"><?=lang('added_date')?></th>
<th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?=lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Add Brand"><span><i class="icon-plus icon-inverse"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Brand"><span><i class="icon-trash icon-inverse"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit brand form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-brand" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?=lang('brand_name')?>:</label></td>
					  <td width="66%"><input type="text" name="brand_name" id="brand_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('address')?>:</label></td>
					  <td width="66%"><input type="text" name="address" id="address" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('city')?>:</label></td>
					  <td width="66%"><input type="text" name="city" id="city" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('contact_person')?>:</label></td>
					  <td width="66%"><input type="text" name="contact_person" id="contact_person" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('phone')?>:</label></td>
					  <td width="66%"><input type="text" name="phone" id="phone" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('mobile')?>:</label></td>
					  <td width="66%"><input type="text" name="mobile" id="mobile" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('email')?>:</label></td>
					  <td width="66%"><input type="text" name="email" id="email" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('status')?>:</label></td>
					  <td width="66%"><label class="radio inline"><input type="radio" value="1" name="status" id="status1" /><?=lang("general_yes")?></label> <label class="radio inline"><input type="radio" value="0" name="status" id="status0" /><?=lang("general_no")?></label></td>
		       </tr><input type="hidden" name="brand_id" id="brand_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->

<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#brand-search-form').form('clear');
			$('#brand-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#brand-table').datagrid({
				queryParams:{data:$('#brand-search-form').serialize()}
				});
		});		
		$('#brand-table').datagrid({
			url:'<?php  echo site_url('brand/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_brand')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removebrand('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_brand')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-brand').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_brand')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#brand-table').datagrid('getRows')[index];
		if (row){
			$('#form-brand').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_brand')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removebrand(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#brand-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('brand/admin/delete_json')?>', {id:[row.brand_id]}, function(){
					$('#brand-table').datagrid('deleteRow', index);
					$('#brand-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#brand-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].brand_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('brand/admin/delete_json')?>',{id:selected},function(data){
						$('#brand-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-brand').form('submit',{
			url: '<?php  echo site_url('brand/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-brand').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#brand-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>