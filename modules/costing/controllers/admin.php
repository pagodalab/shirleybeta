<?php

class Admin extends Admin_Controller { 
    
    function __construct()
    {       
        parent::__construct();
        
        $this->authenticate->check_access('Admin', true);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Settings_model');
		$this->load->model('exchangerate/exchangerate_model');
    }
    
    public function index()
    {
        //we're going to use flash data and redirect() after form submissions to stop people from refreshing and duplicating submissions
        //$this->session->set_flashdata('message', 'this is our message');
        
		$data['page_title'] = "Costing";
		$data['costing'] = $this->Settings_model->get('costing');
		$data['exchangerate'] = $this->exchangerate_model->getExchangerates(array('rate_date'=>date('Y-m-d')))->row_array();
		$this->view($this->config->item('admin_folder').'/costing_form', $data);
    }
	
	public function save()
	{
		$data['margin_50'] = $this->input->post('margin_50');
		$data['margin_50_100'] = $this->input->post('margin_50_100');
		$data['margin_100_500'] = $this->input->post('margin_100_500');
		$data['margin_500_1000'] = $this->input->post('margin_500_1000');
		$data['margin_1000_5000'] = $this->input->post('margin_1000_5000');
		$data['margin_5000_25000'] = $this->input->post('margin_5000_25000');
		$data['margin_above_25000'] = $this->input->post('margin_above_25000');
		$data['admin_charge'] = $this->input->post('admin_charge');
		$data['merchant_charge'] = $this->input->post('merchant_charge');
		$data['processing_fee'] = $this->input->post('processing_fee');
		$data['export_path'] = $this->input->post('export_path');
		$data['hk_path'] = $this->input->post('hk_path');
		$data['uk_path'] = $this->input->post('uk_path');
		
		$exchangerate['rate'] = $this->input->post('exchange_rate');
		$rate_id = $this->input->post('rate_id');
		
		$this->Settings_model->save('costing', $data);
		$success = $this->exchangerate_model->update('EXCHANGERATE',$exchangerate,array('id'=>$rate_id));
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));	
		
	}
	
}