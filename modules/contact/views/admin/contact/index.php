<div region="center" border="false">
	<div style="padding:20px">

		<br/>
		<table id="contact-table" data-options="pagination:true,title:'<?php  echo lang('contact')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',fitColumns:true">
			<thead>
				<th field="checkbox" checkbox="true"></th>
				<th field="name" sortable="true" width="20"><?php echo lang('name')?></th>
				<th field="email" sortable="true" width="15"><?php echo lang('email')?></th>
				<th field="message" sortable="true" width="20"  ><?php echo lang('message')?></th>
				<th field="created_date" sortable="true" width="10"><?php echo lang('created_date')?></th>
				<th field="action" width="5" formatter="getActions"><?php  echo lang('action')?></th>

			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_contact')?>"><?php  echo lang('create')?></a> -->
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_contact')?>"><?php  echo lang('remove_selected')?></a>
			</p>
		</div> 

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#contact-search-form').form('clear');
			$('#contact-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#contact-table').datagrid({
				queryParams:{data:$('#contact-search-form').serialize()}
			});
		});		
		$('#contact-table').datagrid({
			url:'<?php  echo site_url('contact/admin/contact/json')?>',
			height:'auto',
			width:'1088',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var d = '<a href="#" onclick="removecontact('+index+')" class="btn btn-default"  title="<?php  echo lang('delete_contact')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return d;		
	}

	function removecontact(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#contact-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('contact/admin/contact/delete_json')?>', {id:[row.contact_id]}, function(){
					$('#contact-table').datagrid('deleteRow', index);
					$('#contact-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#contact-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].contact_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('contact/admin/contact/delete_json')?>',{id:selected},function(data){
						$('#contact-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	
	
</script>