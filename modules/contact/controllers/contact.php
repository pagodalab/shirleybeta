<?php
class Contact extends Front_Controller {

	var $site_settings;

	function __construct()
	{
		parent::__construct();	
		$this->load->helper(array('form','date','url'));
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->bep_site->set_crumb('Home','');
		$this->bep_site->set_crumb('Contact','contact');
		$this->bep_assets->load_asset_group('VALIDATE');
		$this->load->model('Settings_model');
		$this->load->model('contact/contact_model');;
		$this->load->model('Messages_model');

		$this->site_settings = $this->Settings_model->get('gocart');
		
		
	}
	
	function index()
	{
		$this->form_validation->set_rules('name','Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required|email');
		$this->form_validation->set_rules('message','Message','trim|required');	
		// $this->form_validation->set_rules('recaptcha_response_field','Captcha','trim|required|callback_valid_captcha');
		
		if($this->form_validation->run()===FALSE)
		{
			$data['header'] = 'Contact Us';
			//$data['captcha'] = $this->auth_form_processing->public_captcha();
			$data['gocart'] = $this->site_settings;

			$this->view( "contact/index",$data);	
		}
		else
		{

			$data = array();
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['message'] = $this->input->post('message');
			$data['created_date'] = date("Y-m-d H:i:s");

			$success = $this->contact_model->insert("CONTACTS",$data);

			if($success) {
				$success = $this->_send_mail();
			} 
			redirect(site_url('contact'));

			
		}

	}
	
	
	
	function _send_mail()
	{
		$msg_template = $this->Messages_model->get_message(12);

		$subject = $msg_template['subject'];
		$message = $msg_template['content'];

		$config['mailtype'] = 'html';
		$config['protocol'] = 'sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['wordwrap'] = TRUE;
		// $config['smtp_host'] = 'smtp.vianet.com.np';

		$this->email->initialize($config);

		//Sends to user		
		$this->email->clear(TRUE);

		$this->email->from($this->site_settings['email'], $this->site_settings['company_name']);
		$this->email->to('anish@pagodalabs.com');
		// $this->email->to($this->input->post('email'));
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();		

		//Email to Admin
		$this->email->clear(TRUE);
		$this->email->from($this->site_settings['email'], $this->site_settings['company_name']);
		$this->email->to('anish@pagodalabs.com');
		// $this->email->to($this->site_settings['email']);
		$subject="Contact from :: " .$this->input->post('name');

		$message="The following person has contacted";
		$message.="<br/>=====================================";
		$message.="<br/>Sender Name: " . $this->input->post('name');
		$message.="<br/>Sender Email: " . $this->input->post('email');			
		$message.="<br/>Sender Message: " . $this->input->post('message');

		$this->email->send();		
	}
	
	/*private function _generate_captcha()
	{
		$this->bep_assets->load_asset('recaptcha');
		$this->load->module_library('recaptcha','Recaptcha');
		return $this->recaptcha->recaptcha_get_html();
	}*/

/*	public function valid_captcha()
	{
		 Make sure the captcha library is loaded
		$this->load->module_library('recaptcha','Recaptcha');
		$this->form_validation->set_message('valid_captcha', $this->lang->line('userlib_validation_captcha'));
		 Perform check
		$this->recaptcha->recaptcha_check_answer($this->input->server('REMOTE_ADDR'), $this->input->post('recaptcha_challenge_field'), $this->input->post('recaptcha_response_field'));

		return $this->recaptcha->is_valid;	
	}*/
	
	
	function valid_captcha_bk()
	{
		// Make sure the captcha library is loaded
		$this->load->module_library('recaptcha','Recaptcha');
		$this->form_validation->set_message('valid_captcha', $this->lang->line('userlib_validation_captcha'));
		// Perform check
		$this->recaptcha->recaptcha_check_answer($this->input->server('REMOTE_ADDR'), $this->input->post('recaptcha_challenge_field'), $this->input->post('recaptcha_response_field'));

		return $this->recaptcha->is_valid;	
	}
	function valid_captcha()
	{
		$this->form_validation->set_message('valid_captcha', $this->lang->line('userlib_validation_captcha'));
		if($this->session->userdata('security_number')==$this->input->post('recaptcha_response_field')){
                   // echo "here";exit;
			return true;	
		}

		return false;
	}
	
	function spare_email($email)
	{
		$query = $this->user_model->fetch('Users',NULL,NULL,array('email'=>$email));

		// Set the error message
		$this->form_validation->set_message('spare_email', $this->lang->line('userlib_validation_email'));

		return ($query->num_rows() == 0) ? TRUE : FALSE;
	}	

	function captchaImage(){
		$this->session->unset_userdata('security_number');
		$this->session->set_userdata('security_number',rand(10000,99999));
		$img=imagecreatefromjpeg(FCPATH ."assets/img/captcha/texture.jpg");
		$image_text = $this->session->userdata('security_number');

		$red=rand(100,255);
		$green=rand(100,255);
		$blue=rand(100,255);

		$text_color=imagecolorallocate($img,255-$red,255-$green,255-$blue);

		$text=imagettftext($img, 16, rand(-10,10), rand(10,30), rand(25,35), $text_color,
			FCPATH ."assets/img/captcha/fonts/courbd.ttf", $image_text);

		header("Content-type:image/jpeg");
		header("Content-Disposition:inline ; filename=secure.jpg");
		imagejpeg($img);

	}

	
}