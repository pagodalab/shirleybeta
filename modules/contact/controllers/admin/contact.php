<?php

class Contact extends Admin_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('contact/contact_model');;
		$this->lang->load('contact');
//$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		$data['page_title'] = 'Contact Us';
// 		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'contact';
		$this->view($this->config->item('template_admin') . "admin/contact/index",$data);	
	}

	public function json()
	{
		$this->db->where('del_flag', null);
		$total=$this->contact_model->count();
		paging('id');
		$this->db->where('del_flag', null);
		$rows=$this->contact_model->getContacts()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->contact_model->update('CONTACTS',array('del_flag'=>1, 'deleted_date'=>date("Y-m-d H:i:s")),array('id'=>$row));
			endforeach;
		}
	}    


}