<?php


$lang['contact_id'] = 'Contact Id';
$lang['company_name'] = 'Company Name';
$lang['company_full_name'] = 'Company Full Name';
$lang['bio'] = 'Bio';
$lang['address'] = 'Address';
$lang['phone'] = 'Phone';
$lang['fax'] = 'Fax';
$lang['website'] = 'Website';
$lang['contact_person_phone'] = 'Contact Person Phone';
$lang['contact_person'] = 'Contact Person';
$lang['skype'] = 'Skype';
$lang['language'] = 'Language';
$lang['deals_in'] = 'Deals In';
$lang['business_type'] = 'Business Type';
$lang['logo'] = 'Logo';
$lang['estd_year'] = 'Estd Year';
$lang['contact_type'] = 'Contact Type';
$lang['about_link'] = 'About Link';
$lang['site'] = 'Site';
$lang['action'] = 'Actions';

$lang['create_contact']='Create Contact';
$lang['edit_contact']='Edit Contact';
$lang['delete_contact']='Delete Contact';
$lang['contact_search']='Contact Search';

$lang['name']='Name';
$lang['email']='Email';
$lang['message']='Message';
$lang['created_date']='Created At';

$lang['contact']='Contact';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

