
<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->authenticate->check_access('Admin', true);
        $this->lang->load('exchangerate');
        $this->load->model('exchangerate/exchangerate_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['page_title'] = 'Exchangerate';
		$page = $this->config->item('admin_folder') . "/exchangerate/index";
		$this->view($page,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->exchangerate_model->count();
		paging('id DESC');
		$this->_get_search_param();	
		$rows=$this->exchangerate_model->getExchangerates()->result_array();
		//echo "<pre>"; print_r($this->db->last_query());
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['dollar']!='')?$this->db->like('dollar',$params['search']['dollar']):'';

		}  

		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}

	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->exchangerate_model->getExchangerates()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->exchangerate_model->delete('EXCHANGERATE',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->exchangerate_model->insert('EXCHANGERATE',$data);
        }
        else
        {
            $success=$this->exchangerate_model->update('EXCHANGERATE',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
		$data['rate_date'] = $this->input->post('rate_date');
		$data['rate'] = $this->input->post('rate');

        return $data;
   }
   
   public function todays_rate()
   {
	   $date =  date('Y-m-d');
		$rate['id'] = $this->input->post('id');
		$rate['rate_date'] = $date;
		$rate['rate_euro'] = $this->today_euro_rate()[1];
		$rate['rate_dollar'] = $this->today_dollar_rate()[1];
		$rate['rate_yen'] = $this->today_japanese_rate()[1];
		$rate['rate_hdollar'] = $this->today_hongkong_rate()[1];
		$rate['rate_korea'] = $this->today_korean_rate()[1];
		$rate['rate_adollar'] = $this->today_australlian_rate()[1];
        $rate['rate_pound'] = $this->today_uk_rate()[1];


		//echo "<pre>...."; print_r($rate); exit;
		$query = $this->exchangerate_model->fetch('EXCHANGERATE',NULL,NULL,array('rate_date'=>$date));
		if($query->num_rows() > 0)
		{
			$id = $this->exchangerate_model->getRecentId()->row_array();
			$rate['id'] = $id['MAX(id)'];
			$success=$this->exchangerate_model->update('EXCHANGERATE',$rate,array('id'=>$rate['id']));
		}
		else
		{
        	$success=$this->exchangerate_model->insert('EXCHANGERATE',$rate);
		}
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));

		return TRUE;
	
   }
	    public function today_euro_rate()
	    {
	    	// $date = date('Y-m-d');
		$from = 'EUR';
		$to = 'DKK';
		$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
		$handle = fopen($url, 'r');
		 
		if ($handle) {
		    $result = fgetcsv($handle);
		    fclose($handle);
		}
		//echo "<pre>>>>"; print_r($result); 
 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }
        public function today_uk_rate()
	    {
	    	// $date = date('Y-m-d');
		$from = 'GBP';
		$to = 'DKK';
		$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
		$handle = fopen($url, 'r');
		 
		if ($handle) {
		    $result = fgetcsv($handle);
		    fclose($handle);
		}
		//echo "<pre>>>>"; print_r($result); 
 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }
	     public function today_dollar_rate()
	    {
	    	// $date = date('Y-m-d');
			$from = 'USD';
			$to = 'DKK';
			$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			}
			//echo "<pre>>>>"; print_r($result); 
	 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }
	     public function today_japanese_rate()
	    {
	    	// $date = date('Y-m-d');
			$from = 'JPY';
			$to = 'DKK';
			$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			}
			//echo "<pre>>>>"; print_r($result); 
	 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }
	    public function today_hongkong_rate()
	    {
	    	// $date = date('Y-m-d');
			$from = 'HKD';
			$to = 'DKK';
			$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			}
			//echo "<pre>>>>"; print_r($result); 
	 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }
	     public function today_korean_rate()
	    {
	    	// $date = date('Y-m-d');
			$from = 'KRW';
			$to = 'DKK';
			$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			}
			//echo "<pre>>>>"; print_r($result); 
	 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }

	    public function today_australlian_rate()
	    {
	    	// $date = date('Y-m-d');
			$from = 'AUD';
			$to = 'DKK';
			$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=c4l1&s='.$to.$from.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			}
			//echo "<pre>>>>"; print_r($result); 
	 		//echo '1 '.$to.' is worth '.$result[1].' '.$from.' Based on data on '.$date;// exit;
			return $result;
	    }

	    // public function front_currency()
	    // {
	    // 	$currency = $this->input->post('currency');
	    // 	$date =  date('Y-m-d');
	    // 	$rows=$this->exchangerate_model->getExchangerates(array('rate_date'=>$date))->result_array();
	    // 	if($rows)
	    // 	{
	    // 		$this->session->set_userdata('currency',$currency);
	    // 		 echo json_encode(array('success'=>TRUE));
	    		
	    // 	}
	    // 	else
	    // 	{
	    // 		$success = $this->todays_rate();
	    // 		if($success == TRUE)
	    // 		{
	    // 			$this->session->set_userdata('currency',$currency);
	    // 			 echo json_encode(array('success'=>TRUE));
	    // 		}
	    // 	}
	    	
	    // }


}