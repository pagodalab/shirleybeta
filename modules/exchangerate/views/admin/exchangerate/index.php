
<div region="center" border="false">
<div style="padding:20px">
<?php /*?><div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('exchangerate_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="exchangerate-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?=lang('rate_date')?></label>:</td>
<td><input type="text" name="date[rate_date][from]" id="search_rate_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[rate_date][to]" id="search_rate_date_to"  class="easyui-datebox"/></td>
<td><label><?=lang('dollar')?></label>:</td>
<td><input type="text" name="search[dollar]" id="search_dollar"  class="easyui-validatebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search" iconCls="icon-search"><?php  echo lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear" iconCls="icon-clear">Clear</a>
    </td>
    </tr>
</table>

</form>
</div><?php */?>
<br/>
<table id="exchangerate-table" data-options="pagination:true,title:'<?php  echo lang('exchangerate')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="id" sortable="true" width="30"><?=lang('id')?></th>
    <th field="rate_date" sortable="true" width="50">Date</th>
    <th field="rate_euro" sortable="true" width="50">Rate_euro</th>
    <th field="rate_dollar" sortable="true" width="50">Rate_dollar</th>
    <th field="rate_yen" sortable="true" width="50">Rate_japanese</th>
    <th field="rate_hdollar" sortable="true" width="50">Rate_hongkong</th>
    <th field="rate_korea" sortable="true" width="50">Rate_korea</th>
    <th field="rate_adollar" sortable="true" width="50">Rate_australlia</th>
    <th field="rate_pound" sortable="true" width="50">Rate_pound</th>

 


    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="<?php  echo lang('create_exchangerate')?>"><span><i class="icon-plus"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small load-btn" onclick="update()" title="Today's Exchange Rate"><span><i class="dollar"></i></span> Today's Rate</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="<?php  echo lang('delete_exchangerate')?>"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit exchangerate form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-exchangerate" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?=lang('rate_date')?>:</label></td>
					  <td width="66%"><input name="rate_date" id="rate_date" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('dollar')?>:</label></td>
					  <td width="66%"><input name="rate" id="rate" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->


   
</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){
		$('#clear').click(function(){
			$('#exchangerate-search-form').form('clear');
			$('#exchangerate-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#exchangerate-table').datagrid({
				queryParams:{data:$('#exchangerate-search-form').serialize()}
				});
		});		
		$('#exchangerate-table').datagrid({
			url:'<?php  echo site_url('exchangerate/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_exchangerate')?>"><span><i class="icon-pencil"></i></span> </a>';
		var d = '<a href="#" onclick="removeexchangerate('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_exchangerate')?>"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-exchangerate').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_exchangerate')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#exchangerate-table').datagrid('getRows')[index];
		if (row){
			$('#form-exchangerate').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_exchangerate')?>');
		}
		else
		{
			$.messager.alert('Error','Error');				
		}		
	}
	
	function update()
	{
		$.get('<?php echo site_url('exchangerate/admin/todays_rate')?>',null,function(data){
				//console.log(data);
				
				var today = '<?php echo date('Y-m-d')?>';
				if(data.success == true)
				{
				
					
					alert("Successfully Generated Todays Exchange Rate");
				}
				else
				{
				
					alert("Error Fetching Exchange Rate!");
				}
				$('#exchangerate-table').datagrid('reload');
			},'json');
	}
		
	function removeexchangerate(index)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#exchangerate-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('exchangerate/admin/delete_json')?>', {id:[row.id]}, function(){
					$('#exchangerate-table').datagrid('deleteRow', index);
					$('#exchangerate-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#exchangerate-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
				if(r){				
					$.post('<?php  echo site_url('exchangerate/admin/delete_json')?>',{id:selected},function(data){
						$('#exchangerate-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','Error');	
		}
		
	}
	
	function save()
	{
		$('#form-exchangerate').form('submit',{
			url: '<?php  echo site_url('exchangerate/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-exchangerate').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: 'Success',msg: result.msg});
					$('#exchangerate-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: 'Error',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>