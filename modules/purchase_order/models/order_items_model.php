<?php
Class order_items_model extends MY_Model
{
	var $joins=array();
	function __construct()
	{
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('ORDER_ITEM'=>$this->prefix.'order_items','ORDER'=>$this->prefix.'orders',
							 'PRODUCT'=>$this->prefix.'products','CUSTOMER'=>$this->prefix.'customers','BRAND'=>$this->prefix.'brands');
		$this->_JOINS=array('ORDER'=>array('join_type'=>'LEFT','join_field'=>'orders.id=order_items.order_id',
                                           'select'=>"orders.order_number,orders.customer_id,orders.ordered_on,orders.exchange_rate,concat(orders.ship_lastname,' ', orders.ship_firstname) as ship_name,orders.ship_email,orders.ship_phone,orders.ship_address1",'alias'=>'orders'),
							'PRODUCT'=>array('join_type'=>'LEFT','join_field'=>'products.id=order_items.product_id',
                                           'select'=>"products.name,products.sku,products.price,products.description,products.price,products.brand,products.images,products.is_assortment",'alias'=>'products'),
						   'BRAND'=>array('join_type'=>'LEFT','join_field'=>'products.brand=brands.brand_id',
						   					'select'=>"brands.brand_name,brands.brand_id,brands.contact_person,brands.address,brands.phone,brands.mobile,brands.email",'alias'=>'brands'),
							'CUSTOMER'=>array('join_type'=>'LEFT','join_field'=>'orders.customer_id=customers.id',
                                           'select'=>"concat(customers.lastname,' ', customers.firstname) as customer_name,customers.firstname,customers.lastname,customers.email as cust_email,customers.phone as cust_phone",'alias'=>'customers'),
                            );        
    }
	
	public function getOrderItems($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='order_items.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ORDER_ITEM']. ' order_items');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['ORDER_ITEM'].' order_items');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
	
	public function getPurchaseOrders($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='order_items.*,sum(order_items.quantity)as qnty,group_concat(orders.order_number) as order_numbers';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ORDER_ITEM']. ' order_items');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	public function getOrdersSummary($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='order_items.*,sum(order_items.quantity)as qnty,group_concat(orders.order_number) as order_numbers
	   ,(unit_cost*(sum(order_items.quantity)))as sub_total,((unit_cost*(sum(order_items.quantity)))/exchange_rate)as roi';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ORDER_ITEM']. ' order_items');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
}