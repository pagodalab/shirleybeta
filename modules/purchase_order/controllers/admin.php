<?php

class Admin extends Admin_Controller {	

	function __construct()
	{		
		parent::__construct();
		$this->authenticate->check_access('Admin', true);
		$this->load->model('purchase_order/order_items_model');
		$this->load->model('order/order_model');
		$this->load->model('product/product_model');
		$this->load->model('product/assorted_product_model');
		$this->lang->load('order');
	}
	
	function index()
	{
		$data['page_title'] = 'Purchase Order';
		$this->view($this->config->item('admin_folder').'/index', $data);
	}
		
	public function json()
	{
		$this->order_items_model->joins = array('ORDER','CUSTOMER','PRODUCT','BRAND');
		$this->_get_search_param();
		$this->_get_query_param();
		$total = $this->order_items_model->count();
		paging('order_items.id desc');
		//$this->order_items_model->joins = array('ORDER','PRODUCT','CUSTOMER','BRAND');
		$this->_get_search_param();
		$this->_get_query_param();
		$rows = $this->order_items_model->getOrderItems()->result_array();
		//echo "<pre>.."; print_r($this->db->last_query()); exit;
		foreach($rows as $row)
		{
			if($row['is_assortment'] == 1)
			{
				$assorted_details = array(	
											'order_id'=>$row['order_id'],
											'order_number'=>$row['order_number'],
											'brand_name'=>$row['brand_name'],
											'contact_person'=>$row['contact_person'],
											'phone'=>$row['phone'],
											'email'=>$row['email'],
											'customer_name'=>$row['customer_name'],
											'cust_phone'=>$row['cust_phone'],
											'cust_email'=>$row['cust_email'],
											'ship_name'=>$row['ship_name'],
											'ship_address1'=>$row['ship_address1'],
											'ship_phone'=>$row['ship_phone'],
											'ship_email'=>$row['ship_email'],
											'purchase_status'=>$row['purchase_status'],
											'ordered_on'=>$row['ordered_on']
										);
				$products = $this->assorted_product_model->getAssortedProductsById(array('product_id'=>$row['product_id']))->result_array();
				//echo "<pre> Query..."; print_r($this->db->last_query());
				//echo "<pre> Result..."; print_r($products); //exit;
				foreach($products as $product)
				{
					//echo "Value : "; print_r($product['assorted_id']);
					$assorted = $this->product_model->getProducts(array('id'=>$product['assorted_id']))->row_array();
					$assorted_merged[] = array_merge($assorted,$assorted_details);
					//echo "<pre> Assorted"; print_r($assorted); exit;
					//echo "<pre> Parent"; print_r($assorted_details);
					
				}
				//echo "<pre> Assorted Merged"; print_r($assorted_merged); exit;
			}
			
		}
		if(!empty($assorted_merged))
		{
			$merged = array_merge($rows,$assorted_merged);
		}
		else
		{
			$merged = $rows;
		}
		//echo "<pre>"; print_r($this->db->last_query());
		//echo "<pre>"; print_r($assorted);
		//echo "<pre>"; print_r($merged); exit;
		//echo "<pre>"; print_r($products);
		echo json_encode(array('total'=>$total,'rows'=>$this->gridData($merged)));
	}
	
	public function _get_query_param()
	{
		$id = $this->input->get('id');
		if($id)
		{
			$this->db->where(array('order_id'=>$id));	
		}
	}
	
	private function gridData($rows)
	{
		$data=array();
		foreach($rows as $row)
		{
			
			$row['images']=$this->getMainImage($row['images']);
			$data[]=$row;			
			
		}
		return $data;
	}
	
	private function getMainImage($images)
	{
               
			   $photo  = '<img class="responsiveImage" src="'.theme_img('no_picture.png',FALSE).'" style="height:100px;width:100px" />';
				if($images != 'false')
				{
					
				
					$images	= array_values((array)json_decode($images));
							   

						if(!empty($images[0]))
						{
							$primary    = $images[0];
							foreach($images as $photo)
							{
								if(isset($photo->primary))
								{
									$primary    = $photo;
								}
							}
		
							$photo  = '<img class="responsiveImage" src="'.base_url('uploads/images/medium/'.$primary->filename).'" style="height:100px;width:100px" />';
						}
				}
               return $photo;		
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['firstname']!='')?$this->db->like('customers.firstname',$params['search']['firstname']):'';
			($params['search']['lastname']!='')?$this->db->like('customers.lastname',$params['search']['lastname']):'';
			($params['search']['brand_id']!='')?$this->db->where('brands.brand_id',$params['search']['brand_id']):'';
			($params['search']['status']!='')?$this->db->where('order_items.purchase_status',$params['search']['status']):'';
			($params['search']['order_number']!='')?$this->db->where('orders.order_number',$params['search']['order_number']):'';
			($params['search']['exchange_rate']!='')?$this->db->like('orders.exchange_rate',$params['search']['exchange_rate']):'';
			
			if(!empty($params['date']))
			{
				foreach($params['date'] as $key=>$value){
					$this->_datewise($key,$value['from'],$value['to']);	
				}
			}
	
		}
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}
	
	public function detail($id)
	{
		$this->bep_assets->load_asset_group('DATETIMEPICKER');
		$this->load->model('product/option_model');
		$data['page_title'] = 'Purchase Order Detail' ;
		$this->order_items_model->joins = array('PRODUCT');
		$data['detail'] = $this->order_items_model->getOrderItems(array('order_items.id'=>$id))->row_array();
		//echo "<pre> Detail "; print_r($this->db->last_query());
		$data['product_options']=$this->option_model->get_product_options($data['detail']['product_id']);
		$page = $this->config->item('admin_folder').'/detail';
		
		$this->view($page,$data);
	}
	
	public function save()
	{
		
		//echo "<pre> Posted "; print_r($_POST);
		$contents = $this->order_items_model->getOrderItems(array('id'=>$_POST['id']))->row_array();
		$array = unserialize($contents['contents']);
		$posted['post_options'] = $this->input->post('option');
		$posted['options'] = $this->input->post('option');
		$replaced = array_replace($array,$posted);
		$id = $this->input->post('id');
		$data['contents'] = serialize($replaced);
		$data['order_placed_date'] = $this->input->post('order_placed_date');
		$data['purchased_date'] = $this->input->post('purchased_date');
		$data['purchase_status'] = $this->input->post('purchase_status');
		$data['unit_cost'] = $this->input->post('unit_cost'); 
		$data['unit_sellprice'] = $this->input->post('unit_sellprice');
		$data['amount_paid'] = $this->input->post('amount_paid');

		//echo "<pre> Data "; print_r($data); exit;
		$success = $this->order_items_model->update('ORDER_ITEM',$data,array('id'=>$id));

		if($success)
		{
			$this->session->set_flashdata('message', 'Purchase Order Updated');

			//go back to the order item
			redirect(site_url('purchase_order/admin'));
		}
		else
		{
			$this->session->set_flashdata('message', 'Purchase Order Not Updated');
		}
		
	}

	public function delete_json()
	{
		$id=$this->input->post('id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				//$orderItem = $this->order_items_model->getOrderItems(array('id'=>$row))->row_array();
				//$this->order_model->delete('ORDER',array('id'=>$orderItem['order_id']));
				$this->order_items_model->delete('ORDER_ITEM',array('id'=>$row));

			}
		}
	}
	
	public function printPurchaseOrder()
	{
		$this->order_items_model->joins = array('ORDER','PRODUCT','CUSTOMER','BRAND');
		$this->db->group_by('brands.brand_id');
		$this->db->order_by('order_items.id desc');
		$this->_get_url_params();
		$data['orders'] = $this->order_items_model->getOrderItems()->result_array();
		
		$this->_get_url_params();
		$this->db->group_by('order_items.product_id desc');
		$data['summary'] = $this->order_items_model->getOrdersSummary()->result_array();
		
		$page = $this->config->item('admin_template').'purchase_order/print';
		$this->load->view($page,$data);
		
	}
	
	private function _get_url_params()
	{
		$search=$this->input->get('search');
		if($search)
		{
			($search['status']!='')?$this->db->where('order_items.purchase_status',$search['status']):'';
			($search['brand_id']!='')?$this->db->where('brands.brand_id',$search['brand_id']):'';
			($search['order_number']!='')?$this->db->where('orders.order_number',$search['order_number']):'';
			($search['exchange_rate']!='')?$this->db->like('orders.exchange_rate',$search['exchange_rate']):'';
			
		}
	}
	
	public function printDailyProductList()
	{
		$this->order_items_model->joins = array('ORDER','PRODUCT','CUSTOMER','BRAND');
		$this->db->group_by('products.id');
		$this->db->order_by('order_items.id desc');
		$this->_get_url_params();
		$data['products'] = $this->order_items_model->getOrderItems()->result_array();
		$page = $this->config->item('admin_template').'purchase_order/product_list';
		$this->load->view($page,$data);
		
	}
	
	public function printOrders()
	{
		$data['image'] = $this->input->get('image');
		
		$voucher = $this->input->post('voucher');
		$this->order_items_model->joins = array('PRODUCT','BRAND','ORDER');
		$this->db->group_by('products.id');
		$this->db->order_by('order_items.id desc');
		$this->_get_url_params();
		$data['products'] = $this->order_items_model->getPurchaseOrders()->result_array();
		
		//echo "<pre>"; print_r($this->db->last_query());
		$page = $this->config->item('admin_template').'purchase_order/purchase_order';
		$this->load->view($page,$data);
		
	}
	
	public function summary_json()
	{
		$this->order_items_model->joins = array('ORDER','PRODUCT','CUSTOMER','BRAND');
		$this->_get_search_param();
		$this->_get_query_param();
		$this->db->group_by('order_items.product_id desc');
		$rows = $this->order_items_model->getOrdersSummary()->result_array();
		//echo "<pre>"; print_r($this->db->last_query());
		//$sql="select sum(cnt.debit)as debit,sum(cnt.credit) as credit from (".$query.")cnt";
		//$totalamount=$this->db->query($sql)->row_array();
		$total = '';
		foreach($rows as $row)
		{
			$total += $row['sub_total'];
		}
		$footer=array(array('sku'=>'','order_numbers'=>'','qnty'=>'','sub_total'=>$total,'roi'=>''));
		echo json_encode(array('rows'=>$rows,'footer'=>$footer));
				
	}
	
	public function change_status()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('purchase_status');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->order_items_model->update('ORDER_ITEM',array('purchase_status'=>$status),array('id'=>$row));
			}
		}
	}
	
	public function getAssortedProducts()
	{
		$assorted_id = $this->input->post('id');
		$this->assorted_product_model->joins = array('PRODUCT','BRANDS');
		$data['assorted'] = $this->assorted_product_model->getAssortedProducts(array('product_id'=>$assorted_id))->result_array();
		
		$page = $this->config->item('admin_template').'purchase_order/admin/assorted';
		
		$this->load->view($page,$data);
	}
}