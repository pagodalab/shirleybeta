<title>Daily Product List</title>
<style type="text/css">
.button:hover {
background: -moz-linear-gradient(70% 50% 90deg, #0f0f0f, #2a2a2a);
background: -webkit-gradient(linear, left top, left bottom, from(#0f0f0f), to(#2a2a2a));
border: 1px solid #080808;
}

.button {
background: -moz-linear-gradient(70% 50% 90deg, #2a2a2a, #484848);
background: -webkit-gradient(linear, left top, left bottom, from(#2a2a2a), to(#484848));
-moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
border: 1px solid #303030;
padding: 5px 20px 7px 20px;
height: 30px;
font: bold 12px "Myriad Pro","Century Gothic",Arial, Helvetica, sans-serif;
text-transform: uppercase;
color: #fff;
-moz-border-radius: 0.3em;
-webkit-border-radius: 0.3em;
border-radius: 0.3em;
cursor: pointer;
}
.button1 {background: -moz-linear-gradient(70% 50% 90deg, #2a2a2a, #484848);
background: -webkit-gradient(linear, left top, left bottom, from(#2a2a2a), to(#484848));
-moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
border: 1px solid #303030;
padding: 5px 20px 7px 20px;
height: 30px;
font: bold 12px "Myriad Pro","Century Gothic",Arial, Helvetica, sans-serif;
text-transform: uppercase;
color: #fff;
-moz-border-radius: 0.3em;
-webkit-border-radius: 0.3em;
border-radius: 0.3em;
cursor: pointer;
}
.button11 {background: -moz-linear-gradient(70% 50% 90deg, #2a2a2a, #484848);
background: -webkit-gradient(linear, left top, left bottom, from(#2a2a2a), to(#484848));
-moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
border: 1px solid #303030;
padding: 5px 20px 7px 20px;
height: 30px;
font: bold 12px "Myriad Pro","Century Gothic",Arial, Helvetica, sans-serif;
text-transform: uppercase;
color: #fff;
-moz-border-radius: 0.3em;
-webkit-border-radius: 0.3em;
border-radius: 0.3em;
cursor: pointer;
}

#show-border{
	border:#D2D2D2 1px solid;
	padding:10px;
}

table tr td{
	padding-left:10px;	
}

.right-border{
	border-right:#333 1px solid;
}

.right-border-heading{
	border-right:#333 1px solid;
	border-bottom:#333 1px solid;
}
.bottom-border{
	border-bottom:#333 1px solid;
}
.top-border{
	border-top:#333 1px solid;
}
.top-right-border{
	border-right:#333 1px solid;
	border-top:#333 1px solid;
}
.price{
	text-align:right;
	padding-right:10px;	
}

#print_note{
  margin-bottom: 5px;
}
</style>
<div style="font-family: Arial;"> 
  <!--  start table-content  -->
  <div id="table-content">
  	<h3 align="center">Daily Product List
    	<br/>
        Printed On <?php echo date('Y-m-d h:i:s')?>
    </h3>
      <table border="1px" cellpadding="0" cellspacing="0" width="100%" id="purchase_order-table">
         	<tr>
            	<th>Product Description</th>
                <th>Image</th>
            </tr>
            <?php foreach($products as $product){?>
            <tr>
            	<td>
                	<label><?php echo $product['sku']?> (<strong>Order: </strong><?php echo $product['order_number']?>)</label>
                    <br/>
                    <label><?php echo $product['name']?></label>
                    <br/>
                    <label><?php echo $product['description']?></label>
                    <br/>
                    <label><?php echo 'NRs.: '.$product['price']?></label>
                    <br/>
                    <label><?php echo 'Quantity.: '.$product['quantity']?></label>
                    <br/>
                    <label><?php echo '<strong>Vendor: </strong>'.$product['brand_name']. '<strong>  Contact Person:  </strong>'.$product['contact_person'].' <strong> Contact No.:  </strong>'.$product['phone']?></label>
                </td>
                <td><?php 
						if($product['images'] !== 'false')
						{
							$img = array_values((array)json_decode($product['images']));
							$image = $img[0]->filename;
						 ?>
							<img src="<?php echo base_url('uploads/images/full/'.$image)?>">
						 <?php }else{ ?>
							<img class="responsiveImage" src="<?php echo (theme_img('no_picture.png',FALSE))?>"/>
						 <?php }?>
                </td>
            </tr>
            <?php }?>
      </table>
    <br/>
  </div>
    <br/>
    <br/>
    <br/>
</div>

<script>
	window.print();
</script>