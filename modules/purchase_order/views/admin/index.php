<div id="search-panel" class="easyui-panel" data-options="title:'Purchase Order Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="purchase_order-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr>
	<td><label>Customer Name : </label></td>
    <td><input type="text" name="search[firstname]" id="search_first_name"  class="easyui-validatebox" placeholder="First name"/> <input type="text" name="search[lastname]" id="search_last_name"  class="easyui-validatebox" placeholder="Last name"/></td>
    <td><label>Order Number : </label></td>
    <td><input type="text" name="search[order_number]" id="search_order_number"  class="easyui-validatebox"/></td>
</tr>
<tr>
	<td><label>Brand : </label></td>
    <td><input type="text" name="search[brand_id]" id="search_brand_id"  class="easyui-combobox"/></td>
    <td><label>Status : </label></td>
    <td>
    	<select name="search[status]" id="search_status"  class="easyui-combobox"/>
        	<option value=""></option>
            <option value="Pending">Pending</option>
            <option value="Shopped">Shopped</option>
            <option value="Not Available">Not Available</option>
        </select>
    </td>
</tr>
<tr>
	<td><label>Exchange Rate : </label></td>
    <td><input type="text" name="search[exchange_rate]" id="search_exchange_rate" class="easyui-validatebox"></td>
    <td><label>Date : </label></td>
    <td><input name="date[purchased_date][from]" id="search_purchased_date_from" class="easyui-datebox"> ~ <input name="date[purchased_date][to]" id="search_purchased_date_to" class="easyui-datebox"></td>
</tr>
  <tr>
    <td colspan="4">
        <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?php  echo lang('search')?></a>  
        <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="purchase_order-table" data-options="pagination:true,title:'Purchase Order',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="images" sortable="true" width="50" formatter="formatImage">Image</th>
    <th field="order_number" sortable="true" width="50">Order Number</th>
    <th field="name" sortable="true" width="50">Product</th>
    <th field="sku" sortable="true" width="50">Product Code</th>
    <th field="qnty" sortable="true" width="25">Quantity</th>
    <th field="price" sortable="true" width="25">Amount</th>
    <th field="brand_name" sortable="true" width="50" formatter="formatBrand">Brand</th>
    <?php /*?><th field="contact_person" sortable="true" width="50">Contact Name</th>
    <th field="address" sortable="true" width="50">Address</th><?php */?>
    <th field="customer_name" sortable="true" width="50" formatter="formatCustomer">Customer</th>
    <th field="ship_name" sortable="true" width="50" formatter="formatReceiver">Receiver</th>
    <th field="purchase_status" sortable="true" width="25">Status</th>
    <th field="ordered_on" sortable="true" width="25">Date</th>
    <th field="action" width="50" formatter="getActions">Actions</th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
		<?php /*?><a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Add Order Item"><span><i class="icon-plus"></i></span> Create</a><?php */?>
        <a href="javascript:void(0)" id="print-button" class="tooltip-top btn btn-small" title="Print" target="_blank"><span><i class="icon-print icon-inverse"></i></span> Print</a>
        <a href="javascript:void(0)" id="print_products_button" class="tooltip-top btn btn-small" title="Print" target="_blank"><span><i class="icon-list"></i></span> Daily Product List</a>
        <label class="checkbox inline"><input type="checkbox" name="show_image" id="show_image"> Image</label> 
        <label class="checkbox inline"><input type="checkbox" name="voucher" id="voucher"> Voucher</label> 
<a href="javascript:void(0)" id="purchase-order-button" class="tooltip-top btn btn-small" title="Purchase Order" target="_blank"><span><i class="icon-list-alt"></i></span> Purchase Order</a>        <br/>
        <select name="purchase_status" id="purchase_status" class="easyui-combobox" onchange="enableChangeStatus()">
        	<option value="">Select Status</option>
            <option value="Pending">Pending</option>
            <option value="Shopped">Shopped</option>
            <option value="Not Available">Not Available</option>
        </select>
        <a href="#" class="tooltip-top btn btn-small" onclick="changeStatus()" title="Change Status"><span><i class="icon-cog"></i></span> Change Status</a>
   <?php /*?> <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()" title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a><?php */?>
    </p>

</div> 
<br/>
<table id="summary-table" class="easyui-datagrid" title="Summary" style="width:700px;height:220px" data-options="fitColumns: true,singleSelect: true,rownumbers: true,showFooter: true,collapsible:true,collapsed:false">
        <thead>
            <tr>
                <th data-options="field:'sku',width:80">Product Code</th>
                <th data-options="field:'order_numbers',width:120">Order Number/s</th>
                <th data-options="field:'qnty',width:80,align:'right'">Quantity</th>
                <th data-options="field:'sub_total',width:80,align:'right'">Total (NRs.)</th>
                <th data-options="field:'roi',width:50">ROI($)</th>
            </tr>
        </thead>
    </table>
    
 <div id="product_dlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 	 <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Product Detail</h3>
     </div>
     <div class="modal-body">
        <table>
            <tr>
                <td><strong>Product Code : </strong></td>
                <td><label id="p_code"></label></td>
            </tr>
            <tr>
                <td><strong>Title : </strong></td>
                <td><label id="p_name"></label></td>
            </tr>
            <tr>
                <td><strong>Brand : </strong></td>
                <td><label id="p_brand"></label></td>
            </tr>
            <tr>
                <td><strong>Description : </strong></td>
                <td><label id="p_desc"></label></td>
            </tr>
            <tr>
            	<td><strong>Assorted Products : </strong></td>
                <td>
                	<div id="assorted_products"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Have your personal message written on the cake with maximum 8 words.</strong></td>
            </tr>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span><i class="icon-remove-sign icon-white"></i></span> Close</button>
    </div>
</div>

<script language="javascript" type="text/javascript">
	$(function(){
		$('#search_brand_id').combobox({url:'<?php echo site_url('brand/admin/combo_json')?>',valueField:'brand_id',textField:'brand_name'});
		
		$('#clear').click(function(){
			$('#purchase_order-search-form').form('clear');
			$('#purchase_order-table').datagrid({
				queryParams:null
				});
			$('#summary-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#purchase_order-table').datagrid({
				queryParams:{data:$('#purchase_order-search-form').serialize()}
				});
			$('#summary-table').datagrid({
				queryParams:{data:$('#purchase_order-search-form').serialize()}
			});
		});		
		$('#purchase_order-table').datagrid({
			url:'<?php  echo site_url('purchase_order/admin/json?'.$_SERVER['QUERY_STRING'])?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			},
			rowStyler: function(index,row){
			if (row.purchase_status == 'Shopped'){
				return 'background-color:green;color:#fff;'; // return inline style
				// the function can return predefined css class and inline style
				// return {class:'r1', style:{'color:#fff'}};	
				}
			}
			
		});
		
		$('#summary-table').datagrid({
			url:'<?php  echo site_url('purchase_order/admin/summary_json?'.$_SERVER['QUERY_STRING'])?>',
			height:'auto',
			width:'auto'
		});
		
		$('#purchase-order-button').on('click',function(){
			//window.location.target="_blank";
			var params=$('#purchase_order-search-form').serialize();
			if($('#show_image').is(':checked'))
			{
				params+='&image=1';
			}
			//window.location.href='<?php echo site_url('purchase_order/admin/printOrders')?>?'+params;
			var url='<?php echo site_url('purchase_order/admin/printOrders')?>?'+params;
			$(this).attr('href',url);
			return true;
			
		});
		
		$('#print_products_button').on('click',function(){
			//window.location.target="_blank";
			var params=$('#purchase_order-search-form').serialize();
			var url='<?php echo site_url('purchase_order/admin/printDailyProductList')?>?'+params;
			$(this).attr('href',url);
			return true;
			
		});
		
		$('#print-button').on('click',function(){
			//window.location.target="_blank";
			var params=$('#purchase_order-search-form').serialize();
			var url='<?php echo site_url('purchase_order/admin/printPurchaseOrder')?>?'+params;
			$(this).attr('href',url);
			return true;
			
		});
		
	});
	
	function getActions(value,row,index)
	{
		//console.log('Row contents', row.order_id);
		if(row.order_id)
		{
			var e = '<a href="<?php echo site_url('purchase_order/admin/detail/')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit Order Item"><span><i class="icon-pencil"></i></span> </a>';
		}
		/*var d = '<a href="#" onclick="removepurchase_order('+index+')" class="tooltip-top btn btn-small" title="Delete Order Item"><span><i class="icon-trash"></i></span> </a>';*/
		return e;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}
	
	function formatImage(value,row,index)
	{
		//console.log('Values',value);
		//console.log('Row',row);
		var img = '<a href="javascript:void(0)" onclick="showDetail('+ index +')">'+value+'</a>';
		
		
		return img;
		//return value;
	}
	
	function formatBrand(value,row,index)
	{
		//console.log('Row data',row);
		//console.log('Row content',row.contents); //return false;
		if(row.brand == 0)
		{
			var info = 'Not Available!';
		}
		else
		{
			var info = '<p>'+row.brand_name+'<br/>'+row.contact_person+'<br/>'+row.phone+'<br/>'+row.email+'</p>';
			//var info = 'Brand Found.';
		}
		return info;

	}
	
	function formatCustomer(value,row,index)
	{
		//console.log('Row data',row); //return false;
		var info = '<p>'+row.customer_name+'<br/>'+row.cust_phone+'<br/>'+row.cust_email+'</p>'
		
		return info;

	}
	
	function formatReceiver(value,row,index)
	{
		//console.log('Row data',row); //return false;
		var info = '<p>'+row.ship_name+'<br/>'+row.ship_address1+'<br/>'+row.ship_phone+'<br/>'+row.ship_email+'</p>'
		
		return info;

	}
	
	/*function formatCost(value,row,index)
	{
		//console.log('rows ..',row);
		var total = row.unit_cost*row.qnty ;
		return total;
	}*/
	
	/*function formatDollarCost(value,row,index)
	{
		//console.log('rows ..',row.exchange_rate);
		var total = row.unit_cost*row.qnty ;
		var dollar_price = total/row.exchange_rate;
		//alert('Doller Rate '+dollar_price);
		return parseFloat(dollar_price).toFixed(2);
	}*/

	function edit(index)
	{
		var row = $('#purchase_order-table').datagrid('getRows')[index];
		if (row){
			$('#form-purchase_order').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','Edit Order Item');
		}
		else
		{
			$.messager.alert('Error','Error');				
		}		
	}
	
	function removepurchase_order(index)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#purchase_order-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('purchase_order/admin/delete_json')?>', {id:[row.id]}, function(){
					<?php $this->session->set_flashdata('message', 'Purchase Order Updated');?>
					$('#purchase_order-table').datagrid('deleteRow', index);
					$('#purchase_order-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#purchase_order-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
				if(r){				
					$.post('<?php  echo site_url('purchase_order/admin/delete_json')?>',{id:selected},function(data){
						<?php $this->session->set_flashdata('message', 'Purchase Order Updated');?>
						$('#purchase_order-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','Error');	
		}
		
	}
	
	function save()
	{
		$('#form-purchase_order').form('submit',{
			url: '<?php  echo site_url('purchase_order/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-purchase_order').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: 'Success',msg: result.msg});
					$('#purchase_order-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: 'Error',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	function changeStatus()
	{
		var status = $('#purchase_status').combobox('getValue');
		var rows=$('#purchase_order-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Change?',function(r){
				if(r){				
					$.post('<?php  echo site_url('purchase_order/admin/change_status')?>',{id:selected,purchase_status:status},function(data){
						<?php $this->session->set_flashdata('message', 'Purchase Order Updated');?>
						$('#purchase_order-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No Items Selected!');	
		}
		
	}

	function showDetail(id)
	{
		$('#assorted_products').html('');
		var rows = $('#purchase_order-table').datagrid('getRows');
		var row = rows[id];
		console.log('Assorted ',row.is_assortment);
		console.log('Assorted ',row.order_id);
		$('#p_code').html(row.sku);
		$('#p_name').html(row.name);
		$('#p_desc').html(row.description);
		$('#p_brand').html(row.brand_name);
		if(row.order_id && row.is_assortment == '1')
		{
			console.log('Assorted ',row.product_id);
			$.post('<?php echo site_url('purchase_order/admin/getAssortedProducts')?>',{id:row.product_id},function(data)
			{
				$('#assorted_products').html(data);
			});
		}
		else
		{
			$('#assorted_products').text('No Assorted Products!');
		}
		
		$('#product_dlg').modal();
		return false;
	}
	
</script>