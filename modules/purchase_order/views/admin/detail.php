<div id="item-image">
    <?php 
	if($detail['images'] !== 'false')
	{
		$img = array_values((array)json_decode($detail['images']));
		$image = $img[0]->filename;
	 ?>
     	<img src="<?php echo base_url('uploads/images/full/'.$image)?>">
     <?php }else{ ?>
     	<img class="responsiveImage" src="<?php echo (theme_img('no_picture.png',FALSE))?>"/>
     <?php }?>
     <br/>
     <strong><?php echo $detail['name']?></strong>
</div>
<br/>
<form action="<?php echo site_url('purchase_order/admin/save')?>" method="post">
    <table class="table table-striped">
        <tr>
            <td>Content : </td>
            <td>
                <!--<textarea name="content" style="height:150px;width:500px">-->
                    <?php /* if(isset($detail['contents']))
                    {
                        $product=unserialize($detail['contents']);
                        print_r($product);
                        foreach($product['options'] as $name=>$value){
                        ?>
                             <?php echo $name ?>: <input type="text" name="options[<?php $name?>]" value="<?php echo $value?>"/><br/>
                        <?php
                        }
                    }*/
                    //$db_options = $this->option_model->get_product_options($detail['id']);
                    //print_r($product_options);
                    //echo '<pre>';
                    if(!empty($detail['contents']))
                    {
                 
                    $product=unserialize($detail['contents']);
                    //print_r($product['post_options']);
                    $post_options=$product['post_options'];
                    //print_r(array_keys($product['post_options']));
                    
                    foreach($product_options as $option){
                        //print_r($options);
                        if(in_array($option->id,array_keys($product['post_options']))){
                    ?>
                   
     <div class="control-group">
                                    <label class="control-label"><?php echo $option->name;?></label>
                                    <?php
                                    /*
                                    this is where we generate the options and either use default values, or previously posted variables
                                    that we either returned for errors, or in some other releases of Cart the user may be editing
                                    and entry in their cart.
                                    */
    
                                    //if we're dealing with a textfield or text area, grab the option value and store it in value
                                    if($option->type == 'checklist')
                                    {
                                        $value  = array();
                                        if($post_options && isset($post_options[$option->id]))
                                        {
                                            $value  = $post_options[$option->id];
                                        }
                                    }
                                    else
                                    {
                                        if(isset($option->values[0]))
                                        {
                                            $value  = $option->values[0]->value;
                                            if($post_options && isset($post_options[$option->id]))
                                            {
                                                $value  = $post_options[$option->id];
                                            }
                                        }
                                        else
                                        {
                                            $value = false;
                                        }
                                    }
    
                                    if($option->type == 'textfield'):?>
                                        <div class="controls">
                                            <input type="text" name="option[<?php echo $option->id;?>]" value="<?php echo $post_options[$option->id];?>" class="span4"/>
                                            <?php //echo $required;?>
                                        </div>
                                    <?php elseif($option->type == 'textarea'):?>
                                        <div class="controls">
                                            <textarea class="span4" name="option[<?php echo $option->id;?>]"><?php echo $post_options[$option->id];?></textarea>
                                            <?php //echo $required;?>
                                        </div>
                                    <?php elseif($option->type == 'droplist'):?>
                                        <div class="controls">
                                            <select name="option[<?php echo $option->id;?>]">
                                                <option value=""><?php echo lang('choose_option');?></option>
    
                                            <?php foreach ($option->values as $values):
                                                $selected   = '';
                                                if($value == $values->id)
                                                {
                                                    $selected   = ' selected="selected"';
                                                }?>
    
                                                <option<?php echo $selected;?> value="<?php echo $values->id;?>">
                                                    <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                </option>
    
                                            <?php endforeach;?>
                                            </select>
                                            <?php //echo $required;?>
                                        </div>
                                    <?php elseif($option->type == 'radiolist'):?>
                                        <div class="controls">
                                            <?php foreach ($option->values as $values):
    
                                                $checked = '';
                                                if($value == $values->id)
                                                {
                                                    $checked = ' checked="checked"';
                                                }?>
                                                <label class="radio">
                                                    <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
                                                    <?php echo($values->price != 0)?'(+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                </label>
                                            <?php endforeach;?>
                                            <?php //echo $required;?>
                                        </div>
                                    <?php elseif($option->type == 'checklist'):?>
                                        <div class="controls">
                                            <?php foreach ($option->values as $values):
    
                                                $checked = '';
                                                if(in_array($values->id, $value))
                                                {
                                                    $checked = ' checked="checked"';
                                                }?>
                                                <label class="checkbox">
                                                    <input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
                                                    <?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
                                                </label>
                                                
                                            <?php endforeach; ?>
                                        </div>
                                        <?php echo $required;?>
                                    <?php elseif($option->type == 'calendar'):?>  
                                           <div id="datetimepicker2" class="input-append date">
                                                <input data-format="yyyy-MM-dd" type="text" name="option[<?php echo $option->id;?>]" value="<?php echo $post_options[$option->id];?>"></input>
                                                
                                                <span class="add-on">
                                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                  </i>
                                                </span>
                                              </div>
                                            <?php //echo $required;?>
                                                                 
                                    <?php endif;?>
                                    </div>
                    
                    <?php					
                        }
                    }
                    }
                    else
                    {
                        echo "No Content Available!";
                    }
                    ?>
                <!--</textarea>-->
            </td>
        </tr>
        <tr>
            <td>Order Placed Date : </td>
            <td>
                 <div id="datetimepicker3" class="input-append date">
                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="order_placed_date" value="<?php echo $detail['order_placed_date']?>"></input>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                  </div>
        </td>
        </tr>
        <tr>
            <td>Purchase Date : </td>
            <td>
                 <div id="datetimepicker1" class="input-append date">
                    <input data-format="yyyy-MM-dd" type="text" name="purchased_date" value="<?php echo $detail['purchased_date']?>"></input>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                  </div>
        </td>
        </tr>
        <tr>
            <td>Purchase Status : </td>
            <td>
                <select name="purchase_status" id="purchase_status">
                    <option value="">Select</option>
                    <option value="Pending" <?php echo ($detail['purchase_status']=='Pending')?'selected="selected"':''?>>Pending</option>
                    <option value="Shopped" <?php echo ($detail['purchase_status']=='Shopped')?'selected="selected"':''?>>Shopped</option>
                    <option value="Not Available" <?php echo ($detail['purchase_status']=='Not Available')?'selected="selected"':''?>>Not Available</option>
                  </select>
           </td>
        </tr>
        <tr>
            <td>Unit Cost : </td>
            <td><input type="text" name="unit_cost" id="unit_cost" value="<?php echo $detail['unit_cost']?>"></td>
        </tr>
        <tr>
            <td>Unit Selling Price : </td>
            <td><input type="text" name="unit_sellprice" id="unit_sellprice" value="<?php echo $detail['unit_sellprice']?>"></td>
        </tr>
        <tr>
            <td>Amount Paid : </td>
            <td><input type="text" name="amount_paid" id="amount_paid" value="<?php echo $detail['amount_paid']?>"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?php echo site_url('purchase_order/admin')?>" class="btn btn-danger">Cancel</a>
            </td>
        </tr>
        <input type="hidden" name="id" id="id" value="<?php echo $detail['id']?>">
    </table>
</form>
<script>
$(function() {
    $('#datetimepicker1,#datetimepicker2').datetimepicker({
     	pickTime:false
    });
	
	$('#datetimepicker3').datetimepicker({
     
    });
  });
</script>