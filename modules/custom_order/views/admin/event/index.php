<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div style="margin-bottom: 10px">
	<button class="tooltip-top btn btn-small btn-primary" onclick="showediten()">Content English</button>
	<button  class="tooltip-top btn btn-small btn-success" onclick="showeditda()">Content Danish</button>
</div>
<table id="event-table" data-options="pagination:true,title:'Custom Orders',pagesize:'20', rownumbers:true, collapsible:true,fitColumns:true">
	<thead>
		<th field="checkbox" checkbox="true"></th>
		<th field="name" sortable="true" width="80">Name</th>
		<th field="email" sortable="true" width="100">Email</th>
		<th field="color_name_en" sortable="true" width="100">Color</th>
		<th field="style_name_en" sortable="true" width="100">Style Name</th>
		<th field="name_en" sortable="true" width="100">Material</th>
		<th field="size_title_en" sortable="true" width="100">Size</th>


		<th field="created_date" width="100" >Created</th>
		<th field="action" width="100" formatter="getActions">Action</th>
	</thead>
</table>

<div id="restockNotify_time" class="easyui-dialog" title="Custom Order Response" data-options="iconCls:'icon-save',closed: true" style="width:400px;height:200px;padding:10px">
	<form id="form-restockNotify_time" method="post">
		<table>			

			<tr>
				<td width="34%" ><label><?php echo "Select Date"?>:</label></td>
				<td width="66%"><input type="text" name="restock_by" id="restock_by" class="easyui-datebox"></td>
			</tr>

			<input type="hidden" name="order_email" id="order_email">
			<input type="hidden" name="order_id" id="order_id">
		</table>
		<button type="button" id="restockNotify_time-submit" class="btn btn-primary">Submit</button>
	</form>
</div>

<div id="view_custom_order" class="easyui-dialog" title="Custom Orders" data-options="iconCls:'icon-save',closed: true" style="width:400px;height:200px;padding:10px">
	<table class="table table-striped">			
		<thead>
			<tr>
				<th>Sizes</th>
				<th>Materials</th>
				<th>Colors</th>
				<th>Style Names</th>
			</tr>
		</thead>
		<tbody id="custom_order_datas">

		</tbody>

		<input type="hidden" name="order_email" id="order_email">
		<input type="hidden" name="order_id" id="order_id">
	</table>
</div>

<div id="edit_english_content" class="easyui-dialog" title="Custom Orders" data-options="iconCls:'icon-save',closed: true" style="width:800px;height:400px;padding:10px">
	<table class="table table-striped">			
		<!--<div id="english_content"></div>-->
		<form>
			<textarea id="english_content" class="redactor"></textarea>
		</form>  
		<button onclick="english_submit()">Save</button>
	</table>
</div>

<div id="edit_danish_content" class="easyui-dialog" title="Custom Orders" data-options="iconCls:'icon-save',closed: true" style="width:800px;height:400px;padding:10px">
	<table class="table table-striped">	
		<form>
			<textarea id="danish_content" class="redactor"></textarea>
		</form>  
		<button onclick="danish_submit()">Save</button>
	</table>
</div>


<script language="javascript" type="text/javascript">
	$(function(){

		$('#event-table').datagrid({
			url:'<?php echo site_url('custom_order/admin/json')?>',
			height:'auto',
			width:'auto',

		});

	});

	function getActions(value,row,index)
	{   
		var e = '<a href="#"  class="btn btn-xs btn-link" title="send mail"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';

		return e = '';
	}

	/*function view_orders(index)
	{
		var row = $('#event-table').datagrid('getRows')[index]; 
		$('#custom_order_datas').empty();
		$('#view_custom_order').dialog('open').window('setTitle','Custom Orders');
		$.post('<?php echo site_url("custom_order/admin/view_order")?>',{id: row.id},function(data){
			$.each(data.rows, function( key, value ) {
				$('#custom_order_datas').append('<tr><td>'+value.size+'</td><td>'+value.material+'</td><td>'+value.color+'</td><td>'+value.style_name+'</td></tr>');
			});

		},'JSON');
	}*/

	function send_email(index)
	{
		var row = $('#event-table').datagrid('getRows')[index]; 
		if (row){
			$('#form-restockNotify_time').form('load',row);
			$('#order_email').val(row.email);
			$('#order_id').val(row.id);

			// uploadReady(); //Uncomment This function if ajax uploading
			$('#restockNotify_time').dialog('open').window('setTitle','Custom Order Response');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}	
	}

	$('#restockNotify_time-submit').click(function(){
		var restockvalue = $('#restock_by').datebox('getValue');
		if(restockvalue == '') {
			$('#restock_by').focus();
			return;
		}
		var value = $('#form-restockNotify_time').serialize();
		$.post('<?php echo site_url("custom_order/admin/response_mail")?>',value,function(data){
			if(data.sucess)
			{
				$('#restockNotify_time').dialog('close');
				$('#event-table').datagrid('reload');   
			}

		},'JSON');
	});


</script>

<script>

	function showediten(){
		$.post("<?php   echo site_url('custom_order/admin/english_content')?>", function(result){
			console.log(result.data);
			$('#english_content').redactor('set', result.data);
			// $('#').html(result.data);
			$('#edit_english_content').dialog('open').window('setTitle','Custom Orders Content English');
		},'json');

	}
	function showeditda(){
		// $('#edit_danish_content').dialog('open').window('setTitle','Custom Orders');
		$.post("<?php   echo site_url('custom_order/admin/danish_content')?>", function(result){
			console.log(result.data);
			// $('#danish_content').html(result.data);
			$('#danish_content').redactor('set', result.data);
			$('#edit_danish_content').dialog('open').window('setTitle','Custom Orders Content Danish');
		},'json');
	}
	function danish_submit(){
		var content = $('#danish_content').val();
		$.post("<?php   echo site_url('custom_order/admin/save_danish_content')?>",{content:content}, function(result){
			window.location.reload();
		});
		// alert(content);
	}
	function english_submit()
	{
		var content = $('#english_content').val();
		$.post("<?php   echo site_url('custom_order/admin/save_english_content')?>",{content:content}, function(result){
			window.location.reload();
		});
	}
</script>
