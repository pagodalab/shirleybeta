<?php 
class Custom_order extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('custom_order/custom_order_model');
		$this->load->model('custom_order/personal_detail_model');
		$this->load->model('page/page_model');
		$this->load->model('custom_order_relation/custom_order_relation');
		$this->load->model('style/styles_model');
		$this->load->model('size/size_model');
		$this->load->model('color/color_model');
		$this->load->model('material/material_model');
		
	}
	
	public function index()
	{
		// $data['page_title']	= lang('pages');
		// $data['pages']		= $this->page_model->get_pages(array('id'=>9,'parent_id'=>0))->row_array();
		
		$data['styles'] = $this->styles_model->getStyles()->result_array();
		//$data['materials'] = $this->material_model->getMaterials()->result_array();
		//$data['sizes'] = $this->size_model->getSizes()->result_array();
		//$data['colors'] = $this->color_model->getColors()->result_array();
		
		$this->db->select('setting');
		$this->db->where(array('code'=>'custom_order','setting_key'=>'custom_order_en'));
		$en = $this->db->get('tbl_settings')->row_array();
		
		$this->db->select('setting');
		$this->db->where(array('code'=>'custom_order','setting_key'=>'custom_order_da'));
		$da = $this->db->get('tbl_settings')->row_array();
		
		$data['setting_en'] = $en['setting'];
		$data['setting_da'] = $da['setting'];

		$data['page_title']	= 'Customer Order';
		$this->view('custom_order/index', $data);
	}

	public function getRelationJson()
	{
		$key = $this->input->post('key');
		$id = $this->input->post('id');
		$search = $this->input->post('search');
		$search_index = $this->input->post('search_index');

		$this->db->select($search.'_'.$this->culture_code.','.$search_index);	
		$this->db->order_by($search.'_'.$this->culture_code);	
		$this->db->where($key,$id);

		if($this->input->post('style_id')){
			$this->db->where('style_id',$this->input->post('style_id'));
		}
		if($this->input->post('color_id')){
			$this->db->where('color_id',$this->input->post('color_id'));
		}

		$this->db->group_by($search_index);
		$data = $this->db->get('view_custom_order_relation_all')->result_array();

		echo json_encode(array('data'=>$data));
	}

	public function save()
	{
		$data['name'] = $this->input->post('first_name');
		$data['email'] = $this->input->post('email');
		$data['address'] = $this->input->post('address');
		$data['contact'] = $this->input->post('contact');
		$data['style_name'] = $this->input->post('style');
		$data['color'] = $this->input->post('color');
		$data['size'] = $this->input->post('size');
		$data['material'] = $this->input->post('material');

		if(!empty($data)){
			$data['created_date'] = date("Y-m-d H:i:s");
			$success=$this->custom_order_model->insert('CUSTOM_ORDER',$data);
		}
		if($success)
		{
			$this->session->set_userdata('msg_customorder','Request Sent Sucessfully');
		}
		else
		{
			$this->session->set_userdata('msg_customorder','Request Sent Sucessfully');
		}
		
		redirect($_SERVER['HTTP_REFERER']);
		// echo '<pre>'; print_r($data); exit;
	}
	
}	