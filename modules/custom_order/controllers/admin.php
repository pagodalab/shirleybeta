<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('event/event_model');
		$this->load->model('custom_order/custom_order_model');
		$this->load->model('custom_order/personal_detail_model');
		$this->load->model('custom_order/view_order_model');

		$this->load->model('event/event_category_model');
		$this->load->model('event/event_product_model');
		$this->load->model('category/category_model');
		$this->load->helper('form');
		$this->lang->load('event');
	}

	public function index()
	{
		// Display Page
		$data['page_title'] = 'Custom Orders';
	
		$data['module'] = 'events';
		$this->view($this->config->item('template_admin') . "admin/event/index",$data);
	}

	public function json()
	{
		
		//$this->_get_search_param();	
		//$this->event_model->joins = array('BRANDS','CATEGORY_PRODUCT');
		$total=$this->custom_order_model->count();

		//$this->_get_search_param();
		$rows=$this->view_order_model->getView_order(NULL,'id desc')->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function view_order(){
	    $p_id = $this->input->post('id');
	    $rows = $this->custom_order_model->getCustom_orders(array('personal_id'=>$p_id))->result_array();
	    	echo json_encode(array('rows'=>$rows));
	    exit;	
	}
	
	public function response_mail()
	{
	    $email = $this->input->post('order_email');
	    $id = $this->input->post('order_id');
	    $data['response_time'] = $this->input->post('restock_by');
	    $this->load->library('email');
		$this->load->library('parser');

		$row = $this->db->where('id', '11')->get('tbl_canned_messages')->row_array();
	     
	     $parse_data = array(
			'SITE_URL'			=>	'<a href="'.site_url().'">'.site_url().'</a>',
			'RESPONSE_DATE'   => $data['response_time'],
		
		); 
		
		foreach ($row as $v) {
		    $subject = $v['subject'];
		    $body = $v['content'];
		}
	     
	    $body = $this->parser->parse_string($body, $parse_data); 

// 		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		
			$this->email->from('niirose.prjpti@gmail.com', 'Shirley Bredal');
			$this->email->to($email); 
			$this->email->subject($subject);
			$this->email->message(html_entity_decode($body));
			$this->email->send();
			
		$success = $this->personal_detail_model->update('PERSONAL_DETAIL',$data,array('id'=>$id));
		if($success)
		{
		    $sucess = TRUE;
		}
		else
		{
		    $sucess = FALSE;
		}
		echo json_encode(array('sucess'=>$sucess));
		exit;
	}
	/*public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
		
		}  
	}
	
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}*/
	
	public function save()
	{
		$data = $this->_get_posted_data();

		
		if($data['id'])
		{
			
			$success = $this->contact_detail_model->update('CONTACT_DETAIL',$data,array('id'=>$data['id']));
		}
		else
		{
			$success = $this->contact_detail_model->insert('CONTACT_DETAIL',$data);
			$new_event_id = $this->db->insert_id();
			
		}
		
	
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	
	private function _get_posted_data()
	{
		$data = array();
	    $data['address'] = $this->input->post('address');
	    $data['city'] = $this->input->post('city');
	    $data['country'] = $this->input->post('country');
	    $data['phone'] = $this->input->post('phone');
	    $data['mobile'] = $this->input->post('mobile');
		$data['email'] = $this->input->post('email');
		$data['status'] = $this->input->post('status');
		$data['id'] = $this->input->post('id');
		return $data;
	}
	
	public function delete_json()
	{
		//echo "<pre>"; print_r($_POST); exit;
    		$id=$this->input->post('id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->event_category_model->delete('EVENT_CATEGORY',array('event_id'=>$row));
				$success = $this->event_model->delete('EVENT',array('event_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));
		
	}  
	
	/*public function combo_json()
	{
		$rows=$this->event_model->getEvents()->result_array();
		echo json_encode($rows);    	
	} */
	
	public function getCategories()
	{
		$data['event_id'] = ($this->input->post('id'))?$this->input->post('id'):'';
		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'event';
		$this->load->view($this->config->item('template_admin') . "admin/event/categories",$data);	
	}
	
	public function products($id)
	{
		$data['page_title'] = 'Event Products';
		$this->db->order_by('sort_order asc');
		$this->event_product_model->joins = array('PRODUCT');
		$data['products'] = $this->event_product_model->getEventProducts(array('event_id'=>$id,'status'=>1))->result_array();
		$data['event_id'] = $id;
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'event';
		$this->view($this->config->item('template_admin') . "admin/event/products",$data);		
	}
	
	public function organize_products()
	{
		//echo "<pre>"; print_r($_POST);
		$products = $this->input->post('product');
		$sequence = 1;
		foreach($products as $product)
		{
			//$this->db->where('product_id',$product);
			$this->event_product_model->update('EVENT_PRODUCTS', array('sort_order'=>$sequence),array('product_id'=>$product));
			$sequence++;
		} 
		
	}
	
	public function remove_product()
	{
		//echo "<pre>"; print_r($_POST['products']); exit;
    		$id=$this->input->post('id');
		$event_id = $this->input->post('eve_id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$success = $this->event_product_model->delete('EVENT_PRODUCTS',array('event_id'=>$event_id,'product_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
		
	}  
	
	public function english_content()
	{
	    $this->db->select('setting');
		$this->db->where(array('code'=>'custom_order','setting_key'=>'custom_order_en'));
		$en = $this->db->get('tbl_settings')->row_array();
		$data = $en['setting'];
		
		echo json_encode(array('data'=>$data));
	}
	public function danish_content()
	{
	    	$this->db->select('setting');
		$this->db->where(array('code'=>'custom_order','setting_key'=>'custom_order_da'));
		$da = $this->db->get('tbl_settings')->row_array();
		$data = $da['setting'];
		
		echo json_encode(array('data'=>$data));
	}
	public function save_danish_content(){
	    $data['custom_order_da'] = $this->input->post('content');
	    $this->db->set('setting', $data['custom_order_da']); //value that used to update column  
        $this->db->where(array('code'=> 'custom_order','setting_key'=>'custom_order_da')); //which row want to upgrade  
        $success= $this->db->update('tbl_settings');  //table name
        echo json_encode(array('success'=>$success));
        exit;
	} 
    public function save_english_content(){
	    $data['custom_order_en'] = $this->input->post('content');
	     $this->db->set('setting', $data['custom_order_en']); //value that used to update column  
        $this->db->where(array('code'=> 'custom_order','setting_key'=>'custom_order_en')); //which row want to upgrade  
        $success = $this->db->update('tbl_settings');  //table name
        
        echo json_encode(array('success'=>$success));
        exit;
	}
}
