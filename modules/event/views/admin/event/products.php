<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
	create_sortable();	
});
// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};
function create_sortable()
{
	$('#products').sortable({
		scroll: true,
		helper: fixHelper,
		//axis: 'y',
		handle:'.handle',
		update: function(){
			save_sortable();
		}
	});	
	$('#products').sortable('enable');
}

function save_sortable()
{
	serial=$('#products').sortable('serialize');
	
	$.ajax({
		url:'<?php echo site_url('event/admin/organize_products');?>',
		type:'POST',
		data:serial
	});
}

function removeProduct(id,eve_id)
{
	$.messager.confirm('Confirm','Are you sure to Delete?',function(){
		$.post('<?php echo site_url('event/admin/remove_product')?>',{id:[id],eve_id:eve_id},function(data){
			if(data.success)
			{
				$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
				$('#product-'+id).remove();
			}
		},'json');
	});
}

function selectAll(status)
{
	$('.check_product').prop('checked',status);
}

function remove_selected()
{
	var eve_id = <?php echo $event_id?>;
	if($('.check_product:checked').length >0)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(){
			var checked = [];
			$('.check_product:checked').each(function() {
				checked.push($(this).val());
			});
			
			//console.log('checked ',checked); return false;
			$.post('<?php echo site_url('event/admin/remove_product');?>',{id:checked,eve_id:eve_id},function(data){
				if(data.success)
				{
					$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
					$.each(checked,function(i,value){
						$('#product-'+value).remove();
					});
					
				}
			},'json');
		});
	}
	else
	{
		alert('Please select at least one.'); 
		return false;
		
	}
}

</script>

<div style="margin:20px 0;"></div>
<button type="button" onclick="selectAll(true)" class="btn btn-primary">Select All</button> 
<button type="button" onclick="selectAll(false)" class="btn btn-inverse">Clear</button> 
<button type="button" onclick="remove_selected()" class="btn btn-danger">Remove Selected</button> 
     <div id="products" style="margin:0;padding:0;margin-left:10px;">
     <?php $i=1; foreach($products as $product){
           $img = array_values((array)json_decode($product['images']));
           $image = $img[0]->filename;
          ?>
          <div id="product-<?php echo $product['p_id']?>" style="border:1px solid black;float:left;cursor:move;margin:5px;">
          	<div class="handle">
                    <img src="<?php echo base_url('uploads/images/medium/'.$image)?>" alt="" style="width:250px;height:250px"/><br/>
                     <p align="center"> <label class="checkbox inline"><input type="checkbox" name="product" id="product_check<?php echo $product['p_id']?>" class="check_product" value="<?php echo $product['p_id']?>"> <?php echo $product['name']?></label></p>
                    <div align="center" style="margin:3px">
                         <a href="#" onclick="removeProduct(<?php echo $product['p_id']?>,<?php echo $event_id?>)" class="btn btn-danger"><span><i class="icon-remove-sign icon-white"></i></span> Remove</a>
                        
                    </div>
               </div>
          </div>
     <?php $i++; }?>
     <div class="clearfix"></div>
     </div>
     
</div>