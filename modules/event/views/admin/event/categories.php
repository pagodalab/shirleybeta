<ul>
<?php foreach($categories as $category){
	$checked = $this->event_category_model->count(array('category_id'=>$category['id'],'event_id'=>$event_id));

	?>
	<li style="float:left;margin:5px;list-style:none">
		<label class="checkbox inline">
			<input type="checkbox" name="category_id[]" onclick="checkSubs(<?php echo $category['id']?>)" id="category<?php echo $category['id']?>" value="<?php echo $category['id']?>"  <?php echo ($checked)?'checked="checked"':'' ?>> <strong><?php echo $category['name']?></strong>
		</label>
		<?php 
			$cats = $this->category_model->getCategories(array('parent_id'=>$category['id']))->result_array(); 
		if(count($cats)>0)
		{
			foreach($cats as $cat){	
			$is_checked = $this->event_category_model->count(array('category_id'=>$cat['id'],'event_id'=>$event_id));
		?>
		<br/>
		<label class="checkbox inline" style="margin-left:10px">
			<input type="checkbox" name="category_id[]" class="subcategory<?php echo $category['id']?>" value="<?php echo $cat['id']?>"  <?php echo ($is_checked)?'checked="checked"':'' ?>> <?php echo $cat['name']?>
		</label>
		<?php }}?>
	</li>
<?php }?>
</ul>
 <div class="clearfix"></div>