<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->authenticate->check_access('Admin', true);
		
		$this->load->model('event/event_model');
		$this->load->model('event/event_category_model');
		$this->load->model('event/event_product_model');
		$this->load->model('category/category_model');
		$this->load->helper('form');
		$this->lang->load('event');
	}

	public function index()
	{
		// Display Page
		$data['page_title'] = 'Events';
		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'events';
		$this->view($this->config->item('template_admin') . "admin/event/index",$data);
	}

	public function json()
	{
		
		//$this->_get_search_param();	
		//$this->event_model->joins = array('BRANDS','CATEGORY_PRODUCT');
		$total=$this->event_model->countEvents();
		paging('event_id desc');
		//$this->_get_search_param();
		$rows=$this->event_model->getEvents()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	/*public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
		
		}  
	}
	
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}*/
	
	public function save()
	{
		$data = $this->_get_posted_data();
		//echo "<pre>"; print_r($_POST); exit;
		$category_id = $this->input->post('category_id');
		
		if($data['event_id'])
		{
			
			$success = $this->event_model->update('EVENT',$data,array('event_id'=>$data['event_id']));
		}
		else
		{
			$success = $this->event_model->insert('EVENT',$data);
			$new_event_id = $this->db->insert_id();
			
		}
		
		$event_id = (!($data['event_id']))?$new_event_id:$data['event_id'];
		if($category_id)
		{
			if(is_array($category_id))
			{
				if($data['event_id'])
				{
					$this->event_category_model->delete('EVENT_CATEGORY',array('event_id'=>$event_id));
				}
				foreach($category_id as $category)
				{
					$occ_cat['event_id'] = $event_id;
					$occ_cat['category_id'] = $category;
					
					$this->event_category_model->insert('EVENT_CATEGORY',$occ_cat);
				}
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	
	private function _get_posted_data()
	{
		$data = array();
		$data['event_id'] = $this->input->post('event_id');
		$data['event_name'] = $this->input->post('event_name');
		$data['sort_order'] = $this->input->post('sort_order');
		$data['status'] = $this->input->post('status');
		
		return $data;
	}
	
	public function delete_json()
	{
		//echo "<pre>"; print_r($_POST); exit;
    		$id=$this->input->post('id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->event_category_model->delete('EVENT_CATEGORY',array('event_id'=>$row));
				$success = $this->event_model->delete('EVENT',array('event_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));
		
	}  
	
	/*public function combo_json()
	{
		$rows=$this->event_model->getEvents()->result_array();
		echo json_encode($rows);    	
	} */
	
	public function getCategories()
	{
		$data['event_id'] = ($this->input->post('id'))?$this->input->post('id'):'';
		$data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'event';
		$this->load->view($this->config->item('template_admin') . "admin/event/categories",$data);	
	}
	
	public function products($id)
	{
		$data['page_title'] = 'Event Products';
		$this->db->order_by('sort_order asc');
		$this->event_product_model->joins = array('PRODUCT');
		$data['products'] = $this->event_product_model->getEventProducts(array('event_id'=>$id,'status'=>1))->result_array();
		$data['event_id'] = $id;
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'event';
		$this->view($this->config->item('template_admin') . "admin/event/products",$data);		
	}
	
	public function organize_products()
	{
		//echo "<pre>"; print_r($_POST);
		$products = $this->input->post('product');
		$sequence = 1;
		foreach($products as $product)
		{
			//$this->db->where('product_id',$product);
			$this->event_product_model->update('EVENT_PRODUCTS', array('sort_order'=>$sequence),array('product_id'=>$product));
			$sequence++;
		} 
		
	}
	
	public function remove_product()
	{
		//echo "<pre>"; print_r($_POST['products']); exit;
    		$id=$this->input->post('id');
		$event_id = $this->input->post('eve_id');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$success = $this->event_product_model->delete('EVENT_PRODUCTS',array('event_id'=>$event_id,'product_id'=>$row));
			}
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
		
	}  

}
