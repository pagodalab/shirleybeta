<?php
class Event_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('EVENT'=>$this->prefix.'events');
		$this->_JOINS=array('CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=share_list.customer_id',
                                           'select'=>"concat(customers.firstname,' ',customers.lastname)as customer",'alias'=>'customers'),						
                            );        
    }
	
    public function getEvents($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='events.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['EVENT']. ' events');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):'';//$this->db->order_by('products.name', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countEvents($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['EVENT'].' events');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['EVENT'].' events');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	function save($data)
	{
		/*if($data['customer_id'])
		{
			$result = $this->getShareLists(array('customer_id'=>$data['customer_id']))->row_array();
			if(count($result) > 0)
			{
				$this->update('EVENT',array('customer_id'=>$data['customer_id']),$data);
				return $result['share_list_id'];
			}
			else
			{*/
				$this->insert('EVENT',$data);
				return $this->db->insert_id();
			/*}
		}*/
		
	}
	
	function deleteOccasion($id)
	{
		// delete product 
		$this->db->where('event_id', $id);
		$this->db->delete($this->_TABLES['EVENT']);

	}
	
	public function getOccasionById($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='events.event_id';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['EVENT']. ' events');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		//(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.name', 'ASC');

		/*if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}*/
		return $this->db->get();
    }

}