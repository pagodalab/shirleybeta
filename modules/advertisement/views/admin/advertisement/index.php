<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" title="<?=lang('advertisement_search')?>" style="padding:5px" collapsible="true" collapsed="true" iconCls="icon-search">
            <form action="" method="post" id="advertisement-search-form">
                <table width="100%">
                    <tr><td><label><?=lang('title')?></label></td>
                    <td><input type="text" name="search[title]" id="search_title"  class="easyui-validatebox"/></td>
                    <td><label><?=lang('start_date')?></label></td>
                    <td><input type="text" name="date[start_date][from]" id="search_start_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[start_date][to]" id="search_start_date_to"  class="easyui-datebox"/></td>
                    <tr>
                    <td><label><?=lang('end_date')?></label></td>
                    <td><input type="text" name="date[end_date][from]" id="search_end_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[end_date][to]" id="search_end_date_to"  class="easyui-datebox"/></td>
                    <td><label><?=lang('status')?></label></td>
                    <td><label class="radio inline"><input type="radio" name="search[status]" id="search_status1" value="1"/><?=lang('general_yes')?></label>
                    <label class="radio inline"><input type="radio" name="search[status]" id="search_status0" value="0"/><?=lang('general_no')?></label></td>                    
                    </tr>
                    <tr>
                    <td colspan="4">
                    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
                    <a href="#" class="tooltip-top btn btn-small" id="clear" iconCls="icon-clear"><span><i class="icon-clear"></i></span> Clear</a>
                    </td>
                    </tr>
                </table>
            </form>
       </div>
<br/>
        <table id="advertisement-table" pagination="true" title="<?=lang('advertisement')?>" pagesize="20" rownumbers="true" toolbar="#toolbar" collapsible="true" fitColumns="true">
        	<thead>
                <th field="checkbox" checkbox="true"></th>
                <th field="image_name" sortable="true" width="50" formatter="formatImage"><?=lang('image_name')?></th>
                <th field="title" sortable="true" width="100">Title</th>
                <th field="link" sortable="true" width="50" formatter="formatURL"><?=lang('link')?></th>
                <th field="start_date" sortable="true" width="50"><?=lang('start_date')?></th>
                <th field="end_date" sortable="true" width="50"><?=lang('end_date')?></th>
                <th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?=lang('status')?></th>
	            <th field="action" width="100" formatter="getActions"><?=lang('action')?></th>
      	  </thead>
        </table>

        <div id="toolbar" style="padding:5px;height:auto">
            <p>
                <a href="#" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false" onclick="create()" title="Create Advertisement"><span><i class="icon-plus"></i></span> Create</a>
                <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
            </p>
        </div> 

        <!--for create and edit advertisement form-->
        <div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
            <form id="form-advertisement" method="post" >
                <table>
                	<tr>
                        <td width="34%" ><label><?=lang('image_name')?>:</label></td>
                        <td width="66%">
                        	<label id="upload_image_name" style="display:none"></label>
                        	<input name="image_name" id="image_name" type="text" style="display:none"/>
                        	<input type="file" id="upload_image" name="userfile" style="display:block"/>
                        	<a href="#" id="change-image" title="Delete" style="display:none">
                            	<img src="<?=base_url()?>assets/icons/delete.png" border="0"/></a>
                         </td>
                    </tr>

                    <tr>
                        <td width="34%" ><label>Title:</label></td>
                        <td width="66%"><input type="text" name="title" id="title" class="easyui-validatebox" required="true" style="width:200px"></td>
                    </tr>                    <tr>
                        <td width="34%" ><label><?=lang('link')?>:</label></td>
                        <td width="66%"><input type="text" name="link" id="link" class="easyui-validatebox" required="true" style="width:200px"></td>
                    </tr>

                    <tr>
                        <td width="34%" ><label><?=lang('start_date')?>:</label></td>
                        <td width="66%"><input type="text" name="start_date" id="start_date" class="easyui-datebox" required="true"></td>
                    </tr>
                    <tr>
                        <td width="34%" ><label><?=lang('end_date')?>:</label></td>
                        <td width="66%"><input type="text" name="end_date" id="end_date" class="easyui-datebox" required="true"></td>
                    </tr>
<tr>
                        <td width="34%" ><label><?=lang('status')?>:</label></td>
                        <td width="66%"><label class="radio inline"><input type="radio" name="status" id="status1" value="1"/><?=lang("general_yes")?></label> 
                        				<label class="radio inline"><input type="radio" name="status" id="status0" value="0"/><?=lang("general_no")?></label></td>
	                </tr>
    	            <input type="hidden" name="advertisement_id"/>
                </table>
            </form>
            <div id="dlg-buttons">
            <a href="#" class="tooltip-top btn btn-small" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
            <a href="#" class="tooltip-top btn btn-small" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
            </div>    
            </div>
<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#advertisement-search-form').form('clear');
			$('#advertisement-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#advertisement-table').datagrid({
				queryParams:{data:$('#advertisement-search-form').serialize()}
				});
		});		
		$('#advertisement-table').datagrid({
			url:'<?=site_url('advertisement/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
			$('#form-advertisement').form('load',row);
			$('#dlg').window('open').window('setTitle','<?=lang('edit_advertisement')?>');
			}
		});
		
		$('#change-image').click(function(){
			var filename = $('#image_name').val();
			$.messager.confirm('Confirm','Are you sure?',function(r){
				if(r) {
					$.post('<?php echo site_url('advertisement/admin/upload_delete')?>',{ filename: filename },function(){
			
					 $('#image_name').val('');
					 $('#upload_image_name').html('');
					 $('#upload_image_name').hide();
					 $('#change-image').hide();
					 $('#upload_image').show();
					});
				}
			});
		});			
		
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="Edit Advertisement"><span><i class="icon-pencil"></i></span> </a>';
		var d = '<a href="#" onclick="removeAdvertisement('+index+')" class="tooltip-top btn btn-small" title="Delete Advertisement"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatImage(value)
	{
		var i='<img src="<?=base_url()?>uploads/advertisement/thumb/'+value+'" border="0" height="100" width="100"/>';
		
		return i;
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		
		$('#form-advertisement').form('clear');
		uploadReady();
		$('#upload_image').show();
		$('#image_name').val('');
		$('#upload_image_name').html('');
		$('#upload_image_name').hide();
		$('#change-image').hide();	
		$('#status1').attr('checked',true);
		$('#dlg').window('open').window('setTitle','<?=lang('create_advertisement')?>');
	}	

	function edit(index)
	{
		var row = $('#advertisement-table').datagrid('getRows')[index];
		if (row){
			$('#form-advertisement').form('load',row);
			
			if(row.image_name!='')
			{
				$('#upload_image').hide();
				$('#image_name').val(row.image_name);
				$('#upload_image_name').html(row.image_name);
				$('#upload_image_name').show();
				$('#change-image').show();				
			}
			uploadReady();
			
			$('#dlg').window('open').window('setTitle','<?=lang('edit_advertisement')?>');
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeAdvertisement(index)
	{
		$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#advertisement-table').datagrid('getRows')[index];
				$.post('<?=site_url('advertisement/admin/delete_json')?>', {id:[row.advertisement_id]}, function(){
					$('#advertisement-table').datagrid('deleteRow', index);
					$('#advertisement-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#advertisement-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].advertisement_id);
			}
			
			$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?=site_url('advertisement/admin/delete_json')?>',{id:selected},function(data){
						$('#advertisement-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-advertisement').form('submit',{
			url: '<?=site_url('advertisement/admin/form_json')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-advertisement').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?=lang('success')?>',msg: result.msg});
					$('#advertisement-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?=lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	function uploadReady()
	{
		uploader=$('#upload_image');
		new AjaxUpload(uploader, {
			action: '<?=site_url('advertisement/admin/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only JPG, PNG or GIF files are allowed');
					return false;
				}
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#upload_image').hide();
					$('#image_name').val(filename);
					$('#upload_image_name').html(filename);
					$('#upload_image_name').show();
					$('#change-image').show();
				}
			}		
		});		
	}
</script>