<?php

class Admin extends Admin_Controller
{
	protected $uploadPath = 'uploads/advertisement';
	protected $uploadthumbpath= 'uploads/advertisement/thumb/';

	public function __construct(){
    	parent::__construct();
        $this->load->model('advertisement/advertisement_model');
        $this->lang->load('advertisement/advertisement');
		$this->bep_assets->load_asset('jquery.upload');
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'Advertisement';
		$data['page_title'] = 'Advertisement';
		$data['module'] = 'advertisement';
		$this->view($this->config->item('admin_folder') . "/advertisement/index",$data);		
	}

	public function json()
	{
		$this->_get_search_param();
		$total=$this->advertisement_model->countAdvertisements();
		paging('advertisement_id');
		$this->_get_search_param();
		$rows=$this->advertisement_model->getAdvertisements(array('advertisement_flag'=>0))->result_array();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['title']!='')?$this->db->like('title',$params['search']['title']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
		}  
		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}

	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}

	public function combo_json()
    {
		$rows=$this->advertisement_model->getAdvertisements()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{	
		$data['advertisement_flag']=1;
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->advertisement_model->delete('ADVERTISEMENTS',array('advertisement_id'=>$row));
            endforeach;
		}
	}    

	public function form_json()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('advertisement_id'))
        {
			$data['created_date'] = date('Y-m-d H:i:s');
            $success=$this->advertisement_model->insert('ADVERTISEMENTS',$data);
        }
        else
        {
			$data['modified_date'] = date('Y-m-d H:i:s');
            $success=$this->advertisement_model->update('ADVERTISEMENTS',$data,array('advertisement_id'=>$data['advertisement_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('saved_successful'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['advertisement_id'] = $this->input->post('advertisement_id');
		$data['image_name'] = $this->input->post('image_name');
		$data['title'] = $this->input->post('title');
		$data['link'] = $this->input->post('link');
		$data['start_date'] = $this->input->post('start_date');
		$data['end_date'] = $this->input->post('end_date');
		$data['status'] = $this->input->post('status');
		$data['advertisement_flag'] = $this->input->post('advertisement_flag');

        return $data;
   }
   
  function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		$config['encrypt_name']  = TRUE;

		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
		  $this->load->library('image_lib');
		  
 		  $config['image_library'] = 'gd2';
		  $config['source_image'] = $data['full_path'];	  
		  
          $config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
		  $config['maintain_ratio'] = TRUE;
		  $config['height'] =100;
		  $config['width'] = 100;

		  $this->image_lib->initialize($config);
		  $this->image_lib->resize();
		  echo json_encode($data);
	    }
  	}
	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadthumbpath . '/' . $filename);
		@unlink($this->uploadPath . '/' . $filename);
	} 		    
}