<?php


$lang['advertisement_id'] = 'Advertisement Id';
$lang['image_name'] = 'Image';
$lang['title'] = 'Title';
$lang['link'] = 'Link';
$lang['position'] = 'Position';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['created_date'] = 'Created Date';
$lang['modified_date'] = 'Modified Date';
$lang['display_order'] = 'Display Order';
$lang['status'] = 'Status';
$lang['advertisement_flag'] = 'Advertisement Flag';

$lang['create_advertisement']='Create Advertisement';
$lang['edit_advertisement']='Edit Advertisement';
$lang['delete_advertisement']='Delete Advertisement';
$lang['advertisement_search']='Advertisement Search';

$lang['advertisement']='Advertisement';
$lang['action']='Actions';
$lang['saved_successful']='Saved Successful';
$lang['delete_successful']='Delete Successful';

$lang['delete_confirm'] ='Are you sure you want to delete';

