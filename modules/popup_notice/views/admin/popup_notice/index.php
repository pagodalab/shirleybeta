<div region="center" border="false">
	<div style="padding:20px">
		<!-- <div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('popup_notice_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
		<form action="" method="post" id="popup_notice-search-form">
		<table width="100%" border="1" cellspacing="1" cellpadding="1">
		<tr><td><label><?php echo lang('name_en')?></label>:</td>
		<td><input type="text" name="search[name_en]" id="search_name_en"  class="easyui-validatebox"/></td>
		<td><label><?php echo lang('name_da')?></label>:</td>
		<td><input type="text" name="search[name_da]" id="search_name_da"  class="easyui-validatebox"/></td>
		</tr>
		<tr>
		</tr>
		<tr>
		<td colspan="4">
		<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
		<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
		</td>
		</tr>
		</table>

		</form>
		</div>
		<br/> -->
		<table id="popup_notice-table" data-options="pagination:true,title:'<?php  echo lang('popup_notice')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
			<thead>
				<th data-options="field:'checkbox',checkbox:true"></th>
				<th field="action" width="" formatter="getActions"><?php  echo lang('action')?></th>

				<th data-options="field:'name',sortable:true" width="20%"><?php echo lang('name')?></th>
				<th data-options="field:'image'" formatter="getImage" width="15%"><?php echo lang('image')?></th>
				<th data-options="field:'start_date',sortable:true" width=""><?php echo lang('start_date')?></th>
				<th data-options="field:'end_date',sortable:true" width=""><?php echo lang('end_date')?></th>
				<!-- <th data-options="field:'status',sortable:true" width="8%"><?php echo lang('status')?></th> -->

			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php echo lang('create_popup_notice')?>">Create</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_popup_notice')?>">Selected Remove</a>
			</p>
		</div> 

		<!--for create and edit popup_notice form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px" data-options="closed:true,collapsible:true,modal:true">
			<form action="<?php echo site_url('popup_notice/admin/popup_notice/save'); ?>" id="form-popup_notice" method="post" accept-charset="utf-8" enctype="multipart/form-data">	
				<table>
					<tr>
						<td width="34%" ><label><?php echo lang('name')?>:</label></td>
						<td width="66%"> <input type="text" name="name" class="form-control" required="true" placeholder="Name"> </td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('image')?>:</label></td>
						<td width="66%"> <input type="file" name="image"> </td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('start_date')?>:</label></td>
						<td width="66%"> <input name="start_date" id="start_date" class="datepicker form-control"  required="true" placeholder="Start Date"> </td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('end_date')?>:</label></td>
						<td width="66%"><input name="end_date" id="end_date" class="datepicker form-control"  required="true" placeholder="End Date"><div id="date_error_msg" hidden style="color: red"></div></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('status')?>:</label></td>
						<td width="66%"> 
							<label><input type="radio" name="status" value="1">Yes</label>
							<label><input type="radio" name="status" value="0">No</label>
						</td>
					</tr>
					<input type="hidden" name="id" id="id"/>
				</table>
				<div id="dlg-buttons" class="pull-right">
					<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a> -->
					<div class="btn-group btn-sm">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="type" class="btn btn-link" onClick="javascript:$('#dlg').window('close')" >Cancel</button>
					</div>
				</div>    
			</form>
		</div>
		<!--div ends-->

	</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#popup_notice-search-form').form('clear');
			$('#popup_notice-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#popup_notice-table').datagrid({
				queryParams:{data:$('#popup_notice-search-form').serialize()}
			});
		});		
		$('#popup_notice-table').datagrid({
			url:'<?php  echo site_url('popup_notice/admin/popup_notice/json')?>',
			height:'auto',
			width:'auto',
			/*onDblClickRow:function(index,row)
			{
				edit(index);
			}*/
		});

		$('.datepicker').datepicker({format: 'yyyy-mm-dd',});

	});

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_popup_notice')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		e = '';
		var d = '<a href="#" onclick="removepopup_notice('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_popup_notice')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		var d = '<button class="btn btn-sm btn-link">Delete</button>';
		return e + d;		
	}

	function getImage(v,r,i) {
		var image = "<img src='<?php echo base_url('uploads/notices'); ?>/"+v+"' style='height: 150px;' />";
		return image;
	}

	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		$('#form-popup_notice').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_popup_notice')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#popup_notice-table').datagrid('getRows')[index];
		if (row){
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_popup_notice')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}


	function removepopup_notice(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#popup_notice-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('popup_notice/admin/popup_notice/delete_json')?>', {id:[row.id]}, function(){
					$('#popup_notice-table').datagrid('deleteRow', index);
					$('#popup_notice-table').datagrid('reload');
				});

			}
		});
	}

	function removeSelected()
	{
		var rows=$('#popup_notice-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}

			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('popup_notice/admin/popup_notice/delete_json')?>',{id:selected},function(data){
						$('#popup_notice-table').datagrid('reload');
					});
				}

			});

		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}

	}

	function save()
	{
		$('#form-popup_notice').submit();
		return;


		$('#form-popup_notice').form('submit',{
			url: '<?php  echo site_url('popup_notice/admin/popup_notice/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-popup_notice').form('clear');
					$('#dlg').window('close');
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#popup_notice-table').datagrid('reload');
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				}
			}

		});		

	}


</script>