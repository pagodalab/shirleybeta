<?php

class Popup_notice extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('popup_notice_model');
		$this->lang->load('popup_notice');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'popup_notice';
		//$data['page'] = $this->config->item('template_admin') . "popup_notice/index";
		$data['module'] = 'popup_notice';
		
		$this->view($this->config->item('template_admin') . "admin/popup_notice/index",$data);			
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->db->where('status', 1);
		$total = $this->popup_notice_model->count();

		paging('id desc');

		$this->_get_search_param();	
		$this->db->where('status', 1);
		$rows=$this->popup_notice_model->getPopupNotice()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name_en']!='')?$this->db->like('name_en',$params['search']['name_en']):'';
			($params['search']['name_da']!='')?$this->db->like('name_da',$params['search']['name_da']):'';

		}
		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}

	}

	public function combo_json()
	{
		$rows=$this->popup_notice_model->getPopupNotice()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				// $this->popup_notice_model->delete('POPUP_NOTICE', array('id'=>$row));
				$this->popup_notice_model->update('POPUP_NOTICE', array('status' => 0), array('id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
        $data = $this->_get_posted_data(); //Retrive Posted Data

        if($data == false || empty($data) || @$data['image'] == '') {
        	redirect('popup_notice/admin/popup_notice');
        }

        if(!$this->input->post('id'))
        {
        	$data['created_date'] = date('Y-m-d');
        	$success = $this->popup_notice_model->insert('POPUP_NOTICE',$data);
        }
        else
        {
        	$data['modified_date'] = date('Y-m-d');
        	$success = $this->popup_notice_model->update('POPUP_NOTICE',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        redirect('popup_notice/admin/popup_notice');
        // echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	
    	$config['upload_path']		= 'uploads/notices/';
    	$config['allowed_types']	= 'gif|jpg|png';
    	$config['max_size']			= $this->config->item('size_limit');
    	$this->load->library('upload');
    	$this->upload->initialize($config);

    	if ( ! $upload_data = $this->upload->do_upload('image'))
    	{
    		$error_data['error'] = array('error' => $this->upload->display_errors());
    		$error_data['header'] = 'popup_notice';
    		$error_data['module'] = 'popup_notice';
    		$this->view($this->config->item('template_admin') . "admin/popup_notice/index",$error_data['error']);
    		return false;
    	}
    	
    	$upload_data = $this->upload->data();
    	// redirect('popup_notice/admin/popup_notice');
    	
    	$data['id'] = $this->input->post('id');
    	$data['name'] = $this->input->post('name');
    	$data['image'] = $upload_data['file_name'];
    	$data['start_date'] = $this->input->post('start_date');
    	$data['end_date'] = $this->input->post('end_date');

    	$data['status'] = $this->input->post('status');

    	return $data;
    }


}