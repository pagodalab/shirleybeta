<?php


$lang['id'] = 'Id';
$lang['name'] = 'Event Name';
$lang['image'] = 'Image';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['status'] = 'Status';

$lang['create_popup_notice']='Create Popup Notice';
$lang['edit_popup_notice']='Edit Popup Notice';
$lang['delete_popup_notice']='Delete Popup Notice';
$lang['popup_notice_search']='Popup Notice Search';

$lang['popup_notice']='Popup Notice';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

