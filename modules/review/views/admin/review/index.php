<div id="search-panel" class="easyui-panel" data-options="title:'Review Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="review-search-form">
<table width="100%">
<tr><td><label><?=lang('product_name')?></label></td>
<td><input name="search[product_id]" id="search_product_id"  class="easyui-combobox"/></td>
<td><label><?php echo lang('author');?></label></td>
<td><input name="search[author]" id="search_author"/></td>
</tr>
<tr>
<td><label>Customer name</label></td>
<td><input name="search[customer_id]" id="search_customer_id"  class="easyui-combobox"/></td>
<td><label>Rating</label></td>
<td><select name="search[rating]" id="search_rating">
		<option value=""></option>
		<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
	</select></td>

<!--<td><label>Added Date</label></td>
<td><input type="text" name="date[added_date][from]" id="search_added_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[added_date][to]" id="search_added_date_to"  class="easyui-datebox"/></td></tr><tr>
<td><label>Modified Date</label></td>
<td><input type="text" name="date[modified_date][from]" id="search_modified_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[modified_date][to]" id="search_modified_date_to"  class="easyui-datebox"/></td>-->
</tr>
<tr>
<td><label>Status</label></td>
<td>
	<select name="search[status]" id="search_status"/>
		<option value=""></option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
</td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="review-table" data-options="pagination:true,title:'Review',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="id" sortable="true" width="30">Product Id</th><?php */?>
    <th field="review_id" sortable="true" width="80" formatter="formatImage"><?php echo lang('review_id');?></th>
	<th field="name" sortable="true" width="100"><?php echo lang('product_name');?></th>
    <th field="firstname" sortable="true" width="100"><?php echo lang('customer_id');?></th>
    <th field="author" width="50"><?php echo lang('author');?></th>
    <th field="text" sortable="true" width="50"><?php echo lang('text')?></th>   
    <th field="rating" sortable="true" width="30" align="center"><?php echo lang('rating');?></th>
	<th field="date_added" sortable="true" width="30" align="center"><?php echo lang('date_added');?></th>
	<th field="date_modified" sortable="true" width="30" align="center"><?php echo lang('date_modified');?></th>
    <th field="status" sortable="true" width="30" align="center" formatter="formatStatus"><?php echo lang('status');?></th>
	
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" onclick="create()" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Review</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a>
    </p>

</div> 

<!--for survey detail -->
<div id="detail-dialog" class="easyui-window" style="width:600px;height:500px;padding:10px 20px"
        closed="true" collapsible="true"  buttons="#dlg-buttons">
<div id="product-detail-view"></div>        
</div>        

<!--<div id="dlg_displayDetails" class="easyui-window" title="Product details" style="width:500px;height:auto;padding:10px 20px" closed="true" collapsible="true" buttons="#dlg-buttons">
	<p>Product Name: <span id="det_productname"></span></p>
    <p>Original Price: <span id="det_originalprice"></span></p>
    <p>Sell Price: <span id="det_sellprice"></span></p>
    <p>Expiry Date: <span id="det_expirydate"></span></p>
    <p>Views: <span id="det_views"></span></p>
    <p>Status: <span id="det_status"></span></p>
</div>-->

<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
   <form id="form-review" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('product_name')?>:</label></td>
					  <td width="66%"><input name="product_id" id="product_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('customer_id')?>:</label></td>
					  <td width="66%"><input name="customer_id" id="customer_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('author')?>:</label></td>
					  <td width="66%"><input name="author" id="author" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('text')?>:</label></td>
					  <td width="66%"><textarea name="text" id="text" class="easyui-validatebox" required="true"></textarea></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('rating')?>:</label></td>
					  <td width="66%"><select name="rating" id="rating" class="easyui-combobox" required="true">
					  					<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
					  
					</td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('status')?>:</label></td>
					  <td width="66%"><label><input type="radio" value="1" name="status" id="status1" /><?php echo lang("general_yes")?> </label><label><input type="radio" value="0" name="status" id="status0" /><?php echo lang("general_no")?></label></td>
		       </tr><input type="hidden" name="review_id" id="review_id"/>
    </table>
    </form>

<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>   
</div>

<script language="javascript" type="text/javascript">

	$(function(){
		$('#search_brand').combobox({url:'<?php echo site_url('brand/admin/combo_json')?>',valueField:'brand_id',textField:'brand_name'});
		$('#search_product_id').combobox({url:'<?php echo site_url('product/admin/combo_json')?>',valueField:'id',textField:'name'});
		$('#search_customer_id').combobox({url:'<?php echo site_url('customer/admin/combo_json')?>',valueField:'id',textField:'firstname'});
		$('#product_id').combobox({url:'<?php echo site_url('product/admin/combo_json')?>',valueField:'id',textField:'name'});
		$('#customer_id').combobox({url:'<?php echo site_url('customer/admin/combo_json')?>',valueField:'id',textField:'firstname'});
		
		$('#search_category').combotree({url:'<?php echo site_url('category/admin/treeview_json')?>'});
			
		$('#clear').click(function(){
			$('#review-search-form').form('clear');
			$('#review-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#review-table').datagrid({
				queryParams:{data:$('#review-search-form').serialize()}
				});
		});		
		$('#review-table').datagrid({
			url:'<?=site_url('review/admin/json')?>',
			height:'auto',
			width:'auto',
			/*onDblClickRow:function(index,row)
			{
			$('#form-product').form('load',row);
			$('#dlg').window('open').window('setTitle','<?=lang('edit_product')?>');
			}*/
		});
	
	});
	

	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return "Yes";	
		}
		return "No";
	}
	
	function getActions(value,row,index)
	{
		var e = '<a href=#" onclick="edit('+index+')"  class="tooltip-top btn btn-small" title="<?=lang('edit_product')?>"><span><i class="icon-pencil"></i></span> </a>';
		
		var d = '<a href="#" onclick="removeProduct('+index+')" class="tooltip-top btn btn-small" title="<?=lang('delete_product')?>"><span><i class="icon-trash"></i></span> </a>';
		
		return e+d;		
	}

	function upload(index)
	{
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('product_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {product_id: row.product_id};
		}
		$('#form-upload').form('clear');
	}
		
	function removeProduct(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#review-table').datagrid('getRows')[index];
				//console.log(row); return false;
				$.post('<?=site_url('review/admin/delete_json')?>', {id:[row.review_id]}, function(){
					<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
					$('#review-table').datagrid('deleteRow', index);
					$('#review-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#review-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].review_id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?=site_url('review/admin/delete_json')?>',{id:selected},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
						$('#review-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function formatImage(value)
	{
		//console.log(value);
		//var img = '<img src="<?php echo base_url()?>uploads/images/small/'+value+'">';
		
		
		return value;
		//return value;
	}


	function changeSelected()
	{
		var status = $('#enabled').combobox('getValue');
		var rows=$('#review-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].review_id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to Change?',function(r){
				if(r){				
					$.post('<?=site_url('review/admin/change_status')?>',{id:selected,enabled:status},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
						$('#review-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No Product Selected!');	
		}	
	}
	
	function edit(index)
	{
		console.log(index);
		var row = $('#review-table').datagrid('getRows')[index];
		if (row){
			$('#form-review').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_booking')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	function save()
	{	
		
		$('#form-review').form('submit',{
			url: '<?php  echo site_url('review/admin/save_json')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-review').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#review-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	function create(){
		//Create code here
		$('#form-review').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_review')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	
</script>
