<?php

class Admin extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('review/review_model');
		$this->lang->load('review');
        //$this->lang->load('REVIEWS/REVIEWS');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'Review';
		$data['page_title'] = 'Review';
		$this->view($this->config->item('admin_folder').'/review/index', $data);	
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->review_model->count();
		paging('review_id');
		$this->_get_search_param();	
		$this->review_model->joins=array('PRODUCTS','CUSTOMERS');
		
		$rows=$this->review_model->getReviews(NULL,'reviews.review_id desc')->result();
		/*echo '<pre>'.$this->db->last_query();
		exit;*/
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['product_id']!='')?$this->db->like('reviews.product_id',$params['search']['product_id']):'';
			($params['search']['customer_id']!='')?$this->db->where('reviews.customer_id',$params['search']['customer_id']):'';
			($params['search']['author']!='')?$this->db->like('reviews.author',$params['search']['author']):'';
			($params['search']['rating']!='')?$this->db->like('reviews.rating',$params['search']['rating']):'';
			($params['search']['status']!='')?$this->db->where('reviews.status',$params['search']['status']):'';
			
			if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		}  
	}
	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->review_model->getREVIEWS()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->review_model->delete('REVIEWS',array('review_id'=>$row));
				
            endforeach;
		
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		
        if(!$this->input->post('modified_date'))
        {
			$data['date_added'] =date('Y-m-d H:i:s');		
            $success=$this->review_model->insert('REVIEWS',$data);
			
        }
        else
        {
			$data['date_modified'] =date('Y-m-d H:i:s');			
            $success=$this->review_model->update('REVIEWS',$data,array('review_id'=>$data['review_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			
			redirect(site_url('product/index/'.$this->input->post('product_id')));
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 //echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   public function save_json(){
   
   	$data=$this->_get_posted_data(); //Retrive Posted Data		
		
        if(!$this->input->post('review_id'))
        {
			$data['date_added']=date('Y-m-d');
            $success=$this->review_model->insert('REVIEWS',$data);
			
        }
        else
        {
			$data['date_modified'] = date('Y-m-d');		
            $success=$this->review_model->update('REVIEWS',$data,array('review_id'=>$data['review_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			
			$msg=lang('message_saved_product');
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
   }
   
   private function _get_posted_data()
   {
   		$data=array();
        
		$data['product_id'] = $this->input->post('product_id');
		
		$data['review_id']=$this->input->post('review_id');
		$data['customer_id'] = $this->input->post('customer_id');
		$data['author'] = $this->input->post('author');
		$data['text'] = $this->input->post('text');
		$data['rating'] = $this->input->post('rating');
		
		
		$data['status'] = $this->input->post('status');

        return $data;
   }
   
   public function change_status()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('enabled');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->review_model->update('REVIEWS',array('status'=>$status),array('review_id'=>$row));
			}
		}
	}
	
	
	    
}
?>