<?php

class Review extends Front_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('brand/brand_model');
        $this->lang->load('brand/brand');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'brand';
		$data['page_title'] = 'Brands';
		$this->view($this->config->item('admin_folder').'/brand/index', $data);	
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->brand_model->count();
		paging('brand_id');
		$this->_get_search_param();	
		$rows=$this->brand_model->getBrands()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['brand_name']!='')?$this->db->like('brand_name',$params['search']['brand_name']):'';
			($params['search']['address']!='')?$this->db->like('address',$params['search']['address']):'';
			($params['search']['city']!='')?$this->db->like('city',$params['search']['city']):'';
			($params['search']['contact_person']!='')?$this->db->like('contact_person',$params['search']['contact_person']):'';
			($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
			($params['search']['mobile']!='')?$this->db->like('mobile',$params['search']['mobile']):'';
			($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  
	}
    
	public function combo_json()
    {
		$rows=$this->brand_model->getBrands()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->brand_model->delete('BRANDS',array('brand_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		
		
		
        if(!$this->input->post('modified_date'))
        {
			$data['date_added'] =date('Y-m-d H:i:s');		
            $success=$this->review_model->insert('REVIEWS',$data);
			
        }
        else
        {
			$data['date_modified'] =date('Y-m-d H:i:s');			
            $success=$this->review_model->update('REVIEWS',$data,array('review_id'=>$data['review_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('message_saved_product');
			//redirect(site_url('product/index/'.$this->input->post('product_id')));
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['blog_id'] = $this->input->post('blog_id');
		$data['customer_id'] = $this->input->post('customer_id');
		$data['author'] = $this->input->post('author');
		$data['text'] = $this->input->post('text');
		$data['rating'] = $this->input->post('rating');
		$data['email'] = $this->input->post('email');
		//$data['date_modified'] = $this->input->post('date_modified');
		$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}
?>