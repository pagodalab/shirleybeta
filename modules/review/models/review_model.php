<?php
class Review_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('REVIEWS'=>$this->prefix.'reviews','PRODUCTS'=>$this->prefix.'products','CUSTOMERS'=>$this->prefix.'customers');
		$this->_JOINS=array('PRODUCTS'=>array('join_type'=>'INNER','join_field'=>'reviews.product_id=products.id',
                                           'select'=>'products.name','alias'=>'products'),
							'CUSTOMERS'=>array('join_type'=>'LEFT','join_field'=>'reviews.customer_id=customers.id',
                                           'select'=>'customers.firstname','alias'=>'customers')  

                            );     
    }
	
	

    public function getReviews($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='reviews.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['REVIEWS']. ' reviews');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('review_id', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countProducts($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['PRODUCT'].' products');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['REVIEWS'].' reviews');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	function get_product($id, $related=true)
	{
		$result	= $this->getProducts(array('id'=>$id))->row();
		if(!$result)
		{
			return false;
		}

		$related	= json_decode($result->related_products);
		//$assorted	= json_decode($result->assorted_products);
		
		if(!empty($related))
		{
			//build the where
			$where = array();
			foreach($related as $r)
			{
				$where[] = '`id` = '.$r;
			}

			$this->db->where('('.implode(' OR ', $where).')', null);
			$this->db->where('enabled', 1);

			$result->related_products	= $this->getProducts()->result();
		}
		else
		{
			$result->related_products	= array();
		}
		
		$result->categories			= $this->getProductCategories($result->id);
		
		/*if(!empty($assorted))
		{
			//build the where
			$where = array();
			foreach($assorted as $a)
			{
				$where[] = '`id` = '.$a;
			}

			$this->db->where('('.implode(' OR ', $where).')', null);
			$this->db->where('enabled', 1);

			$result->assorted_products	= $this->getProducts()->result();
		}
		else
		{
			$result->assorted_products	= array();
		}*/

		return $result;
	}

	function getProductCategories($id)
	{
		$this->db->where('product_id', $id);
		$this->db->join($this->_TABLES['CATEGORY'].' categories', 'category_id = categories.id');
		return $this->db->get($this->_TABLES['CATEGORY_PRODUCT'])->result();
	}

	function get_slug($id)
	{
		return $this->db->get_where($this->_TABLES['PRODUCT'], array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from($this->_TABLES['PRODUCT']);
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false)
	{
		if ($product['id'])
		{
			$product['modified_date']=date('Y-m-d H:i:s');
            $this->update('PRODUCT', $product,array('id'=>$product['id']));
			$id	= $product['id'];
		}
		else
		{
			$product['added_date']=date('Y-m-d H:i:s');
            $this->insert('PRODUCT', $product);			
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->getProductCategories($id);
				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c)
				{
					$ids[]	= $c->id;
				}

				//eliminate categories that products are no longer in
				foreach($ids as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->delete('CATEGORY_PRODUCT', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $ids))
					{
						$this->insert('CATEGORY_PRODUCT', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->insert('CATEGORY_PRODUCT', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		
		//return the product id
		return $id;
	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['PRODUCT']);

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete($this->_TABLES['CATEGORY_PRODUCT']);
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete($this->_TABLES['COUPON_PRODUCT']);

	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('tbl_product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	function search_products($term, $limit=false, $offset=false, $by=false, $sort=false)
	{
		$results		= array();
		
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one counts the total number for our pagination
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		$results['count']	= $this->db->count_all_results($this->_TABLES['PRODUCT']);


		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one gets just the ones we need.
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get($this->_TABLES['PRODUCT'], $limit, $offset)->result();
		
		return $results;
	}

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false)
	{
		$product	= $this->db->get_where($this->_TABLES['PRODUCT'], array('id'=>$id))->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product)
		{
			return false;
		}
		
		$product->base_price	= $product->price;
		
		if ($product->saleprice != 0.00)
		{ 
			$product->price	= $product->saleprice;
		}
		
		
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1)
		{
			$product->quantity = 1;
		}
		else
		{
			$product->quantity = $quantity;
		}
		
		
		// attach list of associated downloadables
		$product->file_list	= $this->digital_product_model->get_associations_by_product($id);
		
		return (array)$product;
	}
}

?>