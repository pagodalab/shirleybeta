<?php
Class order_model extends MY_Model
{	
	var $joins=array();
	function __construct()
	{
		
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('ORDER'=>$this->prefix.'orders','ORDER_ITEM'=>$this->prefix.'order_items',
							 'PRODUCT'=>$this->prefix.'products','CUSTOMER'=>$this->prefix.'customers','STAFF_ORDER'=>$this->prefix.'staff_orders','ADMIN'=>$this->prefix.'admin');
		$this->_JOINS=array('ORDER_ITEM'=>array('join_type'=>'INNER','join_field'=>'orders.id=order_items.id',
                                           'select'=>"order_items.product_id,order_items.quantity",'alias'=>'order_items'),
							'PRODUCT'=>array('join_type'=>'INNER','join_field'=>'products.id=order_items.product_id',
                                           'select'=>"products.name,products.sku,products.description",'alias'=>'products'),
							'CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=orders.customer_id',
                                           'select'=>"concat(customers.firstname,' ',customers.lastname) as customer_name,customers.phone",'alias'=>'customers'),
						'STAFF_ORDER'=>array('join_type'=>'INNER','join_field'=>'orders.id=staff_order.order_id',
                                           'select'=>"staff_order.*",'alias'=>'staff_order'),
						'ADMIN'=>array('join_type'=>'INNER','join_field'=>'admins.id=staff_order.staff_id',
                                           'select'=>"concat(admins.firstname,' ',admins.lastname) as staff_name",'alias'=>'admins'),
                            );        
    }

	
	function get_gross_monthly_sales($year)
	{
		$this->db->select('SUM(coupon_discount) as coupon_discounts');
		$this->db->select('SUM(gift_card_discount) as gift_card_discounts');
		$this->db->select('SUM(subtotal) as product_totals');
		$this->db->select('SUM(shipping) as shipping');
		$this->db->select('SUM(tax) as tax');
		$this->db->select('SUM(total) as total');
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->select('MONTH(ordered_on) as month');
		$this->db->group_by(array('MONTH(ordered_on)'));
		$this->db->order_by("ordered_on", "desc");
		$this->db->where('YEAR(ordered_on)', $year);
		
		return $this->db->get($this->_TABLES['ORDER'])->result();
	}
	
	function get_sales_years()
	{
		$this->db->order_by("ordered_on", "desc");
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->group_by('YEAR(ordered_on)');
		$records	= $this->db->get($this->_TABLES['ORDER'])->result();
		$years		= array();
		foreach($records as $r)
		{
			$years[]	= $r->year;
		}
		return $years;
	}
	
	function get_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
	{	
		$this->db->select("orders.*,staff_orders.*,concat(admins.firstname,' ',admins.lastname) as staff_name",FALSE);
		$this->db->from($this->_TABLES['ORDER']. ' orders');
		$this->db->join($this->_TABLES['STAFF_ORDER']. ' staff_orders', 'staff_orders.order_id=orders.id','LEFT');
		$this->db->join($this->_TABLES['ADMIN']. ' admins','admins.id=staff_orders.staff_id','LEFT');
;
		//$this->db->order_by('orders.id desc');
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `orders`.`order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `orders`.`bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `orders`.`bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `orders`.`ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `orders`.`ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `orders`.`status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `orders`.`notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('orders.ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('orders.ordered_on <',$search->end_date);
			}
			if(!empty($search->status))
			{
				$this->db->where('orders.status =',$search->status);
			}
			
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		
		return $this->db->get($this->_TABLES['ORDER'])->result();
	}
	
	function get_orders_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				$this->db->where('ordered_on <',$search->end_date);
			}
			if(!empty($search->status))
			{
				$this->db->where('status =',$search->status);
			}
			
		}
		
		return $this->db->count_all_results($this->_TABLES['ORDER']);
	}

	
	
	//get an individual customers orders
	function get_customer_orders($id, $offset=0,$limit=15)
	{
		//$this->db->join('order_items', 'orders.id = order_items.order_id');
		$this->db->order_by('ordered_on', 'DESC');
		return $this->db->get_where($this->_TABLES['ORDER'], array('customer_id'=>$id), $limit, $offset)->result();
	}
	
	function count_customer_orders($id)
	{
		$this->db->where(array('customer_id'=>$id));
		return $this->db->count_all_results($this->_TABLES['ORDER']);
	}
	
	function get_order($id)
	{
		$this->db->where('id', $id);
		$result 			= $this->db->get($this->_TABLES['ORDER']);
		
		$order				= $result->row();
		$order->contents	= $this->get_items($order->id);
		
		return $order;
	}
	
	function get_items($id)
	{
		$this->db->select('order_id, contents');
		$this->db->where('order_id', $id);
		$result	= $this->db->get($this->_TABLES['ORDER_ITEM']);
		
		$items	= $result->result_array();
		
		$return	= array();
		$count	= 0;
		foreach($items as $item)
		{

			$item_content	= unserialize($item['contents']);
			
			//remove contents from the item array
			unset($item['contents']);
			$return[$count]	= $item;
			
			//merge the unserialized contents with the item array
			$return[$count]	= array_merge($return[$count], $item_content);
			
			$count++;
		}
		return $return;
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_TABLES['ORDER']);
		
		//now delete the order items
		$this->db->where('order_id', $id);
		$this->db->delete($this->_TABLES['ORDER_ITEM']);
	}
	
	function save_order($data, $contents = false)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_TABLES['ORDER'], $data);
			$id = $data['id'];
			
			// we don't need the actual order number for an update
			$order_number = $id;
		}
		else
		{
			$this->db->insert($this->_TABLES['ORDER'], $data);
			$id = $this->db->insert_id();
			
			//create a unique order number
			//unix time stamp + unique id of the order just submitted.
			//$order	= array('order_number'=> date('U').$id);
			$customer_id=isset($data['customer_id'])?$data['customer_id']:0;
			//$order=array('order_number'=>$customer_id.'-'.$this->count_customer_orders($customer_id));  old way to add order number
			$order = array('order_number'=>$id);
			
			//update the order with this order id
			$this->db->where('id', $id);
			$this->db->update($this->_TABLES['ORDER'], $order);
						
			//return the order id we generated
			$order_number = $order['order_number'];
		}
		
		//if there are items being submitted with this order add them now
		if($contents)
		{
			// clear existing order items
			$this->db->where('order_id', $id)->delete($this->_TABLES['ORDER_ITEM']);
			// update order items
			foreach($contents as $item)
			{
				$save					= array();
				$save['contents']		= $item;
				$item					= unserialize($item);
				$save['product_id'] 	= $item['id'];
				$save['quantity'] 		= $item['quantity'];
				$save['order_id']		= $id;
				/*$save['delivery_date']   = (!($item['delivery_date']))?'':$item['delivery_date'];
				$save['product_message'] = (!($item['product_message']))?'':$item['product_message'];
				$save['cake_message']  	= (!($item['cake_message']))?'':$item['cake_message'];*/
				$save['purchase_status']	= 'Pending';
				$save['color']	=(!($item['color']))?'':$item['color'];
				$save['size']	= (!($item['size']))?'':$item['size'];
				//echo "<pre> Save"; print_r($save);
				$this->db->insert($this->_TABLES['ORDER_ITEM'], $save);
				//echo "<pre> Query"; print_r($this->db->last_query());
			}
			//echo "<pre> Contents"; print_r($contents); exit;
		}
		
		return $order_number;

	}
	
	function get_best_sellers($start, $end)
	{
		if(!empty($start))
		{
			$this->db->where('ordered_on >=', $start);
		}
		if(!empty($end))
		{
			$this->db->where('ordered_on <',  $end);
		}

		$data	= $this->db->get('view_best_selling_product')->result();
		
		return $data;
		exit;

		
		/*// just fetch a list of order id's
		$orders	= $this->db->select('id')->get($this->_TABLES['ORDER'])->result();
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get($this->_TABLES['ORDER_ITEM'])->result_array();
			
			foreach($order_items as $i)
			{
				
				if(isset($items[$i['product_id']]))
				{
					$items[$i['product_id']]	+= $i['quantity'];
				}
				else
				{
					$items[$i['product_id']]	= $i['quantity'];
				}
				
			}
		}
		arsort($items);

		// don't need this anymore
		unset($orders);
		
		$return	= array();
		foreach($items as $key=>$quantity)
		{
			$product				= $this->db->where('id', $key)->get($this->_TABLES['PRODUCT'])->row();
			if($product)
			{
				$product->quantity_sold	= $quantity;
			}
			else
			{
				$product = (object) array('sku'=>'Deleted', 'name'=>'Deleted', 'quantity_sold'=>$quantity);
			}
			
			$return[] = $product;
		}
		
		return $return;*/
	}
	
	public function getOrders($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='orders.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ORDER']. ' orders');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function countOrders($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $this->db->from($this->_TABLES['ORDER'].' orders');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
    
    public function updateOrderStatus($data)
	{
		if($data['order_id'])
		{
			if($data['is_delivered'] == 1)
			{
				$order['id'] = $data['order_id'];
				$order['status'] = 'Delivered';
				$order['delivered_date'] = date('Y-m-d h:i:s');
				
				$this->db->where('id', $data['order_id']);
				$this->db->update($this->_TABLES['ORDER'], $order);
			}
			else
			{
				$order['id'] = $data['order_id'];
				$order['status'] = 'Pending';
								
				$this->db->where('id', $data['order_id']);
				$this->db->update($this->_TABLES['ORDER'], $order);
			}
			
		}
	}
	
}