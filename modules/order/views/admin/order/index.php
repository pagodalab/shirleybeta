<?php
	//set "code" for searches
	if(!$code)
	{
		$code = '';
	}
	else
	{
		$code = '/'.$code;
	}
	function sort_url($lang, $by, $sort, $sorder, $code, $admin_folder)
	{
		if ($sort == $by)
		{
			if ($sorder == 'asc')
			{
				$sort	= 'desc';
				$icon	= ' <i class="icon-chevron-up"></i>';
			}
			else
			{
				$sort	= 'asc';
				$icon	= ' <i class="icon-chevron-down"></i>';
			}
		}
		else
		{
			$sort	= 'asc';
			$icon	= '';
		}
			

		$return = site_url('order/admin/index/'.$by.'/'.$sort.'/'.$code);
		
		echo '<a href="'.$return.'">'.lang($lang).$icon.'</a>';

	}
	
if ($term):?>

<div class="alert alert-info">
	<?php echo sprintf(lang('search_returned'), intval($total));?>
</div>
<?php endif;?>

<style type="text/css">
	.pagination {
		margin:0px;
		margin-top:-3px;
	}
</style>
<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<div class="row">
			<div class="span4">
				<?php echo $this->pagination->create_links();?>&nbsp;
			</div>
            
			<div class="span8">
				<?php echo form_open('order/admin/index', 'class="form-inline" style="float:right"');?>
					<fieldset>
                        		<input id="top" type="text" class="span2" name="term" placeholder="<?php echo lang('term')?>" /> 
						<input id="start_top"  value="" class="span2" type="text" placeholder="Start Date"/>
						<input id="start_top_alt" type="hidden" name="start_date" />
						<input id="end_top" value="" class="span2" type="text"  placeholder="End Date"/>
						<input id="end_top_alt" type="hidden" name="end_date" />
						
                         	<select name="status" id="status_top">
                              	<option value="">Select Status</option>
                                   <option value="Order Placed">Order Placed</option>
                                   <option value="Pending">Pending</option>
                                   <option value="Processing">Processing</option>
                                   <option value="Shipped">Shipped</option>
                                   <option value="On Hold">On Hold</option>
                                   <option value="Cancelled">Cancelled</option>
                                   <option value="Delivered">Delivered</option>
                              </select>
						
						<button class="btn" name="submit" value="search"><?php echo lang('search')?></button>
						<!---<button class="btn" name="submit" value="export"><?php echo lang('xml_export')?></button>--->
                        
					</fieldset>
				</form>
			</div>
		</div>
	</div>
     <div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<div class="row">
			<!---<div class="span5">
                <a href="<?php echo site_url('order/admin/printList')?>" class="btn" title="Print List" target="_blank"><i class="icon-print"></i> Print</a>&nbsp;
			</div>--->
            
			<div class="span8">
               	<!---<fieldset class="form-inline">
				<label>Delivery By : 
                	<select name="staff_id" id="staff_id">
                		<option value="">Select</option>
                    	<?php foreach($staffs as $staff){?>
                         	<option value="<?php echo $staff['id']?>"><?php echo $staff['firstname'] . ' '. $staff['lastname']?></option>
                         <?php }?>
                    </select>
                    <button type="button" onclick="assignStaff()" class="btn"><span><i class="icon-ok"></i></span> Assign Order</button>
                </label>
                </fieldset>----->
			</div>
		</div>
	</div>
</div>

<?php echo form_open('order/admin/bulk_delete', array('id'=>'delete_form', 'onsubmit'=>'return submit_form();', 'class="form-inline"')); ?>

<table class="table table-striped">
    <thead>
		<tr>
			<th><input type="checkbox" id="gc_check_all" /> <button type="submit" class="btn btn-small btn-danger"><i class="icon-trash icon-white"></i></button></th>
			<th><?php echo sort_url('order', 'order_number', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<th><?php echo sort_url('bill_to', 'bill_lastname', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<th><?php echo sort_url('ship_to', 'ship_lastname', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<th><?php echo sort_url('ordered_on','ordered_on', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<th><?php echo sort_url('status','status', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<th><?php echo sort_url('total','total', $sort_by, $sort_order, $code, $this->config->item('admin_folder')); ?></th>
			<!---<th>Delivery Info</th>
               <th>Assigned To</th>--->
            <th></th>
	    </tr>
	</thead>

    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">'.lang('no_orders') .'</td></tr>':''?>
    <?php foreach($orders as $order): ?>
	<tr>
		<td><input name="order[]" type="checkbox" value="<?php echo $order->id; ?>" class="gc_check"/></td>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap">
			<?php echo $order->bill_lastname.', '.$order->bill_firstname .'<br/>';
				  echo $order->bill_email.'<br/>';
				  echo $order->bill_phone.', '.$order->bill_address1
			?>
        </td>
		<td style="white-space:nowrap">
			<?php echo $order->ship_lastname.', '.$order->ship_firstname .'<br/>';
				  echo $order->ship_email.'<br/>';
				  echo $order->ship_phone.', '.$order->ship_address1 ?>
        </td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td style="span2">
			<?php echo form_dropdown('status', $this->config->item('order_statuses'), $order->status, 'id="status_form_'.$order->id.'" class="span2" style="float:left;"'); ?>
               <label class="checkbox inline"><input type="checkbox" name="notify" id="notify"> Notify Customer</label>
			<button type="button" class="btn" onClick="save_status(<?php echo $order->id; ?>)" style="float:left;margin-left:4px;"><?php echo lang('save');?></button>
		</td>
		<td><div class="MainTableNotes"><?php echo format_currency($order->total); ?></div></td>
        <td>
        	<!---<a class="btn btn-small" style="float:right;"href="#" onclick="showShippingInfo(<?php echo $order->id; ?>)" title="View Shipping Info"><i class="icon-star-empty"></i> View</a></td>---->
          <!---<td>
          	<?php //echo (!($order->staff_name))?'<p style="color:red">Not Assigned</p>':$order->staff_name; ?>
          </td>--->
		<td>
       		<!-- <a class="btn btn-small" style="float:right;"href="<?php echo site_url('purchase_order/admin?id='.$order->id);?>" title="View Purchase Orders"><i class="icon-th-list"></i> <?php echo lang('form_view')?></a> -->
			<a class="btn btn-small" style="float:right;"href="<?php echo site_url('order/admin/order/'.$order->id);?>"><i class="icon-search"></i> <?php echo lang('form_view')?></a>
		</td>
	</tr>
    <?php endforeach; ?>
    </tbody>
</table>

</form>

<!-- Shipping Info Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delivery Info.</h3>
    </div>
    <div class="modal-body">
	   	<p id="shipping_info" align="center"></p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span><i class="icon-remove-sign icon-white"></i></span> Close</button>
    </div>
</div>
<!-- Shipping Info Modal Ends-->
<script type="text/javascript">
$(document).ready(function(){
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
	
	// set the datepickers individually to specify the alt fields
	$('#start_top').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('#start_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#start_bottom_alt', altFormat: 'yy-mm-dd'});
	$('#end_top').datepicker({dateFormat:'mm-dd-yy', altField: '#end_top_alt', altFormat: 'yy-mm-dd'});
	$('#end_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#end_bottom_alt', altFormat: 'yy-mm-dd'});
});

function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val)
{
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('<?php echo lang('confirm_delete_order') ?>');
	}
	else
	{
		alert('<?php echo lang('error_no_orders_selected') ?>');
		return false;
	}
}

function save_status(id)
{
	if($('#notify').prop('checked'))
	{
		var notify = 1;
	}
	else
	{
		var notify = 0;
	}
	//console.log('notify ',notify); return false;
	show_animation();
	$.post("<?php echo site_url('order/admin/edit_status'); ?>", { id: id, status: $('#status_form_'+id).val(),notify:notify}, function(data){
		setTimeout('hide_animation()', 500);
	});
}

function show_animation()
{
	$('#saving_container').css('display', 'block');
	$('#saving').css('opacity', '.8');
}

function hide_animation()
{
	$('#saving_container').fadeOut();
}

function showShippingInfo(id)
{
	//alert('This is shipping id' +id);
	$('#shipping_info').text('');
	$.post('<?php echo site_url('order/admin/getShipping')?>',{id:id},function(data){
		//console.log('Data >>',data);
		//console.log('Data >>',data.shipping_notes); return false;
			$('#shipping_info').text(data.shipping_notes);
			$('#myModal').modal();
		},'json');
}

function assignStaff()
{
	var staff = $('#staff_id').val();
	
	if($(".gc_check:checked").length > 0)
	{
		if(staff == 0)
		{
			alert('No Staff selected');
			return false;
		}
		else
		{
			//var checked = $(".gc_check:checked").val();
			var selected = [];
			$(".gc_check:checked").each(function(){
					selected.push(this.value);
				});

			//console.log('Checked',selected); return false;
			
			$.post('<?php echo site_url('order/admin/assign')?>',{checked:selected,staff_id:staff},function(data){
					if(data.success)
					{
						alert("Order Assigned Successfully.");
						window.location.href = '<?php echo site_url('order/admin')?>'; 
					}
					else
					{
						alert("Order Assign Unsuccessfull!");
					}
					
				});
				
				return false;
		}
	}
	else
	{
		alert('<?php echo "No order Selected!" ?>');
		return false;
	}
}
</script>

<div id="saving_container" style="display:none;">
	<div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
	<img id="saving_animation" src="<?php echo base_url('assets/img/storing_animation.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
	<div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001"><?php echo lang('saving');?></div>
</div>