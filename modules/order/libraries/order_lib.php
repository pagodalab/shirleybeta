<?php 

class Order_lib
{
	var $CI;
	
	public function __construct()
	{
		$this->CI=&get_instance();
		$this->load->module_model('order','order_model');
		$this->load->module_model('order','order_model');
	}
	
	public function getOrders()
	{
		$orders = $this->CI->order_model->getOrders();
		return $orders->result_array();
	}
	
	public function changeOrderStatus()
	{
		$this->order_model->save_order();
	}
	
}