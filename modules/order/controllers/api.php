<?php 

class Api extends API_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->module_library('order','order_lib');
		$this->load->module_model('order','order_model');
	}
	
	public function orders_get()
	{
		$orders = $this->order_lib->getOrders();
		$this->response(array('order'=>$orders),200);
	}
	
	public function order_post()
	{
		$this->post('order_id');
		$this->post('is_delivered');
		$result = $this->order_lib->changeOrderStatus();
		
    		$this->response(array('success'=>TRUE),200);
	}
	
}