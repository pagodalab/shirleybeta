<?php

class Admin extends Admin_Controller {	

	protected $uploadPath = 'uploads/order';
	function __construct()
	{		
		parent::__construct();

		$this->load->model('order/order_model');
		$this->load->model('Admin_model');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->helper(array('formatting'));
		$this->lang->load('order');
		$this->load->model('size/size_model');
		$this->load->model('color/color_model');
		$this->load->model('staff/staff_order_model');
	}
	
	function index($sort_by='orders.id',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export')
		{
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;

			$data['orders']	= $this->order_model->get_orders($term);		

			foreach($data['orders'] as &$o)
			{
				$o->items	= $this->order_model->get_items($o->id);
			}
			
			
			force_download_content('orders.xml', $this->load->view($this->config->item('admin_folder').'/order/orders_xml', $data, true));
			
			//kill the script from here
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= lang('orders');
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
		$this->db->group_by('orders.id');
 		$data['orders']	= $this->order_model->get_orders($term, $sort_by, $sort_order, $rows, $page);
		//echo $this->db->last_query(); //exit;
		$data['total']	= $this->order_model->get_orders_count($term);
		
		$this->load->library('pagination');
		
		$config=$this->pagination_config();
		$config['base_url']			= site_url('order/admin/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
		
		$data['staffs'] = $this->Admin_model->getAdmins(array('access'=>'Staff'))->result_array();
		//echo "<pre>"; print_r($data['orders']); exit;
			
		$this->view($this->config->item('admin_folder').'/order/index', $data);
	}

	function export()
	{
		$this->load->model('customer_model');
		$this->load->helper('download_helper');
		$post	= $this->input->post(null, false);
		$term	= (object)$post;
		
		$data['orders']	= $this->order_model->get_orders($term);		

		foreach($data['orders'] as &$o)
		{
			$o->items	= $this->order_model->get_items($o->id);
		}

		force_download_content('orders.xml', $this->load->view($this->config->item('admin_folder').'/order/orders_xml', $data, true));
		
	}
	
	function order($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		$this->load->model('gift/gift_card_model');
		$this->bep_assets->load_asset_group('UPLOADER');
			
		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
	
		$message	= $this->session->flashdata('message');
		
	
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->order_model->save_order($save);
			
			$notify = ($this->input->post('notify'))?1:0;
			if($notify == 1)
			{
				//send notification in case notify checked
				$this->_order_notification($id,$save['status']);
			}
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= lang('view_order');
		$data['order']		= $this->order_model->get_order($id);
		
		/*****************************
		* Order Notification details *
		******************************/
		// get the list of canned messages (order)
		$this->load->model('Messages_model');
    	$msg_templates = $this->Messages_model->get_list('order');
		
		// replace template variables
    	foreach($msg_templates as $msg)
    	{
 			// fix html
 			$msg['content'] = str_replace("\n", '', html_entity_decode($msg['content']));
 			
 			// {order_number}
 			$msg['subject'] = str_replace('{order_number}', $data['order']->order_number, $msg['subject']);
			$msg['content'] = str_replace('{order_number}', $data['order']->order_number, $msg['content']);
    		
    		// {url}
			$msg['subject'] = str_replace('{url}', $this->config->item('base_url'), $msg['subject']);
			$msg['content'] = str_replace('{url}', $this->config->item('base_url'), $msg['content']);
			
			// {site_name}
			$msg['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['subject']);
			$msg['content'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['content']);
			
			$data['msg_templates'][]	= $msg;
    	}

		// we need to see if any items are gift cards, so we can generate an activation link
		foreach($data['order']->contents as $orderkey=>$product)
		{
			if(isset($product['is_gc']) && (bool)$product['is_gc'])
			{
				if($this->gift_card_model->is_active($product['sku']))
				{
					$data['order']->contents[$orderkey]['gc_status'] = '[ '.lang('giftcard_is_active').' ]';
				} else {
					$data['order']->contents[$orderkey]['gc_status'] = ' [ <a href="'. base_url() . $this->config->item('admin_folder').'/giftcards/activate/'. $product['code'].'">'.lang('activate').'</a> ]';
				}
			}
		}
		
		$this->view($this->config->item('admin_folder').'/order/order', $data);
		
	}
	
	function packing_slip($order_id)
	{
		$this->load->helper('date');
		$data['order']		= $this->order_model->get_order($order_id);
		
		$this->load->view($this->config->item('admin_folder').'/order/packing_slip.php', $data);
	}
	
	function edit_status()
    {
	    	//echo "<pre>"; print_r($_POST); exit;
		$this->authenticate->is_logged_in();
		$order['id']		= $this->input->post('id');
		$order['status']	= $this->input->post('status');
		$notify = $this->input->post('notify');
		if($notify == 1)
		{
			//send notification email
			$this->_order_notification($order['id'],$order['status']);
		}
		
		$this->order_model->save_order($order);
		
		echo url_title($order['status']);
    }
    
    function send_notification($order_id='')
    {
			// send the message
   		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);

		$this->email->from($this->config->item('email'), $this->config->item('company_name'));
		$this->email->to($this->input->post('recipient'));
		
		$this->email->subject($this->input->post('subject'));
		$this->email->message(html_entity_decode($this->input->post('content')));
		
		$this->email->send();
		
		$this->session->set_flashdata('message', lang('sent_notification_message'));
		redirect('order/admin/order/'.$order_id);
	}
	
	function bulk_delete()
    {
    	$orders	= $this->input->post('order');
    	
		if($orders)
		{
			foreach($orders as $order)
	   		{
	   			$this->order_model->delete($order);
	   		}
			$this->session->set_flashdata('message', lang('message_orders_deleted'));
		}
		else
		{
			$this->session->set_flashdata('error', lang('error_no_orders_selected'));
		}
   		//redirect as to change the url
		redirect(site_url('order/admin'));	
    }
	
	public function json()
	{
		//$total = $this->order_model->get_orders_count(); //old
		//$rows = $this->order_model->get_orders();
		
		$total = $this->order_model->countOrders();
		paging('id desc');
		
		$rows = $this->order_model->getOrders()->result_array();
		
		//echo "<pre>"; print_r($this->db->last_query());
		//echo "<pre> data "; print_r($rows);
		//exit;
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function getShipping()
	{
		$id = $this->input->post('id');
		$data = $this->order_model->getOrders(array('id'=>$id))->row_array();
		
		echo json_encode($data);
		
	}
	
	public function upload() 
	{
		$this->load->model('order/order_images_model');
		$order_id = $this->input->post('order_id');
		$file_path = 'uploads/order';
		@mkdir($file_path);
		 
		//File upload config
        $config['upload_path'] = $this->uploadPath;
		//echo "<pre> path-->>"; print_r($config['upload_path']); exit;
        $config['allowed_types'] = '*';
        $config['max_size'] = '10240';
        $config['remove_spaces'] = true;
        
        //load upload library
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $data['error'] = $this->upload->display_errors('', '');
            echo json_encode($data);
        } else 
		{
			$data = $this->upload->data();
			//echo "<pre> Data==>>"; print_r($data);
			//echo json_encode($data);
			
			if(!empty($data))
			{
				$file_data = array();
				
				$file_data[] = array(
					'file_name' => $data['file_name'],
					'order_id' => $order_id
				);
				$this->db->insert_batch($this->order_images_model->_TABLES['ORDER_IMAGES'], $file_data);
	
				$file_path='uploads/order/'.$order_id;
				@mkdir($file_path);
				foreach ($data as $file):
					@rename('uploads/order/' . $file, $file_path . '/' . $file);
				endforeach;
			}	
        }
    }
	
	public function printList()
	{
		$this->order_model->joins = array('ORDER_ITEM','PRODUCT','CUSTOMER');
		$this->db->order_by('orders.id desc');
		$data['orders'] = $this->order_model->getOrders()->result_array();
		$page = $this->config->item('admin_folder').'/order/print_orders';
		$this->load->view($page,$data);
	}
	
	public function assign()
	{
		//echo "<pre> POSTED "; print_r($_POST);
		$staff = $this->input->post('staff_id');
		$order_id = $this->input->post('checked');
		
		if(is_array($order_id))
		{
			foreach($order_id as $order)
			{
				$data['order_id'] = $order;
				$data['staff_id'] = $staff;
				$data['added_date'] = date('Y-m-d');
				//echo "<pre> Data"; print_r($data);
				$success = $this->staff_order_model->save($data);
			}
		}
		
		if($success)
		{
			$success = TRUE;
		}
		
		
		echo json_encode(array('success'=>$success));
		
	}
	
	function _order_notification($order_id='',$status)
	{
		//echo "<pre>"; print_r($order_id);
		if($order_id !== '')
		{
			//$this->load->helper('text');
			//$this->load->helper('email');
			$this->load->library('email');
			//echo "<pre>"; print_r($order_id); //exit;
			$order = $this->order_model->getOrders(array('id'=>$order_id))->row_array();
			//echo "<pre>"; print_r($order); exit;
			$subject = "Order Status Changed";
			
			$message="Your ".$order['order_number']." Order Status is changed to ".$status.".<br/>";
			$message.="<br/>Sender : ".$this->config->item('company_name');
			$message.="<br/>Sender Email : ".$this->config->item('email');
			
			// send the message
			
			
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
		//	$config['protocol'] = 'smtp';
		//	$config['smtp_host'] = 'smtp.vianet.com.np';
			$config['mailtype'] = 'html';
			
			$this->email->initialize($config);
			
			$this->email->from($this->config->item('email'), $this->config->item('company_name'));
			$this->email->to($order['bill_email']);
			
			$this->email->subject($subject);
			$this->email->message($message);
			//echo "Message " .$message; exit;
			$this->email->send();
			
			$this->session->set_flashdata('message', lang('sent_notification_message'));
			//redirect('order/admin/order/'.$order_id);
		}
		else
		{
			return false;
		}
		//exit;
		
	}
}