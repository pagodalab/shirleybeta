<?php 
class Product extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	function index($id)
	{
		
		$name = "name_".$this->culture_code;
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){

			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['wishlist']);
		}
		
		$this->load->model('size/size_model');
		$data['sizes'] =$this->size_model->getSizes(null,'sequence asc')->result();
		
		$this->load->model('color/color_model');
		$data['colors'] = $this->color_model->getColors()->result();
		
		//get the product
		//$data['reviews'] = $this->review_model->getReviews(array('product_id'=>$id))->result();

		$this->product_model->joins = array('BRANDS');
		$data['product']	= $this->product_model->get_product($id);
		
		if(!$data['product'] || $data['product']->enabled==0)
		{
			show_404();
		}
		
		if($this->set_cookie($id))
		{
			$this->db->set('views', 'views+1', FALSE);
			$this->product_model->update('PRODUCT',NULL,array('id'=>$id));
		}
		
		$data['base_url']			= $this->uri->segment_array();
		
		// load the digital language stuff
		$this->lang->load('digital_product');
		
		$data['options']	= $this->option_model->get_product_options($data['product']->id);
		
		$related			= $data['product']->related_products;

		$data['related']	= array();
		$data['posted_options']	= $this->session->flashdata('option_values');

		//$name="name_".$this->culture_code;
		$data['page_title']			= $data['product']->$name;
		// $data['meta']				= $data['product']->meta;

        //changing meta in header
		$this->meta_keywords 		= ($data['product']->meta_keywords)?$data['product']->meta_keywords:$this->meta_keywords;
		$this->meta_description		= ($data['product']->meta_description)?$data['product']->meta_description:$this->meta_description;
		
		$data['seo_title']			= (!empty($data['product']->seo_title))?$data['product']->seo_title:$data['product']->$name;

		if($data['product']->images == 'false')
		{
			$data['product']->images = array();
		}
		else
		{
			$data['product']->images	= array_values((array)json_decode($data['product']->images));
		}

		$data['gift_cards_enabled'] = $this->gift_cards_enabled;

		$wish_array=$this->go_cart->get_wishlist();
		$data['is_in_wishlist']=(in_array($id,$wish_array))?true:false;
		
		//for YOU MAY ALSO LIKE
		$this->db->order_by('id desc');
		$this->db->limit(6);
		$data['products'] = $this->product_model->getProducts(array('enabled'=>1))->result_array();

		$data['reqsize'] = $this->input->get('reqsize');
		$this->view('product/index', $data);
	}
	
	function search()
	{
		$this->filter=TRUE;
		$data['page_title']	= 'Search Results';
		$this->product_model->joins=array('CATEGORY_PRODUCT');
		$sort_array = array(
			'name/asc' => array('by' => 'products.name_en', 'sort'=>'ASC'),
			'name/desc' => array('by' => 'products.name_en', 'sort'=>'DESC'),
			'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
			'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
			'latest/first' => array('by' => 'id', 'sort'=>'DESC')
		);
		$sort_by	= array('by'=>'sequence', 'sort'=>'ASC');

		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$where = array('enabled'=>1);
		$where = array_merge($this->_get_where(),$where);
		//$this->db->order_by('id desc');
		$this->db->group_by('products.id');
		$this->db->limit(10);
		$data['products'] = $this->product_model->getProducts($where,$sort_by['by'].' ' . $sort_by['sort'])->result_array();
		$data['query'] = $this->input->get('q');
		//echo "<pre> query"; print_r($this->db->last_query());
		//echo "<pre> result"; print_r($data['products']);
		$this->view('product/search', $data);
	}
	
	private function _get_where()
	{
		$where = array();
		$less_than = $this->input->get('less_than');
		$min  = $this->input->get('min');
		$max  = $this->input->get('max');
		$brand = $this->input->get('brand');
		
		if($this->input->get('q'))
		{
			$where["products.name like '%".$this->input->get('q')."%'"] = NULL;//$this->input->get('name');
		}
		
		if($less_than)
		{
			$where["products.price < '".$less_than."'"]=NULL;
		}
		else if($min && $max)
		{
			$where["products.price >= '".$min."' and products.price <= '".$max."'"]=NULL;
		}
		else if($brand)
		{
			$where["products.brand = '".$brand."'"] = NULL;
		}
		
		return $where;
	}	

	function add_to_wishlist()
	{
		$json = array();
		$wishlist=$this->go_cart->get_wishlist();
		

		if ($this->input->post('product_id')) {
			$product_id = $this->input->post('product_id');
		} else {
			$product_id = 0;
		}
		$product_info = $this->product_model->get_product($product_id);

		if ($product_info) {
			if (!in_array($this->input->post('product_id'), $wishlist)) {	
				$wishlist[]=$this->input->post('product_id');
				$this->go_cart->set_wishlist($wishlist);
			}

			$json['success']=TRUE;
			$json['total'] = count($this->go_cart->get_wishlist()) ;
			echo json_encode($json);
		}	
	}	
	
	private function set_cookie($id)
	{
		$this->load->helper('cookie');
		$cookie=$this->input->cookie('thamelmall_products');

		if(!$this->input->cookie('thamelmall_products') || !in_array($id,$cookie))
		{
			$length=count($cookie);
			$cookie = array(
				'name'   => 'thamelmall_products['.$length.']',
				'value'  => $id,
				'expire' => time()+60*10,
				'path'   => '/',
			);
			$this->input->set_cookie($cookie);
			return TRUE;
		}
		return FALSE;
	}	
	
	function more($offset=0)
	{		
		//echo "<pre> hehehehe "; print_r($offset);
		$data['new_offset'] = $offset + $this->page_limit;
		$data['products'] = $this->product_model->getProducts(array('enabled'=>1),'id desc',array('limit'=>$this->page_limit,'offset' => $offset))->result_array();
		
		$output['view']=$this->load->view('product/more',$data,TRUE);
		$output['offset']=$data['new_offset'];
		echo json_encode($output);
	}	
	
	public function page($offset=0)
	{
		$this->filter=TRUE;
		/*$categories=$this->category_model->get_categories_tiered();
						echo '<pre>';
						print_r($categories);
						exit;*/

		//echo "<pre> hehehehe "; print_r($offset);
		/*$sort_array = array(
							'asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							'latest/first' => array('by' => 'id', 'sort'=>'DESC')
							);
		$sort_by	= array('by'=>'id', 'sort'=>'DESC');
	
		if(isset($_GET['price']))
		{
			if(isset($sort_array[$_GET['price']]))
			{
				$sort_by	= $sort_array[$_GET['price']];
			}
		}*/
		
		$this->load->library('pagination');
		//$order_by = array_merge(array('id desc'),$order_by);
		
		$where = array('enabled'=>1);
		$where = array_merge($where,$this->_get_product_where());
		
		$products = $this->product_model->getProducts($where,'id desc',array('limit'=>$this->page_limit,'offset' => $offset))->result_array();
		//echo "<pre> " . $this->db->last_query();
		//echo "<pre>"; print_r($products); //exit;
		$limit=$this->page_limit;
		$config=$this->pagination_config();
		$config['uri_segment'] = '4';
		$config['base_url'] = site_url('product/page/15/');
		$config['total_rows'] = count($products);
		$config['per_page'] = $limit; 
		//echo "<pre> query"; print_r($this->db->last_query());
		$this->pagination->initialize($config); 
		
		$data['new_offset'] = $offset + $this->page_limit;
		$data['products'] = $this->product_model->getProducts($where,'id desc',array('limit'=>$this->page_limit,'offset' => $offset))->result_array();
		//echo "<pre> " . $this->db->last_query(); exit;
		//echo "<pre>"; print_r($data['products']); 
		//$data['orders_pagination'] = $this->pagination->create_links();
		
		//echo "<pre>total rows "; print_r($config['total_rows']);
		//echo "<pre>per Page"; print_r($config['per_page']);*/
		//$output['view']=$this->load->view('product/more',$data,TRUE);
		//$output['offset']=$data['new_offset'];
		
		$this->view('home/page',$data);
	}
	
	private function _get_product_where()
	{
		$where = array();
		$less_than = $this->input->get('less_than');
		$min  = $this->input->get('min');
		$max  = $this->input->get('max');
		$brand = $this->input->get('brand');
		if($less_than)
		{
			$where["products.price < '".$less_than."'"]=NULL;
		}
		else if($min && $max)
		{
			$where["products.price >= '".$min."' and products.price <= '".$max."'"]=NULL;
		}
		else if($brand)
		{
			$where["products.brand = '".$brand."'"] = NULL;
		}
		
		return $where;
	}
	
}