<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		
		$this->load->model(array('material/material_model','product/product_model','coupon/coupons_products_model','category/category_products_model','product/assorted_product_model','occasion/occasion_model','occasion/occasion_product_model','event/event_model','event/event_product_model','color/color_model','size/size_model','occasion/occasion_model','event/event_model'));
		$this->load->helper('form');
		$this->lang->load('product');
	}

	public function index()
	{
		// Display Page

		$data['page_title'] = 'Product';
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'product';
		$this->view($this->config->item('template_admin') . "admin/product/index",$data);		
	}

	public function json()
	{
		/*
		$this->product_model->joins = array('CATEGORY_PRODUCT');
		
		$this->_get_search_param();
		$total=$this->product_model->count();
		echo $this->db->last_query();
		exit;
		//paging('id desc');
		paging('products.name_en asc');
	//  $this->db->group_by('products.id desc');	
		$this->_get_search_param();
	//	$this->db->group_by('products.id desc');	
		$rows=$this->product_model->getProducts()->result_array();
		// echo $this->db->last_query();
		// exit;
		echo json_encode(array('total'=>$total,'rows'=>$this->gridData($rows)));
		*/
		
		
		$this->_get_search_param();	
		

		$total=$this->product_model->count();
		$this->product_model->joins = array('CATEGORY_PRODUCT');
		paging('products.name_en asc');
		$this->_get_search_param();
		$this->db->group_by('products.id desc');	
		$rows=$this->product_model->getProducts()->result_array();
		//echo "<pre>";
		//print_r($rows);
		//exit;
		//echo $this->db->last_query();
		//exit;
		echo json_encode(array('total'=>$total,'rows'=>$this->gridData($rows)));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('products.name_en',$params['search']['name']):'';
			($params['search']['enabled']!='')?$this->db->like('enabled',$params['search']['enabled']):'';
			(!empty($params['search']['is_perishable']))?$this->db->where('is_perishable = 1'):'';
			(!empty($params['search']['is_flexible']))?$this->db->where('is_flexible = 1'):'';
			($params['search']['category']!='')?$this->db->where('category_id',$params['search']['category']):'';
			//($params['search']['brand']!='')?$this->db->where('brand',$params['search']['brand']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
	}
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}
	
	
	private function gridData($rows)
	{
		$data=array();
		foreach($rows as $row)
		{
			
			$row['images']=$this->getMainImage($row['images']);
			$data[]=$row;			
			
		}
		return $data;
	}
	
	private function getMainImage($images)
	{

		$photo  = '<img class="responsiveImage" src="'.theme_img('no_picture.png',FALSE).'" style="height:100px;width:100px" />';
		if($images != 'false')
		{


			$images	= array_values((array)json_decode($images));


			if(!empty($images[0]))
			{
				$primary    = $images[0];
				foreach($images as $photo)
				{
					if(isset($photo->primary))
					{
						$primary    = $photo;
					}
				}

				$photo  = '<img class="responsiveImage" src="'.base_url('uploads/images/medium/'.$primary->filename).'" style="height:100px;width:100px" />';
			}
		}
		return $photo;		
	}
	
	
	public function delete_json()
	{
		$this->load->model('routes/routes_model');
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$product = $this->product_model->getProducts(array('id'=>$row))->row_array();
				$this->routes_model->delete('ROUTES',array('id'=>$product['route_id']));
				$this->coupons_products_model->delete('COUPON_PRODUCT',array('product_id'=>$row));
				$this->category_products_model->delete('CATEGORY_PRODUCT',array('product_id'=>$row));
				$this->product_model->delete('PRODUCT',array('id'=>$row));
			endforeach;
		}
	}  
	
	
	public function combo_json()
	{
		$rows=$this->product_model->getProducts()->result_array();
		echo json_encode($rows);    	
	} 
	
	//basic category search
	function product_autocomplete()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$results	= $this->product_model->product_autocomplete($name, $limit);
			
			$return		= array();
			
			foreach($results as $r)
			{
				$return[$r->id]	= $r->name_en;
			}
			echo json_encode($return);
		}
		
	}
	
	function bulk_save()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect(site_url('product/admin'));
		}

		foreach($products as $id=>$product)
		{
			$product['id']	= $id;
			$this->product_model->save($product);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect(site_url('product/admin'));
	}
	
	function form($id = false, $duplicate = false)
	{

		$this->product_id	= $id;
		$this->load->library('form_validation');
		$this->bep_assets->load_asset_group('DATETIMEPICKER');
		$this->bep_assets->load_asset_group('JQUERY_UI');
		$this->load->model(array('Option_model', 'category/category_model', 'Digital_product_model'));
		$this->lang->load('digital_product');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data['categories']		= $this->category_model->get_categories_tiered();
		$data['file_list']		= $this->Digital_product_model->get_list();

		$data['page_title']		= lang('product_form');

		//default values are empty if the product is new
		$data['id']					= '';
		$data['sku']				= '';
		$data['name_en']			= '';
		$data['name_da']			= '';
		$data['display_name_en']	= '';
		$data['display_name_da']	= '';
		$data['slug']				= '';
		$data['description_en']		= '';
		$data['description_da']		= '';
		$data['excerpt']			= '';
		$data['price']				= '';
		$data['saleprice']			= '';
		$data['sale_percent']		= '';
		$data['weight']				= '';
		$data['track_stock'] 		= '';
		$data['seo_title']			= '';
		$data['meta_keywords']		= '';
		$data['meta_description']	= '';
		$data['shippable']			= '';
		$data['taxable']			= '';
		$data['fixed_quantity']		= '';
		$data['quantity']			= '';
		$data['enabled']			= '';
		$data['related_products']	= array();
		$data['product_categories']	= array();
		$data['images']				= array();
		$data['product_files']		= array();
		//$data['brand']				= '';
		$data['is_assortment']		= '';
		$data['hover_image']        = '';
		
		/*$data['is_overwrite_price']	= '';
		$data['overwrite_price']		= '';
		$data['country_code']		= '';
		$data['delivery_date']		= '';
		$data['delivery_message']	= '';
		$data['is_cake']			= '';
		$data['is_width']			= '';
		
		$data['allow_occasion_sale']= '';
		$data['extend_delivery']	= '';
		$data['is_perishable']		= '';
		$data['is_flexible']		= '';*/
		
		$data['margin_percent']		= '';
		$data['share_points']		= '';
		$data['sale_points']		= '';
		$data['purchase_points']	= '';
		$data['colors']				= '';
		$data['sizes']			= '';
		$data['size_quantity']				= '';
		$data['materials']			= '';
		$data['is_new']				= '';
		$data['is_featured']		= '';
		$data['is_sale']			= ''; //here
		$data['color_select']		= '';
		$data['product_for']		= '';
		//create the photos array for later use
		$data['photos']		= array();
		$data['notes_en']				= '';
		$data['notes_da']				= '';

		$data['colorList'] = $this->color_model->getColors()->result_array();
		
		if ($id)
		{	
			// get the existing file associations and create a format we can read from the form to set the checkboxes
			$pr_files 		= $this->Digital_product_model->get_associations_by_product($id);
			foreach($pr_files as $f)
			{
				$data['product_files'][]  = $f->file_id;
			}
			
			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($id);
			$product					= $this->product_model->get_product($id);
			
			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect(site_url('product/admin'));
			}
			
			//helps us with the slug generation
			$this->product_name	= $this->input->post('slug', $product->slug);
			
			//set values to db values
			$data['id']					= $id;
			$data['sku']				= $product->sku;
			$data['name_en']			= $product->name_en;
			$data['name_da']			= $product->name_da;
			$data['display_name_en']	= $product->display_name_en;
			$data['display_name_da']	= $product->display_name_da;
			$data['seo_title']			= $product->seo_title;
			$data['meta_keywords']		= $product->meta_keywords;
			$data['meta_description']	= $product->meta_description;
			$data['slug']				= $product->slug;
			$data['description_en']		= $product->description_en;
			$data['description_da']		= $product->description_da;
			$data['excerpt']			= $product->excerpt;
			$data['price']				= $product->price;
			$data['saleprice']			= $product->saleprice;
			$data['sale_percent']		= $product->sale_percent;
			$data['weight']				= $product->weight;
			$data['track_stock'] 		= $product->track_stock;
			$data['shippable']			= $product->shippable;
			$data['quantity']			= $product->quantity;
			$data['taxable']			= $product->taxable;
			$data['fixed_quantity']		= $product->fixed_quantity;
			$data['enabled']			= $product->enabled;
			//$data['brand']				= $product->brand;
			$data['is_assortment']		= $product->is_assortment;
			$data['margin_percent']		= $product->margin_percent;
			$data['share_points']		= $product->share_points;
			$data['sale_points']		= $product->sale_points;
			$data['purchase_points']	= $product->purchase_points;
			$data['colors']				= $product->colors;
			$data['sizes']				= $product->sizes;
			$data['size_quantity']			= $product->size_quantity;
			$data['materials']			= $product->materials;
			$data['is_new']				= $product->is_new;
			$data['is_featured']		= $product->is_featured;
			$data['product_for']		= $product->product_for;
			$data['notes_en']				= $product->notes_en;
			$data['notes_da']				= $product->notes_da;
			$data['is_sale']				= $product->is_sale;
			if($product->sale_percent > 0)
			{
				$data['is_sale']				= 1;
			}
			if($product->sale_percent == 0)
			{
				$data['is_sale']				= 0;
			}

			/*if($product->colors)
			{
				$color = explode(',',$product->colors);

				for($i=0; $i< count($color); $i++)
				{
					$result = $this->color_model->getColors(array('color_id'=>$color[$i]))->row_array();
					$data['color_select'][] = array(
							'id'	=>	$result['color_id'],
							'name'	=>	$result['color_name_en'],
							'code'	=>	$result['color_code']
						);
				}
			}*/

			
			
			
			/*$data['is_overwrite_price']	= $product->is_overwrite_price;
			$data['overwrite_price']	= $product->overwrite_price;
			$data['country_code']		= $product->country_code;
			$data['delivery_date']		= $product->delivery_date;
			$data['delivery_message']	= $product->delivery_message;
			$data['is_cake']			= $product->is_cake;
			$data['is_width']			= $product->is_width;
			$data['is_featured']		= $product->is_featured;;
			$data['allow_occasion_sale']	= $product->allow_occasion_sale;
			$data['extend_delivery']		= $product->extend_delivery;
			$data['is_perishable']		= $product->is_perishable;
			$data['is_flexible']		= $product->is_flexible;*/
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit'))
			{
				
				$data['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$data['product_categories'][] = $product_category->id;
				}
				
				$data['related_products']	= $product->related_products;
				
				
				//echo "<pre>"; print_r($this->db->last_query()); exit;
				$data['images']				= (array)json_decode($product->images);
			}
		}
		
		//if $data['related_products'] is not an array, make it one.
		if(!is_array($data['related_products']))
		{
			$data['related_products']	= array();
		}
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= array();
		}
		

		
		//no error checking on these
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');

		$this->form_validation->set_rules('sku', 'lang:sku', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta_data', 'trim');
		$this->form_validation->set_rules('name_en', 'lang:name', 'trim|required|max_length[64]');
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim');
		$this->form_validation->set_rules('description_en', 'lang:description', 'trim');
		$this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
		$this->form_validation->set_rules('price', 'lang:price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('saleprice', 'lang:saleprice', 'trim|numeric|floatval');
		$this->form_validation->set_rules('weight', 'lang:weight', 'trim|numeric|floatval');
		$this->form_validation->set_rules('track_stock', 'lang:track_stock', 'trim|numeric');
		$this->form_validation->set_rules('quantity', 'lang:quantity', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'lang:shippable', 'trim|numeric');
		$this->form_validation->set_rules('taxable', 'lang:taxable', 'trim|numeric');
		$this->form_validation->set_rules('fixed_quantity', 'lang:fixed_quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
		//$this->form_validation->set_rules('brand', 'lang:brand', 'trim|required|numeric');
		$this->form_validation->set_rules('margin_percentage', 'Margin Percentage', 'trim|numeric|floatval');
		$this->form_validation->set_rules('share_points', 'Share Points', 'trim|numeric|floatval');
		$this->form_validation->set_rules('purchase_points', 'Purchase Points', 'trim|numeric|floatval');
		$this->form_validation->set_rules('sale_points', 'Sale Points', 'trim|numeric|floatval');

		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		if($duplicate)
		{
			$data['id']	= false;
		}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$data['product_options']	= $this->input->post('option');
			$data['related_products']	= $this->input->post('related_products');
			$data['assorted_products']	= $this->input->post('assorted_products');
			$data['product_categories']	= $this->input->post('categories');
			$data['images']				= $this->input->post('images');
			$data['product_files']		= $this->input->post('downloads');
			
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('brand/brand_model');
			$data['brands']	=$this->brand_model->getBrands(array('status'=>1))->result_array();
			$data['occasions'] = $this->occasion_model->getOccasions()->result_array();
			$data['events'] = $this->event_model->getEvents()->result_array();
			$this->db->order_by('color_id desc');
			$data['colours'] = $this->color_model->getColors()->result_array();
			$this->db->order_by('sequence asc');
			$data['product_sizes'] = $this->size_model->getSizes()->result_array();
			$data['product_materials']	= $this->material_model->getMaterials()->result_array();
			$data['assorted_products']	= NULL;

			// echo '<pre>';print_r($data);
			// exit;
			if($id)
			{
				$this->assorted_product_model->joins = array('PRODUCT');
				$data['assorted_products']	= $this->assorted_product_model->getAssortedProducts(array('product_id'=>$id))->result_array();
				//$data['occasions'] = $this->occasion_model->getOccasions()->result_array();
				//$data['events'] = $this->event_model->getEvents()->result_array();
				
			}
			
			$this->view($this->config->item('admin_folder').'/product/form', $data);
		}
		else
		{
			// echo "<pre>"; print_r($_POST); exit;

			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('name_en');
			}
			
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');

			if($id)
			{
				$slug		= $this->Routes_model->validate_slug($slug, $product->route_id);
				$route_id	= $product->route_id;
			}
			else
			{
				$slug	= $this->Routes_model->validate_slug($slug);
				
				$route['slug']	= $slug;	
				$route_id	= $this->Routes_model->save($route);
			}

			$save['id']					= $id;
			$save['sku']				= $this->input->post('sku');
			$save['name_en']			= $this->input->post('name_en');
			$save['name_da']			= $this->input->post('name_da');
			$save['display_name_en']	= $this->input->post('display_name_en');
			$save['display_name_da']	= $this->input->post('display_name_da');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta_keywords']		= $this->input->post('meta_keywords');
			$save['meta_description']	= $this->input->post('meta_description');
			$save['description_en']		= $this->input->post('description_en');
			$save['description_da']		= $this->input->post('description_da');
			$save['excerpt']			= $this->input->post('excerpt');
			$save['price']				= $this->input->post('price');
			$save['saleprice']			= $this->input->post('saleprice');
			$save['sale_percent']		= $this->input->post('sale_percent');
			$save['weight']				= $this->input->post('weight');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$save['quantity']			= $this->input->post('quantity');
			$save['shippable']			= $this->input->post('shippable');
			$save['taxable']			= $this->input->post('taxable');
			$save['enabled']			= $this->input->post('enabled');
			//$save['brand']				= $this->input->post('brand');
			$post_images				= $this->input->post('images');
			$save['margin_percent']		= $this->input->post('margin_percent');
			$save['share_points']		= $this->input->post('share_points');
			$save['sale_points']		= $this->input->post('sale_points');
			$save['purchase_points']	= $this->input->post('purchase_points');
			$colors						= $this->input->post('colors');
			$save['product_for']		= $this->input->post('product_for');
			$save['notes_en']			= $this->input->post('notes_en');
			$save['notes_da']			= $this->input->post('notes_da');


			if(is_array($colors))
			{
				$save['colors']			= implode(',',$colors);
			}
			$sizes						= $this->input->post('sizes');
			if(is_array($sizes))
			{
				foreach($sizes as $size)
				{
					if(isset($size['size'])){
						$size_id[] = $size['size']; 
					}
				}	
				$save['sizes']		= implode(',',$size_id);
				
			}
			if(is_array($sizes))
			{
				$save['size_quantity']			= json_encode($sizes);
			}

			$new_stock = array();
			$sizes_db = $this->db->select('sizes, size_quantity')->get_where('tbl_products', array('id' => $id) )->row_array();
			if($sizes_db) {
				$old_size = (json_decode($sizes_db['size_quantity'], true));
				
				if(is_array($sizes)) {
					// Checking if any stock is added
					foreach ($sizes as $key => $value) {
						foreach ($old_size as $k => $v) {
							if($value['size'] == $v['size']) {
								if($value['quantity'] > $v['quantity']) {
									$new_stock[] = $value['size'];
								}
							}
						}
					}
				}
			}
			
			// Restock notifiction start
			if( ! empty($new_stock)) {

				foreach ($new_stock as $key => $value) {
					$this->db->or_where("(product_id = '{$id}' AND size = {$value} AND is_restock = 0)");
					$email_list = $this->db->select('tbl_restock_notify.id, tbl_restock_notify.email')->get('tbl_restock_notify')->result_array();
					if($email_list) {
						$this->send_email_notification($email_list, null, $id, $value );
					}
					foreach ($email_list as $k => $v) {
						$restock_data = array(
							'is_restock' =>	1,
							// 'action'		=>	1,
							'modified_date'	=>	date("Y-m-d"),
							// 'restock_button'=>	date("Y-m-d"),
						);
						$this->db->update('tbl_restock_notify', $restock_data, array('id' => $v['id']));
					}
				}
			}
			// Restock notifiction end

			
			
			$materials						= $this->input->post('materials');
			if(is_array($materials))
			{
				$save['materials']			= implode(',',$materials);
			}

			$save['is_new']				= $this->input->post('is_new');
			$save['is_featured']		= $this->input->post('is_featured');
			$save['is_sale']		= $this->input->post('is_sale');
			
			
			/*$save['is_overwrite_price']	= ($this->input->post('is_overwrite_price'))?1:0;
			$save['overwrite_price']	= $this->input->post('overwrite_price');
			$save['is_featured']		= $this->input->post('is_featured');
			$save['allow_occasion_sale']	= $this->input->post('allow_occasion_sale');
			$save['extend_delivery']		= $this->input->post('extend_delivery');
			$save['delivery_date']		= $this->input->post('delivery_date');
			$save['delivery_message']	= $this->input->post('delivery_message');
			$save['is_cake']			= $this->input->post('is_cake');
			$save['is_width']			= $this->input->post('is_width');
			$save['country_code']		= $this->input->post('country_code');
			$save['is_perishable']		= ($this->input->post('is_perishable'))?1:0;
			$save['is_flexible']		= ($this->input->post('is_flexible'))?1:0;*/
			
			
			$save['slug']				= $slug;
			$save['route_id']			= $route_id;
			
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					foreach($post_images as $key => &$pi)
					{
						if($primary == $key)
						{
							$pi['primary']	= true;
							continue;
						}
					}	
				}
				
			}

			
			$save['images']		= json_encode($post_images);

			if($this->input->post('related_products'))
			{
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			if($this->input->post('assorted_products'))
			{
				//$save['assorted_products'] = $this->input->post('assorted_products');
				
				$save['is_assortment']		= 1;
			}
			else
			{
				//$save['assorted_products'] = '';
				$save['is_assortment']		= 0;
			}
			
			$assortments = $this->input->post('assorted_products');
			if(!empty($assortments))
			{
				$this->assorted_product_model->delete_assorted_product($id);
				foreach($assortments as $assortment)
				{
					$assorted['product_id'] = $save['id'];
					$assorted['assorted_id'] = $assortment;
					
					$this->assorted_product_model->save($assorted);
				}
			}
			else
			{
				$this->assorted_product_model->delete_assorted_product($id);
			}
			//echo "<pre>.......Data......."; print_r($save); exit;
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}
			
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	
			// print_r($save);	
			// exit;
			// save product 
			$product_id	= $this->product_model->save($save, $options, $categories);
			
			// add file associations
			// clear existsing
			$this->Digital_product_model->disassociate(false, $product_id);
			// save new
			$downloads = $this->input->post('downloads');
			if(is_array($downloads))
			{
				foreach($downloads as $d)
				{
					$this->Digital_product_model->associate($d, $product_id);
				}
			}			

			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'product/index/'.$product_id;
			
			$this->Routes_model->save($route);
			
			/*$occasions = $this->input->post('occasion_id');
			$events = $this->input->post('event_id');
			$this->occasion_product_model->delete('OCCASION_PRODUCTS',array('product_id'=>$product_id));
			if(!empty($occasions) && is_array($occasions))
			{
				
				$occasion_product = array();
				foreach($occasions as $occasion)
				{
					$occasion_product['occasion_id'] = $occasion;
					$occasion_product['product_id'] = $product_id;
					$occasion_product['status'] = 1;

					$this->occasion_product_model->insert('OCCASION_PRODUCTS',$occasion_product);
				}
					
			}
			
			$this->event_product_model->delete('EVENT_PRODUCTS',array('product_id'=>$product_id));
			
			if(!empty($events) && is_array($events))
			{
				//$j=0;
				
				$event_product = array();
				foreach($events as $event)
				{
					$event_product['event_id'] = $event;
					$event_product['product_id'] = $product_id;
					$event_product['status'] = 1;

					$this->event_product_model->insert('EVENT_PRODUCTS',$event_product);
				}
					
			}*/

			$this->session->set_flashdata('message', lang('message_saved_product'));

			//go back to the product list
			redirect(site_url('product/admin'));
		}
	}
	
	function product_image_form()
	{
		//print_r($data);
		//$this->bep_assets->load_asset_group('JQUERY_UI');
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}
	
	function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
// 			$config['image_library'] = 'gd2';
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 750;
			$config['height'] = 750;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
// 			$config['image_library'] = 'gd2';
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 450;
			$config['height'] = 450;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
// 			$config['image_library'] = 'gd2';
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			//$config['maintain_ratio'] = TRUE;
			$config['width'] = 210;
			$config['height'] = 210;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['file_name']	= $upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}
	
	function delete($id = false)
	{
		if ($id)
		{	
			$product	= $this->product_model->get_product($id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect(site_url('product/admin'));
			}
			else
			{

				// remove the slug
				$this->load->model('Routes_model');
				$this->Routes_model->delete($product->route_id);

				//if the product is legit, delete them
				$this->product_model->delete_product($id);

				$this->session->set_flashdata('message', lang('message_deleted_product'));
				redirect(site_url('product/admin'));
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect(site_url('product/admin'));
		}
	}
	
	public function change_status()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('enabled');
		if(is_array($id))
		{
			foreach($id as $row)
			{
				$this->product_model->update('PRODUCT',array('enabled'=>$status),array('id'=>$row));
			}
		}
	}
	
	public function getPrice()
	{
		$product_id = $this->input->post('id');
		$rows = $this->product_model->getProducts(array('id'=>$product_id))->row_array();
		
		echo json_encode($rows);
	}
	
	public function deleteAssortedProducts()
	{
		$id = $this->input->post('assorted_id');
		$this->assorted_product_model->delete_assorted_product($id);
		$success = TRUE;
		echo json_encode(array('success'=>$success));
	}

	public function is_new(){		
		$checker = $this->input->post('checker');
		$row = $this->input->post('row_id');
		
		if($checker == 0){
			$this->db->where('id',$row);
			$this->db->update('tbl_products',array('is_new'=>0));	
		}
		else
		{
			$this->db->where('id',$row);
			$this->db->update('tbl_products',array('is_new'=>1));			
		}
	}
	
	public function is_sale(){		
		$checker = $this->input->post('checker');
		$row = $this->input->post('row_id');
		
		if($checker == 0){  //To remove sale
			$product = $this->db->get_where('tbl_products',array('id'=> $row))->row_array();

			$this->db->where('id',$row);
			$success = $this->db->update('tbl_products',array('is_sale'=>0,'sale_percent'=>0,'saleprice'=>$product['saleprice']));	
		}
		else //set Sale
		{
			$this->db->where('id',$row);
			$success = $this->db->update('tbl_products',array('is_sale'=>1));			

		}
		if($success)
		{
			echo json_encode(array('success'=>'success'));
		}
		else
		{
			echo json_encode(array('success'=>'fail'));
		}
	}

	
}