<?php
class Assorted_product_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('ASSORTED_PRODUCTS'=>$this->prefix.'assorted_products','PRODUCT'=>$this->prefix.'products','CATEGORY_PRODUCT'=>$this->prefix.'category_products',
							'COUPON_PRODUCT'=>$this->prefix.'coupon_products','CATEGORY'=>$this->prefix.'categories','BRANDS'=>$this->prefix.'brands');
		$this->_JOINS=array('PRODUCT'=>array('join_type'=>'INNER','join_field'=>'assorted_products.assorted_id=products.id',
                                           'select'=>"products.*",'alias'=>'products'),
							'CATEGORY_PRODUCT'=>array('join_type'=>'INNER','join_field'=>'category_products.product_id=assorted_products.assorted_id',
                                           'select'=>"sequence",'alias'=>'category_products'),
							'BRANDS'=>array('join_type'=>'LEFT','join_field'=>'products.brand=brands.brand_id',
                                           'select'=>"brands.brand_name",'alias'=>'brands'),
                            );        
    }
	
    public function getAssortedProducts($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='assorted_products.*,products.*,LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ASSORTED_PRODUCTS']. ' assorted_products');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.name_en', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countProducts($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['ASSORTED_PRODUCTS'].' assorted_products');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['ASSORTED_PRODUCTS'].' assorted_products');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	function save($assorted)
	{
		$this->insert('ASSORTED_PRODUCTS', $assorted);
	}
	
	function delete_assorted_product($id)
	{
		// delete product 
		$this->db->where('product_id', $id);
		$this->db->delete($this->_TABLES['ASSORTED_PRODUCTS']);

	}
	
	public function getAssortedProductsById($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='assorted_products.assorted_id';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ASSORTED_PRODUCTS']. ' assorted_products');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		//(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.name', 'ASC');

		/*if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}*/
		return $this->db->get();
    }

}