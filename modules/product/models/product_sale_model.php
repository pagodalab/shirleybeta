<?php
class Product_sale_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PRODUCT'=>$this->prefix.'products','CATEGORY_PRODUCT'=>$this->prefix.'category_products',
							'COUPON_PRODUCT'=>$this->prefix.'coupon_products','CATEGORY'=>$this->prefix.'categories','BRANDS'=>$this->prefix.'brands');
		$this->_JOINS=array('BRANDS'=>array('join_type'=>'LEFT','join_field'=>'products.brand=brands.brand_id',
                                           'select'=>"brands.brand_name",'alias'=>'brands'),
                            );        
		
    }
	
	function product_autocomplete($name, $limit)
	{
		return	$this->db->like('name_en', $name)->get($this->_TABLES['PRODUCT'], $limit)->result();
		//return $this->getProducts(array('name'=>$name),NULL,array('limit'=>$limit))->result();
	}
	
	function products($data=array(), $return_count=false)
	{
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			$this->getProducts()->result();
		}
		else
		{
			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{

					$this->db->like('name_en', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join($this->_TABLES['CATEGORY_PRODUCT'], 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results($this->_TABLES['PRODUCT']);
			}
			else
			{
				return $this->db->get($this->_TABLES['PRODUCT'])->result();
			}
			
		}
	}
	

    public function getProducts($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='products.*,LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['PRODUCT']. ' products');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.display_name_en', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countProducts($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['PRODUCT'].' products');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PRODUCT'].' products');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

}