<table id="category-table" title="Categories" class="easyui-treegrid" toolbar="#toolbar" collapsible="true"	 fitColumns="true" singleSelect="true">
    <thead>  
        <tr>  
        <th field="text" sortable="true" width="180">Category</th>
        <?php /*?><th field="created_date" sortable="true" width="180"><?=lang('created_date')?></th><?php */?>
        <th field="status" sortable="true" width="100" align="center" formatter="formatStatus">Enabled</th>
    <th field="action" width="180" formatter="getActions">Action</th>
        </tr>  
    </thead>  
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<?php echo site_url('category/admin/form')?>" class="tooltip-top btn btn-small" title="Add Category"><span><i class="icon-plus"></i></span>Create</a>
    </p>
</div> 
<script language="javascript" type="text/javascript">
	$(function(){
		$('#category-table').treegrid({
			url:'<?=site_url('category/admin/treeview_json')?>',
			height:'auto',
			width:'auto',
			idField:'id',treeField:'text',
			
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('category/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit"><span><i class="icon-pencil"></i></span></a>';
		var d = '<a href="#" onclick="removeCategory()" class="tooltip-top btn btn-small" title="Delete"><span><i class="icon-trash"></i></span></a>';
		return  e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}
	
	function removeCategory()
	{
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#category-table').treegrid('getSelected');
				//console.log('row',row); return false;
				$.post('<?php  echo site_url('category/admin/delete_json')?>', {id:row.id}, function(){
					<?php $this->session->set_flashdata('message', lang('message_delete_category'));?>
					$('#category-table').treegrid('reload');
					
				});

			}
		});	
	}
</script>