<?php

class Admin extends Admin_Controller { 
    
    function __construct()
    {       
        parent::__construct();
        
        $this->authenticate->check_access('Admin', true);
        $this->lang->load('category');
        $this->load->model(array('category/category_model','category/category_products_model'));
    }
    
    function index()
    {
        //we're going to use flash data and redirect() after form submissions to stop people from refreshing and duplicating submissions
        //$this->session->set_flashdata('message', 'this is our message');
        
        $data['page_title'] = lang('categories');
        //$data['categories'] = $this->category_model->get_categories_tiered(true);
        
        $this->view($this->config->item('admin_folder').'/category/index', $data);
    }
	
	public function json()
	{
		//$this->_get_search_param();	
/*		$where = array('pid'=>0);
		if($_POST['pid']){
			$where = array('pid' =>$_POST['pid']);
		}	*/
		$total=$this->category_model->countCategories();
		//paging('id');
		//$this->_get_search_param();	
		$rows=$this->category_model->getCategories()->result_array();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}    
    
	public function delete_json()
	{
		$this->load->model('routes/routes_model');
    		$id=$this->input->post('id');
       	$this->category_model->delete('CATEGORY',array('id'=>$id));
		$category = $this->category_model->getCategories(array('id'=>$id))->row_array();
		$this->routes_model->delete('ROUTES',array('id'=>$category['route_id']));
		$this->category_model->delete('CATEGORY',array('parent_id'=>$id));
        //delete references to this category in the product to category table
        	$this->category_products_model->delete('CATEGORY_PRODUCT',array('category_id'=>$id));
	}    

    function form($id = false)
    {
        
        $config['upload_path']      = 'uploads/images/full';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = $this->config->item('size_limit');
        $config['max_width']        = '10024';
        $config['max_height']       = '768';
        $config['encrypt_name']     = true;
        $this->load->library('upload', $config);
        
        
        $this->category_id  = $id;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $data['categories']     = $this->category_model->getCategories()->result();
        $data['page_title']     = lang('category_form');
        
        //default values are empty if the customer is new
        $data['id']             = '';
        $data['name_en']           = '';
        $data['name_da']        = '';
        $data['slug']           = '';
        $data['description_en']    = '';
        $data['description_da'] = '';
        $data['excerpt']        = '';
        $data['sequence']       = '';
        $data['image']          = '';
        $data['seo_title']      = '';
        $data['meta']           = '';
        $data['parent_id']      = 0;
        $data['enabled']        = '';
        $data['error']          = '';
        
        //create the photos array for later use
        $data['photos']     = array();
        
        if ($id)
        {   
            $category       = $this->category_model->getById($id);

            //if the category does not exist, redirect them to the category list with an error
            if (!$category)
            {
                $this->session->set_flashdata('error', lang('error_not_found'));
                redirect('category/admin');
            }
            
            //helps us with the slug generation
            $this->category_name    = $this->input->post('slug', $category->slug);
            
            //set values to db values
            $data['id']             = $category->id;
            $data['name_en']        = $category->name_en;
            $data['name_da']        = $category->name_da;
            $data['slug']           = $category->slug;
            $data['description_en'] = $category->description_en;
            $data['description_da'] = $category->description_da;
            $data['excerpt']        = $category->excerpt;
            $data['sequence']       = $category->sequence;
            $data['parent_id']      = $category->parent_id;
            $data['image']          = $category->image;
            $data['seo_title']      = $category->seo_title;
            $data['meta']           = $category->meta;
            $data['enabled']        = $category->enabled;
            
        }
        
        $this->form_validation->set_rules('name_en', 'lang:name', 'trim|required|max_length[64]');
        $this->form_validation->set_rules('slug', 'lang:slug', 'trim');
        $this->form_validation->set_rules('description', 'lang:description', 'trim');
        $this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
        $this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
        $this->form_validation->set_rules('parent_id', 'parent_id', 'trim');
        $this->form_validation->set_rules('image', 'lang:image', 'trim');
        $this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
        $this->form_validation->set_rules('meta', 'lang:meta', 'trim');
        $this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
        
        
        // validate the form
        if ($this->form_validation->run() == FALSE)
        {
            $this->view($this->config->item('admin_folder').'/category/form', $data);
        }
        else
        {
            
            
            $uploaded   = $this->upload->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($data['image'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$data['image'];
                        $file[] = 'uploads/images/medium/'.$data['image'];
                        $file[] = 'uploads/images/small/'.$data['image'];
                        $file[] = 'uploads/images/thumbnails/'.$data['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                $data['error']  = $this->upload->display_errors();
                if($_FILES['image']['error'] != 4)
                {
                    $data['error']  .= $this->upload->display_errors();
                    $this->view($this->config->item('admin_folder').'/category/form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image          = $this->upload->data();
                $save['image']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image'];
                $config['new_image']    = 'uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }
            
            $this->load->helper('text');
            
            //first check the slug field
            $slug = $this->input->post('slug');
            
            //if it's empty assign the name field
            if(empty($slug) || $slug=='')
            {
                $slug = $this->input->post('name_en');
            }
            
            $slug   = url_title(convert_accented_characters($slug), 'dash', TRUE);
            
            //validate the slug
            $this->load->model('Routes_model');
            if($id)
            {
                $slug   = $this->Routes_model->validate_slug($slug, $category->route_id);
                $route_id   = $category->route_id;
            }
            else
            {
                $slug   = $this->Routes_model->validate_slug($slug);
                
                $route['slug']  = $slug;    
                $route_id   = $this->Routes_model->save($route);
            }
            
            $save['id']                = $id;
            $save['name_en']           = $this->input->post('name_en');
            $save['name_da']           = $this->input->post('name_da');
            $save['description_en']    = $this->input->post('description_en');
            $save['description_da']    = $this->input->post('description_da');
            $save['excerpt']           = $this->input->post('excerpt');
            $save['parent_id']         = intval($this->input->post('parent_id'));
            $save['sequence']          = intval($this->input->post('sequence'));
            $save['seo_title']         = $this->input->post('seo_title');
            $save['meta']              = $this->input->post('meta');
            $save['enabled']           = $this->input->post('enabled');
            $save['route_id']          = intval($route_id);
            $save['slug']              = $slug;
            
            $category_id    = $this->category_model->save($save);
            
            //save the route
            $route['id']    = $route_id;
            $route['slug']  = $slug;
            $route['route'] = 'category/index/'.$category_id.'';
            
            $this->Routes_model->save($route);
            
            $this->session->set_flashdata('message', lang('message_category_saved'));
            
            //go back to the category list
            redirect('category/admin');
        }
    }

    function delete($id)
    {
        
        $category   = $this->category_model->getById($id);
        //if the category does not exist, redirect them to the customer list with an error
        if ($category)
        {
            $this->load->model('Routes_model');
            
            $this->Routes_model->delete($category->route_id);
            $this->category_model->delete($id);
            
            $this->session->set_flashdata('message', lang('message_delete_category'));
            redirect('category/admin');
        }
        else
        {
            $this->session->set_flashdata('error', lang('error_not_found'));
        }
    }
	
	public function getTree($id = '0')
	{
		$arr = array();
		$sql = "SELECT * FROM tbl_categories WHERE parent_id = ".$id;
		$result = $this->db->query($sql);
		foreach($result->result_array() as $row)
		{
			$arr[] = array(
				'id' => $row['id'],
				'text' => $row['name_en'],
				'status' => $row['enabled'],
				'children' => $this->getTree($row['id'])
			);
		}
		return $arr;
		
	}
	
	public function treeview_json()
	{
		echo json_encode($this->getTree());
	}
}