<?php 
class Category extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->filter=TRUE;

		$this->load->model('size/size_model');
		$this->load->model('material/material_model');
		$this->load->library('pagination');
		$this->load->model('color/color_model');
		$this->load->model('product/product_sale_model');
	}
	
	function index($id)
	{	

		$data['id'] = $id;
		$name = "name_".$this->culture_code;
		
		$data['size_total'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['colors'] = $this->color_model->getColors()->result_array();
		
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){

			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['wishlist']);
		}

		$data['sub_category'] = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$id))->result_array();
		$data['id'] = $id;
		//get the category
		$data['category_detail'] = $this->category_model->getById($id);
		
		$this->menu_active = $data['category_detail']->slug;
		if($data['category_detail']->parent_id != 0)
		{
			$this->submenu_active = $data['category_detail']->slug;
		}
		
		
		if (!$data['category_detail'] || $data['category_detail']->enabled==0)
		{
			show_404();
		}
		
		// $this->product_model->joins=array('CATEGORY_PRODUCT');		
		//$product_count = $this->product_model->count(array('category_id'=>$data['category_detail']->id,'enabled'=>1));
		
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['category_detail']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			// echo'<pre> page '; print_r($page); 
			$page	= $page[0];
		}
		
		// exit;
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		

		$data['meta']		= $data['category_detail']->meta;
		$data['seo_title']	= (!empty($data['category_detail']->seo_title))?$data['category_detail']->seo_title:$data['category_detail']->$name;
		$data['page_title']	= $data['category_detail']->$name;
		
		$sort_array = array(
			'name/asc' => array('by' => 'products.name_en', 'sort'=>'ASC'),
			'name/desc' => array('by' => 'products.name_en', 'sort'=>'DESC'),
			'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
			'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
			);
		$sort_by	= NULL;

		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		

		//set up pagination
		

		$limit=($this->input->get('limit'))?$this->input->get('limit'):6;
		$offset=$this->input->get('per_page');

		$this->product_model->joins=array('CATEGORY_PRODUCT','CATEGORY');
		$this->db->where('(categories.id ='.$data['category_detail']->id.' or categories.parent_id = '.$data['category_detail']->id.')');
		//$this->db->where('categories.id ='.$data['category_detail']->id);
		
		// $where = 'products.enabled = 1';
		
		$where = $this->_get_where();

		if($where)
		{
			$this->db->where($where);
		}	
		
		if($data['category_detail']->id == 71){
			if($this->session->userdata('currency') == 'euro'){
				$this->db->where('products.name_da', 'gift_card_euro');
			}elseif($this->session->userdata('currency') == 'dollar'){
				$this->db->where('products.name_da', 'gift_card_usd');
			}else{
				$this->db->where('products.name_da', 'gift_card_DkK');
			}
			$this->db->order_by('price','asc');
		}
		$this->db->where('products.enabled = 1');
		//$this->db->where("(country_code='".$this->country_code."' OR country_code='BOTH')",NULL);
		$this->db->group_by('products.id');
		if($sort_by)
		{
			$this->db->order_by($sort_by['by'].' ' . $sort_by['sort']);

		}
		

		$data['products']	= $this->product_model->getProducts(NULL,NULL, array('limit'=>$limit,'offset'=> $offset))->result();
// 		echo $this->db->last_query();
	
// 		echo "<pre>".$this->db->last_query();print_r($data['products']);
	   // exit;
		$this->db->where('(categories.id ='.$data['category_detail']->id.' or categories.parent_id = '.$data['category_detail']->id.')');
		if($where)
		{
			$this->db->where($where);
		}	
		$this->db->where('products.enabled = 1');
		$this->db->group_by('products.id');
		if($sort_by)
		{
			$this->db->order_by($sort_by['by'].' ' . $sort_by['sort']);

		}

		$data['count']	= $this->product_model->getProducts(NULL,NULL)->result();
		
//echo "<pre>".$this->db->last_query(); print_r(count($data['count'])); //exit;
		
		$config=$this->pagination_config();
		$config['base_url']		= site_url($base_url);
		$config['uri_segment']	= $segments;
		$config['per_page']		= $limit;
		$config['total_rows']	= count($data['count']);
		$query_string=unset_query_param('per_page');

		$config['base_url']=site_url($this->uri->segment(1)) . '?'.$query_string;
		$config['page_query_string']= TRUE;
		//$this->pagination->initialize($config);
		
		// print_r($config['base_url']);
		// exit;

		// echo count($data['count']);
		// echo "<pre>"; print_r($this->db->last_query());
		// echo "<pre>"; print_r($data['products']);
		// exit;

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		
		
		if($id == '26' || $data['category_detail']->parent_id =='26') //size for baby
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'26'),'sequence asc')->result();
		}
		else if($id == '23' || $data['category_detail']->parent_id == '23') //size for child
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'23'),'sequence asc')->result();
		}	
		else //size for adult
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'24'),'sequence asc')->result();
	    }
// 		$data['sizes'] = $this->size_model->getSizes(NULL,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();

		$this->view('category/index', $data);
	}
	
	private function _get_where()
	{
		//$where = array();
		$where = '';
		$size = $this->input->get('size');
		$gender = $this->input->get('gender');
		$material = $this->input->get('material');
		
		if($size)
		{	

			$size_array= explode(",",$this->input->get('size'));
			// print_r($size_array);
			// echo "count ". count($size_array); 
			for($i=0; $i < count($size_array) ; $i++)
			{	
				if($i == 0)
				{	
					
					$where .='products.sizes like "%'.$size_array[$i].'%"';
					

				}
				else
				{
					$where .=' OR products.sizes like "%'.$size_array[$i].'%"';
				}	
				
				
			}
			
		}
		
		if($gender)
		{
			$gender_array= explode(",",$this->input->get('gender'));
			for($i=0; count($gender_array)> $i ; $i++)
			{	
				if($i == 0)
				{	
					if($size)
					{		
						$where .=' AND products.product_for like "%'.$gender_array[$i].'%"';
					}
					else
					{
						$where .='products.product_for like "%'.$gender_array[$i].'%"';
					}

				}
				else
				{
					$where .='OR products.product_for like "%'.$gender_array[$i].'%"';
				}	
				
				
			}
		}
		
		if($material)
		{
			$material_array= explode(",",$this->input->get('material'));
			for($i=0; count($material_array)> $i ; $i++)
			{	
				if($i == 0)
				{	
					if($size || $gender)
					{
						$where .='AND products.materials like "%'.$material_array[$i].'%"';
					}
					else
					{
						$where .='products.materials like "%'.$material_array[$i].'%"';
					}

				}
				else
				{
					$where .='OR products.materials like "%'.$material_array[$i].'%"';
				}	
				
				
			}
		}
		
		return $where;
	}

	// function priceSort()
	// {
	// 	$data['wishlist']= array();

	// 	if($this->customer_model->is_logged_in(false,false)){

	// 		$this->db->where('id',$this->go_cart->customer('id'));
	// 		$customer=$this->customer_model->getCustomers()->row_array();
	// 		$data['wishlist'] = unserialize($customer['wishlist']);
	// 	}

	// 	$value1 = $this->input->post('value1');
	// 	$value2 = $this->input->post('value2');
	// 	$id = $this->input->post('id');

	// 	$this->product_model->joins=array('CATEGORY_PRODUCT');
	// 	$where = array_merge(array('category_id'=>$id,'enabled'=>1),$this->_get_where());
	// 	$this->db->where('price between "'.$value1.'" and "'.$value2.'"');
	// 	$this->db->where('category_products.category_id',$id);

	// 	$data['products']	= $this->product_model->getProducts()->result();

	// 	foreach ($data['products'] as &$p)
	// 	{
	// 		$p->images	= (array)json_decode($p->images);
	// 		$p->options	= $this->option_model->get_product_options($p->id);
	// 	}


	// 	$this->load->view('category/content',$data);
	// }

	// function colorSort()
	// {
	// 	$data['wishlist']= array();

	// 	if($this->customer_model->is_logged_in(false,false)){

	// 		$this->db->where('id',$this->go_cart->customer('id'));
	// 		$customer=$this->customer_model->getCustomers()->row_array();
	// 		$data['wishlist'] = unserialize($customer['wishlist']);
	// 	}

	// 	$data['colorId'] = $this->input->post('colorId');
	// 	$id = $this->input->post('id');

	// 	$this->product_model->joins=array('CATEGORY_PRODUCT');
	// 	$where = array_merge(array('category_id'=>$id,'enabled'=>1),$this->_get_where());
	// 	$this->db->where('colors like "%'.$data['colorId'].'%"');
	// 	$this->db->where('category_products.category_id',$id);

	// 	$data['products']	= $this->product_model->getProducts()->result();

	// 	foreach ($data['products'] as &$p)
	// 	{
	// 		$p->images	= (array)json_decode($p->images);
	// 		$p->options	= $this->option_model->get_product_options($p->id);
	// 	}

	// 	$this->load->view('category/content',$data);
	// }

	function NewProduct()
	{
		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
		$this->menu_active = 'New';
		$data['colors'] = $this->color_model->getColors()->result_array();
		$this->db->limit('12');
		$this->db->where('category_id <>',71);
		$this->db->where('products.enabled',1);
		$this->db->where('is_new',1);
		$this->db->group_by('id');
		$data['products'] = $this->product_model->getProducts(null,'id desc')->result();
		
// 		echo '<pre>'; echo $this->db->last_query();  print_r($data['products']); exit;

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		$data['type'] = 'NewProduct';
		
		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();
		$this->view('search/index', $data);
	}
	
	function SaleProduct()
	{
		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
		$this->menu_active = 'Sale';
		$data['colors'] = $this->color_model->getColors()->result_array();
		$this->db->limit('12');
		$this->db->where('category_id <>',71);
		$this->db->where('products.enabled',1);;
		$this->db->where('products.is_sale',1);;
		$this->db->group_by('id');
		$data['products'] = $this->product_model->getProducts(null,'id')->result();
        $data['type'] = 'SaleProduct';

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();
		$this->view('search/index', $data);
	}
	
        /*
         * addmore for new product
         */
        function AddNewProduct()
        {
//            print_r($this->input->post());
//            exit;
        	$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
        	$offset = $this->input->post('start');
//		$this->menu_active = 'New';
        	$data['colors'] = $this->color_model->getColors()->result_array();
        	$this->db->limit('12',$offset);
        	$this->db->where('category_id <>',71);
        	$this->db->where('products.enabled',1);
        	$this->db->group_by('id');
        	$data['products'] = $this->product_model->getProducts(null,'id desc')->result();
        	foreach ($data['products'] as &$p)
        	{
        		$p->images	= (array)json_decode($p->images);
        		$p->options	= $this->option_model->get_product_options($p->id);
        	}
        	$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
        	$data['materials'] = $this->material_model->getMaterials()->result();
        	$this->load->view('search/addmore_view', $data);
        }

	// function sortSize()
	// {
	// 	$data['category_detail'] = $this->category_model->getById($this->input->post('id'));
	// 	$data['materials'] = $this->material_model->getMaterials()->result();
	// 	//echo "<pre>"; print_r($_POST);
	// 	$requests	= $this->input->post('checked');
	// 	echo "<pre>"; print_r($requests); exit;
	// 	$flag = false;
	// 	if(count($requests)>0)
	// 	{	
	// 		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
	// 		$this->db->where('categories.id',$this->input->post('id'));
	// 		$this->db->where('products.enabled','1');
	// 		$where ='';
	// 		for($i=0; count($requests)> $i ; $i++)
	// 		{	
	// 			if($flag == false)
	// 			{	

	// 					$where .='products.sizes like "%'.$requests[$i].'%"';

	// 				$flag = true;
	// 			}
	// 			else
	// 			{
	// 				$where .='OR products.sizes like "%'.$requests[$i].'%"';
	// 			}	


	// 		}

	// 		$this->db->where($where);

	// 		$data['products'] = $this->product_model->getProducts()->result();
	// 	}

	// 	// echo $this->db->last_query();
	// 	foreach ($data['products'] as &$p)
	// 	{
	// 		$p->images	= (array)json_decode($p->images);
	// 		$p->options	= $this->option_model->get_product_options($p->id);
	// 	}

	// 	//pagination
	// 	$limit=($this->input->get('limit'))?$this->input->get('limit'):3;
	// 	$offset=$this->input->get('per_page');

	// 	$segments	= $this->uri->total_segments();
	// 	$base_url	= $this->uri->segment_array();

	// 	$config=$this->pagination_config();
	// 	$config['base_url']		= site_url($base_url);
	// 	$config['uri_segment']	= $segments;
	// 	$config['per_page']		= $limit;
	// 	$config['total_rows']	= count($data['products']);
	// 	$query_string=unset_query_param('per_page');

	// 	$config['base_url']=site_url($this->uri->segment(1)) . '?'.$query_string;
	// 	$config['page_query_string']= TRUE;
	// 	$this->pagination->initialize($config);
	// 	//end of pagination

	// 	$data['sizes'] = $this->size_model->getSizes()->result();




	// 	// echo '<pre>';print_r($data['products']);
	// 	// exit;
	// 	$this->load->view('category/index',$data);
	// 	//redirect as to change the url
	// 	//redirect(site_url('order/admin'));	

	// }

	// function sortGender()
	// {
	// 	//echo "<pre>"; print_r($_POST);
	// 	$requests	= $this->input->post('checked');
	// 	//echo "<pre>"; print_r($requests); exit;
	// 	$flag = false;
	// 	if(count($requests)>0)
	// 	{	
	// 		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
	// 		$this->db->where('categories.id',$this->input->post('id'));
	// 		$this->db->where('products.enabled','1');
	// 		$where ='';
	// 		for($i=0; count($requests)> $i ; $i++)
	// 		{	
	// 			if($flag == false)
	// 			{	

	// 					$where .='products.product_for like "%'.$requests[$i].'%"';

	// 				$flag = true;
	// 			}
	// 			else
	// 			{
	// 				$where .='OR products.product_for like "%'.$requests[$i].'%"';
	// 			}	


	// 		}

	// 		$this->db->where($where);

	// 		$data['products'] = $this->product_model->getProducts()->result();
	// 	}

	// 	// echo $this->db->last_query();
	// 	foreach ($data['products'] as &$p)
	// 	{
	// 		$p->images	= (array)json_decode($p->images);
	// 		$p->options	= $this->option_model->get_product_options($p->id);
	// 	}


	// 		$data['sizes'] = $this->size_model->getSizes()->result();

	// 	// echo '<pre>';print_r($data['products']);
	// 	// exit;
	// 	$this->load->view('category/content',$data);
	// }

	// function sortMaterial()
	// {

	// 	//echo "<pre>"; print_r($_POST);
	// 	$requests	= $this->input->post('checked');
	// 	//echo "<pre>"; print_r($requests); exit;
	// 	$flag = false;
	// 	if(count($requests)>0)
	// 	{	
	// 		$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
	// 		$this->db->where('categories.id',$this->input->post('id'));
	// 		$this->db->where('products.enabled','1');
	// 		$where ='';
	// 		for($i=0; count($requests)> $i ; $i++)
	// 		{	
	// 			if($flag == false)
	// 			{	

	// 					$where .='products.materials like "%'.$requests[$i].'%"';

	// 				$flag = true;
	// 			}
	// 			else
	// 			{
	// 				$where .='OR products.materials like "%'.$requests[$i].'%"';
	// 			}	


	// 		}

	// 		$this->db->where($where);

	// 		$data['products'] = $this->product_model->getProducts()->result();
	// 	}

	// 	// echo $this->db->last_query();
	// 	foreach ($data['products'] as &$p)
	// 	{
	// 		$p->images	= (array)json_decode($p->images);
	// 		$p->options	= $this->option_model->get_product_options($p->id);
	// 	}


	// 		$data['sizes'] = $this->size_model->getSizes()->result();

	// 	// echo '<pre>';print_r($data['products']);
	// 	// exit;
	// 	$this->load->view('category/content',$data);
	// 	//redirect as to change the url
	// 	//redirect(site_url('order/admin'));	

	// }

        function show_all($id)
        {
        	$this->load->model('category/category_model');
        	$data['subcategory_list'] = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$id))->result();

        	$this->view('category/list',$data);

        }

	/*function sale()
	{
		$this->menu_active = 'Sale';
	
		$data['colors'] = $this->color_model->getColors()->result_array();
		
		$this->db->limit('20');
		$this->db->where('saleprice < price');
		$this->db->where('enabled',1);
		$data['products'] = $this->product_model->getProducts(null,'id desc')->result();

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
	
		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();
		$this->view('search/index', $data);
	}*/
	
	function sale()
	{
		$this->load->model('banner/banner_model');
		$this->menu_active = 'Sale';

		$data['colors'] = $this->color_model->getColors()->result_array();
		
		$this->db->limit('20');
		$this->db->where('saleprice < price');
		$this->db->where('enabled',1);
		$data['products'] = $this->product_model->getProducts(null,'id desc')->result();

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		$data['banner'] = $this->banner_model->banner(10);
	//echo "<pre>"; print_r($data['banner']); exit;
		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();
		
		$this->view('category/sales', $data);
	}

        /*
         * for jscroll
         */
        function addmore()
        {	
        	$data['id'] = $this->input->post('category');
        	$limit=12;
        	$data['size_total'] = $this->size_model->getSizes(null,'sequence asc')->result();
        	$offset=$this->input->post('start');
        	$data['category_detail'] = $this->category_model->getById($data['id']);
        	$this->product_model->joins=array('CATEGORY_PRODUCT','CATEGORY');
        	$this->db->where('(categories.id ='.$data['category_detail']->id.' or categories.parent_id = '.$data['category_detail']->id.')');


        	$where = $this->_get_where();

        	if($where)
        	{
        		$this->db->where($where);
        	}	

        	if($data['category_detail']->id == 71){
        		if($this->session->userdata('currency') == 'euro'){
        			$this->db->where('products.name_da', 'gift_card_euro');
        		}elseif($this->session->userdata('currency') == 'dollar'){
        			$this->db->where('products.name_da', 'gift_card_usd');
        		}else{
        			$this->db->where('products.name_da', 'gift_card_DkK');
        		}
        	}
        	$this->db->where('products.enabled = 1');
		//$this->db->where("(country_code='".$this->country_code."' OR country_code='BOTH')",NULL);
        	$this->db->group_by('products.id');
        	$data['products']	= $this->product_model->getProducts(NULL,NULL, array('limit'=>$limit,'offset'=> $offset))->result();
        	$this->db->where('(categories.id ='.$data['category_detail']->id.' or categories.parent_id = '.$data['category_detail']->id.')');
        	if($where)
        	{
        		$this->db->where($where);
        	}	
        	$this->db->where('products.enabled = 1');
        	$this->db->group_by('products.id');

        	$data['count']	= $this->product_model->getProducts(NULL,NULL)->result();


        	foreach ($data['products'] as &$p)
        	{
        		$p->images	= (array)json_decode($p->images);
        		$p->options	= $this->option_model->get_product_options($p->id);
        	}

		if($data['id'] == '26' || $data['category_detail']->parent_id =='26') //size for baby
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'26'),'sequence asc')->result();
		}
		else if($data['id'] == '23' || $data['category_detail']->parent_id == '23') //size for child
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'23'),'sequence asc')->result();
		}	
		else //size for adult
		{
			$data['sizes'] = $this->size_model->getSizes(array('type'=>'24'),'sequence asc')->result();
		}
		$this->load->view('category/addmore-view', $data);
	}	
	
	function infinite_scroll()
	{
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$id = $this->input->get('id');	
		$data['offset'] = $offset;

		$data['sub_category'] = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$id))->result_array();
		$data['category_detail'] = $this->category_model->getById($id);

		$where = $this->_get_where();
		if($where)
		{
			$this->db->where($where);
		}	
		
		$sort_by	= NULL;

		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}

		$this->product_model->joins=array('CATEGORY_PRODUCT','CATEGORY');
		$this->db->where('(categories.id ='.$data['category_detail']->id.' or categories.parent_id = '.$data['category_detail']->id.')');

		if($data['category_detail']->id == 71){
			if($this->session->userdata('currency') == 'euro'){
				$this->db->where('products.name_da', 'gift_card_euro');
			}elseif($this->session->userdata('currency') == 'dollar'){
				$this->db->where('products.name_da', 'gift_card_usd');
			}else{
				$this->db->where('products.name_da', 'gift_card_DkK');
			}
			$this->db->order_by('price','asc');
		}
		$this->db->where('products.enabled = 1');
		$this->db->group_by('products.id');
		if($sort_by)
		{
			$this->db->order_by($sort_by['by'].' ' . $sort_by['sort']);
		}

		$data['products']	= $this->product_model->getProducts(NULL,NULL, array('limit'=>$limit,'offset'=> $offset))->result();
		$data['product_count'] = count($data['products']);
		$data['size_total'] = $this->size_model->getSizes(null,'sequence asc')->result();

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		if($data['product_count'] > 0)
		{
			$this->load->view('category/infinite_add',$data);
		}
		else
		{
		}

	}

	function infinite_scroll_sale()
	{
		$limit = $this->input->get("limit");
		$offset = $this->input->get("offset");
		$type = $this->input->get("type");
		// $id = $this->input->get('id');
		$this->menu_active = 'Sale';
		$data['colors'] = $this->color_model->getColors()->result_array();
		$data['offset'] = $offset;
		// $this->db->limit('12');
		
		// $this->db->where('category_id <>',71);
		$this->db->where('products.enabled',1);;
		if($type == 'NewProduct')
		{
		    $this->db->where('products.is_new',1);;
		}
		elseif($type == 'SaleProduct')
		{
		    $this->db->where('products.is_sale',1);;
		}
		else
		{
		   $this->db->like('products.display_name_'.$this->culture_code ,$type);
		 
		}
		
		// $this->db->group_by('id');
		$data['products'] = $this->product_sale_model->getProducts(null,null,array('limit'=>$limit,'offset'=> $offset))->result();
        
       
		$data['product_count'] = count($data['products']);
		
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			// $p->options	= $this->option_model->get_product_options($p->id);
		}
		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();
		if($data['product_count'] > 0)
		{
			$this->load->view('category/infinite_add_sale',$data);
		}
		else
		{
		}

	}
}