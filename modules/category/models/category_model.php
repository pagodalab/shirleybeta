<?php
class Category_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('CATEGORY'=>$this->prefix.'categories','CATEGORY_PRODUCT'=>$this->prefix.'category_products');
		$this->_JOINS=array('KEY'=>array('join_type'=>'INNER','join_field'=>'field.id=field.id',
                                           'select'=>"*",'alias'=>'alias'),
                            );        
    }

    public function getCategories($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='categories.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['CATEGORY']. ' categories');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('categories.name_en', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }

    function getById($id)
    {
        return $this->getCategories(array('id'=>$id))->row();
    }
    
    function get_categories_tiered($admin = false)
    {
        if(!$admin) $this->db->where('enabled', 1);
        $this->db->or_where('id',75);
        
        $this->db->order_by('sequence');
        $this->db->order_by('name_en', 'ASC');
        $categories = $this->db->get($this->_TABLES['CATEGORY'])->result();
        
        $results    = array();
        foreach($categories as $category) {

            // Set a class to active, so we can highlight our current category
            if($this->uri->segment(1) == $category->slug) {
                $category->active = true;
            } else {
                $category->active = false;
            }

            $results[$category->parent_id][$category->id] = $category;
        }
        
        return $results;
    }
    
    /*
	*	Function Save
	*/   
    function save($category)
    {
        if ($category['id'])
        {
			$category['modified_date']=date('Y-m-d H:i:s');
            $this->update('CATEGORY', $category,array('id'=>$category['id']));
            return $category['id'];
        }
        else
        {
			$category['added_date']=date('Y-m-d H:i:s');
            $this->insert('CATEGORY', $category);
            return $this->db->insert_id();
        }
    }
 
    /*
	*	Function Delete
	
    function delete($id)
    {
        $this->delete('CATEGORY',array('id'=>$id));
        //delete references to this category in the product to category table
        $this->delete('CATEGORY_PRODUCT',array('category_id'=>$id));
    }*/

    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['CATEGORY'].' categories');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }	
	
	public function countCategories($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['CATEGORY'].' categories');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }
}