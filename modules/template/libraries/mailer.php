<?php

class Mailer
{
	var $CI;
	
	public function __construct()
	{
		$this->CI=&get_instance();
		$this->CI->load->library('email');
		$this->CI->load->helper('email');	
		$this->CI->load->library('parser');			
	}
	
	function _initialize_email()
	{
		$config['useragent'] = "JAE";
		$config['protocol'] = $this->CI->preference->item('email_protocol');
		$config['mailpath'] = $this->CI->preference->item('email_mailpath');
		$config['smtp_host'] = $this->CI->preference->item('smtp_host');
		//$config['smtp_user'] = $this->CI->preference->item('smtp_user');
		//$config['smtp_pass'] = $this->CI->preference->item('smtp_pass');
		$config['smtp_port'] = $this->CI->preference->item('smtp_port');
		$config['smtp_timeout'] = $this->CI->preference->item('smtp_timeout');
		$config['wordwrap'] = $this->CI->preference->item('email_wordwrap');
		$config['wrapchars'] = $this->CI->preference->item('email_wrapchars');
		$config['mailtype'] = $this->CI->preference->item('email_mailtype');
		$config['charset'] = $this->CI->preference->item('email_charset');
		$config['bcc_batch_mode'] = $this->CI->preference->item('bcc_batch_mode');
		$config['bcc_batch_size'] = $this->CI->preference->item('bcc_batch_size');
		$this->CI->email->initialize($config);
	}	
	
	function send($to,$subject,$message,$data=NULL)
	{
		$this->CI->load->library('parser');
		// Setup Email settings
		$this->_initialize_email();

		// Send email
		$this->CI->email->from($this->CI->preference->item('automated_from_email'), $this->CI->preference->item('automated_from_name'));
		$this->CI->email->to($to);
		$this->CI->email->subject($subject);
		if(!is_null($data))
		{
			$message=$this->CI->parser->parse_string($message,$data,TRUE);
		}
		$this->CI->email->message($message);

		if( !$this->CI->email->send())
		{
			return FALSE;
		}

		return TRUE;
	}
	
}
