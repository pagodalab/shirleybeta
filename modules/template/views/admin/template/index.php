<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('template_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="template-search-form">
<table width="100%">
<tr><td><label><?=lang('template_name')?></label>:</td>
<td><input type="text" name="search[template_name]" id="search_template_name"  class="easyui-validatebox"/></td>
<td><label><?=lang('file_name')?></label>:</td>
<td><input type="text" name="search[file_name]" id="search_file_name"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?=lang('added_date')?></label>:</td>
<td><input type="text" name="date[added_date][from]" id="search_added_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[added_date][to]" id="search_added_date_to"  class="easyui-datebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small"><span><i class="icon-search"></i></span> <?php  echo lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="template-table" data-options="pagination:true,title:'<?php  echo lang('template')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="template_id" sortable="true" width="30"><?=lang('template_id')?></th>
<th field="template_name" sortable="true" width="50"><?=lang('template_name')?></th>
<th field="file_name" sortable="true" width="50"><?=lang('file_name')?></th>
<th field="added_date" sortable="true" width="50"><?=lang('added_date')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Create Template"><span><i class="icon-plus"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit template form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-template" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?=lang('template_name')?>:</label></td>
					  <td width="66%"><input name="template_name" id="template_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('file_name')?>:</label></td>
					  <td width="66%"><input name="file_name" id="file_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?=lang('added_date')?>:</label></td>
					  <td width="66%"><input name="added_date" id="added_date" class="easyui-datetimebox" required="true"></td>
		       </tr><input type="hidden" name="template_id" id="template_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#template-search-form').form('clear');
			$('#template-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#template-table').datagrid({
				queryParams:{data:$('#template-search-form').serialize()}
				});
		});		
		$('#template-table').datagrid({
			url:'<?php  echo site_url('template/admin/template/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="Edit Template"><span><i class="icon-pencil"></i></span> </a>';
		var d = '<a href="#" onclick="removetemplate('+index+')" class="tooltip-top btn btn-small" title="Delete Template"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-template').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_template')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#template-table').datagrid('getRows')[index];
		if (row){
			$('#form-template').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_template')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removetemplate(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#template-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('template/admin/template/delete_json')?>', {id:[row.template_id]}, function(){
					$('#template-table').datagrid('deleteRow', index);
					$('#template-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#template-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].template_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('template/admin/template/delete_json')?>',{id:selected},function(data){
						$('#template-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-template').form('submit',{
			url: '<?php  echo site_url('template/admin/template/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-template').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#template-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>