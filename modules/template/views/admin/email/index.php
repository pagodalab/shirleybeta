<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" title="<?=lang('email_template_search')?>" style="padding:5px;width:1088px" collapsible="true" collapsed="true" iconCls="icon-search">
<form action="" method="post" id="email_template-search-form">
<table width="100%">
<tr><td><label><?=lang('name')?>:</label></td>
<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<td><label><?=lang('subject')?>:</label></td>
<td><input type="text" name="search[subject]" id="search_subject"  class="easyui-validatebox"/></td>
</tr>
<tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="email_template-table" pagination="true" title="<?=lang('email_template')?>" pagesize="20" rownumbers="true" toolbar="#toolbar" collapsible="true"
			 fitColumns="true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="email_template_id" sortable="true" width="10">Id</th>
    <th field="name" sortable="true" width="50"><?=lang('name')?></th>
    <th field="subject" sortable="true" width="80"><?=lang('subject')?></th>
    <th field="created_date" sortable="true" width="50"><?=lang('created_date')?></th>
    <th field="modified_date" sortable="true" width="50"><?=lang('modified_date')?></th>
    <th field="action" width="100" formatter="getActions"><?=lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="tooltip-top btn btn-small" onclick="create()" title="Create Template"><span><i class="icon-plus"></i></span> Create</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<!--for create and edit email_template form-->
<div id="dlg"  class="easyui-dialog" style="width:800px;height:auto;top:20px;padding:10px 20px" data-options="closed:true,collapsible:true,modal:true,buttons:'#dlg-buttons'">
    <form id="form-email_template" method="post" >
    <table>
		<tr>
		              <td width="25%" ><label><?=lang('name')?>:</label></td>
					  <td width="75%"><input name="name" id="name" class="easyui-validatebox" required="true" style="width:500px;"/></td>
		       </tr><tr>
		              <td width="25%" ><label><?=lang('slug_name')?>:</label></td>
					  <td width="75%"><input name="slug_name" id="slug_name" class="easyui-validatebox" required="true"  style="width:500px;"/></td>
		       </tr><tr>
		              <td width="25%" ><label><?=lang('subject')?>:</label></td>
					  <td width="75%"><input name="subject" id="subject" class="easyui-validatebox" required="true"  style="width:500px;"/></td>
		       </tr><tr>
		              <td width="25%" ><label><?=lang('body')?>:</label></td>
					  <td width="75%"><textarea name="body" id="body" class="easyui-validatebox redactor" required="true" style="width:500px;height:300px"></textarea></td>
		       </tr>
    </table>
    <input type="hidden" name="email_template_id"/>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" onClick="save()"><span><i class="icon-ok-sign"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		
		$('#clear').click(function(){
			$('#email_template-search-form').form('clear');
			$('#email_template-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#email_template-table').datagrid({
				queryParams:{data:$('#email_template-search-form').serialize()}
				});
		});		
		$('#email_template-table').datagrid({
			url:'<?=site_url('template/admin/email/json')?>',
			height:'auto',
			width:'1088',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="Edit Email Template"><span><i class="icon-pencil"></i></span> </a>';
		var d = '<a href="#" onclick="removeEmail('+index+')" class="tooltip-top btn btn-small" title="Delete Template"><span><i class="icon-trash"></i></span> </a>';
		return e+d;		
	}
	
	function create(){
		//Create code here
		$('#dlg').window('open').window('setTitle','<?=lang('create_email_template')?>');
		tinymce.get('body').setContent(''); 
		$('#form-email_template').form('clear');
	}	

	function edit(index)
	{
		var row = $('#email_template-table').datagrid('getRows')[index];
		if (row){
			$('#form-email_template').form('load',row);
			$('#dlg').window('open').window('setTitle','<?=lang('edit_email_template')?>');
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeEmail(index)
	{
		$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#email_template-table').datagrid('getRows')[index];
				$.post('<?=site_url('template/admin/email/delete_json')?>', {id:[row.email_template_id]}, function(){
					$('#email_template-table').datagrid('deleteRow', index);
					$('#email_template-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#email_template-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].email_template_id);
			}
			
			$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?=site_url('template/admin/email/delete_json')?>',{id:selected},function(data){
						$('#email_template-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No template Selected!');	
		}
		
	}
	
	function save()
	{
		$('#form-email_template').form('submit',{
			url: '<?=site_url('template/admin/email/form_json')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-email_template').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?=lang('success')?>',msg: result.msg});
					$('#email_template-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?=lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>