<?php


$lang['template_id'] = 'Template Id';
$lang['template_name'] = 'Template Name';
$lang['file_name'] = 'File Name';
$lang['added_date'] = 'Added Date';

$lang['create_template']='Create Template';
$lang['edit_template']='Edit Template';
$lang['delete_template']='Delete Template';
$lang['template_search']='Template Search';

$lang['template']='Template';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

