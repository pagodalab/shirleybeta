<?php


$lang['template_id'] = 'Template Id';
$lang['subject'] = 'Subject';
$lang['description'] = 'Description';
$lang['created_date'] = 'Created Date';
$lang['sent_date'] = 'Sent Date';
$lang['is_sent'] = 'Is Sent';

$lang['create_promotional_mail']='Create Promotional Mail';
$lang['edit_promotional_mail']='Edit Promotional Mail';
$lang['delete_promotional_mail']='Delete Promotional Mail';
$lang['promotional_mail_search']='Promotional Mail Search';

$lang['promotional_mail']='Promotional Mail';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

