<?php

class Template extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->model('template/template_model');
        $this->lang->load('template/template');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'Template';
		$data['page_title'] = 'Template';
		//$data['page'] = $this->config->item('admin_folder') . "template/index";
		$data['module'] = 'template';
		$this->view($this->config->item('admin_folder') . "/template/index",$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->template_model->count();
		paging('template_id');
		$this->_get_search_param();	
		$rows=$this->template_model->getTemplates()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['template_name']!='')?$this->db->like('template_name',$params['search']['template_name']):'';
($params['search']['file_name']!='')?$this->db->like('file_name',$params['search']['file_name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->template_model->getTemplates()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->template_model->delete('TEMPLATES',array('template_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('template_id'))
        {
            $success=$this->template_model->insert('TEMPLATES',$data);
        }
        else
        {
            $success=$this->template_model->update('TEMPLATES',$data,array('template_id'=>$data['template_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['template_id'] = $this->input->post('template_id');
		$data['template_name'] = $this->input->post('template_name');
		$data['file_name'] = $this->input->post('file_name');
		$data['added_date'] = $this->input->post('added_date');

        return $data;
   }
   
   	
	    
}