 <?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	var $_user_id;
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model(array('product/product_model','size/size_model','restock_notify/restock_notify_model'));
		$this->_user_id = $this->session->userdata('admin')['id'];
	}

	public function index()
	{

		// Display Page
		$data['page_title'] = 'Restock Notification';
		//$data['page'] = $this->config->item('template_admin') . "product/index";
		$data['module'] = 'restock_notify';
		$this->view($this->config->item('template_admin') . "admin/restock_notify/index",$data);		
	}

	public function json()
	{
		$this->restock_notify_model->joins = array('PRODUCTS','SIZES');
		paging('restock_notify.id desc');		
		// $this->db->where('is_restock','0');
		$rows=$this->restock_notify_model->getProducts()->result_array();
		
		// $this->db->where('is_restock','0');
		$total=$this->restock_notify_model->count();
		
		echo json_encode(array('total'=>$total, 'rows'=>$rows));
	}

	public function update_size_quantity()
	{
		$post_data = $this->input->post();
		$restock_id = $post_data['id'];
		$size = $post_data['size'];
		$product_id = $post_data['product_id'];
		// $quantity_restock = $post_data['quantity'];

		//generating email list
	$this->db->where(array('id'=>$post_data['id']));
		$email_list = $this->db->select('tbl_restock_notify.email')->get('tbl_restock_notify')->result_array();


		/*$size_quantity = json_decode($post_data['size_quantity']);
		if(is_array($size_quantity))
		{
			foreach ($size_quantity as $key=>$value){
				
				if($value->size == $size)
				{
					$size_quantity[$key]->quantity = $quantity_restock;
				}
			}			
		}
		else
		{				
			foreach ($size_quantity as &$value){

				if($value->size == $size)
				{						
					$value->quantity = $quantity_restock;
				}
			}
		}
		$data['size_quantity'] = json_encode($size_quantity);
		*/


	    $this->db->where(array('id'=>$post_data['id']));
		$success = $this->db->update('tbl_restock_notify',array('is_restock'=>1, 'modified_date' => date('Y-m-d H:i:s'), 'restock_button' => date('Y-m-d H:i:s')));
		/*$this->db->where('id',$product_id);
		$success = $this->db->update('tbl_products',$data);*/

		if( $success ) {
			$this->send_email_notification($email_list,null, $product_id, $size);
			echo json_encode(array('success'=>TRUE));
		}
		
		exit;

	}

	public function restockNotify_time() {
		$post = $this->input->post();

		//generating email list
		$this->db->where(array('id'=>$post['id']));
		$email_list = $this->db->select('tbl_restock_notify.email')->get('tbl_restock_notify')->result_array();


		//updating currently selected useres
		$this->db->where(array('id'=>$post['id']));
		$success = $this->db->update('tbl_restock_notify',array('is_restock'=>1, 'modified_date' => date('Y-m-d H:i:s'), 'by_time_button' => date('Y-m-d H:i:s')));

		if( $success ) {
			$this->send_email_notification($email_list, $post['restock_by'], $post['product_id'], $post['size']);
			echo json_encode(array('success'=>TRUE));
		}
		exit;

	}

	function manual_sent() {
		$id = $this->input->post('id');


		$this->db->where(array('id'=>$id));
		$this->db->update('tbl_restock_notify',array('is_restock'=>1, 'modified_date' => date('Y-m-d H:i:s'), 'sent_button' => date('Y-m-d H:i:s')));

		echo json_encode(array('success'=>true));
		exit;
		
	}

	function delete_item() {
		$id = $this->input->post('id');

		$this->db->delete('tbl_restock_notify',array('id'=>$id));

		echo json_encode(array('success'=>true));
		exit;
	}

	function reserve_item() {
		$id = $this->input->post('id');
		$i = $this->input->post('i');

		$this->db->update('tbl_restock_notify',array('action'=>$i, 'modified_date' => date('Y-m-d H:i:s')), array('id'=>$id));

		echo json_encode(array('success'=>true));

	}

}