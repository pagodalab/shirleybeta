<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<table id="product-table" data-options="pagination:true,title:'Products',pagesize:'10', rownumbers:true,collapsible:false,fitColumns:true">
	<thead>
		<th field="checkbox" checkbox="true"></th>
		<th field="name_en" sortable="true" width="31%">Product Name</th>
		<th field="email" sortable="true">Email</th>
		<th field="size_code" sortable="true" align="center">Size</th>
		<th field="created_date" sortable="true" width="13%" align="center">Request Date</th>
		<th field="action" formatter="getActions">Action</th>
		<th field="acthistory" formatter="getActHistory">Act. History</th>
	</thead>
</table>
<div id="dlg" class="easyui-dialog" title="Restock" data-options="iconCls:'icon-save',closed: true" style="width:400px;height:200px;padding:10px">
	<form id="form-restock_notify" method="post">
		<table>			
			<tr>
				<td width="34%" ><label><?php echo "Name"?>:</label></td>
				<td width="66%"><input name="name_en" id="name_en" class="easyui-text" readonly="true"></td>
			</tr>
			<tr>
				<td width="34%" ><label><?php echo "Size id"?>:</label></td>
				<td width="66%"><input name="size_code" id="size_code" class="easyui-text" readonly="true"></td>
				<input type="hidden" name="size" id="size" >
			</tr>
			<tr hidden>
				<td width="34%" ><label><?php echo "Quantity"?>:</label></td>
				<td width="66%"><input name="quantity" id="quantity" class="easyui-text"></td>
			</tr>
			<input type="hidden" name="size_quantity" id="size_quantity">
			<input type="hidden" name="product_id" id="product_id">
			<input type="hidden" name="email" id="email">
			<input type="hidden" name="id" id="id">
		</table>
		<button type="button" id="restock-submit" class="btn btn-primary">Submit</button>
	</form>
</div>
<div id="restockNotify_time" class="easyui-dialog" title="Restock Notify By Time" data-options="iconCls:'icon-save',closed: true" style="width:400px;height:200px;padding:10px">
	<form id="form-restockNotify_time" method="post">
		<table>			
			<tr>
				<td width="34%" ><label><?php echo "Name"?>:</label></td>
				<td width="66%"><input type="text" name="name_en" id="name_en" class="easyui-text" readonly="true"></td>
			</tr>
			<tr>
				<td width="34%" ><label><?php echo "Size"?>:</label></td>
				<td width="66%"> <input type="text" name="size_code" id="size_code" class="easyui-text" readonly="true"> </td>
				<input type="hidden" name="size" id="size" >
			</tr>
			<tr>
				<td width="34%" ><label><?php echo "Restock By"?>:</label></td>
				<td width="66%"><input type="text" name="restock_by" id="restock_by" class="easyui-datebox"></td>
			</tr>
			<input type="hidden" name="size_quantity" id="size_quantity">
			<input type="hidden" name="product_id" id="product_id">
			<input type="hidden" name="email" id="email">
			<input type="hidden" name="id" id="id">
		</table>
		<button type="button" id="restockNotify_time-submit" class="btn btn-primary">Submit</button>
	</form>
</div>



<script language="javascript" type="text/javascript">

	$(function(){	
		$('#product-table').datagrid({
			url:'<?=site_url('restock_notify/admin/json')?>',
			height:'auto',
			width:'auto',
			rowStyler: function(index,row){
				if (row.is_restock == 1){
					return 'background-color:mistyrose;color:black';
				}
			}
		});
	});
	

	function getActions(value,row,index)
	{
		var admin_id = <?php echo $this->_user_id; ?>;
		var del = '';
		var r = '';
		var action1 = '';
		var action2 = '';


		if(row.restock_button == '0000-00-00 00:00:00') {
			a = '<a href="javascript:void(0)" id="restock_button" class="btn btn-xs btn-link" onclick="opendlg('+index+')" title="Restock"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
		} else {
			a = '<a href="#" class="btn btn-xs btn-link"><i class="fa fa-envelope-open" aria-hidden="true" disabled="disabled"></i></a>';
		}

		if(row.by_time_button == '0000-00-00 00:00:00'){
			b = '<a href="javascript:void(0)" id="restock_notify_button" class="btn btn-xs btn-link" onclick="openRestockNotify('+index+')" title="Notify by Time"><i class="fa fa-calendar" aria-hidden="true"></i></a>';
		}
		else {
			b = '<a href="#" class="btn btn-xs btn-link"><i class="fa fa-calendar-check-o" disabled="disabled" aria-hidden="true"></i></a>';

			if(row.action == 1 ){
				action1 = 'active btn-info';
			} else if(row.action == 2 ) {
				action2 = 'active btn-info';
			}
			r = '<div class="btn-group btn-group-xs" role="group"><button onclick="reserve_item( 1, '+index+')" class="btn '+action1+'" title="Reserve">R</button>';
			r += '<button class="btn '+action2+'" onclick="reserve_item( 2, '+index+')" title="In Stock">S</button></div>';
		}

		if(row.sent_button == '0000-00-00 00:00:00') {
			c = '<a href="javascript:void(0)" id="manual_sent" class="btn btn-xs btn-link" onclick="manual_sent('+index+')" title="Sent"><i class="fa fa-square-o" aria-hidden="true"></i></a>';
		} else {
			c = '<a href="#" class="btn btn-xs btn-link"><i class="fa fa-check-square-o" aria-hidden="true" disabled="disabled"></i></a>';
			r = '';
		}

		if(admin_id == 1 || admin_id == 3) {
			del += '<a href="javascript:void(0)" id="delete_item" class="btn btn-xs btn-link btn-danger" onclick="delete_item('+index+')" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
		}

		return a + b + c + del + r;
	}
	
	function getActHistory(value,row,index)
	{
		if(row.is_restock == 0){
			return "";			
		}
		else {
			return row.modified_date;
		}
	}

	function opendlg(index){		
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$('#form-restock_notify').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').dialog('open').window('setTitle','<?php  echo "Restock Notify";?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	$('#restock-submit').click(function(){
		var value = $('#form-restock_notify').serialize();
		console.log(value);
		$.post('<?php echo site_url("restock_notify/admin/update_size_quantity")?>',value,function(data){
		    if(data.success)
		    {
		     console.log(data.success); 
		     $('#dlg').dialog('close');
			$('#product-table').datagrid('reload');
		    }
		
		},'json');
	});

	function openRestockNotify(index){		
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$('#form-restockNotify_time').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#restockNotify_time').dialog('open').window('setTitle','<?php  echo "Restock Notify Time";?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	function manual_sent(index){		
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$.messager.confirm('Confirm','Confirm to update user as sent?',function(r){
				if (r){
					$.post('<?php echo site_url("restock_notify/admin/manual_sent")?>',{id:row.id},function(result) {
						if(result.success) {
							$('#product-table').datagrid('reload');
						}

					},'json');
				}
			});
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	$('#restockNotify_time-submit').click(function(){
		var restockvalue = $('#restock_by').datebox('getValue');
		if(restockvalue == '') {
			$('#restock_by').focus();
			return;
		}
		var value = $('#form-restockNotify_time').serialize();
		$.post('<?php echo site_url("restock_notify/admin/restockNotify_time")?>',value,function(data){
			$('#restockNotify_time').dialog('close');
			$('#product-table').datagrid('reload');
		},'JSON');
	});
	
	function delete_item(index) {
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$.messager.confirm('Confirm','Confirm Delete?',function(r){
				if (r){
					$.post('<?php echo site_url("restock_notify/admin/delete_item")?>',{id: row.id},function(result) {
						if(result.success) {
							$('#product-table').datagrid('reload');
						}

					},'json');
				}
			});
		}
	}
	
	function reserve_item(i, index) {
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$.post('<?php echo site_url("restock_notify/admin/reserve_item")?>',{i:i, id: row.id},function(result) {
				if(result.success) {
					$('#product-table').datagrid('reload');
				}

			},'json');
		}
	}
</script>
`