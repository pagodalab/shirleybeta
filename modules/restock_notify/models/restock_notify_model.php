<?php
class Restock_notify_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('RESTOCK_NOTIFY'=>$this->prefix.'restock_notify','PRODUCTS'=>$this->prefix.'products','SIZES'=>$this->prefix.'sizes');
		$this->_JOINS=array('PRODUCTS'=>array('join_type'=>'LEFT','join_field'=>'products.id=restock_notify.product_id',
                                           'select'=>'products.name_en,products.size_quantity','alias'=>'products'),
                                           'SIZES'=>array('join_type'=>'LEFT','join_field'=>'sizes.size_id=restock_notify.size',
                                           'select'=>'sizes.size_code','alias'=>'sizes'),
                           
                            );        
    }
    
    public function getProducts($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='restock_notify.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['RESTOCK_NOTIFY']. ' restock_notify');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['RESTOCK_NOTIFY'].' restock_notify');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}