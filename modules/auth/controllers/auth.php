<?php

class Auth extends Front_Controller {
	
	public function __construct()
	{
		parent::__construct();

	}
	
	public function index()
	{
		$this->login();
	}
	public function login($ajax = false)
	{
		//find out if they're already logged in, if they are redirect them to the my account page
		$redirect	= $this->customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default, if they are not logging in
		if ($redirect)
		{
			redirect('account');
		}
		
		if($this->input->get('redirect_url'))
		{
			$this->session->set_userdata('redirect_url',$this->input->get('redirect_url'));
		}
		
		$data['page_title']	= 'Login';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');
		$submitted 		= $this->input->post('submitted');
		if ($submitted)
		{
			$email		= $this->input->post('email');
			$password	= $this->input->post('password');
			$remember   = $this->input->post('remember');
			$redirect	= $this->input->post('redirect');
			$login		= $this->customer_model->login($email, $password, $remember);
			if ($login)
			{
				if ($redirect == '')
				{
					//if there is not a redirect link, send them to the my account page
					$redirect = 'account';
				}
				
				//to login via ajax
				if($ajax)
				{
					die(json_encode(array('result'=>true)));
				}
				else
				{
					if($this->session->userdata('redirect_url')){
						$redirect=$this->session->userdata('redirect_url');
						$this->session->unset_userdata('redirect_url');
						redirect($redirect);
					}
					else
						redirect($redirect);
				}
				
			}
			else
			{
				//this adds the redirect back to flash data if they provide an incorrect credentials
				
				
				//to login via ajax
				if($ajax)
				{
					die(json_encode(array('result'=>false)));
				}
				else
				{
					$this->session->set_flashdata('redirect', $redirect);
					$this->session->set_flashdata('error', lang('login_failed'));
					
					redirect('auth/login');
				}
			}
		}
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->helper('directory');
	
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
		//$data['banners']	= $this->banner_model->get_banners();
		//$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->category_model->get_categories_tiered(0);
			
		$this->view('auth/login', $data);		
	}
	function logout()
	{
		$this->customer_model->logout();
		redirect('auth/login');
	}
	
	function register()
	{
	
		$redirect	= $this->customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default
		if ($redirect)
		{
			redirect('account');
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		/*
		we're going to set this up early.
		we can set a redirect on this, if a customer is checking out, they need an account.
		this will allow them to register and then complete their checkout, or it will allow them
		to register at anytime and by default, redirect them to the homepage.
		*/
		$data['redirect']	= $this->session->flashdata('redirect');
		
		$data['page_title']	= lang('account_registration');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		//default values are empty if the customer is new

		$data['company']	= '';
		$data['firstname']	= '';
		$data['lastname']	= '';
		$data['email']		= '';
		$data['phone']		= '';
		$data['address1']	= '';
		$data['address2']	= '';
		$data['city']		= '';
		$data['state']		= '';
		$data['zip']		= '';



		$this->form_validation->set_rules('company', 'lang:address_company', 'trim');
		$this->form_validation->set_rules('firstname', 'lang:address_firstname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:address_email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'lang:address_phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]');
		$this->form_validation->set_rules('confirm', 'lang:confirm_password', 'required|matches[password]');
		$this->form_validation->set_rules('email_subscribe', 'lang:account_newsletter_subscribe', 'trim|numeric|max_length[1]');

		
		if ($this->form_validation->run() == FALSE)
		{
			//if they have submitted the form already and it has returned with errors, reset the redirect
			if ($this->input->post('submitted'))
			{
				$data['redirect']	= $this->input->post('redirect');				
			}
			
			// load other page content 
			//$this->load->model('banner_model');
			$this->load->helper('directory');
		
			$data['categories']	= $this->category_model->get_categories_tiered(0);
			
			$data['error'] = validation_errors();
			
			$this->view('auth/register', $data);
		}
		else
		{
			
			
			$save['id']		= false;
			
			$save['firstname']			= $this->input->post('firstname');
			$save['lastname']			= $this->input->post('lastname');
			$save['email']				= $this->input->post('email');
			$save['phone']				= $this->input->post('phone');
			$save['company']			= $this->input->post('company');
			$save['active']				= $this->config->item('new_customer_status');
			$save['email_subscribe']	= intval((bool)$this->input->post('email_subscribe'));
			
			$save['password']			= sha1($this->input->post('password'));
			$save['raw_password']			= $this->input->post('password');
			
			$redirect					= $this->input->post('redirect');
			
			//if we don't have a value for redirect
			if ($redirect == '')
			{
				$redirect = 'account';
			}
			
			// save the customer info and get their new id
			$id = $this->customer_model->save($save);

			$this->send_email($save);			
			//lets automatically log them in
			$this->customer_model->login($save['email'], $this->input->post('confirm'));
			
			//we're just going to make this secure regardless, because we don't know if they are
			//wanting to redirect to an insecure location, if it needs to be secured then we can use the secure redirect in the controller
			//to redirect them, if there is no redirect, the it should redirect to the homepage.
			redirect($redirect);
		}
	}
	
	function check_email($str)
	{
		if(!empty($this->customer['id']))
		{
			$email = $this->customer_model->check_email($str, $this->customer['id']);
		}
		else
		{
			$email = $this->customer_model->check_email($str);
		}
		
        if ($email)
       	{
			$this->form_validation->set_message('check_email', lang('error_email'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function forgot_password()
	{
		$data['page_title']	= lang('forgot_password');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$submitted = $this->input->post('submitted');
		//echo "<pre>"; print_r($_POST); //exit;
		if($submitted)
		{
			$this->load->helper('string');
			$email = $this->input->post('email');
			//echo "email ".$this->config->item('email'); echo "name ".$this->config->item('company_name');
			//echo "<pre>"; print_r($email); //exit;
			//$reset = $this->customer_model->reset_password($email);
			$this->load->library('encrypt');
			$customer = $this->customer_model->get_customer_by_email($email);
			if ($customer)
			{
				$this->load->helper('string');
				$this->load->library('email');
				
				$new_password       = random_string('alnum', 8);
				$customer['password']   = sha1($new_password);
				$customer['raw_password']   = $new_password;
				
				//echo "<pre>"; print_r($customer);
				$this->customer_model->save($customer);
				
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				//$config['protocol'] = 'smtp';
				//$config['smtp_host'] = 'smtp.vianet.com.np';
				$config['mailtype'] = 'html';
				
				$this->email->initialize($config);

				$this->email->from($this->config->item('email'), $this->config->item('site_name'));
				$this->email->to($email);
				$this->email->subject($this->config->item('site_name').': Password Reset');
				$this->email->message('Your password has been reset to <strong>'. $new_password .'</strong>.');
				$this->email->send();
				//$this->email->print_debugger();
			
			}
			$reset = TRUE;
			if ($reset)
			{						
				$this->session->set_flashdata('message', lang('message_new_password'));
			}
			else
			{
				$this->session->set_flashdata('error', lang('error_no_account_record'));
			}
			redirect('auth/forgot_password');
		}
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->helper('directory');
	
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
		//$data['banners']	= $this->banner_model->get_banners();
		//$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->category_model->get_categories_tiered();
		
		
		$this->view('auth/forgot_password', $data);
	}
	
	private function send_email($user)
	{
			/* send an email */
			// get the email template
			$res = $this->db->where('id', '6')->get('tbl_canned_messages');
			$row = $res->row_array();
			
			// set replacement values for subject & body
			
			// {customer_name}
			$row['subject'] = str_replace('{customer_name}', $user['firstname'].' '. $user['lastname'], $row['subject']);
			$row['content'] = str_replace('{customer_name}', $user['firstname'].' '. $user['lastname'], $row['content']);
			
			// {url}
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
			
			// {site_name}
			$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
			$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
			
			$this->load->library('email');
			
			$config['mailtype'] = 'html';
			//$config['protocol'] = 'smtp';
			//$config['smtp_host'] = 'smtp.ntc.net.np';
			
			$this->email->initialize($config);
	
			$this->email->from($this->config->item('email'), $this->config->item('company_name'));
			$this->email->to($user['email']);
			$this->email->bcc($this->config->item('email'));
			$this->email->subject($row['subject']);
			$this->email->message(html_entity_decode($row['content']));
			
			$this->email->send();
			
			$this->session->set_flashdata('message', sprintf( lang('registration_thanks'), $user['firstname'] ) );
		
	}
	
	function facebook()
	{
		$redirect	= $this->customer_model->is_logged_in(false, false);
		
		//if they are logged in, we send them back to the my_account by default
		if ($redirect)
		{
			
			redirect('account');
			
		}
		
		if($this->input->get('redirect_url'))
		{
			$redirect = $this->input->get('redirect_url');
			$this->session->set_userdata('redirect_url',$this->input->get('redirect_url'));
		}
			
		try{
			if($this->facebook->getUser()){
				$this->facebookUser=$this->facebook->api('/me');
				$this->addFacebookUser($redirect);
			}
			
		}catch(Exception $e){
			$this->facebookUser=NULL;
			
		}		
	}

	private function addFacebookUser($redirect)
	{
		
			//echo '<pre>';
			$facebook=$this->facebookUser;

			$customer=$this->customer_model->getCustomers(array('email'=>$facebook['email']));
			$password='';
			if($customer->num_rows()==0)
			{
				$save['id']		= false;
				
				$save['firstname']=$facebook['first_name'];
				$save['lastname']=$facebook['last_name'];
				$save['email']=$facebook['email'];
				$save['fb_id']=$facebook['id'];
				$save['active']				= '1';
				$save['email_subscribe']	= '1';
				$save['raw_password']			= uniqid();
				$save['password']			= sha1($save['raw_password']);
				$save['join_date']			= date('Y-m-d H:i:s');
				$id = $this->customer_model->save($save);
				$this->_greet_fb_user();
				$password=$save['raw_password'];
				$this->send_email($save);
						
			}
			else{
				$customer=$customer->row_array();
				$password=$customer['raw_password'];
			}
			
			$this->customer_model->login($facebook['email'],$password);
			
			//echo "<pre>"; print_r($this->go_cart->customer('is_profile_complete'));
			
		
			if($this->go_cart->customer('is_profile_complete') == 0)
			{
				redirect('account/edit');
			}
			else if($redirect)
			{
				$this->session->unset_userdata('redirect_url');
				redirect($redirect);	
			}
			else
			{
				redirect('account');
			}
		
			//redirect(site_url());

	}
	
	private function _greet_fb_user()
	{
		
		//$access_token=$this->facebook->getAccessToken();
		
		if(has_permission($this->facebook,'publish_stream'))
		{
			
			$msg="I have started using thamel.com";
			$picture_url=base_url().'assets/img/logo.png';
			$params = array(
				   'message' => $msg,
				   //'picture' => $picture_url,
				   'link'=> 'http://thamelmall.com?ref=welcome',//site_url('?type=welcome'),
				   'access_token'=>$this->facebook->getAccessToken()
				   //'description'=>'Shop with Nepal\'Biggest online store'
				);
				
			$this->facebook->api('me/feed','POST',$params);
		}
	}

	function check_user_email()
	{
		$email = '';
		$bill_email = $this->input->post('bill_email');
		if($bill_email)
		{
			$email = $this->customer_model->check_email($bill_email);
		}
		
        if($email)
       	{
			echo json_encode(array('success'=>FALSE));
		}
		else
		{
			echo json_encode(array('success'=>TRUE));
		}
	}
}