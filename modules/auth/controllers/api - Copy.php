<?php

class Api extends API_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('Authenticate');
		
	}
	
	public function admin_get()
	{
		//$admins = $this->authenticate->get_admin_list();
		//$this->response(array('admins'=>$admins),200);
	}
	
	public function staff_login_post()
	{
		$username = $this->post('username'); //for post method
		$password = $this->post('password');
		$access = $this->authenticate->login_staff($username,$password);
		if($access == FALSE)
		{
			$this->response(array('success'=>FALSE,'access'=>array($access)),200);
		}
		else
		{
    		$this->response(array('success'=>TRUE,'access'=>array($access)),200);
		}

	}


}