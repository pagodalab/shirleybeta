<?php

class Api extends API_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->library('Authenticate');
		
	}
	
	public function admin_get()
	{
		//$admins = $this->authenticate->get_admin_list();
		//$this->response(array('admins'=>$admins),200);
	}
	
	public function staff_login_post()
	{
		$username = $this->post('username'); //for post method
		$password = $this->post('password');
		$latitude = $this->post('latitude');
		$longitude = $this->post('longitude');
		$access = $this->authenticate->login_staff($username,$password,NULL,$latitude,$longitude);
		if($access == FALSE)
		{
			$this->response(array('success'=>FALSE,'access'=>array($access)),200);
		}
		else
		{
    		$this->response(array('success'=>TRUE,'access'=>array($access)),200);
		}

	}


}