<?php
class Styles_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('STYLES'=>$this->prefix.'styles');
		// $this->_JOINS=array('CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=share_list.customer_id',
  //                                          'select'=>"concat(customers.firstname,' ',customers.lastname)as customer",'alias'=>'customers'),						
  //                           );        
    }
	
    public function getStyles($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='styles.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['STYLES']. ' styles');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):'';//$this->db->order_by('products.name', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['STYLES'].' styles');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	
}