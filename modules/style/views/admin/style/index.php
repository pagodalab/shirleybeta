<table id="event-table" data-options="pagination:true,title:'Styles',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="style_name_en" sortable="true" width="80">Name English</th>
    <th field="style_name_da" sortable="true" width="100">Name Danish</th>
    <th field="status" sortable="true" width="100" formatter="formatStatus">Status</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="javascript:void(0)" onclick="create()" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Styles</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <?php /*?><select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a><?php */?>
    </p>

</div> 

<!--for create and edit event form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
         <form id="form-event" method="post" >
          <div id="tt" class="easyui-tabs" style="width:545px;height:auto;">
               <div title="Insert" style="padding:20px;">
               	<table>
                         <tr>
                              <td width="34%" ><label>Style Name:</label></td>
                              <td width="66%"><input type="text" name="style_name_en" id="style_name_en" class="easyui-validatebox" required="true"></td>
                         </tr>
                         <tr>
                              <td width="34%" ><label>Style Name Danish:</label></td>
                              <td width="66%"><input type="text" name="style_name_da" id="style_name_da" class="easyui-validatebox" required="true"></td>
                         </tr>
                       <tr>
	                        <td width="34%" ><label>Status : </label></td>
	                        <td width="66%">
	                           <input type="radio" name="status" value="1"> Yes
	                           <input type="radio" name="status" value="0"> No
	                        </td>
                 		</tr>
                        
                        
                              <input type="hidden" name="id" id="id"/>
                    </table>
               </div>
               
          </div>
   		</form>
	<div id="dlg-buttons">
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-ok" onClick="save()"><span><i class="icon-ok-sign icon-inverse"></i></span> Save</a>
		<a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><span><i class="icon-remove-sign icon-inverse"></i></span> Cancel</a>
	</div>    
</div>
<!--div ends-->     

<script language="javascript" type="text/javascript">
$(function(){
	
		$('#event-table').datagrid({
			url:'<?=site_url('style/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	
	});
	function create(){
		//Create code here
		$('#categories').html('');
		$('#form-event').form('clear');
		
		$('#dlg').window('open').window('setTitle','Insert Style');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		$('#categories').html('');
		var row = $('#event-table').datagrid('getRows')[index];
		if (row){
			$('#form-event').form('load',row);
			$.post('<?php echo site_url('event/admin/getCategories')?>',{id:row.event_id},function(data){
					$('#categories').html(data);
				});
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_event')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('edit_event')?>"><span><i class="icon-pencil icon-inverse"></i></span></a>';
		var d = '<a href="#" onclick="removeOccasion('+index+')" class="tooltip-top btn btn-small" title="<?php  echo lang('delete_event')?>"><span><i class="icon-trash icon-inverse"></i></span> </a>';
		return e+d;
	}
	
	function save()
	{
		$('#form-event').form('submit',{
			url: '<?php  echo site_url('style/admin/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-event').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#event-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	function upload(index)
	{
		var row = $('#event-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('product_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {product_id: row.product_id};
		}
		$('#form-upload').form('clear');
	}
		
	function removeOccasion(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#event-table').datagrid('getRows')[index];
				//console.log(row); return false;
				$.post('<?=site_url('event/admin/delete_json')?>', {id:[row.event_id]}, function(data){
					$.messager.show({title: '<?php  echo lang('success')?>',msg: data.msg});
					//$('#event-table').datagrid('deleteRow', index);
					$('#event-table').datagrid('reload');
				},'json');

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#event-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?=site_url('style/admin/delete_json')?>',{id:selected},function(data){
						$('#event-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function checkSubs(id)
	{
		var status = false;
		var disabled = true;
		
		if($('#category'+id).prop('checked'))
		{
			status = true;
			disabled = false;
			//console.log(status); 
			//return false;
			//$('.subcategory'+id).prop('checked',status);
		}
		$('.subcategory'+id).prop('checked',status);
		$('.suborder'+id).prop('disabled',disabled);
		$('.order'+id).prop('disabled',disabled);
	}
	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return 'Yes';
		}
		else
		{
			return 'No';
		}
	}
</script>
