<?php

class Admin extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->authenticate->check_access('Admin', true);
		$this->load->helper('form');
		// $this->lang->load('STYLE');
        $this->load->model('style/styles_model');

	}

	public function index()
	{
		// Display Page
		$data['page_title'] = 'Styles';
		// $data['categories'] = $this->category_model->getCategories(array('parent_id'=>0))->result_array();
		$data['module'] = 'style';
		$this->view($this->config->item('template_admin') . "admin/style/index",$data);

	}

	public function json()
	{
		
		//$this->_get_search_param();	
		//$this->styles_model->joins = array('BRANDS','CATEGORY_PRODUCT');
		$total=$this->styles_model->count();
		paging('id desc');
		//$this->_get_search_param();
		$rows=$this->styles_model->getStyles()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	/*public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
		
		}  
	}
	
	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}*/
	
	public function delete_json()
	{
		$id = $this->input->post('id');
		foreach ($id as $row) {
			$success = $this->styles_model->delete('STYLES',array('id'=>$row));
		}
		echo json_encode(array('success' => $success));
	}
	public function save()
	{
		$data = $this->_get_posted_data();
		if($data['id'])
		{
			
			$success = $this->styles_model->update('STYLES',$data,array('id'=>$data['id']));
		}
		else
		{
			$success = $this->styles_model->insert('STYLES',$data);
			// $new_id = $this->db->insert_id();
			
		}
		
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		echo json_encode(array('msg'=>$msg,'success'=>$success));		
	}
	
	private function _get_posted_data()
	{
		$data = array();
		$data['id'] = $this->input->post('id');
		$data['style_name_en'] = $this->input->post('style_name_en');
		$data['style_name_da'] = $this->input->post('style_name_da');
		$data['status'] = $this->input->post('status');
		
		return $data;
	}
	
	
	

}
