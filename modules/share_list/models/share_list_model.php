<?php
class Share_list_model extends  MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('SHARE_LIST'=>$this->prefix.'share_lists','SHARE_LIST_PRODUCT'=>$this->prefix.'sharelist_products','CUSTOMER'=>$this->prefix.'customers','PRODUCT'=>$this->prefix.'products');
		$this->_JOINS=array('SHARE_LIST_PRODUCT'=>array('join_type'=>'INNER','join_field'=>'share_list.share_list_id=sharelist_products.share_list_id',
                                           'select'=>"sharelist_products.product_id",'alias'=>'sharelist_products'),
						'CUSTOMER'=>array('join_type'=>'INNER','join_field'=>'customers.id=share_list.customer_id',
                                           'select'=>"concat(customers.firstname,' ',customers.lastname)as customer",'alias'=>'customers'),
						'PRODUCT'=>array('join_type'=>'INNER','join_field'=>'sharelist_products.product_id=products.id',
                                           'select'=>"products.*",'alias'=>'products')
						
                            );        
    }
	
    public function getShareLists($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='share_list.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['SHARE_LIST']. ' share_list');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):'';//$this->db->order_by('products.name', 'ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
	
	 public function countShareLists($where=NULL)
    {
		$this->db->select('count(*) as rows');
        $this->db->from($this->_TABLES['SHARE_LIST'].' share_list');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
        if( ! is_null($where))
		{
			$this->db->where($where);
		}
        $result=$this->db->get();
        if($result->num_rows()>0)
        {
        	$row=$result->row_array();
            return $row['rows'];
        }
        return 0;
    }	
	
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['SHARE_LIST'].' share_list');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

	function save($data)
	{
		/*if($data['customer_id'])
		{
			$result = $this->getShareLists(array('customer_id'=>$data['customer_id']))->row_array();
			if(count($result) > 0)
			{
				$this->update('SHARE_LIST',array('customer_id'=>$data['customer_id']),$data);
				return $result['share_list_id'];
			}
			else
			{*/
				$this->insert('SHARE_LIST',$data);
				return $this->db->insert_id();
			/*}
		}*/
		
	}
	
	function delete_share_list($id)
	{
		// delete product 
		$this->db->where('share_list_id', $id);
		$this->db->delete($this->_TABLES['SHARE_LIST']);

	}
	
	public function getShareListById($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='share_list.share_list_id';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['SHARE_LIST']. ' share_list');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where): NULL;
		//(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('products.name', 'ASC');

		/*if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}*/
		return $this->db->get();
    }

}