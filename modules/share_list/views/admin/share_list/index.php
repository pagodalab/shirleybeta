<div id="search-panel" class="easyui-panel" data-options="title:'Product Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="product-search-form">
<table width="100%">
<tr><td><label><?=lang('product_name')?></label>:</td>
<td><input name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<td><label>Category</label>:</td>
<td><input name="search[category]" id="search_category"/></td>
</tr>
<tr>
<td><label>Brand</label>:</td>
<td><input name="search[brand]" id="search_brand"  class="easyui-combobox"/></td>
<td><label>Added Date</label>:</td>
<td><input type="text" name="date[added_date][from]" id="search_added_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[added_date][to]" id="search_added_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><input type="checkbox" name="search[is_flexible]" id="search_is_flexible"> Is Flexible</label>
</td>
<td><label><input type="checkbox" name="search[is_perishable]" id="search_is_perishable"> Is Perishable</label>
<td><label>Status</label>:</td>
<td>
	<select type="text" name="search[enabled]" id="search_enabled"  class="easyui-combobox"/>
		<option value=""></option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
</td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="product-table" data-options="pagination:true,title:'Products',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <?php /*?><th field="id" sortable="true" width="30">Product Id</th><?php */?>
    <th field="images" sortable="true" width="80" formatter="formatImage">Image</th>
    <th field="name" sortable="true" width="100"><?=lang('product_name')?></th>
    <th field="sku" sortable="true" width="100">Product Code</th>
    <th field="brand_name" width="50">Brand</th>
    <th field="price" sortable="true" width="50"><?=lang('price')?></th>   
    <th field="views" sortable="true" width="30" align="center">Views</th>
    <th field="enabled" sortable="true" width="30" align="center" formatter="formatStatus">Status</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<? echo site_url("product/admin/form")?>" class="tooltip-top btn btn-small" iconCls="icon-add" plain="false"><span><i class="icon-plus"></i></span> Add Product</a>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <select name="enabled" id="enabled" class="easyui-combobox">
    	<option value="">Select Status</option>
        <option value="1">Enabled</option>
        <option value="0">Disabled</option>
    </select>
    <a href="#" class="tooltip-top btn btn-small" iconCls="icon-cancel" plain="false" onclick="changeSelected()"  title="Change Selected Status"><span><i class="icon-trash"></i></span> Change Selected</a>
    </p>

</div> 

<!--for survey detail -->
<div id="detail-dialog" class="easyui-window" style="width:600px;height:500px;padding:10px 20px"
        closed="true" collapsible="true"  buttons="#dlg-buttons">
<div id="product-detail-view"></div>        
</div>        

<!--<div id="dlg_displayDetails" class="easyui-window" title="Product details" style="width:500px;height:auto;padding:10px 20px" closed="true" collapsible="true" buttons="#dlg-buttons">
	<p>Product Name: <span id="det_productname"></span></p>
    <p>Original Price: <span id="det_originalprice"></span></p>
    <p>Sell Price: <span id="det_sellprice"></span></p>
    <p>Expiry Date: <span id="det_expirydate"></span></p>
    <p>Views: <span id="det_views"></span></p>
    <p>Status: <span id="det_status"></span></p>
</div>-->
<script language="javascript" type="text/javascript">

	$(function(){
		$('#search_brand').combobox({url:'<?php echo site_url('brand/admin/combo_json')?>',valueField:'brand_id',textField:'brand_name'});
		$('#search_category').combotree({url:'<?php echo site_url('category/admin/treeview_json')?>'});
			
		$('#clear').click(function(){
			$('#product-search-form').form('clear');
			$('#product-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#product-table').datagrid({
				queryParams:{data:$('#product-search-form').serialize()}
				});
		});		
		$('#product-table').datagrid({
			url:'<?=site_url('product/admin/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
			$('#form-product').form('load',row);
			$('#dlg').window('open').window('setTitle','<?=lang('edit_product')?>');
			}
		});
	
	});
	

	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return "Yes";	
		}
		return "No";
	}
	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('product/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" title="<?=lang('edit_product')?>"><span><i class="icon-pencil"></i></span> </a>';
		
		var d = '<a href="#" onclick="removeProduct('+index+')" class="tooltip-top btn btn-small" title="<?=lang('delete_product')?>"><span><i class="icon-trash"></i></span> </a>';
		
		var p = '<a href="<?php echo site_url('product/admin/form')?>/'+row.id+'/1" class="tooltip-top btn btn-small" title="Copy"><span><i class="icon-copy"></i></span> </a>';
		
		return p+e+d;		
	}

	function upload(index)
	{
		var row = $('#product-table').datagrid('getRows')[index];
		if (row){
			$('#dlg-upload').window('open').window('setTitle','<?=lang('product_image_upload')?>');
			$("#uploader").pluploadQueue().settings.multipart_params = {product_id: row.product_id};
		}
		$('#form-upload').form('clear');
	}
		
	function removeProduct(index)
	{
		$.messager.confirm('Confirm','Are you sure to delete?',function(r){
			if (r){
				var row = $('#product-table').datagrid('getRows')[index];
				//console.log(row); return false;
				$.post('<?=site_url('product/admin/delete_json')?>', {id:[row.id]}, function(){
					<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
					$('#product-table').datagrid('deleteRow', index);
					$('#product-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#product-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to delete?',function(r){
				if(r){				
					$.post('<?=site_url('product/admin/delete_json')?>',{id:selected},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
						$('#product-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}	
	}
	
	function formatImage(value)
	{
		//console.log(value);
		//var img = '<img src="<?php echo base_url()?>uploads/images/small/'+value+'">';
		
		
		return value;
		//return value;
	}


	function changeSelected()
	{
		var status = $('#enabled').combobox('getValue');
		var rows=$('#product-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you Sure to Change?',function(r){
				if(r){				
					$.post('<?=site_url('product/admin/change_status')?>',{id:selected,enabled:status},function(data){
						<?php $this->session->set_flashdata('message', lang('message_saved_product'));?>
						$('#product-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','No Product Selected!');	
		}	
	}
</script>
