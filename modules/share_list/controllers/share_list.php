<?php 
class Share_list extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('share_list/share_list_model');
	}
	
	function index($id)
	{
		$this->view('product/index', $data);
	}
	
	function search()
	{
		$data['page_title']	= 'Search Results';
		$this->product_model->joins=array('CATEGORY_PRODUCT');
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'sequence', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$where = array('enabled'=>1);
		$where = array_merge($this->_get_where(),$where);
		//$this->db->order_by('id desc');
		$this->db->group_by('products.id');
		$this->db->limit(10);
		$data['products'] = $this->product_model->getProducts($where,$sort_by['by'].' ' . $sort_by['sort'])->result_array();
		$data['query'] = $this->input->get('q');
		//echo "<pre> query"; print_r($this->db->last_query());
		//echo "<pre> result"; print_r($data['products']);
		$this->view('product/search', $data);
	}
	
	private function _get_where()
	{
		$where = array();
		if($this->input->get('q'))
		{
			$where["products.name like '%".$this->input->get('q')."%'"] = NULL;//$this->input->get('name');
		}
		
		return $where;
	}

	function add_to_wishlist()
	{
		$json = array();
		$wishlist=$this->go_cart->get_wishlist();
		

		if ($this->input->post('product_id')) {
			$product_id = $this->input->post('product_id');
		} else {
			$product_id = 0;
		}
		$product_info = $this->product_model->get_product($product_id);

		if ($product_info) {
			if (!in_array($this->input->post('product_id'), $wishlist)) {	
				$wishlist[]=$this->input->post('product_id');
				$this->go_cart->set_wishlist($wishlist);
			}
			 
			$json['success']=TRUE;
			$json['total'] = count($this->go_cart->get_wishlist()) ;
			echo json_encode($json);
		}	
	}	
	
	private function set_cookie($id)
	{
		$this->load->helper('cookie');
		$cookie=$this->input->cookie('thamelmall_products');

		if(!$this->input->cookie('thamelmall_products') || !in_array($id,$cookie))
		{
			$length=count($cookie);
			$cookie = array(
				'name'   => 'thamelmall_products['.$length.']',
				'value'  => $id,
				'expire' => time()+60*10,
				'path'   => '/',
			);
			$this->input->set_cookie($cookie);
			return TRUE;
		}
		return FALSE;
	}	
	
	function more($offset=0)
	{		
		$data['new_offset'] = $offset + $this->page_limit;
		$data['products'] = $this->product_model->getProducts(array('enabled'=>1),'id desc',array('limit'=>$this->page_limit,'offset' => $offset))->result_array();
		
		$output['view']=$this->load->view('product/more',$data,TRUE);
		$output['offset']=$data['new_offset'];
		echo json_encode($output);
	}	
	
	public function page($offset=0)
	{
		/*$categories=$this->category_model->get_categories_tiered();
						echo '<pre>';
						print_r($categories);
						exit;*/
		
		$this->load->library('pagination');
		$products = $this->product_model->getProducts(array('enabled'=>1),'id desc',array('limit'=>NULL,'offset' => $offset))->result_array();
		$limit=2;
		$config=$this->pagination_config();
		$config['uri_segment'] = '4';
		$config['base_url'] = site_url('product/page/15/');
		$config['total_rows'] = count($products);
		$config['per_page'] = $limit; 
		//echo "<pre> query"; print_r($this->db->last_query());
		$this->pagination->initialize($config); 
		
		$data['new_offset'] = $offset + $this->page_limit;
		$data['products'] = $this->product_model->getProducts(array('enabled'=>1),'id desc',array('limit'=>$this->page_limit,'offset' => $offset))->result_array();
		
		//$data['orders_pagination'] = $this->pagination->create_links();
		
		//echo "<pre>total rows "; print_r($config['total_rows']);
		//echo "<pre>per Page"; print_r($config['per_page']);*/
		//$output['view']=$this->load->view('product/more',$data,TRUE);
		//$output['offset']=$data['new_offset'];
		
		$this->view('home/page',$data);
	}
}