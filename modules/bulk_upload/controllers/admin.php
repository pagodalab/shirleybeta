<?php 

class Admin extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('bulk_upload/bulk_request_model');
		$this->load->model('product/product_model');
		$this->load->model('location/location_model');
		$this->load->model('brand/brand_model');
	}
	
	public function index()
	{	
		$user = $this->session->userdata('admin');
		$user_id = (!($user['id']))?$this->go_cart->customer('id'):$user['id'] ;
		$data['header'] = 'Bulk Uploads';
		$data['bulk_requests'] = $this->bulk_request_model->getBulkRequests(array('user_id'=>$user_id),'request_id desc')->result_array();
		$data['page_title'] = 'Bulk Uploads';
		$this->view($this->config->item('admin_folder').'/bulk_upload/index', $data);
	}	
	
	public function upload()
	{
		$user = $this->session->userdata('admin');
		
		$file_path = "uploads/bulk_uploads";
		@mkdir($file_path);
		$config['upload_path'] 	= $file_path;
		$config['allowed_types']	= '*';
		$config['max_size']		= 100000;
		$this->load->library('upload', $config);

		$uploaded = $this->upload->do_upload('file_name');

		$file = $this->upload->data();
		$data['file_name'] = $file['file_name'];
		$data['request_date'] = date('Y-m-d H:i:s');
		$data['status'] = 'Pending';
		$data['user_id'] = (!($user['id']))?$this->go_cart->customer('id'):$user['id'];
		$success = $this->bulk_request_model->insert('BULK_REQUEST',$data);
		
		if($success)
		{
			$this->session->set_flashdata('message','File Uploaded successfully');
		}
		else
		{
			$this->session->set_flashdata('error','Error occured while uploading file!');
		}
		redirect(site_url('bulk_upload/admin'));
	}
	
	public function importer()
	{
		$this->load->library('PHPExcel');
		
		$id = $this->input->post('id');
		$selected = $this->bulk_request_model->getBulkRequests(array('request_id'=>$id))->row_array();
		//echo "<pre>"; print_r($selected['file_name']); 
		$file = 'uploads/bulk_uploads/'.$selected['file_name'];
		
		//echo "<pre>"; print_r($file); exit;
		
		//  Include PHPExcel_IOFactory
		//include 'PHPExcel/IOFactory.php';
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		
		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		date_default_timezone_set('Asia/Kathmandu');
		
		/** PHPExcel_IOFactory */
		require_once (APPPATH.'libraries/PHPExcel/IOFactory.php');
		
		
		if (!file_exists($file)) {
			exit("Please run 05featuredemo.php first." . EOL);
		}
		
		//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($file);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestrow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		$highestcol = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
		$colnum = PHPExcel_Cell::columnIndexFromString($highestcol);
		
		$products = array();

		for($row = 2; $row <=$highestrow; $row++)
		{
			for($col = 0; $col<$colnum; $col++)
			{
				$products['products'][$row-2][$col] = $objWorksheet->getCellByColumnAndRow($col,$row)->getValue();
			}
		}

		$foundProducts = $products['products'];
		
		$this->_insert_products($foundProducts);
		
		$data['imported_date'] = date('Y-m-d H:i:s');
		$data['status'] = 'Imported';
		$success = $this->bulk_request_model->update('BULK_REQUEST',$data, array('request_id'=>$id));
		
		if($success)
		{
			$this->session->set_flashdata('message','Product List Imported Successfully');
			$success = TRUE;
		}
		else
		{
			$this->session->set_flashdata('error','Error Occured While Importing Product List');
			$success = FALSE;
		}
		
		echo json_encode(array('success'=>$success));
		
	}
	
	private function _insert_products($products)
	{
		set_time_limit(0);
		$this->load->helper('text');
		$this->load->model('Routes_model');
		if(is_array($products))
		{
			
			foreach($products as $product)
			{
				$save['id']				= '';
				//$save['sku']				= $this->input->post('sku');
				$save['name']				= $product[0];
				//$save['seo_title']			= $this->input->post('seo_title');
				//$save['meta']				= $this->input->post('meta');
				$save['description']		= $product[1];
				
				$brand = $product[2];
				$row = $this->brand_model->get_brand_by_name($brand);
				if(count($row)>0)
				{
					$save['brand']	= $row['brand_name'];
				}
				else
				{
					$save['brand']		= '';	
				}
				
				$save['quantity']			= $product[3];
				$save['price']				= $product[4];
				$save['saleprice']			= $product[5];
				$save['weight']			= $product[6];
				$country = $product[7];
				$result = $this->location_model->get_country_by_name($country);
				if(count($result)>0)
				{
					$save['country_code']	= $result->iso_code_2;
				}
				else
				{
					$save['country_code']		= '';	
				}
				$save['is_perishable']		= (($product[8] == 'yes'))?1:0;
				$save['is_flexible']		= (($product[9] == 'yes'))?1:0;
				$save['is_cake']			= (($product[10] == 'yes'))?1:0;
				$save['delivery_message']	= (($product[11] == 'yes'))?1:0;
				$save['delivery_date']		= (($product[12] == 'yes'))?1:0;
				$save['taxable']			= (($product[13] == 'yes'))?1:0;
				$save['shippable']			= (($product[14] == 'yes'))?1:0;
				$save['enabled']			= (($product[15] == 'yes'))?1:0;
				$save['track_stock']		= (($product[16] == 'yes'))?1:0;
				$save['fixed_quantity']		= (($product[17] == 'yes'))?1:0;
				
				if(!empty($product[18]))
				{
					$save['overwrite_price']		= $product[18];
					$save['is_overwrite_price']	= 1;
				}
				else
				{
					$save['overwrite_price']		= '';
					$save['is_overwrite_price']	= 0;
				}
				
				$save['is_width']			= (($product[19] == 'yes'))?1:0;
				$save['sku']				= (!($product[20]))?'':$product[20];
				
				$slug = $product[0];
				$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
				//validate the slug
				$slug	= $this->Routes_model->validate_slug($slug);
				//echo "<pre> Slug "; print_r($slug);
				$route_id = '';
				//echo "<pre> Route NULL "; print_r($route_id);

				$route['slug']	= $slug;	
				$route_id	= $this->Routes_model->save($route);
				//echo "<pre> Route"; print_r($route_id);
				
				$save['slug']				= $slug;
				$save['route_id']			= $route_id;
				//$post_images				= $this->input->post('images');
				$save['excerpt']			= $product[0];
				$save['added_date'] 		= date('Y-m-d h:i:s');

				//echo "<pre>"; print_r($save);
				// save product 
				$product_id	= $this->product_model->save($save);
				//echo "<pre>"; print_r($product_id);
				//echo "<pre> Routes 2 "; print_r($route_id);
				$routes['id']	= $route_id;
				$routes['slug']	= $slug;
				$routes['route']	= 'product/index/'.$product_id;
				$this->Routes_model->save($routes);
			} 
		}
	}
	
	public function delete()
	{
		//echo "<pre>"; print_r($_POST);
		$requests	= $this->input->post('checked');
		//echo "<pre>"; print_r($requests); exit;
		if($requests)
		{
			foreach($requests as $request)
			{
				$selected = $this->bulk_request_model->getBulkRequests(array('request_id'=>$request))->row_array();
				@unlink('uploads/bulk_uploads/'.$selected['file_name']);
				$success = $this->bulk_request_model->delete('BULK_REQUEST',array('request_id'=>$request));
			}
			
		}
		if($success)
		{
			$this->session->set_flashdata('message', 'File/s Deleted Successfully');
			$success = TRUE;
		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to delete the files');
			$success = FALSE;
		}
		
		echo json_encode(array('success'=>$success));
		//redirect as to change the url
		//redirect(site_url('order/admin'));	
	}

	public function custom_order() {
		$data['header'] = 'Bulk Custom Order Uploads';
		$data['page_title'] = 'Bulk Custom Order Uploads';
		$this->view($this->config->item('admin_folder').'/bulk_upload/custom_order', $data);

	}

	public function upload_custom()
	{
		$user = $this->session->userdata('admin');
		
		$file_path = "uploads/bulk_uploads";
		@mkdir($file_path);
		$config['upload_path'] 	= $file_path;
		$config['allowed_types']	= '*';
		$config['max_size']		= 100000;
		$this->load->library('upload', $config);

		$uploaded = $this->upload->do_upload('file_name');

		$file = $this->upload->data();

		/**
		didnt add to tbl_bulk_request
		**/

		$this->load->library('PHPExcel');
		
		$file = 'uploads/bulk_uploads/'.$file['file_name'];
				
		//  Include PHPExcel_IOFactory
		//include 'PHPExcel/IOFactory.php';
		// error_reporting(E_ALL);
		// ini_set('display_errors', TRUE);
		// ini_set('display_startup_errors', TRUE);
		
		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		// date_default_timezone_set('Asia/Kathmandu');
		
		/** PHPExcel_IOFactory */
		require_once (APPPATH.'libraries/PHPExcel/IOFactory.php');
		
		
		if (!file_exists($file)) {
			exit("Please run 05featuredemo.php first." . EOL);
		}
		
		//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($file);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestrow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		$highestcol = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
		$colnum = PHPExcel_Cell::columnIndexFromString($highestcol);
		
		$products = array();
		$data = array();

		foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) 
		{
			$worksheetTitle = $worksheet->getTitle();
			$highestColumn = $worksheet->getHighestColumn();
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

			// expects same number of row records for all columns
			$highestRow = $worksheet->getHighestRow();

			// $data[$key] = array();

			for($col = 0; $col < $highestColumnIndex; $col++)
			{
				// if you do not expect same number of row records for all columns
				// get highest row index for each column
				$highestRow = $worksheet->getHighestRow();

				for ($row = 1; $row <= $highestRow; $row++)
				{
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					$val = $cell->getValue();
					// do what you want with cell value

					if($col == 0) 
						$data[$key]['style_id'][] = $val;
					if($col == 1) {
						$data[$key]['size_id'][] = $val;
					}
					if($col == 2)
						$data[$key]['color_id'][] = $val;
				}
			}
		}

		$insert = array();

		foreach ($data as $key => $value) {
			foreach ($value['size_id'] as  $size) {
				foreach ($value['color_id'] as $color) {

					if($size === NULL) {
						continue;
					}
					if($color === NULL) {
						continue;
					}

					$insert[] = array(
						'style_id'	=>	$value['style_id'][0],
						'size_id'	=>	$size,
						'color_id'	=>	$color,
						'material_id'=>	1,
					);

				}
			}
		}

		$this->db->insert_batch('custom_order_relation',$insert);

		if($success)
		{
			$this->session->set_flashdata('message','Custom Order Relations Imported Successfully');
		}
		else
		{
			$this->session->set_flashdata('error','Error Occured While Importing Custom Order Relations');
		}

		redirect(site_url('bulk_upload/admin/custom_order'));

	}


}
