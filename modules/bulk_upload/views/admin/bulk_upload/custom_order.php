<style type="text/css">
.pagination {
	margin:0px;
	margin-top:-3px;
}
</style>
<div style="border-bottom:1px solid #f5f5f5;float:left">
	<a href="<?php echo base_url()?>uploads/bulk_uploads/sample.xlsx" target="_blank" class="btn btn-primary">Download Sample</a>

</div>
<div style="border-bottom:1px solid #f5f5f5;float:right">
	<form action="<?php echo site_url('bulk_upload/admin/upload_custom')?>" method="post" enctype="multipart/form-data">
		<input type="file" id="file_name" name="file_name" required="required">
		<button type="submit" class="btn"><span><i class="icon-plus"></i></span>Add New File</button>
	</form>
</div>
<div class="clearfix"></div>
<form method="post" id="request_files-form">
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('#checkall').on('click',function(){
			var status = false;
			if($(this).is(':checked'))
			{
				status = true;
			}
			$('.get_checked').prop('checked',status);
		});
	});



	function importFile(id)
	{
		if(confirm('Are you sure to import the file?')){
			$.post('<?php echo site_url('bulk_upload/admin/importer')?>',{id:id},function(data){
				console.log(data);
				if(data.success)
				{

					window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
				}
				else
				{
					window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
				}
			});

		}
		return false;
	}

</script>
