<style type="text/css">
	.pagination {
		margin:0px;
		margin-top:-3px;
	}
</style>
<div style="border-bottom:1px solid #f5f5f5;float:left">
	 <a href="<?php echo base_url()?>uploads/bulk_uploads/sample.xlsx" target="_blank" class="btn btn-primary">Download Sample</a>
     <button type="button" class="btn btn-danger" onClick="deleteSelected()" style="float:left;margin-left:4px;">
     	Delete Selected
     </button>
    
</div>
<div style="border-bottom:1px solid #f5f5f5;float:right">
	<form action="<?php echo site_url('bulk_upload/admin/upload')?>" method="post" enctype="multipart/form-data">
		<input type="file" id="file_name" name="file_name" required="required"><button type="submit" class="btn"><span><i class="icon-plus"></i></span>Add New File</button>
     </form>
</div>
<div class="clearfix"></div>
<form method="post" id="request_files-form">
     <table class="table table-striped">
         <thead>
               <tr>
                    <th style="width:1%;"><input type="checkbox" id="checkall"></th>
                    <th>File Title</th>
                    <th>Uploaded Date</th>
                    <th>Status</th>
                    <th>Imported Date</th>
                    <th>Actions</th>
              </tr>
          </thead>
     
         <tbody>
          <?php echo (count($bulk_requests) < 1)?'<tr><td style="text-align:center;" colspan="8"> Bulk Uploads is Empty.</td></tr>':''?>
         <?php foreach($bulk_requests as $request): ?>
          <tr>
               <td><input name="request[]" type="checkbox" value="<?php echo $request['request_id']; ?>" class="get_checked"/></td>
               <td><?php echo $request['file_name']; ?></td>
               <td><?php echo $request['request_date'];?></td>
               <td><?php echo $request['status'];?> </td>
               <td><?php echo $request['imported_date'];?></td>
               <td>
                    <?php if($request['status'] == 'Pending'){?>
                    <button type="button" class="btn" onClick="importFile(<?php echo $request['request_id']?>)" style="float:left;margin-left:4px;">
                    	Import
                    </button>
                    <?php }?>
                    <button type="button" class="btn btn-danger" onClick="deleteFile(<?php echo $request['request_id']; ?>)" style="float:left;margin-left:4px;">
                         Delete
                    </button>
               </td>
          </tr>
         <?php endforeach; ?>
         </tbody>
     </table>
</form>

<script type="text/javascript">
$(document).ready(function(){
		$('#checkall').on('click',function(){
			var status = false;
			if($(this).is(':checked'))
			{
				status = true;
			}
			$('.get_checked').prop('checked',status);
		});
	});
	
function deleteSelected()
{
	console.log($('.get_checked:checked').length);
	if($('.get_checked:checked').length > 0)
	{
		if(confirm('Are you sure to delete the files?')){	
			var selected = [];
			$(".get_checked:checked").each(function(){
					selected.push(this.value);
				});
	
				$.post('<?php echo site_url('bulk_upload/admin/delete')?>',{checked:selected},function(data){
					if(data.success == 'true')
					{
						
						window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
					}
					else
					{
						window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
					}
					
				});
		}
			return false;
	}
	else
	{
		alert('Please select at least one to Delete');
		return false;
	}
}


function importFile(id)
{
	if(confirm('Are you sure to import the file?')){
		$.post('<?php echo site_url('bulk_upload/admin/importer')?>',{id:id},function(data){
			console.log(data);
			if(data.success)
			{
				
				window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
			}
			else
			{
				window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
			}
		});
		
	}
	return false;
}

function deleteFile(id)
{
	if(confirm('Are you sure to delete the file?')){
		$.post('<?php echo site_url('bulk_upload/admin/delete')?>',{checked:[id]},function(data){
			if(data.success == 'true')
			{
				
				window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
			}
			else
			{
				window.location.href = '<?php echo site_url('bulk_upload/admin')?>'; 
			}
			
		});
	}
	return false;
	
}

</script>
