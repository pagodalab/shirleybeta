<div id="search-panel" class="easyui-panel" data-options="title:'Customer Search',collapsible:true,collapsed:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="customer-search-form">
<table width="100%">
<tr><td><label>Customer Name</label>:</td>
<td><input name="search[firstname]" id="search_firstname"  class="easyui-validatebox" placeholder="First Name"/> <input name="search[lastname]" id="search_lastname"  class="easyui-validatebox" placeholder="Last Name"/></td>
<td><label>Status</label>:</td>
<td>
	<select name="search[active]" id="search_active"  class="easyui-combobox"/>
		<option value=""></option>
        <option value="1">Active</option>
        <option value="0">Inactive</option>
    </select>
</td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="#" class="tooltip-top btn btn-small" id="search"><span><i class="icon-search"></i></span> <?=lang('search')?></a>  
    <a href="#" class="tooltip-top btn btn-small" id="clear"><span><i class="icon-clear"></i></span> Clear</a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="customer-table" data-options="pagination:true,title:'Customers',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="first_name" sortable="true" width="150" formatter="formatName">Name</th>
    <th field="email" width="50">Email</th>
    <th field="active" sortable="true" width="50" align="center" formatter="formatStatus">Active</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<? echo site_url("customer/admin/form")?>" class="tooltip-top btn btn-small" title="Add Customer"><span><i class="icon-plus"></i></span> Add customer</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()"  title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    <a href="<?php echo site_url('customer/admin/export_xml');?>" class="tooltip-top btn btn-small" title="Customer XML"><span><i class="icon-download"></i></span> <?php echo lang('xml_download');?></a>
	<a href="<?php echo site_url('customer/admin/get_subscriber_list');?>" class="tooltip-top btn btn-small" title="Subscriber List"><span><i class="icon-download"></i></span> <?php echo lang('subscriber_download');?></a>
    </p>

</div> 

<script language="javascript" type="text/javascript">

	$(function(){
			
		$('#clear').click(function(){
			$('#customer-search-form').form('clear');
			$('#customer-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#customer-table').datagrid({
				queryParams:{data:$('#customer-search-form').serialize()}
				});
		});		
		$('#customer-table').datagrid({
			url:'<?=site_url('customer/admin/json')?>',
			height:'auto',
			width:'auto'

		});
	
	});
	

	
	function formatStatus(value)
	{
		if(value == 1)
		{
			return "Yes";	
		}
		return "No";
	}
	
	function formatName(value,row,index)
	{
		var name = '<p>' +row.lastname + ' ' + row.firstname +'<p>';
		return name;
	}
	
	
	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('customer/admin/form')?>/'+row.id+'" class="tooltip-top btn btn-small" iconcls="icon-edit"  title="<?=lang('edit_customer')?>"><span><i class="icon-pencil"></i></span> </a>';
		
		var d = '<a href="<?php echo site_url('customer/admin/delete/')?>/'+row.id+'" onclick="" class="tooltip-top btn btn-small" iconcls="icon-remove"  title="<?=lang('delete_customer')?>"><span><i class="icon-trash"></i></span> </a>';
		
		return e+d;		
	}
	
</script>
