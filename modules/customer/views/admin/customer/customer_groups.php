<table id="group-table" data-options="pagination:true,title:'Customer Groups',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th field="checkbox" checkbox="true"></th>
    <th field="name" sortable="true" width="150">Name</th>
    <th field="discount" width="50">Discount</th>
    <th field="discount_type" width="50">Discount Type</th>
    <th field="action" width="100" formatter="getActions">Action</th>
    </thead>
</table>


<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="<? echo site_url("customer/admin/edit_group")?>" class="tooltip-top btn btn-small"><span><i class="icon-plus"></i></span> Add group</a>
    <a href="#" class="tooltip-top btn btn-small" onclick="removeSelected()" title="Remove Selected"><span><i class="icon-trash"></i></span> Remove Selected</a>
    </p>

</div> 

<script language="javascript" type="text/javascript">

	$(function(){
			
		/*$('#clear').click(function(){
			$('#group-search-form').form('clear');
			$('#group-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#group-table').datagrid({
				queryParams:{data:$('#group-search-form').serialize()}
				});
		});		*/
		$('#group-table').datagrid({
			url:'<?=site_url('customer/admin/group_json')?>',
			height:'auto',
			width:'auto'

		});
	
	});

	function getActions(value,row,index)
	{
		var e = '<a href="<?php echo site_url('customer/admin/edit_group')?>/'+row.id+'" class="tooltip-top btn btn-small" title="Edit"><span><i class="icon-pencil"></i></span> </a>';
		if(row.id > 1)
		{
			var d = '<a href="#" onclick="removegroup('+index+')" class="tooltip-top btn btn-small" title="Delete"><span><i class="icon-trash"></i></span> </a>';
			
			return e+d;
		}
		else
		{
			return e;
		}
				
	}
	
	function removegroup(index)
	{
		$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
			if (r){
				var row = $('#group-table').datagrid('getRows')[index];
				//console.log('row ',row); return false;
				$.post('<?php  echo site_url('customer/admin/delete_group')?>', {id:[row.id]}, function(){
					$('#group-table').datagrid('deleteRow', index);
					$('#group-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#group-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','Are you sure to Delete?',function(r){
				if(r){				
					$.post('<?php  echo site_url('customer/admin/delete_group')?>',{id:selected},function(data){
						$('#group-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
</script>
