<?php 
class Search extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('size/size_model');
		$this->load->model('material/material_model');
		$this->load->library('pagination');
	}
	
	
	function index($param = NULL)
	{
		if($param==NULL)
		{
		    $param = $this->input->get('q');
		}
		else
		{
		    $param = $param;
		}
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();

		$limit=($this->input->get('limit'))?$this->input->get('limit'):null;
		$offset=$this->input->get('per_page');

		$data['sizes'] = $this->size_model->getSizes(null,'sequence asc')->result();
		$data['materials'] = $this->material_model->getMaterials()->result();

		$this->load->model('color/color_model');
		$data['colors'] = $this->color_model->getColors()->result_array();

		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){
		
			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['wishlist']);
		}
		
		$data['page_title']	= 'Search Results';
		$this->product_model->joins=array('CATEGORY_PRODUCT');
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'sequence', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		if($this->input->get('cat_id'))
		{
			$catId = $this->input->get('cat_id');

			$this->product_model->joins=array('CATEGORY_PRODUCT','CATEGORY');
			
			$this->db->where('categories.id ='.$catId.' or categories.parent_id ='.$catId);
		}	
		// $where = array('enabled'=>1);
		$this->db->where('products.enabled = 1');
		$where = $this->_get_where($param);
		if($where)
		{
			$this->db->where($where);
		}
		//$where = array_merge($this->_get_where(),$where);
		
		//$this->db->order_by('id desc');
		$this->db->group_by('products.id');
		// $this->db->limit(10);

		$data['products'] = $this->product_model->getProducts(NULL,$sort_by['by'].' ' . $sort_by['sort'], array('limit'=>$limit,'offset'=> $offset))->result();
		if(empty($data['products']))
		{
		    


		  $data['products'] =  $this->recursion($param, $data['products'], $limit, $offset, $sort_by);
		 
		}
		$data['count'] = $this->product_model->getProducts(NULL,$sort_by['by'].' ' . $sort_by['sort'])->result();
		
		
		//$data['query'] = $this->input->get('q');

		$config=$this->pagination_config();
		$config['base_url']		= site_url($base_url);
		$config['uri_segment']	= $segments;
		$config['per_page']		= $limit;
		$config['total_rows']	= count($data['count']);
		$query_string=unset_query_param('per_page');

		$config['base_url']=site_url($this->uri->segment(1)) . '?'.$query_string;
		$config['page_query_string']= TRUE;
		$this->pagination->initialize($config);
        
        $data['type'] = $this->input->get('q');    
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		//echo "<pre> query"; print_r($this->db->last_query());
		//echo "<pre> result"; print_r($data['products']);

		$this->view('search/index', $data);


	}
	private function recursion($param, $result, $limit, $offset, $sort_by)
	{
	    if($result)
	    {
	        return $result;
	    }
	    else
	    {
	         $param = substr($param, 0, -1);
	         $where = $this->_get_where($param);
	         	$this->db->where('products.enabled = 1');
            	$this->db->group_by('products.id');
            	$this->db->where($where);
               $result = $this->product_model->getProducts(NULL,$sort_by['by'].' ' . $sort_by['sort'], array('limit'=>$limit,'offset'=> $offset))->result();
              return $this->recursion($param, $result, $limit, $offset, $sort_by); 
	         
	         
	    }
	}
	private function _get_where($param)
	{
		$where = array();
		$where = '';
		$size = $this->input->get('size');
		$gender = $this->input->get('gender');
		$material = $this->input->get('material');
		
		if($param)
		{
			$where = "products.display_name_".$this->culture_code." like '%".$param."%'";
		}


		if($size)
		{	

			$size_array= explode(",",$this->input->get('size'));
			// print_r($size_array);
			// echo "count ". count($size_array); 
			for($i=0; $i < count($size_array) ; $i++)
			{	
				if($i == 0)
				{	
					if($param)
					{
						$where .='AND products.sizes like "%'.$size_array[$i].'%"';
					}
					else
					{
						$where .='products.sizes like "%'.$size_array[$i].'%"';
					}
		
				}
				else
				{
					$where .=' OR products.sizes like "%'.$size_array[$i].'%"';
				}	
				
				
			}
			
		}
		
		if($gender)
		{
			$gender_array= explode(",",$this->input->get('gender'));
			for($i=0; count($gender_array)> $i ; $i++)
			{	
				if($i == 0)
				{	
					if($param || $size)
					{
						$where .='AND products.product_for like "%'.$gender_array[$i].'%"';
					}
					else
					{
						$where .='products.product_for like "%'.$gender_array[$i].'%"';
					}	
				}
				else
				{
					$where .='OR products.product_for like "%'.$gender_array[$i].'%"';
				}	
				
				
			}
		}
		
		if($material)
		{
			$material_array= explode(",",$this->input->get('material'));
			for($i=0; count($material_array)> $i ; $i++)
			{	
				if($i == 0)
				{	
					if($param || $size || $gender)
					{

						$where .='AND products.materials like "%'.$material_array[$i].'%"';
					}
					else
					{
						$where .='products.materials like "%'.$material_array[$i].'%"';
					}
				}
				else
				{
					$where .='OR products.materials like "%'.$material_array[$i].'%"';
				}	
				
				
			}
		}
		
		return $where;
	}

	function priceSort()
	{
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){
		
			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['wishlist']);
		}

		$value1 = $this->input->post('value1');
		$value2 = $this->input->post('value2');
		$q = $this->input->post('q');
		

		$this->product_model->joins=array('CATEGORY_PRODUCT');
		
		
		$this->db->where('price between "'.$value1.'" and "'.$value2.'"');
		$this->db->where('name like "%'.$q.'%"');

		$data['products']	= $this->product_model->getProducts()->result();

		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		$this->load->view('category/content',$data);
	}

	function colorSort()
	{
		$data['wishlist']= array();

		if($this->customer_model->is_logged_in(false,false)){
		
			$this->db->where('id',$this->go_cart->customer('id'));
			$customer=$this->customer_model->getCustomers()->row_array();
			$data['wishlist'] = unserialize($customer['wishlist']);
		}

		$data['colorId'] = $this->input->post('colorId');
		$q = $this->input->post('q');
		

		$this->product_model->joins=array('CATEGORY_PRODUCT');
		
		$this->db->where('colors like "%'.$data['colorId'].'%"');
		$this->db->where('name like "%'.$q.'%"');
		
		$data['products']	= $this->product_model->getProducts()->result();
		
		
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		$this->load->view('category/content',$data);
	}

	function sortSize()
	{

		//echo "<pre>"; print_r($_POST);
		$requests	= $this->input->post('checked');
		$q = $this->input->post('q');
		//echo "<pre>"; print_r($requests); exit;
		$flag = false;
		if(count($requests)>0)
		{	
			//$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
			$this->db->where('products.name_'.$this->culture_code.' like "%'.$q.'%"');
			$this->db->where('products.enabled','1');
			$where ='';
			for($i=0; count($requests)> $i ; $i++)
			{	
				if($flag == false)
				{	
					
						$where .='products.sizes like "%'.$requests[$i].'%"';
					
					$flag = true;
				}
				else
				{
					$where .='OR products.sizes like "%'.$requests[$i].'%"';
				}	
				
				
			}
			
			$this->db->where($where);

			$data['products'] = $this->product_model->getProducts()->result();
		}
		
		// echo $this->db->last_query();
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		

			$data['sizes'] = $this->size_model->getSizes()->result();
		
		// echo '<pre>';print_r($data['products']);
		// exit;
		$this->load->view('search/content',$data);
		//redirect as to change the url
		//redirect(site_url('order/admin'));	
		    
	}

	function categorySort()
	{
		// $data['wishlist']= array();

		// if($this->customer_model->is_logged_in(false,false)){
		
		// 	$this->db->where('id',$this->go_cart->customer('id'));
		// 	$customer=$this->customer_model->getCustomers()->row_array();
		// 	$data['wishlist'] = unserialize($customer['wishlist']);
		// }

		$data['sizes'] = $this->size_model->getSizes()->result();
		$catId = $this->input->post('id');
		$q = $this->input->post('q');
		

		$this->product_model->joins=array('CATEGORY_PRODUCT','CATEGORY');
		
		$this->db->where('categories.id ='.$catId.' or categories.parent_id ='.$catId);
		$this->db->where('products.name_'.$this->culture_code.' like "%'.$q.'%"');
		$this->db->where('products.enabled','1');
		$data['products']	= $this->product_model->getProducts()->result();
		
		
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}

		$this->load->view('search/content',$data);
	}

	function sortGender()
	{
		//echo "<pre>"; print_r($_POST);
		$data['sizes'] = $this->size_model->getSizes()->result();
		
		$q = $this->input->post('q');
		$requests	= $this->input->post('checked');
		//echo "<pre>"; print_r($requests); exit;
		$flag = false;
		if(count($requests)>0)
		{	
			$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
			$this->db->where('products.name_'.$this->culture_code.' like "%'.$q.'%"');
			$this->db->where('products.enabled','1');
			$where ='';
			for($i=0; count($requests)> $i ; $i++)
			{	
				if($flag == false)
				{	
					
						$where .='products.product_for like "%'.$requests[$i].'%"';
					
					$flag = true;
				}
				else
				{
					$where .='OR products.product_for like "%'.$requests[$i].'%"';
				}	
				
				
			}
			
			$this->db->where($where);

			$data['products'] = $this->product_model->getProducts()->result();
		}
		
		// echo $this->db->last_query();
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		

			
		
		// echo '<pre>';print_r($data['products']);
		// exit;
		$this->load->view('search/content',$data);
	}

	function sortMaterial()
	{

		//echo "<pre>"; print_r($_POST);
		$requests	= $this->input->post('checked');
		$q = $this->input->post('q');
		//echo "<pre>"; print_r($requests); exit;
		$flag = false;
		if(count($requests)>0)
		{	
			//$this->product_model->joins = array('CATEGORY_PRODUCT','CATEGORY');
			$this->db->where('products.name_'.$this->culture_code.' like "%'.$q.'%"');
			$this->db->where('products.enabled','1');
			$where ='';
			for($i=0; count($requests)> $i ; $i++)
			{	
				if($flag == false)
				{	
					
						$where .='products.materials like "%'.$requests[$i].'%"';
					
					$flag = true;
				}
				else
				{
					$where .='OR products.materials like "%'.$requests[$i].'%"';
				}	
				
				
			}
			
			$this->db->where($where);

			$data['products'] = $this->product_model->getProducts()->result();
		}
		
		// echo $this->db->last_query();
		foreach ($data['products'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->option_model->get_product_options($p->id);
		}
		

			$data['sizes'] = $this->size_model->getSizes()->result();
		
		// echo '<pre>';print_r($data['products']);
		// exit;
		$this->load->view('search/content',$data);
		//redirect as to change the url
		//redirect(site_url('order/admin'));	
		    
	}


	//Go-cart index function
	// function index($code=false, $page = 0)
	// {
	// 	$this->load->model('Search_model');
		
	// 	//check to see if we have a search term
	// 	if(!$code)
	// 	{
	// 		//if the term is in post, save it to the db and give me a reference
	// 		$term		= $this->input->post('term', true);
	// 		$code		= $this->Search_model->record_term($term);
			
	// 		// no code? redirect so we can have the code in place for the sorting.
	// 		// I know this isn't the best way...
	// 		redirect('search/index/'.$code.'/'.$page);
	// 	}
	// 	else
	// 	{
	// 		//if we have the md5 string, get the term
	// 		$term	= $this->Search_model->get_term($code);
	// 	}
		
	// 	if(empty($term))
	// 	{
	// 		//if there is still no search term throw an error
	// 		$this->session->set_flashdata('error', lang('search_error'));
	// 		redirect('cart');
	// 	}

	// 	$data['page_title']			= lang('search');
	// 	$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
	// 	//fix for the category view page.
	// 	$data['base_url']			= array();
		
	// 	$sort_array = array(
	// 						'name/asc' => array('by' => 'name', 'sort'=>'ASC'),
	// 						'name/desc' => array('by' => 'name', 'sort'=>'DESC'),
	// 						'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
	// 						'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
	// 						);
	// 	$sort_by	= array('by'=>false, 'sort'=>false);
	
	// 	if(isset($_GET['by']))
	// 	{
	// 		if(isset($sort_array[$_GET['by']]))
	// 		{
	// 			$sort_by	= $sort_array[$_GET['by']];
	// 		}
	// 	}
		
	// 		$data['page_title']	= lang('search');
	// 		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
	// 		//set up pagination
	// 		$this->load->library('pagination');
	// 		$config=$this->pagination_config();
	// 		$config['base_url']		= base_url().'search/index/'.$code.'/';
	// 		$config['uri_segment']	= 4;
	// 		$config['per_page']		= 20;
			

			
	// 		$result					= $this->product_model->search_products($term, NULL, $page, $sort_by['by'], $sort_by['sort']);
	// 		$config['total_rows']	= $result['count'];
	// 		$this->pagination->initialize($config);
	
	// 		$data['products']		= $result['products'];
	// 		foreach ($data['products'] as &$p)
	// 		{
	// 			$p->images	= (array)json_decode($p->images);
	// 			$p->options	= $this->option_model->get_product_options($p->id);
	// 		}
			
	// 		$this->view('category/index', $data);
	// }

	
	
}