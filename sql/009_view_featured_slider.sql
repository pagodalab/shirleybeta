CREATE VIEW `view_featured_slider` AS SELECT
tbl_featured_slider.id,
tbl_featured_slider.product_id,
tbl_featured_slider.sequence,
tbl_featured_slider.added_date,
tbl_featured_slider.modified_date,
tbl_featured_slider.`status`,
tbl_products.name_en,
tbl_products.name_da,
tbl_products.display_name_en,
tbl_products.display_name_da,
tbl_products.slug,
tbl_products.images
FROM
tbl_featured_slider
INNER JOIN tbl_products ON tbl_featured_slider.product_id = tbl_products.id 