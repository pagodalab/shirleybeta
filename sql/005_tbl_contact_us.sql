/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : shirleyb_beta

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-11-28 15:36:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_contact_us
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contact_us`;
CREATE TABLE `tbl_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` text,
  `created_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
