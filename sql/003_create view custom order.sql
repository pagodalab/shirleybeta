CREATE VIEW `view_custom_order_relation_all` AS SELECT
custom_order_relation.id,
custom_order_relation.style_id,
custom_order_relation.color_id,
custom_order_relation.size_id,
custom_order_relation.material_id,
tbl_colors.color_name_en,
tbl_colors.color_name_da,
tbl_colors.color_code,
tbl_materials.name_en,
tbl_materials.name_da,
tbl_sizes.size_title_en,
tbl_sizes.size_title_da,
tbl_sizes.size_code,
tbl_sizes.type,
tbl_sizes.sequence,
tbl_styles.style_name_en,
tbl_styles.style_name_da,
tbl_styles.`status`
FROM
custom_order_relation
INNER JOIN tbl_colors ON tbl_colors.color_id = custom_order_relation.color_id
INNER JOIN tbl_materials ON tbl_materials.id = custom_order_relation.material_id
INNER JOIN tbl_sizes ON tbl_sizes.size_id = custom_order_relation.size_id
INNER JOIN tbl_styles ON tbl_styles.id = custom_order_relation.style_id


CREATE  VIEW `view_custom_order_table` AS SELECT
tbl_custom_orders.id,
tbl_custom_orders.`name`,
tbl_custom_orders.email,
tbl_custom_orders.address,
tbl_custom_orders.contact,
tbl_custom_orders.style_name,
tbl_custom_orders.color,
tbl_custom_orders.material,
tbl_custom_orders.size,
tbl_custom_orders.response_time,
view_custom_order_relation_all.color_name_en,
view_custom_order_relation_all.name_en,
view_custom_order_relation_all.size_title_en,
view_custom_order_relation_all.style_name_en
FROM
tbl_custom_orders
INNER JOIN view_custom_order_relation_all ON view_custom_order_relation_all.style_id = tbl_custom_orders.style_name AND view_custom_order_relation_all.color_id = tbl_custom_orders.color AND view_custom_order_relation_all.size_id = tbl_custom_orders.size AND view_custom_order_relation_all.material_id = tbl_custom_orders.material