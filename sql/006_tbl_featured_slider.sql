/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : shirleyb_beta

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-11-30 13:40:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_featured_slider
-- ----------------------------
DROP TABLE IF EXISTS `tbl_featured_slider`;
CREATE TABLE `tbl_featured_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
