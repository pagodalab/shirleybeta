/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : shirleyb_redal

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-12-06 11:38:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_popup_notice
-- ----------------------------
DROP TABLE IF EXISTS `tbl_popup_notice`;
CREATE TABLE `tbl_popup_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_popup_notice
-- ----------------------------
INSERT INTO `tbl_popup_notice` VALUES ('1', 'Dashain', 'popup-notice.jpg', '2017-09-25', '2017-10-03', '1', '2017-09-25 15:07:36', '2017-09-25 15:07:39');
