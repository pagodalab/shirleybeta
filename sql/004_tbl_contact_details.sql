/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : shirleyb_beta

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-12-04 16:06:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_contact_details
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contact_details`;
CREATE TABLE `tbl_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_contact_details
-- ----------------------------
INSERT INTO `tbl_contact_details` VALUES ('1', 'Gairidhara', 'Kathmandu', 'Nepal', '01-4700264', '', 'info@shirleybredal.com', '1');
