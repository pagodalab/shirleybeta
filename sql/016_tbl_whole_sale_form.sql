/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : shirleyb_beta

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-12-12 12:58:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_whole_sale_form
-- ----------------------------
DROP TABLE IF EXISTS `tbl_whole_sale_form`;
CREATE TABLE `tbl_whole_sale_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retail_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `shop_location` varchar(255) DEFAULT NULL,
  `online_address` varchar(255) DEFAULT NULL,
  `establish_year` varchar(255) DEFAULT NULL,
  `brands_retailed` text,
  `reasons` text,
  `del_flag` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
