ALTER 
VIEW `view_custom_order_table` AS 
SELECT
tbl_custom_orders.id,
tbl_custom_orders.`name`,
tbl_custom_orders.email,
tbl_custom_orders.address,
tbl_custom_orders.contact,
tbl_custom_orders.style_name,
tbl_custom_orders.color,
tbl_custom_orders.material,
tbl_custom_orders.size,
tbl_custom_orders.response_time,
view_custom_order_relation_all.color_name_en,
view_custom_order_relation_all.name_en,
view_custom_order_relation_all.size_title_en,
view_custom_order_relation_all.style_name_en,
tbl_custom_orders.created_date,
tbl_custom_orders.modified_date
FROM
	tbl_custom_orders
INNER JOIN view_custom_order_relation_all ON view_custom_order_relation_all.style_id = tbl_custom_orders.style_name
AND view_custom_order_relation_all.color_id = tbl_custom_orders.color
AND view_custom_order_relation_all.size_id = tbl_custom_orders.size
AND view_custom_order_relation_all.material_id = tbl_custom_orders.material ;

