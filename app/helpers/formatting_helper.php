<?php 
function format_address($fields, $br=false)
{
	if(empty($fields))
	{
		return ;
	}
	
	// Default format
	$default = "{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city}, {zone} {zip}\n{country}";
	
	// Fetch country record to determine which format to use
	$CI = &get_instance();
	$CI->load->model('location_model');
	$c_data = $CI->location_model->get_country($fields['country_id']);
	
	if(empty($c_data->address_format))
	{
		$formatted	= $default;
	} else {
		$formatted	= $c_data->address_format;
	}

	$keys = preg_split("/[\s,{}]+/", $formatted);
	foreach ($keys as $id=>$key)
	{
		$formatted = array_key_exists($key, $fields) ? str_replace('{'.$key.'}', $fields[$key], $formatted) : str_replace('{'.$key.'}', '', $formatted);
	}
	
	// remove any extra new lines resulting from blank company or address line
	$formatted		= preg_replace('`[\r\n]+`',"\n",$formatted);
	if($br)
	{
		$formatted	= nl2br($formatted);
	}
	return $formatted;
	
}

/*function format_currency($value, $symbol=true)
{	
	$fmt = numfmt_create( config_item('locale'), NumberFormatter::CURRENCY );
	return numfmt_format_currency($fmt, $value, config_item('currency_iso'));
}*/

function format_currency($value, $symbol=true)
{
	// locale information must be set up for this to return a proper value
	// return money_format(!%i, $value);
	
	if(!is_numeric($value))
	{
		return;
	}

	if($value < 0 )
	{
		$neg = '- ';
	} else {
		$neg = '';
	}
	
	$formatted = number_format(abs($value), 2, '.', ',');
	
	if($symbol)
	{
		$formatted = $neg.config_item('currency_iso').$formatted;
	}
	
	return $formatted;

}

function place_productPrice() {
	$CI = &get_instance();

	$session_currency = $CI->session->userdata('currency');
	$data = array();
	$data['exrate'] = null;
	$data['currency'] = null;

	switch ($session_currency) {
		case 'euro':
		$data['exrate'] = 'rate_euro';
		$data['currency'] = "euro_currency";
			// $amount = round(($price)*$CI->exrate['rate_euro']).' '.$CI->euro_currency;
		break;

		case 'dollar':
		$data['exrate'] = 'rate_dollar';
		$data['currency'] = "usd_currency";
			// $amount =  round(($price)*$CI->exrate['rate_dollar']).' '.$CI->usd_currency;
		break;

		case 'yen':
		$data['exrate'] = 'rate_yen';
		$data['currency'] = "japanese_currency";
			// $amount =  round(($price)*$CI->exrate['rate_yen']).' '.$CI->japanese_currency;
		break;

		case 'HKD':
		$data['exrate'] = 'rate_hdollar';
		$data['currency'] = "hongkong_currency";
			// $amount =  round(($price)*$CI->exrate['rate_hdollar']).' '.$CI->hongkong_currency;
		break;

		case 'won':
		$data['exrate'] = 'rate_korea';
		$data['currency'] = "korean_currency";
			// $amount =  round(($price)*$CI->exrate['rate_korea']).' '.$CI->korean_currency;
		break;

		case 'Pound':
		$data['exrate'] = 'rate_pound';
		$data['currency'] = "british_currency";
			// $amount =  round(($price)*$CI->exrate['rate_pound']).' '.$CI->british_currency;
		break;

		case 'AUD':
		$data['exrate'] = 'rate_adollar';
		$data['currency'] = "australlian_currency";
			// $amount =  round(($price)*$CI->exrate['rate_adollar']).' '.$CI->australlian_currency;
		break;

		default:
		$data['exrate'] = 'rate_danish';
		$data['currency'] = "base_currency";
		break;
	}

	return $data;
}