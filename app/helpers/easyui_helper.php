<?php
function easyui_settings($key)
{
	$combo_settings['MAKER']=array('url'=>site_url('maker/admin/maker/combo_json'),
								'valueField'=>'maker_id',
								'textField'=>'maker_name'
							);
	$combo_settings['MODEL']=array('url'=>site_url('model/admin/model/combo_json'),
								'valueField'=>'model_id',
								'textField'=>'model_name'
							);	
	$combo_settings['VEHICLE_TYPE']=array('url'=>site_url('vehicle/admin/type/combo_json'),
								'valueField'=>'vehicle_type_id',
								'textField'=>'type'
							);							
	$combo_settings['BODY_TYPE']=array('url'=>site_url('body_type/admin/body_type/combo_json'),
								'valueField'=>'body_type_id',
								'textField'=>'body_type_name'
							);
	$combo_settings['STATUS']=array('url'=>site_url('status_type/admin/status_type/combo_json'),
								'valueField'=>'status_id',
								'textField'=>'option'
							);		

	$combo_settings['COLOR']=array('url'=>site_url('color/admin/color/combo_json'),
								'valueField'=>'color_id',
								'textField'=>'color_name'
							
								);
	$combo_settings['FUEL']=array('url'=>site_url('fuel/admin/fuel/combo_json'),
								'valueField'=>'fuel_id',
								'textField'=>'fuel_name'
							
								);
	$combo_settings['TRANSMISSION']=array('url'=>site_url('transmission/admin/transmission/combo_json'),
								'valueField'=>'transmission_id',
								'textField'=>'transmission_name'
							
								);		
	$combo_settings['COUNTRY']=array('url'=>site_url('country/admin/country/combo_json'),
								'valueField'=>'country_id',
								'textField'=>'country_name'
							
								);	
	$combo_settings['PAY_TYPE']=array('url'=>site_url('type/admin/pay/combo_json'),
								'valueField'=>'pay_type_id',
								'textField'=>'name'
								);	
	$combo_settings['GRADE']=array('url'=>site_url('grade/admin/grade/combo_json'),
								'valueField'=>'grade_id',
								'textField'=>'grade_name'
								);																													

	$combo_settings['PREFECTURE']=array('url'=>site_url('prefecture/admin/prefecture/combo_json'),
								'valueField'=>'prefecture_id',
								'textField'=>'prefecture_name'
								);
        
	$combo_settings['PORT']=array('url'=>site_url('port/admin/port/combo_json/107'),
								'valueField'=>'port_id',
								'textField'=>'port_name'
								);
	$combo_settings['PLAN']=array('url'=>site_url('plan/admin/plan/combo_json'),
								'valueField'=>'plan_id',
								'textField'=>'plan_name'
								);	
	$combo_settings['PROMOTION']=array('url'=>site_url('promotion/admin/promotion/combo_json'),
								'valueField'=>'template_id',
								'textField'=>'subject'
								);																
        
	$combo_settings['TEMPLATE']=array('url'=>site_url('template/admin/template/combo_json'),
								'valueField'=>'template_id',
								'textField'=>'template_name'
								);	

	$combo_settings['ACL_GROUPS']=array('url'=>site_url('auth/admin/acl_groups/combo_json'),
								'valueField'=>'id',
								'textField'=>'name'
								);									
	
	return $combo_settings[$key];						
}

function easyui_combobox($obj,$key,$options=array())
{
		$combo_settings=easyui_settings($key);
	if(!empty($options))
	{

		$combo_settings=array_merge($combo_settings, $options);
	}

?>		
	$('#<?=$obj?>').combobox(<?=json_encode($combo_settings)?>);		
<?php		
}

function easyui_combobox_localdata($obj,$key,$data)
{
	$combo_settings=easyui_settings($key);
?>		
	$('#<?=$obj?>').combobox({
			data:<?php echo $data?>,
			valueField:'<?=$combo_settings['valueField']?>',
			textField:'<?=$combo_settings['textField']?>',
           
	});		
<?php		
}

function ckeditor($obj,$config=array())
{
?>

			var config = {
            toolbar : 'Full',
			skin:'office2003',
			language: 'ja',
            height:300,
            width:800,
            filebrowserBrowseUrl:CKEDITOR.basePath +'plugins/filemanager/index.html',
            filebrowserImageBrowseUrl :CKEDITOR.basePath +'plugins/filemanager/index.html?type=Images',
            filebrowserFlashBrowseUrl : CKEDITOR.basePath +'plugins/filemanager/index.html?Type=Flash',
			};
            <?
				if(!empty($config))
				{
					echo 'config='.json_encode($config).';';		
				}	
			?>
            $('<?php echo $obj?>').ckeditor(config);  
<?php	
}

function tinymce($obj,$config=array())
	{
?>
		$('#<?=$obj?>').tinymce({
			script_url : '<?=base_url()?>assets/js/tinymce/tiny_mce.js',
            mode: 'exact',
            skin : "o2k7",
            <? if(empty($config['theme'])){
				echo "theme : 'advanced',";
			}
			?>
            plugins : "advimage,advlink,media,contextmenu,",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			//content_css : "css/content.css",
            relative_urls : false,
            convert_urls : false,
			// Drop lists for link/image/media/template dialogs
			//template_external_list_url : "lists/template_list.js",
			//external_link_list_url : "lists/link_list.js",
			//external_image_list_url : "lists/image_list.js",
			//media_external_list_url : "lists/media_list.js",
			<?php
            if( check('Control Panel',NULL,FALSE) )
			{
				if(empty($config['no_filemanager'])){
            ?>
            
            //file_browser_callback : "ajaxfilemanager",
            <?php
				}
			}
			?>
			// Theme options
			//theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			//theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			//theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			
			
			//theme_advanced_statusbar_location : "bottom",*/
            
		});
        
  
<?php		
	}	