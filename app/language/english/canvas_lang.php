<?php
/******************************************
US English
Admin Canvas Language
******************************************/

//canvas table elements
$lang['canvas_templates']			= 'Canvas Templates';
$lang['search_term']				= 'Search Term';
$lang['template_form']				= 'Template Form';
$lang['template_image']				= 'Image';
$lang['template_title']				= 'Title';
$lang['template_code']				= 'Code';
$lang['template_category']			= 'Categories';
$lang['template_status']			= 'Status';

//form elements
$lang['checklist']					= 'Checklist';
$lang['radiolist']					= 'Radiolist';
$lang['droplist']					= 'Droplist';
$lang['textfield']					= 'Textfield';
$lang['textarea']					= 'Textarea';
$lang['calendar']					= 'Calendar';
$lang['add_option']					= 'Add Option';
$lang['select_option_type']			= 'Select Option Type';

//option form portion
$lang['option_name']				= 'Option Name';
$lang['required']					= 'Required';
$lang['add_item']					= 'Add Option Item';

//related products
$lang['select_a_product']			= 'Search for a related product.';
$lang['add_related_product']		= 'Add Related Product';
$lang['template_name']				= 'Template Name';

//product images
$lang['primary']					= 'Main Image';
$lang['remove']						= 'Remove';
$lang['alt_tag']					= 'Alt Tag';
$lang['caption']					= 'Caption';

//confirmations
$lang['confirm_remove_image']		= 'Are you sure you want to remove this image?';
$lang['confirm_remove_value']		= 'Are you sure you want to remove this value?';
$lang['confirm_remove_option']		= 'Are you sure you want to remove this option?';
$lang['confirm_remove_related']		= 'Are you sure you want to remove this related item?';
$lang['confirm_remove_category']	= 'Are you sure you want to remove this category?';
$lang['confirm_delete_product']		= 'Are you sure you want to delete this product?';

//alerts
$lang['alert_must_name_option']		= 'You must give this option a name.';
$lang['alert_product_related']		= 'This product is already related.';
$lang['alert_select_product']		= 'Please select a product to add first.';
$lang['alert_product_assorted']		= 'This product is already Assorted.';
$lang['confirm_remove_assorted']	= 'Are you sure you want to remove this assorted item?';

//messages & errors
$lang['message_saved_template']		= 'The Template has been saved.';
$lang['message_template_update']	= 'Your Template have been updated.';
$lang['message_deleted_template']	= 'The Template has been deleted.';
$lang['message_failure']			= 'Template Add/Edit Unsuccessful!.';
$lang['error_not_found']			= 'The requested Template could not be found.';
$lang['search_returned']			= 'Your searched returned %d result(s)';

//added
$lang['assort_products']			= 'Assort Products';
$lang['add_assortment_product']		= 'Add Assortment Product';
$lang['occasions_events']			= 'Occasions & Events';
