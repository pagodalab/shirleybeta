<?php
/******************************************
US English
Admin Common Language
******************************************/

//header
$lang['common_sales'] = 'Sales';
$lang['common_orders']= 'Orders';
$lang['common_customers'] = 'Customers';
$lang['common_groups']= 'Groups';
$lang['common_reports'] = 'Reports';
$lang['common_coupons'] = 'Coupons';
$lang['common_giftcards'] = 'Gift Cards';
$lang['common_catalog'] = 'Catalog';
$lang['common_categories']= 'Categories';
$lang['common_filters'] = 'Filters';
$lang['common_products']= 'Products';
$lang['common_digital_products']= 'Digital Products';
$lang['common_content'] = 'Content';
$lang['common_banners'] = 'Banners';
$lang['common_boxes'] = 'Boxes';
$lang['common_pages'] = 'Pages';
$lang['common_administrative']= 'Administrative';
$lang['common_gocart_configuration'] = 'Configuration';
$lang['common_shipping_modules'] = 'Shipping Modules';
$lang['common_payment_modules'] = 'Payment Modules';
$lang['common_widget_modules'] = 'Widget Modules';
$lang['common_canned_messages'] = 'Canned Messages';
$lang['common_locations'] = 'Locations';
$lang['common_administrators']= 'Administrators';
$lang['common_note']= 'Note';
$lang['common_alert'] = 'Alert';
$lang['common_log_out'] = 'Log Out';
$lang['common_front_end'] = 'Front End';
$lang['common_dashboard'] = 'Dashboard';
$lang['common_home']= 'Home';
$lang['common_actions'] = 'Actions';
$lang['enabled'] = 'Enabled';
$lang['disabled'] = 'Disabled';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['general_yes'] = 'Yes';
$lang['general_no'] = 'No';
$lang['common_order_item']= 'Purchase Order';
$lang['common_rates'] = 'Locations - Rates';
$lang['common_occasion'] = 'Occasions';
$lang['common_events'] = 'Events';

//buttons
$lang['save'] = 'Save';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['search'] = 'Search';
$lang['toggle_wysiwyg'] = 'Toggle WYSIWYG';

//Added  By Dixanta
$lang['common_currency'] = 'Currency';
$lang['common_board'] = 'Message Board';
$lang['common_brands'] = 'Brand';
$lang['common_advertisement'] = 'Advertisement';
$lang['common_email_template'] = 'Email Templates';
$lang['common_blog'] = 'Blog';


$lang['common_costing'] = 'Margin';
$lang['success_message'] = 'Success';
$lang['failure_message'] = 'Error';
$lang['review']='Review';

//Added by Sazan
$lang['common_canvas_templates'] = 'Canvas Templates';

$lang['common_restock_notify'] = 'Restock Notify';
$lang['common_featured_slider'] = 'Featured Slider';

$lang['common_popup_notice'] = 'Popup Notice';
$lang['common_wholesale_form'] = 'WholeSale Form';
