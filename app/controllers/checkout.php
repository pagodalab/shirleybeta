<?php 

/* Single page checkout controller*/

class Checkout extends Front_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('rates/location_rate_model');
		$this->load->model('size/size_model');
		$this->load->model('color/color_model');
		

		
		/*make sure the cart isnt empty*/
		if($this->go_cart->total_items()==0)
		{
			redirect('cart/view_cart');
		}

		/*is the user required to be logged in?*/
		if (config_item('require_login'))
		{
			$this->customer_model->is_logged_in('checkout');
		}

		if(!config_item('allow_os_purchase') && config_item('inventory_enabled'))
		{
			/*double check the inventory of each item before proceeding to checkout*/
			$inventory_check	= $this->go_cart->check_inventory();

			if($inventory_check)
			{
				/*
				OOPS we have an error. someone else has gotten the scoop on our customer and bought products out from under them!
				we need to redirect them to the view cart page and let them know that the inventory is no longer there.
				*/
				$this->session->set_flashdata('error', $inventory_check);
				redirect('cart/view_cart');
			}
		}
		/* Set no caching
	
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
		header("Cache-Control: no-store, no-cache, must-revalidate"); 
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	
		*/
		$this->load->library('form_validation');
	}

	function index()
	{
		/*show address first*/
		
		$this->step_1();
	}

	function step_1()
	{
		$data['customer']	= $this->go_cart->customer();
		//print_r($this->input->post());

		if(isset($data['customer']['id']))
		{
			$data['customer_addresses'] = $this->customer_model->get_address_list($data['customer']['id']);
		}

		/*require a billing address*/
		$this->form_validation->set_rules('address_id', 'Billing Address ID', 'numeric');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('address1', 'Address 1', 'trim|required|max_length[128]');
		//$this->form_validation->set_rules('address2', 'Address 2', 'trim|max_length[128]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');
		//$this->form_validation->set_rules('zone_id', 'State', 'trim');

		/*required for gift card*/
		$data['gift_card'] = '0';
		$data['product_in_cart'] = '0';
		$contents = $this->go_cart->contents();
		foreach($contents as $content){
			$name_da[] = $content['name_da'];
		}
		if(in_array('gift_card_DKK', $name_da) || in_array('gift_card_usd', $name_da) || in_array('gift_card_euro', $name_da)){
			$this->form_validation->set_rules('to_email', 'Recipent mail', 'trim|required|valid_mail');
			$this->form_validation->set_rules('to_name', 'Recipent\'s name', 'trim|required|valid_mail');
			$data['gift_card'] = '1';
		}else{
			$data['product_in_cart'] = '1';
		}

		/*if there is post data, get the country info and see if the zip code is required*/
		if($this->input->post('country_id'))
		{
			$country = $this->location_model->get_country($this->input->post('country_id'));
			//echo "<pre>"; print_r($country); exit;
			//if((bool)$country->postcode_required)
			//{
			//	$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[10]');
			//}
		}
		else
		{
			$this->form_validation->set_rules('zip', 'Zip', 'trim|max_length[10]');
		}

		if ($this->form_validation->run() == false)
		{	
			// $this->load->model('rates/location_rate_model');
			// $data['locations'] = $this->location_rate_model->get_location_rates();
			$data['address_form_prefix']	= 'bill';
			//echo '<pre>'; print_r($data);
			//exit;
			$this->view('checkout/address_form', $data);
		}
		else
		{
			/*data for gift card to session*/
			if(in_array('gift_card_DKK', $name_da) || in_array('gift_card_usd', $name_da) || in_array('gift_card_euro', $name_da)){
				$gift_to_session['gift_card']['to_name'] = $this->input->post('to_name');
				$gift_to_session['gift_card']['to_email'] = $this->input->post('to_email');
				$gift_to_session['gift_card']['personal_message'] = $this->input->post('personal_message');
				$this->session->set_userdata($gift_to_session);
			}
			if($this->input->post('register_account') == 'yes')
			{
				$id = $this->registerAccount();
				//$customer['id'] = $id;
			}	

			/*load any customer data to get their ID (if logged in)*/
			$customer				= $this->go_cart->customer();

			$customer['bill_address']['company']		= $this->input->post('company');
			$customer['bill_address']['firstname']	= $this->input->post('firstname');
			$customer['bill_address']['lastname']		= $this->input->post('lastname');
			$customer['bill_address']['email']		= $this->input->post('email');
			$customer['bill_address']['phone']		= $this->input->post('phone');
			$customer['bill_address']['address1']		= $this->input->post('address1');
			$customer['bill_address']['address2']	= $this->input->post('address2');
			$customer['bill_address']['city']			= $this->input->post('city');
			$customer['bill_address']['zip']			= $this->input->post('zip');

			/* get zone / country data using the zone id submitted as state*/
			$country								= $this->location_model->get_country(set_value('country_id'));
			//$zone									= $this->location_model->get_zone(set_value('zone_id'));

			//$customer['bill_address']['zone']			= $zone->code;  /*  save the state for output formatted addresses */
			$customer['bill_address']['zone']			= $this->input->post('zone_id');
			$customer['bill_address']['country']		= $country->name; /*  some shipping libraries require country name */
			$customer['bill_address']['country_code']   = $country->iso_code_2; /*  some shipping libraries require the code */ 
			//$customer['bill_address']['zone_id']		= $this->input->post('zone_id');  /*  use the zone id to populate address state field value */
			$customer['bill_address']['country_id']		= $this->input->post('country_id');

			$country_array = array('country'=>$country->name);
			$this->session->set_userdata($country_array);
			/* for guest customers, load the billing address data as their base info as well */
			if(empty($customer['id']))
			{
				$customer['company']	= $customer['bill_address']['company'];
				$customer['firstname']	= $customer['bill_address']['firstname'];
				$customer['lastname']	= $customer['bill_address']['lastname'];
				$customer['phone']		= $customer['bill_address']['phone'];
				$customer['email']		= $customer['bill_address']['email'];
			}

			if(!isset($customer['group_id']))
			{
				$customer['group_id'] = 1; /* default group */
			}

			/*if there is no address set then return blank*/
			if(empty($customer['ship_address']))
			{
				$customer['ship_address']	= $customer['bill_address'];
			}
			
			/* save customer details*/
			$result = $this->go_cart->save_customer($customer);
			
			// echo "<pre> lkshdlkfkhsdlkf "; 
			// print_r($result); 
			// print_r($this->go_cart->contents());
			// exit;
			/*send to the next form*/
			if($this->input->post('use_shipping')=='yes')
			{
				/*send to the next form*/
				// $this->session->sess_destroy();
				$location_rate = $this->getLocationRate($this->input->post('country_id'));
				//storing location rate in session

				$rate = array(
					'location_rate'		=>	$location_rate,
					'country'	=>	$this->input->post('country_id')
				);

				// print_r($rate); exit;
					//echo "<pre>";print_r($rate);exit;
				$this->session->set_userdata($rate);

				$use_ship = 'TRUE';

				$shipping_methods = $this->_get_shipping_methods();

				foreach($shipping_methods as $key=>$val):

					$ship_encoded	= json_encode(array($key, $val));

				endforeach;

				$this->go_cart->set_additional_detail('shipping_notes', $this->input->post('shipping_notes'));

				$shipping_method	= json_decode($ship_encoded);
				$shipping_code		= md5($ship_encoded);
    // print_r($shipping_method);
    // print_r($shipping_code);
     // exit;



				/* set shipping info */
				$this->go_cart->set_shipping($shipping_method[0], $shipping_method[1]->num, $shipping_code);

				// redirect('checkout/step_2');		
			}
			else 
			{
				//echo "<pre> lalalalalalalalal"; exit; 
				// redirect('checkout/shipping_address');
				$use_ship = 'FALSE';
			}

			

			echo json_encode(array('success'=>'TRUE','details'=>$customer,'shipping'=>$use_ship));

		}
	}


	function shipping_address()
	{
		//echo "<pre> Posted "; print_r($_POST); exit;
		$data['customer']	= $this->go_cart->customer();

		if(isset($data['customer']['id']))
		{
			$data['customer_addresses'] = $this->customer_model->get_address_list($data['customer']['id']);
		}

		/*require a shipping address*/
		$this->form_validation->set_rules('address_id', 'Billing Address ID', 'numeric');
		$this->form_validation->set_rules('ship_first', 'lang:address_firstname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('ship_last', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('ship_email', 'lang:address_email', 'trim|valid_email|max_length[128]');
		$this->form_validation->set_rules('ship_phone', 'lang:address_phone', 'trim|required');
		// $this->form_validation->set_rules('ship_resident_phone', 'lang:address_resident_phone', 'trim');
		// $this->form_validation->set_rules('ship_company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('ship_address1', 'lang:address1', 'trim|required|max_length[128]');
		//$this->form_validation->set_rules('ship_address2', 'lang:address2', 'trim|max_length[128]');
		$this->form_validation->set_rules('ship_city', 'lang:address_city', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('country_id', 'lang:address_country', 'trim|required|numeric');
		//$this->form_validation->set_rules('ship_zone_id', 'lang:address_state', 'trim|max_length[128]');
		// $this->form_validation->set_rules('photo_proof_required', 'Photo Proof', 'trim');
		// $this->form_validation->set_rules('cust_add_id', 'Cust Address Id', 'trim');
		
		// Relax the requirement for countries without zones
		//if($this->location_model->has_zones($this->input->post('country_id')))
		//{
		//	$this->form_validation->set_rules('zone_id', 'lang:address_state', 'trim|required');
		//} else {
		//	 $this->form_validation->set_rules('zone_id', 'lang:address_state'); // will be empty
		//	 echo $this->input->post('country_id');
		//	 echo 'Zone problem';
			//}
//
		/* if there is post data, get the country info and see if the zip code is required */
		if($this->input->post('country_id'))
		{
			$country = $this->location_model->get_country($this->input->post('country_id'));

			/*if((bool)$country->zip_required)
			{
				$this->form_validation->set_rules('zip', 'lang:address_zip', 'trim|required|max_length[10]');
			}*/
		}
		else
		{
			$this->form_validation->set_rules('ship_zip', 'lang:address_zip', 'trim|max_length[10]');
			echo 'Zip problem';
		}

		if ($this->form_validation->run() == false)
		{
			$this->load->model('rates/location_rate_model');
			/* show the address form but change it to be for shipping */
			
			$data['address_form_prefix']	= 'ship';
			// $data['longitude'] = NULL;
			// $data['latitude'] = NULL;
			// $data['locations'] = $this->location_rate_model->get_location_rates();
			//$data['header'] = '';

//			print_r($_POST);
			// echo 'Form validate';
			$this->view('checkout/address_form', $data);
		}
		else
		{
			
			//echo "<pre>"; print_r($_POST); exit;
			//echo "<pre> Posted "; print_r($_POST);
			/* load any customer data to get their ID (if logged in) */
			$customer				= $this->go_cart->customer();

			$customer['ship_address']['company']			= $this->input->post('ship_company');
			$customer['ship_address']['firstname']			= $this->input->post('ship_first');
			$customer['ship_address']['lastname']			= $this->input->post('ship_last');
			$customer['ship_address']['email']				= $this->input->post('ship_email');
			$customer['ship_address']['phone']				= $this->input->post('ship_phone');
			$customer['ship_address']['resident_phone']		= $this->input->post('ship_resident_phone');
			$customer['ship_address']['address1']			= $this->input->post('ship_address1');
			$customer['ship_address']['address2']			= $this->input->post('ship_address2');
			$customer['ship_address']['city']				= $this->input->post('ship_city');
			$customer['ship_address']['zip']				= $this->input->post('ship_zip');
			$customer['ship_address']['longitude']			= $this->input->post('longitude');
			$customer['ship_address']['latitude']			= $this->input->post('latitude');
			//$customer['ship_address']['photo_proof_required']	= (!($this->input->post('photo_proof_required')))?0:1 ;
			
			$location_data = array(
				'address1'=>$this->input->post('address1'),
				'longitude'=>$this->input->post('longitude'),
				'latitude'=>$this->input->post('latitude'),
								//'photo_proof_required'=>$this->input->post('photo_proof_required'),
				'cust_add_id'=>$this->input->post('cust_add_id')
			);
			
			$this->session->set_userdata($location_data);
			
			/* get zone / country data using the zone id submitted as state*/
			$country = $this->location_model->get_country(set_value('country_id'));
			//$zone = $this->location_model->get_zone(set_value('zone_id'));
			//if($this->location_model->has_zones($country->id))
			//{
			//	$customer['ship_address']['zone'] = $zone->code;  /*  save the state for output formatted addresses */
			//} 
			//else 
			//{
				//$customer['ship_address']['zone'] = '';
			//}
			$customer['ship_address']['zone'] = $this->input->post('zone_id');
			$customer['ship_address']['country']		= $country->name;
			$customer['ship_address']['country_code']   = $country->iso_code_2;
			//$customer['ship_address']['zone_id']		= $this->input->post('zone_id');
			$customer['ship_address']['country_id']		= $this->input->post('country_id');

			$country_array = array('country'=>$country->name);
			$this->session->set_userdata($country_array);

			/* for guest customers, load the shipping address data as their base info as well */
			if(empty($customer['id']))
			{
				$customer['company']	= $customer['ship_address']['company'];
				$customer['firstname']	= $customer['ship_address']['firstname'];
				$customer['lastname']	= $customer['ship_address']['lastname'];
				$customer['phone']		= $customer['ship_address']['phone'];
				$customer['email']		= $customer['ship_address']['email'];
			}
			


			$a = array();
			$id = $this->input->post('cust_add_id');
			$a['id']						= ($id==0) ? '' : $id;
			$a['customer_id']				= isset($data['customer']['id'])?$data['customer']['id']:0;
			$a['field_data']['company']		= $this->input->post('ship_company');
			$a['field_data']['firstname']		= $this->input->post('ship_first');
			$a['field_data']['lastname']		= $this->input->post('ship_last');
			$a['field_data']['email']		= $this->input->post('ship_email');
			$a['field_data']['phone']		= $this->input->post('ship_phone');
			$a['field_data']['resident_phone']		= $this->input->post('ship__phone');
			$a['field_data']['address1']		= $this->input->post('ship_address1');
			$a['field_data']['address2']		= $this->input->post('ship_address2');
			$a['field_data']['city']			= $this->input->post('ship_city');
			$a['field_data']['zip']			= $this->input->post('ship_zip');
			$a['field_data']['longitude']		= $this->input->post('longitude');
			$a['field_data']['latitude']		= $this->input->post('latitude');
			// $a['field_data']['location_rate']	= $this->input->post('location_rate');
			
			// $this->session->sess_destroy();
			$location_rate = $this->getLocationRate($this->input->post('country_id'));
			//storing location rate in session
			$rate = array(
				'location_rate'		=>	$location_rate,
				'country'	=>	$this->input->post('country_id')
			);
			$this->session->set_userdata($rate);
//echo "<pre>"; print_r($_POST); exit;

			// get zone / country data using the zone id submitted as state	
			if(!empty($country))
			{
//				$a['field_data']['zone']			= $zone->code;  // save the state for output formatted addresses
				$a['field_data']['zone']			= $this->input->post('zone_id'); 
				$a['field_data']['country']		= $country->name; // some shipping libraries require country name
				$a['field_data']['country_code']   = $country->iso_code_2; // some shipping libraries require the code 
				$a['field_data']['country_id']  	= $this->input->post('country_id');
				//$a['field_data']['zone_id']		= $this->input->post('zone_id');  
			}
			
			
			//echo "<pre> a "; print_r($a);
			$this->customer_model->save_address($a);
 			//exit;
			if(!isset($customer['group_id']))
			{
				$customer['group_id'] = 1; /* default group */
			}
			//echo "<pre> Data"; print_r($customer); exit;
			/*  save customer details */
			$success = $this->go_cart->save_customer($customer);
			

			/* where to next? Shipping? */
			// $shipping_methods = $this->_get_shipping_methods();
			// echo "<pre> step2"; print_r($shipping_methods);
			// exit;

			// if($shipping_methods)
			// {	
			// 	echo 'inside shipping if';
			// 	$this->shipping_form($shipping_methods);
			// }
			// /* now where? continue to step 3 */
			// else
			// {	
			// 	echo 'inside shipping else';
			// 	$this->step_3();
			// }

			/* send to the next form */
			$shipping_methods = $this->_get_shipping_methods();

			foreach($shipping_methods as $key=>$val):

				$ship_encoded	= json_encode(array($key, $val));

			endforeach;

			$this->go_cart->set_additional_detail('shipping_notes', $this->input->post('shipping_notes'));
			
			$shipping_method	= json_decode($ship_encoded);
			$shipping_code		= md5($ship_encoded);
    // print_r($shipping_method); exit;



			/* set shipping info */
			$this->go_cart->set_shipping($shipping_method[0], $shipping_method[1]->num, $shipping_code);

			echo json_encode(array('success'=>$success,'rate'=>$rate));
			// redirect('checkout/step_2');
			
		}
	}

	function step_2()
	{
		/* where to next? Shipping? */
		$shipping_methods = $this->_get_shipping_methods();
		// echo "<pre> step2"; print_r($shipping_methods);
		// exit;

		if($shipping_methods)
		{
			$this->shipping_form($shipping_methods);
		}
		/* now where? continue to step 3 */
		else
		{
			$this->step_4();
		}
	}

	protected function shipping_form($shipping_methods)
	{
		
		$data['customer']			= $this->go_cart->customer();

		/* do we have a selected shipping method already? */
		$shipping					= $this->go_cart->shipping_method();
		$data['shipping_code']		= $shipping['code'];
		$data['shipping_methods']	= $shipping_methods;

		$this->form_validation->set_rules('shipping_notes', 'lang:shipping_information', 'trim|xss_clean');
		$this->form_validation->set_rules('shipping_method', 'lang:shipping_method', 'trim|callback_validate_shipping_option');
		//echo "<pre>shipping"; print_r($_POST); exit;
		if($this->form_validation->run() == false)
		{
			// echo "<pre>fdgtth "; print_r($data); exit;
			$this->view('checkout/shipping_form', $data);
		}
		else
		{	
			// echo "<pre> else "; print_r($data); exit;
			/* we have shipping details! */
			$this->go_cart->set_additional_detail('shipping_notes', $this->input->post('shipping_notes'));
			

			/* parse out the shipping information */
			$shipping_method	= json_decode($this->input->post('shipping_method'));
			$shipping_code		= md5($this->input->post('shipping_method'));
   // print_r($shipping_method); exit;



			/* set shipping info */
			$this->go_cart->set_shipping($shipping_method[0], $shipping_method[1]->num, $shipping_code);


			redirect('checkout/step_4');
		}
	}

	/*
		callback for shipping form 
		if callback is true then it's being called for form_Validation
		In that case, set the message otherwise just return true or false
	*/
		function validate_shipping_option($str, $callback=true)
		{
			$shipping_methods	= $this->_get_shipping_methods();

			if($shipping_methods)
			{
				foreach($shipping_methods as $key=>$val)
				{
					$check	= json_encode(array($key, $val));
					if($str	== md5($check))
					{
						return $check;
					}
				}
			}

			/* if we get there there is no match and they have submitted an invalid option */
			$this->form_validation->set_message('validate_shipping_option', lang('error_invalid_shipping_method'));
			return FALSE;

		}

		private function _get_shipping_methods()
		{
			$shipping_methods	= array();
			/* do we need shipping? */

			$data['gift_card'] = '0';
			$data['product_in_cart'] = '0';
			$contents = $this->go_cart->contents();
			foreach($contents as $content){
				$name_da[] = $content['name_da'];
			}
			if(in_array('gift_card_DKK', $name_da) || in_array('gift_card_usd', $name_da) || in_array('gift_card_euro', $name_da)){
				$data['gift_card'] = '1';
			}else{
				$data['product_in_cart'] = '1';
			}

			if(config_item('require_shipping') && $data['product_in_cart'] == 1)
			{
			//echo "klsahkjhdkj"; //exit;
				/* do the cart contents require shipping? */
				if($this->go_cart->requires_shipping())
				{
					/* ok so lets grab some shipping methods. If none exists, then we know that shipping isn't going to happen! */
					foreach ($this->Settings_model->get('shipping_modules') as $shipping_method=>$order)
					{
					//echo "<pre>"; print_r($shipping_method);// exit;
						$this->load->add_package_path(APPPATH.'packages/shipping/'.$shipping_method.'/');
						/* eventually, we will sort by order, but I'm not concerned with that at the moment */
						$this->load->library($shipping_method);
						$shipping_methods	= array_merge($shipping_methods, $this->$shipping_method->rates());
					//echo "<pre>"; print_r($shipping_methods); //exit;
					}

					/*  Free shipping coupon applied ? */
					if($this->go_cart->is_free_shipping()) 
					{
						/*  add free shipping as an option, but leave other options in case they want to upgrade */
						$shipping_methods[lang('free_shipping_basic')] = "0.00";
					}

				//echo "<pre>"; print_r($_POST);

					if($this->session->userdata('location_rate'))
					{
						$location_rate = array('Location Rate'=>$this->session->userdata('location_rate'));
					//echo "<pre>"; print_r($location_rate);
						$shipping_methods	= $location_rate;
					//echo "<pre>"; print_r($shipping_methods);
					}

				//echo "<pre>"; print_r($shipping_methods); exit;
					/*  format the values for currency display */
					foreach($shipping_methods as &$method)
					{
						/*  convert numeric values into an array containing numeric & formatted values */
						$method = array('num'=>$method,'str'=>format_currency($method));
					}
				//echo "<pre>"; print_r($method); //exit;
				}
			}
			if(!empty($shipping_methods))
			{
				/* everything says that shipping is required! */
				return $shipping_methods;
			//echo "<pre>"; print_r($shipping_methods); 

			}
			else
			{
				return false;
			}
		//exit;
		}

		function step_3()
		{
		/*
		Some error checking
		see if we have the billing address
		*/
		$customer	= $this->go_cart->customer();
		if(empty($customer['bill_address']))
		{
			redirect('checkout/step_1');
		}

		/* see if shipping is required and set. */
		// if(config_item('require_shipping') && $this->go_cart->requires_shipping() && $this->_get_shipping_methods())
		// {
		// 	$code	= $this->validate_shipping_option($this->go_cart->shipping_code());

		// 	if(!$code)
		// 	{
		// 		redirect('checkout/step_2');
		// 	}
		// }

		//echo "<pre>"; print_r($this->_get_payment_methods()); exit;
		if($payment_methods = $this->_get_payment_methods())
		{
			//echo "<pre>"; print_r($this->_get_payment_methods()); exit;
			$this->payment_form($payment_methods);
		}
		/* now where? continue to step 4 */
		else
		{
			$this->step_4();
		}
	}
	public function payment_form()
	{
		// echo 'found';	
		// exit;
		$payment_methods = $this->_get_payment_methods();

		/* find out if we need to display the shipping */
		$data['customer']			= $this->go_cart->customer();
		$data['shipping_method']	= $this->go_cart->shipping_method();

		/* are the being bounced back? */
		$data['payment_method']		= $this->go_cart->payment_method();

		/* pass in the payment methods */
		$data['payment_methods']	= $payment_methods;

		/* require that a payment method is selected */
		$this->form_validation->set_rules('module', 'lang:payment_method', 'trim|required|xss_clean|callback_check_payment');

		$module = $this->input->post('module');
		if($module)
		{
			$this->load->add_package_path(APPPATH.'packages/payment/'.$module.'/');
			$this->load->library($module);
		}

		if($this->form_validation->run() == false)
		{	
			//echo "<pre>validatio:false:"; print_r($data); exit;
			$this->load->view('checkout/payment_form', $data);
		}
		else
		{	
			// echo "<pre>validatio:true:"; print_r($data); exit;
			$this->go_cart->set_payment( $module, $this->$module->description() );
			redirect('checkout/step_4');
		}
	}

	/* callback that lets the payment method return an error if invalid */
	function check_payment($module)
	{
		$check	= $this->$module->checkout_check();

		if(!$check)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_payment', $check);
			return false;
		}
	}

	private function _get_payment_methods()
	{
		$payment_methods	= array();
		if($this->go_cart->total() != 0)
		{
			foreach ($this->Settings_model->get('payment_modules') as $payment_method=>$order)
			{
				$this->load->add_package_path(APPPATH.'packages/payment/'.$payment_method.'/');
				$this->load->library($payment_method);

				$payment_form = $this->$payment_method->checkout_form();

				if(!empty($payment_form))
				{
					$payment_methods[$payment_method] = $payment_form;
				}
			}
		}
		if(!empty($payment_methods))
		{
			return $payment_methods;
		}
		else
		{
			return false;
		}
	}

	function step_4()
	{

		/*required for gift card purchase*/
		$data['gift_card'] = '0';
		$data['product_in_cart'] = '0';
		$contents = $this->go_cart->contents();

		foreach($contents as $content){
			$name_da[] = $content['name_da'];
			if('gift_card_DKK' == $content['name_da'] || 'gift_card_usd' == $content['name_da'] || 'gift_card_euro' == $content['name_da']){
				$this->form_validation->set_rules('to_email', 'Recipent mail', 'trim|required|valid_mail');
				$this->form_validation->set_rules('to_name', 'Recipent\'s name', 'trim|required|valid_mail');
				$data['gift_card'] = '1';
			}else{
				$data['product_in_cart'] = '1';
			}
		}
		/* get addresses */
		$data['customer']		= $this->go_cart->customer();

		$data['shipping_method']	= $this->go_cart->shipping_method();

		$data['payment_method']		= $this->go_cart->payment_method();


		/* Confirm the sale */
		$this->view('checkout/confirm', $data);
			//$this->place_order();
	}

	function login()
	{
		$this->customer_model->is_logged_in('checkout');
	}

	function register()
	{
		$this->customer_model->is_logged_in('checkout', 'auth/register');
	}

	function place_order()
	{		
		// retrieve the payment method
			//$payment 			= array(
			//	'module' => 'paypal_express',
			//	'description' => 'PayPal Express'
			//	);

			//$payment 			= array(
			//	'module' => 'cod',
			//	'description' => 'Cash On Delivery'
			//	);
		$payment 			= $this->go_cart->payment_method();
		$payment_methods	= $this->_get_payment_methods();

		//exit;
		//make sure they're logged in if the config file requires it
		if($this->config->item('require_login'))
		{
			$this->customer_model->is_logged_in();
		}

		// are we processing an empty cart?
		$contents = $this->go_cart->contents();

		if(empty($contents))
		{
			redirect('cart/view_cart');

		} else {
			//  - check to see if we have a payment method set, if we need one
			if(empty($payment) && $this->go_cart->total() > 0 && (bool)$payment_methods == true)
			{					
				// redirect('checkout/step_3');
				redirect('checkout');
			}
		}

		if(!empty($payment) && (bool)$payment_methods == true)
		{
			//load the payment module
			$this->load->add_package_path(APPPATH.'packages/payment/'.$payment['module'].'/');
			$this->load->library($payment['module']);

			// Is payment bypassed? (total is zero, or processed flag is set)
			if($this->go_cart->total() > 0 && ! isset($payment['confirmed'])) {
				//run the payment
				$error_status	= $this->$payment['module']->process_payment();

				if($error_status !== false)
				{
					// send them back to the payment page with the error
					$this->session->set_flashdata('error', $error_status);
					// redirect('checkout/step_3');
					redirect('checkout');
				}
			}
		}


		// save the order
		$order_id = $this->go_cart->save_order();
		foreach($contents as $content){
			if($content['display_name_da'] == 'gift card '){

				$price = explode('_',$content['name_en']);
//                        $this->load->library('email');
				$gift_card_data = $this->session->userdata('gift_card');
				$customer_data = $this->session->userdata('customer');
				$this->email->from($customer_data['bill_address']['email'], $customer_data['firstname'].' '.$customer_data['last_name']);
				$this->email->to($gift_card_data['to_email']);
				$this->email->subject('Gift Card For You From '. $customer_data['firstname'].' '.$customer_data['last_name']);
				$this->email->message('Testing the email class.');

//                        $this->load->library('email');

			$save['code'] = generate_code(); // from the string helper
			$save['to_email'] = $gift_card_data['to_email'];
			$save['to_name'] = $gift_card_data['to_name'];
			$save['from'] = $customer_data['bill_address']['email'];
			$save['personal_message'] = $gift_card_data['to_name'];
			$save['beginning_amount'] = $content['price'];
			$save['activated'] = 1;
			$this->gift_card_model->save_card($save);
			$save['beginning_amount'] = $price[3]." ".$price[0];

				//get the canned message for gift cards
			$row = $this->db->where('id', '1')->get('tbl_canned_messages')->row_array();

				// set replacement values for subject & body
			$row['subject']	= str_replace('{from}', $save['from'], $row['subject']);
			$row['subject']	= str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);

			$row['content']	= str_replace('{code}', $save['code'], $row['content']);
			$row['content']	= str_replace('{amount}', $save['beginning_amount'], $row['content']);
			$row['content']	= str_replace('{from}', $save['from'], $row['content']);
			$row['content']	= str_replace('{personal_message}', nl2br($save['personal_message']), $row['content']);
			$row['content']	= str_replace('{url}', $this->config->item('base_url'), $row['content']);
			$row['content']	= str_replace('{site_name}', $this->config->item('company_name'), $row['content']);

			$this->load->library('email');

			$config['mailtype'] = 'html';

			/*$this->email->initialize($config);
			$this->email->from($this->config->item('email'));
			$this->email->to($save['to_email']);

			$this->email->subject($row['subject']);
			$this->email->message($row['content']);
			$this->email->send();*/

			
			$this->session->set_flashdata('message', lang('message_saved_giftcard'));
		}
	}

	$data['order_id']			= $order_id;
	$data['shipping']			= $this->go_cart->shipping_method();
	$data['payment']			= $this->go_cart->payment_method();
	$data['customer']			= $this->go_cart->customer();
	$data['shipping_notes']		= $this->go_cart->get_additional_detail('shipping_notes');
	$data['referral']			= $this->go_cart->get_additional_detail('referral');

	$order_downloads 			= $this->go_cart->get_order_downloads();

	$data['hide_menu']			= true;

		// run the complete payment module method once order has been saved
	if(!empty($payment))
	{
		if(method_exists($this->$payment['module'], 'complete_payment'))
		{
			$this->$payment['module']->complete_payment($data);
		}
	}
	
		// Send the user a confirmation email

		// - get the email template
	$this->load->model('messages_model');
	$row = $this->messages_model->get_message(7);

	$download_section = '';
	if( ! empty($order_downloads))
	{
			// get the download link segment to insert into our confirmations
		$downlod_msg_record = $this->messages_model->get_message(8);

		if(!empty($data['customer']['id']))
		{
				// they can access their downloads by logging in
			$download_section = str_replace('{download_link}', anchor(site_url('account/my_downloads'), lang('download_link')),$downlod_msg_record['content']);
		} else {
				// non regs will receive a code
			$download_section = str_replace('{download_link}', anchor(site_url('account/my_downloads').'/'.$order_downloads['code'], lang('download_link')), $downlod_msg_record['content']);
		}
	}

	$row['content'] = html_entity_decode($row['content']);

		// set replacement values for subject & body
		// {customer_name}
	$row['subject'] = str_replace('{customer_name}', $data['customer']['firstname'].' '.$data['customer']['lastname'], $row['subject']);
	$row['content'] = str_replace('{customer_name}', $data['customer']['firstname'].' '.$data['customer']['lastname'], $row['content']);

		// {url}
	$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
	$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);

		// {site_name}
	$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
	$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);

		// {order_summary}
	$row['content'] = str_replace('{order_summary}', $this->load->view('order_email', $data, true), $row['content']);

		// {download_section}

	$row['content'] = str_replace('{download_section}', $download_section, $row['content']);
	$row['content'] = str_replace('{customer_email}', $data['customer']['email'], $row['content']);
	$row['content'] = str_replace('{customer_address}', $data['customer']['bill_address']['country'].', '.$data['customer']['bill_address']['city'].', '.$data['customer']['bill_address']['address1'], $row['content']);

	$this->load->library('email');

	$config['charset'] = 'iso-8859-1';
	$config['wordwrap'] = TRUE;
	$config['mailtype'] = 'html';

	/*local setting*/
	$config['protocol'] = 'smtp';
	$config['smtp_host'] = 'smtp.vianet.com.np';

	$this->email->initialize($config);

	$this->email->from($this->config->item('email'), $this->config->item('company_name'));

	//local setting
	$this->email->to('anish@pagodalabs.com');
	/*if($this->customer_model->is_logged_in(false, false))
	{
		$this->email->to($data['customer']['email']);
	}
	else
	{
		$this->email->to($data['customer']['ship_address']['email']);
	}*/

	//email the admin
	//comment for debug
	//$this->email->bcc($this->config->item('email'));
	$this->email->bcc('niroj@pagodalabs.com, garaknoe@gmail.com');

	$this->email->subject($row['subject']);
	$this->email->message($row['content']);

	$this->email->send();

	$data['page_title'] = 'Thanks for shopping with '.$this->config->item('company_name');
	$data['gift_cards_enabled'] = $this->gift_cards_enabled;
	$data['download_section']	= $download_section;


	/*  get all cart information before destroying the cart session info */
	$data['go_cart']['group_discount']      = $this->go_cart->group_discount();
	$data['go_cart']['subtotal']            = $this->go_cart->subtotal();
	$data['go_cart']['coupon_discount']     = $this->go_cart->coupon_discount();
	$data['go_cart']['order_tax']           = $this->go_cart->order_tax();
	$data['go_cart']['discounted_subtotal'] = $this->go_cart->discounted_subtotal();
	$data['go_cart']['shipping_cost']       = $this->go_cart->shipping_cost();
	$data['go_cart']['gift_card_discount']  = $this->go_cart->gift_card_discount();
	$data['go_cart']['total']               = $this->go_cart->total();
	$data['go_cart']['contents']            = $this->go_cart->contents();

	/* remove the cart from the session */
	$this->go_cart->destroy();
	// $this->session->unset_userdata('cart_contents');
	/*  show final confirmation page */
	$this->view('order_placed', $data);
}

public function getLocationRate($country_id)
{
	$this->load->model('rates/location_rate_model');
	$this->load->model('Settings_model');

	$total_cart =  $this->go_cart->total();

	$this->db->where('location_id',$this->input->post('country_id'));
	$this->db->order_by('range_max desc');
	$rates = $this->location_rate_model->getLocationRates()->result_array();


	$happyShipping = $this->Settings_model->get('holiday_shipping');

	foreach ($rates as $key => $value) {

		if($happyShipping['enabled'] == 1) {
			if( $total_cart >= $happyShipping['range_max']) {
				$location_rate = 0; 
				break;
			}
		}

		if( $total_cart >= $value['range_max']) {
			$location_rate = $value['rate']; 
			break;
		} else {
			/*($value['range_max'] == NULL)*/
			$location_rate = $value['rate'];
		}

	}

	// $rate_id = $this->input->post('rate_id');

	// $row = $this->location_rate_model->get_location_rate_by_id($rate_id);

	/*$rates = $this->location_rate_model->getLocationRates()->result_array();

	foreach($rates as $rate)
	{
		if($country_id == $rate['location_id'])
		{
			$location_rate = $rate['rate'];
			break;
		}
		else
		{	
			if($rate['location_id'] == 'others')
			{
				$location_rate = $rate['rate'];	
			}	

		}		
	}*/

	// echo json_encode(array('row'=>$row));
	return $location_rate;
}

public function registerAccount()
{		
	$this->load->library('encrypt');
	$save = array();
	$save['id']		= false;

	$save['firstname']			= $this->input->post('firstname');
	$save['lastname']			= $this->input->post('lastname');
	$save['email']				= $this->input->post('email');
	$save['phone']				= $this->input->post('phone');
	$save['company']			= $this->input->post('company');
	$save['active']				= $this->config->item('new_customer_status');

	$new_password       = random_string('alnum', 8);
	$save['password']   = sha1($new_password);
	$save['raw_password']   = $new_password;

	$id = $this->customer_model->save($save);

	$this->send_email($save);

	return $id;
}

private function send_email($user)
{
	/* send an email */
			// get the email template
	$subject ='Thank you for registering at '.$this->config->item('company_name');
	$message = '<p>Dear '.$user['firstname'].' '. $user['lastname'].',</p><p>Thanks for registering at .'.$this->config->item('company_name').' Your participation is appreciated. You may access your account by using the email address this notice was sent to, and the password '.$user['raw_password'].' which we generated it for you. If you forget your password, on the login page, click the "forgot password" link and you can get a new password generated and sent to you.</p><p>Thanks,<br>'.$this->config->item('company_name').'</p>';

	$this->load->library('email');

	$config['mailtype'] = 'html';
			// $config['protocol'] = 'smtp';
			// $config['smtp_host'] = 'smtp.ntc.net.np';

	$this->email->initialize($config);

	$this->email->clear(TRUE);
	
	$this->email->from($this->config->item('email'), $this->config->item('company_name'));
	$this->email->to($user['email']);
	$this->email->bcc($this->config->item('email'));
	$this->email->subject($subject);
	$this->email->message(html_entity_decode($message));

	$this->email->send();

			//$this->session->set_flashdata('message', sprintf( lang('registration_thanks'), $user['firstname'] ) );

}



}