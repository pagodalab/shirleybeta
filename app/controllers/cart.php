<?php

class Cart extends Front_Controller {
	
	function add()
	{
//            echo '<pre>';
//            print_r($this->input->post());exit;
		/*$redirect_url = ($this->input->get('redirect_url'))?$this->input->get('redirect_url'):'';
		//echo "<pre>"; print($redirect_url); 
		$this->session->set_userdata('redirect_url',$redirect_url);
		if(!$this->customer_model->is_logged_in($redirect_url,false))
		{
			redirect(site_url('auth/login'));
		}
		else
		{*/
			//echo "<pre>"; print_r($_POST); exit;
			// Get our inputs
			
				//  echo '<pre>';
    //           print_r($this->input->post());exit;
			$product_id			= $this->input->post('id');
			$quantity 			= $this->input->post('quantity');
			if($this->input->post('color'))
			{
				$color 				= $this->input->post('color');
			}
			else
			{
				$color = NULL;
			}
			
			if($this->input->post('size'))
			{
					$size 				= $this->input->post('size');
				$this->load->model('size/size_model');				
		        $size_all =$this->size_model->getSizes(array('size_id'=>$size))->row_array();
		        $size_name = $size_all['size_title_en'];
			}
			
			
		


			/*$delivery_date 			= $this->input->post('delivery_date'); //For cake message goes on order item
			$product_message 		= $this->input->post('product_message');
			$cake_message 			= $this->input->post('cake_message'); //This goes on order table for cake message*/
			
			$post_options 		= $this->input->post('option');
			
			//echo "<pre>"; print_r($_POST); exit;
			
			// Get a cart-ready product array
			$product = $this->product_model->get_cart_ready_product($product_id, $quantity);
			
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
			if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
			{
				$stock	= $this->product_model->get_product($product_id);
				
				//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
				$items		= $this->go_cart->contents();
				$qty_count	= $quantity;
				foreach($items as $item)
				{
					if(intval($item['id']) == intval($product_id))
					{
						$qty_count = $qty_count + $item['quantity'];
					}
				}
				
				if($stock->quantity < $qty_count)
				{
					//we don't have this much in stock
					$this->session->set_flashdata('error', sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity));
					$this->session->set_flashdata('quantity', $quantity);
					$this->session->set_flashdata('option_values', $post_options);

					redirect($this->product_model->get_slug($product_id));
				}
			}

			// Validate Options 
			// this returns a status array, with product item array automatically modified and options added
			//  Warning: this method receives the product by reference
			$status = $this->option_model->validate_product_options($product, $post_options);

			
			// don't add the product if we are missing required option values
			if( ! $status['validated'])
			{
				$this->session->set_flashdata('quantity', $quantity);
				$this->session->set_flashdata('error', $status['message']);
				$this->session->set_flashdata('option_values', $post_options);

				redirect($this->product_model->get_slug($product_id));

			} else {

				//Add the original option vars to the array so we can edit it later
				$product['post_options']	= $post_options;
				$product['color']	=	$color;
				
				if($this->input->post('size'))
				{
					$product['size']	= 	$size;
					$product['size_name'] = $size_name;
				}
				else
				{
					$product['size']	= 	NULL;
				}
				
				/*$product['product_message'] = $product_message;
				$product['cake_message'] = $cake_message;
				$product['delivery_date'] = $delivery_date;*/
				
				
				//is giftcard
				$product['is_gc']			= false;
				
				// Add the product item to the cart, also updates coupon discounts automatically
				$success = $this->go_cart->insert($product);
				
				
				// go go gadget cart!
				//redirect('cart/view_cart');

				echo json_encode(array('success'=>$success));
			}
		//}
		}

		function view_cart()
		{
			/*required for gift card purchase*/
			$data['gift_card'] = '0';
			$data['product_in_cart'] = '0';
			$contents = $this->go_cart->contents();
			if(count($contents)>0){
				foreach($contents as $content){
					$name_da[] = $content['name_da'];
				}
				if(in_array('gift_card_DKK', $name_da) || in_array('gift_card_usd', $name_da) || in_array('gift_card_euro', $name_da)){
					$data['gift_card'] = '1';
				}else{
					$data['product_in_cart'] = '1';
				}
			}
			$this->load->model('size/size_model');
			$data['page_title']	= 'View Cart';
			$data['gift_cards_enabled'] = $this->gift_cards_enabled;

			$this->view('view_cart', $data);
		}

		function remove_item($key)
		{
		//drop quantity to 0
			$this->go_cart->update_cart(array($key=>0));

			redirect('cart/view_cart');
		}

		function update($redirect = false)
		{
		//if redirect isn't provided in the URL check for it in a form field

			if(!$redirect)
			{
				$redirect = $this->input->post('redirect');
			}

			

// 			echo "-------------------------------------------------------------------";
// 			print_r($redirect);
// 			exit;
		// see if we have an update for the cart
			$item_keys		= $this->input->post('cartkey');
			$coupon_code	= $this->input->post('coupon_code');
			$gc_code		= $this->input->post('gc_code');

			//echo "<pre> Item Keys "; print_r($item_keys); //exit;
			if($coupon_code==""){
				$this->session->set_flashdata('error', 'No coupon');
			}

			if($coupon_code)
			{
				$coupon_code = strtolower($coupon_code);
			}
			
		//get the items in the cart and test their quantities
			$items			= $this->go_cart->contents();
			$new_key_list	= array();
		//first find out if we're deleting any products
			
			if(is_array($item_keys)){
				foreach($item_keys as $key=>$quantity)
				{
					if(intval($quantity) === 0)
					{
				//this item is being removed we can remove it before processing quantities.
				//this will ensure that any items out of order will not throw errors based on the incorrect values of another item in the cart
						$this->go_cart->update_cart(array($key=>$quantity));
					}
					else
					{
				//create a new list of relevant items
						$new_key_list[$key]	= $quantity;
					}
				}
			}

		//echo "<pre> NEw Item Keys "; print_r($new_key_list); exit;

			$response	= array();
			foreach($new_key_list as $key=>$quantity)
			{
				$product	= $this->go_cart->item($key);
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
				if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
				{
					$stock	= $this->product_model->get_product($product['id']);

				//loop through the new quantities and tabluate any products with the same product id
					$qty_count	= $quantity;
					foreach($new_key_list as $item_key=>$item_quantity)
					{
						if($key != $item_key)
						{
							$item	= $this->go_cart->item($item_key);
						//look for other instances of the same product (this can occur if they have different options) and tabulate the total quantity
							if($item['id'] == $stock->id)
							{
								$qty_count = $qty_count + $item_quantity;
							}
						}
					}
					if($stock->quantity < $qty_count)
					{
						if(isset($response['error']))
						{
							$response['error'] .= '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
						}
						else
						{
							$response['error'] = '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
						}
					}
					else
					{
					//this one works, we can update it!
					//don't update the coupons yet
						$this->go_cart->update_cart(array($key=>$quantity));
					}
				}
				else
				{
					$this->go_cart->update_cart(array($key=>$quantity));
				}
			}

		//if we don't have a quantity error, run the update
			if(!isset($response['error']))
			{
			//update the coupons and gift card code
				$response = $this->go_cart->update_cart(false, $coupon_code, $gc_code);
			// set any messages that need to be displayed
			}
			else
			{
				exit;
				//$response['error'] = '<p>'.lang('error_updating_cart').'</p>'.$response['error'];
			}


		//check for errors again, there could have been a new error from the update cart function
			if(isset($response['error']))
			{
				$this->session->set_flashdata('error', $response['error']);
			}
			if(isset($response['message']))
			{
				$this->session->set_flashdata('message', $response['message']);
			}

			if($redirect)
			{
				$cart_content = $this->session->userdata('cart_contents');				
				unset($cart_content['customer']['bill_address']);
				unset($cart_content['customer']['ship_address']);
				$this->session->set_userdata('cart_contents',$cart_content);
				redirect($redirect);
			}
			else
			{
				redirect('cart/view_cart');
			}
		}


	/***********************************************************
			Gift Cards
			 - this function handles adding gift cards to the cart
			 ***********************************************************/

			 function giftcard()
			 {
			 	if(!$this->gift_cards_enabled) redirect('/');

		// Load giftcard settings
			 	$gc_settings = $this->Settings_model->get("gift_cards");

			 	$this->load->library('form_validation');

			 	$data['allow_custom_amount']	= (bool) $gc_settings['allow_custom_amount'];
			 	$data['preset_values']			= explode(",",$gc_settings['predefined_card_amounts']);

			 	if($data['allow_custom_amount'])
			 	{
			 		$this->form_validation->set_rules('custom_amount', 'lang:custom_amount', 'numeric');
			 	}

			 	$this->form_validation->set_rules('amount', 'lang:amount', 'required');
			 	$this->form_validation->set_rules('preset_amount', 'lang:preset_amount', 'numeric');
			 	$this->form_validation->set_rules('gc_to_name', 'lang:recipient_name', 'trim|required');
			 	$this->form_validation->set_rules('gc_to_email', 'lang:recipient_email', 'trim|required|valid_email');
			 	$this->form_validation->set_rules('gc_from', 'lang:sender_email', 'trim|required');
			 	$this->form_validation->set_rules('message', 'lang:custom_greeting', 'trim|required');

			 	if ($this->form_validation->run() == FALSE)
			 	{
			 		$data['error']				= validation_errors();
			 		$data['page_title']			= lang('giftcard');
			 		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
			 		$this->view('giftcards', $data);
			 	}
			 	else
			 	{

			// add to cart

			 		$card['price'] = set_value(set_value('amount'));

			$card['id']				= -1; // just a placeholder
			$card['sku']			= lang('giftcard');
			$card['base_price']		= $card['price']; // price gets modified by options, show the baseline still...
			$card['name']			= lang('giftcard');
			$card['code']			= generate_code(); // from the string helper
			$card['excerpt']		= sprintf(lang('giftcard_excerpt'), set_value('gc_to_name'));
			$card['weight']			= 0;
			$card['quantity']		= 1;
			$card['shippable']		= false;
			$card['taxable']		= 0;
			$card['fixed_quantity'] = true;
			$card['is_gc']			= true; // !Important
			$card['track_stock']	= false; // !Imporortant
			
			$card['gc_info'] = array("to_name"	=> set_value('gc_to_name'),
				"to_email"	=> set_value('gc_to_email'),
				"from"		=> set_value('gc_from'),
				"personal_message"	=> set_value('message')
				);
			
			// add the card data like a product
			$this->go_cart->insert($card);
			
			redirect('cart/view_cart');
		}
	}
	
	function add_canvas_products()
	{
		//$redirect_url = ($this->input->get('redirect_url'))?$this->input->get('redirect_url'):'';
		//echo "<pre>"; print($redirect_url); 
		//$this->session->set_userdata('redirect_url',$redirect_url);
		//if(!$this->customer_model->is_logged_in($redirect_url,false))
		//{
			//redirect(site_url('auth/login'));
		//}
		//else
		//{
			//echo "<pre>"; print_r($_POST); //exit;
		$options = $this->input->post('option');
		$products = $this->input->post('id');
			//echo "<pre>"; print_r($options);
		foreach($products as $k=>$v)
		{

			if(array_key_exists($v,$options))
			{
				$merged[] = array('id'=>$v,'options'=>$options[$v]);
			}
			else
			{
				$merged[] = array('id'=>$v,'options'=>'');
			}
		}

			//$filtered_array = array_map(null,$products,$options);
			//echo  "<pre>"; print_r($merged); //exit;
		foreach($merged as $merge)
		{
				// Get our inputs
			$product_id			= $merge['id'];
			$quantity 			= 1;

				/*$delivery_date 		= $this->input->post('delivery_date'); //For cake message goes on order item
				$product_message 		= $this->input->post('product_message');
				$cake_message 			= $this->input->post('cake_message'); //This goes on order table for cake message*/
				
				$post_options 		= $merge['options'];
				
				//echo "<pre> product "; print_r($product_id);
				//echo "<pre> post options "; print_r($post_options); 
				
				// Get a cart-ready product array
				$product = $this->product_model->get_cart_ready_product($product_id, $quantity);
				
				//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
				if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
				{
					$stock	= $this->product_model->get_product($product_id);
					
					//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
					$items		= $this->go_cart->contents();
					$qty_count	= $quantity;
					foreach($items as $item)
					{
						if(intval($item['id']) == intval($product_id))
						{
							$qty_count = $qty_count + $item['quantity'];
						}
					}
					
					if($stock->quantity < $qty_count)
					{
						//we don't have this much in stock
						$this->session->set_flashdata('error', sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity));
						$this->session->set_flashdata('quantity', $quantity);
						$this->session->set_flashdata('option_values', $post_options);

						redirect($this->product_model->get_slug($product_id));
					}
				}

				// Validate Options 
				// this returns a status array, with product item array automatically modified and options added
				//  Warning: this method receives the product by reference
				$status = $this->option_model->validate_product_options($product, $post_options);

				// don't add the product if we are missing required option values
				if( ! $status['validated'])
				{
					$this->session->set_flashdata('quantity', $quantity);
					$this->session->set_flashdata('error', $status['message']);
					$this->session->set_flashdata('option_values', $post_options);

					redirect($this->product_model->get_slug($product_id));

				} else {

					//Add the original option vars to the array so we can edit it later
					$product['post_options']	= $post_options;
					/*$product['product_message'] = $product_message;
					$product['cake_message'] = $cake_message;
					$product['delivery_date'] = $delivery_date;*/
					
					
					//is giftcard
					$product['is_gc']			= false;
					
					// Add the product item to the cart, also updates coupon discounts automatically
					$this->go_cart->insert($product);

				}
			}
			//exit;
			//echo "<pre> product "; print_r($product); exit;
			// go go gadget cart!
			redirect('cart/view_cart');
		}
	//}
	}