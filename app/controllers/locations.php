<?php

class Locations extends MY_Controller {	
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('location/location_model');
		
	}
	
	function get_zone_menu()
	{
		$id	= $this->input->post('id');
		$zones	= $this->location_model->get_zones_menu($id);
		
		foreach($zones as $id=>$z):?>
		
		<option value="<?php echo $id;?>"><?php echo $z;?></option>
		
		<?php endforeach;
	}
}