<?php

class Dashboard extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		if($this->authenticate->check_access('Orders'))
		{
			redirect('orders/admin');
		}
		else if($this->authenticate->check_access('Staff'))
		{
			redirect('staff/admin');
		}
		
		$this->load->model('order/order_model');
		$this->load->model('customer/customer_model');
		$this->load->helper('date');
		
		$this->lang->load('dashboard');
	}
	
	function index()
	{
		//check to see if shipping and payment modules are installed
		$data['payment_module_installed']	= (bool)count($this->Settings_model->get('payment_modules'));
		$data['shipping_module_installed']	= (bool)count($this->Settings_model->get('shipping_modules'));
		
		$data['page_title']	=  lang('dashboard');
		
		// get 5 latest orders
		$data['orders']	= $this->order_model->get_orders(false, '' , 'DESC', 5);

		// get 5 latest customers
		$data['customers'] = $this->customer_model->get_customers(5);
				
		
		$this->view($this->config->item('admin_folder').'/dashboard', $data);
	}

}