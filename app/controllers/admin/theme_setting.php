<?php

class Theme_setting extends Admin_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->authenticate->check_access('Admin', true);
		$this->load->model('Settings_model');
		$this->lang->load('settings');
		$this->load->helper('inflector');
		$this->load->library('session');
	}
	
	function index()
	{
		//now time to do it again with shipping
        $christmas     = $this->Settings_model->get('christmas');
        // $enabled_modules    = $this->Settings_model->get('shipping_modules');
        // print_r($christmas); exit;
        $data['christmas']   = $christmas;
      
        $data['page_title'] = 'Christmas Setting';
        $this->view($this->config->item('admin_folder').'/theme_setting', $data);
	}
	
	function settings($module)
	{
		$this->db->set('setting',$module);
		$this->db->where('id',308);
		$this->db->update('tbl_settings');
            $this->session->set_flashdata('message', 'Christmas Theme Changed Sucessfully');
        // $this->session->set_userdata('sucess_msg','');
		redirect($_SERVER['HTTP_REFERER']);
	}
}
