<?php

class pp_standard extends Front_Controller  {

	function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'packages/payment/paypal_standard/');
		$this->load->library('go_cart');
		$this->load->helper('form_helper');
		
	}
	

	function index()
	{
		//we don't have a default landing page
		redirect('');
		
	}
		
	/* 
	   Receive postback confirmation from paypal
	   to complete the customer's order.
	*/
	function pp_return()
	{
				
		// Verify the transaction with paypal
		

		// Process the results
		
			// The transaction is good. Finish order
			
			// set a confirmed flag in the gocart payment property
			$this->go_cart->set_payment_confirmed();
			
			// send them back to the cart payment page to finish the order
			// the confirm flag will bypass payment processing and save up
			redirect('checkout/place_order/');			
			
		
	}
	
	/* 
		Customer cancelled paypal payment
		
	*/
	function pp_cancel()
	{
		//make sure they're logged in if the config file requires it
		if($this->config->item('require_login'))
		{
			$this->Customer_model->is_logged_in();
		}
	
		// User canceled using paypal, send them back to the payment page
		$cart  = $this->session->userdata('cart');	
		$this->session->set_flashdata('message', "<div>Paypal transaction canceled, select another payment method</div>");
		redirect('checkout');
	}

	function pp_ipn(){
	
		/*$post_string = '';	 
		   if ($this->paypal_lib->validate_ipn_curl())
		   {
		
			$paypal_data=$this->paypal_lib->ipn_data;
			$data="";
		
			foreach ($paypal_data as $field => $value)
			{ 
				
				$data.= $field.' = '.$value ."\r\n"; 
			}
			$fp=fopen("paypal_test.txt","w+");
			fwrite($fp,$data);
			fclose($fp);
		}*/
		$this->paypal_lib->validate();
	}
}