<?php

class pp_gate extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	//	$this->load->model('Settings_model');
		$this->load->add_package_path(APPPATH.'packages/payment/paypal_express/');
		$this->load->library(array('paypal', 'httprequest', 'go_cart'));
		$this->load->helper('form_helper');
	}
	

	function index()
	{
	    echo "ashdkasjhd"; exit;
		//we don't have a default landing page
		redirect('');
		
	}
		
	/* 
	   Receive postback confirmation from paypal
	   to complete the customer's order.
	*/
	function pp_return()
	{
				
		// Verify the transaction with paypal
		$final = $this->paypal->doPayment();

		// Process the results
		if ($final['ACK'] == 'Success') {
			// The transaction is good. Finish order
			
			// set a confirmed flag in the gocart payment property
			$this->go_cart->set_payment_confirmed();
			
			// send them back to the cart payment page to finish the order
			// the confirm flag will bypass payment processing and save up
			redirect('checkout/place_order/');			
			
		} else {
			// Possible fake request; was not verified by paypal. Could be due to a double page-get, should never happen under normal circumstances
			$this->session->set_flashdata('message', "<div>Paypal did not validate your order. Either it has been processed already, or something else went wrong. If you believe there has been a mistake, please contact us.</div>");
			redirect('checkout');
		}
	}
	
	/* 
		Customer cancelled paypal payment
		
	*/
	function pp_cancel()
	{
	   
		//make sure they're logged in if the config file requires it
		if($this->config->item('require_login'))
		{
			$this->Customer_model->is_logged_in();
		}
	
		// User canceled using paypal, send them back to the payment page
		$cart  = $this->session->userdata('cart');	
		$this->session->set_flashdata('message', "<div>Paypal transaction canceled, select another payment method</div>");
		redirect('checkout');
	}
	
	function pp_ipn()
	{

		   set_time_limit(0);
		   $this->load->library('parser');		
		   $to    = 'sajan@pagodalabs.com';
		   
		   //$file = fopen("PayaplKotest.txt","w"); 
		
		   if ($this->paypal_lib->validate())
		   {
		
			$paypal_data=$this->paypal_lib->ipn_data;
			
			$order_info=explode('|',$paypal_data['custom']);
			
			
			$paypal_data['order_by_email']=$order_info['0'];
			$paypal_data['order_by']=$order_info['1'];
			// $paypal_data['price']=$order_info['2'];
			$paypal_data['order_date']=$order_info['2'];
			// $paypal_data['product_id'] = $order_info['4'];
			
			
			$parse_data=array('PRODUCT_ID'=>$paypal_data['item_number'],
							  'PRODUCT_NAME'=>$paypal_data['item_name'],
							  'ORDER_BY'=>$paypal_data['order_by'],
							  'ORDER_BY_EMAIL'=>$paypal_data['order_by_email'],
							  'PRICE'=>$paypal_data['amount'],							  
							  'QUANTITY'=>$paypal_data['quantity'],
							  'ORDER_DATE'=>$paypal_data['order_date'],
							  'TXN_ID'=>$paypal_data['txn_id'],
							  'TOTAL'=>$paypal_data['mc_gross']);

			
			 // $this->_update_booking($paypal_data);
			 
			  $subject = $this->config->item('company_name').' (Received Payment) for '.$paypal_data['item_name'];
			  
			  $body=$this->load->view('paypal/order_template',NULL,TRUE);
			  
			  $body=$this->parser->parse_string($body,$parse_data,TRUE);
			  
	
			  // load email lib and email results
		
			$this->email->set_mailtype('html');
			$this->email->to($to);
			
			$this->email->from($this->paypal_lib->ipn_data['payer_email'], $this->paypal_lib->ipn_data['payer_name']);
			$this->email->subject($subject);
			$this->email->message($body);	
			
			$this->email->send();
			
			$this->email->clear();
			//Customer email
			$body=$this->load->view('paypal/customer_template',NULL,TRUE);
			
			$body=$this->parser->parse_string($body,$parse_data,TRUE);
			
			$this->email->set_mailtype('html');
			$this->email->to($paypal_data['order_by_email']);
			$this->email->from($this->config->item('email'), $this->config->item('company_name'));
			$this->email->subject($this->config->item('company_name').' - '.$paypal_data['item_name']);
			$this->email->message($body);	
			//$this->email->attach('tickets/'.$ticket_file_name);
			$this->email->send();	

		
		   }
	}


}