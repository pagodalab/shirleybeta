<?php if($christmas): ?>
	<table class="table table-striped">
		<tbody>
		
			<tr>
				<td>Christmas Theme</td>
				<td>
					<span class="btn-group pull-right">
				<?php if($christmas['enable'] == 1): ?>
					<a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="<?php echo site_url($this->config->item('admin_folder').'/theme_setting/settings/0');?>" ><i class=" icon-minus icon-white"></i> Uninstall</a>
				<?php else: ?>
					<a class="btn" onclick="return confirm('Are you sure?')" href="<?php echo site_url($this->config->item('admin_folder').'/theme_setting/settings/1');?>"><i class="icon-ok"></i> <?php echo lang('install');?></a>
				<?php endif; ?>
					</span>
				</td>
			</tr>
		
		</tbody>
	</table>
<?php endif; ?>

<script type="text/javascript">
	setTimeout(function(){
       $('#flash_message').fadeOut();
       $('#flash_message').val('');

	},5000)
</script>