<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php print $this->bep_site->get_metatags();?>
<title>Cart<?php echo (isset($page_title))?' :: '.$page_title:''; ?></title>

<link rel="icon" type="image/jpg" href="<?php echo theme_url('assets') ?>/img/star.png" />

<?php print $this->bep_site->get_variables();?>
<?php print $this->bep_assets->get_header_assets();?>
<?php print $this->bep_site->get_js_blocks();?>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<!-- <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="application/javascript"></script> -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker.min.css" type="text/css" />

<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.ui.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js" type="application/javascript"></script>



<?php if($this->authenticate->is_logged_in(false, false)):?>
    
<style type="text/css">
    body {
        margin-top:50px;
    }
    
    @media (max-width: 979px){ 
        body {
            margin-top:0px;
        }
    }
    @media (min-width: 980px) {
        .nav-collapse.collapse {
            height: auto !important;
            overflow: visible !important;
        }
     }
    
    .nav-tabs li a {
        text-transform:uppercase;
        background-color:#f2f2f2;
        border-bottom:1px solid #ddd;
        text-shadow: 0px 1px 0px #fff;
        filter: dropshadow(color=#fff, offx=0, offy=1);
        font-size:12px;
        padding:5px 8px;
    }
    
    .nav-tabs li a:hover {
        border:1px solid #ddd;
        text-shadow: 0px 1px 0px #fff;
        filter: dropshadow(color=#fff, offx=0, offy=1);
    }

</style>
<script type="text/javascript">
$(document).ready(function(){
   //$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('.redactor').redactor({
            minHeight: 200,
            imageUpload: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/upload_image');?>',
            fileUpload: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/upload_file');?>',
            imageGetJson: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/get_images');?>',
            imageUploadErrorCallback: function(json)
            {
                alert(json.error);
            },
            fileUploadErrorCallback: function(json)
            {
                alert(json.error);
            }
      });
});
</script>
<?php endif;?>
</head>
<body>
<?php if($this->authenticate->is_logged_in(false, false)):?>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <?php $admin_url = site_url($this->config->item('admin_folder')).'/';?>
            
            <a class="brand" href="<?php echo $admin_url;?>">Cart</a>
            
            <div class="nav-collapse">
                <ul class="nav">
                    <li><a href="<?php echo $admin_url;?>"><?php echo lang('common_home');?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('common_sales') ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('order/admin')?>"><?php echo lang('common_orders') ?></a></li>
                            
                            <?php if($this->authenticate->check_access('Admin')) : ?>
                            <!--<li><a href="<?php echo site_url('purchase_order/admin')?>"><?php echo lang('common_order_item') ?></a></li>-->
                            <li><a href="<?php echo site_url('customer/admin')?>"><?php echo lang('common_customers') ?></a></li>
                            <li><a href="<?php echo site_url('subscribe/admin')?>"><?php echo "Subscribe"//lang('common_customers') ?></a></li>
                            <!--<li><a href="<?php echo site_url('customer/admin/groups')?>"><?php echo lang('common_groups') ?></a></li>-->
                            <li><a href="<?php echo site_url('report/admin')?>"><?php echo lang('common_reports') ?></a></li>
                            <li><a href="<?php echo site_url('coupon/admin')?>"><?php echo lang('common_coupons') ?></a></li>
                            <li><a href="<?php echo site_url('giftcard/admin')?>"><?php echo lang('common_giftcards') ?></a></li>
<?php $admin = $this->session->userdata('admin');?>
                            <?php if($admin['username'] == 'pagodalabs'){
?>
                            <li><a href="<?php echo site_url('giftcard/admin/cardproductform')?>"><?php echo lang('common_giftcardproducts') ?></a></li>
<?php }?>
                            <?php endif; ?>
                        </ul>
                    </li>



                    <?php
                    // Restrict access to Admins only
                    if($this->authenticate->check_access('Admin')) : ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('common_catalog') ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <!--<li><a href="<?php echo site_url('brand/admin')?>"><?php echo lang('common_brands') ?></a></li>-->
							<!--<li><a href="<?php echo site_url('review/admin')?>"><?php echo lang('review') ?></a></li>-->
                            <li><a href="<?php echo site_url('category/admin')?>"><?php echo lang('common_categories') ?></a></li>
                            <li><a href="<?php echo site_url('product/admin')?>"><?php echo lang('common_products') ?></a></li>
                            <li><a href="<?php echo site_url('restock_notify/admin')?>"><?php echo lang('common_restock_notify'); ?></a></li>
                            <li><a href="<?php echo site_url('feature_slider/admin')?>"><?php echo lang('common_featured_slider'); ?></a></li>
                            <li><a href="<?php echo site_url('popup_notice/admin/popup_notice')?>"><?php echo lang('common_popup_notice'); ?></a></li>
                            <li><a href="<?php echo site_url('whole_sale_form/admin')?>"><?php echo lang('common_wholesale_form'); ?></a></li>
                           
                            <!--<li><a href="<?php echo $admin_url;?>digital_products"><?php echo lang('common_digital_products') ?></a></li>-->
                            <!--<li><a href="<?php echo site_url('occasion/admin');?>"><?php echo lang('common_occasion') ?></a></li>-->
                            <!--<li><a href="<?php echo site_url('event/admin');?>"><?php echo lang('common_events') ?></a></li>-->
                            <!--<li><a href="<?php echo site_url('canvas/admin');?>"><?php echo lang('common_canvas_templates') ?></a></li>-->
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('common_content') ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                         
	                        <!--<li><a href="<?php echo site_url('advertisement/admin')?>"><?php echo lang('common_advertisement') ?></a></li>-->
                            <li><a href="<?php echo site_url('banner/admin')?>"><?php echo lang('common_banners') ?></a></li>
                            <!--<li><a href="<?php echo site_url('board/admin')?>"><?php echo lang('common_board') ?></a></li>-->
                            <!--<li><a href="<?php echo site_url('currency/admin')?>"><?php echo lang('common_currency') ?></a></li>-->
                            <li><a href="<?php echo site_url('page/admin')?>"><?php echo lang('common_pages') ?></a></li>
                            <li><a href="<?php echo site_url('template/admin/email')?>"><?php echo lang('common_email_template') ?></a></li>
                            <li><a href="<?php echo site_url('color/admin')?>">Colors</a></li>
                            <li><a href="<?php echo site_url('size/admin')?>">Sizes</a></li>
                            <li><a href="<?php echo site_url('material/admin/material')?>">Materials</a></li>
                            <li><a href="<?php echo site_url('style/admin');?>">Styles</a></li>


                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('common_administrative') ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('admin/settings');?>"><?php echo lang('common_gocart_configuration') ?></a></li>
                            <li><a href="<?php echo $admin_url;?>shipping"><?php echo lang('common_shipping_modules') ?></a></li>
                            <li><a href="<?php echo $admin_url;?>payment"><?php echo lang('common_payment_modules') ?></a></li>
                            <li><a href="<?php echo $admin_url;?>settings/canned_messages"><?php echo lang('common_canned_messages') ?></a></li>
                            <li><a href="<?php echo site_url('location/admin')?>"><?php echo lang('common_locations') ?></a></li>
                            <li><a href="<?php echo $admin_url;?>admin"><?php echo lang('common_administrators') ?></a></li>
                            <!--<li><a href="<?php echo site_url('settings/margin');?>"><?php echo lang('common_costing') ?></a></li>-->
                            <li><a href="<?php echo site_url('rates/admin');?>"><?php echo lang('common_rates') ?></a></li>
                            
                            <li><a href="<?php echo site_url('exchangerate/admin');?>"><?php echo lang('exchangerate') ?></a></li>
                             <li><a href="<?php echo site_url('contact_detail/admin');?>">Contact Details</a></li>
                             <li><a href="<?php echo $admin_url;?>theme_setting">Christmas Theme</a></li>
                           
                            
                            
                            
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Custom Orders<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('custom_order/admin')?>">Order Requests</a></li>
                            <li><a href="<?php echo site_url('custom_order_relation/admin');?>">Custom Order Relations</a></li>
                                                       
                        </ul>
                    </li>   

                    <!--<li><a href="<?php echo site_url('exchangerate/admin')?>">ExchangeRate</a></li>-->
                    <li><a href="<?php echo site_url('bulk_upload/admin')?>">Bulk Uploads</a></li>
                   <li><a href="<?php echo site_url('blog/admin')?>"><?php echo lang('common_blog') ?></a></li>
                     <!--<li><a href="<?php echo site_url('blog/admin/info')?>">Info</a></li>-->
                    <?php endif; ?>
                </ul>
                
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('common_actions');?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url($this->config->item('admin_folder').'/dashboard');?>"><?php echo lang('common_dashboard') ?></a></li>
                            <li><a href="<?php echo site_url();?>"><?php echo lang('common_front_end') ?></a></li>
                            <li><a href="<?php echo site_url($this->config->item('admin_folder').'/login/logout');?>"><?php echo lang('common_log_out') ?></a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.nav-collapse -->
        </div>
    </div><!-- /navbar-inner -->
</div>
<?php endif;?>
<div class="container">
    <?php
    //lets have the flashdata overright "$message" if it exists
    if($this->session->flashdata('message'))
    {
        $message    = $this->session->flashdata('message');
    }
    
    if($this->session->flashdata('error'))
    {
        $error  = $this->session->flashdata('error');
    }
    
    if(function_exists('validation_errors') && validation_errors() != '')
    {
        $error  = validation_errors();
    }
    ?>
    
    <div id="js_error_container" class="alert alert-error" style="display:none;"> 
        <p id="js_error"></p>
    </div>
    
    <div id="js_note_container" class="alert alert-note" style="display:none;">
        
    </div>
    
    <?php if (!empty($message)): ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $message; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($error)): ?>
        <div class="alert alert-error">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
</div>      

<script>
$(document).ready(function(e) {
    $(".alert").animate({opacity: 1.0}, 3000).fadeOut('slow');
	$('.alert').click(function()
	{
		$(this).hide();
	});
});

</script>

<div class="container">
    <?php if(!empty($page_title)):?>
    <div class="page-header">
        <h1><?php echo  $page_title; ?></h1>
    </div>
    <?php endif;?>
    
  