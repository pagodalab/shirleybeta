<?php if(!$payment_module_installed):?>
	
	<div class="alert">
		<a class="close" data-dismiss="alert">×</a>
		<strong><?php echo lang('common_note') ?>:</strong> <?php echo lang('no_payment_module_installed'); ?>
	</div>

<?php endif;?>

<?php if(!$shipping_module_installed):?>
	<div class="alert">
		<a class="close" data-dismiss="alert">×</a>
		<strong><?php echo lang('common_note') ?>:</strong> <?php echo lang('no_shipping_module_installed'); ?>
	</div>

<?php endif;?>

<div class="row-fluid">
    <div class="span6">
        <h3><?php echo lang('recent_orders') ?></h3> 
        <table id="order-table" data-options="pagination:true,title:'Orders',pagesize:'20', rownumbers:true,toolbar:'#order_toolbar',collapsible:true,fitColumns:true">
            <thead>
            <th field="order_number" sortable="true" width="60">Order Number</th>
            <th field="bill_last_name" width="50" formatter="formatBillName">Bill To</th>
            <th field="ship_lastname" width="50" formatter="formatShipName">Ship To</th>
            <th field="ordered_on" width="50">Ordered On</th>
            <th field="status" width="50">Status</th>
            </thead>
        </table>
        <div id="order_toolbar">
        	<p><a class="btn btn-small" href="<?php echo site_url('order/admin');?>"><span><i class="icon-th-list"></i></span> <?php echo lang('view_all_orders');?></a></p>
        </div>        
    </div>
    <div class="span6">
        <h3><?php echo lang('recent_customers') ?></h3>
         <table id="customer-table" data-options="pagination:true,title:'Customers',pagesize:'20', rownumbers:true,toolbar:'#customer_toolbar',collapsible:true,fitColumns:true">
            <thead>
            <th field="firstname" sortable="true" width="50">First Name</th>
            <th field="lastname" width="50">Last Name</th>
            <th field="email" width="80">Email</th>
            <th field="active" width="50" formatter="formatStatus">Active</th>
            </thead>
        </table> 
        <div id="customer_toolbar">
        	<p> <a class="btn btn-small" href="<?php echo site_url('customer/admin');?>"><span><i class="icon-th-list"></i></span> <?php echo lang('view_all_customers');?></a></p>
        </div>       
    </div>
</div>
<script>
$(function(){
		$('#order-table').datagrid({
			url:'<?=site_url('order/admin/json')?>',
			height:'auto',
			width:'auto'

		});
		
		$('#customer-table').datagrid({
			url:'<?=site_url('customer/admin/json')?>',
			height:'auto',
			width:'auto'

		});
	
	});
	
	function formatBillName(value,row,index)
	{
		return '<p>'+row.bill_lastname+ ' '+ row.bill_firstname+'</p>';
	}
	
	function formatShipName(value,row,index)
	{
		return '<p>'+row.ship_lastname+ ' '+ row.ship_firstname+'</p>';
	}
	
	function formatStatus(value,row,index)
	{
		if(row.active == 1)
		{
			return 'Yes';
		}
		else
		{
			return 'No';
		}
	}
</script>