<?php

/**
 * The base controller which is used by the Front and the Admin controllers
 */
class MY_Controller extends MX_Controller
{
	var $facebookUser;
	
	public function __construct()
	{
		
		parent::__construct();

		//kill any references to the following methods
		$mthd = $this->router->method;
		if($mthd == 'view' || $mthd == 'partial' || $mthd == 'set_template')
		{
			show_404();
		}
		//load base libraries, helpers and models
		$this->load->database();

		// load the migrations class & settings model
		$this->load->model('Settings_model');
	
		//load in config items from the database
		$settings = $this->Settings_model->get('gocart');
		foreach($settings as $key=>$setting)
		{
			//special for the order status settings
			if($key == 'order_statuses')
			{
				$setting = json_decode($setting, true);
			}
			$this->config->set_item($key, $setting);
		}
	
		
		//load the default libraries
		$this->_init_facebook(); //Load Facebook Libraries
		
		$this->load->library(array('session', 'authenticate', 'go_cart'));
		$this->load->model(array('customer/customer_model','category/category_model', 'location/location_model'));
		$this->load->helper(array('url', 'file', 'string', 'html', 'language'));
		$this->load->library('site/bep_site');
		$this->load->library('site/bep_assets');
		
		$this->bep_site->set_metatag('content-type','text/html; charset=utf-8',TRUE);
		$this->bep_site->set_metatag('robots','all');
		$this->bep_site->set_metatag('pragma','cache',TRUE);				
        
        //if SSL is enabled in config force it here.
        if (config_item('ssl_support') && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off'))
		{
			$CI =& get_instance();
			$CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
			redirect($CI->uri->uri_string());
		}

		//print_r($this->uri->segments);exit;
		

		/*if($this->session->userdata('language'))

		{

			$this->config->set_item('language',$this->session->userdata('language'));

			$this->config->set_item('language_code',$this->session->userdata('language_code'));

		}

		$this->current_culture=$this->config->item('language');

		$this->culture_code=$this->config->item('language_code');*/

		$this->load->model('size/size_model');
	}

	function _init_facebook()
	{
		$this->load->add_package_path(APPPATH.'third_party/facebook/');
		$this->load->library('facebook',array('appId'=>'411146595632272','secret'=>'d4f88e617a4c83eb3b4877872d7b44fa','cookie' => TRUE, 'fileUpload'=>TRUE));		
		$this->load->helper('facebook');
		
	}

	function set_current_culture($language)
	{

		$this->session->set_userdata('language',strtolower($language));

	}

	
}//end MY_Controller

include_once('Front_Controller.php');
include_once('Member_Controller.php');
include_once('Admin_Controller.php');


