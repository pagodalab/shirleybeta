<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Router.php";

class MY_Router extends MX_Router {

	function __construct()
	{
		parent::__construct();
	}
	
		public function _validate_request($segments) {

		if (count($segments) == 0) return $segments;
		
		/* locate module controller */
		if ($located = $this->locate($segments)) return $located;

		$segments=explode("/",$this->_slug_request($segments));
		if ($located = $this->locate($segments)) return $located;
		
		
		/* use a default 404_override controller */
		if (isset($this->routes['404_override']) AND $this->routes['404_override']) {
			$segments = explode('/', $this->routes['404_override']);
			if ($located = $this->locate($segments)) return $located;
		}

		
		//
		
		
		/* no controller found */
		show_404();
	}


	function _get_db_route($slug)
	{
		
		return DB()->where('slug',$slug)->get('tbl_routes')->row_array();
	}

	function _slug_request($segments)
	{
// now try the GoCart specific routing
		$segments = array_splice($segments, -2, 2);
		

		// Turn the segment array into a URI string
		$uri = implode('/', $segments);

		//look through the database for a route that matches and apply the same logic as above :-)
		//load the database connection information
		require_once BASEPATH.'database/DB'.EXT;

		$total_segments=count($segments);

		if($total_segments==1)
		{
			$row	= $this->_get_db_route($segments[0]);
			return $row['route'];
		}
		else
		{
			$segments	= array_reverse($segments);
			$row	= $this->_get_db_route($segments[0]);		
			return $row['route'];
		}
		
		/*if(count($segments) == 2)
		{
			$row	= $this->_get_db_route($segments[1]);

			if(!empty($row))
			{
				return $row['route'];
			}
		}
		else
		{	
			$segments	= array_reverse($segments);
			//start with the end just to make sure we're not a multi-tiered category or category/product combo before moving to the second segment
			//we could stop people from naming products or categories after numbers, but that would be limiting their use.
			$row	= $this->_get_db_route($segments[0]);
			//set a pagination flag. If this is set true in the next if statement we'll know that the first row is segment is possibly a page number
			$page_flag	= false;
			if($row)
			{
				return $row['route'];
			}
			else
			{
				//this is the second go
				$row	= $this->_get_db_route($segments[1]);
				$page_flag	= true;
			}
			
			//we have a hit, continue down the path!
			if($row)
			{
				if(!$page_flag)
				{
					return $this->_set_request(explode('/', $row['route']));
				}
				else
				{
					$key = $row['slug'].'/([0-9]+)';
					
					//pages can only be numerical. This could end in a mighty big error!!!!
					if (preg_match('#^'.$key.'$#', $uri))
					{
						$row['route'] = preg_replace('#^'.$key.'$#', $row['route'],$uri);
						return $this->_set_request(explode('/', $row['route']));
					}
				}
			}
		}*/
		
		// If we got this far it means we didn't encounter a
		// matching route so we'll set the site default route
		//$this->_set_request($this->uri->segments);		
	}
}