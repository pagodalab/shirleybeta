<?php


class Admin_Controller extends MY_Controller 
{
	
	private $template;
	var $culture_code="en";
        var $exrate = false;
	function __construct()
	{
		parent::__construct();
		
		$this->authenticate->is_logged_in(uri_string());
		
		//load the base language file
		$this->lang->load('admin_common');
		$this->lang->load('media');

		$this->bep_site->set_metatag('robots','nofollow, noindex');
		$this->bep_site->set_metatag('pragma','nocache',TRUE);

		$this->load->helper(array('easyui','paging'));
		// Load the ADMIN asset group
		$this->bep_assets->load_asset_group('ADMIN');
		$this->bep_assets->load_asset_group('EASYUI');
		$this->bep_assets->load_asset_group('EASYUI-EXTENDED');
		$this->culture_code= $this->config->item('language_abbr');
                $this->exrate = $this->getExchangeRate();		
	}
	
	function view($view, $vars = array(), $string=false)
	{
		//if there is a template, use it.
		$template	= '';
		if($this->template)
		{
			$template	= $this->template.'_';
		}

		if($string)
		{
			$result	 = $this->load->view('admin/'.$template.'header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('admin/'.$template.'footer', $vars, true);
			
			return $result;
		}
		else
		{
			$this->load->view('admin/'.$template.'header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('admin/'.$template.'footer', $vars);
		}
		
		//reset $this->template to blank
		$this->template	= false;
	}
	
	/* Template is a temporary prefix that lasts only for the next call to view */
	function set_template($template)
	{
		$this->template	= $template;
	}

	function pagination_config()
	{
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['full_tag_open'] = '<div class="pagination"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			return $config;		
	}	
        function getExchangeRate()
	{
		$this->load->model('exchangerate/exchangerate_model');
		
		$this->db->limit('1');
		$exrate = $this->exchangerate_model->getExchangerates(null,'id desc')->row_array();
		return $exrate;
	}
	
	public function send_email_notification($email_list, $restock_by = NULL, $product_id = NULL, $size = NULL)
	{
		$email_list = array(array('email' => 'anish@pagodalabs.com'), array('email' => 'sanish@pagodalabs.com'), ); 

	    $row = $this->db->get_where('view_restock_product', array('product_id' => $product_id, 'size_id' =>$size))->row_array();
	    
	    $parse_data = array(
			'SITE_URL'			=>	'<a href="'.site_url().'">'.site_url().'</a>',
			'RESTOCK_DATE' 		=> 	$restock_by,
			'PRODUCT_NAME_EN'	=>	$row['name_en'],
			'PRODUCT_NAME_DA'	=>	$row['name_da'],
			'PRODUCT_URL'		=>	'<a href="'.site_url($row['slug']).'">'. site_url($row['slug']) . '</a>',
			'SIZE_CODE'			=>	$row['size_code'],
			'SIZE_TITLE_EN'		=>	$row['size_title_en'],
			'SIZE_TITLE_DA'		=>	$row['size_title_da'],
		);
			
		$this->load->library('email');
		$this->load->library('parser');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = 'smtp.vianet.com.np';

		$config['mailtype'] = 'html';
		$this->email->initialize($config);



		if($restock_by == NULL){
			$settings = $this->db->get_where('tbl_settings',array('code' => 'restock_email'))->result_array();
		} 
		else {
			$settings = $this->db->get_where('tbl_settings',array('code' => 'restock_email_time'))->result_array();
		}

		foreach ($settings as $v) {
			if($v['setting_key'] == 'subject'){
				$subject = $v['setting'];
			}
			if($v['setting_key'] == 'body'){
				$body = $v['setting'];

			}
		}

		$subject = $this->parser->parse_string($subject, $parse_data);
		$body = $this->parser->parse_string($body, $parse_data);

		foreach ($email_list as $value) {
			$this->email->from('admin@shirleybredal.com', 'Shirley Bredal');
			$this->email->to($value['email']); 
			$this->email->subject($subject);
			$this->email->message(html_entity_decode($body));
			$this->email->send();
		}
// 		exit;

	}
}