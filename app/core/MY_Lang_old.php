<?php

/**
 * Matchbox Language class
 *
 * This file is part of Matchbox
 *
 * Matchbox is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matchbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package   Matchbox
 * @copyright 2007-2008 Zacharias Knudsen
 * @license   http://www.gnu.org/licenses/gpl.html
 * @version   $Id: MY_Language.php 204 2008-02-24 01:30:00Z zacharias@dynaknudsen.dk $
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Extends the CodeIgniter Language class
 *
 * All code not encapsulated in {{{ Matchbox }}} was made by EllisLab
 *
 * @package   Matchbox
 * @copyright 2007-2008 Zacharias Knudsen
 * @license   http://www.gnu.org/licenses/gpl.html
 */
class MY_Lang extends CI_Lang
{
    // special URIs (not localized)
    private $special = array (
        "admin",
        "ajax",
        "api",


    );
    // {{{ Matchbox
function __construct() {
        
        global $URI, $CFG, $IN;
        
        $config =& $CFG->config;
        
        $index_page    = $config['index_page'];
        $lang_ignore   = $config['lang_ignore'];
        $default_abbr  = $config['language_abbr'];
        $lang_uri_abbr = $config['lang_uri_abbr'];
        
        /* get the language abbreviation from uri */
        $uri_abbr = $URI->segment(1);

        /* adjust the uri string leading slash */
        $URI->uri_string = preg_replace("|^\/?|", '/', $URI->uri_string);
        
        if ($lang_ignore) {
            
            if (isset($lang_uri_abbr[$uri_abbr])) {
            
                /* set the language_abbreviation cookie */
                $IN->set_cookie('user_lang', $uri_abbr, $config['sess_expiration']);
                
            } else {
                
                /* get the language_abbreviation from cookie */
                $lang_abbr = $IN->cookie($config['cookie_prefix'].'user_lang');
            
            }
            
            if (strlen($uri_abbr) == 2) {
                
                /* reset the uri identifier */
                $index_page .= empty($index_page) ? '' : '/';
                
                /* remove the invalid abbreviation */
                $URI->uri_string = preg_replace("|^\/?$uri_abbr\/?|", '', $URI->uri_string);
            
                /* redirect */
                header('Location: '.$config['base_url'].$index_page.$URI->uri_string);
                exit;
            }
            
        } else {
            
            /* set the language abbreviation */
            $lang_abbr = $uri_abbr;
        }

        /* check validity against config array */
        if (isset($lang_uri_abbr[$lang_abbr])) {
           
           /* reset uri segments and uri string */
           $URI->_reindex_segments(array_shift($URI->segments));
           $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);
           
           /* set config language values to match the user language */
           $config['language'] = $lang_uri_abbr[$lang_abbr];
           $config['language_abbr'] = $lang_abbr;
            
           /* if abbreviation is not ignored */
           if ( ! $lang_ignore) {
                   
                   /* check and set the uri identifier */
                   $index_page .= empty($index_page) ? $lang_abbr : "/$lang_abbr";
                
                /* reset the index_page value */
                $config['index_page'] = $index_page;
           }

           /* set the language_abbreviation cookie */               
           $IN->set_cookie('user_lang', $lang_abbr, $config['sess_expiration']);
           
        } else {
                       
            /* if abbreviation is not ignored */   
            if ( ! $lang_ignore) {                   
                   
                   /* check and set the uri identifier to the default value */    
                $index_page .= empty($index_page) ? $default_abbr : "/$default_abbr";
                
                if (strlen($lang_abbr) == 2) {
                    
                    /* remove invalid abbreviation */
                    $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);
                }
                
                /* redirect */
		
			
			$uri=$URI->uri_string();
			$uri_segment = $this->get_uri_lang($uri,$config['lang_uri_abbr']);
			
			if(!$this->is_special($uri_segment['parts'][0])){

				$index_url = empty($CFG->config['index_page']) ? '' : $CFG->config['index_page']."/";
				$new_url = $CFG->config['base_url'].str_replace('//','/',$index_url.$this->default_lang().'/'.$uri);
				
				header("Location: " . $new_url, TRUE, 302);
				exit;
			}

            }

            /* set the language_abbreviation cookie */                
            $IN->set_cookie('user_lang', $default_abbr, $config['sess_expiration']);
        }
        
        log_message('debug', "Language_Identifier Class Initialized");
    }
	
    function default_lang()
    {
        return 'en';
    }	
	
    function is_special($lang_code)
    {
        if ((!empty($lang_code)) && (in_array($lang_code, $this->special)))
            return TRUE;
        else
            return FALSE;
    }	
	
   function get_uri_lang($uri = '',$lang)
    {
        if (!empty($uri))
        {
            $uri = ($uri[0] == '/') ? substr($uri, 1): $uri;

            $uri_expl = explode('/', $uri, 2);
            $uri_segment['lang'] = NULL;
            $uri_segment['parts'] = $uri_expl;

            if (array_key_exists($uri_expl[0], $lang))
            {
                $uri_segment['lang'] = $uri_expl[0];
            }
            return $uri_segment;
        }
        else
            return FALSE;
    }	
    /**
     * Loads language file from module
     *
     * @param  string
     * @param  string
     * @param  string
     * @param  bool
     * @return void
     * @access public
     */
    function module_load($module, $langfile = '', $idiom = '', $return = false)
    {
        return $this->load($langfile, $idiom, $return, $module);
    }

    // }}}

    /**
     * Load a language file
     *
     * @access    public
     * @param    mixed    the name of the language file to be loaded. Can be an array
     * @param    string    the language (english, etc.)
     * @return    void
     */
    function load($langfile = '', $idiom = '', $return = FALSE)
    {
        $langfile = str_replace(EXT, '', str_replace('_lang.', '', $langfile)).'_lang'.EXT;

        if (in_array($langfile, $this->is_loaded, TRUE))
        {
            return;
        }

        // {{{ Matchbox

        $ci     = &get_instance();

        $module = $ci->load->_matchbox->argument(3);

        if ($idiom == '') {
            $deft_lang = $ci->config->item('language');
            $idiom = ($deft_lang == '') ? 'english' : $deft_lang;
        }
		$found=TRUE;
        if (!$filepath = $ci->load->_matchbox->find('language/' . $idiom . '/' . $langfile, $module, 2)) {
            //show_error('Unable to load the requested language file: language/' . $langfile);
			$found=FALSE;
			
        }
		else
		{
			include($filepath);
		}

		foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
		{
			if (file_exists($package_path.'language/'.$idiom.'/'.$langfile))
			{
				include($package_path.'language/'.$idiom.'/'.$langfile);
				$found = TRUE;
				break;
			}
		}

       

        // }}}

        if ( ! isset($lang))
        {
            log_message('error', 'Language file contains no data: language/'.$idiom.'/'.$langfile);
            return;
        }

        if ($return == TRUE)
        {
            return $lang;
        }

        $this->is_loaded[] = $langfile;
        $this->language = array_merge($this->language, $lang);
        unset($lang);

        log_message('debug', 'Language file loaded: language/'.$idiom.'/'.$langfile);
        return TRUE;
    }
}

?>