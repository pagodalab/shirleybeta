<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

// Originaly CodeIgniter i18n library by Jérôme Jaglale
// http://maestric.com/en/doc/php/codeigniter_i18n
// modification by Yeb Reitsma

/*
in case you use it with the HMVC modular extension
uncomment this and remove the other lines
load the MX_Loader class */

require APPPATH."third_party/MX/Lang.php";

//class MY_Lang extends MX_Lang {

class MY_Lang extends MX_Lang {


  /**************************************************
   configuration
  ***************************************************/

  // languages
  private $languages = array(
    'en' => 'english',
    'da' => 'danish'
  );

  // special URIs (not localized)
  private $special = array (
    "admin"
  );

  // where to redirect if no language in URI
  private $uri;
  private $default_uri;
  private $lang_code;

  /**************************************************/


 function __construct() {
        
        global $URI, $CFG, $IN;
        
        $config =& $CFG->config;
        
        $index_page    = $config['index_page'];
        $lang_ignore   = $config['lang_ignore'];
        $default_abbr  = $config['language_abbr'];
        $lang_uri_abbr = $config['lang_uri_abbr'];
        
        /* get the language abbreviation from uri */
        $uri_abbr = $URI->segment(1);

        /* adjust the uri string leading slash */
        $URI->uri_string = preg_replace("|^\/?|", '/', $URI->uri_string);
        
        if ($lang_ignore) {
            
            if (isset($lang_uri_abbr[$uri_abbr])) {
            
                /* set the language_abbreviation cookie */
                $IN->set_cookie('user_lang', $uri_abbr, $config['sess_expiration']);
                
            } else {
                
                /* get the language_abbreviation from cookie */
                $lang_abbr = $IN->cookie($config['cookie_prefix'].'user_lang');
            
            }
            
            if (strlen($uri_abbr) == 2) {
                
                /* reset the uri identifier */
                $index_page .= empty($index_page) ? '' : '/';
                
                /* remove the invalid abbreviation */
                $URI->uri_string = preg_replace("|^\/?$uri_abbr\/?|", '', $URI->uri_string);
            
                /* redirect */
                header('Location: '.$config['base_url'].$index_page.$URI->uri_string);
                exit;
            }
            
        } else {
            
            /* set the language abbreviation */
            $lang_abbr = $uri_abbr;
        }

        /* check validity against config array */
        if (isset($lang_uri_abbr[$lang_abbr])) {
           
           /* reset uri segments and uri string */
           $URI->_reindex_segments(array_shift($URI->segments));
           $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);
           
           /* set config language values to match the user language */
           $config['language'] = $lang_uri_abbr[$lang_abbr];
           $config['language_abbr'] = $lang_abbr;
            
           /* if abbreviation is not ignored */
           if ( ! $lang_ignore) {
                   
                   /* check and set the uri identifier */
                   $index_page .= empty($index_page) ? $lang_abbr : "/$lang_abbr";
                
                /* reset the index_page value */
                $config['index_page'] = $index_page;
           }

           /* set the language_abbreviation cookie */               
           $IN->set_cookie('user_lang', $lang_abbr, $config['sess_expiration']);
           
        } else {
                       
            /* if abbreviation is not ignored */   
            if ( ! $lang_ignore) {                   
                   
                   /* check and set the uri identifier to the default value */    
                $index_page .= empty($index_page) ? $default_abbr : "/$default_abbr";
                
                if (strlen($lang_abbr) == 2) {
                    
                    /* remove invalid abbreviation */
                    $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);
                }
                
                /* redirect */
    
      
      $uri=$URI->uri_string();
      $uri_segment = $this->get_uri_lang($uri,$config['lang_uri_abbr']);
      
      if(!$this->is_special($uri_segment['parts'][0])){

        $index_url = empty($CFG->config['index_page']) ? '' : $CFG->config['index_page']."/";
        $new_url = $CFG->config['base_url'].str_replace('//','/',$index_url.$this->default_lang().'/'.$uri);
        
        header("Location: " . $new_url, TRUE, 302);
        exit;
      }

            }

            /* set the language_abbreviation cookie */                
            $IN->set_cookie('user_lang', $default_abbr, $config['sess_expiration']);
        }
        
        log_message('debug', "Language_Identifier Class Initialized");
    }


  // get current language
  // ex: return 'en' if language in CI config is 'english' 
  function lang()
  {
    global $CFG;        
    $language = $CFG->item('language');

    $lang = array_search($language, $this->languages);
    if ($lang)
    {
      return $lang;
    }

    return NULL;    // this should not happen
  }


  function is_special($lang_code)
  {
    if ((!empty($lang_code)) && (in_array($lang_code, $this->special)))
      return TRUE;
    else
      return FALSE;
  }


  function switch_uri($lang)
  {
    if ((!empty($this->uri)) && (array_key_exists($lang, $this->languages)))
    {

      if ($uri_segment = $this->get_uri_lang($this->uri))
      {
        $uri_segment['parts'][0] = $lang;
        $uri = implode('/',$uri_segment['parts']);
      }
      else
      {
        $uri = $lang.'/'.$this->uri;
      }
    }

    return $uri;
  }

  //check if the language exists
  //when true returns an array with lang abbreviation + rest
  function get_uri_lang($uri = '')
  {
    if (!empty($uri))
    {
      $uri = ($uri[0] == '/') ? substr($uri, 1): $uri;

      $uri_expl = explode('/', $uri, 2);
      $uri_segment['lang'] = NULL;
      $uri_segment['parts'] = $uri_expl;  

      if (array_key_exists($uri_expl[0], $this->languages))
      {
        $uri_segment['lang'] = $uri_expl[0];
      }
      return $uri_segment;
    }
    else
      return FALSE;
  }


  // default language: first element of $this->languages
  function default_lang()
  {
    $browser_lang = !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
    $browser_lang = substr($browser_lang, 0,2);
    if(array_key_exists($browser_lang, $this->languages))
        return $browser_lang;
    else{
        reset($this->languages);
        return key($this->languages);
    }
  }


  // add language segment to $uri (if appropriate)
  function localized($uri)
  {
    if (!empty($uri))
    {
      $uri_segment = $this->get_uri_lang($uri);
      if (!$uri_segment['lang'])
      {

        if ((!$this->is_special($uri_segment['parts'][0])) && (!preg_match('/(.+)\.[a-zA-Z0-9]{2,4}$/', $uri)))
        {
          $uri = $this->lang() . '/' . $uri;
        }
      }
    }
    return $uri;
  }


/**
     * Same behavior as the parent method, but it can load the first defined 
     * lang configuration to fill other languages gaps. This is very useful
     * because you don't have to update all your lang files during development
     * each time you update a text. If a constant is missing it will load
     * it in the first language configured in the array $languages. (OPB)
     * 
     * 
     * @param boolean $load_first_lang false to keep the old behavior. Please
     * modify the default value to true to use this feature without having to 
     * modify your code 
     */
    function load($langfile = '', $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '', $load_first_lang = false) {
        if ($load_first_lang) {
            reset($this->languages);
            $firstKey = key($this->languages);
            $firstValue = $this->languages[$firstKey];

            if ($this->lang_code != $firstKey) {
                $addedLang = parent::load($langfile, $firstValue, $return, $add_suffix, $alt_path);
                if ($addedLang) {
                    if ($add_suffix) {
                        $langfileToRemove = str_replace('.php', '', $langfile);
                        $langfileToRemove = str_replace('_lang.', '', $langfileToRemove) . '_lang';
                        $langfileToRemove .= '.php';
                    }
                    $this->is_loaded = array_diff($this->is_loaded, array($langfileToRemove));
                }
            }
        }
        return parent::load($langfile, $idiom, $return, $add_suffix, $alt_path);
    }

} 

// END MY_Lang Class

/* End of file MY_Lang.php */
/* Location: ./application/core/MY_Lang.php */