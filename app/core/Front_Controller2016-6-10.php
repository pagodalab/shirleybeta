<?php

class Front_Controller extends MY_Controller
{
	
	//we collect the categories automatically with each load rather than for each function
	//this just cuts the codebase down a bit
	var $categories	= '';
	//load all the pages into this variable so we can call it from all the methods
	var $pages = '';
	
	// determine whether to display gift card link on all cart pages
	//  This is Not the place to enable gift cards. It is a setting that is loaded during instantiation.
	var $gift_cards_enabled;
	
	var $message_board;
	
	var $page_limit=16;
	
	var $country_ip = '';
	var $country_code = 'DKK';
	var $currency_code = 'USD';
	var $base_currency= 'DKK';
	var $euro_currency = 'EURO';
	var $usd_currency = 'USD';
	var $menu_active = FALSE;
	var $culture_code="en";
	var $lang_code = 'en';

	var $filter=FALSE;
	var $exrate = false;
	var $settings = false;
	var $meta_keywords = false;
	var $meta_description = false;
	//var $exchange_rate = $this->getTodaysRate();
    var $submenu_active = FALSE;

	function __construct(){
	
		parent::__construct();
		//load the theme package
		$this->load->add_package_path('themes/'.config_item('theme').'/');
		$this->bep_assets->load_asset_group('ADMIN_LOGIN');
		$this->initialize();
		$this->_loadPackages();	
		$this->country_code=$this->_set_country_code();
		$this->currencies = $this->getcurrencies();
		$this->vendors = $this->getVendors();
		$this->occasions = $this->getOccasions();
		$this->events = $this->getEvents();
		$this->menu_category = $this->getCategories();
		$this->canvas_templates = $this->getCanvasTemplates();
		$this->session->set_userdata(array('counter'=>0));
		$this->exrate = $this->getExchangeRate();
		$this->culture_code= $this->config->item('language_abbr');
		$this->settings = $this->getSettings();
		$this->sendBlogMail();
		

	}
	
	private function initialize()
	{
				//load GoCart library
		$this->load->library('banner/banners');
		//load needed models
		$this->load->model(array('page/page_model', 'product/product_model', 'product/digital_product_model', 'giftcard/gift_card_model', 'product/option_model', 'order/order_model', 'Settings_model','review/review_model'));
		//load helpers
		$this->load->helper(array('form_helper', 'formatting_helper','cookie_helper'));
		//load common language
		$this->lang->load('common');
		//fill in our variables
		$this->categories	= $this->category_model->get_categories_tiered(0);
		$this->pages		= $this->page_model->get_pages_tiered();
		
		// check if giftcards are enabled
		$gc_setting = $this->Settings_model->get('gift_cards');
		if(!empty($gc_setting['enabled']) && $gc_setting['enabled']==1)
		{
			$this->gift_cards_enabled = true;
		}			
		else
		{
			$this->gift_cards_enabled = false;
		}
		
		$this->load->model('board/board_model');
		
		$this->message_board=$this->board_model->getActiveMessage();
		
		if($this->meta_keywords == false || $this->meta_description == false)
		{
			$setting = $this->getSettings();
			
			$this->meta_keywords = $setting['meta_keywords'];
			$this->meta_description = $setting['meta_description'];

		}	
		
		if($this->session->userdata('currency') == 'euro')
		{
		    $this->lang_code = 'en';
		}
		else
		{
		    $this->lang_code = 'da';
		}
		
	}
		
	/*
	This works exactly like the regular $this->load->view()
	The difference is it automatically pulls in a header and footer.
	*/
	function view($view, $vars = array(), $string=false)
	{
		if($string)
		{
			$result	 = $this->load->view('header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('footer', $vars, true);
			
			return $result;
		}
		else
		{
			$this->load->view('header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('footer', $vars);
		}
	}
	
	/*
	This function simply calls $this->load->view()
	*/
	function partial($view, $vars = array(), $string=false)
	{
		if($string)
		{
			return $this->load->view($view, $vars, true);
		}
		else
		{
			$this->load->view($view, $vars);
		}
	}

	private function _loadPackages()
	{
		
		$this->extensions = $this->Settings_model->get('widget_modules');	
		foreach($this->extensions as $key=>$value):
			$this->load->add_package_path(APPPATH.'packages/widget/'.$key.'/');
			$this->load->library($key);		
		endforeach;
	}	
	
	function pagination_config()
	{
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
			$config['full_tag_close'] = '</ul>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			return $config;		
	}		
	
	private function _get_country_code()
	{
		$this->load->library('geoip');
		$geoip=$this->geoip;
		$gi = $geoip->geoip_open("GeoIP.dat",GEOIP_STANDARD);
		$country_code = $geoip->geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']);
		
		return $country_code;	
	}
	
	private function _set_country_code()
	{
     	$location=get_cookie('location');
		$country_code='';
		if($location)
		{
		   $country_code=(strtoupper($location)); 
		}
		else
		{
		  $country_code = strtoupper($this->_get_country_code());
		  

			//$countries=array('AU','NP','HK','US','UK');
			$countries=array('NP','US');
			if(!in_array($this->country_code,$countries))
			{
				$country_code='NP';
			}
                 $this->session->set_userdata('country_code',$country_code);
		}
		//echo "<pre>"; print_r($country_code);
		return $country_code;
	}
	
	function getcurrencies()
	{
		$this->load->model('currency/currency_model');
		$currencies = $this->currency_model->getCurrencies(array('status'=>1))->result_array();
		return $currencies;
	}
	
	function getVendors()
	{
		$this->load->model('brand/brand_model');
		$brands = $this->brand_model->getBrands(array('status'=>1),NULL,array('limit'=>8,'offset'=>''))->result_array();
		return $brands;
	}
	
	function getOccasions()
	{
		$this->load->model('occasion/occasion_model');
		$this->db->order_by('sort_order asc');
		$occasions = $this->occasion_model->getOccasions(array('status'=>1))->result_array();
		return $occasions;
	}
	
	function getEvents()
	{
		$this->load->model('event/event_model');
		$this->db->order_by('sort_order asc');
		$events = $this->event_model->getEvents(array('status'=>1))->result_array();
		return $events;
	}
	
	function getCategories()
	{
		$this->load->model('category/category_model');
		$this->db->order_by('sequence asc');
		$categories = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>0))->result_array();
		return $categories;
	}

	function getTodaysRate()
	{
		$this->load->model('exchangerate/exchangerate_model');
		$todays = $this->exchangerate_model->getExchangerates(array('rate_date'=>date('Y-m-d')))->row_array();

		return $todays['rate'];
	}
	
	function getCanvasTemplates()
	{
		$this->load->model('canvas/canvas_model');
		$this->db->order_by('template_id desc');
		$templates = $this->canvas_model->getCanvasTemplates(array('status'=>1))->result_array();
		
		return $templates;
	}
	
	
	function getExchangeRate()
	{
		$this->load->model('exchangerate/exchangerate_model');
		
		$this->db->limit('1');
		$exrate = $this->exchangerate_model->getExchangerates(null,'id desc')->row_array();
		return $exrate;
	}
	
	function getSettings()
	{
		$this->load->model('Settings_model');
		$data = $this->Settings_model->get('gocart');
		return $data;
	}
	
	function sendBlogMail()
	{	
		$this->load->library('email');
			
		$this->load->model('blog/blog_model');
		$this->load->model('subscribe/subscribe_model');

		$this->db->where('post_date = curdate() and status = 1 and email_send = 0');
		$blogs = $this->blog_model->getBlogs()->result_array();

		$subscribers = $this->subscribe_model->getSubscribes(array('status'=>'1'))->result_array();

		$this->load->model('messages_model');
		$row = $this->messages_model->get_message(9);

		// $subject ='New Blog post from '.$this->config->item('company_name');
		
		$config['mailtype'] = 'html';
		// $config['protocol'] = 'smtp';
		// $config['smtp_host'] = 'smtp.via.net.np';
				
		$this->email->initialize($config);

		foreach($blogs as $blog)
		{	

			$message = $blog['content_en'];

			$row['content'] = html_entity_decode($row['content']);
		
		// set replacement values for subject & body
		
		// {url}
		$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
		
		// {site_name}
		$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
		$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
			
		// {order_summary}
		$row['content'] = str_replace('{blog_contains}', $blog['content_en'], $row['content']);
		
			

			foreach($subscribers as $subscriber)
			{
				$this->email->clear(TRUE);

				$this->email->from($this->config->item('email'), $this->config->item('company_name'));
				$this->email->to($subscriber['email']);
				$this->email->subject($row['subject']);
				$this->email->message(html_entity_decode($row['content']));
				
				$this->email->send();
			}

			$data = array('email_send'=>1,'id'=>$blog['id']);
			$this->blog_model->save($data);

		}

	}

}