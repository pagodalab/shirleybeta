<?php
class Member_Controller extends Front_Controller
{
	var $customer;
	public function __construct()
	{
		parent::__construct();
		
		if(!$this->customer_model->is_logged_in(false,false))
		{
			redirect(site_url('auth/login'));
		}
		$this->load->model(array('location_model'));
		$this->customer = $this->go_cart->customer();		
		
	}

}