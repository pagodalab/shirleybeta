<?php
class Admin_model extends MY_Model
{
	var $joins=array();	
	public function __construct()
	{
		parent::__construct();
		$this->prefix='tbl_';
		$this->_TABLES=array('ADMIN'=>$this->prefix.'admin');
	}

	function getAdmins($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
	{
       $fields='admins.*';
                
        $this->db->select($fields,FALSE);
        $this->db->from($this->_TABLES['ADMIN']. ' admins');
		
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):$this->db->order_by('username','ASC');

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();			
	}	

	public function getById($id)
	{
		return $this->getAdmins(array('id'=>$id))->row();
	}

    public function count($where=NULL)
    {
        $this->db->from($this->_TABLES['ADMIN'].' admins');
       (! is_null($where))?$this->db->where($where):NULL;

        return $this->db->count_all_results();
    }	
}