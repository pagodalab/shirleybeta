<?php
Class Messages_model extends CI_Model
{
	function __construct()
	{
			parent::__construct();
		$this->prefix='tbl_';
		$this->_TABLES=array('CANNED_MESSAGE'=>$this->prefix.'canned_messages',
							 );
	}
	
	
	function get_list($type='')
	{
		if($type!='')
		{
			$this->db->where('type', $type);
		}
		$res = $this->db->get($this->_TABLES['CANNED_MESSAGE']);
		return $res->result_array();
	}
	
	function get_message($id)
	{
		$res = $this->db->where('id', $id)->get($this->_TABLES['CANNED_MESSAGE']);
		return $res->row_array();
	}
	
	function save_message($data)
	{
		if($data['id'])
		{
			$this->db->where('id', $data['id'])->update($this->_TABLES['CANNED_MESSAGE'], $data);
			return $data['id'];
		}
		else 
		{
			$this->db->insert($this->_TABLES['CANNED_MESSAGE'], $data);
			return $this->db->insert_id();
		}
	}
	
	function delete_message($id)
	{
		$this->db->where('id', $id)->delete($this->_TABLES['CANNED_MESSAGE']);
		return $id;
	}
	
	
}