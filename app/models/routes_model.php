<?php
class Routes_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('ROUTE'=>$this->prefix.'routes');
    }

	// save or update a route and return the id
	function save($route)
	{
		if(!empty($route['id']))
		{
			$this->update('ROUTE', $route,array('id'=>$route['id']));
			return $route['id'];
		}
		else
		{
			$this->insert('ROUTE', $route);
			return $this->db->insert_id();
		}
	}
	
	function check_slug($slug, $id=false)
	{
		if($id)
		{
			$this->db->where('id !=', $id);
		}
		$this->db->where('slug', $slug);
		$this->db->from($this->_TABLES['ROUTE']);
		return (bool) $this->db->count_all_results();
	}
	
	function validate_slug($slug, $id=false, $count=false)
	{
		if($this->check_slug($slug.$count, $id))
		{
			if(!$count)
			{
				$count	= 1;
			}
			else
			{
				$count++;
			}
			return $this->validate_slug($slug, $id, $count);
		}
		else
		{
			return $slug.$count;
		}
	}
	
	function delete($id)
	{
		$this->delete('ROUTE',array('id'=>$id));
	}
}