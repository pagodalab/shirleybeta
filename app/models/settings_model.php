<?php
Class Settings_model extends CI_Model
{
	function __construct()
	{
			parent::__construct();
			$this->_prefix='tbl_';
			$this->_TABLES=array('SETTINGS'=>$this->_prefix.'settings');
	}
	
	
	
	function get($code)
	{
		$this->db->where('code', $code);
		$result	= $this->db->get($this->_TABLES['SETTINGS']);
		
		$return	= array();
		foreach($result->result() as $results)
		{
			$return[$results->setting_key]	= $results->setting;
		}
		return $return;	
	}
	
	/*
	settings should be an array
	array('setting_key'=>'setting')
	$code is the item that is calling it
	ex. any shipping settings have the code "shipping"
	*/
	function save($code, $values)
	{
	
		//get the settings first, this way, we can know if we need to update or insert settings
		//we're going to create an array of keys for the requested code
		$settings	= $this->get($code);
	
		
		//loop through the settings and add each one as a new row
		foreach($values as $key=>$value)
		{
			//if the key currently exists, update the setting
			if(array_key_exists($key, $settings))
			{
				$update	= array('setting'=>$value);
				$this->db->where('code', $code);
				$this->db->where('setting_key',$key);
				$this->db->update($this->_TABLES['SETTINGS'], $update);
			}
			//if the key does not exist, add it
			else
			{
				$insert	= array('code'=>$code, 'setting_key'=>$key, 'setting'=>$value);
				$this->db->insert($this->_TABLES['SETTINGS'], $insert);
			}
			
		}
		
	}
	
	//delete any settings having to do with this particular code
	function delete($code)
	{
		$this->db->where('code', $code);
		$this->db->delete($this->_TABLES['SETTINGS']);
	}
	
	//this deletes a specific setting
	function delete_setting($code, $setting_key)
	{
		$this->db->where('code', $code);
		$this->db->where('setting_key', $setting_key);
		$this->db->delete($this->_TABLES['SETTINGS']);
	}
}