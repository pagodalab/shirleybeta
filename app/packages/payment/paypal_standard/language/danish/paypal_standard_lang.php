<?php

$lang['paypal_standard']				= 'PayPal Standar';

//paypal library messages
$lang['paypal_error']				= 'Der opstod en fejl under behandlingen af din betaling via PayPal';
$lang['paypal_desc']				= 'Du vil blive dirigeret til Paypal hjemmeside for at bekræfte din betaling. Når din betaling er godkendt, vil du blive dirigeret tilbage til vores hjemmeside og din ordre vil være komplet.';


//paypal admin
$lang['pp_username']				= 'Paypal API Username';
$lang['pp_password']				= 'Paypal API Password';
$lang['pp_key']						= 'Paypal API Signature';

$lang['test_mode_label']			= 'Mode';
$lang['test_mode']					= 'Test (Sandbox)';
$lang['live_mode']					= 'Live (Production)';

$lang['currency_label']				= 'USD, EUR, etc.';