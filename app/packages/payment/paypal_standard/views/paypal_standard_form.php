<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<label><?php echo lang('enabled');?></label>
<select name="enabled" class="span3">
	<option value="1"<?php echo((bool)$settings['enabled'])?' selected="selected"':'';?>><?php echo lang('enabled');?></option>
	<option value="0"<?php echo((bool)$settings['enabled'])?'':' selected="selected"';?>><?php echo lang('disabled');?></option>
</select>

<label><?php echo lang('test_mode_label') ?></label>
<select name="paypal_server_mode" class="span3">
	<option value="1"<?php echo((bool)$settings['paypal_server_mode'])?' selected="selected"':'';?>><?php echo lang('test_mode');?></option>
	<option value="0"<?php echo((bool)$settings['paypal_server_mode'])?'':' selected="selected"';?>><?php echo lang('live_mode');?></option>
</select>

<label><?php echo lang('pp_business_email') ?></label>
<input class="span3" name="paypal_business_email" type="text" value="<?php echo @$settings["paypal_business_email"] ?>" size="50" >



