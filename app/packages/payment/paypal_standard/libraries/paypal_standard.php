<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal_standard
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->library('paypal_lib');
		$this->CI->lang->load('paypal_standard');
		
		$this->method_name	= lang('paypal_standard');
	}
	
	/*
	checkout_form()
	this function returns an array, the first part being the name of the payment type
	that will show up beside the radio button the next value will be the actual form if there is no form, then it should equal false
	there is also the posibility that this payment method is not approved for this purchase. in that case, it should return a blank array 
	*/
	
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get('paypal_standard');
		$enabled	= $settings['enabled'];
		
		$form			= array();
		if($enabled)
		{
			$form['name'] = $this->method_name;
			
			$form['form'] = $this->CI->load->view('pp_checkout', array(), true);
			
			return $form;
			
		} else return array();
		
	}
	
	
	function checkout_check()
	{
		// Nothing to check in this module
		return false;
	}
	
	function description()
	{
		return lang('paypal_standard');
	}
	
	//back end installation functions
	function install()
	{
		
		$config['paypal_business_email'] = '';
		$config['paypal_confirmation_email'] = '';; 
		$config['paypal_server_mode'] = '1';
		$config['enabled'] = '0';
		
		//not normally user configurable, these are hidden on the form
		$config['return_url'] = "pp_gate/pp_return/";
		$config['cancel_url'] = "pp_gate/pp_cancel/";

		$this->CI->Settings_model->save('paypal_standard', $config);
	}
	
	function uninstall()
	{
		$this->CI->Settings_model->delete('paypal_standard');
	}
	
	//payment processor
	function process_payment()
	{
		$process	= false;
		
		$store		= $this->CI->config->item('company_name');
		$customer	= $this->CI->go_cart->customer();
		$name 		= $customer['firstname'].' '.$customer['lastname'];
		$order_date = date('Y-m-d');
		// this will forward the page to the paypal interface, leaving gocart behind
		// the user will be sent back and authenticated by the paypal gateway controller pp_gate.php

	
		$this->CI->paypal_lib->add_field('cmd','_xclick');
		$this->CI->paypal_lib->add_field('currency_code',$this->CI->config->item('currency_iso'));
		$this->CI->paypal_lib->add_field('item_name',$store.' order');
		$this->CI->paypal_lib->add_field('amount',$this->CI->go_cart->total());
		$this->CI->paypal_lib->add_field('notify_url',site_url('pp_standard/pp_ipn'));
		$this->CI->paypal_lib->add_field('return',site_url('pp_standard/pp_return'));
		$this->CI->paypal_lib->add_field('cancel_return',site_url('pp_standard/pp_cancel'));

		$this->CI->paypal_lib->add_field('custom', $customer['ship_address']['email'].'|'.$name.'|'.$order_date);

		$this->CI->paypal_lib->paypal_auto_form();
		exit;		
		// If we get to this step at all, something went wrong	
		return lang('paypal_error');
			
	}
	
	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->Settings_model->get('paypal_standard');
		}
		else
		{
			$settings = $post;
		}
		//retrieve form contents
		return $this->CI->load->view('paypal_standard_form', array('settings'=>$settings), true);
	}
	
	function check()
	{	
		$error	= false;
		
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			//we save the settings if it gets here
			$this->CI->Settings_model->save('paypal_standard', $_POST);
			
			return false;
		}
	}
}
