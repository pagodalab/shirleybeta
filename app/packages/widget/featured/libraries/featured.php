<?php

class Featured
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('featured');				
	}

	//back end installation functions
	function install()
	{
		
		$config['featured_products'] = '';

		$this->CI->settings_model->save('featured', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('featured');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		$this->CI->load->model('product/product_model');
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('featured');
		}
		else
		{

			$settings = $post;
		}

		$products = explode(',', $settings['featured_products']);
		$data['settings']=$settings;
		$data['modules']=array();
		
		$data['products']=array();
		foreach ($products as $product_id) {
			$product_info = $this->CI->product_model->getProducts(array('product_id'=>$product_id,'status'=>'1','delete_flag'=>0,'sold'=>0))->row_array();
			
			if ($product_info) {
				$data['products'][] = array(
					'id' => $product_info['product_id'],
					'name' => $product_info['product_name']
				);
			}
		}	
		if(!empty($settings['featured_module']))
		{
			$data['modules']=@unserialize($settings['featured_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('featured', array('featured_module'=>serialize($_POST['featured_module'])));
			$this->CI->settings_model->save('featured', array('featured_products'=>$_POST['featured_products']));				
			return false;
		}
	}
	
	function view($settings)
	{
		$featured_settings= $this->CI->settings_model->get('featured');
		$data['settings']=$settings;
		$products=explode(',',$featured_settings['featured_products']);
		

		$this->CI->load->model('product/product_model');
		$this->CI->db->where_in('product_id',$products);			
		$data['products']=$this->CI->product_model->getProducts(array('status'=>1,'delete_flag'=>0),NULL,array('limit'=>$settings['limit']))->result_array();
		return $this->CI->load->view('extensions/featured',$data,TRUE);
	}	
}