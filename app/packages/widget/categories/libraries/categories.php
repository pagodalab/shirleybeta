<?php

class Categories
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('categories');				
	}

	//back end installation functions
	function install()
	{
		
		$config['categories_module'] = '';

		$this->CI->settings_model->save('categories', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('categories');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('categories');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		//$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['categories_module']))
		{
			$data['modules']=@unserialize($settings['categories_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('categories', array('categories_module'=>serialize($_POST['categories_module'])));
			return false;
		}
	}
	
	function view($settings)
	{
		$this->CI->load->driver('cache');
		$this->CI->load->model('category/category_model');
		$this->CI->load->model('product/product_model');

		$data['categories'] = array();
		
		if (!$data['categories'] = $this->CI->cache->file->get('sidebar_categories'))
		{
			$categories=$this->CI->category_model->getCategories(array('parent_id'=>0,'category_flag'=>0,'status'=>1))->result_array();

			foreach($categories as $category):
			$children_data = array();
			
			$children = $this->CI->category_model->getCategories(array('parent_id'=>$category['category_id'],'category_flag'=>0,'status'=>1))->result_array();	
			$total_products=0;		
			$selected=FALSE;
			foreach ($children as $child) {
				
				if($this->CI->selected_category_id && $child['category_id']==$this->CI->selected_category_id)
				{
					$selected=TRUE;
				}
				$product_total = $this->CI->product_model->countProducts(array('category_id'=>$child['category_id'],'status'=>'1','delete_flag'=>0));
				$children_data[] = array(
					'category_id' => $child['category_id'],
					'category_name'        => $child['category_name'] . ' (' . $product_total . ')',
					'link'        => site_url($child['slug_name'])	
				);	
				$total_products=$total_products + $product_total;
			}
			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'category_name'        => $category['category_name']. ' (' . $total_products . ')',
				'children'    => $children_data,
				'link'        => site_url($child['slug_name']),
				'selected'=>$selected	
			);	
			endforeach;		
			$this->CI->cache->file->save('sidebar_categories', $data['categories'], 300); 
		}


//		$data['categories']=$this->CI->category_model->getCategories(array('parent_id'=>0))->result_array();
		return $this->CI->load->view('extensions/categories',$data,TRUE);
	}	
}