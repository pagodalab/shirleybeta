<?php

class Carousel
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('carousel');				
	}

	//back end installation functions
	function install()
	{
		
		$config['carousel_module'] = '';

		$this->CI->settings_model->save('carousel', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('carousel');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('carousel');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		//$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['carousel_module']))
		{
			$data['modules']=@unserialize($settings['carousel_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('carousel', array('carousel_module'=>serialize($_POST['carousel_module'])));
			return false;
		}
	}
	
	function view($settings)
	{
		$this->CI->bep_assets->load_asset_group('JCAROUSEL');
		$data['settings']=$settings;
		return $this->CI->load->view('extensions/carousel',$data,TRUE);
	}	
}