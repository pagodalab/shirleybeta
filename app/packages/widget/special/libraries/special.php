<?php

class Special
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('bestseller');				
	}

	//back end installation functions
	function install()
	{
		
		$config['bestseller_module'] = '';

		$this->CI->Settings_model->save_settings('bestseller', $config);
	}
	
	function uninstall()
	{
		$this->CI->Settings_model->delete_settings('bestseller');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->Settings_model->get_settings('bestseller');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['bestseller_module']))
		{
			$data['modules']=@unserialize($settings['bestseller_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->Settings_model->save_settings('bestseller', array('bestseller_module'=>serialize($_POST['bestseller_module'])));
			return false;
		}
	}
	
	function view($settings)
	{
		return $this->CI->load->view('extensions/bestseller',NULL,TRUE);
	}	
}