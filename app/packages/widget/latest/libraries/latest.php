<?php

class Latest
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('latest');				
	}

	//back end installation functions
	function install()
	{
		
		$config['latest_module'] = '';

		$this->CI->settings_model->save('latest', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('latest');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('latest');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		//$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['latest_module']))
		{
			$data['modules']=@unserialize($settings['latest_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('latest', array('latest_module'=>serialize($_POST['latest_module'])));
			return false;
		}
	}
	
	function view($settings)
	{

		$this->CI->load->model('product/product_model');		
		$data['products']=$this->CI->product_model->getProducts(array('status'=>1,'delete_flag'=>0,'sold'=>0),'product_id desc',array('limit'=>$settings['limit']))->result_array();		
		return $this->CI->load->view('extensions/latest',$data,TRUE);
	}	
}