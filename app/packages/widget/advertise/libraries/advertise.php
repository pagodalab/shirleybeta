<?php

class Advertise
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('advertise');				
	}

	//back end installation functions
	function install()
	{
		
		$config['advertise_module'] = '';

		$this->CI->settings_model->save('advertise', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('advertise');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		$this->CI->load->model('advertisement/advertisement_model');
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('advertise');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		$data['advertisements']=$this->CI->advertisement_model->getAdvertisements(array('status'=>1))->result_array();
		//$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['advertise_module']))
		{
			$data['modules']=@unserialize($settings['advertise_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('advertise', array('advertise_module'=>serialize($_POST['advertise_module'])));
			return false;
		}
	}
	
	function view($settings)
	{
		$this->CI->load->model('advertisement/advertisement_model');
		$data['advertisement']=$this->CI->advertisement_model->getAdvertisements(array('advertisement_id'=>$settings['advertise']))->row_array();
		$data['settings']=$settings;
		return $this->CI->load->view('extensions/advertise',$data,TRUE);
	}	
}