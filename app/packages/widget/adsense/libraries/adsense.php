<?php

class Adsense
{
	var $CI;	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		//$this->CI->lang->load('adsense');				
	}

	//back end installation functions
	function install()
	{
		
		$config['adsense_module'] = '';

		$this->CI->settings_model->save('adsense', $config);
	}
	
	function uninstall()
	{
		$this->CI->settings_model->delete('adsense');
	}	

	//admin end form and check functions
	function form($post	= false)
	{
		$this->CI->load->model('googlead/googlead_model');
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->settings_model->get('adsense');
		}
		else
		{

			$settings = $post;
		}

		$data['settings']=$settings;
		$data['modules']=array();
		$data['advertisements']=$this->CI->googlead_model->getGoogleAds()->result_array();
		//$data['layouts'] = $this->CI->Layout_model->get_layouts();
		if(!empty($settings['adsense_module']))
		{
			$data['modules']=@unserialize($settings['adsense_module']);
		}
	
		//retrieve form contents
		return $this->CI->load->view('admin_form',$data, true);
	}	

	function check()
	{	
		$error	= false;
		
		// The only value that matters is currency code.
		//if ( empty($_POST['']) )
			//$error = "<div>You must enter a valid currency code</div>";
					
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			$this->CI->settings_model->save('adsense', array('adsense_module'=>serialize($_POST['adsense_module'])));
			return false;
		}
	}
	
	function view($settings)
	{
		$this->CI->load->model('googlead/googlead_model');
		$data['adsense']=$this->CI->googlead_model->getGoogleAds(array('googlead_id'=>$settings['adsense']))->row_array();
		$data['settings']=$settings;
		return $this->CI->load->view('extensions/adsense',$data,TRUE);
	}	
}