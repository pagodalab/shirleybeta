<?php

/******************************************

US English
Common Language

******************************************/




/*******************

Site front end

*******************/

//common terms
$lang['gender']					= 'gender';
$lang['read_more']              = 'læs mere';

$lang['already_have_account']	= ' Allerede oprettet som bruger? Klik her for at logge ind.';
$lang['subscribe_blog']			= 'følg vores blog og nyhedsbrev';
$lang['subscribe'] 				= 'tilmeld';
$lang['invalid_email']			= 'Invalid Email Address';
$lang['subscribe_blog_sucess']	= 'Thank You for subscribing!';
$lang['other_colors']		= 'Other Colors';

$lang['use_for_register']		= 'Opret en konto til senere brug.';
$lang['order_placed']			= 'din ordre er nu gennemført';
$lang['delivery_method']		= 'Levering Metode';
$lang['webshop_opening']		= 'web shop åbner snart ';
$lang['latest_product']			= 'nye varer';
$lang['search_result']			= 'søgeresultat';
$lang['checkout_details']		= 'checkout detaljer';
$lang['customer']				= 'kunde';
$lang['receiver']				= 'modtager af pakke';
$lang['change_delivery_method']	= 'vælg en anden leverings metode';
$lang['change_payment_method']	= 'vælge en anden betalings metode 

';
$lang['shipping_notes']			= 'Special request';



$lang['sizes']					= 'størrelser';
$lang['categories']				= 'kategorier';
$lang['blogs']					= 'BLOG ';
$lang['new']					= 'Nyt!';
$lang['signup']					= 'opret konto';
$lang['get_account']			= 'få en kundekonto';
$lang['account_features']		= 'ved at oprette en kundekonto, kan du fremover:';
$lang['access_checkout']		= 'Adgang til vores kassen';
$lang['exclusive_offer']		= 'bliv skrevet op til nyheder og eksklusive tilbud.';
$lang['create_account']			= 'opret en kundekonto';
$lang['size']					= 'Størrelse';
$lang['about']					= 'om';
$lang['about_us']				= 'Om os';
$lang['materials']				= 'Materialer';

$lang['welcome']				= 'velkommen';
$lang['follow_social']			= 'følg os';
$lang['add_to_basket']			= 'læg i kurv ';
$lang['sale']					= 'udsalg';
$lang['show_all']				= 'vis alle';
$lang['sort_by']				= 'sorter efter';
$lang['size_guide']				= 'størrelses guide';
$lang['customer_service']		= 'kundeservice';
$lang['payment_security_pay']	= 'Betaling og sikkerhed';
$lang['shipping_cost_terms']	= 'Fragt og vilkår';
$lang['returns']				= 'Returvarer';
$lang['contact_us']				= 'Kontakt os';
$lang['stockists']				= 'Forhandlere';
$lang['home']					= 'hjem';
$lang['comment']				= 'kommenter';
$lang['edit_account']			= 'ændr din konto';
$lang['edit_address']			= 'ændr adresse';
$lang['change_password']		= 'Ændr adgangskode';
$lang['my_wishlist']			= 'min ønskeliste';
$lang['orders']					= 'ordrer';
$lang['view_cart']				= 'se kurv';
$lang['color']					= 'farve';
$lang['quantity']				= 'antal';



$lang['login']					= 'log ind';
$lang['logout']					= 'log ud';
$lang['my_account']				= 'min konto';
$lang['my_downloads']			= 'mine downloads';
$lang['enabled']				= 'aktivér';
$lang['disabled']				= 'deaktiver';
$lang['mode']					= 'ja';
$lang['currency']				= 'valuta';
$lang['active']					= 'Aktiv';
$lang['yes']					= 'Ja';
$lang['no']						= 'nej';
$lang['download_link']			= 'Klik her for at se dine downloads!';
$lang['catalog']				= 'katalog';
$lang['account']				= 'konto';
$lang['close']					= 'lukke';

$lang['empty_cart']				= 'din kurv er tom';
$lang['single_item']			= 'Der er %d element i din indkøbskurv';
$lang['multiple_items']			= 'Der er %d lukke  i din kurv';
$lang['empty_view_cart']		= 'din kurv er tom !';
$lang['your_cart']				= 'din kurv';

$lang['on_sale']				= 'udsalg!';
$lang['related_products_title']	= 'måske vil du også kunne lide...';
$lang['form_submit']			= 'indsend';
$lang['form_register']			= 'registrer';
$lang['form_login']				= 'login';
$lang['reset_password']			= 'nulstil dit password';
$lang['form_search']			= 'Søg';
$lang['form_cancel']			= 'annuller';
$lang['form_add_to_cart']		= 'tilføj kurv';
$lang['form_update_cart']		= 'opdater kurv';
$lang['form_checkout']			= 'checkout';
$lang['form_continue']			= 'fortsæt';
$lang['paypal_form_continue']		= 'Fortsæt med kassen';
$lang['form_previous']			= 'ændr fakturering';
$lang['form_choose']			= 'vælg';
$lang['form_edit']				= 'rediger';
$lang['form_delete']			= 'slet';
$lang['form_search']			= 'søg';
$lang['form_view']				= 'se';
$lang['form_save']				= 'gem';
$lang['form_from']				= 'fra ';
$lang['form_to']				= 'til';

$lang['step_1']					= 'trin 1';
$lang['step_2']					= 'trin 2';
$lang['free_shipping_basic']	= 'gratis fragt ( basis)';

$lang['search']					= 'Søg';
$lang['search_error']			= 'du har ikke indtastet et søgeord';
$lang['amount']					= 'beløb';
$lang['custom_amount']			= 'Brugerdefineret Beløb';
$lang['preset_amount']			= 'forudindstillet mængde';
$lang['recipient_name']			= 'modtager navn';
$lang['recipient_email']		= 'modtager email';
$lang['sender_email']			= 'Sender Email';
$lang['custom_greeting']		= 'Brugerdefineret Hilsen Message';
$lang['check_out']				= 'checkout';

$lang['sort_by_name_asc']		= 'sorter efter navne fra A-Å';
$lang['sort_by_name_desc']		= 'Sorter efter navne fra Å-A';
$lang['sort_by_price_asc']		= 'sorter efter pris; lav til høj';
$lang['sort_by_price_desc']		= 'sorter efter pris ; høj til lav';

$lang['products']				= 'produkter';
$lang['out_of_stock']			= 'ikke på lager';
$lang['no_products']			= 'der er ingen produkter i denne kategori i øjeblikket.';
$lang['no_image_available']		= 'intet billede tilgængeligt';
$lang['product_reg']			= 'reg :';
$lang['product_price']			= 'pris :';
$lang['product_sale']			= 'pris:';
$lang['available_options']		= 'valgmuligheder ';
$lang['choose_option']			= 'vælg en af disse ';
$lang['tab_description']		= 'beskrivelse ';
$lang['tab_related_products']	= 'lignende produkter';
$lang['subcategories']			= 'under kategorier';
$lang['edit_category']			= 'ændr kategori';
$lang['edit_product']			= 'ændr produkt';

$lang['loading']					= 'Loading&hellip;';
$lang['coupon_label']				= 'indtast din kode, hvis du har en kupon:';
$lang['gift_card_label']			= 'indtast din kode, hvis du har et gavekort:';
$lang['apply_gift_card']			= 'benyt gavekort';
$lang['apply_coupon']				= 'Anvend kupon';
$lang['customer_information']		= 'kundeinformation';
$lang['error_save_payment']			= 'der opstod et problem i forbindelse med valg af betalingsform';
$lang['error_choose_payment']		= 'vælg venligst betalingsform';
$lang['error_choose_shipping']		= 'vælg venligst forsendelse';
$lang['continue_shopping']			= 'fortsæt med at shoppe';
$lang['register_now']				= 'tilmeld dig nu';
$lang['submit_order']				= 'Fortsæt med paypal';
$lang['edit_customer_information']	= 'rediger kunde information ';
$lang['communication_error']		= 'der opstod et uforudset server problem.';
$lang['no_payment_needed']			= 'betaling ikke nødvendig.';
$lang['choose_payment_method']		= 'valg af forsendelse';
$lang['shipping_method']			= 'Forsendelse metode';
$lang['no_shipping_needed']			= 'Din bestilling indeholder ikke nogen produkter, som kræver shipping.';
$lang['payment_information']		= 'betalings information';
$lang['payment_method']				= 'betalingsmetode';
$lang['submit_payment_method']		= 'vælg betalingsmetode';
$lang['product_information']		= 'produkt information ';
$lang['price_and_quantity']			= 'pris &amp; Mængde';
$lang['sku']						= 'style kode';
$lang['name']						= 'navn';
$lang['price']						= 'pris';
$lang['description']				= 'beskrivelse';
$lang['quantity']					= 'antal';
$lang['totals']						= 'total';
$lang['group_discount']				= 'Gruppe Rabat';
$lang['subtotal']					= 'Subtotal';
$lang['cart_total']					= 'Total';
$lang['coupon_discount']			= 'rabatkode';
$lang['gift_card_discount']			= 'gavekort';
$lang['discounted_subtotal']		= 'total rabat';
$lang['shipping']					= 'forsendelse';
$lang['tax']						= 'tax';
$lang['taxes']						= 'Skatter';
$lang['grand_total']				= 'Grand Total';
$lang['address_manager']			= 'adresse info';
$lang['address_form']				= 'adresse formular';
$lang['your_addresses']				= 'dine adresser';
$lang['address_button']				= 'ændr adresse';
$lang['shipping_address_button']	= 'ændre betalings adresse';
$lang['billing_address_button']		= 'ændr forsendelses metode';
$lang['shipping_method_button']		= 'ændr betalings metode';
$lang['billing_method_button']		= 'Skift faktureringsmetode';
$lang['shipping_and_billing']		= 'Forsendelse &amp; faktureringsoplysninger';
$lang['billing_address']			= 'betalings adresse';
$lang['choose_billing_address']		= 'vælg betalings adresse';
$lang['use_address_for_shipping']	= 'denne adresse er modtager adresse';
$lang['shipping_address']			= 'modtager adresse';
$lang['shipping_information']		= 'modtager information';
$lang['billing_information']		= 'vælg modtager adresse ';
$lang['choose_shipping_address']	= 'vælg adressee';
$lang['choose_address']				= 'Vælg Adresse';
$lang['ship_to_address']			= 'send til denne adresse';
$lang['contact_info']				= 'kontakt information';
$lang['address_company']			= 'firma';
$lang['address']					= 'adresse linje';
$lang['address1']					= 'adresse linje 1';
$lang['address2']					= 'adresse linje 2';
$lang['address_firstname']			= 'fornavn';
$lang['address_lastname']			= 'efternavn';
$lang['address_email']				= 'email adresse';
$lang['address_phone']				= 'telefon nummer';
$lang['address_city']				= 'by';
$lang['address_zip']				= 'postnummer';
$lang['address_country']			= 'land';
$lang['address_state']				= 'stat';
$lang['message_address_saved']		= 'din adresse er gemt!';

$lang['default']					= 'Standard';
$lang['additional_order_details']	= 'Yderligere Ordredetaljer';
$lang['additional_details']			= 'Yderligere detaljer';
$lang['heard_about']				= 'Hvor har du hørt om os?';
$lang['shipping_instructions']		= 'Shipping Notes';

//checkout confirmation
$lang['thank_you']					= 'Tak for shopping med os '; // terminates with company/site name, defined in gocart config as 'company_name'

//authorize.net library messages
$lang['please_fix_errors']				= 'Ret følgende fejl:';
$lang['enter_card_name']				= 'Indtast venligst dit fornavn og efternavn, som det vises på kortet';
$lang['invalid_card_exp']				= 'Udløbsdatoen synes ikke at være gyldig';
$lang['invalid_card_num']				= 'Kortet indtastede nummer er ikke et gyldigt kreditkortnummer';
$lang['enter_card_code']				= 'Indtast det trecifrede sikkerhedskode på bagsiden af kortet';
$lang['transaction_declined']			= 'Transaktion Afvist. Tjek venligst dine kortoplysninger, og prøv igen.';
$lang['enter_test_mode_credentials']	= 'Du skal indtaste login-værdier for TEST-tilstand';
$lang['enter_live_mode_credentials']	= 'Du skal indtaste login-værdier for LIVE-tilstand';
$lang['all_required_fields']			= 'Udfyld alle obligatoriske felter.';
$lang['card_number']					= 'kortnummer';
$lang['expires_on']						= 'udløber den';
$lang['cvv_code']						= 'CVV-kode';

//giftcards
$lang['giftcard']					= 'Gavekort';
$lang['giftcard_choose_amount']		= 'Vælg gavekort Beløb';
$lang['giftcard_custom_amount']		= 'Brugerdefineret Beløb';
$lang['giftcard_to']				= 'til (navn)';
$lang['giftcard_email']				= 'til (Email)';
$lang['giftcard_from']				= 'fra';
$lang['giftcard_message']			= 'Personlig besked';
$lang['giftcard_excerpt']			= 'Gavekort til en ven<br/>til: %s';

$lang['email']						= 'email';
$lang['password']					= 'adgangskode';
$lang['confirm_password']			= 'bekræft adgangskode';
$lang['return_to_login']			= 'gå tilbage til login';
$lang['forgot_password']			= 'glemt adgangskode?';
$lang['keep_me_logged_in']			= 'Forbliv logget ind!';
$lang['register']					= 'register';
$lang['login_failed']				= 'autentificering mislykkedes!';
$lang['go_to_login']				= 'gå til login';

$lang['account_registration']			= 'opret en konto';
$lang['registration_thanks']			= 'tak fordi du oprettede en konto hos %s!';
$lang['error_email']					= 'denne email er allerede oprettet i vores system.';
$lang['error_no_account_record']		= 'vi kan desværre ikke finde dig i vores system.';
$lang['message_new_password']			= 'et ny adgangskode er sendt til din email adresse.';
$lang['message_account_updated']		= 'din konto er nu opdateret';

$lang['error_invalid_shipping_method']		= 'vælg venligst en gyldig leveringsmetode.';
$lang['delete_address_confirmation']		= 'er du sikker på, du gerne vil slette denne adresse?';
$lang['error_must_have_address']			= 'Du skal lade mindst 1 adresse i adressefeltet Manager.';
$lang['account_information']				= 'kontooplysninger';
$lang['account_company']					= 'Firma';
$lang['account_firstname']					= 'Fornavn';
$lang['account_lastname']					= 'efternavn';
$lang['account_email']						= 'Email';
$lang['account_phone']						= 'tlf nr';
$lang['account_password']					= 'adgangskode';
$lang['account_confirm']					= 'bekræfte adgangskode';
$lang['account_newsletter_subscribe']		= 'tilmeld dig vores nyhedsbrev';
$lang['account_password_instructions']		= 'hvis du ikke vil ændre din adgangskode, så lad begge felter være tomme.';
$lang['default_billing']					= 'Default Bølling';
$lang['default_shipping']					= 'Default Shipping';
$lang['order_history']						= 'tidligere ordrer';
$lang['order_date']							= 'bestilt d.';
$lang['order_number']						= 'ordre nummer';
$lang['order_status']						= 'status';
$lang['add_address']						= 'Tilføj adresse';
$lang['no_order_history']					= 'der er ingen tidligere ordrer registreret.';

$lang['referral']							= 'henvisninger';
$lang['is_gift']							= 'gave';
$lang['gift_note']							= 'Gift Note';
$lang['order_details']						= 'ordre detaljer';

//cart library messages
$lang['coupon_not_apply']					= 'den rabat kode, du har indtastet, kan ikke bruges til de valgte varer.';
$lang['coupon_applied']						= 'din rabat kode er hermed registreret og anvendt.';
$lang['coupon_invalid']						= 'beklager, men din rabat kode er ikke gyldig.';
$lang['coupon_already_applied']				= ' koden er  korrekt anvendt og rabatten fratrukket.';
$lang['invalid_coupon_code']				= 'du bør kunne se dit gavekorts saldo nu.';
$lang['giftcard_balance_applied']			= 'dit gavekorts kode er ikke korrekt.';
$lang['giftcard_zero_balance']				= 'dette gavekort er allerede blevet anvendt.';
$lang['giftcard_not_exist']					= 'Gavekortet kode, du indtastede findes ikke.';
$lang['giftcard_already_applied']			= 'dette gavekort er allerede blevet anvendt';
$lang['error_updating_cart']				= 'der opstod en fejl, under opdateringen af din kurv . Vi beklager, prøv venligst igen';
$lang['not_enough_stock']					= 'vi har desværre ikke det nævnte antal på lager. Vælg venligst et lavere antal eller email os for custom ordre..';
$lang['remove_item']						= 'er du sikker op, du gerne vil slette denne vare?';

$lang['card_notice'] = 'du bliver omdirigeret til paypals betalingsside. Her kan du betale med kort og behøver ikke at oprette en paypal konto.';
$lang['amount_not_enough_for_checkout'] 	= 'idet vi benytter os af kurer med den bedste service for vores kunder, betaler vi halvdelen af din forsendelses pris, og derfor har vi en minimumsordre på ';
$lang['withoutpaypal'] = 'du kan betale med kort på Paypals side. Du behøver ikke en paypal konto';

$lang['missingyoursize'] = 'Mangler du en str?';
$lang['missing_size_text'] = '<p> fandt du ikke den størrelse, du skulle bruge? Vi kan strikke den til dig! Det koster ikke ekstra. </p> <p> Vi har 200 fantastisk dygtige strikkedamer engageret i Kathmandu, under fair forhold, der strikker hjemmefra. Hvis vi har garnet på lager, kan vi lave en custom bestilling til dig indenfor 3 uger. </p> <p> ( Bemærk at det ikke er muligt med special ordrer i september og oktober, der der er en stor festival igang i Kathmandu; Dashain, og alle vores didis( strikkedamer) fester med familien ) </p> <p> Klik på linket herunder og udfyld vores form eller mail os på info@shirleybredal.com. </p>';

$lang['common_washing'] = 'vask';
$lang['washing_desc_text'] = '<p>Washing instructions for best care of your hand knitted item : dry clean or gentle handwash/ programme (max 30 degrees ) without spinning , do not wring/twist the wet piece. Roll it into a towel to squeeze out excess water and always dry it flat.</p>';

$lang['restock_notify_text'] = 'Indtast venligst din email og vi kontakter dig, når dette produkt er på lager igen eller kan reserveres';
