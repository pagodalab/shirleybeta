<?php

$lang['paypal_express']				= 'PayPal Express Payment';

//paypal library messages
$lang['paypal_error']				= 'There was an error processing your payment through PayPal';
$lang['paypal_desc']				= 'You will be directed to the safest payment in the Market:Paypal.You do not need to have an account.Please click on the submit button and pay normally by card.';

//paypal admin
$lang['pp_business_email']			= 'Paypal Business Email';
$lang['pp_password']				= 'Paypal API Password';
$lang['pp_key']						= 'Paypal API Signature';

$lang['test_mode_label']			= 'Mode';
$lang['test_mode']					= 'Test (Sandbox)';
$lang['live_mode']					= 'Live (Production)';

$lang['currency_label']				= 'USD, EUR, etc.';