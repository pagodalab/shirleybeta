<?php

/******************************************

US English
Common Language

******************************************/




/*******************

Site front end

*******************/

//common terms
$lang['gender']					= 'gender';
$lang['read_more']              = 'Read more';

$lang['already_have_account']	= 'Already have an account? Click here to login.';
$lang['subscribe_blog']			= 'Subscribe to our blog';
$lang['subscribe'] 				= 'Subscribe';
$lang['invalid_email']			= 'Invalid Email Address';
$lang['subscribe_blog_sucess']	= 'Thank You for subscribing!';
$lang['other_colors']		= 'Other Colors';	
	
$lang['use_for_register']		= 'Create an account for future use';
$lang['order_placed']			= 'Your order has been successfully submitted';
$lang['delivery_method']		= 'Delivery Method';
$lang['webshop_opening']		= 'Web Shop Opening Soon';
$lang['latest_product']			= 'Latest Product';
$lang['search_result']			= 'Search Result';
$lang['checkout_details']		= 'Checkout Details';
$lang['customer']				= 'Customer';
$lang['receiver']				= 'Receiver';
$lang['change_delivery_method']	= 'Change Delivery Method';
$lang['change_payment_method']	= 'Change Payment Method';
$lang['shipping_notes']			= 'Special request';

$lang['sizes']					= 'sizes';
$lang['categories']				= 'categories';
$lang['blogs']					= 'BLOG';
$lang['new']					= 'NEW!';
$lang['signup']					= 'SIGNUP';
$lang['get_account']			= 'Get an Account';
$lang['account_features']		= 'By creating an account, you will be able to:';
$lang['access_checkout']		= 'Access to our checkout';
$lang['exclusive_offer']		= 'Sign up for our exclusive news and offers.';
$lang['create_account']			= 'Create Account';
$lang['size']					= 'Size';
$lang['about']					= 'About';
$lang['about_us']				= 'About us';
$lang['materials']				= 'Materials';


$lang['welcome']				= 'WELCOME';
$lang['follow_social']			= 'FOLLOW US ON';
$lang['add_to_basket']			= 'Add to Basket';
$lang['sale']					= 'sale';
$lang['show_all']				= 'show all';
$lang['sort_by']				= 'Sort By';
$lang['size_guide']				= 'Size guide';
$lang['customer_service']		= 'Customer Service';
$lang['payment_security_pay']	= 'Payment & Security';
$lang['shipping_cost_terms']	= 'Shipping Cost & Terms';
$lang['returns']				= 'Returns';
$lang['contact_us']				= 'Contact us';
$lang['stockists']				= 'Stockists';
$lang['home']					= 'Home';
$lang['comment']				= 'comment';
$lang['edit_account']			= 'Edit Account';
$lang['edit_address']			= 'Edit Address';
$lang['change_password']		= 'Change Password';
$lang['my_wishlist']			= 'My Wishlist';
$lang['orders']					= 'Orders';
$lang['view_cart']				= 'View Cart';
$lang['color']					= 'Color';
$lang['quantity']				= 'Quantity';


$lang['login']					= 'LOG IN';
$lang['logout']					= 'Log out';
$lang['my_account']				= 'My Account';
$lang['my_downloads']			= 'My Downloads';
$lang['enabled']				= 'Enabled';
$lang['disabled']				= 'Disabled';
$lang['mode']					= 'Mode';
$lang['currency']				= 'Currency';
$lang['active']					= 'Active';
$lang['yes']					= 'Yes';
$lang['no']						= 'No';
$lang['download_link']			= 'Click here to access your downloads!';
$lang['catalog']				= 'Catalog';
$lang['account']				= 'Account';
$lang['close']					= 'Close';

$lang['empty_cart']				= 'Your cart is empty';
$lang['single_item']			= 'There is %d item in your cart';
$lang['multiple_items']			= 'There are %d items in your cart';
$lang['empty_view_cart']		= 'There are no products in your cart!';
$lang['your_cart']				= 'Your Cart';

$lang['on_sale']				= 'On Sale!';
$lang['related_products_title']	= 'You might also like...';
$lang['form_submit']			= 'Submit';
$lang['form_register']			= 'Register';
$lang['form_login']				= 'Login';
$lang['reset_password']			= 'Reset Password';
$lang['form_search']			= 'Search';
$lang['form_cancel']			= 'Cancel';
$lang['form_add_to_cart']		= 'Add to Cart';
$lang['form_update_cart']		= 'Update Cart';
$lang['form_checkout']			= 'Checkout';
$lang['form_continue']			= 'Continue';
$lang['paypal_form_continue']		= 'Continue to Checkout';
$lang['form_previous']			= 'Edit Billing';
$lang['form_choose']			= 'Choose';
$lang['form_edit']				= 'Edit';
$lang['form_delete']			= 'Delete';
$lang['form_search']			= 'Search';
$lang['form_view']				= 'View';
$lang['form_save']				= 'Save';
$lang['form_from']				= 'From';
$lang['form_to']				= 'to';

$lang['step_1']					= 'Step 1';
$lang['step_2']					= 'Step 2';
$lang['free_shipping_basic']	= 'Free Shipping (basic)';

$lang['search']					= 'Search';
$lang['search_error']			= 'You did not supply a search term';
$lang['amount']					= 'Amount';
$lang['custom_amount']			= 'Custom Amount';
$lang['preset_amount']			= 'Preset Amount';
$lang['recipient_name']			= 'Recipient Name';
$lang['recipient_email']		= 'Recipient Email';
$lang['sender_email']			= 'Sender Email';
$lang['custom_greeting']		= 'Custom Greeting Message';
$lang['check_out']				= 'Check Out';

$lang['sort_by_name_asc']		= 'Sort by name A to Z';
$lang['sort_by_name_desc']		= 'Sort by name Z to A';
$lang['sort_by_price_asc']		= 'Sort by price Low to High';
$lang['sort_by_price_desc']		= 'Sort by price High to Low';

$lang['products']				= 'Products';
$lang['out_of_stock']			= 'Out of Stock';
$lang['no_products']			= 'There are currently no available products in this category.';
$lang['no_image_available']		= 'No Image Available';
$lang['product_reg']			= 'reg:';
$lang['product_price']			= 'Price:';
$lang['product_sale']			= 'SALE:';
$lang['available_options']		= 'Available Options';
$lang['choose_option']			= 'Choose an Option';
$lang['tab_description']		= 'Description';
$lang['tab_related_products']	= 'Related Products';
$lang['subcategories']			= 'Subcategories';
$lang['edit_category']			= 'Edit Category';
$lang['edit_product']			= 'Edit Product';

$lang['loading']					= 'Loading&hellip;';
$lang['coupon_label']				= 'If you have a coupon, enter the code here:';
$lang['gift_card_label']			= 'If you have a Gift Certificate, enter the code here:';
$lang['apply_gift_card']			= 'Apply Gift Card';
$lang['apply_coupon']				= 'Apply Coupon';
$lang['customer_information']		= 'Customer Information';
$lang['error_save_payment']			= 'There was a problem saving your payment method';
$lang['error_choose_payment']		= 'Please choose a payment method';
$lang['error_choose_shipping']		= 'Please choose a shipping method';
$lang['continue_shopping']			= 'Continue Shopping';
$lang['register_now']				= 'Register Now';
$lang['submit_order']				= 'Continue with Paypal';
$lang['edit_customer_information']	= 'Edit Customer Information';
$lang['communication_error']		= 'There was an unexpected problem communicating with the server.';
$lang['no_payment_needed']			= 'There is no payment necessary.';
$lang['choose_payment_method']		= 'Choose Payment Method';
$lang['shipping_method']			= 'Shipping Method';
$lang['no_shipping_needed']			= 'Your order does not include any items that require shipping.';
$lang['payment_information']		= 'Payment Information';
$lang['payment_method']				= 'Payment Method';
$lang['submit_payment_method']		= 'Submit Payment Method';
$lang['product_information']		= 'Product Info';
$lang['price_and_quantity']			= 'Price &amp; Quantity';
$lang['sku']						= 'SKU';
$lang['name']						= 'Name';
$lang['price']						= 'Price';
$lang['description']				= 'Description';
$lang['quantity']					= 'Quantity';
$lang['totals']						= 'Total';
$lang['group_discount']				= 'Group Discount';
$lang['subtotal']					= 'Subtotal';
$lang['cart_total']					= 'Total';
$lang['coupon_discount']			= 'Coupon Discount';
$lang['gift_card_discount']			= 'Gift Card Discount';
$lang['discounted_subtotal']		= 'Discounted Subtotal';
$lang['shipping']					= 'Shipping';
$lang['tax']						= 'Tax';
$lang['taxes']						= 'Taxes';
$lang['grand_total']				= 'Grand Total';
$lang['address_manager']			= 'Address Manager';
$lang['address_form']				= 'Address Form';
$lang['your_addresses']				= 'Your Addresses';
$lang['address_button']				= 'Change Address';
$lang['shipping_address_button']	= 'Change Shipping Address';
$lang['billing_address_button']		= 'Change Billing Address';
$lang['shipping_method_button']		= 'Change Shipping Method';
$lang['billing_method_button']		= 'Change Billing Method';
$lang['shipping_and_billing']		= 'Shipping &amp; Billing Information';
$lang['billing_address']			= 'Billing Address';
$lang['choose_billing_address']		= 'Choose Your Billing Address';
$lang['use_address_for_shipping']	= 'Use this address for shipping';
$lang['shipping_address']			= 'Shipping Address';
$lang['shipping_information']		= 'Shipping Information';
$lang['billing_information']		= 'Billing Information';
$lang['choose_shipping_address']	= 'Choose Your Shipping Address';
$lang['choose_address']				= 'Choose Address';
$lang['ship_to_address']			= 'Ship to this address';
$lang['contact_info']				= 'Contact Info';
$lang['address_company']			= 'Company';
$lang['address']					= 'Payment and Billing address';
$lang['address1']					= 'Address 1*';
$lang['address2']					= 'Address 2';
$lang['address_firstname']			= 'First Name*';
$lang['address_lastname']			= 'Last Name*';
$lang['address_email']				= 'Email*';
$lang['address_phone']				= 'Phone*';
$lang['address_city']				= 'City*';
$lang['address_zip']			= 'Zip';
$lang['address_country']			= 'Country';
$lang['address_state']				= 'State';
$lang['address_zone_id']            = 'State';
$lang['message_address_saved']		= 'Your address has been saved!';

$lang['default']					= 'Default';
$lang['additional_order_details']	= 'Additional Order Details';
$lang['additional_details']			= 'Additional Details';
$lang['heard_about']				= 'How did you hear about us?';
$lang['shipping_instructions']		= 'Shipping Notes';

//checkout confirmation
$lang['thank_you']					= 'Thanks for shopping with '; // terminates with company/site name, defined in gocart config as 'company_name'

//authorize.net library messages
$lang['please_fix_errors']				= 'Please fix the following errors:';
$lang['enter_card_name']				= 'Please enter your first and last name as it appears on the card';
$lang['invalid_card_exp']				= 'The expiration date does not appear to be valid';
$lang['invalid_card_num']				= 'The card number you entered is not a valid credit card number';
$lang['enter_card_code']				= 'Please enter the three digit security code on the reverse side of the card';
$lang['transaction_declined']			= 'Transaction Declined. Please check your card information and try again.';
$lang['enter_test_mode_credentials']	= 'You must enter login values for TEST mode';
$lang['enter_live_mode_credentials']	= 'You must enter login values for LIVE mode';
$lang['all_required_fields']			= 'Please complete all required fields.';
$lang['card_number']					= 'Card Number';
$lang['expires_on']						= 'Expires On';
$lang['cvv_code']						= 'CVV Code';

//giftcards
$lang['giftcard']					= 'Gift Card';
$lang['giftcard_choose_amount']		= 'Choose Gift Card Amount';
$lang['giftcard_custom_amount']		= 'Custom Amount';
$lang['giftcard_to']				= 'To (Name)';
$lang['giftcard_email']				= 'To (Email)';
$lang['giftcard_from']				= 'From';
$lang['giftcard_message']			= 'Personal Message';
$lang['giftcard_excerpt']			= 'Gift Card for a friend<br/>To: %s';

$lang['email']						= 'Email';
$lang['password']					= 'Password';
$lang['confirm_password']			= 'Confirm Password';
$lang['return_to_login']			= 'Return to Login';
$lang['forgot_password']			= 'Forgot Password';
$lang['keep_me_logged_in']			= 'Keep me logged in!';
$lang['register']					= 'Register';
$lang['login_failed']				= 'Authentication Failed!';
$lang['go_to_login']				= 'Go to Login';

$lang['account_registration']			= 'Account Registration';
$lang['registration_thanks']			= 'Thanks for registering %s!';
$lang['error_email']					= 'The requested email is already in use.';
$lang['error_no_account_record']		= 'There is no record of your account.';
$lang['message_new_password']			= 'A new password has been generated and sent to your email.';
$lang['message_account_updated']		= 'Your account has been updated';

$lang['error_invalid_shipping_method']		= 'Please select a valid shipping method.';
$lang['delete_address_confirmation']		= 'Are you sure you want to delete this address?';
$lang['error_must_have_address']			= 'You Must leave at least 1 address in the Address Manager.';
$lang['account_information']				= 'Account Information';
$lang['account_company']					= 'Company';
$lang['account_firstname']					= 'First Name';
$lang['account_lastname']					= 'Last Name';
$lang['account_email']						= 'Email';
$lang['account_phone']						= 'Phone';
$lang['account_password']					= 'Password';
$lang['account_confirm']					= 'Confirm Password';
$lang['account_newsletter_subscribe']		= 'Subscribe to our email list';
$lang['account_password_instructions']		= 'If you do not wish to change your<br/>password, leave both fields blank.';
$lang['default_billing']					= 'Default Billing';
$lang['default_shipping']					= 'Default Shipping';
$lang['order_history']						= 'Order History';
$lang['order_date']							= 'Ordered On';
$lang['order_number']						= 'Order Number';
$lang['order_status']						= 'Status';
$lang['add_address']						= 'Add Address';
$lang['no_order_history']					= 'You have no orders in your account history.';

$lang['referral']							= 'Referals';
$lang['is_gift']							= 'Gift';
$lang['gift_note']							= 'Gift Note';
$lang['order_details']						= 'Order Details';

//cart library messages
$lang['coupon_not_apply']					= 'The coupon code you entered does not apply to any products in your cart.';
$lang['coupon_applied']						= 'The coupon has been applied to your products.';
$lang['coupon_invalid']						= 'Sorry, but the coupon you entered is not valid at this time.';
$lang['coupon_already_applied']				= 'That coupon code has already been applied to your items.';
$lang['invalid_coupon_code']				= 'There was no coupon with that code. Check to make sure you entered it correctly.';
$lang['giftcard_balance_applied']			= 'You should see your card balance displayed.';
$lang['giftcard_zero_balance']				= 'Sorry, but the gift card you entered is expired or has a zero balance.';
$lang['giftcard_not_exist']					= 'The gift card code you entered does not exist.';
$lang['giftcard_already_applied']			= 'Your gift card is already applied';
$lang['error_updating_cart']				= 'There was an error updating the cart!';
$lang['not_enough_stock']					= 'The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.';
$lang['remove_item']						= 'Are you sure you want to remove this item from your cart?';

$lang['card_notice'] = ' You will be directed to paypals payment site. Paypal offers normal card payments without having a paypal account.';
$lang['amount_not_enough_for_checkout'] 	= 'As we are using the best courier service available and contributing to part of that cost, we have a minimum order of :';
$lang['withoutpaypal'] = "You can pay with card on Paypal's payment site . You don't need a paypal account.";

$lang['missingyoursize'] = "Missing your size?";
$lang['missing_size_text'] = '<p> Didn\'t find the size you were looking for? We can knit it for you! There is no extra charge. </p> <p> We have 200 women engaged knitting for us under fair conditions, from home. If we have the yarn in stock, we can make a custom order for you in 3 weeks. Please use the contact form below or email us at info@shirleybredal.com. </p> <p> (In September and October custom orders are not possible. There is a big festival in Kathmandu called Dashain and everyone is celebrating) </p>';
$lang['common_washing'] = 'Washing';

$lang['common_washing'] = 'Washing';
$lang['washing_desc_text'] = '<p>Washing instructions for best care of your hand knitted item : dry clean or gentle handwash/ programme (max 30 degrees ) without spinning , do not wring/twist the wet piece. Roll it into a towel to squeeze out excess water and always dry it flat.</p>';

$lang['restock_notify_text'] = 'Please provide us your email and we will be in touch when the items becomes available to reserve or purchase.';
