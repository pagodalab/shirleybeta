</div>
<!-- ------------------------------- Content Wrapper Ends ------------------------------- -->


<!-- ------------------------------- Restock Notification Modal ------------------------------- -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog" id="restock">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">RESTOCK NOTIFICATION</h4>
			</div>
			<div class="modal-body">               
				<p><?php echo lang('restock_notify_text'); ?></p>
				<br>
				<form id="restock_notificatioin" method="post">
					<input type="email" id="notify_email" name="email" class="form-control unfancy" placeholder="Enter your email" required="true">
					<input type="hidden" name="size" id="restock_size_id">
					<input type="hidden" name="product_id" id="restock_product_id">         
					<br/>
					<div class="pull-right">
						<button type="button" id="submit-notify" class="btn-shirley unfancy btn-sm">Submit</button>              
						<button type="button" class="btn-shirley unfancy btn-sm" data-dismiss="modal">Close</button>
					</div>
				</form>
				<!-- modal body end -->
			</div>
			<!-- modal content end -->
		</div>
		<!-- modal dialog end -->
	</div>
	<!-- myModal end -->
</div>

<script type="text/javascript">
	function send(product,size,quantity,product_id) {
		if(quantity == 0 || quantity == ''){
			restockNotification(product_id,size);
		}else{
			window.location.href = '<?php echo site_url()?>/'+product+'?reqsize='+size;
		}
	}
</script>

<script type="text/javascript">

	$('#submit-notify').click(function(){
		var dialog = document.getElementById('window');  
		var email = $('#notify_email').val();
		var size = $('#restock_size_id').val();
		var product_id = $('#restock_product_id').val();

		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(email) == false){
			$('#notify_email').focus();
			return;
		}

		$.post('<?php echo site_url("home/insert_email_notificatoin")?>',{email_notify:email,product_size:size,product_id:product_id},function(data){
			if(data.success){
				$('body').css("overflow", "visible");
				alert(data.msg);
				$('#myModal').modal('hide');
			} else {
				alert(data.msg);
			}
		},'JSON');
	});


	function restockNotification(product_id, size_id) {
		$('#myModal').modal('show');

		$('#restock_size_id').val(size_id);
		$('#restock_product_id').val(product_id);

	}
</script>
<!-- ------------------------------- Restock Notification Modal ENDS ------------------------------- -->


<!-- ------------------------------- Footer Wrapper Starts ------------------------------ -->
<div id="footer-wrapper">
	<div class="custom-container container-small-width footer-custom-container">
		<div class="footer-top-row">
			<div class="ft-about ft-tooltipper">
				<a href="<?php echo site_url('about-us') ?>">About Us</a>
				<div class="footer_tooltip">
					<div class="_text">
						<div>
							<p>Visit this page to read Our Story book and watch our video, to learn about how we work, including who we work with and steps taken towards sustainability.</p>
						</div>
					</div>
				</div>
				
			</div>
			<div class="ft-logo"><a href="<?php echo site_url() ?>"><img src="<?php echo theme_url('assets/img/logo_icons/logo.png') ?>"></a> </div>
			<div class="ft-cust ft-tooltipper">
				<a href="<?php echo site_url('page/index/11') ?>">Customer Service </a>
				<div class="footer_tooltip">
					<div class="_text">
						<ul>
							<li><a href="<?php echo site_url('customer-service') ?>#size-guide">Size Guide</a></li>
							<!--<li><a href="<?php echo site_url('customer-service') ?>#">Shipping Rates</a></li>-->
							<li><a href="<?php echo site_url('customer-service') ?>#custom-order">Custom Order Form</a></li>
							<li><a href="<?php echo site_url('customer-service') ?>#contact-form">Contact Form</a></li>
							<li><a href="<?php echo site_url('customer-service') ?>#stockists">List of Stockists</a></li>
							<li><a href="<?php echo site_url('customer-service') ?>#payment-and-security-pay1">Guidance on Payments and Security</a></li>
							<li><a href="<?php echo site_url('customer-service') ?>#returns1">Returns Policy</a></li>
							<li><a href="<?php echo site_url('customer-service') ?>#wholesaler-application-form">Wholesaler Application Form</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom-row">
			<h2>Payment Method</h2>
			<ul>
				<li><img src="<?php echo theme_url('assets/img/logo_icons/visa.png') ?>"> </li>
				<li><img src="<?php echo theme_url('assets/img/logo_icons/paypal.png') ?>"> </li>
				<li><img src="<?php echo theme_url('assets/img/logo_icons/master-card.png') ?>"> </li>
				<li><img src="<?php echo theme_url('assets/img/logo_icons/maestro-card.png') ?>"> </li>


			</ul>
		</div>
	</div>
</div>
<!-- -------------------------------- Footer Wrapper Ends ------------------------------- -->


</div>
<!-- ----------------------------------- Wrapper Ends ----------------------------------- -->


<script src="<?php echo theme_url('assets')?>/js/bootstrap.min.js"></script>
<script src="<?php echo theme_url('assets')?>/js/jquery.equalheights.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo theme_url('assets')?>/js/slick.min.js"></script>
<script src="<?php echo theme_url('assets')?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo theme_url('assets')?>/js/thescripts.js"></script>
<?php echo theme_js('jquery.elevatezoom.js', true);?>
<script>
	(function() {

		[].slice.call( document.querySelectorAll( '.si-icons-default > .si-icon' ) ).forEach( function( el ) {
			var svgicon = new svgIcon( el, svgIconConfig );
		} );

		// new svgIcon( document.querySelector( '.si-icons-easing .si-icon-plus' ), svgIconConfig, { easing : mina.backin } );

	})();

	// $('section svg').click(function () {
	// 	$('#main-menu').toggleClass('active');
	// });


	// (function() {
	// 	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {  
	// 		new SelectFx(el);
	// 	} );
	// })();

	// $('.bxslider').bxSlider({
	// 	pagerCustom: '#bx-pager'
	// });
</script>
<script>
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {  
			new SelectFx(el);
		} );
	})();
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$('.your-class').slick({
			dots: true,
			autoplay: true,
		});
		$('.thesubmenu ul li.subcategory_active').each(function(){
			$(this).parent().closest('li').addClass('category_active');
		});
	});
</script>
<script>
	$('#subscribe-submit').on('click',function(){

		var email = $('#subscribe-email').val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");  

		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length)
		{
			$('#subscribe-msg').text('<?php echo lang('invalid_email');?>!').css('color','red');

			return false;
		}
		else
		{

			$.ajax({
				type: "POST",
				url: "<?php echo site_url('subscribe/save');?>",
				data: $('#subscribe-form').serialize(),
				dataType : 'json',
				success: function(data) {

					if(data.success)
					{
						$('#subscribe-msg').text('Thanks for subscribing!').css('color','#a7a7a7');
						$('#subscribe-form').trigger('reset');
// $('#subscribeModal').modal('show');
}  
// $.post('<?php echo site_url('booking/paypal_auto_form');?>',{},function(data){
//    $('#paypalView').html(data);
// });

}
});
		}

		return false;
	});

	$('#subscribe-submit-footer').on('click',function(){


		var email = $('#subscribe-email-footer').val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");  

		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length)
		{
			$('#subscribe-msg-footer').text('<?php echo lang('invalid_email');?>!').css('color','red');

			return false;
		}
		else
		{

			$.ajax({
				type: "POST",
				url: "<?php echo site_url('subscribe/save');?>",
				data: $('#subscribe-form-footer').serialize(),
				dataType : 'json',
				success: function(data) {

					if(data.success)
					{
						$('#subscribe-msg-footer').text('<?php echo lang('subscribe_blog_sucess');?>!').css('color','white');
						$('#subscribe-form-footer').trigger('reset');
						// $('#subscribeModal').modal('show');
					}  
					// $.post('<?php echo site_url('booking/paypal_auto_form');?>',{},function(data){
						// $('#paypalView').html(data);
					// });

				}
			});
		}

		return false;
	});
</script>
<script>
	$(document).ready(function(){

		var CurrencyCounter=getCookie("CurrencyCounter");
		if (CurrencyCounter==0) 
		{
			$.get('<?php echo site_url('home/todays_rate_eur')?>',null,function(data){

				var today = '<?php echo date('Y-m-d')?>';

			},'json');

			setCookie('CurrencyCounter', 1, 1);
			var CurrencyCounter=getCookie("CurrencyCounter");

		}
		else
		{
			return false;
		}


	}); 

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
		}
		return "";
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*3600 * 1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function add_to_cart(product_id,product_name,event, gc)
	{
		console.log(gc);

		var size = $('.size-'+product_id).val();

		if (gc === undefined) {
			gc = 0;
		}
		if(size == '' && gc == 0)
		{

			alert('Please Select a size');
			event.preventDefault();
		}  

		else
		{

			var data = $('#form-'+product_id).serialize();
			console.log(data);
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('cart/add');?>",
				data: $('#form-'+product_id).serialize(),
				dataType : 'json',

				success: function(data) {

					$('#cartMessage').html(''+product_name+' has been successfully added to your cart');
					$('#cartModal').modal('show');

				}
			});
			event.preventDefault();
		}
	}

</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-67870910-1', 'auto');
	ga('send', 'pageview');

</script>
<script>
	$('.menu-open').click(function(e) {

		if ($('.themenu').hasClass('open')) {

			$('.themenu, .menu-opener, .thesubmenu').removeClass('open');

		}

		else {
			$('.themenu').addClass('open');
		}

	});

	$('.menu-opener').click(function(e) {
		$(this).toggleClass('open');
		$(this).parent('li').children('.thesubmenu').toggleClass('open');
	});
</script>
<script>
	function validateCheckout()
	{

		var checkout_amount = <?php echo ($this->go_cart->total())?$this->go_cart->total():0 ?>;
		if(checkout_amount && checkout_amount >= 199 )
		{ 
			$('#redirect_path').val('checkout');
			$('#update_cart_form').submit();
		}
		else
		{
			$('#checkoutModal').modal();
			return false;
		}
	}
</script>
<script type="text/javascript">
	function sizeSet(qty,size,id)
	{   
		var dialog = document.getElementById('window');  
		if(qty > 0)
		{
			$('.size-'+id).val(size);

		}
		else
		{


			$('#myModal').modal('show');
			$('#product_size1').val(size);
			$('#product_id1').val(id);

		}  
	}

	$('.size-btn').on("click",function(e){


		return false;

		e.preventDefault;
	});



	$('#restock_notificatioin').keypress(
		function(event){
			if (event.which == '13') {
				event.preventDefault();
			}
		});

	function getFormData(formId) {
		return $('#' + formId).serializeArray().reduce(function (obj, item) {
			var name = item.name,
			value = item.value;

			if (obj.hasOwnProperty(name)) {
				if (typeof obj[name] == "string") {
					obj[name] = [obj[name]];
					obj[name].push(value);
				} else {
					obj[name].push(value);
				}
			} else {
				obj[name] = value;
			}	
			return obj;
		}, {});
	}
</script>
<script> 

	window['adrum-start-time']= new Date().getTime(); 

	window['adrum-app-key'] = '<app-key-for-this-app>'; 

</script> 
<script src="<?php echo theme_url('assets')?>/js/adrum.js"></script>

</html>