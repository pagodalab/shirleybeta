 <div id="options">
    <div id="products">
        <div id="catalog">
        	<div>
                <ul>
				<?php foreach($products as $product){?>
                    <li style="list-style:none;float:left;margin:2px">
                        <?php
                        $photo  = theme_img('no_picture.png', lang('no_image_available'));
                        $product->images    = array_values($product->images);
            
                        if(!empty($product->images[0]))
                        {
                            $primary    = $product->images[0];
                            foreach($product->images as $photo)
                            {
                                if(isset($photo->primary))
                                {
                                    $primary    = $photo;
                                }
                            }

                            $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"  data-cat="'.$product->category_name.'" data-id="'.$product->id.'" height="100px" width="100px" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'"/>';
                        }
                        
                        echo $photo;
                        ?>
                    </li>
                <?php }?> 
            </ul>
           </div>     
        </div>
    </div>
</div>

<script>

$(function(){
	
	$( "#catalog li img" ).draggable({
	  //appendTo: "body",
      helper: "clone",
	  containment:"#scrap-template"
    });
	
	
});

</script>