 <section>
            <div class="second-page-container">
                <div class="container">
                    <div class="row">

                        <div class="col-md-9">
                            <div class="block-breadcrumb">
                               <ul class="breadcrumb">
                                   
                                </ul>
                            </div>

                            <div class="header-for-light">
							   <h1 class="wow fadeInRight animated" data-wow-duration="1s"><?php //echo $template['canvas_title'];?></h1>
							</div>
 <div class="block-product-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	<div style="float:left">
    	<img src="<?php echo base_url('uploads/canvas/user_canvas/templates/'.$template['canvas_image'])?>">
    </div>
   </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


                                        <div class="product-detail-section">
    	<h3 align="left">Products</h3>
        <form action="<?php echo site_url('cart/add_canvas_products');?>" method="post">
    	<?php $total = '';
		 foreach($products as $product){
		?>
            <div style="float:left;margin:10px;">
            	<input type="checkbox" checked="checked" name="id[<?php echo $product->product_id?>]" id="product<?php echo $product->product_id?>" value="<?php echo $product->product_id?>" onclick="toggleProduct(<?php echo $product->product_id?>)">
                <input type="hidden" name="product_price[]" id="product_price<?php echo $product->product_id?>" value="<?php echo $product->sort_price?>">
				<?php
					$photo  = theme_img('no_picture.png', lang('no_image_available'));
                    $product->images    = array_values($product->images);
                
                    if(!empty($product->images[0]))
                    {
                        $primary    = $product->images[0];
                        foreach($product->images as $photo)
                        {
                            if(isset($photo->primary))
                            {
                                $primary    = $photo;
                            }
                        }
                
                        $photo  = '<img class="responsiveImage" src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" />';
                    }
                    
                ?>
                <a href="<?php echo site_url($product->slug)?>" target="_blank"><?php echo $photo;?></a>
                <br/>
                <?php echo $product->product_name;?>
                <br/>
                Rs.<?php echo $product->sort_price;
					$total+=$product->sort_price;
				?>
                <?php 
					$options	= $this->option_model->get_product_options($product->product_id);
					$posted_options	= $this->session->flashdata('option_values');
					if(count($options) > 0):  
					foreach($options as $option):
						$required   = '';
						if($option->required)
						{
							$required = ' <p class="help-block">Required</p>';
						}
				?>
                            <div class="control-group">
                                <label class="control-label"><?php echo $option->name;?></label>
                                <?php
                                /*
                                this is where we generate the options and either use default values, or previously posted variables
                                that we either returned for errors, or in some other releases of Cart the user may be editing
                                and entry in their cart.
                                */

                                //if we're dealing with a textfield or text area, grab the option value and store it in value
                                if($option->type == 'checklist')
                                {
                                    $value  = array();
                                    if($posted_options && isset($posted_options[$option->id]))
                                    {
                                        $value  = $posted_options[$option->id];
                                    }
                                }
                                else
                                {
                                    if(isset($option->values[0]))
                                    {
                                        $value  = $option->values[0]->value;
                                        if($posted_options && isset($posted_options[$option->id]))
                                        {
                                            $value  = $posted_options[$option->id];
                                        }
                                    }
                                    else
                                    {
                                        $value = false;
                                    }
                                }

                                if($option->type == 'textfield'):?>
                                    <div class="controls">
                                        <input type="text" name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>]" value="<?php echo $value;?>" class="span2"/>
                                        <?php echo $required;?>
                                    </div>
                                <?php elseif($option->type == 'textarea'):?>
                                    <div class="controls">
                                        <textarea class="span2" name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>]"><?php echo $value;?></textarea>
                                        <?php echo $required;?>
                                    </div>
                                <?php elseif($option->type == 'droplist'):?>
                                    <div class="controls">
                                        <select name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>]" required>
                                            <option value=""><?php echo lang('choose_option');?></option>

                                        <?php foreach ($option->values as $values):
                                            $selected   = '';
                                            if($value == $values->id)
                                            {
                                                $selected   = ' selected="selected"';
                                            }?>

                                            <option<?php echo $selected;?> value="<?php echo $values->id;?>">
                                                <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo $values->name;?>
                                            </option>

                                        <?php endforeach;?>
                                        </select>
                                        <?php echo $required;?>
                                    </div>
                                <?php elseif($option->type == 'radiolist'):?>
                                    <div class="controls">
                                        <?php foreach ($option->values as $values):

                                            $checked = '';
                                            if($value == $values->id)
                                            {
                                                $checked = ' checked="checked"';
                                            }?>
                                            <label class="radio">
                                                <input<?php echo $checked;?> type="radio" name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
                                                <?php echo($values->price != 0)?'(+'.format_currency($values->price).') ':''; echo $values->name;?>
                                            </label>
                                        <?php endforeach;?>
                                        <?php echo $required;?>
                                    </div>
                                <?php elseif($option->type == 'checklist'):?>
                                    <div class="controls">
                                        <?php foreach ($option->values as $values):

                                            $checked = '';
                                            if(in_array($values->id, $value))
                                            {
                                                $checked = ' checked="checked"';
                                            }?>
                                            <label class="checkbox">
                                                <input<?php echo $checked;?> type="checkbox" name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
                                                <?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
                                            </label>
                                            
                                        <?php endforeach; ?>
                                    </div>
                                    <?php echo $required;?>
								<?php elseif($option->type == 'calendar'):?>  
                                    <div class="controls">
                                        <input type="text" name="option[<?php echo $product->product_id?>][<?php echo $option->id;?>]" value="<?php echo $value;?>" class="span2 calendar"/>
                                        <?php echo $required;?>
                                    </div>                                                                  
                                <?php endif;?>
                                </div>
				<?php endforeach; endif;?>
            </div>
       <?php }?>
       <div style="clear:both"></div>
       <div id="canvas-total">
       		<h3><strong>Total :</strong> Rs.<span id="total"><?php echo $total?></span></h3>
            <br/>
            <button class="btn btn-inverse btn-large" type="submit" value="submit" ><i class="fa fa-shopping-cart"></i> Add to Cart</button>
       </div>
       </form>
    </div>
     </div>
                                </div>



                            </div>  
            </div>
</div></div></div>
        </section>

<script>
var total = <?php echo $total?>;
function toggleProduct(id)
{
	var product_price = $('#product_price'+id).val();
	var is_checked = $('#product'+id).is(':checked');
	if(is_checked == true)
	{
		total = parseFloat(total)+parseFloat(product_price);
		$('#total').text(total);
		$('#product_total').val(total);
	}
	else
	{
		total = total-product_price;
		$('#total').text(total);
		$('#product_total').val(total);
	}
}

</script>