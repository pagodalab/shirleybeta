<div class="row">
    <div class="span12">
        <div class="category_container">
        	<h3>Recent Templates</h3>
            <div class="row">
           	<?php foreach($templates as $template){?>
                <div class="span3 product">
                	<div class="product-image">
                    	<img src="<?php echo base_url('uploads/canvas/templates/'.$template['image'])?>">
                    </div>
                    <p><a href="#"><?php echo $template['template_name']?></a></p>
                </div>
            <?php }?>
            </div>
        </div>

    </div>
    </div>
</div>
