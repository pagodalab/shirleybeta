

$(document).ready(function () {
    smootScroll();

}); /*Document ready end*/




$(window).scroll(function(){
    var wScroll = $(this).scrollTop();

    if(wScroll >= $('.title-and-info-section').offset().top) {
        $('.left-nav').addClass('make-visible');
    }else {
        $('.left-nav').removeClass('make-visible');
    }
});


$(window).scroll(function() {
    var windscroll = $(window).scrollTop();
    if (windscroll >= 100) {
        $('fieldset section').each(function(i) {
            if ($(this).position().top <= windscroll + 100) {
                $('.left-nav li.selected').removeClass('selected');
                $('.left-nav li').eq(i).addClass('selected');
            }
        });

    } else {
        $('.left-nav li.selected').removeClass('selected');
        $('.left-nav li:first').addClass('selected');
    }

})

function classChange() {

}

function smootScroll(){
    $('.left-nav a[href^="#"]').on('click',function (e) {
        e.preventDefault();
        $(".left-nav li").removeClass("selected");
        $(this).parents("li").addClass("selected");
        var target = this.hash;
        $target = $(target);
            $('html, body').stop().animate({
                'scrollTop':  $target.offset().top //no need of parseInt here
                }, 900, 'swing', function () {
                window.location.hash = target;
            });
    });
}