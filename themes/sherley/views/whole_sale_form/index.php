<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title><?php echo $page_title; ?></title>


	<!-- Icons start -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-128.png" sizes="128x128" />

	<!-- Importing Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,700|Open+Sans:300,400,700" rel="stylesheet">
	<!-- Importing Fonts Ends -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo theme_url('views/whole_sale_form/css') ?>/style.css">

</head>

<body>
	<div class="content-wrapper">
		<div class="container">
			<aside class="left-nav">
				<ul>
					<li class="selected"><a id="" href="#first-section">Tender Application</a></li>
					<li><a id="" href="#second-section">Our Collection</a></li>
					<li><a id="" href="#third-section">What we have Achieved</a></li>
					<li><a id="" href="#fourth-section">Wholesale Selection Process</a></li>
					<li><a id="" href="#fifth-section">About You</a></li>
				</ul>
			</aside>

			<section class="logo-section">
				<div class="container">
					<img src="<?php echo theme_url('assets/img/logo_icons') ?>/logo.png" alt="shirley">
				</div>
			</section>

			<fieldset>
				<section class="title-and-info-section" id="first-section">

					<div class="section-title">
						<h1><?php echo lang('wholesale_title') ?></h1>
					</div>
					<p>
						Retailers (shopfront or web) with a serious interest in becoming a wholesale customer of Shirley Bredal products can express their interest by completing the application form below. Wholesale Customer applications are reviewed on a periodic basis as production capacity increases.
					</p>

				</section>

				<section class="common-text-section" id="second-section">

					<div class="section-title">
						<h1>Our Collection</h1>
					</div>

					<div class="section-content">
						<p>Shirley Bredal started our collection in Denmark in 2006. The collection is now based in Kathmandu, operating as a social enterprise.</p>

						<p>Our collection offers a stable, classic range of clothing, hand-knitted by women in Nepal with fine wool. Our focus is delivering high-quality, comfort and timeless pieces that can be handed down through generations.
						With our signature merino yarn from Mongolia, we produce a large Autumn/Winter collection. We have a limited Summer collection in cotton.</p>

						<p>A few new items pop-up each year in both the Autumn/Winter and Summer collections and for special occasions throughout the year.</p>

						<p>Winding and dying is handled in Kathmandu using Swiss dye, an AZO free environmental dye.</p>
					</div>

				</section>

				<section class="common-text-section" id="third-section">

					<div class="section-title">
						<h1>A little bit about what we have achieved so far</h1>
					</div>

					<div class="section-content">
						<p>Currently, we supply retailers in Australia, Canada, China, Denmark, France, Germany, Ireland, Kuwait, the Netherlands, New Zealand and Norway and sell directly to customers via our online shop.</p>
						<p>Around 200 women from minority groups in Kathmandu Valley have been trained in knitting, quality control, product finishings and production management. The women we work with are mainly housewives from low-income households who knit from their own homes to create a side income for their families. Some also reside in refuges and rely on this work for their income.</p>
						<p>All knitters work on a part-time basis, according to their availability and are paid a fair wage. Wages are also scaled against the standard time taken to knit each style, according to the thickness of the wool and size of the item.</p>
					</div>

				</section>

				<section class="common-text-section" id="fourth-section">

					<div class="section-title">
						<h1>Wholesale selection process</h1>
					</div>

					<div class="section-content">
						<p>Our work is very personal. We aim to build long-term relationships with the women we work with and help them by ensuring the longevity of our business. To do this we partner with retailers who are able to market our clothing line at a high-price range to minimise the risk of sudden competition with clothing of cheaper quality. </p>
					</div>

				</section>

				<section class="question-section" id="fifth-section">
					<div class="section-title">
						<h1>About You</h1>
					</div>

					<div class="section-content">
						<p>Please provide details about your company, so we may review your interest in partnering with us as a wholesaler: </p>

						<div class="questions-container">
							<form id="form-whole_sale_form" action="<?php echo site_url('whole_sale_form/save_whole_sale_form')?>" method="POST">

								<div class="question-item radio">
									<h3>Retail Type: <span>*</span></h3>

									<label>
										<input type="radio" name="retail_type" value="Shopfront only" required> <span>Shopfront only</span>
									</label>
									<label>
										<input type="radio" name="retail_type" value="Webshop only"> <span>Webshop only</span>
									</label>

									<label>
										<input type="radio" name="retail_type" value="Shopfront and Webshop"> <span>Shopfront and Webshop</span>
									</label>
								</div>

								<div class="question-item">
									<h3>Full Company Name: <span>*</span></h3>
									<input type="text" name="name" placeholder="Full Company Name" required>
								</div>

								<div class="question-item">
									<h3>Company e-mail address: <span>*</span></h3>
									<input type="email" name="email" placeholder="Company e-mail address" required>
								</div>

								<div class="question-item">
									<h3>Company Phone Number(s):  <span>*</span></h3>
									<input type="number" name="phone" placeholder="Company Phone Number(s)" min="0" required>
								</div>

								<div class="question-item">
									<h3>Shop address: <span>*</span></h3>
									<input type="text" name="address" placeholder="Shop address" required>
								</div>


								<div class="question-item radio">
									<h3>Shop location: <span>*</span></h3>

									<label>
										<input type="radio" name="shop_location" value="Busy retail zone, along a main street/square" required > <span>Busy retail zone, along a main street/square</span>
									</label>
									<label>
										<input type="radio" name="shop_location" value="Inside a shopping centre in the city centre"> <span>Inside a shopping centre in the city centre</span>
									</label>

									<label>
										<input type="radio" name="shop_location" value="Inside a shopping centre in a suburban area"> <span>Inside a shopping centre in a suburban area</span>
									</label>

									<label>
										<input type="radio" name="shop_location" value="Boutique shop in a suburban area"> <span>Boutique shop in a suburban area</span>
									</label>

									<label>
										<input type="radio" name="shop_location" value="Boutique shop in a rural area"> <span>Boutique shop in a rural area</span>
									</label>

									<label>
										<input type="radio" name="shop_location" value="Not applicable - Webshop only"> <span>Not applicable - Webshop only</span>
									</label>

								</div>

								<div class="question-item">
									<h3>Online shop address: <span>*</span></h3>
									<input type="text" name="online_address" placeholder="Online shop address" required >
								</div>

								<div class="question-item">
									<h3>Company Establishment Year:  <span>*</span></h3>
									<input type="text" name="establish_year" placeholder="Company Establishment Year" required >
								</div>

								<div class="question-item">
									<h3>Five Biggest Brands Retailed via your Company:<span>*</span></h3>
									<input type="text" name="brands_retailed" placeholder="Your Answer" required >
								</div>

								<div class="question-item">
									<h3>Reasons why you believe your company is compatible with our brand and collection:<span>*</span></h3>
									<input type="text" name="reasons" placeholder="Your Answer" required>
								</div>


								<div class="form-action">
									<button type="submit">Submit</button>
								</div>
							</form>
						</div>
					</div>

				</section>
			</fieldset>
		</div>
	</div>

	<div id="modal-form_submitted" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">WholeSale Application Submitted</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Your wholesale application has been successfully submitted. 
					Many thanks for your application and we will be in touch soon!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Done</button>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

	<!-- Custom JS -->
	<script src="<?php echo theme_url('views/whole_sale_form/js') ?>/theScripts.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			var justSubmitted = Cookies.get('whole_sale_form-submitted');
			if (justSubmitted) {
				$("#modal-form_submitted").modal();
				Cookies.remove('whole_sale_form-submitted');
			}

			$("#form-whole_sale_form").submit(function(e) {
				Cookies.set('whole_sale_form-submitted', 'true', { expires: 7 });
			});

		});
	</script>


</body>
</html>