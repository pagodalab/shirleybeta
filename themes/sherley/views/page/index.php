<style type="text/css">
.about-body-section h1 {
    margin-bottom: 15px;
}

</style>

<div class="about-page co-new-page remove-top-space">

    <!-- <section class="co-banner-section">
        <div class="banner-background" style="/*background-image: url('<?php echo theme_url('assets')?>/img/banner/banner-common-page.jpg') */">
            <div class="banner-content">
                <h1><?php $heading = 'title_'.$this->culture_code;?><?php echo $page->$heading;?></h1>
            </div>
        </div>
    </section> -->

    <section>
        <h1 class="page_title text-center text-uppercase"><?php $heading = 'title_'.$this->culture_code;?><?php echo $page->$heading;?></h1>
    </section>


    <section class="about-body-section">
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php $content = 'content_'.$this->culture_code; ?>
                    <?php echo  $page->$content; ?>
                </div>
            </div>
        </div>
    </section>

    <?php if($page->id == 1): ?>
        <section class="about-body-section">
            <div class="custom-container container-small-width">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h1>Our Team</h1>
                        <!-- ISSUU page here -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <iframe style="width:100%; height:500px;" src="http://e.issuu.com/embed.html#21132039/55919722" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </section>

        <?php //print_r($subpages); ?>
        <?php foreach ($subpages as $key => $value):  ?>
            <section class="about-body-section">
                <div class="custom-container container-small-width">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h1><?php echo $value["title_{$this->culture_code}"] ?></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <?php //$content = 'content_'.$this->culture_code; ?>
                            <?php echo  $value["content_{$this->culture_code}"]; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endforeach; ?>
    <?php endif; ?>


    <?php if($page->id == 11): ?>
        <?php foreach ($subpages as $key => $value):  ?>
            <section class="about-body-section" id="<?php echo $value['slug'] ?>"> 
                <div class="custom-container container-small-width">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h1><?php echo $value["title_{$this->culture_code}"] ?></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <?php //$content = 'content_'.$this->culture_code; ?>
                            <?php echo  $value["content_{$this->culture_code}"]; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endforeach; ?>
    <?php endif; ?>

</div>


