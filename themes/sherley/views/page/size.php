<style>
table, th, td {
    border: 1px solid #d7d7d7;
    border-collapse: collapse;
    color: #8d8d8d;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 {
    width: 100%;    
    background-color: #f1f1c1;
}
</style>
<div id="content-wrapper">
    
<div class="container">
<div class="row">
    <div class="col-sm-10">
         <div class="listing">
        <a href="<?php echo site_url();?>"><?php echo lang('home');?> > </a>
        <a href="">Size guide</a>
        
         
        
      
             <p class="sherley-clothing">Clothing Sizes</p>
          </div>
        
    
        <p class="sherley-leftwork">The following size guide is indicative.<br>
            </p>
        
        <div class="sherley-table">
            <p>Baby clothes 0-2 Years</p>
            
            <table style="width:90%">
  <tr>
    <th>The Child's age in months</th>
    <th>0-1</th>		
    <th>0-1</th>
       <th>2-4</th>
       <th>4-6</th>
       <th>6-9</th>
       <th>9-12</th>
       <th>18</th>
       <th>24</th>
  </tr>
  <tr>
    <th>Size of Clothes(Child's Height)</th>
      <th>50-56</th>
      <th>56</th>
      <th>62</th>
      <th>68</th>
      <th>74</th>
      <th>80</th>
      <th>86</th>
      <th>92</th>
  </tr>

</table>

        </div>  
        
        <br>
        
         <div class="sherley-table">
            <p>Children clothes 0-2 Years</p>
            
            <table style="width:90%">
  <tr>
    <th>The Child's age in months</th>
    <th>3</th>		
    <th>4</th>
       <th>5</th>
       <th>6</th>
       <th>7</th>
       <th>8</th>
       <th>9</th>
       <th>10</th>
       <th>11</th>
       <th>12</th>
  </tr>
  <tr>
     <th>Size of Clothes (child's height in cm)</th>
      <th>98</th>
      <th>104</th>
      <th>110</th>
      <th>116</th>
      <th>122</th>
      <th>128</th>
      <th>134</th>
      <th>140</th>
      <th>146</th>
      <th>152</th>
  </tr>

</table>

        </div>  
          
        <br>    
   <!--- <div class="sherley-lasttext">
        <h4>Shoe sizes</h4>
        
        <p>

It is important that the shoe fits the child's foot, and therefore the child's feet measured before buying shoes.
Under each shoe model are the internal dimensions of the specific model.
The child's feet measured easiest way to let the child stand with his back against a wall. Make  that the heel hits the wall and measure the distance
from the wall and out of the child's longest toe.
It is recommended that the shoes purchased with 1 to 1.5 cm in adult supplements. That is, the shoe's  dimensions must be 1 to 1.5 cm longer
than the child's foot.
        </p>
        
        <a href=""><img src="<?php echo theme_url('assets/img/legimage.jpg');?>"></a>
        </div>--->    
    </div>
    
     <div class="col-sm-2">
            <div class="about-right">
            <!---<h5>CATEGORIES</h5>--->
            <hr>
            </div>    
              
             <?php /*?>
                <?php $categories = $this->menu_category ;?>
        <?php foreach($categories as $category):?>
           <ul class="inline">
              <div class="rotater"> 
               <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category['id']?>" aria-expanded="true" aria-controls="collapse<?php echo $category['id']?>">              
               <a href="<?php echo site_url($category['slug']);?>">
                    <?php echo $category['name_'.$this->culture_code];?>
                  </a>
               </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
                 <div class="panel panel-default" style="margin: 0;">
                   <div id="collapse<?php echo $category['id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $category['id']?>" style="height: auto;">
                       <?php $subcategories = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$category['id']))->result_array();?>
                       <?php if(count($subcategories) >0 ):?>
                        <div class="panel-body">
                              <ul class="body">
                             
                                        <?php foreach($subcategories as $subcategory):?>
                                 <li><img src="<?php echo theme_url('assets/img/star.png');?>">
                                    <a href="<?php echo site_url($subcategory['slug']);?>">
                                      <?php echo $subcategory['name_'.$this->culture_code];?>
                                    </a>
                                  </li>
                                  
                                  <?php endforeach;?>
                            </ul>
                        </div>
                  <?php endif;?>
                  </div>
                </div>
               </div>  
               
          </ul>
         <?php endforeach;?>
         <?php */?>
        </div>    
    
      
    
          
        
    
      </div>
    </div>
</div>