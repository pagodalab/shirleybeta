<style>
#content-wrapper {
    padding-top: 97px;
}

#header-wrapper .header-navigation{
    background: transparent;
    -webkit-transition: all .2s ease;
    -moz-transition: all .2s ease;
    -ms-transition: all .2s ease;
    -o-transition: all .2s ease;
    transition: all .2s ease;
}

#header-wrapper .header-navigation{
    background: rgba(255, 255, 255, 0.53);
}

#header-wrapper .header-navigation .main-menu ul li a {
    color: #3e3636;
    font-weight: 600;
}

@media all and ( max-width:  1599px) {
    #content-wrapper {
        padding-top: 85px;
    }

    #header-wrapper .header-navigation{
        -webkit-transition: all .2s ease;
        -moz-transition: all .2s ease;
        -ms-transition: all .2s ease;
        -o-transition: all .2s ease;
        transition: all .2s ease;
    }

    #header-wrapper .header-navigation .main-menu ul li a {
        color: #3e3636;
        font-weight: 600;
    }
}

@media all and ( max-width:  1199px) {
    #content-wrapper {
        padding-top: 76px;
    }
}

@media all and ( max-width:  1199px) {
    #content-wrapper {
        padding-top: 67px;
    }
}

</style>

<div class="home-page co-new-page remove-top-space">
    <section class="koala-section">
        <img src="<?php echo theme_url('assets')?>/img/gif/koala-with-balloon.gif">
    </section>

<!--     <section class="koala-section-left">
        <img src="<?php echo theme_url('assets')?>/img/gif/4.png">
    </section> -->

    <section class="home-banner-section">
        <div class="home-banner-slider">
            <?php foreach ($banners as $key => $value) {?>
            <div class="home-banner-item" style="background-image: url('<?php echo base_url('uploads/banners/'.$value->image)?>')">
                <div class="banner-content">
                    <div class="banner-back">
                        <h1><?php echo $value->name; ?></h1>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </section>


    <section class="home-body-section">
        <div class="nice_things">
            <div class="nice_thing" data-num="5">
                <img src="<?php echo theme_url('assets/img/blackstar.png'); ?>" />
            </div>
            <div class="nice_thing" data-num="5">
                <img src="<?php echo theme_url('assets/img/gingerman.png'); ?>" />                
            </div>
            <div class="nice_thing" data-num="5">
                <img src="<?php echo theme_url('assets/img/sugacane.png'); ?>" />                
            </div>
        </div>
        <div class="custom-container container-small-width-home">
            <div class="art-item">
                <div class="left-art co-padding-block">
                    <div class="image-container item-story">
                     <a href="<?php echo site_url('about-us') ?>">
                         <img src="<?php echo theme_url('assets')?>/img/home_page/story.png">
                         <figcaption class="text-center">Our Story</figcaption>
                     </a>
                 </div>

             </div>
             <div class="right-art co-padding-block">
                <div class="image-container item-flower">
                    <img src="<?php echo theme_url('assets')?>/img/home_page/flower.png">
                </div>
            </div>
        </div>

        <div class="art-item">
            <div class="left-art co-padding-block">
                <div class="image-container item-new">
                    <div class="home-new-product-slider">

                        <?php foreach($feature_slider as $key=>$value): ?>
                            <?php $image = array_values(json_decode($value['images'],true)); ?>
                            <div class="home-new-product-item">
                                <a href="<?php echo site_url($value['slug']); ?>">
                                    <img class="product-gif-img" src="<?php echo base_url('uploads/images/medium/'.$image[0]['filename']) ?>">
                                    <img class="new-gif-icon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png">
                                </a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
            <div class="right-art co-padding-block">
                <div class="image-container item-custom-order">
                    <a href="<?php echo site_url('custom_order') ?>">
                        <img src="<?php echo theme_url('assets')?>/img/home_page/custom-order.png">
                        <figcaption>Custom Order</figcaption>
                    </a>
                </div>
            </div>
        </div>

        <div class="art-item double-height">
            <div class="left-art co-padding-block">
                <div class="image-container item-bottom-left">
                    <div class="image-container item-sale">
                        <a href="<?php echo site_url('category/SaleProduct') ?>">
                            <img src="<?php echo theme_url('assets')?>/img/home_page/sale.png">
                        </a>
                    </div>

                    <div class="newsletter-item">
                        <div class="newsletter-form">
                            <h1>Sign up for newsletter</h1>
                            <form  id='subscribe-form-footer'>
                                <input type="email" name="email" placeholder="Enter Your Email Address" id="subscribe-email-footer" class="subscribe">
                                <button type="submit" id="subscribe-submit-footer" value="<?php echo lang('subscribe');?>">Sign Up</button>
                                <div id="subscribe-msg-footer"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-art co-padding-block">
                <div class="image-container item-koala">
                    <?php if(count($featured_category)>0){?>
                    <!-- href="<?php echo site_url($featured_category['slug'])?>" -->
                    <a href="<?php echo site_url('rompers-shorts-and-pants')?>">
                        <img src="<?php echo theme_url('assets')?>/img/home_page/home-main-koala.png">
                    </a>
                    <?php }else{?>
                    <a href="">
                        <img src="<?php echo theme_url('assets')?>/img/home_page/home-main-koala.png">
                    </a>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="instagram-section">
    <div class="custom-container container-small-width-home">
        <h2><?php echo ($this->culture_code == 'en')?"Follow Instagram":"F&oslash;lg p&acirc; Instagram"; ?> @shirleybredal</h2>
        <!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/0431c9f1bdca50778f1d210cb196ca23.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
    </div>
</section>
</div>



<script>

    $(window).scroll(function(){
        var wScroll = $(this).scrollTop();

        if(wScroll >= $('.home-body-section').offset().top) {
            $('body').addClass('nav-background');
        }
        if(wScroll < $('.home-body-section').offset().top) {
            $('body').removeClass('nav-background');
        }

        if ($(this).scrollTop() > 10) {
            $('.koala-section').addClass('scroll');
        } else {
            $('.koala-section').removeClass('scroll');
        }

        if ($(this).scrollTop() > 10) {
            $('.koala-section-left').addClass('scroll');
        } else {
            $('.koala-section-left').removeClass('scroll');
        }
    });


    //RANDOMNESS

    //RANDOMNESS
    $(document).ready(function(){
     var christmas = <?php echo $this->christmas['enable'] ?>;
     if(christmas == 1){

        var getRandomRotation = function() {
            var degree = Math.floor(Math.random() * 360);
            if(degree < 90 || degree > 270) {
                return degree;
            } else {
                getRandomRotation();
            }
        }

        $('.nice_thing').each(function(i, e) {

            var num = parseInt($(e).data('num'));
            var content = $(e).html();


            for(var i = 0; i < num; i++) {
                var randomClone = $('<div class="nice_thing clone">'+content+'</div>');
                var left = Math.floor(Math.random() * 80) + 0;
                var top = Math.floor(Math.random() * 80) + 0;
                var rotate = getRandomRotation();

                $(randomClone).css({
                    left: left+'%',
                    top: top+'%',
                    transform: 'rotate('+rotate+'deg)'
                });
                $('.nice_things').append(randomClone);
            }


        });
    }

});
</script>

<!-- ################ Popup Notice ################ -->
<style>
#notice_popup .modal-body {
    background-color: #fff;
    padding: 0px;
    margin-bottom: 0px;
}
</style>

<!-- Popup Modal -->
<div class="modal fade" id="notice_popup" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body notice_popup">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <?php foreach ($popup_notice as $key => $value): ?>
                    <div><img src="<?php echo base_url('uploads/notices/'.$value['image']); ?>" class='img-responsive'></div>
                <?php endforeach; ?>
            </div>
        </div> 
        <!-- end modal content -->
    </div>
</div> 
<!-- end popup modal -->

<script type="text/javascript">
    $(function(){
        $('.notice_popup').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            // adaptiveHeight: true,
        });
        
        <?php if( ! empty($popup_notice) ) : ?>
        $("#notice_popup").modal('show');
        <?php endif; ?>
    });
</script>
<!-- ################ END Popup Notice ################ -->
