<div id="content-wrapper">
	<div class="container">
	<div class="inner-container">
	<div class="row">
    	<h1>Login</h1>
             <div class="col-md-7 forms row col">
				<?php echo form_open('auth/login', 'class="form-horizontal"'); ?>
                    <div class="col-md-3 col">
                        <label class="control-label" for="email"><?php echo lang('email');?></label><br/>
                        <label class="control-label" for="password"><?php echo lang('password');?></label>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col">
                        <input type="text" name="email" class="form-control" required="required"/><br/>
                        <input type="password" name="password" class="form-control" autocomplete="off" required="required"/>
                        <label class="checkbox">
                              <input name="remember" value="true" type="checkbox" />
                               <?php echo lang('keep_me_logged_in');?>
					</label>
                        <br/>
                        <input type="submit" value="<?php echo lang('form_login');?>" name="submit" class="btn btn-default"/>
                        <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
						<input type="hidden" value="submitted" name="submitted"/>
                              <br/>
                        OR
                        <br/>
                        <?php $redirect_url=$this->input->get('redirect_url');?>
                        <a href="<? echo $this->facebook->getLoginUrl(array('redirect_uri'=>site_url('auth/facebook?redirect_url='.$redirect_url),'scope'=>'email,publish_stream,user_photos,user_birthday,user_hometown,user_interests,user_checkins'))?>" class="btn btn-primary "><span class="glyphicon glyphicon-user"></span> Login Via Facebook</a>
                    </div>
                </form>
            </div>
            
            <div class="col-md-4 col-md-offset-1">
            	<div class="forms">
                	<p><a href="#">Get an Account</a></p><br>
                    <p><span>By creating an account, you will be able to:</span></p>
                    <p>Access to our checkout</p>
                    <p>Sign up for our exclusive news and offers.</p>
                    <a class="btn btn-default" style="float: left;margin-top: 25px;" href="<?php echo site_url('auth/register')?>">Create Account</a>
                </div>
                 
            </div> 
        
        </div>
    </div>
</div>
</div>