            <div id="content-wrapper">
                <div class="container login">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="page-title">Login</h2>   
                        </div>
                        <div class="col-sm-6">                          
                            <form class="login-form" action="<?php echo site_url('auth/login')?>" method="post">
                                <div class="form-group">
                                
                                     <label class="control-label" for="email"><?php echo lang('email');?></label><br/>
                                    <input type="text" name="email" class="form-control" required="required" placeholder="Email"/><br/>
                                </div>
                                <div class="form-group">
                                   <label class="control-label" for="password"><?php echo lang('password');?></label>
                                    <input type="password" name="password" class="form-control" autocomplete="off" required="required" placeholder="Password"/>
                                    
                                </div>
                                <div class="checkbox">
                                    <label class="checkbox">
                              <input name="remember" value="true" type="checkbox" />
                               <?php echo lang('keep_me_logged_in');?>
                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn largebut"><?php echo lang('form_login');?></button>
                                    <?php $redirect_url=($this->input->get('redirect_url'))?$this->input->get('redirect_url'):$redirect ;?>
                        <a href="<? echo $this->facebook->getLoginUrl(array('redirect_uri'=>site_url('auth/facebook?redirect_url='.$redirect_url),'scope'=>'email,publish_stream,user_photos,user_birthday,user_hometown,user_interests,user_checkins'))?>" class="btn largebut facebook"> Login with Facebook</a>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <a href="<?php echo site_url('auth/forgot_password')?>" >Forgot Password</a>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <div class="account-info">
                                <p>By Creating an accoun, you will be ale to access to our Checkout.
                                Sign up for out exclusive news and offers.</p>
                                <a class="btn largebut" type="submit" style="float: left;margin-top: 25px;" href="<?php echo site_url('auth/register')?>">Create Account</a>

                            </div>
                        </div>
                    </div><!--row end-->
                </div>
            </div> <!--Content Wrapper--> 

     

