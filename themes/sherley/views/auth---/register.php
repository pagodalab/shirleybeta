<div id="content-wrapper">
                <div class="container register">
                    <div class="row">
                    <?php
    
    $first      = array('id'=>'bill_firstname' ,'placeholder'=>'First Name', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'),'required'=>'required');
    $last       = array('id'=>'bill_lastname' ,'placeholder'=>'Last Name', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'),'required'=>'required');
    $email      = array('id'=>'bill_email' ,'placeholder'=>'Email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'),'required'=>'required');
    $phone      = array('id'=>'bill_phone','placeholder'=>'Phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'),'required'=>'required');
    ?>
                        <div class="col-sm-12">
                            <h2 class="page-title"><?php echo lang('form_register');?></h2>  
                        </div>
                        <div class="col-sm-8">                          
                           <?php echo form_open('auth/register'); ?>
                <input type="hidden" name="submitted" value="submitted" />
                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-sm-6">
                                      <label for="account_firstname"><?php echo lang('account_firstname');?></label>
                                      <?php echo form_input($first);?>
                                      
                                    </div>
                                    <div class="col-sm-6">
                                         <label for="account_lastname"><?php echo lang('account_lastname');?></label>
                                         <?php echo form_input($last);?>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-sm-6">
                                      <label for="account_email"><?php echo lang('account_email');?></label>
                                      <?php echo form_input($email);?>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="account_phone"><?php echo lang('account_phone');?></label>
                                        <?php echo form_input($phone);?>
                                    </div>
                                    </div>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="email_subscribe" value="1" <?php echo set_radio('email_subscribe', '1', TRUE); ?>/> <?php echo lang('account_newsletter_subscribe');?></label>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-sm-6">
                                      <label for="account_password"><?php echo lang('account_password');?></label>
                                      <input type="password" name="password" placeholder="Password" value="" class="form-control" autocomplete="off" required="required"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="account_confirm"><?php echo lang('account_confirm');?></label>
                                        <input type="password" name="confirm" placeholder="Confirm Password" value="" class="form-control" autocomplete="off" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="<?php echo lang('form_register');?>" class="btn largebut" />&nbsp&nbsp or,&nbsp&nbsp                        
                                    
                                    <a href="<?php echo site_url('auth/login'); ?>"><?php echo lang('go_to_login');?></a>

                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div><!--row end-->
                </div>
            </div> <!--Content Wrapper--> 