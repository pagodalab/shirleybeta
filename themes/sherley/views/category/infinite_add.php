<?php
foreach ($products as $product): ?>
    <?php
    $photo = theme_img('no_picture.png');
    $hover_photo = array();
    if ($product->images) {
        $product->images = array_values($product->images);
        if (!empty($product->images[0])) {
            $primary = $product->images[0];
            foreach ($product->images as $photo) {
                if (isset($photo->primary)) {
                    $primary = $photo;
                }
                $hover_photo[] = base_url('uploads/images/medium/' . $photo->filename);
            }
            $photo = base_url('uploads/images/medium/' . $primary->filename);
        }
    }
    ?>
    <div class="col-md-4 col-sm-6">
        <div class="product-list-item">
            <?php $name = "display_name_" . $this->culture_code;
            if ($product->$name != ''):
                // echo $product->$name;
            else:
                $name = "name_" . $this->culture_code;

            endif;
            ?>
            <a href="<?php echo site_url($product->slug); ?>">
                <div class="_img">
                    <?php if($product->category != 'gift-card'): ?>
                        <a href="<?php echo site_url($product->slug); ?>" >
                            <div class="product-category-page-slider-<?php echo $offset?>">
                                <img src="<?php echo $photo ?>">
                                <?php if($hover_photo){ ?>
                                    <?php foreach($hover_photo as $hover): ?>
                                        <img src="<?php echo $hover ?>">
                                    <?php endforeach; } ?>
                            </div>
                            <?php if ($product->is_new == 1): ?>
                                <img class="new-item-balloon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png"> <!-- new tag -->
                            <?php endif; ?>
                        </a>

                    <?php else: ?>
                        <a href="" onclick="confirm_addto_cart('<?php echo $product->id; ?>','<?php echo htmlentities($product->$name) ?>',event)">
                            <div class="product-category-page-slider">
                                <img src="<?php echo $photo ?>">
                                <?php if($hover_photo){ ?>
                                    <?php foreach($hover_photo as $hover): ?>
                                        <img src="<?php echo $hover ?>">
                                    <?php endforeach; } ?>
                            </div>
                            <?php if ($product->is_new == 1): ?>
                                <img class="new-item-balloon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png"> <!-- new tag -->
                            <?php endif; ?>
                        </a>

                        <script>
                            function confirm_addto_cart(product_id,product_name,event){
                                r = confirm('Do you like to add giftcard in cart?');
                                if(r){
                                    add_to_cart(product_id,product_name,event,1);
                                }
                            }
                        </script>
                    <?php endif; ?>
                </div>
            </a>
            <!-- _img Ends-->

            <div class="_details">
                <div class="details-left-column">
                    <div class="_name">
                        <a href="<?php echo site_url($product->slug); ?>"> <?php $name = "display_name_" . $this->culture_code; ?>
                            <?php if ($product->$name != ''): ?>
                                <?php echo strtoupper($product->$name); ?>
                            <?php else: ?>
                                <?php $name = "name_" . $this->culture_code; ?>
                                <?php echo strtoupper($product->$name); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div id="co-product-size">
                        <form id="form-<?php echo $product->id; ?>" action="" method="post">
                            <ul>
                                <input type="hidden" name="id" value="<?php echo $product->id; ?>"/>
                                <input type="hidden" name="quantity" value="1"/>
                                <?php $items = $this->go_cart->contents(); ?>
                                <?php if ($product->size_quantity){ ?>
                                    <?php //$size_id = explode(',',$product->sizes); ?>
                                    <?php $product_sizes = json_decode($product->size_quantity); ?>
                                    <?php
                                    $j = 0;
                                    $item_size = NULL;
                                    $qty_count = 0;
                                    ?>
                                    <?php foreach ($size_total as $size): ?>
                                        <?php foreach ($product_sizes as $q_size): ?>

                                            <?php
                                            foreach ($items as $item) {
                                                //echo "<pre>".$item['id'];
                                                if ($item['id'] == $product->id && $item['size'] == $q_size->size) {
                                                    $qty_count+=$item['quantity'];
                                                    $item_size = $item['size'];
                                                    //echo "<pre>item_size "; print_r($item_size);
                                                    //echo "<pre>qty_count "; print_r($qty_count);
                                                }
                                            }
                                            $size_status = '';
                                            $size_available = $q_size->quantity;

                                            if ($item_size) {
                                                if ($q_size->size == $item_size) {
                                                    $size_available = intval($q_size->quantity) - intval($qty_count);
                                                    // $size_status = 'disabled="disabled"';
                                                } else {
                                                    $size_available = $q_size->quantity;
                                                }
                                            }
                                            ?>

                                            <?php if ($q_size->size == $size->size_id): ?>
                                                <?php
                                                if ($q_size->quantity <= 0) { /* $size_status = 'disabled="disabled"'; */
                                                }
                                                ?>
                                                <?php if ($j == 0): ?>
                                                    <input type="hidden" name="size" class="size-<?php echo $product->id; ?>" value="<?php //echo $q_size->size   ?>"/>
                                                    <?php $j++; ?>
                                                <?php endif; ?>
                                                <!-- <button class="size-btn" onclick="sizeSet(<?php echo $q_size->quantity ?>,<?php echo $q_size->size ?>,<?php echo $product->id; ?>)" value="<?php echo $q_size->size ?>" <?php echo $size_status ?>>
                                                                                    <span class="size-text <?php echo ($q_size->quantity == 0) ? 'text-through' : ''; ?>"><?php echo $size->size_code; ?></span>
                                                                                </button> -->
                                                <li>
                                                    <button type="button" class="size-btn <?php echo ($q_size->quantity == 0)?'text-through':'';?>" onclick='send(<?php echo '"'.$product->slug.'",'.$q_size->size.','.$q_size->quantity.','.$product->id;?>)' value="<?php echo $q_size->size ?>" >

                                                        <span><?php echo $size->size_code; ?></span>
                                                        <?php  if($q_size->quantity=='0' || $q_size->quantity=='' ){ ?>
                                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                        <?php } ?>
                                                    </button>
                                                </li>
                                            <?php endif; ?>

                                        <?php endforeach; ?>
                                    <?php endforeach;
                                } ?>
                            </ul>
                        </form>
                    </div>
                </div>

                <?php $ex_currency = place_productPrice(); ?>
                <div class="details-right-column">
                    <div class="_price">
                        <?php if(round($product->price) > round($product->saleprice)): ?>
                            <span class="price-cross"><?php echo round(($product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                            <span class="price-new"><?php echo round(((round($product->price) > round($product->saleprice)) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                        <?php else: ?>
                            <span><?php echo round(((round($product->price) > round($product->saleprice)) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                        <?php endif; ?>
                    </div>
                </div>

            </div><!-- Details end -->
        </div> <!-- Product-list-item end-->
    </div> <!-- Col Ends -->
<?php endforeach;?>

<script type="text/javascript">
    // slickInit()
    $('.product-category-page-slider-<?php echo $offset?>').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            autoplay: true,
            autoplaySpeed: 800
        });

    $('.product-category-page-slider-<?php echo $offset?>').slick('slickPause');

    $('.product-category-page-slider-<?php echo $offset?>').hover(
        function () {
            $(this).slick('slickPlay');
        },
        function () {
            $(this).slick('pause');
        }
    );
</script>
