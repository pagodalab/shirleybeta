<div class="category-page co-new-page remove-top-space">
    <?php if ( (!empty($category_detail->image) && $category_detail->parent_id == 0) || ($category_detail->parent_id == 75 && !empty($category_detail->image)) ) { ?>
    <section class="co-banner-section">
        <div class="banner-background" style="background-image: url('<?php echo base_url('uploads/images/full/' . $category_detail->image) ?>')">
        <?php if(! $category_detail->parent_id == 75): ?>
            <div class="banner-content">
                <?php $name = 'name_' . $this->culture_code; ?>
                <h1><?php echo strtoupper($category_detail->$name); ?></h1>
            </div>
        <?php endif;?>
        </div>
    </section>
    <?php } ?>

    <section class="product-list-section add-space-top">
        <div class="custom-container">
            <script>
                $(document).ready(function () {
                    var detailsHeight = 0;
                    $('._details').each(function () {
                        if($(this).height() > detailsHeight) {
                            detailsHeight = $(this).height();
                        }
                    });
                    $('._details').outerHeight(detailsHeight);
                });
            </script>
            <div class="row">
                <?php
                foreach ($products as $product): ?>
                    <?php
                        $photo = theme_img('no_picture.png');
                        $hover_photo = array();
                    if ($product->images) {
                        $product->images = array_values($product->images);
                        if (!empty($product->images[0])) {
                            $primary = $product->images[0];
                            foreach ($product->images as $photo) {
                                if (isset($photo->primary)) {
                                    $primary = $photo;
                                }
                                $hover_photo[] = base_url('uploads/images/medium/' . $photo->filename);
                            }
                            $photo = base_url('uploads/images/medium/' . $primary->filename);
                        }
                    }
                ?>
                    <div class="col-md-4 col-sm-6">
                        <div class="product-list-item">
                            <?php $name = "display_name_" . $this->culture_code;
                            if ($product->$name != ''):
                                // echo $product->$name;
                            else:
                                $name = "name_" . $this->culture_code;

                            endif;
                            ?>
                                <div class="_img">
                                    <?php if($product->category != 'gift-card'): ?>
                                        <a href="<?php echo site_url($product->slug); ?>" >
                                            <div class="product-category-page-slider">
                                                <img src="<?php echo $photo ?>">
                                                <?php if($hover_photo){ ?>
                                                    <?php foreach($hover_photo as $hover): ?>
                                                        <img src="<?php echo $hover ?>">
                                                    <?php endforeach; } ?>
                                            </div>
                                            <?php if ($product->is_new == 1): ?>
                                                <img class="new-item-balloon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png"> <!-- new tag -->
                                            <?php endif; ?>
                                        </a>

                                    <?php else: ?>
                                        <a href="" onclick="confirm_addto_cart('<?php echo $product->id; ?>','<?php echo htmlentities($product->$name) ?>',event)">
                                            <div class="product-category-page-slider">
                                                <img src="<?php echo $photo ?>">
                                                <?php if($hover_photo){ ?>
                                                    <?php foreach($hover_photo as $hover): ?>
                                                        <img src="<?php echo $hover ?>">
                                                    <?php endforeach; } ?>
                                            </div>
                                            <?php if ($product->is_new == 1): ?>
                                                <img class="new-item-balloon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png"> <!-- new tag -->
                                            <?php endif; ?>
                                        </a>

                                        <script>
                                            function confirm_addto_cart(product_id,product_name,event){
                                                r = confirm('Do you like to add giftcard in cart?');
                                                if(r){
                                                    add_to_cart(product_id,product_name,event,1);
                                                }
                                            }
                                        </script>
                                    <?php endif; ?>
                                </div>
                            <!-- _img Ends-->

                            <div class="_details">
                                <div class="details-left-column">
                                    <div class="_name">
                                        <a href="<?php echo site_url($product->slug); ?>"> 
                                            <?php $name = "display_name_" . $this->culture_code; ?>
                                            <?php if ($product->$name != ''): ?>
                                                <?php echo strtoupper($product->$name); ?>
                                            <?php else: ?>
                                                <?php $name = "name_" . $this->culture_code; ?>
                                                <?php echo strtoupper($product->$name); ?>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div id="co-product-size">
                                        <form id="form-<?php echo $product->id; ?>" action="" method="post">
                                            <ul>
                                                <input type="hidden" name="id" value="<?php echo $product->id; ?>"/>
                                                <input type="hidden" name="quantity" value="1"/>
                                                <?php $items = $this->go_cart->contents(); ?>
                                                <?php if ($product->size_quantity){ ?>
                                                    <?php //$size_id = explode(',',$product->sizes); ?>
                                                    <?php $product_sizes = json_decode($product->size_quantity); ?>
                                                    <?php
                                                    $j = 0;
                                                    $item_size = NULL;
                                                    $qty_count = 0;
                                                    ?>
                                                    <?php foreach ($size_total as $size): ?>
                                                        <?php foreach ($product_sizes as $q_size): ?>

                                                            <?php
                                                            foreach ($items as $item) {
                                                                //echo "<pre>".$item['id'];
                                                                if ($item['id'] == $product->id && $item['size'] == $q_size->size) {
                                                                    $qty_count+=$item['quantity'];
                                                                    $item_size = $item['size'];
                                                                    //echo "<pre>item_size "; print_r($item_size);
                                                                    //echo "<pre>qty_count "; print_r($qty_count);
                                                                }
                                                            }
                                                            $size_status = '';
                                                            $size_available = $q_size->quantity;

                                                            if ($item_size) {
                                                                if ($q_size->size == $item_size) {
                                                                    $size_available = intval($q_size->quantity) - intval($qty_count);
                                                                    // $size_status = 'disabled="disabled"';
                                                                } else {
                                                                    $size_available = $q_size->quantity;
                                                                }
                                                            }
                                                            ?>

                                                            <?php if ($q_size->size == $size->size_id): ?>
                                                                <?php
                                                                if ($q_size->quantity <= 0) { /* $size_status = 'disabled="disabled"'; */
                                                                }
                                                                ?>
                                                                <?php if ($j == 0): ?>
                                                                    <input type="hidden" name="size" class="size-<?php echo $product->id; ?>" value="<?php //echo $q_size->size   ?>"/>
                                                                    <?php $j++; ?>
                                                                <?php endif; ?>
                                                                <!-- <button class="size-btn" onclick="sizeSet(<?php echo $q_size->quantity ?>,<?php echo $q_size->size ?>,<?php echo $product->id; ?>)" value="<?php echo $q_size->size ?>" <?php echo $size_status ?>>
                                                                                    <span class="size-text <?php echo ($q_size->quantity == 0) ? 'text-through' : ''; ?>"><?php echo $size->size_code; ?></span>
                                                                                </button> -->
                                                                <li>
                                                                    <button class="size-btn <?php echo ($q_size->quantity == 0)?'text-through':'';?>" onclick='send(<?php echo '"'.$product->slug.'",'.$q_size->size.','.$q_size->quantity.','.$product->id;?>)' value="<?php echo $q_size->size ?>" >

                                                                        <span><?php echo $size->size_code; ?></span>
                                                                        <?php  if($q_size->quantity=='0' || $q_size->quantity=='' ){ ?>
                                                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                                        <?php } ?>
                                                                    </button>
                                                                </li>
                                                            <?php endif; ?>

                                                        <?php endforeach; ?>
                                                    <?php endforeach;
                                                } ?>
                                            </ul>
                                        </form>
                                    </div>
                                </div>

                                <?php $ex_currency = place_productPrice(); ?>
                                <div class="details-right-column">
                                    <div class="_price">
                                        <?php if(round($product->price) > round($product->saleprice)): ?>
                                            <span class="price-old"><?php echo round(($product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                                        <?php endif; ?>
                                        <span><?php echo round(((round($product->price) > round($product->saleprice)) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                                    </div>
                                </div>

                            </div><!-- Details end -->
                        </div> <!-- Product-list-item end-->
                    </div> <!-- Col Ends -->
                <?php endforeach;?>

                <div id="append_infinity"></div>
                <div id="loading" hidden style="text-align: center;"> <img src="<?php echo theme_url('') ?>/assets/img/loading-blue.gif" style="height: 40px; opacity: 0.5;"></div>
            </div> <!-- row end -->
        </div>
    </section>


</div>



<script type="text/javascript">


    /* Script to add class to the size */
    $("#co-product-size li button").click(function () {
        $("#co-product-size li").removeClass("active");
        $(this).parent('li').addClass("active");
    });
    /* Script to add class to the size */


    var holderHeights = function () {

        var imgHeight = 0;
        var detailsHeight = 0;

        var productImg = $('._img');
        $(productImg).each(function (i, e) {
            if($(e).height() > imgHeight) {
                imgHeight = $(e).height();
            }
        });

        $(productImg).css('min-height', imgHeight);

    };

    $(document).ready(function () {
        holderHeights();
    });
    $(window).ready(function () {
        holderHeights();
    });

    $(function () {
        //var view = '<?php echo $this->session->userdata('view') ? $this->session->userdata('view') : $this->session->set_userdata('view', 'list'); ?>';


        $('#list').hide();
        $('#grid').show();

    });
    $('#grid-btn').on('click', function () {

        <?php $this->session->set_userdata('view', 'grid'); ?>
        $('#grid').show();

        $('#li-list').attr('class', 'condenser3');
        $('#li-grid').attr('class', 'active condenser2');




        $('#list').hide();

    });

    $('#list-btn').on('click', function () {

        <?php $this->session->set_userdata('view', 'list'); ?>

        $('#list').show();

        $('#li-list').attr('class', 'active condenser3');
        $('#li-grid').attr('class', 'condenser2');

        $('#grid').hide();
    });

    // function add_to_cart(id)
    // {
    //   $('#form-'+id).submit();
    // }

    //function to sort by size
    function SortSelected()
    {
        // console.log($('.get_checked:checked').length);
        var page = $('#per_page').val();

        var size = [];
        $(".get_checked:checked").each(function () {
            size.push(this.value);
        });
        var gender = [];
        $(".get_check_gender:checked").each(function () {
            gender.push(this.value);
        });

        var material = [];
        $(".get_checked_material:checked").each(function () {
            material.push(this.value);
        });

        $('#category-content').html('<img src="<?php echo theme_url('') ?>/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        // $.post('<?php echo site_url('category/sortSize') ?>',{checked:selected,id:'<?php echo $id; ?>',sort:'size'},function(data){

        //   $('#category-content').html(data);

        // });

        window.location.href = '<?php echo $category_detail->slug ?>?id=<?php echo $id ?>&size=' + size + '&gender=' + gender + '&material=' + material + '&limit=' + page + '';

    }

    // function sortGender()
    // {
    //   var selected = [];
    //        $(".get_check_gender:checked").each(function(){
    //           selected.push(this.value);
    //         });
    //       $('#category-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');

    //       $.post('<?php echo site_url('category/sortGender') ?>',{checked:selected,id:'<?php echo $id; ?>'},function(data){

    //           $('#category-content').html(data);

    //       });
    // }

    // function materialSort()
    // {

    //   //console.log($('.get_checked:checked').length);

    //       var selected = [];
    //        $(".get_checked_material:checked").each(function(){
    //           selected.push(this.value);
    //         });
    //       $('#category-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
    //         $.post('<?php echo site_url('category/sortMaterial') ?>',{checked:selected,id:'<?php echo $id; ?>'},function(data){

    //           $('#category-content').html(data);

    //         });

    //    // window.location.href='<?php echo site_url('category/sortMaterial') ?>?checked=selected&&id=<?php echo $id ?>';
    // }

    /*function sizeSet(qty, size, id)
    {
        if (qty > 0)
        {
            $('.size-' + id).val(size);

            // $('#cart-button-'+id).attr('disabled',false);
        }
        else
        {
            alert('We are OUT-OF-STOCK for the given Size');
            // $('#cart-button-'+id).attr('disabled','disabled');
        }


    }


    $('.size-btn').on("click", function (e) {

        //alert(size);

        // return false;

        // e.preventDefault;
    });*/

    function per_page()
    {
        var page = $('#per_page').val();
        //alert(page);
        window.location.href = '<?php echo $category_detail->slug ?>?&limit=' + page;
    }

</script>
<script type="text/javascript">
    $(document).ready(function() {
        var win = $(window);
        var nodata = false;
        var flag = true;
        var limit = 6;
        var offset = 6;
        var id = <?php echo $id;?>;

        // Each time the user scrolls
        win.scroll(function() {
            // End of the document reached?
            if(flag && !nodata){
                if ($(document).height() - win.height() -600  < win.scrollTop()) {
                    $('#loading').css('display', 'block').appendTo('#main .row');
                    flag = false;
                    $.ajax({
                        url: '<?php echo site_url('category/infinite_scroll'); ?>',
                        dataType: 'html',
                        method: "GET",
                        data: { limit: limit, offset: offset,id:id }, 
                        success: function(html) {
                            // $('.product-slide-indivisual').slick('reinit');
                            // $('.product-slide-indivisual').removeClass("slick-initialized slick-slider");
                            // // $('.product-slide-indivisual').slick.destroy();
                            // slickInit();
                            offset+=6;
                            if(!html)
                            {
                                nodata = true; //no data recieved
                                $('#nodata').show();
                            }
                            $('#main .row').append(html);
                            $('#append_infinity').append(html);
                               /* For Image Height Start */
                            var imgHeight = 0;
                            var detailsHeight = 0;
                           
                            var productImg = $('._img');
                            $(productImg).each(function (i, e) {
                                if($(e).height() > imgHeight) {
                                    imgHeight = $(e).height();
                                }
                            });
                    
                            $(productImg).css('min-height', imgHeight);
                            /* For Image Height End */
                            
                            /* For Details Height */
                            var detailsHeight = 0;
                            $('._details').each(function () {
                                if($(this).height() > detailsHeight) {
                                    detailsHeight = $(this).height();
                                }
                            })
                          
                            $('._details').outerHeight(detailsHeight).attr('data-test', detailsHeight);
                            /* For Details Height End */
                        
                            $('#loading').hide();
                            flag=true;
                            holderHeights();
                        },
                        error: function(html){
                            alert("Error! Contact admin for further information.");
                            flag = false;
                        }
                    });
                }
            }
        }); /*win.scroll END*/
    });
</script>

