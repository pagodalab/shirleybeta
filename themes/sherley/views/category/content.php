<?php if(count($products) > 0):?>
<div id="grid">
    <div class="col-sm-10">
        <?php foreach($products as $product):?> 
                <div class="col-xs-6 col-sm-4">
                            <div class="sherley-img">
                                 <div class="sherley-unite">
           

    <?php 
        if(!empty($product->images))
        {
            //$primary    = $product->images[0];
            //$i=0;
            foreach($product->images as $photo)
            {
                 
                 if(array_key_exists('primary', $photo)):?>
                <div class="hero"> 
                <a href="<?php echo site_url($product->slug);?>" >
                    <img src="<?php echo base_url('uploads/images/full/'.$photo->filename)?>"/>
                </a> 
                </div>
    <?php   
        //$i++; 
                endif;
            }
        }
    ?>  
                        
                                 <form id="form-<?php echo $product->id;?>" action="" method="post">
                                    <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                                    <input type="hidden" name="quantity" value="1"/>
                                   
                                    
                                     <div class="sherley-unique-modify">
                                        <?php if($product->sizes):?>
                                            <?php $size_id = explode(',',$product->sizes);?>
                                        <p>Available Sizes</p>
                                         <div class="sherley-button">
                                            <?php $j=0;?>
                                            <?php for($i = 0; $i<count($size_id); $i++):?>
                                                <?php foreach($sizes as $size):?>
                                                    
                                                    <?php if($size_id[$i] == $size->size_id):?>
                                                      <?php if($j == 0):?>  
                                                   <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php echo $size_id[$i]?>"/>
                                                        <?php $j++;?>
                                                      <?php endif;?>
                                                    <button class="size-btn" onclick="sizeSet(<?php echo $size_id[$i]?>,<?php echo $product->id;?>)" value="<?php echo $size_id[$i]?>">
                                                        <?php echo $size->size_code;?>
                                                    </button>
                                                    <?php endif; ?>
                                                    
                                                <?php endforeach;?>
                                            <?php endfor;?>
                                            
                                         </div>
                                        <?php endif;?>
                                     
                                     
                                </div>  
                                </div>
                                
                                <div class="sherly-united">
                                    <h5 class="uppercase">
                                    <?php $name = "name_".$this->culture_code; ?>
                                      <?php echo strtoupper($product->$name);?>
                                       <br>
                                    </h5>

                                <p class="intro" style="color: #bae3dd;"> 
                                 <?php if($this->session->userdata('currency') == 'euro'):?>

                                         <p><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate']).' '.$this->new_currency; ?></p>
                               
                                <?php else:?>
                                
                                  <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?></p>
                               <?php endif;?>
                               </p> 
                                 <div class="sherley-basket-new">
                                     <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo htmlentities($product->$name)?>')">
                                        <p><?php echo strtoupper(lang('add_to_basket'))?></p>
                                        <img src="<?php echo theme_url('assets/img/basket.png')?>">
                                    </button>
                                     


                                      </div>
                                </div>    
                                </form>
                                <?php if($product->is_new == 1):?>
                                     <div class="sherley-new1">
                                        <a href=""><img src="<?php echo theme_url('assets/img/new.png')?>"> </a>
                                    </div>    
                                <?php endif;?>
                               
                               
                               
                        </div>  
                             
                    </div>
        
            <?php endforeach;?>
    
      </div>
</div><!-- end of grid view-->

<div id="list">


  <div class="col-sm-10">
    <?php foreach($products as $product):?> 
        <div class="col-sm-4">
        
     <div class="sherley-img">
      <?php 
        if(!empty($product->images))
        {
            //$primary    = $product->images[0];
            //$i=0;
            foreach($product->images as $photo)
            {
                 
                 if(array_key_exists('primary', $photo)):?>
                
                <a href="<?php echo site_url($product->slug);?>" >
                    <img src="<?php echo base_url('uploads/images/medium/'.$photo->filename)?>"/>
                </a> 
    <?php   
        //$i++; 
                endif;
            }
        }
    ?>   
            <?php if($product->is_new == 1):?>      
               <div class="sherley-old">
                  <a href=""><img src="<?php echo theme_url('assets/img/new.png');?>"> </a>
              </div>
            <?php endif;?>                                         
        </div> 
             
        </div>
    <div class="col-sm-8">
            
          <div class="sherley-letters">
          <?php $name = 'name_'.$this->culture_code;?>
        <h3 ><?php echo $product->$name;?></h3>
        
        <?php $description = 'description_'.$this->culture_code;?>
        <p><?php echo $product->$description;?></p> 
        
	<?php if($product->sizes):?> 
          <?php $size_id = explode(',',$product->sizes);?>
            <p> Available in sizes for
              <?php for($i = 0; $i<count($size_id); $i++):?>
                <?php foreach($sizes as $size):?>

                  <?php if($size_id[$i] == $size->size_id):?>
                    <?php $size_title = 'size_title_'.$this->culture_code;?>
                   <?php echo $size->$size_title.',';?>
                  <?php endif; ?>

                <?php endforeach;?>
                
              <?php endfor;?>
            </p>
        <?php endif; ?>  

        <span style="color: #bae3dd;">
          <?php if($this->session->userdata('currency') == 'euro'):?>
                                        <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?>
                                <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?>
                                
                                 <?php elseif($this->session->userdata('currency') == 'yen'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_yen']).' '.$this->japanese_currency; ?>
                                 <?php elseif($this->session->userdata('currency') == 'HKD'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_hdollar']).' '.$this->hongkong_currency; ?>
                                          
                                 <?php elseif($this->session->userdata('currency') == 'won'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_korea']).' '.$this->korean_currency; ?>
                                                     
                                 <?php elseif($this->session->userdata('currency') == 'Pound'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_pound']).' '.$this->british_currency; ?>
                                 <?php elseif($this->session->userdata('currency') == 'AUD'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_adollar']).' '.$this->australlian_currency; ?>          
                                <?php else:?>
                                
                                  <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?>
                               <?php endif;?> </span>
              
               <div class="sherley-rightcorner">
                 <form id="form-<?php echo $product->id;?>" action="<?php echo site_url('cart/add');?>" method="post">
                    <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                    <input type="hidden" name="quantity" value="1"/>

                    <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo htmlentities($product->$name);?>')"><p><?php echo lang('form_add_to_cart');?></p><img src="<?php echo theme_url('assets/img/basket.png');?>"></button>
                  </form>
               </div>
        </div>
            
            
        </div> 
        <div class="clearfix"></div>
        
        <hr>
        
        
    <?php endforeach;?>                
        </div>
</div> <!-- end of list-->
<?php else:?>
  <p>No Product Found</p>
<?php endif;?>

<script>
 $(function(){
  //var view = '<?php echo $this->session->userdata('view')?$this->session->userdata('view'):$this->session->set_userdata('view','list');?>';
  
   
    $('#list').hide();
    $('#grid').show();
  
});
</script>