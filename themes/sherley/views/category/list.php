<div id="content-wrapper">
    
  <div class="container">
<div class="row">
    <div class="col-sm-2">
    <div class="listing">
        <a href="<?php echo site_url('');?>">Home ></a>
        
        <a href=""><?php echo lang('categories');?></a>
         <h5><?php echo strtoupper(lang('categories'));?></h5>
       
                  <hr>
         
        
       </div>
    <div class="sherley-box-new">
    
           <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    
    </div>
      
        <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
   <ul class="nav navbar-nav li-uppercase">
     <?php $categories = $this->menu_category ;?>
        <?php foreach($categories as $category):?>
           <ul class="inline">
              <div class="rotater">              
               <a href="<?php echo site_url($category['slug']);?>">
                  <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category['id']?>" aria-expanded="false" aria-controls="collapse<?php echo $category['id']?>">
                  <?php echo strtoupper($category['name_'.$this->culture_code]);?>
                  </a>
               </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
                 <div class="panel panel-default" style="margin: 0;">
                   <div id="collapse<?php echo $category['id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $category['id']?>" style="height: auto;">
                      
                    <?php $subcategories = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$category['id']),'sequence asc')->result_array();?>
                    <?php if(count($subcategories) > 0):?>  
                    <div class="panel-body">
                          <ul class="body">
                          
                                    <?php foreach($subcategories as $subcategory):?>
                             <li><img src="<?php echo theme_url('assets/img/star.png');?>">
                                <a href="<?php echo site_url($subcategory['slug']);?>">
                               
                                  <?php echo strtoupper($subcategory["name_".$this->culture_code]);?>
                                </a>
                              </li>
                              
                              <?php endforeach;?>
                        </ul>
                    </div>
                  <?php endif;?>
                  </div>
                </div>
               </div>  
               
          </ul>
         <?php endforeach;?>

         
       </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    </div>
    </div>
            
      <br>
      <br>
      <br>

      <?php /*if(isset($sub_category)){?>
        <?php if(isset($this->categories[$id] ) && count($this->categories[$id]) > 0): $cols=3; ?>
            <?php echo lang('subcategories'); ?>
          <div class=col-sm-10>
                    <?php foreach($this->categories[$id] as $subcategory):?>
                     
                        <a href="<?php echo site_url(implode('/', $base_url).'/'.$subcategory->slug); ?>"><?php echo $subcategory->name;?></a>
                       
                                   
                    <?php endforeach;?>
                 </div>
           <br/><br/><br/>
        
        <?php endif;?>
    <?php }*/?>
<div id="category-content">

<div id="grid">
    <div class="col-sm-10">
        <?php foreach($subcategory_list as $subcat):?> 
                 <div class="col-xs-6 col-sm-4">
                  <div class="sherley-img">

    <?php 
        if($subcat->image)
        {?>
         <a href="<?php echo site_url($subcat->slug);?>" >
                    <img src="<?php echo base_url('uploads/images/medium/'.$subcat->image)?>" style="height:283px; width:266px;"/>
                </a> 
    <?php   
        //$i++; 
              
        }
    ?>   
            <h5 class="uppercase">
            <?php $name='name_'.$this->culture_code;?>
                <?php echo $subcat->$name;?><br>
            </h5>
              
    </div>  
         
  </div>
        
            <?php endforeach;?>
    
      </div>
</div><!-- end of grid view-->



</div><!--End category content-->

    </div>
</div>

<script type="text/javascript">
  
  $(function(){
  //var view = '<?php echo $this->session->userdata('view')?$this->session->userdata('view'):$this->session->set_userdata('view','list');?>';
  
   
    $('#list').hide();
    $('#grid').show();
  
});
$('#grid-btn').on('click',function(){
  
  <?php $this->session->set_userdata('view','grid');?>
  $('#grid').show();
  
  $('#li-list').attr('class','condenser3');
  $('#li-grid').attr('class','active condenser2');
  
  
  

  $('#list').hide();
  
});

$('#list-btn').on('click',function(){
  
  <?php $this->session->set_userdata('view','list');?>
  
  $('#list').show();

  $('#li-list').attr('class','active condenser3');
  $('#li-grid').attr('class','condenser2');
 
  $('#grid').hide();
});

// function add_to_cart(id)
// {
//   $('#form-'+id).submit();
// }

function SortSelected()
{
  console.log($('.get_checked:checked').length);
  
      var selected = [];
       $(".get_checked:checked").each(function(){
          selected.push(this.value);
        });
      $('#category-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        $.post('<?php echo site_url('category/sortSize')?>',{checked:selected,id:'<?php echo $id;?>'},function(data){
         
          $('#category-content').html(data);

        });
   
}

function sortGender()
{
  var selected = [];
       $(".get_check_gender:checked").each(function(){
          selected.push(this.value);
        });
      $('#category-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        
      $.post('<?php echo site_url('category/sortGender')?>',{checked:selected,id:'<?php echo $id;?>'},function(data){
         
          $('#category-content').html(data);

      });
}

function materialSort()
{
  
  //console.log($('.get_checked:checked').length);
  
      var selected = [];
       $(".get_checked_material:checked").each(function(){
          selected.push(this.value);
        });
      $('#category-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        $.post('<?php echo site_url('category/sortMaterial')?>',{checked:selected,id:'<?php echo $id;?>'},function(data){
         
          $('#category-content').html(data);

        });
   
}
</script>