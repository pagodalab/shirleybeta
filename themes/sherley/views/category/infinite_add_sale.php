<?php foreach($products as $product){?>
    <?php
    //echo "<pre>"; print_r($product['images']);
    // $photo  = theme_img('no_picture.png', lang('no_image_available'));
    $photo  = theme_img('no_picture.png');
    $hover_photo  =  array();

    if($product->images)
    {

        $image_content = $product->images;
        foreach($image_content as $img){

            if($img)
            {
                if(array_key_exists('primary',$img))
                {
                    $image = $img->filename;
                    $photo  = base_url('uploads/images/medium/'.$image);

                }
                $hover_photo[]  = base_url('uploads/images/medium/'.$img->filename);
            }
        }

    }
    ?>
    <div class="col-md-4 col-sm-6">
        <div class="product-list-item">
            <div class="_img">
                <a href="<?php echo site_url($product->slug)?>">
                    <div class="product-category-page-slider-<?php echo $offset?>">
                        <img src="<?php echo $photo?>">
                        <?php if($hover_photo){ ?>
                            <?php foreach($hover_photo as $hover): ?>
                                <img src="<?php echo $hover ?>">
                            <?php endforeach; } ?>
                    </div>
                    <?php if($product->is_new == 1):?>
                        <img class="new-item-balloon" src="<?php echo theme_url('assets')?>/img/logo_icons/new-product-icon.png""> <!-- new tag -->
                    <?php endif; ?>
                </a>
            </div>

            <div class="_details">
                <div class="details-left-column">
                    <div class="_name">
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php $lang = 'display_name_'.$this->culture_code?>
                            <?php if($product->$lang !=''){?>
                                <?php echo strtoupper($product->$lang);?>
                            <?php }else {?>
                                <?php $lang = 'name_'.$this->culture_code?>
                                <?php echo strtoupper($product->$lang);?>
                            <?php } ?>
                        </a>
                    </div>
                    <div id="co-product-size">
                        <form id="form-<?php echo $product->id;?>" action="" method="post">
                            <ul>
                                <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                                <input type="hidden" name="quantity" value="1"/>
                                <?php  $items = $this->go_cart->contents();?>
                                <?php if($product->size_quantity):?>
                                    <?php //$size_id = explode(',',$product['sizes']);?>
                                    <?php $product_sizes  = json_decode($product->size_quantity);?>
                                    <?php $j=0; $item_size=NULL; $qty_count = 0;?>
                                    <?php foreach($sizes as $size):?>
                                        <?php foreach($product_sizes as $q_size):?>
                                            <?php
                                            foreach($items as $item){
                                                //echo "<pre>".$item['id'];
                                                if($item['id'] == $product->id && $item['size'] == $q_size->size){
                                                    $qty_count+=$item['quantity'];
                                                    $item_size = $item['size'];
                                                }
                                            }
                                            $size_status = '';
                                            $size_available = $q_size->quantity;
                                            if($item_size)
                                            {
                                                if($q_size->size == $item_size)
                                                {
                                                    $size_available = intval($q_size->quantity)-intval($qty_count);

                                                }
                                                else
                                                {
                                                    $size_available = $q_size->quantity;
                                                }
                                            }
                                            ?>

                                            <?php if($q_size->size == $size->size_id):?>
                                                <?php
                                                if($q_size->quantity <= 0){  /*$size_status = 'disabled="disabled"';*/ }?>
                                                <?php if($j == 0):?>
                                                    <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php //echo $q_size->size?>"/>
                                                    <?php $j++;?>
                                                <?php endif;?>
                                                <?php /*
             <button class="size-btn <?php echo ($q_size->quantity == 0)?'text-through':'';?>" onclick="sizeSet(<?php echo $q_size->quantity ?>,<?php echo $q_size->size ?>,<?php echo $product->id; ?>)" value="<?php echo $q_size->size ?>" <?php //echo $size_status ?>z>
               <?php echo $size->size_code; ?>
               <?php  if($q_size->quantity=='0' || $q_size->quantity=='' ){ ?>
               <!--<img src="<?php echo theme_url('assets/img/email2grey.png');?>" class="grey-img" width="20px">-->
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
               <?php } ?>
             </button>*/?>
                                                <li>
                                                    <button type="button" class="size-btn <?php echo ($q_size->quantity == 0)?'text-through':''?>" onclick='send(<?php echo '"'.$product->slug.'",'.$q_size->size.','.$q_size->quantity.','.$product->id;?>)'  value="<?php echo $q_size->size ?>" >
                                                        <?php echo $size->size_code; ?>
                                                        <?php  if($q_size->quantity=='0' || $q_size->quantity=='' ){ ?>
                                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                        <?php } ?>
                                                    </button>
                                                </li>
                                            <?php endif; ?>


                                        <?php endforeach;?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                            </ul>
                        </form>
                    </div>
                </div>

                <?php $ex_currency = place_productPrice(); ?>
                <div class="details-right-column">
                    <div class="_price">
                        <?php if(round($product->price) > round($product->saleprice)): ?>
                            <span class="price-cross"><?php echo round(($product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                            <span class="price-new"><?php echo round(((round($product->price) > round($product->saleprice)) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                        <?php else: ?>
                            <span><?php echo round(((round($product->price) > round($product->saleprice)) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></span>
                        <?php endif; ?>
                    </div>
                </div>


            </div>


        </div>

    </div>
<?php }?>



<script type="text/javascript">
    /*function slickCarousel() {
        slickInit();
    }

    function unSlickCarousel() {
        if($('.product-slide-indivisual').hasClass('slick-initialized')){
            $('.product-slide-indivisual').removeClass("slick-initialized slick-slider");
        }
    }

    var holderHeights = function () {

        var imgHeight = 0;
        var detailsHeight = 0;

        var productImg = $('._img');
        $(productImg).each(function (i, e) {
            if($(e).height() > imgHeight) {
                imgHeight = $(e).height();
            }
        });

        $(productImg).css('min-height', imgHeight);

    };

    // $(document).ready(function () {
    holderHeights();
    // });
    // $(window).ready(function () {
    holderHeights();
    // });*/

    // slickInit()
    $('.product-category-page-slider-<?php echo $offset?>').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            autoplay: true,
            autoplaySpeed: 800
        });

    $('.product-category-page-slider-<?php echo $offset?>').slick('slickPause');

    $('.product-category-page-slider-<?php echo $offset?>').hover(
        function () {
            $(this).slick('slickPlay');
        },
        function () {
            $(this).slick('pause');
        }
    );
</script>