<div class="confirm-page co-new-page">
    <section class="order-details-section">
        <div class="custom-container container-small-width">
            <div class="section-title">
                <h1><?php echo lang('form_checkout');?></h1>
            </div>

            <div class="section-content">
                <?php include('order_details.php');?>
            </div>
        </div>
    </section>

    <section class="confirm-product-list-section">
        <div class="custom-container container-small-width">
            <?php include('summary.php');?>

            <div class="row">
                <div class="col-sm-12">
                    <a class="btn-shirley btn-dark-small" href="<?php echo site_url('checkout/place_order');?>"><?php echo lang('submit_order');?></a>
                </div>
            </div>
        </div>
    </section>
</div>


<!--
<div id="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="header-for-light">
                <h2 class="wow fadeInRight animated" data-wow-duration="1s"><?php /*echo lang('form_checkout');*/?></h2>
            </div>
            <div class="row">
                <?php /*include('order_details.php');*/?>
                <?php /*include('summary.php');*/?>

                <div class="row">
                    <div class="col-sm-12">
                        <a class="btn btn-default" href="<?php /*echo site_url('checkout/place_order');*/?>"><?php /*echo lang('submit_order');*/?></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
           -->