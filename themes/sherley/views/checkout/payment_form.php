

<?php if (validation_errors()):?>
	<div class="alert alert-error">
		<a class="close" data-dismiss="alert">Close</a>
		<?php echo validation_errors();?>
	</div>
<?php endif;?>

<?php //include('order_details.php');?>

	<div class="row">
		<div class="col-sm-12">
			
			<div class="tabbable tabs-left">
				
				<?php
				if(empty($payment_method))
				{
					$selected	= key($payment_methods);
				}
				else
				{
					$selected	= $payment_method['module'];
				}
				?>
				<div class="tab-content">
					<?php foreach ($payment_methods as $method=>$info):?>
                        <?php echo form_open('checkout/step_3', 'id="form-'.$method.'"');?>
                            <input type="hidden" name="module" value="<?php echo $method;?>" />
                        <?php //echo $info['form'];?>

                        <?php /*<input type="checkbox" id="terms-<?php echo $method?>" checked="checked" onclick="valid('<?php echo $method?>')"> I accept terms and conditions<br/>*/?>

                                <input class="btn-shirley btn-sm btn-dark-small" type="submit" value="<?php echo "Continue with Checkout";//echo lang('paypal_form_continue');?>" id="submit-btn-<?php echo $method?>" style="width:100%; margin-top:10px; //background-color:#bdbdbd;"/>

						        <div class="checkout-note">
								    <p><?php echo lang('withoutpaypal');?> </p>
								</div>

                        </form>
                </div>
					<?php endforeach;?>
				</div>
			</div>
    </div>



          
<script type="text/javascript">
	/*function valid(method)
	{	
	
		if($('#terms-'+method).is(":checked"))
		{
			$('#submit-btn-'+method).show();
		}
		else
		{
			$('#submit-btn-'+method).hide();
		}	

	}*/
</script>

