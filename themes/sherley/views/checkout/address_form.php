<style type="text/css">
    .placeholder {
        display:none;
    }
</style>

<div class="checkout-page co-new-page">
    <section>
        <div class="custom-container container-small-width">
            <article>
                <div class="box-border block-form wow fadeInLeft" data-wow-duration="1s">
                    <div class="tab-content">
                        <div class="tab-pane active" id="address">
                            <?php if (validation_errors()):?>
                                <div class="alert alert-error">
                                    <a class="close" data-dismiss="alert">Close</a>
                                    <?php echo validation_errors();?>
                                </div>
                            <?php endif;?>

                            <script type="text/javascript">
                                $(document).ready(function(){

                                    //if we support placeholder text, remove all the labels
                                    if(!supports_placeholder())
                                    {
                                        $('.placeholder').show();
                                    }

                                    <?php
                                    // Restore previous selection, if we are on a validation page reload
                                    $zone_id = set_value('zone_id');

                                    echo "\$('#zone_id').val($zone_id);\n";
                                    ?>
                                });

                                function supports_placeholder()
                                {
                                    return 'placeholder' in document.createElement('input');
                                }
                            </script>



                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $('#country_id').change(function(){
                                        populate_zone_menu();
                                    });

                                    $('#ship_country_id').change(function(){
                                        ship_populate_zone_menu();
                                    });

                                });
                                // context is  bill
                                function populate_zone_menu(value)
                                {
                                    $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
                                        $('#zone_id').html(data);
                                    });
                                }
                                // context is ship
                                function ship_populate_zone_menu(value)
                                {
                                    $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#ship_country_id').val()}, function(data) {
                                        $('#ship_zone_id').html(data);
                                    });
                                }
                            </script>
                            <?php /* Only show this javascript if the user is logged in */ ?>
                            <?php if($this->customer_model->is_logged_in(false, false)) : ?>
                                <script type="text/javascript">
                                    <?php
                                    $add_list = array();
                                    foreach($customer_addresses as $row) {
                                        // build a new array
                                        $add_list[$row['id']] = $row['field_data'];
                                    }
                                    $add_list = json_encode($add_list);
                                    echo "eval(addresses=$add_list);";
                                    ?>

                                    function populate_address(address_id)
                                    {
                                        if(address_id == '')
                                        {
                                            return;
                                        }

                                        // - populate the fields
                                        $.each(addresses[address_id], function(key, value){

                                            $('.address[name='+key+']').val(value);

                                            // repopulate the zone menu and set the right value if we change the country
                                            if(key=='zone_id')
                                            {
                                                zone_id = value;
                                            }
                                        });

                                        // repopulate the zone list, set the right value, then copy all to billing
                                        $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
                                            $('#zone_id').html(data);
                                            $('#zone_id').val(zone_id);
                                        });
                                    }

                                </script>
                            <?php endif;?>

                            <?php
                            $countries = $this->location_model->get_countries_menu();

                            if(!empty($customer[$address_form_prefix.'_address']['country_id']))
                            {
                                $zone_menu	= $this->location_model->get_zones_menu($customer[$address_form_prefix.'_address']['country_id']);
                            }
                            else
                            {
                                $zone_menu = array(''=>'')+$this->location_model->get_zones_menu(array_shift(array_keys($countries)));
                            }
                            // if(!empty($customer['bill_address']['country_id']))
                            // {
                            // 	$zone_menu	= $this->location_model->get_zones_menu($customer[$address_form_prefix.'_address']['country_id']);
                            // }
                            // else
                            // {
                            // 	$zone_menu = array(''=>'')+$this->location_model->get_zones_menu(array_shift(array_keys($countries)));
                            // }
                            // if(!empty($customer['ship_address']['country_id']))
                            // {
                            // 	$zone_menu	= $this->location_model->get_zones_menu($customer[$address_form_prefix.'_address']['country_id']);
                            // }
                            // else
                            // {
                            // 	$zone_menu = array(''=>'')+$this->location_model->get_zones_menu(array_shift(array_keys($countries)));
                            // }
                            //form elements

                            $company	= array('id'=>'company','placeholder'=>lang('address_company'),'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company', @$customer[$address_form_prefix.'_address']['company']));
                            $address1	= array('id'=>'address1','placeholder'=>lang('address1'), 'class'=>'form-control', 'name'=>'address1', 'value'=> set_value('address1', @$customer[$address_form_prefix.'_address']['address1']));
                            $address2	= array('id'=>'address2','placeholder'=>lang('address2'), 'class'=>'form-control', 'name'=>'address2', 'value'=>  set_value('address2', @$customer[$address_form_prefix.'_address']['address2']));
                            $first		= array('id'=>'firstName','placeholder'=>lang('address_firstname'), 'class'=>'form-control', 'name'=>'firstname', 'value'=>  set_value('firstname', @$customer[$address_form_prefix.'_address']['firstname']));
                            $last		= array('id'=>'lastName','placeholder'=>lang('address_lastname'), 'class'=>'form-control', 'name'=>'lastname', 'value'=>  set_value('lastname', @$customer[$address_form_prefix.'_address']['lastname']));
                            $email		= array('id'=>'email','placeholder'=>lang('address_email'), 'class'=>'form-control', 'name'=>'email', 'value'=> set_value('email', @$customer[$address_form_prefix.'_address']['email']));
                            $phone		= array('id'=>'phone','placeholder'=>lang('address_phone'), 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone', @$customer[$address_form_prefix.'_address']['phone']));
                            $city		= array('id'=>'city','placeholder'=>lang('address_city'), 'class'=>'form-control', 'name'=>'city', 'value'=> set_value('city', @$customer[$address_form_prefix.'_address']['city']));
                            $zip		= array('id'=>'zip','placeholder'=>lang('address_zip'), 'maxlength'=>'10', 'class'=>'form-control', 'name'=>'zip', 'value'=> set_value('zip', @$customer[$address_form_prefix.'_address']['zip']));
                            $zone_id	= array('id'=>'zone_id','placeholder'=>lang('address_zone_id'), 'class'=>'form-control', 'name'=>'zone_id', 'value'=> set_value('zone', @$customer[$address_form_prefix.'_address']['zone']));

                            //form element for shipping form
                            $ship_company	= array('id'=>'ship_company','placeholder'=>lang('address_company'),'class'=>'form-control', 'name'=>'ship_company', 'value'=> set_value('company', @$customer[$address_form_prefix.'_address']['company']));
                            $ship_address1	= array('id'=>'ship_address1','placeholder'=>lang('address1'), 'class'=>'form-control', 'name'=>'ship_address1', 'value'=> set_value('address1', @$customer[$address_form_prefix.'_address']['address1']));
                            $ship_address2	= array('id'=>'ship_address2','placeholder'=>lang('address2'), 'class'=>'form-control', 'name'=>'ship_address2', 'value'=>  set_value('address2', @$customer[$address_form_prefix.'_address']['address2']));
                            $ship_first		= array('id'=>'ship_firstName','placeholder'=>lang('address_firstname'), 'class'=>'form-control', 'name'=>'ship_first', 'value'=>  set_value('firstname', @$customer[$address_form_prefix.'_address']['firstname']));
                            $ship_last		= array('id'=>'ship_lastName','placeholder'=>lang('address_lastname'), 'class'=>'form-control', 'name'=>'ship_last', 'value'=>  set_value('lastname', @$customer[$address_form_prefix.'_address']['lastname']));
                            $ship_email		= array('id'=>'ship_email','placeholder'=>lang('address_email'), 'class'=>'form-control', 'name'=>'ship_email', 'value'=> set_value('email', @$customer[$address_form_prefix.'_address']['email']));
                            $ship_phone		= array('id'=>'ship_phone','placeholder'=>lang('address_phone'), 'class'=>'form-control', 'name'=>'ship_phone', 'value'=> set_value('phone', @$customer[$address_form_prefix.'_address']['phone']));
                            $ship_city		= array('id'=>'ship_city','placeholder'=>lang('address_city'), 'class'=>'form-control', 'name'=>'ship_city', 'value'=> set_value('city', @$customer[$address_form_prefix.'_address']['city']));
                            $ship_zip		= array('id'=>'ship_zip','placeholder'=>lang('address_zip'), 'maxlength'=>'10', 'class'=>'form-control', 'name'=>'ship_zip', 'value'=> set_value('zip', @$customer[$address_form_prefix.'_address']['zip']));
                            $ship_zone_id	= array('id'=>'ship_zone_id','placeholder'=>lang('address_zone_id'),'class'=>'form-control', 'name'=>'ship_zone_id', 'value'=> set_value('zone', @$customer[$address_form_prefix.'_address']['zone']));



                            ?>
                            <div class="row">
                                <?php
                                // echo '<pre>';
                                // $cart_content = $this->session->userdata('cart_contents');
                                // print_r($cart_content);
                                // $customer_bill = $cart_content['customer']['bill_address'];
                                //print_r($customer_bill);
                                //exit;
                                ?>

                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1. <?php echo lang('address');?>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <form id="billing-form">
                                                <input type="hidden" name="product_in_cart" value="<?php echo $product_in_cart?>">
                                                <input type="hidden" name="product_in_cart" value="<?php echo $gift_card?>">
                                                <div class="">
                                                    <?php // Address form ?>
                                                    <div class="span8 offset2">

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="span4">
                                                                    <h2 class="sherley-user panel-title-custom">
                                                                        <?php //echo lang('address');?>
                                                                        <?php echo "CONTACT INFORMATION"; ?>
                                                                    </h2>
                                                                    <a href="#" onclick="edit_cust_info()" class="shirley-checkout-edit">Edit</a>
                                                                </div>
                                                            </div>
                                                        </div> <!-- end of class row-->

                                                        <?php
                                                        $cart_content = $this->session->userdata('cart_contents');
                                                        $customer_bill = @$cart_content['customer']['bill_address'];
                                                        //echo '<pre>';
                                                        //print_r($customer_bill);
                                                        //exit;
                                                        ?>
                                                        <div class="sherleyaddress-new"  id="contact-info" <?php echo ($customer_bill == '') ? '' : 'hidden'; ?>>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($first);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($last);?>
                                                                </div>
                                                            </div>

                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($email);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($phone);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_dropdown('country_id',$countries, @$customer['bill_address']['country_id'], 'id="country_id" class="form-control"');?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($address1);?>
                                                                </div>
                                                            </div>
                                                            <div class="row  form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($address2);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($city);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($zip);?>
                                                                </div>
                                                                <div class="col-sm-6" id="state">
                                                                    <?php //echo form_dropdown('zone_id',$zone_menu, @$customer['bill_address']['zone_id'],'id="zone_id" class="form-control"');?>
                                                                    <?php echo form_input($zone_id); ?>
                                                                </div>
                                                            </div>

                                                            <?php /*** for gift card ***/
                                                            if($gift_card == 1){?>
                                                                <br><br><br>
                                                                <div class="span12">
                                                                    <h2 class="sherley-user panel-title-custom">
                                                                        Gift Card
                                                                    </h2>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="row form-group">
                                                                    <div class="col-sm-6">
                                                                        <?php echo form_input(array('name'=>'to_name', 'value'=>set_value('to_name'),'placeholder'=>'Name' ,'class'=>'form-control'));?>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <?php echo form_input(array('name'=>'to_email', 'value'=>set_value('to_email'), 'placeholder'=>'Email To', 'class'=>'form-control'));?>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-sm-12">
                                                                        <?php echo form_textarea(array('name'=>'personal_message', 'value'=>set_value('personal_message'), 'class'=>'form-control', 'placeholder'=>'Message','class'=>'form-control', 'style'=>'height:75px;'));?>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                            <?php }/*** for gift card ***/?>
                                                            <?php if($address_form_prefix=='ship') : ?>
                                                                <!-- <input class="btn-default-1" type="button" value="<?php echo lang('form_previous');?>" onclick="window.location='<?php echo base_url('checkout/step_1') ?>'"/> -->
                                                            <?php endif; ?>
                                                            <div class="row form-group shirley-hidden-field">
                                                                <div class="col-sm-6">
                                                                    <label class="control-label" for="use_shipping">
                                                                        <?php //echo form_checkbox(array('name'=>'use_shipping', 'value'=>'yes', 'id'=>'use_shipping', 'checked'=>$use_shipping)) ?>
                                                                        <?php echo form_checkbox(array('name'=>'use_shipping', 'value'=>'yes', 'id'=>'use_shipping','checked'=>'true', 'hidden' => 'true')) ?>
                                                                        <?php //echo lang('ship_to_address') ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <input id="btn-billing-continue" class="btn-shirley btn-sm" type="button" value="<?php echo "Next" ?>"/>
                                                            <div id="bill-error-msg" style=" float: right; margin-top: 11px;"></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="display-data-bill">
                                                                <div id="firstname-bill">
                                                                    <?php echo @$customer_bill['firstname']?> <?php echo @$customer_bill['lastname']?>
                                                                </div>
                                                                <div id="email-bill">
                                                                    <?php echo @$customer_bill['email']?>
                                                                </div>
                                                                <div id="phone_no-bill">
                                                                    <?php echo @$customer_bill['phone']?>
                                                                </div>
                                                                <div id="country-bill">
                                                                    <?php echo @$customer_bill['country']?>
                                                                </div>
                                                                <div id="zone-bill">
                                                                    <?php echo @$customer_bill['zone']?>
                                                                </div>
                                                                <div id="city-bill">
                                                                    <?php echo @$customer_bill['city']?>
                                                                </div>
                                                                <div id="address1-bill">
                                                                    <?php echo @$customer_bill['address1']?>
                                                                </div>
                                                                <div id="zip-bill">
                                                                    <?php echo @$customer_bill['zip']?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div><!--span offset end-->
                                    </div><!--row-->
                                </div>
                                <!-- Panel Closing-->

                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" >
                                            2. <?php echo lang('shipping_address');?>
                                        </a>
                                    </div>

                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo">
                                        <div class="panel-body">
                                            <div class="">
                                                <?php // shipping form ?>
                                                <div class="span8 offset2">

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="span4" >
                                                                <h2 class="sherley-user panel-title-custom">
                                                                    <?php //echo lang('shipping_address');?>
                                                                    <?php echo "SHIPPING"; ?>
                                                                </h2>
                                                                <a href="#" onclick="edit_ship_info()" class="shirley-checkout-edit">Edit</a>
                                                            </div>
                                                        </div>

                                                    </div> <!-- end of class row-->
                                                    <?php
                                                    $cart_content = $this->session->userdata('cart_contents');
                                                    $customer_ship = @$cart_content['customer']['ship_address'];
                                                    ?>
                                                    <form id="shipping-form" name="shipping-form" >
                                                        <div class="sherleyaddress-new" id="shipping-info" <?php echo ($customer_ship == '') ? '' : 'hidden'; ?>>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_first);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_last);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_email);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_phone);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_dropdown('country_id',$countries, @$customer['ship_address']['country_id'], 'id="ship_country_id" class="form-control"');?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_address1);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_address2);?>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_city);?>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-sm-6">
                                                                    <?php echo form_input($ship_zip);?>
                                                                </div>
                                                                <div class="col-sm-6" id="ship_state">
                                                                    <?php
                                                                    //echo form_dropdown('zone_id',$zone_menu, @$customer['ship_address']['zone_id'], 'id="ship_zone_id" class="form-control" ');?>
                                                                    <?php echo form_input($ship_zone_id);?>
                                                                </div>
                                                            </div>

                                                            <input id="btn-shipping-continue" class="btn-shirley btn-sm" type="button" value="<?php echo 'Next';?>"/>
                                                            <div id="ship-error-msg" style=" float: right; margin-top: 11px;"></div>
                                                    </form>
                                                </div>
                                            </div><!--span offset end-->
                                        </div><!--row-->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="display-data-ship">
                                                    <div id="firstname-ship" style="float:left; margin-right:3px;">
                                                        <?php echo @$customer_ship['firstname']?>
                                                    </div>
                                                    <div id="lastname-ship">
                                                        <?php echo @$customer_ship['lastname']?>
                                                    </div>
                                                    <div id="email-ship">
                                                        <?php echo @$customer_ship['email']?>
                                                    </div>
                                                    <div id="phone_no-ship">
                                                        <?php echo @$customer_ship['phone']?>
                                                    </div>
                                                    <div id="country-ship">
                                                        <?php echo @$customer_ship['country']?>
                                                    </div>
                                                    <div id="city-ship">
                                                        <?php echo @$customer_ship['city']?>
                                                    </div>
                                                    <div id="state-ship">
                                                        <?php echo @$customer_ship['zone']?>
                                                    </div>
                                                    <div id="address1-ship">
                                                        <?php echo @$customer_ship['address1']?>
                                                    </div>
                                                    <div id="zip-ship">
                                                        <?php echo @$customer_ship['zip']?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- Panel Closing-->
                            </div>

                            <div class="panel">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title" style="color: #bdbdbd;">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" >
                                            3. Summary
                                        </a>
                                    </h4>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <?php include('cart_detail.php');?>
                                        </div>

                                        <div id="payment-form"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Panel Closing-->
                        </div>

                    </div><!--formgroup-->







                    <?php if($this->customer_model->is_logged_in(false, false)) : ?>

                        <!-- <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="address_manager">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?php echo lang('your_addresses');?></h4>
            </div>
                <div class="modal-body">
                <p>
                    <table class="table table-striped">
                    <?php
                        $c = 1;
                        foreach($customer_addresses as $a):?>
                        <tr>
                            <td>
                                <?php
                            $b	= $a['field_data'];
                            echo nl2br(format_address($b));
                            ?>
                            </td>
                            <td style="width:100px;"><input type="button" class="btn btn-primary choose_address pull-right" onclick="populate_address(<?php echo $a['id'];?>);" data-dismiss="modal" value="<?php echo lang('form_choose');?>" /></td>
                        </tr>
                    <?php endforeach;?>
                    </table>
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
        </div>
     </div> -->
                    <?php endif;?>
                </div>
            </article>
        </div>
    </section>
</div>







<script>

    $(document).ready(function(){

        <?php
        $cart_content = $this->session->userdata('cart_contents');
        $customer_ship = @$cart_content['customer']['ship_address'];
        if($customer_ship != ''){
        ?>
        $('#collapseTwo').slideDown('fast');
        <?php } ?>
    });

    $('#payment-form').html('<img src="<?php echo theme_img("loading-blue.gif");?>" style="height: 30px; margin-left: 309px;"/>');
    $.post("<?php echo site_url('checkout/payment_form');?>",{},function(data){
        $('#payment-form').html(data);
    });
    /*$(document).ready(function(){
     // $('#shipping-form').hide();

     var country = $('#country_id').val();

     if(country == '223')
     {
     $('#state').show();
     }
     else
     {
     $('#state').hide();
     }
     var country = $('#ship_country_id').val();

     if(country == '223')
     {
     $('#ship_state').show();
     }
     else
     {
     $('#ship_state').hide();
     }

     });*/

    // $('#use_shipping').on('click',function(){
    // 	var check = $('#use_shipping:checked').val();

    // 	if(check == 'yes')
    // 	{

    // 		$('#shipping-form').hide();

    // 	}
    // 	else{

    // 		$('#shipping-form').show();

    // 	}

    // });

    //billing form submit
    $('#btn-billing-continue').on('click',function(){

        var address1 = $('#address1').val()
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var city = $('#city').val();
        var zip = $('#ship_zip').val();
        //var state = $('#zone').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        if(address1 == '' || firstName == '' || lastName == '' || email == '' || phone == '' || city == '')
        {
            $('#bill-error-msg').html('<font color="red">(*) mark fields are required</font>');
            return false;
        }
        else if(atpos< 1 || dotpos<atpos+2 || dotpos+2 >= email.length)
        {
            $('#bill-error-msg').text('Email Address Not Valid!').css('color','red');

            return false;
        }
        else if(isNaN(phone) == true)
        {
            $('#bill-error-msg').text('Enter correct Phone Number').css('color','red');

            return false;
        }
        else
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('checkout/step_1');?>",
                data: $('#billing-form').serialize(),
                dataType : 'json',
                success: function(data) {
                    console.log(data);
                    if(data.success == 'TRUE'){
                        if(data.shipping == 'FALSE'){
                            $('#collapseTwo').slideDown('slow');
                        }
                        else{
                            $('#collapseTwo').slideUp('fast');
                            location.reload();
                        }

                        $('#contact-info').hide();
                        $('.display-data-bill').show();
                        $('#firstname-bill').html(data.details['bill_address']['firstname']);
                        $('#firstname-bill').append(" "+data.details['bill_address']['lastname']);
                        //$('#lastname-bill').html(lastName);
                        $('#email-bill').html(data.details['bill_address']['email']);
                        $('#phone_no-bill').html(data.details['bill_address']['phone']);
                        $('#address1-bill').html(data.details['ship_address']['address1']);
                        $('#country-bill').html(data.details['ship_address']['country']);
                        $('#city-bill').html(data.details['ship_address']['city']);
                        $('#zip-bill').html(data.details['ship_address']['zip']);
                        //$('#zone-bill').html(zone);
                    }
                }
            });
        }

    });

    //shipping form submit
    $('#btn-shipping-continue').on('click',function(){

        var address1 = $('#ship_address1').val()
        var firstName = $('#ship_firstName').val();
        var lastName = $('#ship_lastName').val();
        var email = $('#ship_email').val();
        var phone = $('#ship_phone').val();
        var city = $('#ship_city').val();
        var zip = $('#ship_zip').val();
        //var state = $('#ship_zone').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        if(address1 == '' || firstName == '' || lastName == '' || email == '' || phone == '' || city == '')
        {
            $('#ship-error-msg').html('<font color="red">(*) mark fields are required</font>');
            return false;
        }
        else if(atpos< 1 || dotpos<atpos+2 || dotpos+2 >= email.length)
        {
            $('#ship-error-msg').text('Email Address Not Valid!').css('color','red');

            return false;
        }
        else if(isNaN(phone) == true)
        {
            $('#ship-error-msg').text('Enter correct Phone Number').css('color','red');

            return false;
        }
        else
        {


            $.ajax({
                type: "POST",
                url: "<?php echo site_url('checkout/shipping_address');?>",
                data: $('#shipping-form').serialize(),
                dataType : 'json',
                success: function(data) {
                    $('#shipping-info').hide();
                    $('.display-data-ship').show();
                    // $('#firstname-ship').html(firstName);
                    // $('#lastname-ship').html(lastName);
                    // $('#email-ship').html(email);
                    // $('#phone_no-ship').html(phone);
                    // $('#city-ship').html(city);
                    // $('#zip-ship').html(zip);
                    // $('#state-ship').html(state);
                    location.reload();
                }
            });
        }

    });

    $('#btn-checkout-final').click(function(){
        window.location.href = "<?php echo site_url('checkout/step_4');?>";
    });

    //end of shipping form submit

    /*$('#country_id').on('change',function(){
     var country = $('#country_id').val();

     if(country == '223')
     {
     $('#state').show();
     }
     else
     {
     $('#state').hide();
     }
     });
     $('#ship_country_id').on('change',function(){
     var country = $('#ship_country_id').val();

     if(country == '223')
     {
     $('#ship_state').show();
     }
     else
     {
     $('#ship_state').hide();
     }
     });*/


</script>
<script type="text/javascript">
    // $('#use_shipping').change(function () {
    // 	if ($(this).is(':checked')) {
    // 		$('#btn-billing-continue').val('Pay with Paypal');

    // 	} else {
    // 		//alert(' is now unchecked');
    // 		$('#btn-billing-continue').val('Continue');
    // 	}
    // });

    function edit_cust_info(){
        $('#contact-info').show();
        $('.display-data-bill').hide();
        $('#bill-error-msg').hide();
        $('#btn-billing-continue').val('Update');

    }
    function edit_ship_info(){

        var address1 = $('#address1-ship').text().replace(/\s+/, "");
        var firstName = $('#firstname-ship').text().replace(/\s+/, "");
        var lastName = $('#lastname-ship').text().replace(/\s+/, "");
        var email = $('#email-ship').text().replace(/\s+/, "");
        var phone = $('#phone_no-ship').text().replace(/\s+/, "");
        var city = $('#city-ship').text().replace(/\s+/, "");
        var zip = $('#zip-ship').text().replace(/\s+/, "");
        var state = $('#state-ship').text().replace(/\s+/, "");
        //var zip = $('#zip-ship').val();

        //console.log(address1);

        $('#shipping-info').show();
        $('.display-data-ship').hide();

        $('#ship_firstName').val(firstName);
        $('#ship_lastName').val(lastName);
        $('#ship_email').val(email);
        $('#ship_phone').val(phone);
        $('#ship_city').val(city);
        $('#ship_zip').val(zip);
        $('#ship_address1').val(address1);
        $('#ship_zone_id').val(state);

        $('#btn-shipping-continue').val('Update');

    }

</script>