<?php echo form_open('cart/update', array('id' => 'update_cart_form_home')); ?>
    <?php if ($this->go_cart->total_items() == 0){?>
        <div id="cart_head">
            <div class="_title text-center">
                <h2>CART</h2>
                <a class="close" data-dismiss="alert"><i id="closeCart" class="fa fa-angle-right"></i></a>
                <p>
                    <?php echo lang('empty_view_cart'); ?>
                </p>
            </div>
        </div>
    <?php } else {?>

        <div id="cart_head">
            <div class="_title text-center">
                <h2>CART</h2>
                <a class="close" data-dismiss="alert"><i id="closeCart" class="fa fa-angle-right"></i></a>
            </div>
        </div>

        <div id="cart_body">
            <div class="cartCard_product_container">
                <?php if ($this->go_cart->group_discount() > 0) { ?>

                <?php if ($this->session->userdata('currency') == 'euro'){?>

                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                <?php }elseif ($this->session->userdata('currency') == 'dollar'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                <?php }elseif ($this->session->userdata('currency') == 'yen'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                <?php }elseif ($this->session->userdata('currency') == 'HKD'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>
                <?php }elseif ($this->session->userdata('currency') == 'won'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                 <?php }elseif ($this->session->userdata('currency') == 'Pound'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                <?php }elseif ($this->session->userdata('currency') == 'AUD'){?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php }else{?>

                    <?php echo round($this->go_cart->group_discount()) . ' ' . $this->base_currency; ?>
                <?php }?>
                <?php } ?>
                <?php
                $subtotal = 0;
                foreach ($this->go_cart->contents() as $cartkey => $product):
                    ?><?php
                        $product['images'] = array_values((array) json_decode($product['images']));
                        $no_photo = '<img src="' . theme_img('no_picture.png') . '" alt="" height="150px" width="150px"/>';
                        $product['images'] = array_values($product['images']);
                    ?>

                        <div class="cartCard_product_item">
                            <div class="row">
                        <div class="col-xs-4 cart_picture_container">
                            <a href="#">
                            <?php
                        if (!empty($product['images'])){

                            foreach ($product['images'] as $photo):

                                if ($photo) {

                                    if (array_key_exists('primary', $photo)):
                                        if (file_exists('uploads/images/thumbnails/' . $photo->filename)) {
                                            ?>
                                <img src="<?php echo base_url('uploads/images/thumbnails/' . $photo->filename); ?>">
                            </a>
                             <?php
                                        } else {
                                            echo $no_photo;
                                        }
                                        ?>
                                        <?php
                                    endif;
                                } else {
                                    echo $no_photo;
                                    break;
                                }
                            endforeach;
                            ?>
                        <?php } ?>
                        </div>


                        <div class="col-xs-8 cart_detail_container">
                            <div class="_details_side_cart">
                               <a href="javascript:void(0)" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                  window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                                                    }" >
                                                                     <i id="remove_cartItem" class="fa fa-close"></i></a>
                                <h4><?php if ($product['display_name_' . $this->culture_code] != ''){?>
                                <?php echo strtoupper($product["name_" . $this->culture_code]); ?>
                            <?php }else{?>
                                <?php echo strtoupper($product["name_" . $this->culture_code]); ?>
                            <?php }?></h4>
                                <p>Size:  <?php if ($product['size'] != ''){?>
                            <?php $size = $this->size_model->getSizes(array('size_id' => $product['size']))->row_array(); ?>
                            <?php echo $size['size_title_' . $this->culture_code]; ?>
                        <?php } ?></p>
                                <p>Price: <?php if ($this->session->userdata('currency') == 'euro'){ ?>

                                <?php echo round($product['price'] * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                            <?php }elseif ($this->session->userdata('currency') == 'dollar'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                              <?php }elseif ($this->session->userdata('currency') == 'yen'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                              <?php }elseif ($this->session->userdata('currency') == 'HKD'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                          <?php }elseif ($this->session->userdata('currency') == 'AUD'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                          <?php }elseif ($this->session->userdata('currency') == 'Pound'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                         <?php }elseif ($this->session->userdata('currency') == 'won'){?>

                                <?php echo round($product['price'] * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>

                            <?php }else{?>

                                <?php echo round($product['price']) . ' ' . $this->base_currency; ?>
                            <?php } ?></p>

                            <?php if (!(bool) $product['fixed_quantity']){?>
                                <p>Quantity: <?php $product_sizes = json_decode($product['size_quantity']); ?>

                                            <select class="quantity_home" name="cartkey[<?php echo $cartkey; ?>]">
                                                <?php foreach ($product_sizes as $size_qty): ?>
                                                    <?php if ($size_qty->size == $product['size']): ?>

                                                        <?php for ($qty = 1; $qty != 15; $qty++): ?>
                                                            <?php if ($qty <= $size_qty->quantity): ?>
                                                                <option value="<?php echo $qty; ?>"  <?php
                                                                if ($product['quantity'] == $qty) {
                                                                    echo 'selected';
                                                                }
                                                                ?>><?php echo $qty; ?></option>
                                                                    <?php endif; ?>
                                                                <?php endfor; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                            </select></p>
                                <?php }else{?>

                                     <?php echo $product['quantity'] ?>
                                <input type="hidden" name="cartkey[<?php echo $cartkey; ?>]" value="1"/>
                                <button type="button" class="pull-right" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                                            window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                                        }" style="background: transparent">
                                   <a href="javascript:void(0)" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                                                        window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                                                    }" > <img src="<?php echo theme_url('assets/img/cross.png') ?>"></a>
                                </button>
                            <?php }?>
                            </div>
                        </div>
                    </div>
                        </div>
                <?php endforeach;?>
            </div>
        </div>

        <div id="cart_foot">
            <div class="row">
                <div class="col-xs-6 _cart_coupon">
                    <input type="text" name="coupon_code" class="form-control input-shirley" placeholder="<?php echo lang('apply_coupon');?>">
                    <input class="button-coupon" style="height:0px;width:0px;padding: 0;margin: 0;opacity:0" type="submit" value="<?php echo lang('apply_coupon');?>"/>

                        <?php if($this->gift_cards_enabled){?>
                            <!--  <label style="margin-top:15px;"><?php echo lang('gift_card_label');?></label> -->
                            <!--<input type="text" name="gc_code" class="span3 side-cart-coup side-cart-coup-2" placeholder="<?php /*echo lang('apply_gift_card');*/?>">-->
                            <!--<input class="btn btn-default" type="submit" value="<?php echo lang('apply_gift_card');?>"/>-->
                        <?php }?>
                </div>
                <div class="col-xs-6 _cart_price">
                    <p> TOTAL:
                    <?php /* if ($this->session->userdata('currency') == 'euro'): ?>

                      <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                      <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                      <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                      <?php else: ?>

                      <?php echo round($this->go_cart->subtotal()) . ' ' . $this->base_currency; ?>
                      <?php endif; */ ?>

                    <?php /*
                     * grand total
                     */ ?>
                    <?php if ($this->session->userdata('currency') == 'euro'){?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                    <?php }elseif ($this->session->userdata('currency') == 'dollar'){?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                    <?php } elseif ($this->session->userdata('currency') == 'yen'){ ?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                     <?php } elseif ($this->session->userdata('currency') == 'HKD'){ ?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                    <?php } elseif ($this->session->userdata('currency') == 'won'){ ?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>

                     <?php } elseif ($this->session->userdata('currency') == 'Pound'){ ?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                    <?php } elseif ($this->session->userdata('currency') == 'AUD'){ ?>

                        <?php echo round($this->go_cart->total() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                    <?php }else{?>

                        <?php echo round($this->go_cart->total()) . ' ' . $this->base_currency; ?>
                    <?php } ?>
                </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                     <a href="<?php echo site_url('cart/view_cart') ?>"><button class="btn-shirley btn-sm btn-dark-small">CHECK OUT</button></a>
                </div>
            </div>
        </div>


        <?php } ?>

    <?php echo form_close()?>

    <script>

    $('.quantity_home').on('change',function(){

        $('#update_cart_form_home').submit();
    });

</script>