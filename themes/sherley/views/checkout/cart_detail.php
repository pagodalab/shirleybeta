<table class="table-responsive table cart-table">
    <thead>
    <tr>
        <!-- <th class="card_product_image">Image</th> -->
        <!-- <th class="card_product_image"><?php echo lang('sku'); ?></th> -->
        <th class="card_product_image"><?php echo lang('name'); ?></th>
        <th class="card_product_image"><?php echo lang('price'); ?></th>
        <th class="card_product_image"><?php echo lang('size'); ?></th>
        <th class="card_product_image"><?php echo lang('quantity'); ?></th>
        <th class="card_product_image"><?php echo lang('totals'); ?></th>

    </tr>
    </thead>


    <tfoot>

    <?php
    /*         * ************************************************************
      Subtotal Calculations
     * ************************************************************ */
    ?>
    <?php
    //echo $this->go_cart->is_free_shipping().'asdf'; exit;?>
    <?php if ($this->go_cart->group_discount() > 0) : ?>
        <tr>
            <td colspan="4"><?php echo lang('group_discount'); ?></td>
            <td>-
                <?php if ($this->session->userdata('currency') == 'euro'): ?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>
                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>
                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>
                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'won'): ?>
                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                    <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php else: ?>

                    <?php echo round($this->go_cart->group_discount()) . ' ' . $this->base_currency; ?>
                <?php endif; ?>


            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td colspan="4"><?php echo lang('subtotal'); ?></td>
        <td id="gc_subtotal_price">
            <?php if ($this->session->userdata('currency') == 'euro'): ?>

                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>
                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'yen'): ?>
                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>


            <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>
                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'won'): ?>
                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
            <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                <?php echo round($this->go_cart->subtotal() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>
            <?php else: ?>

                <?php echo round($this->go_cart->subtotal()) . ' ' . $this->base_currency; ?>
            <?php endif; ?>
        </td>
    </tr>


    <?php if ($this->go_cart->coupon_discount() > 0) { ?>
        <tr>
            <td colspan="4"><?php echo lang('coupon_discount'); ?></td>
            <td id="gc_coupon_discount">-
                <?php if ($this->session->userdata('currency') == 'euro'): ?>

                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>


                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'won'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>
                    <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php else: ?>

                    <?php echo round($this->go_cart->coupon_discount()) . ' ' . $this->base_currency; ?>
                <?php endif; ?>
            </td>
        </tr>
        <?php if ($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?>
            <tr>
                <td colspan="4"><?php echo lang('discounted_subtotal'); ?></td>
                <td id="gc_coupon_discount">
                    <?php if ($this->session->userdata('currency') == 'euro'): ?>

                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>
                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'yen'): ?>
                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>
                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>
                    <?php elseif ($this->session->userdata('currency') == 'won'): ?>
                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                    <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>
                        <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>
                    <?php else: ?>

                        <?php echo round($this->go_cart->discounted_subtotal()) . ' ' . $this->base_currency; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php
        }
    }
    /*         * ************************************************************
      Custom charges
     * ************************************************************ */
    $charges = $this->go_cart->get_custom_charges();
    if (!empty($charges)) {
        foreach ($charges as $name => $price) :
            ?>

            <tr>
                <td colspan="4"><?php echo $name ?></td>
                <td>
                    <?php if ($this->session->userdata('currency') == 'euro'): ?>

                        <?php echo round($price * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                        <?php echo round($price * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                        <?php echo round($price * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                    <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                        <?php echo round($price * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                        <?php echo round($price * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                    <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                        <?php echo round($price * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                    <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                        <?php echo round($price * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                    <?php else: ?>

                        <?php echo round($price . ' ' . $this->base_currency); ?>
                    <?php endif; ?>
                </td>
            </tr>

            <?php
        endforeach;
    }

    /*         * ************************************************************
      Order Taxes
     * ************************************************************ */
    if ($product_in_cart == 1) {
        // Show shipping cost if added before taxes
        //if($this->go_cart->contents())
        // echo '<pre>';
        // print_r($this->go_cart->contents());
        // exit;
        if($this->go_cart->is_free_shipping() != '1'){

            if ($this->config->item('tax_shipping') && $this->go_cart->shipping_cost() > 0) :
                ?>
                <tr>
                    <td colspan="4"><?php echo lang('shipping'); ?></td>
                    <td>
                        <?php if ($this->session->userdata('currency') == 'euro'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                        <?php else: ?>

                            <?php echo round($this->go_cart->shipping_cost()) . ' ' . $this->base_currency; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endif;


            if ($this->go_cart->order_tax() > 0) :
                ?>
                <tr>
                    <td colspan="4"><?php echo lang('tax'); ?></td>
                    <td>
                        <?php if ($this->session->userdata('currency') == 'euro'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>
                        <?php else: ?>

                            <?php echo round($this->go_cart->order_tax()) . ' ' . $this->base_currency; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php
            endif;
// Show shipping cost if added after taxes
            if (!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost() > 0) :
                ?>
                <tr>
                    <td colspan="4"><?php echo lang('shipping'); ?></td>
                    <td>
                        <?php if ($this->session->userdata('currency') == 'euro'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                        <?php else: ?>

                            <?php echo round($this->go_cart->shipping_cost()) . ' ' . $this->base_currency; ?>
                        <?php endif; ?>

                    </td>
                </tr>
            <?php endif ?>
        <?php }
    } ?>

    <?php
    /* * ************************************************************
      Gift Cards
     * ************************************************************ */
    if ($this->go_cart->gift_card_discount() > 0) :
        ?>
        <tr>
            <td colspan="4"><?php echo lang('gift_card_discount'); ?></td>
            <td>-
                <?php if ($this->session->userdata('currency') == 'euro'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php else: ?>

                    <?php echo round($this->go_cart->gift_card_discount()) . ' ' . $this->base_currency; ?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endif; ?>

    <?php
    /* * ************************************************************
      Grand Total
     * ************************************************************ */
    ?>
    <tr>
        <td colspan="4"><?php echo lang('grand_total'); ?></td>
        <td>
            <?php if ($this->session->userdata('currency') == 'euro'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
            <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
            <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
            <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

            <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                <?php echo round($this->go_cart->total() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

            <?php else: ?>

                <?php echo round($this->go_cart->total()) . ' ' . $this->base_currency; ?>
            <?php endif; ?>
        </td>
    </tr>
    </tfoot>

    <tbody>
    <?php
    $subtotal = 0;

    foreach ($this->go_cart->contents() as $cartkey => $product):
        ?>
        <tr>

            <?php /* ?><td>
                  <?php
                  $product['images'] = array_values((array)json_decode($product['images']));

                  $photo  = theme_img('no_picture.png', lang('no_image_available'));
                  $product['images']    = array_values($product['images']);

                  if(!empty($product['images'])):

                  foreach($product['images'] as $photo):
                  if(!isset($product['color'])):

                  if(array_key_exists('primary', $photo)): ?>
                  <img class="pull-left cart-image" src="'<?php echo base_url('uploads/images/thumbnails/'.$photo->filename);?>'">
                  <?php endif;
                  else:?>
                  <?php if(isset($photo->color)):?>
                  <?php if($product['color'] == ($photo->color)):?>
                  <img class="pull-left cart-image" src="'<?php echo base_url('uploads/images/thumbnails/'.$photo->filename);?>'">
                  <?php endif;?>
                  <?php endif;?>
                  <?php endif;

                  endforeach;?>
                  <?php endif;?>

                  </td>
                  <td><?php echo $product['sku']; ?></td><?php */ ?>
            <td> <?php if ($product['display_name_' . $this->culture_code] != ''): ?>
                    <?php echo strtoupper($product["name_" . $this->culture_code]); ?>
                <?php else: ?>
                    <?php echo strtoupper($product["name_" . $this->culture_code]); ?>
                <?php endif; ?></td>
            <td>
                <?php if ($this->session->userdata('currency') == 'euro'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                    <?php echo round($product['price'] * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php else: ?>

                    <?php echo round($product['price']) . ' ' . $this->base_currency; ?>
                <?php endif; ?>
            </td>
            <td>
                <?php if ($product['size'] != ''): ?>
                    <?php $size = $this->size_model->getSizes(array('size_id' => $product['size']))->row_array(); ?>
                    <?php echo $size['size_title_' . $this->culture_code]; ?>
                <?php endif; ?>
            </td>


            <td style="white-space:nowrap">
                <?php if ($this->uri->segment(1) == 'cart'): ?>
                    <?php if (!(bool) $product['fixed_quantity']): ?>
                        <div class="control-group">
                            <div class="controls">
                                <div class="input-append">

                                    <?php $product_sizes = json_decode($product['size_quantity']); ?>

                                    <select class="quantity" name="cartkey[<?php echo $cartkey; ?>]">
                                        <?php foreach ($product_sizes as $size_qty): ?>
                                            <?php if ($size_qty->size == $product['size']): ?>

                                                <?php for ($qty = 1; $qty != 15; $qty++): ?>
                                                    <?php if ($qty <= $size_qty->quantity): ?>
                                                        <option value="<?php echo $qty; ?>"  <?php if ($product['quantity'] == $qty) {
                                                            echo 'selected';
                                                        } ?>><?php echo $qty; ?></option>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                    <button type="button" class="btn btn-danger" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                            window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                            }"><span class="glyphicon glyphicon-trash"></span></button>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php echo $product['quantity'] ?>
                        <input type="hidden" name="cartkey[<?php echo $cartkey; ?>]" value="1"/>
                        <button type="button" class="btn btn-danger" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                }">
                            <span class="glyphicon glyphicon-trash"></span></button>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo $product['quantity'] ?>
                <?php endif; ?>
            </td>
            <td>
                <?php if ($this->session->userdata('currency') == 'euro'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                    <?php echo round($product['price'] * $product['quantity'] * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                <?php else: ?>

                    <?php echo round($product['price'] * $product['quantity']) . ' ' . $this->base_currency; ?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>

    $('.quantity').on('change', function () {

        $('#update_cart_form').submit();
    });

</script>
