

<div class="table-responsive">


    <table class="cart_table">
        <tr>
            <th>Product</th>
            <th>Product Detail</th>
            <th>Size</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th></th>
        </tr>
        <?php 
            // echo"<pre>";print_r($this->go_cart->contents()); exit;
        ?>
        <?php  foreach ($this->go_cart->contents() as $cartkey => $product): ?>
            <tr>
                <td data-title="product">
                    <span>
                        <?php   if($product['images'])
                        {

                            $image_content = array_values((array)json_decode($product['images']));
                            foreach($image_content as $img)

                                if($img)
                                {


                                    if(array_key_exists('primary',$img))

                                    {
                                        $image = $img->filename;
                                        $photo  = base_url('uploads/images/medium/'.$image);
                                        $hover_photo = $photo;
                                    }

                                }
                            }
                            if($product['images'])
                            {

                                $image_content = array_values((array)json_decode($product['images']));
                                foreach($image_content as $img)
                                    if($img)
                                    {
                                        if(array_key_exists('hover',$img))
                                        {
                                            $image = $img->filename;
                                            $hover_photo  = base_url('uploads/images/medium/'.$image);
                                        }

                                    }
                                }
                                ?>
                                <img src="<?php echo $photo?>">
                            </span>
                        </td>
                        <td data-title="Detail">
                            <span><?php echo $product['display_name_'.$this->culture_code]?></span>
                        </td>
                        <td data-title="Size">
                            <span>
                                <?php if ($product['size'] != ''): ?>
                                    <?php $size = $this->size_model->getSizes(array('size_id' => $product['size']))->row_array(); ?>
                                    <?php echo $size['size_title_' . $this->culture_code]; ?>
                                <?php endif; ?>
                            </span>
                        </td>
                       <td data-title="Price">
                            <?php $ex_currency = place_productPrice(); ?>
                            <span>
                                <?php echo round($product['price'] * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency'] ?>
                            </span>
                        </td>
                        <td data-title="Quantity" style="white-space:nowrap">
                            <span>
                                <?php if ($this->uri->segment(1) == 'cart'): ?>
                                    <?php if (!(bool) $product['fixed_quantity']): ?>
                                        <div class="control-group">
                                            <div class="controls">
                                                <div class="input-append">

                                                    <?php $product_sizes = json_decode($product['size_quantity']); ?>
                                                    <select class="quantity" name="cartkey[<?php echo $cartkey; ?>]">
                                                        dfssdfs
                                                        <?php foreach ($product_sizes as $size_qty): ?>

                                                            <?php if ($size_qty->size == $product['size']): ?>

                                                                <?php for ($qty = 1; $qty != 15; $qty++): ?>

                                                                    <?php if ($qty <= $size_qty->quantity): ?>
                                                                        <option value="<?php echo $qty; ?>"  <?php echo ($product['quantity'] == $qty)?'selected':'' ?>> <?php echo $qty; ?>
                                                                        </option>
                                                                    <?php endif; ?>
                                                                <?php endfor; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <?php echo $product['quantity'] ?>
                                        <input type="hidden" name="cartkey[<?php echo $cartkey; ?>]" value="1"/>
                                        <button type="button" class="btn btn-danger" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                            window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                                        }">
                                        <span class="glyphicon glyphicon-trash"></span></button>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php echo $product['quantity'] ?>
                                <?php endif; ?>
                            </span>
                        </td>
                         <td data-title="Total">
                            <span><?php echo round($product['price'] * $product['quantity'] * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency'] ?></span>
                        </td>
                        <td class="cross">
                            <a href="#" class="remove-cart" onclick="if (confirm('<?php echo lang('remove_item'); ?>')) {
                                window.location = '<?php echo site_url('cart/remove_item/' . $cartkey); ?>';
                            }"><i class="fa fa-close"></i> </a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
        <script>

            $('.quantity').on('change', function () {

                $('#update_cart_form').submit();
            });

        </script>
