<div class="row">
    <?php if(!empty($customer['bill_address'])):?>
        <div class="col-sm-6 shirley-confirm-customer">
            <?php /*?><a href="<?php echo site_url('checkout/step_1');?>" class="btn btn-default">


                <?php if($customer['bill_address'] != @$customer['ship_address'])
                {
                    echo "Change Sender Detail";
                }
                else
                {
                    echo "Change Address";
                }
                ?>
            </a><?php */?>
            <h3 class="subtitle"><?php echo lang('customer');?></h3>
            <div class="clear sep"></div>
                <ul>
                    <li><?php echo $customer['bill_address']['firstname'] . ' ' . $customer['bill_address']['lastname'];?></li>
                    <li><?php echo $customer['bill_address']['address1'];?></li>
                    <li><?php echo $customer['bill_address']['address2'] . ', ' . $customer['bill_address']['city'];?></li>
                    <li><?php echo $customer['bill_address']['country'];?></li>
                    <li><?php echo $customer['bill_address']['phone'];?></li>
                    <li><?php echo $customer['bill_address']['email'];?></li>
                </ul>
            <div class="padder"></div>
        </div>
    <?php endif;?>

    <?php if(config_item('require_shipping')):?>
        <?php if($this->go_cart->requires_shipping()):?>
            <div class="col-sm-6 shirley-confirm-receiver">
                <?php /*?><a href="<?php echo site_url('checkout/shipping_address');?>" class="btn btn-default"><?php echo "Change Receiver Detail";?></a><?php */?>
                <h3 class="subtitle"><?php echo lang('receiver');?></h3>
                <div class="clear sep"></div>
                <ul>
                    <?php //echo format_address($customer['ship_address'], true);?>
                    <li><?php echo $customer['ship_address']['firstname'] . ' ' . $customer['ship_address']['lastname'];?></li>
                    <li><?php echo $customer['ship_address']['address1'];?></li>
                    <li><?php echo $customer['ship_address']['address2'] . ', ' . $customer['ship_address']['city'];?></li>
                    <li><?php echo $customer['ship_address']['country'];?></li>
                    <li><?php echo $customer['ship_address']['phone'];?></li>
                    <?php //echo $customer['ship_address']['resident_phone'];?>
                    <li><?php echo $customer['ship_address']['email'];?></li>
                </ul>
                <div class="padder"></div>

            </div> <div class="clearfix"></div>

            <?php

            if(!empty($shipping_method) && !empty($shipping_method['method'])):?>
                <!-- <div class="col-sm-4">

                <hr/>
                <strong><?php echo lang('delivery_method');?></strong><br/>
                <?php if($this->session->userdata('currency') == 'euro'):?>

                <?php echo $shipping_method['method'].': '.round($shipping_method['price']*$this->exrate['rate']).' '.$this->new_currency;?>
                 <?php else:?>

                  <?php echo $shipping_method['method'].': '.round($shipping_method['price']).$this->base_currency; ?>
               <?php endif;?>
               <br/><br/>
                <p><a href="<?php echo site_url('checkout/step_2');?>" class="btn btn-default"><?php echo "Change Delivery Method";?></a><hr/></p>
            </div> -->
            <?php endif;?>
        <?php endif;?>
    <?php endif;?>

    <?php if(!empty($payment_method)):?>
        <div class="col-md-6 shirley-confirm-payment">
            <p><?php echo lang('payment_method');?></p>
            <div class="sep"></div>
            <p><?php echo $payment_method['description'];?></p>
            <div class="padder"></div>
            <!-- <a href="<?php echo site_url('checkout');?>" class="btn btn-default"><?php echo "Change Payment Method";?></a><hr/> -->
        </div>
    <?php endif;?>

</div>