<div class="register-page co-new-page">
    <section class="register-section">
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="register-form co-form">
                        <?php
                            $company	= array('placeholder'=>lang('account_company'),'id'=>'bill_company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company'));
                            $first		= array('placeholder'=>lang('account_firstname'),'id'=>'bill_firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'),'required'=>'required');
                            $last		= array('placeholder'=>lang('account_lastname'),'id'=>'bill_lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'),'required'=>'required');
                            $email		= array('placeholder'=>lang('account_email'),'id'=>'bill_email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'),'required'=>'required','onBlur'=>'checkEmail()');
                            $phone		= array('placeholder'=>lang('account_phone'),'id'=>'bill_phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'),'required'=>'required');
                        ?>
                        <div class="form-title">
                            <h2><?php echo lang('form_register');?></h2>
                        </div>
                        <?php echo form_open('auth/register'); ?>
                            <div class="form-fields">
                                <input type="hidden" name="submitted" value="submitted" />
                                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

                                <div class="form-group">
                                    <?php echo form_input($company);?>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php echo form_input($first);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo form_input($last);?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php echo form_input($email);?>
                                        </div>

                                        <div class="col-md-6">
                                            <?php echo form_input($phone);?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="checkbox" name="email_subscribe" value="1" <?php echo set_radio('email_subscribe', '1', TRUE); ?>/>
                                    <span><?php echo lang('account_newsletter_subscribe');?></span>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" id="password" value="" class="form-control" autocomplete="off" required="required" placeholder="<?php echo lang('account_password');?>"/>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="confirm" id="confirm_password" value="" class="form-control" autocomplete="off" required="required" placeholder="<?php echo lang('account_confirm');?>"/>
                                </div>
                            </div>

                            <div class="form-action">
                                <input type="submit" value="<?php echo lang('form_register');?>" id="register-submit" class="btn-shirley btn-sm" onclick="showRegisterModal()"/>
                                <a href="<?php echo site_url('auth/login'); ?>" class="btn-shirley btn-sm" id="register-cancel"><?php echo lang('go_to_login');?></a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>





 
<!-- Modal -->
<div class="addcart-modal">
    <div class="modal fade bs-example-modal-sm" id="loadingModal"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Processing...</h4>
          </div>
        </div>
      </div>
    </div>
</div>
<script>
function checkEmail()
{
    var bill_email = $('#bill_email').val();
    if(bill_email)
    {
		$('#register-submit').attr('disabled','disabled');
        $.post("<?php echo site_url('auth/check_user_email')?>",{bill_email : bill_email},function(data){
			if(data.success)
			{
			    //alert('Email Available');
				$('#register-submit').removeAttr('disabled');
			}
			else
			{
				alert('Error, Invalid Email Or Email Already Exists!');
				$('#bill_email').val('').focus();
				//$('#register-submit').attr('disabled','disabled');
			}
		},'json');

    }    
}


function showRegisterModal()
{
    var bill_firstname = $('#bill_firstname').val();
    var bill_lastname = $('#bill_lastname').val();
    var bill_email = $('#bill_email').val();
    var bill_phone = $('#bill_phone').val();
    var password = $('#password').val();
    var confirm_password = $('#confirm_password').val();
    
    if(bill_firstname && bill_lastname && bill_email && bill_phone && password && confirm_password)
    {
        $('#loadingModal').modal();
    }
}
</script>