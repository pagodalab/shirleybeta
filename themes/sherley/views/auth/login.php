<div class="login-page co-new-page">
    <section class="login-section">
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-5">
                    <div class="login-form co-form">
                        <div class="form-title">
                            <h2><?php echo lang('login');?></h2>
                        </div>
                        <?php echo form_open('auth/login', 'class="form-horizontal"'); ?>
                        <div class="form-fields">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" required="required"  placeholder="<?php echo lang('email');?>"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" autocomplete="off" required="required" placeholder="<?php echo lang('password');?>"/>
                            </div>
                            <div class="form-group">
                                <input name="remember" value="true" type="checkbox" />
                                <span><?php echo lang('keep_me_logged_in');?></span>
                            </div>
                        </div>

                        <div class="form-action">
                            <input type="submit" value="<?php echo lang('login');?>" name="submit" class="btn-shirley btn-sm"/>
                            <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
                            <input type="hidden" value="submitted" name="submitted"/>
                            <a href="<?php echo site_url('auth/forgot_password')?>" class="btn-shirley btn-sm"><?php echo lang('forgot_password');?></a>
                        </div>

                        </form>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-3">
                    <div class="sign-up-form co-form">
                        <div class="form-title">
                            <h2><?php echo lang('get_account');?></h2>
                        </div>

                        <div class="form-fields">
                            <ul>
                                <li><?php echo lang('account_features');?></li>
                                <li><?php echo lang('access_checkout');?></li>
                                <li><?php echo lang('exclusive_offer');?></li>
                            </ul>
                            </ul>

                        </div>

                        <div class="form-action">
                            <a class="btn-shirley btn-sm" href="<?php echo site_url('auth/register')?>"><?php echo lang('create_account');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

