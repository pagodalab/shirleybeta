<div class="forgot-password-page co-new-page">
    <section class="forgot-password-section">
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-4">
                    <div class="login-form co-form">
                        <div class="form-title">
                            <h2><?php echo lang('forgot_password');?></h2>
                        </div>

                        <?php echo form_open('auth/forgot_password', 'class="form-horizontal"') ?>
                            <div class="form-fields">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" placeholder="<?php echo lang('email');?>"/>
                                </div>
                            </div>

                            <div class="form-action">
                                <input type="hidden" value="submitted" name="submitted"/>
                                <input type="submit" value="<?php echo lang('reset_password');?>" name="submit" class="btn-shirley btn-sm"/>

                                <a href="<?php echo site_url('auth/login'); ?>"class="btn-shirley btn-sm"><?php echo lang('return_to_login');?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


