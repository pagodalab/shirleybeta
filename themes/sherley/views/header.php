<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <?php $settings = $this->settings; ?>
	<title>Welcome to <?php echo $settings['company_name']?> </title>

    <!----------------------- Dependencies Starts ----------------------->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?php echo $this->meta_keywords;?>">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">



     <!-- Icons start -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo theme_url('assets')?>/img/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?php echo theme_url('assets')?>/img/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
     <!-- Icons Ends -->
    <meta property="og:title" content="Shirley Bredal"/>
    <meta property="og:type"   content="website" />
    <meta property="og:url"    content="http://shirleybredal.com/en"/>
    <!--<meta property="og:image" content="http://crowdfunding.koboldwatch.com/application/themes/pagoda_c/img/background/truck.jpg"/>-->
    <meta property="og:site_name" content="Shirley Bredal"/>
    <meta property="og:description" content="<?php echo $this->meta_description;?>">
    <!------------------------ Dependencies Ends ------------------------>

    <!--------------------------- Fonts Starts -------------------------->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <!---------------------------- Fonts Ends --------------------------->


    <!---------------------------- Css Starts --------------------------->
    <!-------------- Packages Starts -------------->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/bower_components/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo theme_url('assets');?>/bower_components/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/bower_components/slick.css">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/bower_components/slick-theme.css">
    <!--------------- Packages Ends --------------->

    <!-------------- Site Css Starts -------------->
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/css/global.css<?php echo '?ver='.mt_rand(); ?>">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/css/theStyles.css<?php echo '?ver='.mt_rand(); ?>">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/css/responsive.css<?php echo '?ver='.mt_rand(); ?>">
    <link rel="stylesheet" href="<?php echo theme_url('assets')?>/css/overrides.css<?php echo '?ver='.mt_rand(); ?>">
    <!-- <link rel="stylesheet" href="<?php echo theme_url('assets')?>/css/snowfall.css"> -->
    <!--------------- Site Css Ends --------------->

    <!----------------------------- Css Ends ---------------------------->


    <!-------------------------- Scripts Starts ------------------------->

    <script src="<?php echo theme_url('assets')?>/js/jquery.js"></script>
    <script type="text/javascript">
        /* Start of Tawk.to Script */
        var $_Tawk_API={},$_Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/55fe6e0dec060d141f437533/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->

        $(document).ready(function () {
            $('#slct, #slct2').on('change' , function () {
                var value = $(this).val();

                // $.post("<?php   echo site_url('exchangerate/admin/front_currency')?>", {currency:[value]}, function(r){
                // 	if(r) {
                // 		reload
                // 	}
                window.location.href="<?php echo site_url('home/changeCurrency');?>/"+value;
                $('#show').val("value");
            });
        });
    </script>

    <!--------------------------- Scripts Ends -------------------------->

</head>

<body>
    <?php 
        $url = explode('/',site_url()); 
    ?>

    <div class="addcart-modal">
        <div class="modal fade bs-example-modal-sm" id="cartModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" style="color: #bdbdbd;">YOUR CART</h4>
                    </div>
                    <div class="modal-body">
                        <div id="cartMessage" style="color: #bdbdbd; margin-top: 21px" class="text-center"></div>
                        <div class="bear text-center">
                            <img src="<?php echo theme_url('assets/img/koala-bear.jpg') ?>" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn-shirley unfancy btn-sm" onclick="location.reload();"><?php echo lang('continue_shopping');?></button>
                        <button onclick="window.location.href='<?php echo site_url('cart/view_cart')?>';" class="btn-shirley unfancy btn-sm">View Cart</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------------------------------------ Wrapper Starts ------------------------------------>
    <div id="wrapper">
        <div id="menu_underlay"></div>
        <div id="cartCard">
            <?php include('checkout/summaryhome.php');?>
        </div>

        <!--------------------------- Header Wrapper Starts ------------------ -->

        <div id="header-wrapper">
            <div class="top-header">
                <div class="custom-container">
                    <div class="logo-links-wrapper">
                        <div class="logo-container">
                        <a href="<?php echo site_url() ?>"><img src="<?php echo theme_url('assets/img/logo_icons/logo.png') ?>"></a>
                    </div>

                    <div class="links-container">
                        <div class="nav-toggle">
                            <i class="fa fa-bars"></i>
                        </div>
                        <ul>
                            <li>
                                <div id="search_wrap">
                                    <form action="<?php echo site_url('search')?>" method="get">
                                        <input class="newsearch" class="form-control search-btn" type="search" name="q" value="<?php echo $this->input->get('q')?>">
                                    </form>
                                </div>
                            </li>
                            <li>
                                <div class="header-currency">
                                    <select id="slct">
                                        <option value="danish" <?=($this->session->userdata('currency')=="danish")?'selected':''; ?> >DKK</option>
                                        <?php  $currencies = $this->currencies;
                                        foreach ($currencies as $currency):
                                            ?>

                                            <option value="<?php echo $currency['currency_name']; ?>" <?=($this->session->userdata('currency')==$currency['currency_name'])?'selected':''; ?> >
                                                <?php echo strtoupper($currency['currency_code']);?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </li>

                            <li>
                                <div class="header-flag">
                                    <a href="javascript:void(0)" onclick="window.location.href='<?php echo site_url('home/setFlagSession/danish')?>'"><img src="<?php echo theme_url('assets/img/logo_icons/flag_dn.jpg') ?>"></a>
                                    <a href="javascript:void(0)" onclick="window.location.href='<?php echo site_url('home/setFlagSession/euro')?>'"><img src="<?php echo theme_url('assets/img/logo_icons/flag_en.jpg') ?>"></a>
                                </div>
                            </li>

                            <?php if(!$this->customer_model->is_logged_in(false, false)):?>
                            <li>
                                <div class="header-login">
                                    <a href="<?php echo site_url('auth/login');?>" >LOGIN</a>
                                </div>
                            </li>
                            <?php endif;?>

                            <?php if($this->customer_model->is_logged_in(false, false)):?>
                            <li class="sherley-welcome">
                                <div class="header-logged-in">
                                    <a href="<?php echo  site_url('account');?>"><?php echo lang('welcome');?> <?php echo $this->go_cart->customer('firstname');?></a>
                                </div>
                            </li>
                            <?php endif;?>
                            <li>
                                <a href="#" id="cartBag">
                                    <div class="header-bag">
                                        <img src="<?php echo theme_url('assets/img/logo_icons/bag.png') ?>">
                                        <span><?php echo count($this->go_cart->contents());?></span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>

            <div class="header-navigation">
                <!-- This is visible only in mobile view -->
                <div class="mobile-nav" id="mobile-nav">
                    <div class="mobile-logo-container">
                        <a href="<?php echo site_url() ?>"><img src="<?php echo theme_url('assets/img/logo_icons/logo.png') ?>"></a>
                        <i class="fa fa-times" aria-hidden="true"></i>

                        <ul class="mobile-first-row">
                            <li>
                                <div class="header-currency">
                                    <select id="slct2">
                                        <option>Currency</option>
                                        <!----><option value="danish">DKK</option>
                                        <?php  $currencies = $this->currencies;
                                        foreach ($currencies as $currency):
                                            ?>

                                            <option value="<?php echo $currency['currency_name']; ?>" <?=($this->session->userdata('currency')==$currency['currency_name'])?'selected':''; ?> >
                                                <?php echo strtoupper($currency['currency_code']);?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="header-flag">
                                    <a href="javascript:void(0)" onclick="window.location.href='<?php echo site_url('home/setFlagSession/danish')?>'"><img src="<?php echo theme_url('assets/img/logo_icons/flag_dn.jpg') ?>"></a>
                                    <a href="javascript:void(0)" onclick="window.location.href='<?php echo site_url('home/setFlagSession/euro')?>'"><img src="<?php echo theme_url('assets/img/logo_icons/flag_en.jpg') ?>"></a>
                                </div>
                            </li>
                        </ul>

                        <ul class="mobile-second-row">
                            <li>
                                <div class="header-login">
                                    <a href="<?php echo site_url('auth/login');?>" >LOGIN</a>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- This is visible only in mobile view -->
                <div class="main-menu" id="main_menu">
                    <ul class="list-inline">
                        <?php if(isset($page) && $page == 'home'){?>
                    <?php $categories = $this->home_menu_category ;?>
                            <li class="<?php if($this->menu_active == 'New'){ echo 'category_active';}?> menu-item-157">

                                <a href="<?php echo site_url('category/NewProduct');?>" ><?php echo lang('new');?></a>
                            </li>

                            <?php foreach($categories as $category):?>
                                <li class="menu-item-158 <?php if($this->menu_active == $category['slug']){ echo 'category_active';}?>"><a href="<?php echo site_url($category['slug']);?>" target="_self">
                                        <?php $catName = "name_".$this->culture_code;?>
                                        <?php echo strtoupper($category[$catName]);?>
                                    </a>

                                    <?php $sub_cats = $this->category_model->getCategories(array('parent_id'=>$category['id'],'enabled'=>1),'sequence asc')->result_array();?>

                                    <?php if(count($sub_cats)>0 || !empty($parent['image'])): ?>

                                        <ul class="sub-menu">
                                            <li  class="<?php if($this->submenu_active == $category['slug']){ echo 'subcategory_active';}?> menu-item-229"><a href="<?php echo site_url($category['slug'])?>"><?php echo lang('show_all');?></a></li>

                                            <?php foreach($sub_cats as $sub_cat):?>
                                                <li  class="<?php if($this->submenu_active == $sub_cat['slug']){ echo 'subcategory_active';}?> menu-item-229">
                                                    <span class="menu-opener"></span>
                                                    <a href="<?php echo site_url($sub_cat['slug'])?>" target="_self" class=""><?php  echo $sub_cat["name_".$this->culture_code]?></a>
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                </li>
                            <?php endforeach;?>
                            <?php if($this->featured_menu_category): ?>
                                <li class="menu-item-157"><a href="<?php echo site_url($this->featured_menu_category['slug'])?>"><?php echo $this->featured_menu_category['name_'.$this->culture_code]?></a></li>
                            <?php endif; ?>
                            
                            
                        <?php }else{?>
                            <?php $categories = $this->menu_category ;?>
                            <li class="<?php if($this->menu_active == 'New'){ echo 'category_active';}?> menu-item-157">

                                <a href="<?php echo site_url('category/NewProduct');?>" ><?php echo lang('new');?></a>
                            </li>

                            <?php foreach($categories as $category):?>
                                <li class="menu-item-157 <?php if($this->menu_active == $category['slug']){ echo 'category_active';}?>"><a href="<?php echo site_url($category['slug']);?>" target="_self">
                                        <?php $catName = "name_".$this->culture_code;?>
                                        <?php echo strtoupper($category[$catName]);?>
                                    </a>




                                    <?php $sub_cats = $this->category_model->getCategories(array('parent_id'=>$category['id'],'enabled'=>1),'sequence asc')->result_array();?>

                                    <?php if(count($sub_cats)>0 || !empty($parent['image'])): ?>

                                        <ul class="sub-menu">
                                            <li  class="<?php if($this->submenu_active == $category['slug']){ echo 'subcategory_active';}?> menu-item-229"><a href="<?php echo site_url($category['slug'])?>"><?php echo lang('show_all');?></a></li>

                                            <?php foreach($sub_cats as $sub_cat):?>
                                                <li  class="<?php if($this->submenu_active == $sub_cat['slug']){ echo 'subcategory_active';}?> menu-item-229">
                                                    <span class="menu-opener"></span>
                                                    <a href="<?php echo site_url($sub_cat['slug'])?>" target="_self" class=""><?php  echo $sub_cat["name_".$this->culture_code]?></a>
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                </li>
                            <?php endforeach;?>
                            <!-- <li><a href="<?php echo site_url('about-us')?>">Our Story</a> </li> -->
                            <?php if($this->featured_menu_category): ?>
                                <li class="menu-item-157"><a href="<?php echo site_url($this->featured_menu_category['slug'])?>"><?php echo $this->featured_menu_category['name_'.$this->culture_code]?></a></li>
                            <?php endif; ?>
                            <li  class="<?php if($this->menu_active == 'Sale'){ echo 'category_active';}?> menu-item-157"><a href="<?php echo site_url('category/SaleProduct');?>">Sale</a> </li>
                            <li><a href="<?php echo site_url('custom_order')?>">Custom Order</a> </li>

                        <?php }?>
                            <li class="mobile-visible"><a href="<?php echo site_url('shipping-cost-and-terms')?>">Shipping Cost and Terms</a> </li>

                    </ul>
                </div>
            </div>
        </div>

        <!--------------------------------- Header Wrapper Ends ---------------------------------->



    <!-------------------------------- Content Wrapper Starts -------------------------------->
    <div id="content-wrapper">

         <?php  if($this->christmas['enable'] == 1): ?>
            <div class="snow">
                <div class="s-large" style="background-image: url('<?php echo theme_url('assets')?>/img/snow/snow_large.png')"></div>
                <div class="s-medium" style="background-image: url('<?php echo theme_url('assets')?>/img/snow/snow_medium.png')"></div>
                <!-- <div class="s-small" style="background-image: url('<?php /*echo theme_url('assets')*/?>/img/snow/snow_small.png')"></div> -->
            </div>
        <?php endif; ?>