<div id="content-wrapper">
    
  <div class="container">
<div class="row">
    <div class="col-sm-2">
    <div class="listing">
        <a href="<?php echo site_url('');?>"><?php echo lang('home');?> ></a>
       
        <a href=""><?php echo lang('form_search');?></a>
         <h5 style="margin-top: 200px;"><?php echo strtoupper(lang('sort_by'));?></h5>
       
                  <hr>
         
        
       </div>
    <?php /*?>
     <?php $categories = $this->menu_category ;?>
        <?php foreach($categories as $category):?>
           <ul class="inline">
              <div class="rotater"> 
              <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category['id']?>" aria-expanded="true" aria-controls="collapse<?php echo $category['id']?>">              
                <a href="javascript:void(0)"  onclick="categorySort(<?php echo $category['id']?>)">
                      <?php echo strtoupper($category['name_'.$this->culture_code]);?>
                </a>
               </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
                 <div class="panel panel-default" style="margin: 0;">
                   <div id="collapse<?php echo $category['id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $category['id']?>" style="height: auto;">
                      
                    <?php $subcategories = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$category['id']),'sequence asc')->result_array();?>
                    <?php if(count($subcategories) > 0):?>  
                    <div class="panel-body">
                          <ul class="body">
                          
                                    <?php foreach($subcategories as $subcategory):?>
                                      
                                            <li>
                                              <img src="<?php echo theme_url('assets/img/star.png');?>">
                                              <a href="javascript:void(0)" onclick="categorySort(<?php echo $subcategory['id']?>)">
                                                <?php echo strtoupper($subcategory['name_'.$this->culture_code]);?>
                                                
                                              </a>
                                            </li>
                             
                              <?php endforeach;?>
                        </ul>
                    </div>
                  <?php endif;?>
                  </div>
                </div>
               </div>  
               
          </ul>
         <?php endforeach;?>
           <?php */?>
            <!---Filter by gender-->
 
           <ul class="inline">
  
  <div class="rotater">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseone" aria-expanded="true" aria-controls="collapseOne"> 
        <img class="this-img" src="<?php echo theme_url('assets/img/star.png');?>" >
   <?php echo ucwords(lang('gender'));?>:
    </a>
  </div>
 <div class="sherley-newerline">
        <hr>
  </div>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-default" style="margin: 0;">
      <div id="collapseone" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone" style="height: auto;">

       
        <div class="panel-body">
          <ul class="body">
             <?php $genderCheck = explode(',', $this->input->get('gender'))?>
            <li>
              <input name="gender[]" type="checkbox" value="m" class="get_check_gender" onclick="SortSelected()" <?php if(in_array("m", $genderCheck)){ echo 'Checked';}?>/>
              Boy
            </li>
            <li>
              <input name="gender[]" type="checkbox" value="f" class="get_check_gender" onclick="SortSelected()"<?php if(in_array("f", $genderCheck)){ echo 'Checked';}?>/>
              Girl
            </li>
            <li>
              <input name="gender[]" type="checkbox" value="b" class="get_check_gender" onclick="SortSelected()" <?php if(in_array("b", $genderCheck)){ echo 'Checked';}?>/>
              Both
            </li>
          </ul>
        </div>
      </div>
   
    </div>
  </div>                                                 
                         


</ul>   
 
           <!--End of filter by-->
             <!-- sorting by size -->
<?php if(count($sizes)>0):?>
  <ul class="inline">
    
    <div class="rotater">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapseOne"> 
        <img class="this-img" src="<?php echo theme_url('assets/img/star.png');?>" >
       <?php echo ucwords(lang('sizes'));?>:
      </a>
    </div>
 <div class="sherley-newerline">
        <hr>
  </div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

      <div class="panel panel-default" style="margin: 0;">
        <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" style="height: auto;">

          
          <div class="panel-body">
            <ul class="body">
               <?php $sizeCheck = explode(',', $this->input->get('size'))?>
            <?php foreach($sizes as $sizeSort):?>

            <li>
             <input name="size_id[]" type="checkbox" value="<?php echo $sizeSort->size_id; ?>" class="get_checked" onclick="SortSelected()" <?php if(in_array($sizeSort->size_id,$sizeCheck)){ echo 'checked';}?>/>
             
             <?php $sizeTitle = "size_title_".$this->culture_code;?>
             <?php echo $sizeSort->$sizeTitle;?></a>
            </li>
          <?php endforeach;?>
            </ul>
          </div>
        </div>
     
      </div>
    </div>                                                 
                           


  </ul>  
<?php endif;?>
<!-- end of sort size--> 
<!--filter by material-->
 <?php if(count($materials)>0):?>
<ul class="inline">
  
  <div class="rotater">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsethree"> 
        <img class="this-img" src="<?php echo theme_url('assets/img/star.png');?>" >
     <?php echo ucwords(lang('materials'));?>:
    </a>
  </div>
 <div class="sherley-newerline">
        <hr>
  </div>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-default" style="margin: 0;">
      <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree" style="height: auto;">

       
        <div class="panel-body">
          <ul class="body">
            <?php $materialCheck = explode(',', $this->input->get('material'))?>
             <?php foreach($materials as $material):?>

              <li>
              <input name="materials[]" type="checkbox" value="<?php echo $material->id; ?>" class="get_checked_material" onclick="SortSelected()" <?php if(in_array($material->id,$materialCheck)){ echo 'checked';}?>/>
                
                <?php $mat_name ='name_'.$this->culture_code;?>
                  <?php echo $material->$mat_name;?>
              
              </a></li>
            <?php endforeach;?>
           
          </ul>
        </div>
      </div>
   
    </div>
  </div>                                                 
                         


</ul>   
<?php endif;?>
<!--end of filter by material-->
    </div>
   
     <div class="col-sm-10">

      <h4 style="font-weight:300">
      <?php if($this->menu_active == 'New'){ echo strtoupper(lang('latest_product'));}else{ echo lang('search_result').':'.$this->input->get('q');}?>
     </h4>    
    <div id="search-content">
     <div class="main-pagination">   
     <div class="per-page">
      <?php echo $this->pagination->create_links();?>
     </div> 
    <div class="page-limit">  
      <select id="per_page" onchange="SortSelected()">      
          <option value="">Select Perpage</option>  
          <option value="10" <?php if($this->input->get('limit') == 10){ echo 'selected';} ?>>10 per page</option>
          <option value="20" <?php if($this->input->get('limit') == 20){ echo 'selected';} ?>>20 per page</option>       
          <option value="" <?php if($this->input->get('limit') == ''){ echo 'selected';} ?>>Show all</option>
      </select>
    </div>  
  </div>
  <div class="clearfix"></div>
    <ul class="nav nav-pills pull-right">
     <li role="presentation" class="active condenser2" id="li-grid"><a href="javascript:void(0)" id="grid-btn"></a></li>
      <li role="presentation" class="condenser3" id="li-list"><a href="javascript:void(0)" id="list-btn">  </a>   </li>
     
    </ul>  

      <br>
      <br>
      <br>


      <?php /*if(isset($sub_category)){?>
        <?php if(isset($this->categories[$id] ) && count($this->categories[$id]) > 0): $cols=3; ?>
            <?php echo lang('subcategories'); ?>
          <div class=col-sm-10>
                    <?php foreach($this->categories[$id] as $subcategory):?>
                     
                        <a href="<?php echo site_url(implode('/', $base_url).'/'.$subcategory->slug); ?>"><?php echo $subcategory->name;?></a>
                       
                                   
                    <?php endforeach;?>
                 </div>
           <br/><br/><br/>
        
        <?php endif;?>
    <?php }*/?>
 
<?php if(count($products) > 0):?>
<div id="grid">
   
        <?php foreach($products as $product):?> 
       <div class="col-xs-6 col-sm-4">
                            <div class="sherley-img">
                                 <div class="sherley-unite">
           

    <?php $photo  = theme_img('no_picture.png', lang('no_image_available'));
          $product->images    = array_values($product->images);

        if(!empty($product->images[0]))
                    {
                        $primary    = $product->images[0];
                        foreach($product->images as $photo)
                        {
                            if(isset($photo->primary))
                            {
                                $primary    = $photo;
                            }
                        }
                        $photo  = '<img src="'.base_url('uploads/images/full/'.$primary->filename).'" />';
                          }
       
    ?>  
                <div class="hero"> 
                <a href="<?php echo site_url($product->slug);?>" >
                    <?php echo $photo;?>
                </a> 
                </div>
                          <form id="form-<?php echo $product->id;?>" action="" method="post">
                                    <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                                    <input type="hidden" name="quantity" value="1"/>
                                   
                                    <?php /* if($product->colors):?>
                                       <?php $color_id = explode(',',$product->colors);?>
                                       <?php $j=0;?>
                                        <?php for($i = 0; $i<count($color_id); $i++):?>
                            <?php foreach($colors as $color):?>
                                
                                <?php if($color_id[$i] == $color['color_id']):?>
                                  
                                     <?php if($j==0){?>
                                        <input type="hidden" name="color" class="color" value="<?php echo $color['color_id'];?>"/>
                                        
                                         <?php }?>
                                    
                                  
                                   <?php $j++;?>
                                <?php endif; ?>
                                                                        
                            <?php endforeach;?>
                        <?php endfor;?> 
                                  
                                 <?php endif; */ ?>
                                    
                                      <div class="sherley-unique-modify">
                                        <?php if($product->size_quantity):?>
                                            <?php //$size_id = explode(',',$product->sizes);?>
                                            <?php $product_sizes  = json_decode($product->size_quantity);?>

                                        <p>Available Sizes</p>
                                         <div class="sherley-button">
                                            <?php  $items = $this->go_cart->contents();?>
                                            <?php $j=0; $item_size=NULL; $qty_count = 0;?>

                                            <?php foreach($sizes as $size):?>
                                                <?php   foreach($product_sizes as $q_size):?>

                                                   <?php 
                                                      foreach($items as $item)
                                                      {
                                                          //echo "<pre>".$item['id'];
                                                          if($item['id'] == $product->id && $item['size'] == $q_size->size)
                                                          {
                                                              $qty_count+=$item['quantity'];
                                                              $item_size = $item['size'];
                                                               //echo "<pre>item_size "; print_r($item_size);
                                                               //echo "<pre>qty_count "; print_r($qty_count);
                                                          }
                                                         
                                                      }
                                                      $size_status = '';
                                                      $size_available = $q_size->quantity;
                                                      
                                                      if($item_size)
                                                      {
                                                          if($q_size->size == $item_size)
                                                          {
                                                              $size_available = intval($q_size->quantity)-intval($qty_count);
                                                              // $size_status = 'disabled="disabled"';
                                                          }
                                                          else
                                                          {
                                                              $size_available = $q_size->quantity;
                                                          }
                                                      }
                                                    ?>
                                                    
                                                    <?php if($q_size->size == $size->size_id):?>
                                                       <?php if($q_size->quantity <= 0){  /*$size_status = 'disabled="disabled"';*/ }?>
                                                      <?php if($j == 0):?>  
                                                   <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php //echo $q_size->size?>"/>
                                                        <?php $j++;?>
                                                      <?php endif;?>
                                                    <button class="size-btn" onclick="sizeSet(<?php echo $q_size->quantity?>,<?php echo $q_size->size?>,<?php echo $product->id;?>)" value="<?php echo $q_size->size?>" <?php echo $size_status?>>
                                                        <?php echo $size->size_code;?>
                                                    </button>
                                                    <?php endif; ?>
                                                    
                                                <?php endforeach;?>
                                            <?php endforeach;?>
                                            
                                         </div>
                                        <?php endif;?>
                                     
                                     
                                </div>  
                                </div>
                                
                                
                                <div class="sherly-united">
                                    <h5 class="uppercase">
                                    <?php $name="display_name_".$this->culture_code;
                                        if($product->$name!=''):
                                          echo $product->$name;
                                        else:
                                          $name = "name_".$this->culture_code;
                                          echo $product->$name;
                                        endif;
                                      ?>
                                       <br>
                                    </h5>

                                <?php if($product->sale_percent > 0){?>
                                  	<!-- added actual sale price -->
                                    <p class="text-through uppercase" style="color: #bae3dd;"> 
									  <?php if($this->session->userdata('currency') == 'euro'):?>
                                            <?php echo round(($product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?>
                                    <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                            <?php echo round(($product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?>
                                    <?php else:?>
                                    
                                      <?php echo round(($product->price)).' '.$this->base_currency; ?>
                                      <?php endif;?>
                                      </p>
                                  <?php }?>
								  
                                  <p class="uppercase" style="color: #bae3dd;"> 
								  <?php if($this->session->userdata('currency') == 'euro'):?>
                                        <p><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?></p>
                                <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                          <p><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?></p>
                                <?php else:?>
                                
                                  <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?></p>
                               <?php endif;?> </p>  
                                 <div class="sherley-basket-new">
                                     <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo $product->$name?>',event)">
                                        <p><?php echo strtoupper(lang('add_to_basket'))?></p>
                                        <img src="<?php echo theme_url('assets/img/basket.png')?>">
                                    </button>
                                     


                                      </div>
                                </div>    
                                </form>
                                <?php if($product->is_new == 1):?>
                                     <div class="sherley-new1">
                                        <a href=""><img src="<?php echo theme_url('assets/img/new.png')?>"> </a>
                                    </div>    
                                <?php endif;?>
                               
                               
                               
                        </div>  
                             
                    </div> 
            <?php endforeach;?>
    
      
</div><!-- end of grid view-->

<div id="list">


  
    <?php foreach($products as $product):?> 
        <div class="col-xs-6 col-sm-4">
        
     <div class="sherley-img">
     <?php $photo  = theme_img('no_picture.png', lang('no_image_available'));
          $product->images    = array_values($product->images);

        if(!empty($product->images[0]))
                    {
                        $primary    = $product->images[0];
                        foreach($product->images as $photo)
                        {
                            if(isset($photo->primary))
                            {
                                $primary    = $photo;
                            }
                        }
                    
                    $photo  = '<img src="'.base_url('uploads/images/full/'.$primary->filename).'" />';
                     // $photo  = "<img src="'.base_url('uploads/images/full/'.$photo->filename).'">";
               
      
        //$i++; 
               
            }
       
    ?>  
                <div class="sherley-img-list">
                <a href="<?php echo site_url($product->slug);?>" >
                    <?php echo $photo;?>
                </a> 
                </div>
    
            <?php if($product->is_new == 1):?>      
               <div class="sherley-new1">
                  <a href=""><img src="<?php echo theme_url('assets/img/new.png');?>"> </a>
              </div>
            <?php endif;?>                                         
        </div> 
             
        </div>
    <div class="col-sm-8">
            
          <div class="sherley-letters">
       
        <h3>
          <?php $nameProduct='name_'.$this->culture_code;?>
          <?php echo $product->$nameProduct;?>
           
        </h3>
        
        <p>
         
          <?php $description = 'description_'.$this->culture_code;?>
          <?php echo $product->$description;?>
          
        </p> 
       
        <?php if($product->sizes):?> 
          <?php $size_id = explode(',',$product->sizes);?>
            <p> Available in sizes for
              <?php for($i = 0; $i<count($size_id); $i++):?>
                <?php foreach($sizes as $size):?>

                  <?php if($size_id[$i] == $size->size_id):?>
                   <?php $sizeTitle = 'size_title_'.$this->culture_code;?>
                    
                          <?php echo $size->$sizeTitle.',';?>
                   
                  
                  <?php endif; ?>

                <?php endforeach;?>
                
              <?php endfor;?>
            </p>
        <?php endif; ?>  
         <span>
         <?php if($product->size_quantity):?>
                                            <?php //$size_id = explode(',',$product->sizes);?>
                                            <?php $product_sizes  = json_decode($product->size_quantity);?>

                                        
                                           <p> 
                                            <?php  $items = $this->go_cart->contents();?>
                                            <?php $j=0; $item_size=NULL; $qty_count = 0;?>

                                            <?php foreach($sizes as $size):?>
                                                <?php   foreach($product_sizes as $q_size):?>

                                                   <?php 
                                                      foreach($items as $item)
                                                      {
                                                          //echo "<pre>".$item['id'];
                                                          if($item['id'] == $product->id && $item['size'] == $q_size->size)
                                                          {
                                                              $qty_count+=$item['quantity'];
                                                              $item_size = $item['size'];
                                                               //echo "<pre>item_size "; print_r($item_size);
                                                               //echo "<pre>qty_count "; print_r($qty_count);
                                                          }
                                                         
                                                      }
                                                      $size_status = '';
                                                      $size_available = $q_size->quantity;
                                                      
                                                      if($item_size)
                                                      {
                                                          if($q_size->size == $item_size)
                                                          {
                                                              $size_available = intval($q_size->quantity)-intval($qty_count);
                                                              // $size_status = 'disabled="disabled"';
                                                          }
                                                          else
                                                          {
                                                              $size_available = $q_size->quantity;
                                                          }
                                                      }
                                                    ?>
                                                    
                                                    <?php if($q_size->size == $size->size_id):?>
                                                               <?php if($q_size->quantity <= 0){  /*$size_status = 'disabled="disabled"';*/ }?>
                                                      <?php if($j == 0):?>  
                                                   <!-- <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php //echo $q_size->size?>"/> -->
                                                        <?php $j++;?>
                                                      <?php endif;?>
                                                    
                                                     <button class="sherley-cust-input" onclick="sizeSet(<?php echo $q_size->quantity?>,<?php echo $q_size->size?>,<?php echo $product->id;?>)" value="<?php echo $q_size->size?>" <?php echo $size_status?>>
                                                        <?php echo $size->size_code;?>
                                                    </button> 
                                                   
                                                    <?php endif; ?>
                                                    
                                                <?php endforeach;?>
                                            <?php endforeach;?>
                                            
                                        </p>
                                        <?php endif;?>
                                        
                                        
                                        <?php if($product->sale_percent > 0){?>
                                  	<!-- added actual sale price -->
                                    <span class="text-through" style="color: #bae3dd;"> 
									  <?php if($this->session->userdata('currency') == 'euro'):?>
                                            <?php echo round(($product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?>
                                    <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                              <?php echo round(($product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?>
                                    <?php else:?>
                                    
                                      <?php echo round(($product->price)).' '.$this->base_currency; ?>
                                      <?php endif;?>
                                      </span>
                                  <?php }?> 
                                  &nbsp;&nbsp;                                       
                                        
                                        <span style="color: #bae3dd;">
       <?php if($this->session->userdata('currency') == 'euro'):?>
                                        <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?>
                                <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                          <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?>
                                <?php else:?>
                                
                                  <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?>
                               <?php endif;?>
        </span>
              
               <div class="sherley-rightcorner">
                 <form id="form-<?php echo $product->id;?>" action="<?php echo site_url('cart/add');?>" method="post">
                    <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                    <input type="hidden" name="quantity" value="1"/>
                     <input type="hidden" name="size" class="size-<?php echo $product->id;?>"/>
                    
                     <?php /* if($product->colors):?>
                                       <?php $color_id = explode(',',$product->colors);?>
                                       <?php $j=0;?>
                                        <?php for($i = 0; $i<count($color_id); $i++):?>
                            <?php foreach($colors as $color):?>
                                
                                <?php if($color_id[$i] == $color['color_id']):?>
                                  
                                     <?php if($j==0){?>
                                        <input type="hidden" name="color" class="color" value="<?php echo $color['color_id'];?>"/>
                                        
                                         <?php }?>
                                    
                                  
                                   <?php $j++;?>
                                <?php endif; ?>
                                                                        
                            <?php endforeach;?>
                        <?php endfor;  ?> 
                                  
                                 <?php endif;*/ ?>
                                 
                                   
                    
                    <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo $product->$name?>',event)"><p><?php echo lang('form_add_to_cart');?> </p><img src="<?php echo theme_url('assets/img/basket.png');?>"></button>
                  </form>
               </div>
        </div>
            
            
        </div> 
        <div class="clearfix"></div>
        
        <hr>
        
        
    <?php endforeach;?>                
        </div>
</div> <!-- end of list-->
<?php else:?>
  <p>No product found</p>
<?php endif;?>
<div class="clearfix"></div>
 <?php echo $this->pagination->create_links();?>

</div>

<!-- end of search-content-->

    </div>
</div>

<script type="text/javascript">
  
  $(function(){
  //var view = '<?php echo $this->session->userdata('view')?$this->session->userdata('view'):$this->session->set_userdata('view','list');?>';
  
   
    $('#list').hide();
    $('#grid').show();
  
});
$('#grid-btn').on('click',function(){
  
  <?php $this->session->set_userdata('view','grid');?>
  $('#grid').show();
  
  $('#li-list').attr('class','condenser3');
  $('#li-grid').attr('class','active condenser2');
  
  
  

  $('#list').hide();
  
});

$('#list-btn').on('click',function(){
  
  <?php $this->session->set_userdata('view','list');?>
  
  $('#list').show();

  $('#li-list').attr('class','active condenser3');
  $('#li-grid').attr('class','condenser2');
 
  $('#grid').hide();
});

function add_to_cart(id)
{
  $('#form-'+id).submit();
}

function SortSelected()
{
  var q = '<?php echo is_null($this->input->get('q'))?'':$this->input->get('q');?>';
  var page = $('#per_page').val();
  //console.log($('.get_checked:checked').length);
  
      var size = [];
       $(".get_checked:checked").each(function(){
          size.push(this.value);
        });

        var gender = [];
       $(".get_check_gender:checked").each(function(){
          gender.push(this.value);
        });

         var material = [];
       $(".get_checked_material:checked").each(function(){
          material.push(this.value);
        });

      $('#search-content').html('<img src="<?php echo theme_url('');?>/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        // $.post('<?php echo site_url('search/sortSize')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){
         
        //   $('#search-content').html(data);

        // });
      
      window.location.href='<?php echo site_url("search")?>?q='+q+'&size='+size+'&gender='+gender+'&material='+material+'&limit='+page+'';
   
}

function categorySort(catId)
{
  var q = '<?php echo $this->input->get('q');?>';
  var page = $('#per_page').val();
   var size = [];
       $(".get_checked:checked").each(function(){
          size.push(this.value);
        });

        var gender = [];
       $(".get_check_gender:checked").each(function(){
          gender.push(this.value);
        });

         var material = [];
       $(".get_checked_material:checked").each(function(){
          material.push(this.value);
        });
  
      $('#search-content').html('<img src="<?php echo theme_url('');?>/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
      //   $.post('<?php echo site_url('search/categorySort')?>',{id:catId,q:'<?php echo $this->input->get("q");?>'},function(data){
         
      //     $('#search-content').html(data);

      //   });
       window.location.href='<?php echo site_url('search')?>?q=<?php echo $this->input->get('q');?>&size='+size+'&gender='+gender+'&material='+material+'&cat_id='+catId+'&limit='+page+'';
   
}

// function sortGender()
// {
//   var selected = [];
//        $(".get_check_gender:checked").each(function(){
//           selected.push(this.value);
//         });

//        var q = '<?php echo $this->input->get('q');?>';
//       $('#search-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        
//       $.post('<?php echo site_url('search/sortGender')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){
         
//           $('#search-content').html(data);

//       });
// }

// function materialSort()
// {
//   var q = '<?php echo $this->input->get('q');?>';
//   //console.log($('.get_checked:checked').length);
  
//       var selected = [];
//        $(".get_checked_material:checked").each(function(){
//           selected.push(this.value);
//         });
//       $('#search-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
//         $.post('<?php echo site_url('search/sortMaterial')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){
         
//           $('#search-content').html(data);

//         });
   

// }

function sizeSet(qty,size,id)
{   
    if(qty > 0)
    {
      $('.size-'+id).val(size);
    
      // $('#cart-button-'+id).attr('disabled',false);   
    }
    else
    {
      alert('We are OUT-OF-STOCK for the given Size');
      // $('#cart-button-'+id).attr('disabled','disabled'); 
    }  
    
   
}
$('.size-btn').on("click",function(e){
   
    //alert(size);
    
    return false;
   
    e.preventDefault;
});

</script>