<div id="content-wrapper">

  <div class="">
    <div class="row">
      <div class="col-sm-2">
        <div class="listing">
          <a href="<?php echo site_url('');?>"><?php echo lang('home');?> ></a>

          <a href=""><?php echo lang('form_search');?></a>
          <h5 style="margin-top: 200px;"><?php echo strtoupper(lang('sort_by'));?></h5>

          <hr>


        </div>




      </div>

      <div class="col-sm-12">

        <h4 style="font-weight:300">
         <?php if($this->menu_active == 'New'){echo strtoupper(lang('latest_product'));}elseif($this->menu_active == 'Sale'){echo strtoupper("NEW SALES");}else{echo lang('search_result').':'.$this->input->get('q'); } ?>
         </h4>    
         <div id="search-content">
           <div class="main-pagination">   
             <div class="per-page">
              <?php echo $this->pagination->create_links();?>
            </div> 

          </div>
          <div class="clearfix"></div>


          <br>
          <br>
          <br>



          <?php if(count($products) > 0):?>
            <div id="grid" class="featured_grid">

              <?php foreach($products as $product):?>
                <?php 
            //echo "<pre>"; print_r($product['images']);
            // $photo  = theme_img('no_picture.png', lang('no_image_available'));
                $photo  = theme_img('no_picture.png');
                $hover_photo  = theme_img('no_picture.png');

                if($product->images)
                {

                  $image_content = $product->images;
                  foreach($image_content as $img)
                    if($img)
                    {
                      if(array_key_exists('primary',$img))
                      {
                        $image = $img->filename;
                        $photo  = base_url('uploads/images/medium/'.$image);
                        $hover_photo  = $photo;
                      }

                    }
                    if($img)
                    {
                      if(array_key_exists('hover',$img))
                      {
                        $image = $img->filename;
                        $hover_photo  = base_url('uploads/images/medium/'.$image);
                      }

                    }
                  }
                  ?>
                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12"> <!-- loop column -->
                   <div class="featured_single">
                    <a href="<?php echo site_url($product->slug)?>">
                      <div class="featured_image">
                        <div class="main_image" style="background-image: url(<?php echo $photo?>);">
                          <div class="hover_image" style="background-image: url(<?php echo $hover_photo?>);"></div>
                        </div>
                        <?php if($product->is_new == 1):?>
                          <img src="<?php echo theme_url('assets/img/new.png')?>"> <!-- new tag -->
                        <?php endif;?>
                      </div>
                    </a>
                    <form id="form-<?php echo $product->id;?>" action="" method="post">
                      <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                      <input type="hidden" name="quantity" value="1"/>

                      <div class="featured_text">
                        <!-- product name-->
                        <p><h5 class="head-cap">
                          <?php $lang = 'display_name_'.$this->culture_code?>
                          <?php if($product->$lang !=''):?>
                            <?php echo strtoupper($product->$lang);?>
                          <?php else:?> 
                            <?php $lang = 'name_'.$this->culture_code?>
                            <?php echo strtoupper($product->$lang);?>
                          <?php endif;?> 
                          <br>
                        </h5></p>
                        <!--product size-->
                        <!--<h4>size</h4>-->
                        <h4>
                          <div class="sherley-unique">

                           <?php  $items = $this->go_cart->contents();?>

                           <?php if($product->size_quantity):?>
                            <?php //$size_id = explode(',',$product['sizes']);?>

                            <?php $product_sizes  = json_decode($product->size_quantity);?>

                            <div class="sherley-button">
                              <?php $j=0; $item_size=NULL; $qty_count = 0;?>
                              <?php foreach($sizes as $size):?>



                                <?php   foreach($product_sizes as $q_size):?>


                                 <?php 
                                 foreach($items as $item)
                                 {
                                                  //echo "<pre>".$item['id'];
                                  if($item['id'] == $product->id && $item['size'] == $q_size->size)
                                  {
                                    $qty_count+=$item['quantity'];
                                    $item_size = $item['size'];
                                                       //echo "<pre>item_size "; print_r($item_size);
                                                       //echo "<pre>qty_count "; print_r($qty_count);
                                  }

                                }
                                $size_status = '';

                                $size_available = $q_size->quantity;

                                if($item_size)
                                {
                                  if($q_size->size == $item_size)
                                  {
                                    $size_available = intval($q_size->quantity)-intval($qty_count);
                                                      // $size_status = 'disabled="disabled"';
                                  }
                                  else
                                  {
                                    $size_available = $q_size->quantity;
                                  }
                                }
                                ?>



                                <?php if($q_size->size == $size->size_id):?>
                                 <?php if($q_size->quantity <= 0){  /*$size_status = 'disabled="disabled"';*/ }?>
                                 <?php if($j == 0):?>  
                                   <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php //echo $q_size->size?>"/>
                                   <?php $j++;?>
                                 <?php endif;?>

                                 <button class="size-btn <?php echo ($q_size->quantity == 0)?'text-through':'';?>" onclick="sizeSet(<?php echo $q_size->quantity ?>,<?php echo $q_size->size ?>,<?php echo $product->id; ?>)" value="<?php echo $q_size->size ?>" <?php //echo $size_status ?>z>
                                   <?php echo $size->size_code; ?>
                                   <?php  if($q_size->quantity=='0' || $q_size->quantity=='' ){ ?>
                                   <img src="<?php echo theme_url('assets/img/email2grey.png');?>" class="grey-img">
                                   <img src="<?php echo theme_url('assets/img/email2.png');?>" class="white-img">
                                   <?php } ?>
                                 </button>

                               <?php endif; ?>


                             <?php endforeach;?>
                           <?php endforeach;?>
                         </div>
                       <?php endif;?>


                     </div> 
                   </h4>
                   <!--					<p><span>379 DKK</span></p>-->


                   <?php if($product->is_sale): ?>
                    <p class="intro" style="color: #757575; padding: 5px 0;"> 
                      <?php if($this->session->userdata('currency') == 'euro'):?>
                        <p><span style="text-decoration:line-through;color:#cfcfcf;"><?php echo round(($product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?></span>
                          <span style="color:red"><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?></span>
                        </p>
                      <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                        <p><span style="text-decoration:line-through;color:#cfcfcf;"><?php echo round(($product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?></span>
                          <span style="color:red"><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?></span>
                        </p>
                      <?php else:?>
                        <span style="text-decoration:line-through;color:#cfcfcf;"><?php echo round(($product->price)).' '.$this->base_currency; ?></span>
                        <span style="color:red"><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?></span>
                      </p>
                    <?php endif;?> </p> 
                  <?php else: ?>
                    <p class="intro" style="color: #757575; padding: 5px 0;"> 
                      <?php if($this->session->userdata('currency') == 'euro'):?>
                        <p><span><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?></span></p>
                      <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                        <p><span><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?></span></p>
                      <?php else:?>
                        <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?></p>
                      <?php endif;?> </p> 
                    <?php endif; ?>


                    <div class="add_to_basket">
                      <?php $lang = 'name_'.$this->culture_code?>
                      <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo htmlentities($product->$lang)?>',event)">
                        <p><?php echo strtoupper(lang('add_to_basket'))?></p>
                        <img src="<?php echo theme_url('assets/img/basket_grey.png')?>">
                      </button>
                    </div>

                  </div>
                </form>
              </div>
            </div> <!-- loop column -->
          <?php endforeach;?>
          <!-- Infinity Scroll -->
          <div id="append_infinity"></div>
          <div id="loading" hidden style="text-align: center;"><img src="<?php echo theme_url('') ?>/assets/img/loading-blue.gif" style="height: 40px; opacity: 0.5;"></div>
          <!-- Infinity Scroll --> 

        <?php /*foreach($products as $product):?> 
       <div class="col-xs-12 col-md-3">
                            <div class="featured_single">
                                 <div class="sherley-unite">
           

    <?php $photo  = theme_img('no_picture.png', lang('no_image_available'));
          $product->images    = array_values($product->images);

        if(!empty($product->images[0]))
                    {
                        $primary    = $product->images[0];
                        foreach($product->images as $photo)
                        {
                            if(isset($photo->primary))
                            {
                                $primary    = $photo;
                            }
                        }
                        $photo  = '<img src="'.base_url('uploads/images/full/'.$primary->filename).'" />';
                          }
       
    ?>  

    <a href="<?php echo site_url($product['slug'])?>">
                                    <div class="featured_image">
                                            <div class="main_image" style="background-image: url(<?php echo $photo?>);">
                                                    <div class="hover_image" style="background-image: url(<?php echo $photo?>);"></div>
                                            </div>
                                            <?php if($product['is_new'] == 1):?>
                                                <img src="<?php echo theme_url('assets/img/new.png')?>"> <!-- new tag -->
                                            <?php endif;?>
                                    </div>
                                </a>
                <div class="hero"> 
                <a href="<?php echo site_url($product->slug);?>" >
                    <?php echo $photo;?>
                </a> 
                </div>
                          <form id="form-<?php echo $product->id;?>" action="" method="post">
                                    <input type="hidden" name="id" value="<?php echo $product->id;?>"/>
                                    <input type="hidden" name="quantity" value="1"/>
                                   
                                    <?php /* if($product->colors):?>
                                       <?php $color_id = explode(',',$product->colors);?>
                                       <?php $j=0;?>
                                        <?php for($i = 0; $i<count($color_id); $i++):?>
                            <?php foreach($colors as $color):?>
                                
                                <?php if($color_id[$i] == $color['color_id']):?>
                                  
                                     <?php if($j==0){?>
                                        <input type="hidden" name="color" class="color" value="<?php echo $color['color_id'];?>"/>
                                        
                                         <?php }?>
                                    
                                  
                                   <?php $j++;?>
                                <?php endif; ?>
                                                                        
                            <?php endforeach;?>
                        <?php endfor;?> 
                                  
                                 <?php endif; */ /*?>
                                    
                                      <div class="sherley-unique-modify">
                                        <?php if($product->size_quantity):?>
                                            <?php //$size_id = explode(',',$product->sizes);?>
                                            <?php $product_sizes  = json_decode($product->size_quantity);?>

                                        <p>Available Sizes</p>
                                         <div class="sherley-button">
                                            <?php  $items = $this->go_cart->contents();?>
                                            <?php $j=0; $item_size=NULL; $qty_count = 0;?>

                                            <?php foreach($sizes as $size):?>
                                                <?php   foreach($product_sizes as $q_size):?>

                                                   <?php 
                                                      foreach($items as $item)
                                                      {
                                                          //echo "<pre>".$item['id'];
                                                          if($item['id'] == $product->id && $item['size'] == $q_size->size)
                                                          {
                                                              $qty_count+=$item['quantity'];
                                                              $item_size = $item['size'];
                                                               //echo "<pre>item_size "; print_r($item_size);
                                                               //echo "<pre>qty_count "; print_r($qty_count);
                                                          }
                                                         
                                                      }
                                                      $size_status = '';
                                                      $size_available = $q_size->quantity;
                                                      
                                                      if($item_size)
                                                      {
                                                          if($q_size->size == $item_size)
                                                          {
                                                              $size_available = intval($q_size->quantity)-intval($qty_count);
                                                              // $size_status = 'disabled="disabled"';
                                                          }
                                                          else
                                                          {
                                                              $size_available = $q_size->quantity;
                                                          }
                                                      }
                                                    ?>
                                                    
                                                    <?php if($q_size->size == $size->size_id):?>
                                                       <?php if($q_size->quantity <= 0){  /*$size_status = 'disabled="disabled"';*/ /*}?>
                                                      <?php if($j == 0):?>  
                                                   <input type="hidden" name="size" class="size-<?php echo $product->id;?>" value="<?php //echo $q_size->size?>"/>
                                                        <?php $j++;?>
                                                      <?php endif;?>
                                                    <button class="size-btn" onclick="sizeSet(<?php echo $q_size->quantity?>,<?php echo $q_size->size?>,<?php echo $product->id;?>)" value="<?php echo $q_size->size?>" <?php echo $size_status?>>
                                                        <span class="size-text <?php echo ($q_size->quantity == 0)?'text-through':'';?>"><?php echo $size->size_code;?></span>
                                                    </button>
                                                    <?php endif; ?>
                                                    
                                                <?php endforeach;?>
                                            <?php endforeach;?>
                                            
                                         </div>
                                        <?php endif;?>
                                     
                                     
                                </div>  
                                </div>
                                
                                
                                <div class="sherly-united">
                                    <h5 class="uppercase">
                                    <?php $name="display_name_".$this->culture_code;
                                        if($product->$name!=''):
                                          echo $product->$name;
                                        else:
                                          $name = "name_".$this->culture_code;
                                          echo $product->$name;
                                        endif;
                                      ?>
                                       <br>
                                    </h5>

                                <?php if($product->sale_percent > 0){?>
                                  	<!-- added actual sale price -->
                                    <p class="text-through uppercase" style="color: #bae3dd;"> 
									  <?php if($this->session->userdata('currency') == 'euro'):?>
                                            <?php echo round(($product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?>
                                    <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                            <?php echo round(($product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?>
                                    <?php else:?>
                                    
                                      <?php echo round(($product->price)).' '.$this->base_currency; ?>
                                      <?php endif;?>
                                      </p>
                                  <?php }?>
								  
                                  <p class="uppercase"  style="color: #bae3dd;"> 
								  <?php if($this->session->userdata('currency') == 'euro'):?>
                                        <p><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_euro']).' '.$this->euro_currency; ?></p>
                                <?php elseif($this->session->userdata('currency') == 'dollar'):?>
                                          <p><?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)*$this->exrate['rate_dollar']).' '.$this->usd_currency; ?></p>
                                <?php else:?>
                                
                                  <?php echo round((($product->saleprice > 0)?$product->saleprice:$product->price)).' '.$this->base_currency; ?></p>
                               <?php endif;?> </p>  
                                 <div class="sherley-basket-new">
                                     <button onclick="add_to_cart('<?php echo $product->id;?>','<?php echo htmlentities($product->$name)?>',event)">
                                        <p><?php echo strtoupper(lang('add_to_basket'))?></p>
                                        <img src="<?php echo theme_url('assets/img/basket.png')?>">
                                    </button>
                                     


                                      </div>
                                </div>    
                                </form>
                                <?php if($product->is_new == 1):?>
                                     <div class="sherley-new1">
                                        <a href=""><img src="<?php echo theme_url('assets/img/new.png')?>"> </a>
                                    </div>    
                                <?php endif;?>
                               
                               
                               
                        </div>  
                             
                    </div> 
                  <?php endforeach;*/?>


                </div><!-- end of grid view-->

              </div> <!-- end of list-->
            <?php else:?>
              <p>No product found</p>
            <?php endif;?>
            <p id='nodata' hidden  style="text-align: center;">No More Product Found</p>
            <div class="clearfix"></div>
            <?php echo $this->pagination->create_links();?>

          </div>

          <!-- end of search-content-->

        </div>
      </div>

      <script type="text/javascript">

        $(function(){
  //var view = '<?php echo $this->session->userdata('view')?$this->session->userdata('view'):$this->session->set_userdata('view','list');?>';
  

  $('#list').hide();
  $('#grid').show();
  
});
        $('#grid-btn').on('click',function(){

          <?php $this->session->set_userdata('view','grid');?>
          $('#grid').show();

          $('#li-list').attr('class','condenser3');
          $('#li-grid').attr('class','active condenser2');




          $('#list').hide();

        });

        $('#list-btn').on('click',function(){

          <?php $this->session->set_userdata('view','list');?>

          $('#list').show();

          $('#li-list').attr('class','active condenser3');
          $('#li-grid').attr('class','condenser2');

          $('#grid').hide();
        });

        function add_to_cart(id)
        {
          $('#form-'+id).submit();
        }

        function SortSelected()
        {
          var q = '<?php echo is_null($this->input->get('q'))?'':$this->input->get('q');?>';
          var page = $('#per_page').val();
  //console.log($('.get_checked:checked').length);
  
  var size = [];
  $(".get_checked:checked").each(function(){
    size.push(this.value);
  });

  var gender = [];
  $(".get_check_gender:checked").each(function(){
    gender.push(this.value);
  });

  var material = [];
  $(".get_checked_material:checked").each(function(){
    material.push(this.value);
  });

  $('#search-content').html('<img src="<?php echo theme_url('');?>/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
        // $.post('<?php echo site_url('search/sortSize')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){

        //   $('#search-content').html(data);

        // });

        window.location.href='<?php echo site_url("search")?>?q='+q+'&size='+size+'&gender='+gender+'&material='+material+'&limit='+page+'';

      }

      function categorySort(catId)
      {
        var q = '<?php echo $this->input->get('q');?>';
        var page = $('#per_page').val();
        var size = [];
        $(".get_checked:checked").each(function(){
          size.push(this.value);
        });

        var gender = [];
        $(".get_check_gender:checked").each(function(){
          gender.push(this.value);
        });

        var material = [];
        $(".get_checked_material:checked").each(function(){
          material.push(this.value);
        });

        $('#search-content').html('<img src="<?php echo theme_url('');?>/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
      //   $.post('<?php echo site_url('search/categorySort')?>',{id:catId,q:'<?php echo $this->input->get("q");?>'},function(data){

      //     $('#search-content').html(data);

      //   });
      window.location.href='<?php echo site_url('search')?>?q=<?php echo $this->input->get('q');?>&size='+size+'&gender='+gender+'&material='+material+'&cat_id='+catId+'&limit='+page+'';

    }

// function sortGender()
// {
//   var selected = [];
//        $(".get_check_gender:checked").each(function(){
//           selected.push(this.value);
//         });

//        var q = '<?php echo $this->input->get('q');?>';
//       $('#search-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');

//       $.post('<?php echo site_url('search/sortGender')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){

//           $('#search-content').html(data);

//       });
// }

// function materialSort()
// {
//   var q = '<?php echo $this->input->get('q');?>';
//   //console.log($('.get_checked:checked').length);

//       var selected = [];
//        $(".get_checked_material:checked").each(function(){
//           selected.push(this.value);
//         });
//       $('#search-content').html('<img src="http://localhost/sherley/themes/sherley/assets/img/loading-blue.gif" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
//         $.post('<?php echo site_url('search/sortMaterial')?>',{checked:selected,q:'<?php echo $this->input->get("q");?>'},function(data){

//           $('#search-content').html(data);

//         });


// }

function sizeSet(qty,size,id)
{   
  if(qty > 0)
  {
    $('.size-'+id).val(size);
    
      // $('#cart-button-'+id).attr('disabled',false);   
    }
    else
    {
      alert('We are OUT-OF-STOCK for the given Size');
      // $('#cart-button-'+id).attr('disabled','disabled'); 
    }  
    

  }
  $('.size-btn').on("click",function(e){

    //alert(size);
    
    return false;

    e.preventDefault;
  });

</script>
<script type="text/javascript">
  $(document).ready(function() {
    var win = $(window);
    var nodata = false;
    var flag = true;
    var limit = 4;
    var offset = 4;
    // var id = <?php //echo $id;?>;

    // Each time the user scrolls
    win.scroll(function() {
      console.log(flag);
      console.log(nodata);
        // End of the document reached?
        if(flag && !nodata){
          if ($(document).height() - win.height() -600  < win.scrollTop()) {

            $('#loading').show();
            flag = false;
            $.ajax({
              url: '<?php echo site_url('category/infinite_scroll_sale'); ?>',
              dataType: 'html',
              method: "GET",
              data: { limit: limit, offset: offset },
              success: function(html) {
                offset+=4;
                if(!html)
                {
                nodata = true; //no data recieved
                            $('#nodata').show();
                          }
                          $('#append_infinity').append(html);
                          $('#loading').hide();
                          flag=true;
                        },
                        error: function(html){
                          alert("Error! Contact admin for further information.");
                          flag = false;
                        }
                      });
          }
        }
      }); /*win.scroll END*/
  });
</script>