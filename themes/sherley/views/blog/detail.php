<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53ce28ea2236f6e5"></script>

<div id="content-wrapper">
    
  <div class="container">
<div class="row">
    <div class="col-sm-8">
     <div class="listing">
        <a href="<?php echo site_url();?>"><?php echo lang('home');?> > </a>
        <a href="<?php echo site_url('blog');?>"><?php echo lang('blogs');?></a>
        
         
        
      
       
          </div>
       <div id="sortContent">    
      <?php $date = strtotime($blog->post_date);?>
         <p class="blog">
          <img src="<?php echo theme_url('assets/img/star.png');?>">  
         <?php echo date("l",$date).','.date("F",$date).' '.date("d",$date).','.date("Y",$date);?></p>
         <?php $title = 'title_'.$this->culture_code;?>
        <h4 class="blogger"><a href="<?php echo site_url($blog->slug);?>" style="color:#a7a7a7;"><?php echo mb_strtoupper($blog->$title);?></a></h4>
         
        <div class="sherley-baby">
           <?php $content = 'content_'.$this->culture_code;?>
            <div align="left"><?php if($blog->image_name):?>
                                <img src="<?php echo base_url('uploads/blog/'.$blog->image_name)?>">
                              <?php endif;?><?php echo $blog->$content;?></div>
            <br>
             
             <p align="left">Posted by Shirley bredal</p>
            
            <div class="sherley-social" align="left">
                 <div class="addthis_sharing_toolbox"></div>
                   <!-- <button><img src="<?php echo theme_url('assets/img/gmail.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/b.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/facebook.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/twitter.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/pinterest.png');?>"></button> -->
                  
                  <br/>
                          <?php if($comments):?>
                             <h4>Comments</h4>
                <?php foreach($comments as $key2):?>
                
                  
                    <hr/>
                    Name : <?php echo $key2['author'];?> on [ <?php echo $key2['date_added'];?> ] 
                    
                    <br/>
                    <?php echo $key2['text'];?><br/>
                    <hr/>
                 
                
                <?php endforeach;?>
                
              <?php endif;?>
                          <!--For showing review-->
                <!--For showing review-->
               <div id="message"></div>
                <h4>Leave Comments</h4>
                
                <form name="review_form" method="POST" id="review_form" action="<?php //echo site_url($villa['slug_name']);?>"/>
                
                
                  <!--<b>Your Name:</b><br />-->
                  <input type="text" name="author" id="author" value="" size="30" placeholder="Your Name"/>
                  
                  <br/>
                  <br/>
                  
                  <!--<b>Your Email:</b><br/>-->
                  <input type="email" name="email" id="email" value="" size="30" placeholder="Your Email"/>
                  
                  <input type="hidden" name="blog_id" value="<?php echo $blog->id;?>"/>
                
                  <br />
                  <br />
                
                  <!--<b>Your Review:</b><br/>-->
                  <textarea name="text" cols="40" rows="8" id="text" style="width: 98%;" placeholder="Leave a Comment" class="home-textarea-size" ></textarea>
                  
                  <span style="font-size: 11px;"><span style="color: #FF0000;">Note:</span> HTML is not translated!</span><br />
                  
                  <label id="review_error"><font color="red">All field are compulsory.</font></label><br/>
                  
                  
                   <span class="text-error"><?php echo form_error('message')?></span>
                  <!-- <div style="font-style:normal;font-size:14px;"> <img src="<?=site_url('home/captchaImage') ?>" alt="captcha image" width="70" height="30" />
                    <input type="text" name="recaptcha_response_field" id="recaptcha_response_field" class="required" style="position:relative;width:130px;padding: 2px;padding-bottom: 4px; margin-bottom:10px"> -->
                   
                  <div class="clearfix"></div>
                  <!-- <b>Rating:</b> <span>Bad</span>&nbsp;
                  <input type="radio" name="rating" value="1" />
                &nbsp;
                  <input type="radio" name="rating" value="2" />
                &nbsp;
                   <input type="radio" name="rating" value="3" />
                &nbsp;
                  <input type="radio" name="rating" value="4" />
                &nbsp;
                  <input type="radio" name="rating" value="5" />
                &nbsp;<span>Good</span>
      <br/> -->
                <input type="submit" id="btn_submit" name="submit" value="Comment" class="btn btn-default">
                
                </form><br/>
            </div>
             </div>
            </div><!---end of sortcontent-->
            
       
        
        
         
            
       
    
    </div>
    <div class="col-sm-2">
        
    </div>    
    
       <div class="col-sm-2">
            <div class="about-right">
            <a href="#" onClick="$('#subscribeModal').modal();" class="btn btn-primary" style="background: #edd2c4; border: none; margin-top: 4px;"><?php echo ucfirst(lang('subscribe_blog'));?></a>
            <h5><?php echo strtoupper(lang('sort_by'));?></h5>
            <hr>
            </div>

             <?php for($year=2014; $year<=date('Y'); $year++):?> 
              <ul class="inline">
                             
              <div class="rotater">              
               <a> <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $year?>" aria-expanded="true" aria-controls="collapse<?php echo $year?>"> 
               <?php echo $year?> 
                  </a>
               </div>
    
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
                 <div class="panel panel-default" style="margin: 0;">
       <div id="collapse<?php echo $year?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $year?>" style="height: auto;">
          
      <div class="panel-body">
            <ul class="body">
             <?php $months=array(
                  '1'=>'January',
                  '2'=>'February',
                  '3'=>'March',
                  '4'=>'April',
                  '5'=>'May',
                  '6'=>'June',
                  '7'=>'July',
                  '8'=>'August',
                  '9'=>'September',
                  '10'=>'October',
                  '11'=>'November',
                  '12'=>'December'
                    
                );
              ?>
              <?php if($year == date('Y')):?>
                <?php for($i=01; $i <= date('m'); $i++):?>
                 <li><a href="javascript:void(0)" onclick="sortDate(<?php echo $year?>,<?php echo $i;?>)"><img src="<?php echo theme_url('assets/img/star.png');?>"> <?php echo $months[$i];?></a></li>
                <?php endfor;?>
              <?php else:?>
                   <?php for($i=01; $i <= 12; $i++):?>
                 <li><a href="javascript:void(0)" onclick="sortDate(<?php echo $year?>,<?php echo $i;?>)"><img src="<?php echo theme_url('assets/img/star.png');?>"> <?php echo $months[$i];?></a></li>
                <?php endfor;?>
              <?php endif;?> 
          </ul>    
            
            
           </div>
      </div>
                  </div>
            </div>  
               
          </ul>
         <?php endfor;?>
        
        </div>    
    
    
 </div>
    </div>
</div>

 <script>
 $(document).ready(function() {
    $('#review_error').hide();
  });

 $("#btn_submit").click(function() {
  
  $('.error').hide();
  //var dataString = 'author='+ author + '&product_id=' + product_id + '&text=' + text;
  //alert (dataString);return false;
  var name = $("input#author").val();
    var email = $("input#email").val();
    var text = $("#text").val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".")
    
     if (name == "" || email == "" || text == "")
    {
      $("#review_error").show();
      
      return false;
    }
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length)
    {
      
      $('#review_error').text('Email Address Not Valid!').css('color','red');
       $("#review_error").show();
      return false;;
    }
  
  $.ajax({
      type: "POST",
      url: "<?php echo site_url('review/save');?>",
      data: $('#review_form').serialize(),
      dataType : 'json',
    success: function(data) {
        if(data.success)
        {
            $('#message').html("<div class='alert alert-success'><strong>Thankyou,</strong> Your Comment has been successfully submitted.</div>")
          .fadeIn(1500).fadeOut(10000);
          $("#review_form").trigger('reset');
          location.reload();
          }
    }
});
return false;

  

});



  function sortDate(year,month)
  {
      if(month <= 9)
      {
        var date = year+'-0'+month;
      }
      else
      {
        var date = year+'-'+month;
      }  
      
       $('#sortContent').html('<img src="<?php echo theme_url('assets/img/loading-blue.gif');?>" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;"> loading...');
      $.post('<?php echo site_url("blog/sortDate");?>',{date:date},function(data){
          $('#sortContent').html(data);
      });
  }

  function sortDateYear(year)
  {
     
        var date = year;
      
      
       $('#sortContent').html('<img src="<?php echo theme_url('assets/img/loading-blue.gif');?>" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;"> loading...');
      $.post('<?php echo site_url("blog/sortDate");?>',{date:date},function(data){
          $('#sortContent').html(data);
      });
  }


 </script>