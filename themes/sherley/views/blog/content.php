 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53ce28ea2236f6e5"></script>
 <?php if(count($blogs) > 0):?>
	    <?php foreach($blogs as $blog):?>

	    	<?php $date = strtotime($blog['post_date']);?>

	         <p class="blog">
	          <img src="<?php echo theme_url('assets/img/star.png');?>">   
	         <?php echo date("l",$date).','.date("F",$date).' '.date("d",$date).','.date("Y",$date);?></p>
	        <h4 class="blogger"><a href="<?php echo site_url($blog['slug']);?>" style="color:#a7a7a7;"><?php echo strtoupper($blog['title_'.$this->culture_code]);?></a></h4>
	         
	        <div class="sherley-baby">
	            <div align="left">
                    <?php if($blog['image_name']):?>
                                <img src="<?php echo base_url('uploads/blog/'.$blog['image_name'])?>">
                              <?php endif;?>
	            	<?php echo substr($blog['content_'.$this->culture_code],0,50);?>
	            </div>
	            <br>
	             
	             <p align="left">Posted by Shirley bredal</p>
	            
	            <div class="sherley-social" align="left">
	                  <div class="addthis_sharing_toolbox"></div>
                   <!-- <button><img src="<?php echo theme_url('assets/img/gmail.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/b.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/facebook.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/twitter.png');?>"></button>
                  <button><img src="<?php echo theme_url('assets/img/pinterest.png');?>"></button> -->
	            </div>
	             </div>
	            </div>
	    <?php endforeach;?>        
     <?php else: ?>
     	<p>No Blogs </p>
     <?php endif;?> 