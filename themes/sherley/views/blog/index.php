<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53ce28ea2236f6e5"></script>
<div id="content-wrapper">
    
<div class="container">
	<div class="row">
        <div class="col-sm-8">
         <div class="listing">
             <a href="<?php echo site_url();?>"><?php echo lang('home');?> > </a>
            <a href="<?php echo site_url('blog');?>"><?php echo lang('blogs');?></a>
          </div>
    
            <div id="sortContent">      
            <?php if(count($blogs) > 0):?>
                <?php foreach($blogs as $blog):?>
            
                    <?php $date = strtotime($blog['post_date']);?>
            
                     <p class="blog">
                         <img src="<?php echo theme_url('assets/img/star.png');?>">
                         <?php echo date("l",$date).','.date("F",$date).' '.date("d",$date).','.date("Y",$date);?>
                     </p>
                     
                    <h4 class="blogger"><a href="<?php echo site_url($blog['slug']);?>" style="color:#a7a7a7;"><?php echo mb_strtoupper($blog['title_'.$this->culture_code]);?></a></h4>
                     
                    <div class="sherley-baby">
                        <div align="left">
                            <?php if($blog['image_name']):?>
                                <img src="<?php echo base_url('uploads/blog/'.$blog['image_name'])?>">
                            <?php endif;?>
                            <?php echo strip_tags(substr($blog['content_'.$this->culture_code],0,50));?>............
                        </div>
                    </div>
                        <br>
                         
                        <p align="left">Posted by Shirley bredal</p>
                        
                        <div class="sherley-social" align="left">
                             <div class="addthis_sharing_toolbox"></div>
                             <?php /*?> <button><img src="<?php echo theme_url('assets/img/gmail.png');?>"></button>
                            <button><img src="<?php echo theme_url('assets/img/b.png');?>"></button>
                            <button><img src="<?php echo theme_url('assets/img/facebook.png');?>"></button>
                            <button><img src="<?php echo theme_url('assets/img/twitter.png');?>"></button>
                            <button><img src="<?php echo theme_url('assets/img/pinterest.png');?>"></button> <?php */?>
                        </div>
        
                <?php endforeach;?>        
             <?php else: ?>
                <p>No Blogs </p>
             <?php endif;?> 
            </div><!-- end of sortContent-->
        
        </div>
  
    
           <div class="col-sm-2">
                <div class="about-right">
                <a href="#" onClick="$('#subscribeModal').modal();" class="btn btn-primary" style="background: #edd2c4; border: none; margin-top: 4px;"><?php echo ucfirst(lang('subscribe_blog'));?></a>
                <h5><?php echo strtoupper(lang('sort_by'));?></h5>
                <hr>
                </div>    
                <?php for($year=2014; $year<=date('Y'); $year++):?> 
                  <ul class="inline">
                                 
                   <div class="rotater">
                   <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $year?>" aria-expanded="true" aria-controls="collapse<?php echo $year?>">               
                   <a href="javascript:void(0)" onclick="sortDateYear(<?php echo $year?>)" >
                   <?php echo $year?>
                      </a>
                   </div>
                   
        
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
     
                     <div class="panel panel-default" style="margin: 0;">
           <div id="collapse<?php echo $year?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $year?>" style="height: auto;">
              
          <div class="panel-body">
                <ul class="body">
                 <?php $months=array(
                      '1'=>'January',
                      '2'=>'February',
                      '3'=>'March',
                      '4'=>'April',
                      '5'=>'May',
                      '6'=>'June',
                      '7'=>'July',
                      '8'=>'August',
                      '9'=>'September',
                      '10'=>'October',
                      '11'=>'November',
                      '12'=>'December'
                        
                    );
                  ?>
                  <?php if($year == date('Y')):?>
                    <?php for($i=01; $i <= date('m'); $i++):?>
                     <li><a href="javascript:void(0)" onclick="sortDate(<?php echo $year?>,<?php echo $i;?>)"><img src="<?php echo theme_url('assets/img/star.png');?>"> <?php echo $months[$i];?></a></li>
                    <?php endfor;?>
                  <?php else:?>
                       <?php for($i=01; $i <= 12; $i++):?>
                     <li><a href="javascript:void(0)" onclick="sortDate(<?php echo $year?>,<?php echo $i;?>)"><img src="<?php echo theme_url('assets/img/star.png');?>"> <?php echo $months[$i];?></a></li>
                    <?php endfor;?>
                  <?php endif;?>
              </ul>    
                
                
               </div>
          </div>
                      </div>
                </div>  
                   
              </ul>
             <?php endfor;?>
            
            </div>    
    
    
 		</div>
    </div>
</div>
 
 <script>

  function sortDate(year,month)
  {
      if(month <= 9)
      {
        var date = year+'-0'+month;
      }
      else
      {
        var date = year+'-'+month;
      }  
      
       $('#sortContent').html('<img src="<?php echo theme_url('assets/img/loading-blue.gif');?>" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
      $.post('<?php echo site_url("blog/sortDate");?>',{date:date},function(data){
          $('#sortContent').html(data);
      });
  }

  function sortDateYear(year)
  {
     
        var date = year;
      
      
       $('#sortContent').html('<img src="<?php echo theme_url('assets/img/loading-blue.gif');?>" style="margin-left: 439px; height: 60px; margin-bottom: 130px; margin-top: 130px;">');
      $.post('<?php echo site_url("blog/sortDate");?>',{date:date},function(data){
          $('#sortContent').html(data);
      });
  }


 </script>