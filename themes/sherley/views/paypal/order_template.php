<p>Dear Shirley Bredal KTM pvt. ltd,</p>
<p>{PRODUCT_NAME} ({PRODUCT_ID}) has been purchased via paypal. Please see the following Details:</p>
<p>Product ID: {PRODUCT_ID}</p>
<p>Product NAME: {PRODUCT_NAME}</p>
<p>Ordered By: {ORDER_BY}</p>
<p>Ordered Person Email: {ORDER_BY_EMAIL}</p>
<p>Quantity: {QUANTITY}</p>
<p>Price: {PRICE}</p>
<p>Total: {TOTAL}</p>
<p>Paypal Transaction ID : {TXN_ID}</p>
<p>Order Date: {ORDER_DATE}</p>
<p>Thank you</p>
<p>&nbsp;</p>
