<div id="content-wrapper">
                <div class="container">
                    <div class="">
                        <div class="header-for-light">
						
                            <h3 class="sherley-user"><?php echo lang('your_cart');?></h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">	
<?php if ($this->go_cart->total_items()==0):?>
    <div class="alert alert-warning alert-dismissable">
        <a class="close" data-dismiss="alert">×</a>
        <?php echo lang('empty_view_cart');?>
    </div>
<?php else: ?>
    
    <?php /*?><div class="page-header">
        <h2><?php echo lang('your_cart');?></h2>
    </div><?php */?>
    <?php echo form_open('cart/update', array('id'=>'update_cart_form'));?>
    
	
	
	
    <?php include('checkout/summary.php');?>
    
    
    <div class="">
    <div class="sherley-spannew">
        <div class="span5">
            <label><?php echo lang('coupon_label');?></label>
            <input type="text" name="coupon_code" class="span3">
            <input class="btn btn-default button-coupon" type="submit" value="<?php echo lang('apply_coupon');?>"/>
            <br/>
            <?php if($gift_cards_enabled):?>
                   <label style="margin-top:15px;"><?php echo lang('gift_card_label');?></label>
                <input type="text" name="gc_code" class="span3" style="margin:0px;">
                <input class="btn btn-default button-coupon" type="submit" value="<?php echo lang('apply_gift_card');?>"/> 
            <?php endif;?>
        </div>
        </div>
        
        <div class="sherley-spanold">
        <div class="span7" style="text-align:right;">
                <input id="redirect_path" type="hidden" name="redirect" value=""/>
    
                <?php if(!$this->customer_model->is_logged_in(false,false)): ?>
                     <!-- <input class="btn btn-default" type="submit" onclick="$('#redirect_path').val('checkout/login');" value="<?php echo lang('login');?>"/> -->
                    <!-- <input class="btn btn-default" type="submit" onclick="$('#redirect_path').val('checkout/register');" value="<?php echo lang('register_now');?>"/> -->
                <?php endif; ?>
                    <input class="btn btn-default update-btn" type="submit" value="<?php echo lang('form_update_cart');?>"/>
                    
            
             <a href="<?php echo site_url('')?>" class="btn btn-default button-coupon"><?php echo lang('continue_shopping');?></a>
             
             <div class="sherley-checkout">
             <?php if ($this->customer_model->is_logged_in(false,false) || !$this->config->item('require_login')): ?>
                <input class="btn btn-default" type="button" onclick="validateCheckout()" value="<?php echo lang('form_checkout');?>"/>
            <?php endif; ?>
            </div>
        </div>
        </div>
    </div><!--row-->

</form>
<?php endif; ?>

   </div>
                    </div>
                </div>
            </div> 
        </div>
        
        
<!-- Modal -->
<div class="addcart-modal">
    <div class="modal fade bs-example-modal-sm" id="checkoutModal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" style="color: #bae3dd;">Notice</h4>
          </div>
          <div class="modal-body">
              <div id="cartMessage" style="color: #edd2c4;">
                <?php if($this->session->userdata('currency') == 'euro'):
                        $min_checkout_amount = round(199*$this->exrate['rate_euro']).' '.$this->euro_currency; 
                      elseif($this->session->userdata('currency') == 'dollar'):	
                        $min_checkout_amount = round(199*$this->exrate['rate_dollar']).' '.$this->usd_currency; 
                      else:
                        $min_checkout_amount = round(199).' '.$this->base_currency; 
                      endif;
                ?>
                <?php echo lang('amount_not_enough_for_checkout');?> <?php echo $min_checkout_amount;?>
              </div>
          </div>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
</div>



<script>
	/*function validateCheckout()
	{
		var checkout_amount = <?php echo ($this->go_cart->total())?$this->go_cart->total():0 ?>;
		if(checkout_amount && checkout_amount >= 199 )
		{	
			$('#redirect_path').val('checkout');
			$('#update_cart_form').submit();
		}
		else
		{
			$('#checkoutModal').modal();
			return false;
		}
	}*/
</script>