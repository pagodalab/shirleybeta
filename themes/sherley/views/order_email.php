<H3><?php echo lang('order_number')?>: <?php echo $order_id ?></H3>

{download_section}
<div class="shipping-info">
	<table style="width: 100%; text-align: left; border-collapse: collapse; color: #8e8f8e">
		<tr style="border-bottom: 1px solid #bdbdbd;">
			<td>
				<strong><?php echo lang('billing_address');?></strong><br/>
				<?php 		$bill = $customer['bill_address'];
				
				if(!empty($bill['company'])) echo $bill['company'].'<br>';
				echo $bill['firstname'].' '.$bill['lastname'].' &lt;'.$bill['email'].'&gt;<br>';
				echo $bill['phone'].'<br>';
				echo $bill['address1'].'<br>';
				if(!empty($bill['address2'])) echo $bill['address2'].'<br>';
				echo $bill['city'].', '.$bill['zone'].' '.$bill['zip'];
				?> <br/>
			</td>
			<td>
				<strong><?php echo lang('shipping_address');?></strong><br/>
				<?php 		$ship = $customer['ship_address'];
				
				if(!empty($ship['company'])) echo $ship['company'].'<br>';
				echo $ship['firstname'].' '.$ship['lastname'].' &lt;'.$ship['email'].'&gt;<br>';
				echo $ship['phone'].'<br>';
				echo $ship['address1'].'<br>';
				if(!empty($ship['address2'])) echo $ship['address2'].'<br>';
				echo $ship['city'].', '.$ship['zone'].' '.$ship['zip'];
				?> <br/>
			</td>
			<td>
				<strong><?php echo lang('payment_information');?></strong><br/>
				<?php echo $payment['description']; ?>
			</td>
		</tr>
	</table>
</div>
<div class="client-table">
	
	<table style="width: 100%; text-align: left; border-collapse: collapse; color: #8e8f8e">
		<thead>
			<tr style="border-bottom: 1px solid #bdbdbd;">
				<th style="line-height: 2">Name</th>
				<!--<th style="line-height: 2">Color</th>-->
				<th style="line-height: 2">Size</th>
				<th style="line-height: 2">Qty</th>
				<th style="line-height: 2">Rate</th>
				<th style="line-height: 2">Gross</th>
			</tr>
		</thead>
		
		<tbody>
			<?php
			$td	= 'class="gc_even"';
			$subtotal = 0;
			foreach ($this->go_cart->contents() as $cartkey=>$product):?>
			<!--<pre><?php print_r($product)?></pre>-->
			<tr <?php echo $td;?>>
				<td style="line-height: 2">
					<span><?php echo $product['name_'.$this->culture_code]; ?></span><br/>
				</td>
<!--                                <td style="line-height: 2">
                                        <span>Sku: <?php echo $product['sku']; ?></span>
                                    </td>-->
                                    <td style="line-height: 2"><?php echo $product['size_name']?></td>
                                    <td style="line-height: 2"><?php echo $product['quantity']?></td>
				<?php /*<td>
					
					<?php echo $product['excerpt'];
						if(isset($product['options'])) {
							echo '<table cellspacing="0" cellpadding="0">';
							foreach ($product['options'] as $name=>$value)
							{
								echo '<tr>';
								if(is_array($value))
								{
									echo '<td><strong>'.$name.':</strong></td><td class="cart_option">';
									foreach($value as $item)
									{
										echo '<div>'.$item.'</div>';
									}
									echo '</td>';
								} 
								else 
								{
									echo '<td><strong>'.$name.':</strong></td><td class="cart_option" > '.$value.'</td>';
								}
								echo '</tr>';
							}
							echo '</table>';
						}
						?>
					
					</td>*/?>
					<td style="line-height: 2">
						<?php echo format_currency($product['price']);?>
					</td>
					<td style="line-height: 2"><?php echo format_currency($product['price']*$product['quantity']); ?></td>
					<td style="line-height: 2">&nbsp;</td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	<hr>
	<table  style="width: 100%; text-align: left; border-collapse: collapse; color: #8e8f8e">
		<?php if($this->go_cart->group_discount() > 0)  : ?> 
			<tr>
				<td colspan="3" style="line-height: 2"><?php echo lang('group_discount');?></td>
				<td colspan="3" style="line-height: 2"><?php echo format_currency(0-$this->go_cart->group_discount()); ?></td>
			</tr>
		<?php endif; ?>

		<tr>
			<td colspan="3" style="line-height: 2"><?php echo lang('subtotal');?></td>
			<td colspan="3" style="line-height: 2">
				<?php echo format_currency($this->go_cart->subtotal()); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="line-height: 2"><?php echo lang('shipping');?>: <?php echo $shipping['method'] ?></td>
			<td colspan="3" style="line-height: 2"><?php echo format_currency($shipping['price']) ?></td>
			<tr>
				<?php if($this->go_cart->coupon_discount() > 0)  :?> 
					<tr>
						<td colspan="3" style="line-height: 2"><?php echo lang('coupon_discount');?></td>
						<td colspan="3" style="line-height: 2"><?php echo format_currency(0-$this->go_cart->coupon_discount()); ?>                </td>
					</tr>
					<?php if($this->go_cart->order_tax() != 0) :// Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?> 
						<tr>
							<td colspan="3" style="line-height: 2"><?php echo lang('discounted_subtotal');?></td>
							<td colspan="3" style="line-height: 2"><?php echo format_currency($this->go_cart->discounted_subtotal(), 2, '.', ','); ?>                </td>
						</tr>

						<?php 
					endif;
				endif;
				?>
				<?php if($this->go_cart->order_tax() != 0) : ?> 
					<tr>
						<td colspan="3" style="line-height: 2"><?php echo lang('taxes');?></td>

						<td colspan="3" style="line-height: 2"><?php echo format_currency($this->go_cart->order_tax()); ?>                </td>
					</tr>
				<?php endif;   ?>

				<?php if($this->go_cart->gift_card_discount() != 0) : ?> 
					<tr>
						<td colspan="3" style="line-height: 2"><?php echo lang('gift_card');?></td>

						<td colspan="3" style="line-height: 2"><?php echo format_currency($this->go_cart->gift_card_discount()); ?>                </td>
					</tr>
				<?php endif;   ?>
				<tr> 
					<td colspan="3" style="line-height: 2">
						<div></div>
						<?php echo lang('grand_total');?>
					</td>
					<td colspan="3" style="line-height: 2">
						<?php echo format_currency($this->go_cart->total()); ?>
					</td>
				</tr>
			</table>
		</div>