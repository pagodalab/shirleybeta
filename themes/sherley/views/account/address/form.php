<?php

$f_id		= array('id'=>'f_id', 'style'=>'display:none;', 'name'=>'id', 'value'=> set_value('id',isset($id)?$id:''));
$f_company	= array('id'=>'f_company', 'class'=>'input', 'name'=>'company', 'value'=> set_value('company',isset($company)?$company:''));
$f_address1	= array('id'=>'f_address1', 'class'=>'input', 'name'=>'address1', 'value'=>set_value('address1', isset($address1)?$address1:''));
$f_address2	= array('id'=>'f_address2', 'class'=>'input', 'name'=>'address2', 'style'=>'display:none;','value'=> set_value('address2',isset($address2)?$address2:''));
$f_first	= array('id'=>'f_firstname', 'class'=>'input', 'name'=>'firstname', 'value'=> set_value('firstname', isset($firstname)?$firstname:''));
$f_last		= array('id'=>'f_lastname', 'class'=>'input', 'name'=>'lastname', 'value'=> set_value('lastname',isset($lastname)?$lastname:''));

$f_first_kana= array('id'=>'f_firstname_kana', 'class'=>'input', 'name'=>'firstname_kana','value'=> set_value('firstname_kana',isset($firstname_kana)?$firstname_kana:''));
$f_last_kana	= array('id'=>'f_lastname_kana', 'class'=>'input', 'name'=>'lastname_kana', 'value'=> set_value('lastname_kana',isset($lastname_kana)?$lastname_kana:''  ));
$f_company_kana	= array('id'=>'f_company_kana', 'class'=>'input', 'name'=>'company_kana', 'value'=> set_value('company_kana',isset($company_kana)?$company_kana:'' ));

$f_email	= array('id'=>'f_email', 'class'=>'input', 'name'=>'email', 'value'=>set_value('email', isset($email)?$email:''));
$f_phone	= array('id'=>'f_phone', 'class'=>'input', 'name'=>'phone', 'value'=> set_value('phone', isset($phone)?$phone:''));
$f_fax		= array('id'=>'f_fax', 'class'=>'input', 'name'=>'fax', 'value'=> set_value('fax', isset($fax)?$fax:''));
$f_city		= array('id'=>'f_city', 'class'=>'input', 'name'=>'city', 'value'=>set_value('city', isset($city)?$city:''));
$f_zip		= array('id'=>'f_zip', 'maxlength'=>'10', 'class'=>'bill input', 'name'=>'zip', 'value'=> set_value('zip',isset($zip)?$zip:''));

echo form_input($f_id);

?>
<div id="form_error" class="error" style="display:none;"></div>
    <div class="laxtable_d">
        <table class="form laxtable">
            <tbody>
            <tr>
              <td><span class="required1">*</span><?php echo lang('address_lastname');?></td>
              <td><?php echo form_input($f_last);?></td>
                <td><span class="required1">*</span><?php echo lang('address_firstname');?></td>
                <td><?php echo form_input($f_first);?></td>
            </tr>
            <tr>
              <td><span class="required1">*</span><?php echo lang('lastname_kana');?></td>
              <td><?php echo form_input($f_last_kana);?></td>
                <td><span class="required1">*</span><?php echo lang('firstname_kana');?></td>
              <td><?php echo form_input($f_first_kana);?></td>
            </tr>
            
            <tr>
              <td><span class="required1"></span><?php echo lang('address_company');?></td>
              <td><?php echo form_input($f_company);?> </td>
                <td><span class="required1"></span><?php echo lang('company_kana');?></td>
              <td><?php echo form_input($f_company_kana);?> </td>
            </tr>
            
             <tr>
              <td><span class="required1"></span><?php echo lang('address_postcode');?></td>
              <td><?php echo form_input($f_zip);?></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr style="display: none;">
              <td><span class="required1">*</span><?php echo lang('address_country');?></td>
              <td><?php echo form_dropdown('country_id', $countries_menu, set_value('country_id', $country_id), 'style="width:200px; display:block;" id="f_country_id" class="input"');?></td>
                <td></td>
                <td></td>
            </tr>            
             <tr>
              <td><span class="required1">*</span><?php echo lang('address_state');?></td>
              <td><?php echo form_dropdown('zone_id', $zones_menu, set_value('zone_id', $zone_id), 'style="width:126px; display:block;" id="f_zone_id" class="input"');?></td>
                <td></td>
                <td></td>
            </tr>
           <tr>
              <td><span class="required1">*</span><?php echo lang('address_city');?></td>
              <td><?php echo form_input($f_city);?></td>
                <td></td>
                <td></td>
            </tr>
            
             <tr>
              <td><span class="required1">*</span><?php echo lang('address');?></td>
              <td><?php echo form_input($f_address1).'<br/>'.form_input($f_address2);?></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
              <td><span class="required1">*</span><?php echo lang('address_phone');?></td>
              <td><?php echo form_input($f_phone);?></td>
                <td></td>
                <td></td>
            </tr>
             <tr>
              <td><span class="required1">*</span><?php echo lang('fax');?></td>
              <td><?php echo form_input($f_fax);?></td>
                <td></td>
                <td></td>
            </tr>
             <tr>
              <td><span class="required1">*</span><?php echo lang('address_email');?></td>
              <td><?php echo form_input($f_email);?>
                </td>
                <td></td>
                <td></td>
            </tr>
          </tbody>
          </table>
     </div>   
  
	<div class="clear"></div>
	<div class="center">
		<input type="button" value="<?php echo lang('register');?>" onclick="save_address(); return false;" class="axe1 "/>
	</div>
	
<script type="text/javascript">
$(function(){
    $('#f_email').width(150);
	$('#f_country_id').change(function(){
			$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#f_country_id').val()}, function(data) {
			  $('#f_zone_id').html(data);
			});
		});
});

function save_address()
{
	$.post("<?php echo site_url('secure/address/form');?>/"+$('#f_id').val(), {	company: $('#f_company').val(),
																				firstname: $('#f_firstname').val(),
																				lastname: $('#f_lastname').val(),
																				firstname_kana: $('#f_firstname_kana').val(),
																				lastname_kana: $('#f_lastname_kana').val(),
																				company_kana: $('#f_company_kana').val(),
																				email: $('#f_email').val(),
																				phone: $('#f_phone').val(),
																				fax: $('#f_fax').val(),
																				address1: $('#f_address1').val(),
																				address2: $('#f_address2').val(),
																				city: $('#f_city').val(),
																				country_id: $('#f_country_id').val(),
																				zone_id: $('#f_zone_id').val(),
																				zip: $('#f_zip').val()
																				},
		function(data){
			if(data == 1)
			{
				window.location = "<?php echo site_url('secure/address');?>";
			}
			else
			{
				$('#form_error').show().html(data);
				//call resize twice to fix a wierd bug where the height is overcompensated
				$.fn.colorbox.resize();
			}
		});
}
</script>