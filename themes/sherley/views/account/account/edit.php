<div id="content-wrapper">
<div class="container">
    <div class="row">    
        <ul class="breadcrumb">
            <li> <a href="<?php echo site_url() ?>">Home</a></li>
            <li><a href="<?php echo site_url('account') ?>">Account</a></li>
            <li class="active">Edit Account</li>
        </ul>
        <?php //echo "<pre>"; print_r($customer);?>
        <script type="text/javascript">
            $(document).ready(function(){
                
                //if we support placeholder text, remove all the labels
                if(!supports_placeholder())
                {
                    $('.placeholder').show();
                }
                
                <?php
                // Restore previous selection, if we are on a validation page reload
                $zone_id = set_value('zone_id');
        
                echo "\$('#zone_id').val($zone_id);\n";
                ?>
            });
            
            function supports_placeholder()
            {
                return 'placeholder' in document.createElement('input');
            }
        </script>
           
		<div class="inner-container">
          	
            <?php
		  //echo "<pre>"; print_r($customer);
            $first = array('id' => 'firstname', 'class' => 'form-control', 'name' => 'firstname', 'value' => set_value('firstname', $customer['firstname']));
            $last = array('id' => 'lastname', 'class' => 'form-control', 'name' => 'lastname', 'value' => set_value('lastname', $customer['lastname']));
            //$company = array('id' => 'company', 'class' => 'form-control', 'name' => 'company', 'value' => set_value('company', $customer['company']));
            $email = array('id' => 'email', 'class' => 'form-control', 'name' => 'email', 'value' => set_value('email', $customer['email']));
            $phone = array('id' => 'phone', 'class' => 'form-control', 'name' => 'phone', 'value' => set_value('phone', $customer['phone']));
		  $address1 = array('id' => 'address1', 'class' => 'form-control', 'name' => 'address1', 'value' => set_value('address1', $customer['address1']));
		  $address2 = array('id' => 'address2', 'class' => 'form-control', 'name' => 'address2', 'value' => set_value('address2', $customer['address2']));
		  $zip = array('id' => 'zip', 'class' => 'form-control', 'name' => 'zip', 'value' => set_value('zip', $customer['zip']));
		  $city = array('id' => 'city', 'class' => 'form-control', 'name' => 'city', 'value' => set_value('city', $customer['city']));
		  
		if(!empty($customer['country_id']))
        {
            $zone_menu	= $this->location_model->get_zones_menu($customer['country_id']);
        }
        else
        {
            $zone_menu = array(''=>'')+$this->location_model->get_zones_menu(array_shift(array_keys($countries)));
        }
		  
            ?>
		<div class="account-form">
            <?php echo form_open('account/edit','role="form"'); ?>
            <table class="table">
     
		
                <h2><?php echo lang('account_information'); ?></h2>
		<?php //echo "<pre>"; print_r($this->session->all_userdata());?>
                
                <?php /*?><div class="row">
                    <div class="col-sm-12">
                        <label for="company" ><?php echo lang('account_company');?></label>
                        <?php echo form_input($company);?>
                    </div>
                </div><?php */?>
               <div class="form-group">
                <div class="row">	
                    <div class="col-sm-6">
                        <label class="control-label"><?php echo lang('account_firstname');?></label>
                        <?php echo form_input($first);?>
                    </div>
                
                    <div class="col-sm-6">
                        <label for="account_lastname"><?php echo lang('account_lastname');?></label>
                        <?php echo form_input($last);?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="account_email"><?php echo lang('account_email');?></label>
                        <?php echo form_input($email);?>
                    </div>
                
                    <div class="col-sm-6">
                        <label for="account_phone"><?php echo lang('account_phone');?></label>
                        <?php echo form_input($phone);?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="account_country">Country</label>
                        <?php echo form_dropdown('country_id',$countries, @$customer['country_id'], 'id="country_id" class="address form-control col-md-8"');?>
                    </div>
                
                    <div class="col-sm-6">
                        <label for="account_zone">State</label>
                        <?php echo form_dropdown('zone_id',$zone_menu, @$customer['zone_id'], 'id="zone_id" class="address form-control col-md-8" ');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="account_address1">Address1</label>
                        <?php echo form_input($address1);?>
                    </div>
                
                    <div class="col-sm-6">
                        <label for="account_address2">Address2</label>
                        <?php echo form_input($address2);?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="account_city">City</label>
                        <?php echo form_input($city);?>
                    </div>
                
                    <div class="col-sm-6">
                        <label for="account_zip">Zip</label>
                        <?php echo form_input($zip);?>
                    </div>
                </div>
			</div>	<br/>
                <input type="submit" value="<?php echo lang('form_submit');?>" class="btn btn-default" />
            </table>

            </form>
            </div>
        </div>
    </div>
</div>
</div>
<!------ home_feature ---------------->


<script>
$(document).ready(function() {
            $('#country_id').change(function(){
                populate_zone_menu();
            });	
        
        });
        // context is ship or bill
        function populate_zone_menu(value)
        {
            $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
                $('#zone_id').html(data);
            });
        }
</script>