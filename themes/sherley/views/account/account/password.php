<!------ home_feature ---------------->
 <div id="content-wrapper">
<div class="container">
    <div class="row">
            <ul class="breadcrumb">
				   <li> <a href="<?php echo site_url()?>"><?php echo lang('home');?></a></li>
				   <li><a href="<?php echo site_url('secure/account')?>"><?php echo lang('account_info');?></a></li>
				   <li><a href="<?php echo site_url('secure/account/password')?>"><?php echo lang('change_password');?></a></li>
			  </ul>
          
		  <div class="inner-container">        
           <!----Order History---->     
    <div class="order_history">
                            <h2 class="sherley-user"><span>Change</span> Password</h2>
                        </div>
                         <div class="order_history">
                                <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
           <?php echo form_open('account/password'); ?>
           
          
           
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="account_email"><?php echo lang('account_password');?> *</label>
							<div>
								<input type="password" name="password" id="password" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="account_phone"><?php echo lang('account_confirm');?></label>
							<div>
								<input type="password" name="confirm" id="confirm" class="form-control"/>
							</div>
					</div>  
				</div>          
            
            </div>
        
              <input type="submit" value="<?php echo lang('form_submit');?>" class="btn btn-default" />
                
            </form>
        
        </div>
  </div>
</div>
  <!------ home_feature ---------------->
