<?php $this->load->view('header')?>
<!------ left menu---------------->
<?php $this->load->view('menu/secure_menu')?>
<!------ left menu---------------->

<!------ home_feature ---------------->
        <div id="home_feature">

<div id="content"> 
<div class="my_content_1">
 <div class="breadcrumb">
  <ul id="crumbs">
       <li> <a href="<?php echo site_url()?>"><?php echo lang('home');?></a></li>
       <li><a href="<?php echo site_url('secure/account')?>"><?php echo lang('account_info');?></a></li>
	   <li><a href="<?php echo site_url('secure/newsletter')?>"><?php echo lang('account_newsletter_subscribe');?></a></li>
  </ul>
  </div>
		<div id="my_information">
			<?php echo form_open('secure/account/newsletter'); ?>
				
				<div class="form_wrap">
					<div>
						 <?php echo lang('account_newsletter_subscribe');?>: <input type="radio" name="email_subscribe" value="1" <?php if((bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/> <?= lang('subscribe')?>
                         <input type="radio" name="email_subscribe" value="0" <?php if(!(bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/> <?= lang('unsubscribe')?>
					</div>
					
				</div>
                <div class="form_wrap form_wrap_lax" style="text-align:center;">
					<input type="submit" value="<?php echo lang('save');?>"  class="axe1" />
				</div>
             </form>
          </div>
</div>
</div>          

        </div>
 <!------ home_feature ---------------->
<?php $this->load->view('footer')?>
