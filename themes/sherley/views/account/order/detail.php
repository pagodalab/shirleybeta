       <div id="content-wrapper">
<div class="container">
    <div class="row">
            <ul class="breadcrumb">
       <li> <a href="<?php echo site_url()?>"><?php echo lang('home');?></a></li>
       <li><a href="<?php echo site_url('account')?>"><?php echo lang('account_info');?></a></li>
	   <li><a href="<?php echo site_url('account/order')?>"><?php echo lang('order_history');?></a></li>
  </ul>
  
 
<div class="row">
<div class="col-sm-4">
		<h3><?php echo lang('account_info');?></h3>
		
		<?php echo (!empty($order->company))?$order->company.'<br>':'';?>
		<?php echo $order->firstname;?> <?php echo $order->lastname;?> <br/>
		<?php echo $order->email;?> <br/>
		<?php echo $order->phone;?>
		
	</div>
	<div class="col-sm-4">
		<h3><?php echo lang('billing_address');?></h3>
		<?php echo (!empty($order->bill_company))?$order->bill_company.'<br/>':'';?>
		<?php echo $order->bill_firstname.' '.$order->bill_lastname;?> <br/>
		<?php echo $order->bill_address1;?><br>
		<?php echo (!empty($order->bill_address2))?$order->bill_address2.'<br/>':'';?>
		<?php echo $order->bill_city.', '.$order->bill_zone.' '.$order->bill_zip;?><br/>
		<?php echo $order->bill_country;?><br/>
		
		<?php echo $order->bill_email;?><br/>
		<?php echo $order->bill_phone;?>
	</div>
   <div class="col-sm-4">
		<h3><?php echo lang('shipping_address');?></h3>
		<?php echo (!empty($order->ship_company))?$order->ship_company.'<br/>':'';?>
		<?php echo $order->ship_firstname.' '.$order->ship_lastname;?> <br/>
		<?php echo $order->ship_address1;?><br>
		<?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
		<?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
		<?php echo $order->ship_country;?><br/>
		
		<?php echo $order->ship_email;?><br/>
		<?php echo $order->ship_phone;?>
	</div>
</div>

<div class="row">
	<ul class="bill_row2">
   <li> <div class="col-sm-4 order_details_">
		<h3><?php echo lang('order_details');?></h3>
		
		<?php if(!empty($order->referral)):?>
			<strong><?php echo lang('referral');?>: </strong><?php echo $order->referral;?><br/>
		<?php endif;?>
		<?php if(!empty($order->is_gift)):?>
			<strong><?php echo lang('is_gift');?></strong>
		<?php endif;?>
		
		<?php if(!empty($order->gift_message)):?>
			<strong><?php echo lang('gift_note');?></strong><br/>
			<?php echo $order->gift_message;?>
		<?php endif;?>
		
	</div></li>
	<li><div class="col-sm-4 payment_method_">
		<h3><?php echo lang('payment_method');?></h3>
		<?php echo $order->payment_info; ?>
	</div></li>
	<li><div class="col-sm-4 shipping_details">
		<h3><?php echo lang('shipping_details');?></h3>
		<?php echo $order->shipping_method; ?>
		<?php if(!empty($order->shipping_notes)):?><div style="margin-top:10px;"><?php echo $order->shipping_notes;?></div><?php endif;?>
	</div></li>
    </ul>
</div>
<br/>
<div align="center">
	<h3>Images</h3>
    <br/>
    <?php if(empty($images)){
			echo "No Images Available!";
		}else{ 
		foreach($images as $image){?>
        	<a href="<?php echo base_url('uploads/order/'.$order->id.'/'.$image['file_name'])?>" class="fresco" data-fresco-group="shared_options" data-fresco-group-options="ui: 'inside'" data-fresco-caption="<?php echo $image['file_name']?>"><img src="<?php echo base_url('uploads/order/'.$order->id.'/'.$image['file_name'])?>" alt="" height="150px" width="150px"></a>
	<?php }}?>
</div>
<br/>
<table class="table table-bordered">
	<thead>
		<tr>
			<th><?php echo lang('name');?></th>
			<th><?php echo lang('description');?></th>
			<th><?php echo lang('price');?></th>
			<th><?php //echo lang('quantity');?></th>
			<th><?php echo lang('totals');?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($order->contents as $orderkey=>$product):?>
		<tr>
			<td>
				<?php echo $product['name_'.$this->culture_code];?>
				<?php echo (trim($product['sku']) != '')?'<br/><small>'.lang('sku').': '.$product['sku'].'</small>':'';?>
				
			</td>
			<td>
				<?php //echo $product['excerpt'];?>
				<?php
				
				// Print options
				if(isset($product['options']))
				{
					foreach($product['options'] as $name=>$value)
					{
						$name = explode('-', $name);
						$name = trim($name[0]);
						if(is_array($value))
						{
							echo '<div>'.$name.':<br/>';
							foreach($value as $item)
							{
								echo '- '.$item.'<br/>';
							}	
							echo "</div>";
						}
						else
						{
							echo '<div>'.$name.': '.$value.'</div>';
						}
					}
				}
				
				if(isset($product['gc_status'])) echo $product['gc_status'];
				?>
			</td>
			<td><?php echo format_currency($product['price']);?></td>
			<td><?php //echo $product['quantity'];?></td>
			<td><?php echo format_currency($product['price']*$product['quantity']);?></td>
		</tr>
		<?php endforeach;?>
		</tbody>
		<tfoot>
		<?php if($order->coupon_discount > 0):?>
		<tr>
			<td><strong><?php echo lang('coupon_discount');?></strong></td>
			<td colspan="3"></td>
			<td><?php echo format_currency(0-$order->coupon_discount); ?></td>
		</tr>
		<?php endif;?>
		
		<tr>
			<td><strong><?php echo lang('subtotal');?></strong></td>
			<td colspan="3"></td>
			<td><?php echo format_currency($order->subtotal); ?></td>
		</tr>
		
		<?php 
		$charges = @$order->custom_charges;
		if(!empty($charges))
		{
			foreach($charges as $name=>$price) : ?>
				
		<tr>
			<td><strong><?php echo $name?></strong></td>
			<td colspan="3"></td>
			<td><?php echo format_currency($price); ?></td>
		</tr>	
				
		<?php endforeach;
		}
		?>
		<tr>
			<td><strong><?php echo lang('shipping');?></strong></td>
			<td colspan="3"><?php echo $order->shipping_method; ?></td>
			<td><?php echo format_currency($order->shipping); ?></td>
		</tr>
		
		<tr>
			<td><strong><?php echo lang('tax');?></strong></td>
			<td colspan="3"></td>
			<td><?php echo format_currency($order->tax); ?></td>
		</tr>
		<?php if($order->gift_card_discount > 0):?>
		<tr>
			<td><strong><?php echo lang('giftcard_discount');?></strong></td>
			<td colspan="3"></td>
			<td><?php echo format_currency(0-$order->gift_card_discount); ?></td>
		</tr>
		<?php endif;?>
		<tr>
			<td><h3><?php echo lang('total');?></h3></td>
			<td colspan="3"></td>
			<td><strong><?php echo format_currency($order->total); ?></strong></td>
		</tr>
	</tfoot>
</table>
</div>
</div>

        </div>
        
 <!------ home_feature ---------------->