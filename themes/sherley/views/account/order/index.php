      <div id="content-wrapper">
<div class="container">
    <div class="row">
            <ul class="breadcrumb">
  
				       <li> <a href="<?php echo site_url()?>">Home</a></li>
				       <li><a href="<?php echo site_url('account')?>">Account</a></li>
					   <li><a href="<?php echo site_url('account/order')?>">Order History</a></li>
				 </ul>
                    
                <div>
                    <div class="container">
                        <div class="header-for-light">
                            <h2 class="wow fadeInRight animated" data-wow-duration="1s"><?php echo lang('order_history');?></h2>
                        </div>
                        <div class="row">
                            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
		
		<?php if($orders):
			echo $orders_pagination;
		?>
		<table class="table">
			<thead>
				<tr>
					<th><?php echo lang('order_date');?></th>
					<th><?php echo lang('order_number');?></th>
					<th><?php echo lang('order_status');?></th>
                    <th><?php echo lang('order_total');?></th>
                    <th>Action</th>
				</tr>
			</thead>

			<tbody>
			<?php
			foreach($orders as $order): ?>
				<tr>
					<td>
						<?php $d = format_date($order->ordered_on); 
				
						$d = explode(' ', $d);
						echo $d[0].' '.$d[1].', '.$d[3];
				
						?>
					</td>
					<td><a href="<?php echo site_url('account/order/detail/'.$order->id)?>"><?php echo $order->order_number; ?></a></td>
					<td><?php echo $order->status;?></td>
                    <th><?php echo format_currency($order->total);?></th>
                    <td><a href="<?php echo site_url('account/order/detail/'.$order->id)?>">View</a></td>
				</tr>
		
			<?php endforeach;?>
			</tbody>
		</table>
		<?php else: ?>
			<?php echo lang('no_order_history');?>
		<?php endif;?>
	</div>
</div>

</div>
</div>


              </div>
            </div>
        