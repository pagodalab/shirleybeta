
       
        <section>
            <div class="second-page-container">
                <div class="block">
                    <div class="container">
                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s">My <span>Wishlist</span></h1>
                        </div>

       
 
              <div class="row">
                            <div class="col-md-12">
                            <?php if(count($products)>0):?>
                                <table class="cart-table table wow fadeInLeft" data-wow-duration="1s">
                                    <thead>
                                        <tr>
                                            <th class="card_product_image">Image</th>
                                            <th class="card_product_name"><?php echo lang('sku');?></th>
                                            <th class="card_product_model"><?php echo lang('name');?></th>
                                            <th class="card_product_quantity"><?php echo lang('price');?></th>
                                            <th class="card_product_price">Action</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
				
                                    <?php
                              	
                              	  foreach($products as $product):
                              	  ?>
                                      <tr>
                                        <td  class="card_product_image" data-th="Image">  
                                			
                                         <a href="<?php echo site_url($product->slug)?>">
                                         <?php
                                              $photo  = theme_img('no_picture.png', lang('no_image_available'));

                              				if($product->images == 'false')
                              				{
                              					 $product->images = array();
                              				}
                              				else
                              				{
                              					 $product->images	= array_values((array)json_decode($product->images));
                              				}				
                                              //$product->images    = array_values($product->images);

                                              if(!empty($product->images[0]))
                                              {
                                                  $primary    = $product->images[0];
                                                  foreach($product->images as $photo)
                                                  {
                                                      if(isset($photo->primary))
                                                      {
                                                          $primary    = $photo;
                                                      }
                                                  }

                                                  $photo  = '<img class="responsiveImage" src="'.base_url('uploads/images/small/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                              }
                                              echo $photo
                              			?>
                                          </a>
                                          </td>
                                          
                                          <td class="card_product_name" data-th="Product Name"><?php echo $product->sku?></td>
                                          
                                        <td class="card_product_name" data-th="Product Name">
                                        <a href="<?php echo site_url($product->slug)?>"><?php echo $product->name?></a></td>

                                        <td class="card_product_price" data-th="Unit Price">            
                                              <div class="price">
                                             	 <?php echo ($product->saleprice==0)? format_currency($product->price):format_currency($product->saleprice);?>          
                                              </div>
                                          </td>
                                        <td class="card_product_total" data-th="Add to Cart">
                                        <a href="<?php echo site_url($product->slug)?>" class="btn-default-1">Buy Now</a>
                                        <a href="<?php echo site_url('account/wishlist/delete')?>?id=<?php echo $product->id?>" class="btn-default-1 trash" style="text-decoration:none;">Remove</a></td>
                                      </tr>
                                    <?php
                              	  endforeach;
                                  
                              	  ?>
                                  <?php if(isset($canvases)):?>
                                  <?php foreach($canvases as $canvas):?>
                                      <tr>
                                        <td>
                                         
                                          <?php 
                                            if($canvas['canvas_image'])
                                            {
                                              $photo  = '<img src="'.base_url('uploads/canvas/user_canvas/templates/'.$canvas['canvas_image']).'" style="height:175px; width:117px;" alt=""/>';
                                              }
                                            else{
                                              $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                             }
                                             
                                          ?> 
                                          <a href="<?php echo site_url('canvas/detail/'.$canvas['user_canvas_id']);?>"><?php echo $photo;?></a>  
                                        </td>
                                        <td><!-- no sku of canvas--></td>
                                        <td><?php echo $canvas['canvas_title'];?></td>
                                        <td></td>
                                         <td class="card_product_total" data-th="Add to Cart">
                                        <a href="<?php echo site_url('canvas/detail/'.$canvas['user_canvas_id'])?>" class="btn-default-1">Buy Now</a>
                                        <a href="<?php echo site_url('account/wishlist/delete_canvas')?>?id=<?php echo $canvas['user_canvas_id']?>" class="btn-default-1 trash" style="text-decoration:none;">Remove</a></td>
                                      </tr>
                                  <?php endforeach?>
                                <?php endif;?>
                                   
                                </tbody>
                            </table>
                        <?php else:
                                echo '<div class="alert alert-info">No items in your wishlist</div>';
                                endif;
                        ?>
                        </div>
                    </div>

                    </div>
                </div>
            </div> 
        </section>



