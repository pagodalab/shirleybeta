<!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script> -->

<div id="content-wrapper">

  <div class="container">
    <div class="row">
     <div class="col-sm-2">
      <div class="listing">
        <!--<a href="<?php echo site_url();?>"><?php echo lang('home');?> ></a>-->
        <!--<a href=""><?php echo strtoupper(lang('contact_us'));?></a>-->
        
        <!-- <h5><?php echo strtoupper(lang('categories'));?></h5>-->
        <!--<hr>-->

      </div>

        <?php /*$categories = $this->menu_category ;?>
        <?php foreach($categories as $category):?>
           <ul class="inline">
              <div class="rotater">              
               <a href="<?php echo site_url($category['slug']);?>">
                  <img class="this-img collapsed" src="<?php echo theme_url('assets/img/star.png');?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category['id']?>" aria-expanded="true" aria-controls="collapse<?php echo $category['id']?>"> 
                    <?php echo strtoupper($category['name_'.$this->culture_code]);?>
                  </a>
               </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
                 <div class="panel panel-default" style="margin: 0;">
                   <div id="collapse<?php echo $category['id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $category['id']?>" style="height: auto;">
                      
                    <div class="panel-body">
                          <ul class="body">
                          <?php $subcategories = $this->category_model->getCategories(array('enabled'=>1,'parent_id'=>$category['id']),'sequence asc')->result_array();?>
                                    <?php foreach($subcategories as $subcategory):?>
                             <li>
                                <a href="<?php echo site_url($subcategory['slug']);?>">
                                  <?php echo strtoupper($subcategory['name_'.$this->culture_code]);?>
                                </a>
                              </li>
                              
                              <?php endforeach;?>
                        </ul>
                    </div>
                  </div>
                </div>
               </div>  
               
          </ul>
        <?php endforeach;*/?>
        

      </div>
    </div>
    <div class="row">

      <div class="col-sm-10">
        <div class="sherley-contact">
          <h5><?php echo strtoupper(lang('contact_us'));?></h5>
          <hr>

          <div class="sherley-contactinput">
            <form action="" method="post" id="contact-form">
              <div class="contact-input form-group">

               <input name="name" type="text" class="form-control" id="name" value="<?php echo set_value('name')?>" placeholder="<?php echo lang('name');?>" required/>
               <span class="error"><?php echo form_error('name')?></span>

             </div> 

             <div class="email-input form-group">
              <input name="email" type="email" class="form-control" value="<?php echo set_value('email')?>" placeholder="<?php echo lang('address_email');?>" required/>
              <span class="error"><?php echo form_error('email')?></span>

            </div>

            <div class="clearfix"></div>
            <div class="message-input form-group">

              <textarea name="message" id="message" class="form-control" rows="10" cols="40" placeholder="<?php echo lang('comment');?>" required><?php echo set_value('message')?></textarea> 
              <span class="error"><?php echo form_error('message')?></span>
            </div>
            <br>

            <div class="button-last">
              <button type="submit" id="submit" value="Send" class="btn btn-default"  style="margin:0"><?php echo lang('form_submit');?></button>
            </div>   
          </form> 
          <div class="message-input"> 
            Email us at <?php echo $gocart['email'] ?>  
          </div>
        </div>    
        
      </div>    
    </div>    

  </div>
</div>
</div>