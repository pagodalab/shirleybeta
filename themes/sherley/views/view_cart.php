<div class="cart-page co-new-page remove-top-space">
    <section class="co-banner-section">
        <div class="banner-background" style="background-image: url('<?php echo theme_url('assets')?>/img/banner/banner-cart.jpg')">
            <div class="banner-content">
                <h1><?php echo lang('your_cart');?></h1>
            </div>
        </div>
    </section>
    <section class="cart-list-section add-space-top">
        <div class="custom-container container-small-width">
            <?php if ($this->go_cart->total_items()==0){?>
                <div class="alert alert-warning alert-dismissable">
                    <a class="close" data-dismiss="alert">×</a>
                    <?php echo lang('empty_view_cart');?>
                </div>
            <?php }else {?>
                <?php echo form_open('cart/update', array('id'=>'update_cart_form'));?>
                <?php include('checkout/summary.php');?>

                <div class="page-content">
                    <div id="cart_other">
                        <div class="row">
                            <div class="col-md-6 col-sm-8 left-block">
                                <div class="row">
                                    <div class="col-sm-4 coupon-title-container">
                                        <p>Enter Coupon Code:</p>
                                    </div>
                                    <div class="col-sm-8 coupon-from-container">
                                        <div class="row">
                                            <div class="col-sm-8 coupon-field">
                                                <input type="text" class="form-control input-shirley" name="coupon_code">
                                            </div>
                                            <div class="col-sm-4 coupon-submit">
                                                <button type="submit" class="btn-shirley btn-dark-small">SUBMIT</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4 right-block">

                                <?php if ($this->go_cart->group_discount() > 0) : ?>
                                    <?php echo lang('group_discount'); ?>
                                    >
                                    <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                    <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                    <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                    <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                        <?php echo round($this->go_cart->group_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                                    <?php else: ?>

                                        <?php echo round($this->go_cart->group_discount()) . ' ' . $this->base_currency; ?>
                                    <?php endif; ?>



                                <?php endif; ?>
                                <div class="pull-right text-right">
                                    <div class="sub-total">
                                        <p>Subtotal: <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                            <?php echo round($this->go_cart->total() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                        <?php else: ?>

                                            <?php echo round($this->go_cart->total()) . ' ' . $this->base_currency; ?>
                                        <?php endif; ?></p>
                                    </div>
                                    <input id="redirect_path" type="hidden" name="redirect" value=""/>
                                    <button class="btn-shirley btn-dark-small"  type="button" onclick="validateCheckout()">Check Out</button>
                                </div>

                                <?php if ($this->go_cart->coupon_discount() > 0) { ?>
                                    <?php echo lang('coupon_discount'); ?></td>

                                    <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                        <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                        <?php echo round($this->go_cart->coupon_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                                    <?php else: ?>

                                        <?php echo round($this->go_cart->coupon_discount()) . ' ' . $this->base_currency; ?>
                                    <?php endif; ?>

                                    <?php if ($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?>
                                        <?php echo lang('discounted_subtotal'); ?>

                                        <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                            <?php echo round($this->go_cart->discounted_subtotal() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                                        <?php else: ?>

                                            <?php echo round($this->go_cart->discounted_subtotal()) . ' ' . $this->base_currency; ?>
                                        <?php endif; ?>

                                        <?php
                                    }
                                }
                                /*         * ************************************************************
                                  Custom charges
                                 * ************************************************************ */
                                $charges = $this->go_cart->get_custom_charges();
                                if (!empty($charges)) {
                                    foreach ($charges as $name => $price) :
                                        ?>


                                        <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                        <?php echo round($price * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                                    <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                        <?php echo round($price * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                        <?php echo round($price * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                        <?php echo round($price * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                        <?php echo round($price * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                        <?php echo round($price * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>
                                    <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                        <?php echo round($price * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                                    <?php else: ?>

                                        <?php echo round($price . ' ' . $this->base_currency); ?>
                                    <?php endif; ?>


                                        <?php
                                    endforeach;
                                }

                                /*         * ************************************************************
                                  Order Taxes
                                 * ************************************************************ */
                                if ($product_in_cart == 1) {
                                    // Show shipping cost if added before taxes
                                    //if($this->go_cart->contents())
                                    // echo '<pre>';
                                    // print_r($this->go_cart->contents());
                                    // exit;
                                    if($this->go_cart->is_free_shipping() != '1'){

                                        if ($this->config->item('tax_shipping') && $this->go_cart->shipping_cost() > 0) :
                                            ?>
                                            <?php echo lang('shipping'); ?>
                                            <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                        <?php else: ?>

                                            <?php echo round($this->go_cart->shipping_cost()) . ' ' . $this->base_currency; ?>
                                        <?php endif; ?>

                                        <?php endif;


                                        if ($this->go_cart->order_tax() > 0) :
                                            ?>
                                            <?php echo lang('tax'); ?>
                                            <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                            <?php echo round($this->go_cart->order_tax() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                                        <?php else: ?>

                                            <?php echo round($this->go_cart->order_tax()) . ' ' . $this->base_currency; ?>
                                        <?php endif; ?>

                                            <?php
                                        endif;
// Show shipping cost if added after taxes
                                        if (!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost() > 0) :
                                            ?>
                                            <?php echo lang('shipping'); ?>
                                            <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                        <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                        <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                            <?php echo round($this->go_cart->shipping_cost() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>


                                        <?php else: ?>

                                            <?php echo round($this->go_cart->shipping_cost()) . ' ' . $this->base_currency; ?>
                                        <?php endif; ?>


                                        <?php endif ?>
                                    <?php }
                                } ?>

                                <?php
                                /* * ************************************************************
                                  Gift Cards
                                 * ************************************************************ */
                                if ($this->go_cart->gift_card_discount() > 0) :
                                    ?>
                                    <?php echo lang('gift_card_discount'); ?>

                                    <?php if ($this->session->userdata('currency') == 'euro'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_euro']) . ' ' . $this->euro_currency; ?>

                                <?php elseif ($this->session->userdata('currency') == 'dollar'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_dollar']) . ' ' . $this->usd_currency; ?>

                                <?php elseif ($this->session->userdata('currency') == 'yen'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_yen']) . ' ' . $this->japanese_currency; ?>

                                <?php elseif ($this->session->userdata('currency') == 'HKD'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_hdollar']) . ' ' . $this->hongkong_currency; ?>

                                <?php elseif ($this->session->userdata('currency') == 'won'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_korea']) . ' ' . $this->korean_currency; ?>
                                <?php elseif ($this->session->userdata('currency') == 'Pound'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_pound']) . ' ' . $this->british_currency; ?>

                                <?php elseif ($this->session->userdata('currency') == 'AUD'): ?>

                                    <?php echo round($this->go_cart->gift_card_discount() * $this->exrate['rate_adollar']) . ' ' . $this->australlian_currency; ?>

                                <?php else: ?>

                                    <?php echo round($this->go_cart->gift_card_discount()) . ' ' . $this->base_currency; ?>
                                <?php endif; ?>

                                <?php endif; ?>


                            </div>
                        </div>
                    </div>

                </div>

                <?php  echo form_close()?>
            <?php } ?>
        </div>
    </section>
</div>






<div class="addcart-modal">
    <div class="modal fade bs-example-modal-sm" id="checkoutModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" style="color: #bae3dd;">Notice</h4>
                </div>
                <div class="modal-body">
                    <div id="cartMessage" style="color: #edd2c4;">
                        <?php if($this->session->userdata('currency') == 'euro'):
                            $min_checkout_amount = round(199*$this->exrate['rate_euro']).' '.$this->euro_currency;
                        elseif($this->session->userdata('currency') == 'dollar'):
                            $min_checkout_amount = round(199*$this->exrate['rate_dollar']).' '.$this->usd_currency;

                        elseif($this->session->userdata('currency') == 'yen'):
                            $min_checkout_amount = round(199*$this->exrate['rate_yen']).' '.$this->japanese_currency;
                        elseif($this->session->userdata('currency') == 'HKD'):
                            $min_checkout_amount = round(199*$this->exrate['rate_hdollar']).' '.$this->hongkong_currency;
                        elseif($this->session->userdata('currency') == 'AUD'):
                            $min_checkout_amount = round(199*$this->exrate['rate_adollar']).' '.$this->australlian_currency;
                        elseif($this->session->userdata('currency') == 'won'):
                            $min_checkout_amount = round(199*$this->exrate['rate_korea']).' '.$this->korean_currency;
                        elseif($this->session->userdata('currency') == 'Pound'):
                            $min_checkout_amount = round(199*$this->exrate['rate_pound']).' '.$this->british_currency;
                        else:
                            $min_checkout_amount = round(199).' '.$this->base_currency;
                        endif;
                        ?>
                        <?php echo lang('amount_not_enough_for_checkout');?> <?php echo $min_checkout_amount;?>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>