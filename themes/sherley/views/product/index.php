<div class="co-new-page product-detail-page">


    <section class="product-detail-section">
        <div class="custom-container">
            <div class="product-detail-row">
                <div class="product-detail-left-block">
                    <div class="product-detail-image-container">

                        <div class="product-detail-image-slider">
                            <?php if(!empty($product->images)): ?>
                                <?php foreach ($product->images as $key => $value): ?>
                                    <div class="product-image-item">
                                        <img src="<?php echo base_url('uploads/images/full/'.$value->filename) ?>">
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="product-image-item">
                                    <img src="<?php echo theme_url('assets/img/no_picture.png') ?>" style="width: 350px;">
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="product-detail-thumb-container">
                            <div class="section-title">
                                <h2>AVAILABLE COLORS:</h2>
                            </div>
                            <div class="product-detail-thumb-slider">
                                <?php foreach($product->related_products as $key => $value): ?>
                                    <?php $related = array_values(json_decode($value->images,true)); ?>
                                    <div class="thumb-item">
                                        <a href="<?php echo site_url($value->slug);?>">
                                            <img src="<?php echo base_url('uploads/images/full/'.$related[0]['filename']) ?>" alt="<?php echo $value->seo_title; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product-detail-right-block">
                    <div class="product-description-container">
                        <div class="product-description-item" id="product-name">
                            <?php $product_name = ($product->display_name_en)?"display_name_".$this->culture_code: "name_".$this->culture_code;  ?>
                            <h1><?php echo $product->$product_name; ?></h1>
                        </div>

                        <div class="product-description-item" id="product-price">
                            <?php $ex_currency = place_productPrice(); ?>
                            <?php //if($product->sale_percent > 0):  // commented to show only single price?>
                            <h2><?php echo round((($product->saleprice > 0) ? $product->saleprice : $product->price) * $this->exrate[$ex_currency['exrate']]) . ' ' . $this->$ex_currency['currency']; ?></h2>
                            <?php //else: ?>
                            <!-- <h2><?php echo round(($product->price)*$this->exrate[$ex_currency['exrate']]). ' '. $this->$ex_currency['currency']; ?></h2> -->
                            <?php //endif; ?>
                        </div>

                        <div class="product-description-item" id="co-product-size">
                            <?php $product_sizes = json_decode($product->size_quantity); ?>
                            <h4>Sizes</h4>
                            <?php $size_count = count((array)$product_sizes); ?>
                            <ul>
                                <?php foreach ($sizes as $key => $value): /*all sizes from controller*/?>
                                    <?php foreach ($product_sizes as $k => $v): /* sizes of product*/?>
                                        <?php if ($v->size == $value->size_id): ?>
                                            <input type="hidden" name="size" class="size" id="product_size" value="<?php echo $value->size_id ?>"/>
                                            <li class="" id="size_list-<?php echo $v->size; ?>">
                                                <a href="javascript:void(0)" onclick="sizeSet1(<?php echo $v->quantity ?>,<?php echo $v->size ?>,<?php echo $product->id; ?>)" >
                                                    <button><?php echo $value->size_code; ?> <?php if($v->quantity == 0 ): ?><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php endif; ?></button>
                                                </a>
                                            </li>

                                            <script type="text/javascript">
                                                $(function(){
                                                    <?php if($reqsize == $v->size ): ?>
                                                    $('#size_list-<?php echo $reqsize; ?>').attr('class', 'active');
                                                    sizeSet1(1, <?php echo $reqsize; ?>, <?php echo $product->id; ?>);
                                                    <?php endif; ?>
                                                    
                                                    <?php if($size_count == 1): ?>
                                                    sizeSet1(<?php echo $v->quantity ?>,<?php echo $v->size ?>,<?php echo $product->id; ?>);
                                                    $('#size_list-<?php echo $v->size; ?>').attr('class', 'active');
                                                    <?php endif; ?>
                                                });
                                            </script>

                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php 
                        $items = $this->go_cart->contents();
                        $qty_count = null;
                        $item_size = null;
                        foreach ($product_sizes as $key => $size):
                            foreach ($items as $k => $item){
                                if ($item['id'] == $product->id && $item['size'] == $size->size) {
                                    $qty_count += $item['quantity'];
                                    $item_size = $item['size'];
                                }
                            }

                            $size_available = $size->quantity;

                            if ($item_size) {
                                if ($size->size == $item_size) {
                                    $size_available = intval($size->quantity) - intval($qty_count);
                                } else {
                                    $size_available = $size->quantity;
                                }
                            }

                            ?>
                            <span id='quantity_size-<?php echo $size->size; ?>' rel='<?php echo $size->quantity; ?>' hidden></span>
                            <!-- <span id='quantity_size-<?php echo $size->size; ?>' rel='<?php echo $size_available; ?>' hidden></span> -->
                        <?php endforeach; ?>

                        <div class="product-description-item" id="product-quantity">
                            <span>Quantity</span>
                            <select name="quantity" id="quantity" id="form-control">
                                <!-- <option value="4">4</option> -->
                            </select>
                        </div>

                        <div class="product-description-item" id="product-add-to-cart">
                            <?php $name = "display_name_".$this->culture_code; ?>
                            <button type="button" class="btn-shirley" onclick="add_to_cart('<?php echo $product->id; ?>','<?php echo htmlentities($product->$name) ?>',event)">
                                Add to basket
                            </button>
                        </div>

                        <div class="product-description-item" id="product-description">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#desc"><?php echo lang('tab_description') ?></a></li>
                                <li><a data-toggle="tab" href="#wash"><?php echo lang('common_washing') ?></a></li>
                                <li><a data-toggle="tab" href="#missing"><?php echo lang('missingyoursize') ?></a></li>
                            </ul>
                            <div class="tab-content">
                                <?php $description = "description_" . $this->culture_code; ?>
                                <div id="desc" class="tab-pane fade in active">
                                    <p><?php echo ($product->$description) ? $product->$description : ''; ?></p>
                                </div>
                                <div id="wash" class="tab-pane fade">
                                    <?php echo lang('washing_desc_text') ?>
                                </div>
                                <?php $notes = "notes_" . $this->culture_code; ?>
                                <div id="missing" class="tab-pane fade">
                                    <?php echo lang('missing_size_text') ?>
                                    <p>To fill out a custom order form:  <a href="<?php echo site_url('custom_order'); ?>">Click Here</a></p>
                                </div>
                            </div>

                        </div>

                        <form id="form-<?php echo $product->id ?>" action="<?php echo site_url('cart/add'); ?>" >
                            <input type="hidden" name="id" value="<?php echo $product->id; ?>" />
                            <input type="hidden" name="size" id="size_1" class='size-<?php echo $product->id?>' />
                            <input type="hidden" name="quantity" id="qty1"/>
                            <input type="hidden" name="color" id="color"/>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="product-detail-similar-section">
        <div class="custom-container">
            <div class="section-title">
                <h1>You may also like</h1>
            </div>

            <div class="section-content">
                <div class="product-detail-similar-slider" id="equalheight">
                    <?php foreach ($products as $value): ?>
                        <?php if ($value['images']) :?>
                            <?php 
                            $image_content = array_values(json_decode($value['images'],true));
                            if (is_array($image_content)) {
                                foreach ($image_content as $img) {
                                    if ($img) {
                                        if (array_key_exists('primary', $img)) {
                                            $primary_image = base_url('uploads/images/medium/' . $img['filename']);
                                        }
                                        if (array_key_exists('hover', $img)) {
                                            $hover_image = base_url('uploads/images/medium/' . $img['filename']);
                                        }
                                    }
                                }
                            } else {
                                $primary_image =    theme_url('assets/img/no_picture.png');
                            }
                            ?>
                            <div class="similar-item">
                                <a href="<?php echo site_url($value['slug'])?>">
                                    <img src="<?php echo $primary_image ?>">
                                    <figcaption><?php echo strtoupper($value["display_name_" . $this->culture_code]); ?></figcaption>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

</div>




<script>
    /* Script for toggling active class in Size Starts*/
    $("#co-product-size li").click(function () {
        $("#co-product-size li").removeClass("active");
        $(this).addClass("active");
    });
    /* Script for toggling active class in Size Ends */


    $(document).ready(function () {
        /* Script for thumb slider Starts */
        $('.product-detail-image-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false
        });
        /* Script for thumb slider Ends */

        /* Script for thumb slider Starts */
        $('.product-detail-thumb-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            autoplay: true,
            responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });
        /* Script for thumb slider Ends */

        /* Script for similar slider Starts */
        $('.product-detail-similar-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            autoplay: true,
            responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });
        /* Script for similar slider Ends */



        $('.product-detail-similar-slider img').imagesLoaded().always( function( instance ) {
            $('.product-detail-similar-slider .similar-item').equalHeights();
        });
    });

    function sizeSet1(qty, size, id) {
        $('#size_1').val(size);
        $('#qty1').val(qty);

        available = parseInt($('#quantity_size-'+size).attr('rel'));
            // available = qty;

            var dialog = document.getElementById('window');
            var content = "";
            if(available > 0) {

                if(available > 10 ) {
                    available = 10;
                }

                for (var i = 1; i <= available; i++) {
                    content += '<option value="' + i + '">' + i + '</option>';
                }

                $('#quantity').html(content).attr('disabled', false);
                $('#product-add-to-cart').show();
            } else {

                // Restock notification
                restockNotification(id, size);

                // $('#myModal').modal('show');
                // $('#product_size1').val(size);
                // $('#product_id1').val(id);
                $('#exit').click(function () {
                    $('body').css("overflow", "visible");
                    dialog.close();
                });
                $('#quantity').html('').attr('disabled', 'disabled');
                $('#product-add-to-cart').hide();
                return false;
            }

            return;

            $('.size').val(size);
            $('.quantity_show').attr('class', 'hide');
            $('#quantity_size-' + id).attr('class', 'quantity_show');

        }






    </script>

    <script type="text/javascript">
        $('.addcart-modal').on('hidden.bs.modal', function () {
            location.reload();
        })
    </script>

