
<?php 
if($this->session->userdata('msg_customorder')){ ?>
<div class="alert alert-info custom_order_msg">

    <?php echo $this->session->userdata('msg_customorder');

    $this->session->unset_userdata('msg_customorder'); ?>
</div>
<?php } ?>


<div class="custom-order-page co-new-page">
    <section>
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php if($this->culture_code == 'en'): ?>
                        <?php echo $setting_en; ?>
                    <?php else: ?>
                        <?php echo $setting_da; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="custom-order-section">
        <div class="custom-container container-small-width">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="custom-order-form co-form">
                        <form  action="<?php echo site_url()?>/custom_order/save" id="custom_order_form"  method="post">
                            <div class="form-title">
                                <h2>Custom Order Form</h2>
                            </div>

                            <div class="form-fields">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input  type="text" class="form-control" id="first_name" name="first_name" placeholder="Name">
                                        </div>
                                        <div class="col-sm-6">
                                            <input  type="text" class="form-control" id="Email" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select  class="form-control" id="style" name="style" onchange="getrelatedColor()">
                                                <option>Styles </option>
                                                <?php foreach($styles as $style): ?>
                                                    <option value="<?php echo $style['id'] ?>"><?php echo $style['style_name_'.$this->culture_code] ?></option>

                                                <?php endforeach; ?>
                                            </select>
                                            <input type="hidden" name="hidden_style" id="hidden_style">
                                        </div>
                                        <div class="col-sm-6">
                                            <select  class="form-control" id="color" name="color" onchange="getrelatedMaterial()" >
                                                <option><?php echo lang('color') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select  class="form-control" id="material" name="material" onchange="getrelatedSize()">
                                                <option><?php echo lang('materials') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select  class="form-control" id="size" name="size" onchange="hiddenchange()">
                                                <option><?php echo lang('size') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-action pull-right">
                                <input type="hidden" name="hidden_style" id="hidden_style">
                                <input type="hidden" name="hidden_color" id="hidden_color">
                                <input type="hidden" name="hidden_material" id="hidden_material">
                                <input type="hidden" name="hidden_size" id="hidden_size">
                                <button type="button" id="submit_btn" class="btn-shirley btn-sm" ><?php echo lang('form_submit') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="verification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-uppercase">Please Verify Your Order</h4>
            </div>
            <div class="modal-body">
                <p><?php echo lang('name') ?>: <span id="placer_name"></span></p>
                <p><?php echo lang('email') ?>: <span id="placer_email"></span></p>
                <p>Style: <span id="placer_style"></span></p>
                <p><?php echo lang('color') ?>: <span id="placer_color"></span></p>
                <p><?php echo lang('materials') ?>: <span id="placer_material"></span></p>
                <p><?php echo lang('size') ?>: <span id="placer_size"></span></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn-shirley unfancy btn-sm" id="approved_btn" data-dismiss="modal">Approved</button>

                <button type="button" class="btn-shirley unfancy btn-sm" data-dismiss="modal"><?php echo lang('close') ?></button>

            </div>
        </div>

    </div>
</div>











<script>


</script>
<script>
    $(document).ready(function()
    {
        $(".custom_order_msg").animate({opacity: 1.0}, 5000).fadeOut('slow');
        $('.custom_order_msg').click(function()
        {
            $(this).hide();
        });
    });

    function getrelatedColor()
    {
        $('#hidden_style').val('');
        var value =  $('#style option:selected').text();
        $('#hidden_style').val(value);
        $('#color').html('<option>Loading</option>');
        $('#material').html('<option>Materials</option>');
        $('#size').html('<option>Size</option>');

        var id = $('#style').val();
        $.post("<?php   echo site_url('custom_order/getRelationJson')?>", {id:id,key:'style_id',search:'color_name',search_index:'color_id'}, function(result){
            $('#color').html('<option>Colors</option>');
            $.each( result.data, function( key, value ) {
                $('#color').append('<option value="'+value.color_id+'">'+value.color_name_<?php echo $this->culture_code ?>+'</option>') 
            });
        },'json');
    }

    function getrelatedMaterial()
    {
        $('#hidden_color').val('');
        var value =  $('#color option:selected').text();
        $('#hidden_color').val(value);
        $('#material').html('<option>Loading</option>');
        $('#size').html('<option>Size</option>');
        
        var id = $('#color').val();
        var style_id = $('#style').val();

        $.post("<?php   echo site_url('custom_order/getRelationJson')?>", {id:id,key:'color_id',search:'name',search_index:'material_id', style_id:style_id }, function(result){
            $('#material').html('<option>Materials</option>');

            $.each( result.data, function( key, value ) {
                $('#material').append('<option value="'+value.material_id+'">'+value.name_<?php echo $this->culture_code ?>+'</option>') 
            });
        },'json');
    }

    function getrelatedSize()
    {
        $('#hidden_material').val('');
        var value =  $('#material option:selected').text();
        $('#hidden_material').val(value);
        $('#size').html('<option>Loading</option>');
        var id = $('#material').val();
        var style_id = $('#style').val();
        var color_id = $('#color').val();

        $.post("<?php   echo site_url('custom_order/getRelationJson')?>", {id:id,key:'material_id',search:'size_title',search_index:'size_id', style_id:style_id, color_id:color_id}, function(result){
            $('#size').html('<option>Sizes</option>');

            $.each( result.data, function( key, value ) {
                $('#size').append('<option value="'+value.size_id+'">'+value.size_title_<?php echo $this->culture_code ?>+'</option>') 
            });
        },'json');
    }

    function hiddenchange()
    {
        $('#hidden_size').val('');
        var value =  $('#size option:selected').text();
        $('#hidden_size').val(value);
    }

    $('#submit_btn').on('click',function(e){
        $('#placer_name').html($('#first_name').val());
        $('#placer_address').html($('#address').val());
        $('#placer_contact').html($('#Contact').val());
        $('#placer_email').html($('#Email').val());
        $('#placer_style').html($('#hidden_style').val());
        $('#placer_color').html($('#hidden_color').val());
        $('#placer_material').html($('#hidden_material').val());
        $('#placer_size').html($('#hidden_size').val());
        $('#verification_modal').modal('show');
    });

    $('#approved_btn').on('click',function(e){
        $('#custom_order_form').submit();
    });
</script>
