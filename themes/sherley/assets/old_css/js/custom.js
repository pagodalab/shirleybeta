$(document).ready(function(){

	var windowheight=$(window).height();
	$('.fullpage').height(windowheight);

	$('.navbar-toggle').click(function(){
		$('.navbar-collapse').toggleClass('collapse')
	});

	$('.toggle').click(function(){
		$('.side_pannel').toggleClass('open');
		$('.full_content').toggleClass('squeeze');
		$('.overlay').toggleClass('shown');
		$('body').css("overflow", "hidden");
	});

	$('.side_pannel_close').click(function(){
		$('.side_pannel').removeClass('open');
		$('.full_content').removeClass('squeeze');
		$('.overlay').removeClass('shown');
		$('body').css("overflow", "visible");
		$('.left_menu').removeClass('left_open');
	});

	$('.search_full').click(function(){
		$('.search_bar_mobile').toggleClass('shown');
	});

	$('.menu_ham').click(function(){
		$('.left_menu').toggleClass('left_open');
		$('.overlay').toggleClass('shown');
		$('body').css("overflow", "hidden");
	});


	$('.has_children').click(function(e){
		e.preventDefault();

		if($(this).parent('li').children('ul').hasClass('open')) {
			$(this).parent('li').children('ul').removeClass('open');
		}

		else {
			$('.sub-menu').removeClass('open');
			$(this).parent('li').children('ul').addClass('open');
		}
	});


	$('.overlay').click(function(e){
		e.preventDefault();

		if($('.overlay').hasClass('shown')) {
			$('.overlay').removeClass('shown');
			$('.side_pannel').removeClass('open');
			$('.full_content').removeClass('squeeze');
			$('body').css("overflow", "visible");
			$('.left_menu').removeClass('left_open');
		}
	});

	var result = $(window).height();
	$(".cart_products").css('max-height', result - 325);
	
	
	

	

});

$(window).load(function(){
	$(".cart_products").mCustomScrollbar({
		theme: 'light-thin'
	});
});