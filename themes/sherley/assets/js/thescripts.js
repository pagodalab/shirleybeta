var winWidth = $(window).width();
var winHeight = $(window).height();


$(document).ready(function () {
    cartInit();
    menuInit();
    slickInit();
    heightInit();


    $(".ft-cust").hover(
        function () {
            $('.koala-section').addClass('hide')
        },
        function () {
            $('.koala-section').removeClass('hide')
        }
    );



});

$(window).resize(function(){
    var winWidth = $(window).width();
    var winHeight = $(window).height();
    if(winWidth > 768){
        $('#main_menu').css('height', 'auto');
    }
});


function slickInit(){

    $('.home-banner-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        speed: 200,
        pauseOnHover: false
    });

    /* Home Slider Item */
    $('.home-new-product-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 1000,
        pauseOnHover: true
    });
    /* Home Slider Item */

    $('.product-category-page-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        autoplay: true,
        speed: 200
    });

    $('.product-category-page-slider').slick('slickPause');

    $('.product-category-page-slider').hover(
        function () {
            $(this).slick('slickPlay');
        },
        function () {
            $(this).slick('pause');
        }
    );

}

function menuInit(){

    if(winWidth > 768) {

        $('.main-menu > ul > li').hover(
            function () {
                $(this).children('a').append("<div class='liner lineAnimStart'></div>");
            },
            function () {
                $('.liner').animate({
                    left: '100%'
                }, 300, 'swing', function () {
                    $(this).remove();
                });
            }
        );

    }


    /* For the header navigation */
    $('.nav-toggle').click(function () {
        $('body').addClass('mobile-nav-open').css('overflow', 'hidden');
        $('body').width(winWidth);
    });

    $('.mobile-nav .mobile-logo-container .fa').click(function () {
        $('body').removeClass('mobile-nav-open').css('overflow', 'initial');
    });

    $('#content-wrapper').click(function () {
        $('body').removeClass('mobile-nav-open').css('overflow', 'initial');
    });
    /* For the header navigation */


    if(winWidth <= 767){
        /* For the dropdown links inside mobile menu */
        $( "#main_menu .sub-menu" ).parent('li').addClass( "has-sub-menu" );
        $('.has-sub-menu > a').click(function (e) {
            e.preventDefault();
        });

        $('#main_menu li').click(function () {

            if($(this).hasClass('subMenuOpen')) {
                $(this).removeClass('subMenuOpen');
            }
            else {
                $('#main_menu li').removeClass('subMenuOpen');
                $(this).addClass('subMenuOpen');
            }
        });
        /* For the dropdown links inside mobile menu */

        /* For mobile menu height */
        var mobileLogo = $('#mobile-nav').outerHeight();
        var mobileMenuHeight = (winHeight - mobileLogo);
        $('#main_menu').height(mobileMenuHeight);

        $('#main_menu').mCustomScrollbar({
            theme: 'dark-thin'
        });
        /* For mobile menu height */
    }




        /*    $('.mobile-menu').click(function () {
         $('#main_menu').addClass('open');
         $('body').width(winWidth).addClass('menuOpen');
         });

         $('#menu_underlay').click(function () {
         $('#main_menu').removeClass('open');
         $('body').removeClass('menuOpen').removeClass('cartOpen').css('width', 'auto'); //CART CARD CHANGE
         });

         $('.mobile-nav-logo .fa').click(function () {
         $('#main_menu').removeClass('open');
         $('body').removeClass('menuOpen').removeClass('cartOpen').css('width', 'auto'); //CART CARD CHANGE
         });

         $( "#main_menu .sub-menu" ).parent('li').addClass( "has-sub-menu" );

         if(winWidth <= 769){
         $('.has-sub-menu > a').click(function (e) {
         e.preventDefault();
         });
         }*/


}

function cartInit(){
    var cartHeadHeight = $('#cart_head').outerHeight();
    console.log(cartHeadHeight);
    var cartFootHeight = $('#cart_foot').outerHeight();
    var cartBodyHeight = winHeight - (cartHeadHeight + cartFootHeight);


    $('#cart_body').height(cartBodyHeight);

    $('#cart_body').mCustomScrollbar({
        theme: 'dark-thin'
    });

    $('#cartBag').click(function (e) {
        e.preventDefault();
        $('body').width(winWidth).addClass('cartOpen');
    });


    $('#cart_head a.close').click(function(e){
        e.preventDefault();
        $('body').removeClass('cartOpen').css('width', 'auto');
    });

    $('#menu_underlay').click(function () {
        $('body').removeClass('cartOpen').css('width', 'auto');
    });
}


function heightInit() {
    /* Home Banner Height */
    var navHeight = $('.top-header').outerHeight();

    $('.home-banner-slider .home-banner-item').height(winHeight - (navHeight));
    /* Home Banner Height Ends*/
}











